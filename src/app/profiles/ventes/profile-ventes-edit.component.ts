import { Component, OnInit, Input, EventEmitter, Output, AfterContentChecked,
  AfterViewChecked
  } from '@angular/core';
  // import { ArticleService } from './article.service';
  
  import {ProfileVentes } from './profile-ventes.model';
  import {ProfilesService } from '../profiles.service';
  
  import {FormControl} from '@angular/forms';
  import {map, startWith} from 'rxjs/operators';
  import {Observable} from 'rxjs';
  import { Router, ActivatedRoute, ParamMap } from '@angular/router';
  import {MatSnackBar} from '@angular/material';
  @Component({
    selector: 'app-profile-ventes-edit',
    templateUrl: './profile-ventes-edit.component.html',
    // styleUrls: ['./players.component.css']
  })
  export class ProfileVentesEditComponent implements OnInit, AfterContentChecked {
  
  // private client: Client = new Client();
  
  
  private profileVentes:  ProfileVentes = new  ProfileVentes();
  
    constructor( private route: ActivatedRoute,
      private router: Router , 
      private profilesService: ProfilesService, private matSnackBar: MatSnackBar,
     ) {
    }
    ngOnInit() {
    
      
        const codeProfileVentes = this.route.snapshot.paramMap.get('codeProfileVentes');
  
    }
  
    loadGridProfilesVentes() {
      this.router.navigate(['/pms/administration/profilesVentes']) ;
    }
   /* supprimerUtilisateur() {
       
        this.utilisateurService.deleteUtilisateur(this.utilisateur.code).subscribe((response) => {
  
          this.openSnackBar( ' Element Supprimé');
          this.loadGridUtilisateurs() ;
              });
    }*/
  updateProfileVentes() {
  
    this.profilesService.updateProfileVentes(this.profileVentes).subscribe((response) => {
      if (response.error_message ===  undefined) {
        this.openSnackBar( ' Profile mis à jour ');
      } else {
        this.openSnackBar( ' Erreurs.. ');
        
       // show error message
      }
  });
      
  }
  
   openSnackBar(messageToDisplay) {
    this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
        verticalPosition: 'top'});
   }
  
  ngAfterContentChecked() {
  
  }
  
  }
  
  
  