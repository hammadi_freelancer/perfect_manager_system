import { Component, OnInit, Output, ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
// import {AccessRight} from './access-right.model';
import {ProfileVentes} from './profile-ventes.model';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {ProfilesService} from '../profiles.service';
import { ProfileVentesElement } from './profile-ventes.interface';

import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
// import { DialogAddDocumentClientComponent} from './popups';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {ProfileVentesDataFilter } from './profile-ventes-data.filter';
import { SelectItem } from 'primeng/api';
@Component({
  selector: 'app-profiles-ventes-grid',
  templateUrl: './profiles-ventes-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class ProfilesVentesGridComponent implements OnInit {

  private profileVentes: ProfileVentes =  new ProfileVentes();
  private showFilter = false;
  private showSelectColumnsMultiSelect = false;
  private profileVentesDataFilter = new ProfileVentesDataFilter();
  private selection = new SelectionModel<ProfileVentes>(true, []);

  @Input()
  private listProfilesVentes: any = [] ;


 selectedProfileVentes: ProfileVentes ;

private deleteEnCours = false;

 private  dataProfilesVentesSource: MatTableDataSource<ProfileVentesElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay :ColumnDefinition[] = [];

 private columnsToSelect : SelectItem[];
  private  selectedColumnsDefinitions: string[];

 private selectedColumnsNames: string[];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];

  constructor( private route: ActivatedRoute, private profilesService: ProfilesService,
    private router: Router, private matSnackBar: MatSnackBar, private dialog: MatDialog ) {
  }
  ngOnInit() {
    this.defaultColumnsDefinitions = [
      {name: 'code' , displayTitle: 'Code'},
     {name: 'description' , displayTitle: 'Déscription'}
  ];
    
    this.columnsToSelect = [
      {label: 'Code', value: 'code', title: 'Code'},
      {label: 'Déscription', value: 'description', title: 'Déscription'}
      
      
  ];
  this.selectedColumnsDefinitions = [
    'code',
   'description'
    ];
    this.profilesService.getPageProfilesVentes(0, 5, null).subscribe((profiles) => {
      this.listProfilesVentes= profiles;
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });
  

  }
  deleteProfileVentes(profileVentes) {
     // console.log('call delete !', client );
    this.profilesService.deleteProfileVentes(profileVentes.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData(null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.profilesService.getPageProfilesVentes(0, 5, filter).subscribe((profiles) => {
    this.listProfilesVentes = profiles;
    // this.listClients[this.listClients.length] = this.lastClientAdded;
    this.transformDataToByConsumedByMatTable();
   });
  // this.listClientsOrgin = this.listClients ;
}


checkOneActionInvoked() {
  const listSelectedProfilesVentes: ProfileVentes[] = [];

if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((pElement) => {
  return pElement.code;
});
this.listProfilesVentes.forEach(gr => {
  if (codes.findIndex(code => code === gr.code) > -1) {
    listSelectedProfilesVentes.push(gr);
  }
  });
}
// this.select.emit(listSelectedClients);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listProfilesVentes !== undefined) {
    this.listProfilesVentes.forEach(pV => {
      listElements.push(
        {
          code: pV.code ,
          description: pV.description ,
      } );
    });
  }
    this.dataProfilesVentesSource = new MatTableDataSource<ProfileVentesElement>(listElements);
    this.dataProfilesVentesSource.sort = this.sort;
    this.dataProfilesVentesSource.paginator = this.paginator;
    this.profilesService.getTotalProfileVentes(this.profileVentesDataFilter).subscribe((response) => {
     //  console.log('Total Clients   is ', response);
     if (this.dataProfilesVentesSource.paginator !== undefined) {
       this.dataProfilesVentesSource.paginator.length = response['totalProfilesVentes'];
     }
     });
}

masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
    //   this.dataAccessRightsSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataProfilesVentesSource.data.length;
  return numSelected === numRows;
}
/*applyFilter(filterValue: string) {
  this.dataAccessRightsSource.filter = filterValue.trim().toLowerCase();
  if (this.dataAccessRightsSource.paginator) {
    this.dataAccessRightsSource.paginator.firstPage();
  }
}*/
loadAddProfileVentesComponent() {
  this.router.navigate(['/pms/administration/ajouterProfileVentes']) ;

}
loadEditProfileVentesComponent() {
  if (this.selection.selected.length === 0) {
    this.openSnackBar(' Aucun Profile Ventes sélectionné' );
    // this.loadData();
    } else {
  this.router.navigate(['/pms/administration/editerProfileVentes', this.selection.selected[ this.selection.selected.length - 1].code]) ;
    }
}
supprimerProfilesVentes()
{
  if ( this.selection.selected.length !== 0) {
    this.deleteEnCours = true;
      this.selection.selected.forEach(function(profile, index) {
          this.profilesService.deleteProfileVentes(profile.code).subscribe((response) => {
            if (this.selection.selected.length - 1 === index) {
              this.deleteEnCours = false;
              this.openSnackBar( ' Profile(s) Ventes Supprimé(s)');
               this.loadData(null);
              }
          });
    
      }, this);
    } else {
      this.openSnackBar('Aucun Profile Ventes Séléctionné!');
    }
}
refresh()
{
  this.loadData(null);
  
}
  openSnackBar(messageToDisplay) {
 this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
     verticalPosition: 'top'});
}

changePage($event) {
  console.log('page event is ', $event);
 this.profilesService.getPageProfilesVentes($event.pageIndex, $event.pageSize,
  this.profileVentesDataFilter).subscribe((profiles) => {
    this.listProfilesVentes = profiles;
    const listElements = [];
 if (this.listProfilesVentes !== undefined) {
    this.listProfilesVentes.forEach(pV => {
      listElements.push( {
        code: pV.code ,
        description: pV.description
      }
      );
    });
  }
     this.dataProfilesVentesSource= new MatTableDataSource<ProfileVentesElement>(listElements);
     this.profilesService.getTotalProfileVentes(this.profileVentesDataFilter).
     subscribe((response) => {
      console.log('Total clients is ', response);
      // this.dataClientsSource.paginator = this.paginator;
      if (this.dataProfilesVentesSource.paginator) {
        
       this.dataProfilesVentesSource.paginator.length = response.totalProfilesVentes;
      }
     });

    });
}
toggleFilterPanel() {
  this.showFilter = !this.showFilter;
}
toggleShowColumnsMultiSelect() {
 this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
}
loadFiltredData() {
 // console.log('the filter is ', this.utilisateurDataFilter);
 this.loadData(this.profileVentesDataFilter);
}

specifyListColumnsToBeDisplayed() {
  
   console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
   this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
 ?
    this.defaultColumnsDefinitions : [];
    this.selectedColumnsNames = [];
    
    if (this.selectedColumnsDefinitions !== undefined)
     {
       this.selectedColumnsDefinitions.forEach((colName) => { 
            const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
            const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
            const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
            this.columnsDefinitionsToDisplay.push(colDef);
           });
     }
    this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
   this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
              return colDef.displayTitle;
    });
    console.log('columns to disp:', this.columnsDefinitionsToDisplay);
    this.columnsNames = [];
    this.columnsNames[0] = 'select';
    this.columnsDefinitionsToDisplay .map((colDef) => {
     this.columnsNames.push(colDef.name);
    if (this.selectedColumnsDefinitions !== undefined) {
     this.selectedColumnsNames.push(colDef.displayTitle);
    }
 });
    this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
     return colDef.displayTitleAr;
 });
 }






}
