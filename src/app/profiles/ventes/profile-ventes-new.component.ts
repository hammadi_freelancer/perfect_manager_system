import { Component, OnInit, Input, EventEmitter, Output,
  AfterViewChecked
  } from '@angular/core';
  // import { ArticleService } from './article.service';
  import {ProfileVentes} from './profile-ventes.model';
  import {ProfilesService} from '../profiles.service';
  import {FormControl} from '@angular/forms';
  import {map, startWith} from 'rxjs/operators';
  import {Observable} from 'rxjs';
  import { Router, ActivatedRoute, ParamMap } from '@angular/router';
  import {MatSnackBar} from '@angular/material';
 //  import { SelectValues } from './select-values.interface';
  @Component({
    selector: 'app-profile-ventes-new',
    templateUrl: './profile-ventes-new.component.html',
    // styleUrls: ['./players.component.css']
  })
export class ProfileVentesNewComponent implements OnInit {
  private profileVentes: ProfileVentes = new ProfileVentes();


    constructor( private route: ActivatedRoute,
      private router: Router ,
       private profilesService: ProfilesService,
       private matSnackBar: MatSnackBar ) {
    }
    ngOnInit() {
   
    }
  saveProfileVentes() {
    
      this.profilesService.addProfileVentes(this.profileVentes).subscribe((response) => {
          if (response.error_message ===  undefined) {
            // this.save.emit(response);
            this.profileVentes = new ProfileVentes();
            this.openSnackBar('Profile Ventes Enregistré');
          } else {
           // show error message
           this.openSnackBar(  'Erreurs!');
           
          }
      });
    }
  
  loadGridProfilesVentes() {
    this.router.navigate(['/pms/administration/profilesVentes']) ;
  }
  
     openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }

  }
