import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';

import {ProfileAchats} from './achats/profile-achats.model' ;
import {ProfileVentes} from './ventes/profile-ventes.model' ;

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';

const URL_GET_TOTAL_PROFILES_ACHATS = 'http://localhost:8082/profilesAchats/getTotalProfilesAchats';

const  URL_GET_LIST_PROFILES_ACHATS = 'http://localhost:8082/profilesAchats/getProfilesAchats';
const  URL_GET_PAGE_PROFILES_ACHATS = 'http://localhost:8082/profilesAchats/getPageProfilesAchats';

const URL_ADD_PROFILES_ACHATS = 'http://localhost:8082/profilesAchats/addProfileAchats';
const URL_GET_PROFILE_ACHATS = 'http://localhost:8082/profilesAchats/getProfileAchats';

const URL_UPDATE_PROFILES_ACHATS = 'http://localhost:8082/profilesAchats/updateProfileAchats';
const URL_DELETE_PROFILES_ACHATS = 'http://localhost:8082/profilesAchats/deleteProfileAchats';



const URL_GET_TOTAL_PROFILES_VENTES = 'http://localhost:8082/profilesVentes/getTotalProfilesVentes';

const  URL_GET_LIST_PROFILES_VENTES = 'http://localhost:8082/profilesVentes/getProfilesVentes';
const  URL_GET_PAGE_PROFILES_VENTES= 'http://localhost:8082/profilesVentes/getPageProfilesVentes';

const URL_ADD_PROFILES_VENTES = 'http://localhost:8082/profilesVentes/addProfileVentes';
const URL_GET_PROFILE_VENTES = 'http://localhost:8082/profilesVentes/getProfileVentes';

const URL_UPDATE_PROFILES_VENTES = 'http://localhost:8082/profilesVentes/updateProfileVentes';
const URL_DELETE_PROFILES_VENTES = 'http://localhost:8082/profilesVentes/deleteProfileVentes';



const URL_GET_TOTAL_PROFILES_STOCKS = 'http://localhost:8082/profilesStocks/getTotalProfilesStocks';

const  URL_GET_LIST_PROFILES_STOCKS = 'http://localhost:8082/profilesStocks/getProfilesStocks';
const  URL_GET_PAGE_PROFILES_STOCKS = 'http://localhost:8082/profilesStocks/getPageProfilesStocks';

const URL_ADD_PROFILES_STOCKS = 'http://localhost:8082/profilesStocks/addProfileStocks';
const URL_GET_PROFILE_STOCKS = 'http://localhost:8082/profilesStocks/getProfileStocks';

const URL_UPDATE_PROFILES_STOCKS = 'http://localhost:8082/profilesStocks/updateProfileStocks';
const URL_DELETE_PROFILES_STOCKS = 'http://localhost:8082/profilesStocks/deleteProfileStocks';



@Injectable({
  providedIn: 'root'
})
export class ProfilesService {
 
  constructor(private httpClient: HttpClient) {

   }

   addProfileAchats(profileAchats: ProfileAchats): Observable<any> {
    return this.httpClient.post<ProfileAchats>(URL_ADD_PROFILES_ACHATS, profileAchats);
  }
  
  getProfileAchats(codeProfileAchats): Observable<ProfileAchats> {
    return this.httpClient.get<ProfileAchats>(URL_GET_PROFILE_ACHATS + '/' + codeProfileAchats);
  }
   updateProfileAchats(profileAchats: ProfileAchats): Observable<any> {
    return this.httpClient.put<ProfileAchats>(URL_UPDATE_PROFILES_ACHATS, profileAchats);
  }
  deleteProfileAchats(codeProfileAchats): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_PROFILES_ACHATS + '/' + codeProfileAchats);
  }
    getPageProfilesAchats(pageNumber, nbElementsPerPage, filter): Observable<ProfileAchats[]> {
      const filterStr = JSON.stringify(filter);
      const params = new HttpParams()
      .set('pageNumber', pageNumber)
      .set('nbElementsPerPage', nbElementsPerPage)
      .set('sort', 'name')
      .set('filter', filterStr);
      return this.httpClient
      .get<ProfileAchats[]>(URL_GET_PAGE_PROFILES_ACHATS, {params});
     }
     getListProfilesAchats(filter): Observable<ProfileAchats[]> {
      const filterStr = JSON.stringify(filter);
      const params = new HttpParams()
      .set('filter', filterStr);
      return this.httpClient
      .get<ProfileAchats[]>(URL_GET_LIST_PROFILES_ACHATS, {params});
     }
     getTotalProfileAchats(filter): Observable<any> {
      
          const filterStr = JSON.stringify(filter);
          const params = new HttpParams()
          .set('filter', filterStr);
          return this.httpClient
          .get<any>(URL_GET_TOTAL_PROFILES_ACHATS, {params});
      }

      addProfileVentes(profileVentes: ProfileVentes): Observable<any> {
        return this.httpClient.post<ProfileVentes>(URL_ADD_PROFILES_VENTES, profileVentes);
      }
      
      getProfileVentes(codeProfileVentes): Observable<ProfileVentes> {
        return this.httpClient.get<ProfileVentes>(URL_GET_PROFILE_VENTES + '/' + codeProfileVentes);
      }
       updateProfileVentes(profileVentes: ProfileVentes): Observable<any> {
        return this.httpClient.put<ProfileVentes>(URL_UPDATE_PROFILES_VENTES, profileVentes);
      }
      deleteProfileVentes(codeProfileVentes): Observable<any> {
        return this.httpClient.delete<any>(URL_DELETE_PROFILES_VENTES + '/' + codeProfileVentes);
      }
        getPageProfilesVentes(pageNumber, nbElementsPerPage, filter): Observable<ProfileVentes[]> {
          const filterStr = JSON.stringify(filter);
          const params = new HttpParams()
          .set('pageNumber', pageNumber)
          .set('nbElementsPerPage', nbElementsPerPage)
          .set('sort', 'name')
          .set('filter', filterStr);
          return this.httpClient
          .get<ProfileVentes[]>(URL_GET_PAGE_PROFILES_VENTES, {params});
         }
         getTotalProfileVentes(filter): Observable<any> {
          
              const filterStr = JSON.stringify(filter);
              const params = new HttpParams()
              .set('filter', filterStr);
              return this.httpClient
              .get<any>(URL_GET_TOTAL_PROFILES_VENTES, {params});
          }
          getListProfilesVentes(filter): Observable<ProfileVentes[]> {
            const filterStr = JSON.stringify(filter);
            const params = new HttpParams()
            .set('filter', filterStr);
            return this.httpClient
            .get<ProfileVentes[]>(URL_GET_LIST_PROFILES_VENTES, {params});
           }

}
