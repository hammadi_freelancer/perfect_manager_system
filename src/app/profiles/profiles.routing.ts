import { Injectable } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { ProfilesAchatsGridComponent } from './achats/profiles-achats-grid.component';
import { ProfileAchatsNewComponent } from './achats/profile-achats-new.component';
import { ProfileAchatsEditComponent } from './achats/profile-achats-edit.component';

import { ProfilesVentesGridComponent } from './ventes/profiles-ventes-grid.component';
import { ProfileVentesNewComponent } from './ventes/profile-ventes-new.component';
import { ProfileVentesEditComponent } from './ventes/profile-ventes-edit.component';


import {ModuleWithProviders  } from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';

 export const  profilesRoute: Routes = [
    {
        path: 'profilesAchats',
        component: ProfilesAchatsGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterProfileAchats',
        component: ProfileAchatsNewComponent,
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerProfileAchats/:codeProfileAchats',
        component: ProfileAchatsEditComponent,
        resolve: { 
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'profilesVentes',
        component: ProfilesVentesGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterProfileVentes',
        component: ProfileVentesNewComponent,
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerProfileVentes/:codeProfileVentes',
        component: ProfileVentesEditComponent,
        resolve: { 
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    }
];



