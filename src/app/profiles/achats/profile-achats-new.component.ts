import { Component, OnInit, Input, EventEmitter, Output,
    AfterViewChecked
    } from '@angular/core';
    // import { ArticleService } from './article.service';
    import {ProfileAchats} from './profile-achats.model';
    import {ProfilesService} from '../profiles.service';
    import {FormControl} from '@angular/forms';
    import {map, startWith} from 'rxjs/operators';
    import {Observable} from 'rxjs';
    import { Router, ActivatedRoute, ParamMap } from '@angular/router';
    import {MatSnackBar} from '@angular/material';
   //  import { SelectValues } from './select-values.interface';
    @Component({
      selector: 'app-profile-achats-new',
      templateUrl: './profile-achats-new.component.html',
      // styleUrls: ['./players.component.css']
    })
export class ProfileAchatsNewComponent implements OnInit {
    private profileAchats: ProfileAchats = new ProfileAchats();


      constructor( private route: ActivatedRoute,
        private router: Router ,
         private profilesService: ProfilesService,
         private matSnackBar: MatSnackBar ) {
      }
      ngOnInit() {
     
      }
    saveProfileAchats() {
      
        this.profilesService.addProfileAchats(this.profileAchats).subscribe((response) => {
            if (response.error_message ===  undefined) {
              // this.save.emit(response);
              this.profileAchats = new ProfileAchats();
              this.openSnackBar('Profile Achats Enregistré');
            } else {
             // show error message
             this.openSnackBar(  'Erreurs!');
             
            }
        });
      }
    
    loadGridProfilesAchats() {
      this.router.navigate(['/pms/administration/profilesAchats']) ;
    }
    
       openSnackBar(messageToDisplay) {
        this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
            verticalPosition: 'top'});
       }
 
    }
