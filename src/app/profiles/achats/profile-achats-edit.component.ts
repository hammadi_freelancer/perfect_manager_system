import { Component, OnInit, Input, EventEmitter, Output, AfterContentChecked,
AfterViewChecked
} from '@angular/core';
// import { ArticleService } from './article.service';

import {ProfileAchats } from './profile-achats.model';
import {ProfilesService } from '../profiles.service';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {MatSnackBar} from '@angular/material';
@Component({
  selector: 'app-profile-achats-edit',
  templateUrl: './profile-achats-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class ProfileAchatsEditComponent implements OnInit, AfterContentChecked {

// private client: Client = new Client();


private profileAchats:  ProfileAchats = new  ProfileAchats();

  constructor( private route: ActivatedRoute,
    private router: Router , 
    private profilesService: ProfilesService, private matSnackBar: MatSnackBar,
   ) {
  }
  ngOnInit() {
  
    
      const codeProfileAchats = this.route.snapshot.paramMap.get('codeProfileAchats');

  }

  loadGridProfilesAchats() {
    this.router.navigate(['/pms/administration/profilesAchats']) ;
  }
 /* supprimerUtilisateur() {
     
      this.utilisateurService.deleteUtilisateur(this.utilisateur.code).subscribe((response) => {

        this.openSnackBar( ' Element Supprimé');
        this.loadGridUtilisateurs() ;
            });
  }*/
updateProfileAchats() {

  this.profilesService.updateProfileAchats(this.profileAchats).subscribe((response) => {
    if (response.error_message ===  undefined) {
      this.openSnackBar( ' Profile mis à jour ');
    } else {
      this.openSnackBar( ' Erreurs.. ');
      
     // show error message
    }
});
    
}

 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

ngAfterContentChecked() {

}

}


