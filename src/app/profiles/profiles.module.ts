import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
 import { AppComponent } from '../app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ReactiveFormsModule } from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatChipsModule} from '@angular/material/chips';
import {MatMenuModule} from '@angular/material/menu';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {TableModule} from 'primeng/table';
import {MatSortModule} from '@angular/material/sort';
import {MatSelectModule} from '@angular/material/select';
import {MatTabsModule} from '@angular/material/tabs';
import {PickListModule} from 'primeng/picklist';
import {MultiSelectModule} from 'primeng/multiselect';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatProgressBarModule} from '@angular/material/progress-bar';

// import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';
import { ProfilesAchatsGridComponent } from './achats/profiles-achats-grid.component';
import { ProfileAchatsNewComponent } from './achats/profile-achats-new.component';
import { ProfileAchatsEditComponent } from './achats/profile-achats-edit.component';
import { HomeProfileAchatsComponent } from './achats/home-profile-achats.component';

import { ProfilesVentesGridComponent } from './ventes/profiles-ventes-grid.component';
import { ProfileVentesNewComponent } from './ventes/profile-ventes-new.component';
import { ProfileVentesEditComponent } from './ventes/profile-ventes-edit.component';
import { HomeProfileVentesComponent } from './ventes/home-profile-ventes.component';

 import { JwtModule, JWT_OPTIONS  } from '@auth0/angular-jwt';
 import { JwtHelperService } from '@auth0/angular-jwt';
import {FormsModule } from '@angular/forms';
// import { commonRoute } from './common.route';
 import {APP_BASE_HREF} from '@angular/common';

   import { AccessRightsResolver } from '../utilisateurs/accessRights-resolver';
  //  import { Resolver } from '../utilisateurs/ut';
   
 // const BASE_URL = [{provide: APP_BASE_HREF, useValue: '/common'}];



 import {MatPaginatorModule} from '@angular/material/paginator';
 import { RouterModule, provideRoutes} from '@angular/router';
 // import { CommunicationService } from './magasins/communication.service';
 import {MatSidenavModule} from '@angular/material/sidenav';
 import {MatRadioModule} from '@angular/material/radio';
@NgModule({
  declarations: [
  
    ProfilesAchatsGridComponent,
    ProfileAchatsNewComponent,
    ProfileAchatsEditComponent,

    ProfileVentesEditComponent,
    ProfileVentesNewComponent,
    ProfilesVentesGridComponent,
    
    HomeProfileVentesComponent,
    HomeProfileAchatsComponent
    
 
  ],
  imports: [
    BrowserModule, HttpClientModule, MatDatepickerModule, MatNativeDateModule,
     BrowserAnimationsModule, ReactiveFormsModule, MatAutocompleteModule,
    MatInputModule, MatCardModule, MatCheckboxModule, MatButtonModule, MatIconModule,
    MatMenuModule, MatProgressSpinnerModule, MatTableModule, RouterModule,
    FormsModule, TableModule , MatSortModule, MatSelectModule, MatTabsModule,
     PickListModule, MatPaginatorModule, MatSidenavModule, MatRadioModule, MatChipsModule,
     JwtModule, MultiSelectModule, MatExpansionModule, MatProgressBarModule
     

],
  providers: [MatNativeDateModule, AccessRightsResolver, JwtHelperService ,  // BASE_URL
  ],
  exports : [
    RouterModule,

  ],
  bootstrap: [AppComponent]
})
export class ProfilesModule { }
