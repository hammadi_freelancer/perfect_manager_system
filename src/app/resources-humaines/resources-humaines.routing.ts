import { Injectable } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { EmployesGridComponent } from './employes/employes-grid.component';
import { EmployeNewComponent } from './employes/employe-new.component';
import { EmployeEditComponent } from './employes/employe-edit.component';

import {ModuleWithProviders  } from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';


export const RHRoute: Routes = [
    {
        path: 'employes',
        component: EmployesGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterEmploye',
        component: EmployeNewComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerEmploye/:codeEmploye',
        component: EmployeEditComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
];

