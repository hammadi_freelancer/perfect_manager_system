
import { Component, OnInit, Inject,Input, ViewChild, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {EmployeService} from './employe.service';
import {Employe} from './employe.model';

import {PayementEmploye} from './payement-employe.model';
import { PayementEmployeElement } from './payement-employe.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogAddPayementEmployeComponent } from './popups/dialog-add-payement-employe.component';
import { DialogEditPayementEmployeComponent} from './popups/dialog-edit-payement-employe.component';
@Component({
    selector: 'app-payements-employes-grid',
    templateUrl: 'payements-employes-grid.component.html',
  })
  export class PayementsEmployesGridComponent implements OnInit, OnChanges {
     private listPayementsEmployes: PayementEmploye[] = [];
     private  dataPayementsEmployesSource: MatTableDataSource<PayementEmployeElement>;
     private selection = new SelectionModel<PayementEmployeElement>(true, []);
    
     @Input()
     private employe : Employe;

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private employeService: EmployeService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.employeService.getPayementsEmployesByCodeEmploye(this.employe.code).subscribe((listPayementsEmployes) => {
        this.listPayementsEmployes= listPayementsEmployes;
  // console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
  }
  ngOnChanges()
  {
    this.specifyListColumnsToBeDisplayed();
    
    this.employeService.getPayementsEmployesByCodeEmploye(this.employe.code).subscribe((listPayementsEmployes) => {
      this.listPayementsEmployes = listPayementsEmployes;
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }
  checkOneActionInvoked() {
    const listSelectedPayementsEmployes: PayementEmploye[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((payementEElement) => {
    return payementEElement.code;
  });
  this.listPayementsEmployes.forEach(payEmp => {
    if (codes.findIndex(code => code === payEmp.code) > -1) {
        listSelectedPayementsEmployes.push(payEmp);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listPayementsEmployes!== undefined) {

      this.listPayementsEmployes.forEach(payEmp => {
             listElements.push( {code: payEmp.code ,
          datePaiement: payEmp.datePaiement,
          numeroPaiement: payEmp.numeroPaiement,
          montant: payEmp.montant,
          motif: payEmp.motif,
          etat: payEmp.etat,
          description: payEmp.description
        } );
      });
    }
      this.dataPayementsEmployesSource = new MatTableDataSource<PayementEmployeElement>(listElements);
      this.dataPayementsEmployesSource.sort = this.sort;
      this.dataPayementsEmployesSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [
     {name: 'numeroPaiement' , displayTitle: 'N°Paiement'}, 
     {name: 'datePaiement' , displayTitle: 'Date Paiement'},
     {name: 'montant' , displayTitle: 'Montant'},
     {name: 'motif' , displayTitle: 'Motif'},
     {name: 'etat' , displayTitle: 'Etat'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataPayementsEmployesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataPayementsEmployesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataPayementsEmployesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataPayementsEmployesSource.paginator) {
      this.dataPayementsEmployesSource.paginator.firstPage();
    }
  }
  
  showAddPayementDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeEmploye : this.employe.code
   };
 
   const dialogRef = this.dialog.open(DialogAddPayementEmployeComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(payAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditPayementDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codePayementEmploye : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditPayementEmployeComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(payAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Paiement Séléctionnée!');
    }
    }
    supprimerPayements()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(doc, index) {
              this.employeService.deletePayementEmploye(doc.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar(  ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Paiement Séléctionnée!');
        }
    }
    refresh() {
      this.employeService.getPayementsEmployesByCodeEmploye(this.employe.code).subscribe((listPayementsEmployes) => {
        this.listPayementsEmployes = listPayementsEmployes;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
