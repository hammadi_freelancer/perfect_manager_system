export class EmployeDataFilter
{
    code: string;
    matricule: string;
    numeroCNSS: string;
    numeroCIN: string;
    numeroPassport: string;
    numeroCarteSejour: string;
    nom: string;
    prenom: string;
    nomMere: string;
    prenomMere: string;
    nomPere: string;
    dateNaissanceFromObject: Date;
    dateIntegrationFromObject: Date;
    dateIntegrationToObject: Date;
    dateNaissanceToObject: Date;
    numeroRue: number;
    avenue: string;
    ville: string;
    pays: string;
    email: string;
    mobile: string;
    tel: string;
    nombreEnfants: number;
    nombreEnfantsHandicapes: number;
    niveauAcademique: string;
    grade: string;
    postePrincipale: string;
    posteSecondaire: string;
    competences: string;
    nombreAnnesExperiences: number;
    typeContrat: string;
    etatCivile: string;
}




