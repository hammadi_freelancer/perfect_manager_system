

import { BaseEntity } from '../../common/base-entity.model' ;
type Etat = 'Initial' | 'Valide' | 'Annule' | '';
type MOTIF = 'PAIEMENT_SALAIRE' | 'PAIEMENT_PRIMES' | 'VIREMENT_CNSS'
|'PAIEMENT_FRAIS_MISSION' |'AVANCE' |'AUTRE';

/*type Motif = 'Ventes'
 | 'PayementsCreditsClients' |
 'VirementCheque' |
  'Emprunts' ;*/

export class PayementEmploye extends BaseEntity {
   constructor(
       public  code?: string,
       public codeEmploye?: string,
    public numeroPaiement?: string,
    public datePaiementObject?: Date,
    public datePaiement?: string,
    public montant?: string,
    public motif?: MOTIF,
    public etat?: Etat,
    public imageFace1?: any,
    public imageFace2?: any,
) {
    super();
}
}
