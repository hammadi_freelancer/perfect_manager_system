import { Component, OnInit, Output, ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {Employe} from './employe.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {EmployeService} from './employe.service';
import { EmployeElement } from './employe.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';

import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import { EmployeDataFilter } from './employe-data.filter';
import {SelectItem} from 'primeng/api';
@Component({
  selector: 'app-employes-grid',
  templateUrl: './employes-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class EmployesGridComponent implements OnInit {

 //  private employe: Employe =  new Employe();
  private selection = new SelectionModel<EmployeElement>(true, []);

  private employeDataFilter: EmployeDataFilter = new EmployeDataFilter();
  private columnsToSelect : SelectItem[];
   private  selectedColumnsDefinitions: string[];
  private selectedColumnsNames: string[];
@Output()
select: EventEmitter<Employe[]> = new EventEmitter<Employe[]>();

//private lastClientAdded: Client;

 selectedEmploye: Employe ;
 private   showFilter = false;
 private   showFilterClient = false;
 private   showFilterFournisseur = false;
 private showSelectColumnsMultiSelect = false;

 @Input()
 private listEmployes: any = [] ;
 listEmployesOrgin: any = [];
 private checkedAll: false;
 private  dataEmployesSource: MatTableDataSource<EmployeElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay : ColumnDefinition[] = [];
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
 private etatsCiviles: any[] = [
  {value: 'Marie', viewValue: 'Marié'},
  {value: 'Divorce', viewValue: 'Divorcé'},
  {value: 'Celibataire', viewValue: 'Célibataire'},
];
private typesContrat: any[] = [
  {value: 'CDI', viewValue: 'CDI'},
  {value: 'CDD', viewValue: 'CDD'},
  {value: 'SIVP', viewValue: 'SIVP'},
  {value: 'Karama', viewValue: 'Karama'},
];
  constructor( private route: ActivatedRoute, private employeService: EmployeService,
    private router: Router, private matSnackBar: MatSnackBar ) {
  }
  ngOnInit() {
    this.columnsToSelect = [
      {label: 'Matricule', value: 'matricule', title: 'Matricule'},
      {label: 'N° CNSS', value: 'numeroCNSS', title: 'N° CNSS'},
      {label: 'N° CIN', value: 'numeroCIN', title: 'N° CIN'},
      {label: 'N° Passport', value: 'numeroPassport', title: 'N° Passport'},
      {label: 'N° Carte Séjour', value: 'numeroCarteSejour', title: 'N° Carte Séjour'},
      {label: 'Nom', value: 'nom', title: 'Nom'},
      {label: 'Prénom', value: 'prenom', title: 'Prénom'},      
      {label: 'Pére', value: 'nomPere', title: 'Père'},
      {label: 'Mère', value: 'nomMere', title: 'Mère'},
      {label: 'Date Naissance', value: 'dateNaissance', title: 'Date Naissance'},
      {label: 'Date Intégration', value:  'dateIntegration', title: 'Date Intégration' },
      {label: 'Rue', value:  'numeroRue', title: 'Rue' },
      {label: 'Avenue', value:  'avenue', title: 'Avenue' },
      {label: 'Ville', value:  'ville', title: 'Ville' },
      {label: 'Pays', value:  'pays', title: 'Pays' },
      {label: 'Email', value:  'email', title: 'Email' },
      {label: 'Mobile', value:  'mobile', title: 'Mobile' },
      {label: 'Tél', value:  'tel', title: 'Tél' },
      {label: 'Nbre Enfants', value:  'nombreEnfants', title: 'Nbre Enfants' },
      {label: 'Nbre Enfants Handicapés', value:  'nombreEnfantsHandicapes', title: 'Nbre Enfants Handicapés' },
      {label: 'Niveau Académique', value:  'niveauAcademique', title: 'Niveau Académique' },
      {label: 'Grade', value:  'grade', title: 'Grade' },
      {label: 'Poste Principale', value:  'postePrincipale', title: 'Poste Principale' },
      {label: 'Poste Secondaire', value:  'posteSecondaire', title: 'Poste Secondaire' },
      {label: 'Compétences', value:  'competences', title: 'Compétences' },
      {label: 'Nbre Années Expr.', value:  'nombreAnnesExperiences', title: 'Nbre Années Expr.' },
      {label: 'Type Contrat', value:  'typeContrat', title: 'Type Contrat' },
      {label: 'N° Carte Séjour', value:  'typeContrat', title: 'N° Carte Séjour' },
      
      {label: 'Etat Civile', value:  'etatCivile', title: 'Etat Civile' },
      {label: 'Etat', value:  'etat', title: 'Etat' }
  ];
  this.selectedColumnsDefinitions = [
    'matricule',
   'numeroCIN' ,
   'nom' ,
   'prenom',
   'dateNaissance'
    ];

  this.employeService.getPageEmployes(0, 5,null).subscribe((employes) => {
    this.listEmployes = employes;
    this.defaultColumnsDefinitions = [
      {name: 'matricule' , displayTitle: 'Matricule'},
      {name: 'numeroCIN' , displayTitle: 'N° CIN'},
      {name: 'numeroCNSS' , displayTitle: 'N° CNSS'},
      {name: 'numeroPassport' , displayTitle: 'N° Passport'},
      {name: 'numeroCarteSejour' , displayTitle: 'N° Carte Séjour'},
       {name: 'nom' , displayTitle: 'Nom'},
       {name: 'prenom' , displayTitle: 'Prénom'},
       {name: 'nomPere' , displayTitle: 'Père'},
       {name: 'nomMere' , displayTitle: 'Mère'},
      {name: 'dateNaissance' , displayTitle: 'Date Naissance'},
      {name: 'dateIntegration' , displayTitle: 'Date Intégration'},
      {name: 'numeroRue' , displayTitle: 'Rue'},
      {name: 'avenue' , displayTitle: 'Avenue'},
      {name: 'ville' , displayTitle: 'Ville'},
      {name: 'pays' , displayTitle: 'Pays'},
      {name: 'email' , displayTitle: 'Email'},
      {name: 'mobile' , displayTitle: 'Mobile'},
      {name: 'tel' , displayTitle: 'Tél'},
      {name: 'numeroCarteSejour' , displayTitle: 'N° Carte Séjour'},
      {name: 'nombreEnfants' , displayTitle: 'Nbre Enfants'},
      {name: 'etatCivile' , displayTitle: 'Etat Civile'},
      {name: 'typeContrat' , displayTitle: 'Type Contrat'},
      {name: 'nombreAnnesExperiences' , displayTitle: 'Nbre Années Expr.'},
      {name: 'competences' , displayTitle: 'Compétences'},
      {name: 'posteSecondaire' , displayTitle: 'Poste Secondaire'},
      {name: 'postePrincipale' , displayTitle: 'Poste Principale'},
       {name: 'grade' , displayTitle: 'Grade'},
       {name: 'niveauAcademique' , displayTitle: 'Niveau Académique'},
       {name: 'nombreEnfantsHandicapes' , displayTitle: 'Nbre Enfants Handicapés'},
       {name: 'etat', displayTitle: 'Etat' }
      ];
  this.specifyListColumnsToBeDisplayed();
  this.transformDataToByConsumedByMatTable();
});
   /* this.creditsService.getCredits().subscribe((credits) => {
      console.log('list credits ', credits);
      // this.listCredits = credits;
      this.listCredits =  this.listCredits.map((credit) => {
        if (credit.client === 'undefined' || credit.client === null ) {
           credit.client = new Client();
         }
         return credit;
   });      this.listCreditsOrgin = this.listCredits ;
     });*/
    // this.listClientsOrgin = this.listClients ;

  }
  deleteEmploye(employe) {
      console.log('call delete !', employe );
    this.employeService.deleteEmploye(employe).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData(null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.employeService.getPageEmployes(0, 5, filter).subscribe((employes) => {
    this.listEmployes = employes;
  this.specifyListColumnsToBeDisplayed();
  this.transformDataToByConsumedByMatTable();
});
  // this.listClientsOrgin = this.listClients ;
}


checkOneActionInvoked() {
  const listSelectedEmployes: Employe[] = [];
console.log(this.selection.selected);
if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((employeElement) => {
  return employeElement.code;
});
this.listEmployes.forEach(employe => {
  if (codes.findIndex(code => code === employe.code) > -1) {
    listSelectedEmployes.push(employe);
  }
  });
}
this.select.emit(listSelectedEmployes);
}
transformDataToByConsumedByMatTable() {

  const listElements = [];
  if (this.listEmployes !== undefined) {
    this.listEmployes.forEach(employe => {
      listElements.push( {
        code: employe.code ,
        matricule: employe.matricule ,
        numeroCNSS: employe.numeroCNSS ,
        numeroCIN: employe.numeroCIN ,
        numeroPassport: employe.numeroPassport ,
        numeroCarteSejour: employe.numeroCarteSejour ,
        nom: employe.nom ,
        prenom: employe.prenom ,
        nomPere: employe.nomPere,
        nomMere: employe.nomMere ,
        prenomMere: employe.prenomMere ,
        dateNaissance: employe.dateNaissance,
        dateIntegration: employe.dateIntegration,
        numeroRue: employe.numeroRue,
        avenue: employe.avenue,
        ville: employe.ville,
        email: employe.email,
        pays: employe.pays,
        mobile: employe.mobile,
        tel: employe.tel,
        nombreEnfants: employe.nombreEnfants,
        nombreEnfantsHandicapes: employe.nombreEnfantsHandicapes,
        niveauAcademique: employe.niveauAcademique,
        grade: employe.grade,
        postePrincipale: employe.postePrincipale,
        posteSecondaire: employe.posteSecondaire,
        competences: employe.competences,
        nombreAnnesExperiences: employe.nombreAnnesExperiences,
        typeContrat:  employe.typeContrat,
        etatCivile:  employe.etatCivile,
      } );
    });
  }
    this.dataEmployesSource = new MatTableDataSource<EmployeElement>(listElements);
    this.dataEmployesSource.sort = this.sort;
    this.dataEmployesSource.paginator = this.paginator;
    this.employeService.getTotalEmployes(this.employeDataFilter).subscribe((response) => {
      //  console.log('Total credits is ', response);
        this.dataEmployesSource.paginator.length = response.totalEmployes;
      });
}
specifyListColumnsToBeDisplayed() {
  
   console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
   this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
 ?
    this.defaultColumnsDefinitions : [];
    this.selectedColumnsNames = [];
    
    if (this.selectedColumnsDefinitions !== undefined)
     {
       this.selectedColumnsDefinitions.forEach((colName) => { 
            const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
            const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
            const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
            this.columnsDefinitionsToDisplay.push(colDef);
           });
     }
    this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
   this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
              return colDef.displayTitle;
    });
    console.log('columns to disp:', this.columnsDefinitionsToDisplay);
    this.columnsNames = [];
    this.columnsNames[0] = 'select';
    this.columnsDefinitionsToDisplay .map((colDef) => {
     this.columnsNames.push(colDef.name);
    if (this.selectedColumnsDefinitions !== undefined) {
     this.selectedColumnsNames.push(colDef.displayTitle);
    }
 });
    this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
     return colDef.displayTitleAr;
 });
 }

masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataEmployesSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataEmployesSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataEmployesSource.filter = filterValue.trim().toLowerCase();
  if (this.dataEmployesSource.paginator) {
    this.dataEmployesSource.paginator.firstPage();
  }
}
loadAddEmployeComponent() {
  this.router.navigate(['/pms/rh/ajouterEmploye']) ;

}
loadEditEmployeComponent() {
  if (this.selection.selected.length === 0) {
    this.openSnackBar(' Aucun élément sélectionné' );
    // this.loadData();
    } else
   {
  this.router.navigate(['/pms/rh/editerEmploye', this.selection.selected[ this.selection.selected.length - 1].code]) ;
    }
}
supprimerEmployes()
{
  console.log('NBRE OF ELEMENTS SELECTED');
  console.log(this.selection.selected.length);
  console.log(' ELEMENTS SELECTED');
  console.log(this.selection.selected);
  if ( this.selection.selected.length !== 0) {
      this.selection.selected.forEach(function(employe, index) {
          this.employesService.deleteEmploye(employe.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.openSnackBar( ' Element(s) Supprimé(s)');
             this.loadData(null);
            }
      }, this);
    } else {
      this.openSnackBar('Aucun Employé Séléctionnée');
    }
}
refresh()
{
  this.loadData(null);

}
  openSnackBar(messageToDisplay) {
 this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
     verticalPosition: 'top'});
}

changePage($event) {
  console.log('page event is ', $event);
 this.employeService.getPageEmployes($event.pageIndex, $event.pageSize,
  this.employeDataFilter ).subscribe((employes) => {
    this.listEmployes = employes;
   // this.transformDataToByConsumedByMatTable();
   const listElements = [];
   this.listEmployes.forEach(employe => {
    listElements.push( {
      code: employe.code ,
      matricule: employe.matricule ,
      numeroCNSS: employe.numeroCNSS ,
      numeroCIN: employe.numeroCIN ,
      numeroPassport: employe.numeroPassport ,
      numeroCarteSejour: employe.numeroCarteSejour ,
      nom: employe.nom ,
      prenom: employe.prenom ,
      nomPere: employe.nomPere,
      nomMere: employe.nomMere ,
      prenomMere: employe.prenomMere ,
      dateNaissance: employe.dateNaissance,
      dateIntegration: employe.dateIntegration,
      numeroRue: employe.numeroRue,
      avenue: employe.avenue,
      ville: employe.ville,
      email: employe.email,
      pays: employe.pays,
      mobile: employe.mobile,
      tel: employe.tel,
      nombreEnfants: employe.nombreEnfants,
      nombreEnfantsHandicapes: employe.nombreEnfantsHandicapes,
      niveauAcademique: employe.niveauAcademique,
      grade: employe.grade,
      postePrincipale: employe.postePrincipale,
      posteSecondaire: employe.posteSecondaire,
      competences: employe.competences,
      nombreAnnesExperiences: employe.nombreAnnesExperiences,
      typeContrat:  employe.typeContrat,
      etatCivile:  employe.etatCivile,
    } );
  });
 this.dataEmployesSource = new MatTableDataSource<EmployeElement>(listElements);
 this.employeService.getTotalEmployes(this.employeDataFilter).subscribe((response) => {
   this.dataEmployesSource.paginator.length = response.totalEmployes;
});

});
}
toggleFilterPanel()
{
   this.showFilter = !this.showFilter;
}
toggleShowColumnsMultiSelect()
{
  this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
}
loadFiltredData()
{
  console.log('the filter is ', this.employeDataFilter);
  this.loadData(this.employeDataFilter);
}
}
