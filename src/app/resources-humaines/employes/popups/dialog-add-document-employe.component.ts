
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {EmployeService} from '../employe.service';
import {DocumentEmploye} from '../document-employe.model';

@Component({
    selector: 'app-dialog-add-document-employe',
    templateUrl: 'dialog-add-document-employe.component.html',
  })
  export class DialogAddDocumentEmployeComponent implements OnInit {
     private documentEmploye: DocumentEmploye;
     private saveEnCours = false;
     private typesDoc: any[] = [
      {value: 'FichePaie', viewValue: 'Fiche De Paie'},
      {value: 'HistoriqueCNSS', viewValue: 'Historique CNSS'},
      {value: 'RIB', viewValue: 'RIB'},
      {value: 'CV', viewValue: 'CV'},
      {value: 'LettreMotivation', viewValue: 'Lettre de Motivation'},
      {value: 'AttestationTravail', viewValue: 'Attestation de Travail'},
      {value: 'LettreRecommandation', viewValue: 'Lettre de Recommandation'},
      {value: 'ContratTravail', viewValue: 'Contrat de Travail'},
      {value: 'Autre', viewValue: 'Autre'},
    ];

     constructor(  private employeService: EmployeService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.documentEmploye = new DocumentEmploye();
    }
    saveDocumentEmploye() 
    {
      this.documentEmploye.codeEmploye = this.data.codeEmploye;
       console.log(this.documentEmploye);
        this.saveEnCours = true;
       this.employeService.addDocumentEmploye(this.documentEmploye).subscribe((response) => {
         this.saveEnCours = false;
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.documentEmploye = new DocumentEmploye();
             this.openSnackBar(  'Document Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
     imageFace1Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentEmploye.imageFace1 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imageFace2Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentEmploye.imageFace2 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
  }
