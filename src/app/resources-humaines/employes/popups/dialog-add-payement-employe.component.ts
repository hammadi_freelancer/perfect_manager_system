
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {EmployeService} from '../employe.service';
import {PayementEmploye} from '../payement-employe.model';

@Component({
    selector: 'app-dialog-add-payement-employe',
    templateUrl: 'dialog-add-payement-employe.component.html',
  })

  export class DialogAddPayementEmployeComponent implements OnInit {
     private payementEmploye: PayementEmploye;
     private saveEnCours = false;
     private motifs: any[] = [
      {value: 'PAIEMENT_SALAIRE', viewValue: 'Paiement Salaire'},
      {value: 'PAIEMENT_PRIMES', viewValue: 'Paiement Primes'},
      {value: 'VIREMENT_CNSS', viewValue: 'Virement CNSS'},
      {value: 'PAIEMENT_FRAIS_MISSION', viewValue: 'Paiement Frais de Mission'},
      {value: 'AVANCE', viewValue: 'Avance'},
      {value: 'Autre', viewValue: 'Autre'},
    ];
    private etats: any[] = [
        {value: 'Initial', viewValue: 'Initial'},
        {value: 'Valide', viewValue: 'Valide'},
        {value: 'Annule', viewValue: 'Annule'},
      ];

     constructor(  private employeService: EmployeService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.payementEmploye = new PayementEmploye();
    }
    savePayementEmploye() 
    {
        this.payementEmploye.codeEmploye = this.data.codeEmploye;
        //  console.log(this.documentEmploye);
        this.saveEnCours = true;
       this.employeService.addPayementEmploye(this.payementEmploye).subscribe((response) => {
         this.saveEnCours = false;
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.payementEmploye = new PayementEmploye();
             this.openSnackBar(  'Paiement Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
     imageFace1Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.payementEmploye.imageFace1 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imageFace2Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.payementEmploye.imageFace2 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
  }
