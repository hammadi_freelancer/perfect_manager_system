
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {EmployeService} from '../employe.service';
import {DocumentEmploye} from '../document-employe.model';

@Component({
    selector: 'app-dialog-edit-document-employe',
    templateUrl: 'dialog-edit-document-employe.component.html',
  })
  export class DialogEditDocumentEmployeComponent implements OnInit {
     private documentEmploye: DocumentEmploye;
     private updateEnCours = false;
     private typesDoc: any[] = [
      {value: 'FichePaie', viewValue: 'Fiche De Paie'},
      {value: 'HistoriqueCNSS', viewValue: 'Historique CNSS'},
      {value: 'RIB', viewValue: 'RIB'},
      {value: 'CV', viewValue: 'CV'},
      {value: 'LettreMotivation', viewValue: 'Lettre de Motivation'},
      {value: 'AttestationTravail', viewValue: 'Attestation de Travail'},
      {value: 'LettreRecommandation', viewValue: 'Lettre de Recommandation'},
      {value: 'ContratTravail', viewValue: 'Contrat de Travail'},
      {value: 'Autre', viewValue: 'Autre'},
    ];
     constructor(  private employeService: EmployeService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {

        this.employeService.getDocumentEmploye(this.data.codeDocumentEmploye).subscribe((doc) => {
            this.documentEmploye = doc;
     });
    }
    
    updateDocumentEmploye() 
    {
      this.updateEnCours = true;
       this.employeService.updateDocumentEmploye(this.documentEmploye).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.openSnackBar(  'Document Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
     imageFace1Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentEmploye.imageFace1 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imageFace2Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentEmploye.imageFace2 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
  }
