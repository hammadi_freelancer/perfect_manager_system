
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {EmployeService} from '../employe.service';
import {PayementEmploye} from '../payement-employe.model';

@Component({
    selector: 'app-dialog-edit-payement-employe',
    templateUrl: 'dialog-edit-payement-employe.component.html',
  })

  export class DialogEditPayementEmployeComponent implements OnInit {
     private payementEmploye: PayementEmploye;
     private updateEnCours = false;
     private motifs: any[] = [
      {value: 'PAIEMENT_SALAIRE', viewValue: 'Paiement Salaire'},
      {value: 'PAIEMENT_PRIMES', viewValue: 'Paiement Primes'},
      {value: 'VIREMENT_CNSS', viewValue: 'Virement CNSS'},
      {value: 'PAIEMENT_FRAIS_MISSION', viewValue: 'Paiement Frais de Mission'},
      {value: 'AVANCE', viewValue: 'Avance'},
      {value: 'Autre', viewValue: 'Autre'},
    ];
    private etats: any[] = [
        {value: 'Initial', viewValue: 'Initial'},
        {value: 'Valide', viewValue: 'Valide'},
        {value: 'Annule', viewValue: 'Annule'},
      ];

     constructor(  private employeService: EmployeService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit() {
        this.employeService.getPayementEmploye(this.data.codePayementEmploye).subscribe((pay) => {
            this.payementEmploye = pay;
     });
    }
    updatePayementEmploye() 
    {
        //  console.log(this.documentEmploye);
        this.updateEnCours = true;
       this.employeService.updatePayementEmploye(this.payementEmploye).subscribe((response) => {
         this.updateEnCours = false;
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             // this.payementEmploye = new PayementEmploye();
             this.openSnackBar(  'Paiement Mis à jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
     imageFace1Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.payementEmploye.imageFace1 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imageFace2Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.payementEmploye.imageFace2 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
  }
