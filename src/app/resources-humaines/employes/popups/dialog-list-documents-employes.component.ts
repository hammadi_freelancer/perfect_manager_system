
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {EmployeService} from '../employe.service';
import {DocumentEmploye} from '../document-employe.model';
import { DocumentEmployeElement } from '../document-employe.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-documents-employes',
    templateUrl: 'dialog-list-documents-employes.component.html',
  })
  export class DialogListDocumentsEmployesComponent implements OnInit {
     private listDocumentsEmployes: DocumentEmploye[] = [];
     private  dataDocumentsEmployesSource: MatTableDataSource<DocumentEmployeElement>;
     private selection = new SelectionModel<DocumentEmployeElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private employeService: EmployeService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
     //  console.log('code credit in dialog component is :', this.data.codeClient);
      this.employeService.getDocumentsEmployesByCodeEmploye(this.data.codeEmploye)
      .subscribe((listDocumentsEmployes) => {
        this.listDocumentsEmployes = listDocumentsEmployes;
// console.log('recieving data from backend', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedDocumentsEmployes: DocumentEmploye[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentEElement) => {
    return documentEElement.code;
  });
  this.listDocumentsEmployes.forEach(documentEn => {
    if (codes.findIndex(code => code === documentEn.code) > -1) {
      listSelectedDocumentsEmployes.push(documentEn);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsEmployes!== undefined) {

      this.listDocumentsEmployes.forEach(docEn => {
             listElements.push( {code: docEn.code ,
          dateReception: docEn.dateReception,
          numeroDocumentEmploye: docEn.numeroDocumentEmploye,
          typeDocument: docEn.typeDocument,
          description: docEn.description
        } );
      });
    }
      this.dataDocumentsEmployesSource = new MatTableDataSource<DocumentEmployeElement>(listElements);
      this.dataDocumentsEmployesSource.sort = this.sort;
      this.dataDocumentsEmployesSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocumentEmploye' , displayTitle: 'N°Doc'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsEmployesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsEmployesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsEmployesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsEmployesSource.paginator) {
      this.dataDocumentsEmployesSource.paginator.firstPage();
    }
  }

}
