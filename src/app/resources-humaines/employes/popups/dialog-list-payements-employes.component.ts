
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {EmployeService} from '../employe.service';
import {PayementEmploye} from '../payement-employe.model';
import { PayementEmployeElement } from '../payement-employe.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-payements-employes',
    templateUrl: 'dialog-list-payements-employes.component.html',
  })
  export class DialogListPayementsEmployesComponent implements OnInit {
     private listPayementsEmployes: PayementEmploye[] = [];
     private  dataPayementsEmployesSource: MatTableDataSource<PayementEmployeElement>;
     private selection = new SelectionModel<PayementEmployeElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private employeService: EmployeService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
     //  console.log('code credit in dialog component is :', this.data.codeClient);
      this.employeService.getPayementsEmployesByCodeEmploye(this.data.codeEmploye)
      .subscribe((listPayementsEmployes) => {
        this.listPayementsEmployes = listPayementsEmployes;
// console.log('recieving data from backend', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedPayementsEmployes: PayementEmploye[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((payementEElement) => {
    return payementEElement.code;
  });
  this.listPayementsEmployes.forEach(payEmp => {
    if (codes.findIndex(code => code === payEmp.code) > -1) {
        listSelectedPayementsEmployes.push(payEmp);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listPayementsEmployes!== undefined) {

      this.listPayementsEmployes.forEach(payEmp => {
             listElements.push( {code: payEmp.code ,
          datePayement: payEmp.datePaiement,
          numeroPayement: payEmp.numeroPaiement,
          montant: payEmp.montant,
          motif: payEmp.motif,
          etat: payEmp.etat,
          description: payEmp.description
        } );
      });
    }
      this.dataPayementsEmployesSource = new MatTableDataSource<PayementEmployeElement>(listElements);
      this.dataPayementsEmployesSource.sort = this.sort;
      this.dataPayementsEmployesSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [
     {name: 'numeroPayement' , displayTitle: 'N°Paiement'}, 
     {name: 'datePayement' , displayTitle: 'Date Paiement'},
     {name: 'montant' , displayTitle: 'Montant'},
     {name: 'motif' , displayTitle: 'Motif'},
     {name: 'etat' , displayTitle: 'Etat'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataPayementsEmployesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataPayementsEmployesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataPayementsEmployesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataPayementsEmployesSource.paginator) {
      this.dataPayementsEmployesSource.paginator.firstPage();
    }
  }

}
