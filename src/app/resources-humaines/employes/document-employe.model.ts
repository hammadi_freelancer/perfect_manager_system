
import { BaseEntity } from '../../common/base-entity.model' ;

type TypeDocument =   'HistoriqueCNSS' |  'FichePaie' |
'RIB' | 'CV' | 'LettreMotivation' |  'AttestationTravail' | 'LettreRecommandation' 
| 'ContratTravail' | 'Autre';

export class DocumentEmploye extends BaseEntity {
    constructor(
        public code?: string,
         public codeEmploye?: string,
         public numeroDocumentEmploye?: string,
        public dateReceptionObject?: Date,
        public dateReception?: string,
        public typeDocument?: TypeDocument,
        public fileExtension?: string,
        public imageFace1?: any,
        public imageFace2?: any,
     ) {
         super();
        
       }
  
}
