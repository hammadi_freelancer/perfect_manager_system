
import { BaseEntity } from '../../common/base-entity.model' ;
// import { Tranche } from './tranche.model' ;
import { Employe } from '../employes/employe.model' ;
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
// import { Service } from '../../stocks/articles/article.service';


export class EmployeFilter  {
    public selectedCin: string;
    public selectedMatricule: string;
    
    public selectedNom: string;
    public selectedPrenom: string;

    private controlNoms = new FormControl();
    private controlPrenoms = new FormControl();
    private controlCins = new FormControl();

    private listNoms: string[] = [];
    private listPrenoms: string[] = [];
    private listCins: string[] = [];

    private filteredNoms: Observable<string[]>;
    private filteredPrenoms: Observable<string[]>;
    private filteredCins: Observable<string[]>;

    
    constructor (employes: Employe[]) {
      if (employes !== null) {
       //  const listClientsMorals = clients.filter((client) => (client.categoryClient === 'PERSONNE_MORALE'));
        // const listClientsPhysiques = clients.filter((client) => (client.categoryClient === 'PERSONNE_PHYSIQUE'));
        this.listCins = employes.filter((employe) =>
        (employe.numeroCIN !== undefined && employe.numeroCIN != null )).map((employe) => (employe.numeroCIN ));
        this.listNoms = employes.filter((employe) =>
        (employe.nom !== undefined && employe.nom != null )).map((employe) => (employe.nom ));

        // this.listNoms = clients.map((client) => (client.nom));
        this.listPrenoms = employes.filter((employe) =>
        (employe.prenom !== undefined && employe.prenom != null )).map((client) => (client.prenom ));
 

    

      this.filteredNoms = this.controlNoms.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterNoms(value))
        );
        this.filteredPrenoms = this.controlPrenoms.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterPrenoms(value))
        );
        this.filteredCins = this.controlCins.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterCins(value))
        );
    }

  }
    private _filterNoms(value: string): string[] {
      let  filterValue = '';
      if (value !== undefined) {
        filterValue = value.toLowerCase();
      }
        return this.listNoms.filter(nom => nom.toLowerCase().includes(filterValue));
      }
      private _filterCins(value: string): string[] {
        let  filterValue = '';
        if (value !== undefined) {
          filterValue = value.toLowerCase();
        }
        return this.listCins.filter(cin => cin.toLowerCase().includes(filterValue));
      }
      private _filterPrenoms(value: string): string[] {
        let  filterValue = '';
        if (value !== undefined) {
          filterValue = value.toLowerCase();
        }
        return this.listPrenoms.filter(prenom => prenom.toLowerCase().includes(filterValue));
      }


      

      public getControlPrenoms(): FormControl {
        return this.controlPrenoms;
      }
      public getControlNoms(): FormControl {
        return this.controlNoms;
      }
      public getControlCins(): FormControl {
        return this.controlCins;
      }
      public getFiltredPrenoms(): Observable<string[]> {
        return this.filteredPrenoms;
      }
      public getFiltredNoms(): Observable<string[]> {
        return this.filteredNoms;
      }
      public getFiltredCins(): Observable<string[]> {
        return this.filteredCins;
      }
    
}





