import { Component, OnInit, Input, EventEmitter, Output, AfterContentChecked,
AfterViewChecked
} from '@angular/core';
// import { ArticleService } from './article.service';
import {Employe} from './employe.model';
import {EmployeService} from './employe.service';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {MatSnackBar} from '@angular/material';
import { MotifView } from './motif-view.interface';
@Component({
  selector: 'app-employe-edit',
  templateUrl: './employe-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class EmployeEditComponent implements OnInit, AfterContentChecked {

// private client: Client = new Client();


private employe: Employe = new Employe();

private tabsDisabled = true;
private etatsCiviles: any[] = [
  {value: 'Marie', viewValue: 'Marié'},
  {value: 'Divorce', viewValue: 'Divorcé'},
  {value: 'Celibataire', viewValue: 'Célibataire'},
];
private etatsEmployes: any[] = [
  {value: 'Annule', viewValue: 'Annulé'},
  {value: 'Initial', viewValue: 'Initial'},
  {value: 'Valide', viewValue: 'Validé'}
];

  constructor( private route: ActivatedRoute,
    private router: Router , private employeService: EmployeService, private matSnackBar: MatSnackBar,
    
   ) {
  }
  ngOnInit() {
      const codeEmploye = this.route.snapshot.paramMap.get('codeEmploye');
      this.employeService.getEmploye(codeEmploye).subscribe((employe) => {
               this.employe = employe;
               if (this.employe.etat === 'Valide') {
                this.tabsDisabled = false;
               }
               if (this.employe.etat === 'Valide' || this.employe.etat === 'Annule')
                {
                  this.etatsEmployes.splice(1, 1);
                }
                if (this.employe.etat === 'Initial')
                  {
                    this.etatsEmployes.splice(0, 1);
                  }
      });
     
  }
 /* loadAddEComponent() {
    this.router.navigate(['/pms/caisse/ajouterEntree']) ;
  
  }*/
  loadGridEmployes() {
    this.router.navigate(['/pms/rh/employes']) ;
  }
  supprimerEmploye() {
     
      this.employeService.deleteEmploye(this.employe.code).subscribe((response) => {

        this.openSnackBar( ' Employé Supprimé');
        this.loadGridEmployes() ;
            });
  }
updateEmploye() {

  console.log(this.employe);
  this.employeService.updateEmploye(this.employe).subscribe((response) => {
    if (response.error_message ===  undefined) {
      this.openSnackBar( ' Employé mis à jour ');
      if (this.employe.etat === 'Valide')
        {
          this.tabsDisabled = false;
          this.etatsEmployes = [
            {value: 'Annule', viewValue: 'Annulé'},
            {value: 'Valide', viewValue: 'Validé'}
  
          ];
        }
        if (this.employe.etat === 'Annule')
          {
            this.tabsDisabled = true;
         
          }
      
      //  this.save.emit(response);
      // this.client = new Client();
      // this.modeUpdate = false;
    } else {
      this.openSnackBar( ' Erreurs.. ');
      
     // show error message
    }
});
}

 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

ngAfterContentChecked() {

}
selectedTabChanged($event) {
  if ($event.index === 4) { // Composition tab was selected
   // this.loadListArticles();
   } else if ($event.index === 1) {
    //  this.loadListSchemasConfiguration() ;
   }
  }
}


