
export interface PayementEmployeElement {
    code: string;
    numeroPaiement: string;
     datePaiement: string;
     montant: string;
     motif: string;
     description: string;
     etat: string;

   }
