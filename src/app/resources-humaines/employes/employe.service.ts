import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';
import { HttpParams } from '@angular/common/http';

import {Employe} from './employe.model';
import { DocumentEmploye } from './document-employe.model';
import { PayementEmploye } from './payement-employe.model';

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_EMPLOYES = 'http://localhost:8082/rh/employes/getEmployes';
const  URL_GET_PAGE_EMPLOYES = 'http://localhost:8082/rh/employes/getPageEmployes';

const URL_ADD_EMPLOYE = 'http://localhost:8082/rh/employes/addEmploye';
const URL_GET_EMPLOYE = 'http://localhost:8082/rh/employes/getEmploye';

const URL_UPDATE_EMPLOYE = 'http://localhost:8082/rh/employes/updateEmploye';
const URL_DELETE_EMPLOYE = 'http://localhost:8082/rh/employes/deleteEmploye';
const  URL_GET_TOTAL_EMPLOYES = 'http://localhost:8082/rh/employes/getTotalEmployes';



const  URL_ADD_DOCUMENT_EMPLOYE = 'http://localhost:8082/rh/documentsEmployes/addDocumentEmploye';
const  URL_GET_LIST_DOCUMENT_EMPLOYE = 'http://localhost:8082/rh/documentsEmployes/getDocumentsEmployes';
const  URL_DELETE_DOCUCMENT_EMPLOYE = 'http://localhost:8082/rh/documentsEmployes/deleteDocumentEmploye';
const  URL_UPDATE_DOCUMENT_EMPLOYE = 'http://localhost:8082/rh/documentsEmployes/updateDocumentEmploye';
const URL_GET_DOCUMENT_EMPLOYE = 'http://localhost:8082/rh/documentsEmployes/getDocumentEmploye';
const URL_GET_LIST_DOCUMENT_EMPLOYE_BY_CODE_EMPLOYE =
 'http://localhost:8082/rh/documentsEmployes/getDocumentsEmployesByCodeEmploye';


 const  URL_ADD_PAYEMENT_EMPLOYE = 'http://localhost:8082/rh/paiementsEmployes/addPaiementEmploye';
 const  URL_GET_LIST_PAYEMENT_EMPLOYE = 'http://localhost:8082/rh/paiementsEmployes/getPaiementsEmployes';
 const  URL_DELETE_PAYEMENT_EMPLOYE = 'http://localhost:8082/rh/paiementsEmployes/deletePaiementEmploye';
 const  URL_UPDATE_PAYEMENT_EMPLOYE = 'http://localhost:8082/rh/paiementsEmployes/updatePaiementEmploye';
 const URL_GET_PAYEMENT_EMPLOYE = 'http://localhost:8082/rh/paiementsEmployes/getPaiementEmploye';
 const URL_GET_LIST_PAYEMENT_EMPLOYE_BY_CODE_EMPLOYE =
  'http://localhost:8082/rh/paiementsEmployes/getPaiementsEmployesByCodeEmploye';


@Injectable({
  providedIn: 'root'
})
export class EmployeService {



  constructor(private httpClient: HttpClient) {

   }
   getTotalEmployes(filter): Observable<any> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('filter', filterStr);
        return this.httpClient
        .get<any>(URL_GET_TOTAL_EMPLOYES, {params});
   }

   getEmployes(): Observable<Employe[]> {
    const params = new HttpParams();
    return this.httpClient
    .get<Employe[]>(URL_GET_LIST_EMPLOYES, {params});
   }
 

   getPageEmployes(pageNumber, nbElementsPerPage, filter): Observable<Employe[]> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('pageNumber', pageNumber)
    .set('nbElementsPerPage', nbElementsPerPage)
    .set('filter', filterStr);
    return this.httpClient
    .get<Employe[]>(URL_GET_PAGE_EMPLOYES, {params});
   }

   addEmploye(employe: Employe): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_EMPLOYE, employe);
  }
  updateEmploye(employe: Employe): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_EMPLOYE, employe);
  }
  deleteEmploye(codeEmploye): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_EMPLOYE+ '/' + codeEmploye);
  }
  getEmploye(codeEmploye): Observable<Employe> {
    return this.httpClient.get<Employe>(URL_GET_EMPLOYE + '/' + codeEmploye);
  }



  
  addDocumentEmploye(documentEmploye: DocumentEmploye): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_DOCUMENT_EMPLOYE, documentEmploye);
    
  }

  getDocumentsEmployes(): Observable<DocumentEmploye[]> {
    return this.httpClient
    .get<DocumentEmploye[]>(URL_GET_LIST_DOCUMENT_EMPLOYE);
   }
   getDocumentEmploye(codeDocumentEmploye): Observable<DocumentEmploye> {
    return this.httpClient.get<DocumentEmploye>(URL_GET_DOCUMENT_EMPLOYE + '/' + codeDocumentEmploye);
  }
  deleteDocumentEmploye(codeDocumentEmploye): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_DOCUCMENT_EMPLOYE + '/' + codeDocumentEmploye);
  }
  getDocumentsEmployesByCodeEmploye(codeEmploye): Observable<DocumentEmploye[]> {
    return this.httpClient.get<DocumentEmploye[]>(URL_GET_LIST_DOCUMENT_EMPLOYE_BY_CODE_EMPLOYE + '/' + codeEmploye);
  }
  updateDocumentEmploye(doc: DocumentEmploye): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_DOCUMENT_EMPLOYE, doc);
  }


  addPayementEmploye(payementEmploye: PayementEmploye): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_PAYEMENT_EMPLOYE, payementEmploye);
    
  }

  getPayementsEmployes(): Observable<PayementEmploye[]> {
    return this.httpClient
    .get<PayementEmploye[]>(URL_GET_LIST_PAYEMENT_EMPLOYE);
   }
   getPayementEmploye(codePayementEmploye): Observable<PayementEmploye> {
    return this.httpClient.get<PayementEmploye>(URL_GET_PAYEMENT_EMPLOYE + '/' + codePayementEmploye);
  }
  deletePayementEmploye(codePayementEmploye): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_PAYEMENT_EMPLOYE + '/' + codePayementEmploye);
  }
  getPayementsEmployesByCodeEmploye(codeEmploye): Observable<PayementEmploye[]> {
    return this.httpClient.get<PayementEmploye[]>(URL_GET_LIST_PAYEMENT_EMPLOYE_BY_CODE_EMPLOYE + '/' + codeEmploye);
  }
  updatePayementEmploye(pay: PayementEmploye): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_PAYEMENT_EMPLOYE, pay);
  }



}
