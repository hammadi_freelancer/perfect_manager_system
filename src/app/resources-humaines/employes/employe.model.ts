

import { BaseEntity } from '../../common/base-entity.model' ;
type Etat = 'Initial' | 'Valide' | 'Annule' | '';
type Etat_CIVILE = 'Celibataire' | 'Marie' | 'Divorce' | '';
type TYPE_CONTRAT= 'CDI' | 'CDD' | 'SIVP' | 'KARAMA' |'Autre' | '';

/*type Motif = 'Ventes'
 | 'PayementsCreditsClients' |
 'VirementCheque' |
  'Emprunts' ;*/

export class Employe extends BaseEntity {
   constructor(public code?: string,
    public matricule?: string,
    public numeroCNSS?: string,
    public numeroCIN?: string,
    public numeroPassport?: string,
    public numeroCarteSejour?: string,
    public nom?: string,
    public prenom?: string,
    public nomMere?: string,
    public prenomMere?: string,
    public nomPere?: string,
    public dateNaissanceObject?: Date,
    public dateNaissance?: string,
    public dateIntegrationObject?: Date,
    public dateIntegration?: string,
    public numeroRue?: number,
    public avenue?: string,
    public ville?: string,
    public gouvernorat?: string,
    public pays?: string,
    public email?: string,
    public mobile?: string,
    public tel?: string,
    public nombreEnfants?: number,
    public nombreEnfantsHandicapes?: number,
    public niveauAcademique?: string,
    public grade?: string,
    public postePrincipale?: string,
    public posteSecondaire?: string,
    public competences?: string,
    public nombreAnnesExperiences?: number,
    public typeContrat?: TYPE_CONTRAT,
    public etatCivile?: Etat_CIVILE,
    
  //   public motif?: Motif, public dateOperation?: string,
    //public montant?: number,
    public etat?: Etat,
) {
    super();
}
}
