


export interface DocumentEmployeElement {
         code: string ;
         dateReception: string ;
         numeroDocumentEmploye: number;
         typeDocument?: string;
          description?: string;
}
