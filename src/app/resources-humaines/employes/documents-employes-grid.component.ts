
import { Component, OnInit, Inject,Input, ViewChild, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {EmployeService} from './employe.service';
import {Employe} from './employe.model';

import {DocumentEmploye} from './document-employe.model';
import { DocumentEmployeElement } from './document-employe.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogAddDocumentEmployeComponent } from './popups/dialog-add-document-employe.component';
import { DialogEditDocumentEmployeComponent } from './popups/dialog-edit-document-employe.component';
@Component({
    selector: 'app-documents-employes-grid',
    templateUrl: 'documents-employes-grid.component.html',
  })
  export class DocumentsEmployesGridComponent implements OnInit, OnChanges {
     private listDocumentsEmployes: DocumentEmploye[] = [];
     private  dataDocumentsEmployesSource: MatTableDataSource<DocumentEmployeElement>;
     private selection = new SelectionModel<DocumentEmployeElement>(true, []);
    
     @Input()
     private employe : Employe;

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private employeService: EmployeService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.employeService.getDocumentsEmployesByCodeEmploye(this.employe.code).subscribe((listDocumentsEmployes) => {
        this.listDocumentsEmployes= listDocumentsEmployes;
  // console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
  }
  ngOnChanges()
  {
    this.specifyListColumnsToBeDisplayed();
    
    this.employeService.getDocumentsEmployesByCodeEmploye(this.employe.code).subscribe((listDocumentsEmployes) => {
      this.listDocumentsEmployes = listDocumentsEmployes;
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }

  checkOneActionInvoked() {
    const listSelectedDocumentsEmployes: DocumentEmploye[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentEmpElement) => {
    return documentEmpElement.code;
  });
  this.listDocumentsEmployes.forEach(documentE => {
    if (codes.findIndex(code => code === documentE.code) > -1) {
      listSelectedDocumentsEmployes.push(documentE);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsEmployes!== undefined) {

      this.listDocumentsEmployes.forEach(docE => {
             listElements.push( {code: docE.code ,
          dateReception: docE.dateReception,
          numeroDocumentEmploye: docE.numeroDocumentEmploye,
          typeDocument: docE.typeDocument,
          description: docE.description
        } );
      });
    }
      this.dataDocumentsEmployesSource= new MatTableDataSource<DocumentEmployeElement>(listElements);
      this.dataDocumentsEmployesSource.sort = this.sort;
      this.dataDocumentsEmployesSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    //  console.log('calling specifyListColumnsToBeDisplayed documents grid');
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
    
     this.columnsDefinitions = [
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocumentEmploye' , displayTitle: 'N°DOC'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
      console.log(this.columnsNames);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsEmployesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsEmployesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsEmployesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsEmployesSource.paginator) {
      this.dataDocumentsEmployesSource.paginator.firstPage();
    }
  }
  showAddDocumentDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeEmploye : this.employe.code
   };
 
   const dialogRef = this.dialog.open(DialogAddDocumentEmployeComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(docAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditDocumentDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeDocumentEmploye : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditDocumentEmployeComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(docAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Document Séléctionnée!');
    }
    }
    supprimerDocuments()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(doc, index) {
              this.employeService.deleteDocumentEmploye(doc.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar(  ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Document Séléctionnée!');
        }
    }
    refresh() {
      this.employeService.getDocumentsEmployesByCodeEmploye(this.employe.code).subscribe((listDocumentsEmployes) => {
        this.listDocumentsEmployes = listDocumentsEmployes;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
