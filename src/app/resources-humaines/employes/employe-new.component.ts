import { Component, OnInit, Input, EventEmitter, Output,
    AfterViewChecked
    } from '@angular/core';
    // import { ArticleService } from './article.service';
    import {Employe} from './employe.model';
    import {EmployeService} from './employe.service';
    import {FormControl} from '@angular/forms';
    import {map, startWith} from 'rxjs/operators';
    import {Observable} from 'rxjs';
    import { Router, ActivatedRoute, ParamMap } from '@angular/router';
    import {MatSnackBar} from '@angular/material';
    import { MotifView } from './motif-view.interface';
    
    @Component({
      selector: 'app-employe-new',
      templateUrl: './employe-new.component.html',
      // styleUrls: ['./players.component.css']
    })
export class EmployeNewComponent implements OnInit {
    private employe: Employe = new Employe();
    @Input()
    private listEmployes: any = [] ;
 
    private file: any;

    private etatsCiviles: any[] = [
      {value: 'Marie', viewValue: 'Marié'},
      {value: 'Divorce', viewValue: 'Divorcé'},
      {value: 'Celibataire', viewValue: 'Célibataire'},
    ];
    private typesContrat: any[] = [
      {value: 'CDI', viewValue: 'CDI'},
      {value: 'CDD', viewValue: 'CDD'},
      {value: 'SIVP', viewValue: 'SIVP'},
      {value: 'Karama', viewValue: 'Karama'},
    ];
      constructor( private route: ActivatedRoute,
        private router: Router , private employeService: EmployeService, private matSnackBar: MatSnackBar ) {
      }
      ngOnInit() {
     
      }
    saveEmploye() {
       
        // console.log(this.entree);
        this.employeService.addEmploye(this.employe).subscribe((response) => {
            if (response.error_message ===  undefined) {
              // this.save.emit(response);
             // this.entree = new Entree();
              this.openSnackBar(  'Employe Enregistré');
            } else {
             // show error message
             this.openSnackBar(  'Erreurs!');
             
            }
        });
    }
    loadGridEmployes() {
      this.router.navigate(['/pms/rh/employes']) ;
    }
    fileChanged($event) {
        this.file = $event.target.files[0];
        const fileReader = new FileReader();
        fileReader.onload = (e) => {
          console.log(fileReader.result);
          // this.article.image = fileReader.result;
          // this.filesData[indexFile] = fileData;
        };
        fileReader.readAsDataURL(this.file);
       }
       vider() {
         this.employe = new Employe();
       }
       openSnackBar(messageToDisplay) {
        this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
            verticalPosition: 'top'});
       }
 
    }
