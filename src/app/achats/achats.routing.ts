import { Injectable } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { FournisseursGridComponent } from './fournisseurs/fournisseurs-grid.component';
import { FournisseurNewComponent } from './fournisseurs/fournisseur-new.component';
import { FournisseurEditComponent } from './fournisseurs/fournisseur-edit.component';

import {ModuleWithProviders  } from '@angular/core';

import {APP_BASE_HREF} from '@angular/common';
import { CreditAchatsNewComponent } from './credits-achats/credit-achats-new.component';
import { CreditAchatsEditComponent } from './credits-achats/credit-achats-edit.component';
import { CreditAchatsHomeComponent } from './credits-achats/credit-achats-home.component';
import { CreditsAchatsGridComponent } from './credits-achats/credits-achats-grid.component';
import { FournisseursResolver } from './fournisseurs-resolver';

import { FactureAchatsNewComponent } from './factures-achats/facture-achats-new.component';
import { FactureAchatsEditComponent } from './factures-achats/facture-achats-edit.component';
import { FacturesAchatsGridComponent } from './factures-achats/factures-achats-grid.component';

import { ReleveAchatsNewComponent } from './releves-achats/releve-achats-new.component';
import { ReleveAchatsEditComponent } from './releves-achats/releve-achats-edit.component';
import { RelevesAchatsGridComponent } from './releves-achats/releves-achats-grid.component';


import { DemandeAchatsNewComponent } from './demandes-achats/demande-achats-new.component';
import { DemandeAchatsEditComponent } from './demandes-achats/demande-achats-edit.component';
import { DemandesAchatsGridComponent } from './demandes-achats/demandes-achats-grid.component';


import { ArticlesResolver } from './articles-resolver';
import { MagasinsResolver } from './magasins-resolver';
import { UnMesuresResolver } from './unMesures-resolver';



export const achatsRoute: Routes = [
    {
        path: 'fournisseurs',
        component: FournisseursGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterFournisseur',
        component: FournisseurNewComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerFournisseur/:codeFournisseur',
        component: FournisseurEditComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'creditsAchats',
        component: CreditsAchatsGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterCreditAchats',
        component: CreditAchatsNewComponent,
        resolve: { fournisseurs: FournisseursResolver
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerCreditAchats/:codeCreditAchats',
        component: CreditAchatsEditComponent,
        resolve: { fournisseurs: FournisseursResolver
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },

    {
        path: 'facturesAchats',
        component: FacturesAchatsGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterFactureAchats',
        component: FactureAchatsNewComponent,
        resolve: { 
                fournisseurs: FournisseursResolver,
                 articles: ArticlesResolver,
                 unMesures : UnMesuresResolver,
                 magasins : MagasinsResolver
                 
        
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerFactureAchats/:codeFactureAchats',
        component: FactureAchatsEditComponent,
        resolve: { 
            fournisseurs: FournisseursResolver,
            articles: ArticlesResolver,
            unMesures : UnMesuresResolver,
            magasins : MagasinsResolver
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },

    {
        path: 'relevesAchats',
        component: RelevesAchatsGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterReleveAchats',
        component: ReleveAchatsNewComponent,
        resolve: { 
            fournisseurs: FournisseursResolver,
             articles: ArticlesResolver,
             unMesures : UnMesuresResolver,
            //  magasins : MagasinsResolver
             
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerReleveAchats/:codeReleveAchats',
        component: ReleveAchatsEditComponent,
        resolve: { 
            fournisseurs: FournisseursResolver, 
             articles: ArticlesResolver, 
             unMesures : UnMesuresResolver,
            //  magasins : MagasinsResolver
             
             
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
    },

    {
        path: 'demandesAchats',
        component: DemandesAchatsGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterDemandeAchats',
        component: DemandeAchatsNewComponent,
        resolve: { 
                fournisseurs: FournisseursResolver,
                 articles: ArticlesResolver,
                 unMesures : UnMesuresResolver,
                 magasins : MagasinsResolver
                 
        
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerDemandeAchats/:codeDemandeAchats',
        component: DemandeAchatsEditComponent,
        resolve: { 
            fournisseurs: FournisseursResolver,
            articles: ArticlesResolver,
            unMesures : UnMesuresResolver,
            magasins : MagasinsResolver
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    }
];
// export const achatsRoutingModule: ModuleWithProviders = RouterModule.forRoot(achatsRoute);
// commonRouting.providers = [{ provide: APP_BASE_HREF, useValue: '/common'} ] ;


