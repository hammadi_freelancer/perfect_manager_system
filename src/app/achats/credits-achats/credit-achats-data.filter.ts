export class CreditAchatsDataFilter {
    
        numeroCreditAchats: string;
        montantAPayer: number;
        descriptionEchange: string;
        montantReste: number;
        montantTouche: number;
        
        dateDebutPayementFromObject: Date;
        dateDebutPayementToObject: Date;
        dateLastPayementFromObject: Date;
        dateLastPayementToObject: Date;
        dateNextPayementFromObject: Date;
        dateNextPayementToObject: Date;
        dateCreditAchatsFromObject: Date;
        dateCreditAchatsToObject: Date;
        
        periodTranche: string;
        nbreTranches: number;
        etat: string;
        cinFournisseur: string;
        nomFournisseur: string;
        prenomFournisseur: string;
        raisonFournisseur: string;
        matriculeFournisseur: string;
        registreFournisseur: string;
    }
    