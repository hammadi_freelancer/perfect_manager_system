import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import {CreditAchats} from './credit-achats.model' ;
import { LigneCreditAchats } from './ligne-credit-achats.model';
import {PayementCreditAchats} from './payement-credit-achats.model' ;
import {AjournementCreditAchats} from './ajournement-credit-achats.model' ;
import {TrancheCreditAchats} from './tranche-credit-achats.model';
import {DocumentCreditAchats} from './document-credit-achats.model' ;

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_CREDITS_ACHATS = 'http://localhost:8082/achats/credits-achats/getCreditsAchats';
const URL_GET_CREDIT_ACHATS = 'http://localhost:8082/achats/credits-achats/getCreditAchats';

const  URL_ADD_CREDIT_ACHATS = 'http://localhost:8082/achats/credits-achats/addCreditAchats';
const  URL_UPDATE_CREDITS_ACHATS = 'http://localhost:8082/achats/credits-achats/updateCreditAchats';
const  URL_DELETE_CREDIT_ACHATS = 'http://localhost:8082/achats/credits-achats/deleteCreditAchats';
const  URL_GET_TOTAL_CREDITS_ACHATS = 'http://localhost:8082/achats/credits-achats/getTotalCreditsAchats';

// Payement Credits Achats

const  URL_ADD_PAYEMENT_CREDIT_ACHATS = 'http://localhost:8082/finance/payementsCreditsAchats/addPayementCreditAchats';
const  URL_GET_LIST_PAYEMENT_CREDIT_ACHATS = 'http://localhost:8082/finance/payementsCreditsAchats/getPayementsCreditsAchats';
const  URL_DELETE_PAYEMENT_CREDIT_ACHATS = 'http://localhost:8082/finance/payementsCreditsAchats/deletePayementCreditAchats';
const  URL_UPDATE_PAYEMENT_CREDIT_ACHATS = 'http://localhost:8082/finance/payementsCreditsAchats/updatePayementCreditAchats';
const URL_GET_PAYEMENT_CREDIT_ACHATS = 'http://localhost:8082/finance/payementsCreditsAchats/getPayementCreditAchats';
const URL_GET_LIST_PAYEMENT_CREDIT_ACHATS_BY_CODE_CREDIT =
'http://localhost:8082/finance/payementsCreditsAchats/getPayementsCreditsAchatsByCodeCredit';
const  URL_RECUPERATE_MONTANTS_BY_PERIODE = 'http://localhost:8082/finance/credits/getMontantsByPeriode';


const  URL_ADD_AJOURNEMENT_CREDIT_ACHATS = 'http://localhost:8082/finance/ajournementsCreditsAchats/addAjournementCreditAchats';
const  URL_GET_LIST_AJOURNEMENT_CREDIT_ACHATS  = 'http://localhost:8082/finance/ajournementsCreditsAchats/getAjournementsAchats';
const  URL_DELETE_AJOURNEMENT_CREDIT_ACHATS  = 'http://localhost:8082/finance/ajournementsCreditsAchats/deleteAjournementAchats';
const  URL_UPDATE_AJOURNEMENT_CREDIT_ACHATS  = 'http://localhost:8082/finance/ajournementsCreditsAchats/updateAjournementAchats';
const URL_GET_AJOURNEMENT_CREDIT_ACHATS  = 'http://localhost:8082/finance/ajournementsCreditsAchats/getAjournementAchats';
const URL_GET_LIST_AJOURNEMENT_CREDIT__ACHATS_BY_CODE_CREDIT =
 'http://localhost:8082/finance/ajournementsCreditsAchats/getAjournementsCreditsAchatsByCodeCredit';



 const  URL_ADD_TRANCHE_CREDIT_ACHATS = 'http://localhost:8082/finance/tranchesCreditsAchats/addTrancheCreditAchats';
 const  URL_GET_LIST_TRANCHE_CREDIT_ACHATS = 'http://localhost:8082/finance/tranchesCreditsAchats/getTranchesCreditsAchats';
 const  URL_DELETE_TRANCHE_CREDIT_ACHATS = 'http://localhost:8082/finance/tranchesCreditsAchats/deleteTrancheCreditAchats';
 const  URL_UPDATE_TRANCHE_CREDIT_ACHATS = 'http://localhost:8082/finance/tranchesCreditsAchats/updateTrancheCreditAchats';
 const URL_GET_TRANCHE_CREDIT_ACHATS = 'http://localhost:8082/finance/tranchesCreditsAchats/getTrancheCreditAchats';
 const URL_GET_LIST_TRANCHE_CREDIT__ACHATS_BY_CODE_CREDIT =
  'http://localhost:8082/finance/tranchesCreditsAchats/getTranchesCreditsAchatsByCodeCredit';

  const  URL_ADD_DOCUMENT_CREDIT_ACHATS = 'http://localhost:8082/finance/documentsCreditsAchats/addDocumentCreditAchats';
  const  URL_GET_LIST_DOCUMENT_CREDIT_ACHATS = 'http://localhost:8082/finance/documentsCreditsAchats/getDocumentsCreditsAchats';
  const  URL_DELETE_DOCUCMENT_CREDIT_ACHATS = 'http://localhost:8082/finance/documentsCreditsAchats/deleteDocumentCreditAchats';
  const  URL_UPDATE_DOCUMENT_CREDIT_ACHATS = 'http://localhost:8082/finance/documentsCreditsAchats/updateDocumentCreditAchats';
  const URL_GET_DOCUMENT_CREDIT_ACHATS = 'http://localhost:8082/finance/documentsCreditsAchats/getDocumentCreditAchats';
  const URL_GET_LIST_DOCUMENT_CREDIT_ACHATS_BY_CODE_CREDIT =
   'http://localhost:8082/finance/documentsCredits/getDocumentsCreditsAchatsByCodeCredit';

   const  URL_ADD_LIGNE_CREDIT_ACHATS  = 'http://localhost:8082/finance/lignesCreditsAchats/addLigneCreditAchats';
   const  URL_GET_LIST_LIGNE_CREDIT_ACHATS  = 'http://localhost:8082/finance/lignesCreditsAchats/getLignesCreditsAchats';
   const  URL_DELETE_LIGNE_CREDIT_ACHATS  = 'http://localhost:8082/finance/lignesCreditsAchats/deleteLigneCreditAchats';
   const  URL_UPDATE_LIGNE_CREDIT_ACHATS  = 'http://localhost:8082/finance/lignesCreditsAchats/updateLigneCreditAchats';
   const URL_GET_LIGNE_CREDIT_ACHATS  = 'http://localhost:8082/finance/lignesCreditsAchats/getLigneCreditAchats';
   const URL_GET_LIST_LIGNE_CREDIT__ACHATS_BY_CODE_CREDIT =
    'http://localhost:8082/finance/lignesCredits/getLignesCreditsAchatsByCodeCredit';

@Injectable({
  providedIn: 'root'
})
export class CreditAchatsService {

  constructor(private httpClient: HttpClient) {

   }

   getCreditsAchats(pageNumber, nbElementsPerPage, filter): Observable<CreditAchats[]> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('pageNumber', pageNumber)
    .set('nbElementsPerPage', nbElementsPerPage)
    .set('filter', filterStr);
    return this.httpClient
    .get<CreditAchats[]>(URL_GET_LIST_CREDITS_ACHATS, {params});
   }

   getTotalCreditsAchats(filter): Observable<any> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('filter', filterStr);
    return this.httpClient
    .get<any>(URL_GET_TOTAL_CREDITS_ACHATS, {params});
   }

   addCreditAchats(creditAchats: CreditAchats): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_CREDIT_ACHATS, creditAchats);
  }
  updateCreditAchats(creditAchats: CreditAchats): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_CREDITS_ACHATS, creditAchats);
  }
  deleteCreditAchats(codeCreditAchats): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_CREDIT_ACHATS + '/' + codeCreditAchats);
  }
  getCreditAchats(codeCreditAchats): Observable<CreditAchats> {
    return this.httpClient.get<CreditAchats>(URL_GET_CREDIT_ACHATS + '/' + codeCreditAchats);
  }




  addPayementCreditAchats(payementCreditAchats: PayementCreditAchats): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_PAYEMENT_CREDIT_ACHATS, payementCreditAchats);
    
  }
  updatePayementCreditAchats(pay: PayementCreditAchats): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_PAYEMENT_CREDIT_ACHATS, pay);
  }
  getPayementsCreditsAchats(): Observable<PayementCreditAchats[]> {
    return this.httpClient
    .get<PayementCreditAchats[]>(URL_GET_LIST_PAYEMENT_CREDIT_ACHATS);
   }
   getPayementCreditAchats(codePayementCreditAchats): Observable<PayementCreditAchats> {
    return this.httpClient.get<PayementCreditAchats>(URL_GET_PAYEMENT_CREDIT_ACHATS + '/' + codePayementCreditAchats);
  }
  deletePayementCreditAchats(codePayementCreditAchats): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_PAYEMENT_CREDIT_ACHATS + '/' + codePayementCreditAchats);
  }
  getPayementsCreditsAchatsByCodeCredit(codeCredit): Observable<PayementCreditAchats[]> {
    return this.httpClient.get<PayementCreditAchats[]>(URL_GET_LIST_PAYEMENT_CREDIT_ACHATS_BY_CODE_CREDIT + '/' + codeCredit);
  }
  /* getVentes(): Array<Vente> {
       const listVentes = new Array();
       listVentes.push(new Vente('azr', '23-03-1985', '077777', 899888 , 8777, 5666, '6/9/2009' ));
       listVentes.push(new Vente('qksqkskq', '23-03-1985', '06666', 899888 , 8777 ));
       listVentes.push(new Vente('jjjjj', '23-03-1985', '077777', 899888 , 8777 ));
       return listVentes;
   }*/


   addAjournementCreditAchats(ajournementCreditAchats: AjournementCreditAchats): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_AJOURNEMENT_CREDIT_ACHATS, ajournementCreditAchats);
    
  }
  updateAjournementCreditAchats(ajour: AjournementCreditAchats): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_AJOURNEMENT_CREDIT_ACHATS, ajour);
  }
  getAjournementsCreditsAchats(): Observable<AjournementCreditAchats[]> {
    return this.httpClient
    .get<AjournementCreditAchats[]>(URL_GET_LIST_AJOURNEMENT_CREDIT_ACHATS);
   }
   getAjournementCreditAchats(codeAjournementCreditAchats): Observable<AjournementCreditAchats> {
    return this.httpClient.get<AjournementCreditAchats>(URL_GET_AJOURNEMENT_CREDIT_ACHATS + '/' + codeAjournementCreditAchats);
  }
  deleteAjournementCreditAchats(codeAjournementCreditAchats): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_AJOURNEMENT_CREDIT_ACHATS + '/' + codeAjournementCreditAchats);
  }
  getAjournementsCreditsAchatsByCodeCredit(codeCredit): Observable<AjournementCreditAchats[]> {
    return this.httpClient.get<AjournementCreditAchats[]>(URL_GET_LIST_AJOURNEMENT_CREDIT__ACHATS_BY_CODE_CREDIT + '/' + codeCredit);
  }
   



  addTrancheCreditAchats(trancheCreditAchats: TrancheCreditAchats): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_TRANCHE_CREDIT_ACHATS, trancheCreditAchats);
    
  }
  updateTrancheCreditAchats(tr: TrancheCreditAchats): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_TRANCHE_CREDIT_ACHATS, tr);
  }


  getTranchesCreditsAchats(): Observable<TrancheCreditAchats[]> {
    return this.httpClient
    .get<TrancheCreditAchats[]>(URL_GET_LIST_TRANCHE_CREDIT_ACHATS);
   }
   getTrancheCreditAchats(codeTrancheCreditAchats): Observable<TrancheCreditAchats> {
    return this.httpClient.get<TrancheCreditAchats>(URL_GET_TRANCHE_CREDIT_ACHATS + '/' + codeTrancheCreditAchats);
  }
  deleteTrancheCreditAchats(codeTrancheCreditAchats): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_TRANCHE_CREDIT_ACHATS + '/' + codeTrancheCreditAchats);
  }
  getTranchesCreditsAchatsByCodeCredit(codeCreditAchats): Observable<TrancheCreditAchats[]> {
    return this.httpClient.get<TrancheCreditAchats[]>(
      URL_GET_LIST_TRANCHE_CREDIT__ACHATS_BY_CODE_CREDIT + '/' + codeCreditAchats);
  }

  addDocumentCreditAchats(documentCreditAchats: DocumentCreditAchats): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_DOCUMENT_CREDIT_ACHATS, documentCreditAchats);
    
  }
  updateDocumentCreditAchats(doc: DocumentCreditAchats): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_DOCUMENT_CREDIT_ACHATS, doc);
  }
  getDocumentsCreditsAchats(): Observable<DocumentCreditAchats[]> {
    return this.httpClient
    .get<DocumentCreditAchats[]>(URL_GET_LIST_DOCUMENT_CREDIT_ACHATS);
   }
   getDocumentCreditAchats(codeDocumentCreditAchats): Observable<DocumentCreditAchats> {
    return this.httpClient.get<DocumentCreditAchats>(URL_GET_DOCUMENT_CREDIT_ACHATS
       + '/' + codeDocumentCreditAchats);
  }
  deleteDocumentCreditAchats(codeDocumentCreditAchats): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_DOCUCMENT_CREDIT_ACHATS +
       '/' + codeDocumentCreditAchats);
  }
  getDocumentsCreditsAchatsByCodeCredit(codeCreditAchats): Observable<DocumentCreditAchats[]> {
    return this.httpClient.get<DocumentCreditAchats[]>(
      URL_GET_LIST_DOCUMENT_CREDIT_ACHATS_BY_CODE_CREDIT
       + '/' + codeCreditAchats);
  }


  addLigneCreditAchats(ligneCreditAchats: LigneCreditAchats): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_LIGNE_CREDIT_ACHATS, ligneCreditAchats);
    
  }
  updateLigneCreditAchats(ligneCreditAchats: LigneCreditAchats): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_LIGNE_CREDIT_ACHATS, ligneCreditAchats);
  }
  getLignesCreditsAchats(): Observable<LigneCreditAchats[]> {
    return this.httpClient
    .get<LigneCreditAchats[]>(URL_GET_LIST_LIGNE_CREDIT_ACHATS);
   }
   getLigneCreditAchats(codeLigneCreditAchats): Observable<LigneCreditAchats> {
    return this.httpClient.get<LigneCreditAchats>(URL_GET_LIGNE_CREDIT_ACHATS + '/' + codeLigneCreditAchats);
  }
  deleteLigneCreditAchats(codeLigneCreditAchats): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_LIGNE_CREDIT_ACHATS + '/' + codeLigneCreditAchats);
  }
  getLignesCreditsAchatsByCodeCredit(codeLigneCreditAchats): Observable<LigneCreditAchats[]> {
    return this.httpClient.get<LigneCreditAchats[]>(URL_GET_LIST_LIGNE_CREDIT__ACHATS_BY_CODE_CREDIT + '/' + codeLigneCreditAchats);
  }


  recuperateMontants(dateDepart, dateFin): Observable<any>
  {
    return this.httpClient.post<any>(URL_RECUPERATE_MONTANTS_BY_PERIODE , {'startDate': dateDepart, 'findDate': dateFin});
    
  }

}
