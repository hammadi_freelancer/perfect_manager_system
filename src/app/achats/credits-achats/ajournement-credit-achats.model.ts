
import { BaseEntity } from '../../common/base-entity.model' ;

type Etat = 'Initial' | 'Valide' | 'Annule' 
type ModePayement = 'Espece' | 'Cheque' | 'Virement'

export class AjournementCreditAchats extends BaseEntity {
    constructor(
        public code?: string,
         public codeCreditAchats?: string,
        public dateOperationObject?: Date,
        public nouvelleDatePayementObject?: Date,
        public motif?: string,
        public nouvelleDatePayement?: string,
        public dateOperation?: string,
        
     ) {
         super();
        
       }
  
}
