
import { Component, OnInit, Inject, ViewChild, Input, OnChanges} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {CreditAchatsService} from './credit-achats.service';
import { CreditAchats } from './credit-achats.model';
import {AjournementCreditAchats} from './ajournement-credit-achats.model';
import { AjournementCreditAchatsElement } from './ajournement-credit-achats.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import { DialogAddAjournementCreditAchatsComponent } from './popups/dialog-add-ajournement-credit-achats.component';
import { DialogEditAjournementCreditAchatsComponent } from './popups/dialog-edit-ajournement-credit-achats.component';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-ajournements-credits-achats-grid',
    templateUrl: 'ajournements-credits-achats-grid.component.html',
  })
  export class AjournementsCreditsAchatsGridComponent implements OnInit, OnChanges {
     private listAjournementCreditsAchats: AjournementCreditAchats[] = [];
     private  dataAjournementsCreditsAchatsSource: MatTableDataSource<AjournementCreditAchatsElement>;
     private selection = new SelectionModel<AjournementCreditAchatsElement>(true, []);
     @Input()
     private creditAchats : CreditAchats;
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private creditAchatsService: CreditAchatsService,
     private matSnackBar: MatSnackBar, public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.creditAchatsService.getAjournementsCreditsAchatsByCodeCredit(this.creditAchats.code).subscribe((listAjournementsCreditsAc) => {
        this.listAjournementCreditsAchats = listAjournementsCreditsAc;
// console.log('recieving data from backend', this.listAjournementCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }

ngOnChanges()
{
  this.creditAchatsService.getAjournementsCreditsAchatsByCodeCredit(this.creditAchats.code).subscribe((listAjournementsCreditsAc) => {
    this.listAjournementCreditsAchats = listAjournementsCreditsAc;
// console.log('recieving data from backend', this.listAjournementCredits  );
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
     });
}
  checkOneActionInvoked() {
    const listSelectedAjournementsCreditsAchats: AjournementCreditAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((ajCreditAElement) => {
    return ajCreditAElement.code;
  });
  this.listAjournementCreditsAchats.forEach(ajournementCreditAch => {
    if (codes.findIndex(code => code === ajournementCreditAch.code) > -1) {
      listSelectedAjournementsCreditsAchats.push(ajournementCreditAch);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listAjournementCreditsAchats !== undefined) {

      this.listAjournementCreditsAchats.forEach(ajournementCreditAch => {
             listElements.push( {code: ajournementCreditAch.code ,
          dateOperation: ajournementCreditAch.dateOperation,
          nouvelleDatePayement: ajournementCreditAch.nouvelleDatePayement,
          motif: ajournementCreditAch.motif,
          description: ajournementCreditAch.description
        } );
      });
    }
      this.dataAjournementsCreditsAchatsSource = new MatTableDataSource<AjournementCreditAchatsElement>(listElements);
      this.dataAjournementsCreditsAchatsSource.sort = this.sort;
      this.dataAjournementsCreditsAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'},
     {name: 'nouvelleDatePayement' , displayTitle: 'N°elle Date Pay.'},
     {name: 'motif' , displayTitle: 'Motif'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataAjournementsCreditsAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataAjournementsCreditsAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataAjournementsCreditsAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataAjournementsCreditsAchatsSource.paginator) {
      this.dataAjournementsCreditsAchatsSource.paginator.firstPage();
    }
  }

  showAddAjournementDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeCreditAchats : this.creditAchats.code
   };
 
   const dialogRef = this.dialog.open(DialogAddAjournementCreditAchatsComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(ajAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditAjournementDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeAjournementCreditAchats : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditAjournementCreditAchatsComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(ajAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Ajournement Séléctionnée!');
    }
    }
    supprimerAjournements()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(ajour, index) {
              this.creditAchatsService.deleteAjournementCreditAchats(ajour.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( this.selection.selected.length + ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Ajournement Séléctionnée!');
        }
    }
    refresh() {
      this.creditAchatsService.getAjournementsCreditsAchatsByCodeCredit(this.creditAchats.code).subscribe((listAjourCreditsA) => {
        this.listAjournementCreditsAchats = listAjourCreditsA;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }


}
