import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {CreditAchats} from './credit-achats.model';
import { MODES_GENERATION_TRANCHES} from './credit-achats.model';

import {CreditAchatsService} from './credit-achats.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {ActionData} from './action-data.interface';
import { FournisseurService } from '../fournisseurs/fournisseur.service';
import { CreditAchatsFilter } from './credit-achats.filter';
import {Fournisseur} from '../fournisseurs/fournisseur.model';
import {MatSnackBar} from '@angular/material';


@Component({
  selector: 'app-credit-achats-new',
  templateUrl: './credit-achats-new.component.html',
  // styleUrls: ['./players.component.css']
})
export class CreditAchatsNewComponent implements OnInit {

 private creditAchats: CreditAchats =  new CreditAchats();
 private saveEnCours = false;
@Input()
private listCreditsAchats: any = [] ;

@Output()
save: EventEmitter<CreditAchats> = new EventEmitter<CreditAchats>();

@Output()
showDocumentsPanel: EventEmitter<Boolean> = new EventEmitter<Boolean>();


@ViewChild('EDITIONBUTTONS') editionButtons: ElementRef;

@ViewChild('SAVEBUTTON') saveButton: ElementRef;

@ViewChild('PANELTRANCHES') panelTranches : ElementRef;
@ViewChild('PANELHEADER') panelHeader : ElementRef;

@Input()
private actionData: ActionData;

private creditAchatsFilter: CreditAchatsFilter;
private listFournisseurs: Fournisseur[];

private etatsCreditAchats: any[] = [
  {value: 'Initial', viewValue: 'Initial'},
  {value: 'Valide', viewValue: 'Validé'}
];

private periodesCreditsAchats: any[] = [
  {value: 'Semaine', viewValue: 'Semaine'},
  {value: 'Mois', viewValue: 'Mois'},
  {value: 'Trimestre', viewValue: 'Trimestre'},
  {value: 'Simestre', viewValue: 'Simestre'},
  {value: 'Annee', viewValue: 'Annéé'}
  
  
];
  constructor( private route: ActivatedRoute,
    private router: Router, private creditAchatsService: CreditAchatsService, private fournisseurService: FournisseurService,
   private matSnackBar: MatSnackBar) {
  }



  ngOnInit() {

              this.listFournisseurs = this.route.snapshot.data.fournisseurs;
              this.creditAchatsFilter = new CreditAchatsFilter(this.listFournisseurs);
  }
  saveCreditAchats() {
    
    
    console.log(this.creditAchats);
    if (this.creditAchats.etat === '' || this.creditAchats.etat === undefined) {
      this.creditAchats.etat = 'Initial';
    }
    if (this.creditAchats.montantAPayer === 0 || this.creditAchats.montantAPayer === undefined )
      {
        this.openSnackBar(  'Il\'est indisponsable de préciser le montant de crédit');
        
      } else  {
        this.saveEnCours = true;
    this.creditAchatsService.addCreditAchats(this.creditAchats).subscribe((response) => {
      this.saveEnCours = false;
        if (response.error_message ===  undefined) {
          this.creditAchats =  new CreditAchats();
          this.openSnackBar(  'Crédit  Enregistré');
        } else {
         // show error message
         this.openSnackBar(  'Erreur:Opération Echouée');
         
        }
    });
  }
  }
  fillDataNomPrenom()
  {
    const listFiltred  = this.listFournisseurs.filter((fournisseur) => (fournisseur.cin === this.creditAchats.fournisseur.cin));
    this.creditAchats.fournisseur.nom = listFiltred[0].nom;
    this.creditAchats.fournisseur.prenom = listFiltred[0].prenom;
  }
  fillDataMatriculeRaison()
  {
    const listFiltred  = this.listFournisseurs.filter((fournisseur) =>
     (fournisseur.registreCommerce === this.creditAchats.fournisseur.registreCommerce));
    this.creditAchats.fournisseur.matriculeFiscale = listFiltred[0].matriculeFiscale;
    this.creditAchats.fournisseur.raisonSociale = listFiltred[0].raisonSociale;
  }
  vider() {
    this.creditAchats =  new CreditAchats();
  }
  openSnackBar(messageToDisplay) {
   this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
       verticalPosition: 'top'});
  }

loadGridCreditsAchats() {
  this.router.navigate(['/pms/achats/creditsAchats']) ;
}
previous() {

}

}
