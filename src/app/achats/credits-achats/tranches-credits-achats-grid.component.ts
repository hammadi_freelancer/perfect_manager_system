
import { Component, OnInit, Inject, ViewChild, Input, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {CreditAchatsService} from './credit-achats.service';
import {CreditAchats } from './credit-achats.model';
import {TrancheCreditAchats} from './tranche-credit-achats.model';
import { TrancheCreditAchatsElement } from './tranche-credit-achats.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DialogAddTrancheCreditAchatsComponent } from './popups/dialog-add-tranche-credit-achats.component';
import { DialogEditTrancheCreditAchatsComponent } from './popups/dialog-edit-tranche-credit-achats.component';

@Component({
    selector: 'app-tranches-credits-achats-grid',
    templateUrl: 'tranches-credits-achats-grid.component.html',
  })
  export class TranchesCreditsAchatsGridComponent implements OnInit, OnChanges {

     @Input()
     private creditAchats : CreditAchats;

     private listTranchesCreditsAchats: TrancheCreditAchats[] = [];
     private  dataTranchesCreditsAchatsSource: MatTableDataSource<TrancheCreditAchatsElement>;
     private selection = new SelectionModel<TrancheCreditAchatsElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private creditAchatsService: CreditAchatsService,
     private matSnackBar: MatSnackBar,
    public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.creditAchatsService.getTranchesCreditsAchatsByCodeCredit(this.creditAchats.code).subscribe((listTranchesCreditsAchats) => {
        this.listTranchesCreditsAchats = listTranchesCreditsAchats;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }

  ngOnChanges()
  {
    this.creditAchatsService.getTranchesCreditsAchatsByCodeCredit(this.creditAchats.code).subscribe((listTranchesCreditsA) => {
      this.listTranchesCreditsAchats = listTranchesCreditsA;
// console.log('recieving data from backend', this.listTranchesCredits  );
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });

  }
  checkOneActionInvoked() {
    const listSelectedTranchesCreditsAchats: TrancheCreditAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((trancheCreditAElement) => {
    return trancheCreditAElement.code;
  });
  this.listTranchesCreditsAchats.forEach(trancheCreditAchatsElement => {
    if (codes.findIndex(code => code === trancheCreditAchatsElement.code) > -1) {
      listSelectedTranchesCreditsAchats.push(trancheCreditAchatsElement);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listTranchesCreditsAchats !== undefined) {

      this.listTranchesCreditsAchats.forEach(trancheCreditA => {
             listElements.push( {code: trancheCreditA.code ,
            datePayementProgramme: trancheCreditA.datePayementProgramme,
            montantProgramme: trancheCreditA.montantProgramme,
          etat: trancheCreditA.etat,
          description: trancheCreditA.description
        
        } );
      });
    }
      this.dataTranchesCreditsAchatsSource = new MatTableDataSource<TrancheCreditAchatsElement>(listElements);
      this.dataTranchesCreditsAchatsSource.sort = this.sort;
      this.dataTranchesCreditsAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'datePayementProgramme' , displayTitle: 'Date de Payement Prévue'},
     {name: 'montantProgramme' , displayTitle: 'Montant Prévu'},
     {name: 'etat' , displayTitle: 'Etat'},
      {name: 'description' , displayTitle: 'description'}
    
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataTranchesCreditsAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataTranchesCreditsAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataTranchesCreditsAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataTranchesCreditsAchatsSource.paginator) {
      this.dataTranchesCreditsAchatsSource.paginator.firstPage();
    }
  }
  showAddTrancheDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeCreditAchats : this.creditAchats.code
   };
 
   const dialogRef = this.dialog.open(DialogAddTrancheCreditAchatsComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(trAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditTrancheDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeTrancheCreditAchats : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditTrancheCreditAchatsComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(trAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucune Tranche Séléctionnée!');
    }
    }
    supprimerTranches()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(tranche, index) {
            console.log('tranche to delete is ', tranche);
              this.creditAchatsService.deleteTrancheCreditAchats(tranche.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( this.selection.selected.length + ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucune Tranche Séléctionnée!');
        }
    }
    refresh() {
      this.creditAchatsService.getTranchesCreditsAchatsByCodeCredit(this.creditAchats.code).subscribe((listTranchesCreditsAchats) => {
        this.listTranchesCreditsAchats = listTranchesCreditsAchats;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
