import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { AlertMessage } from '../../common/alert-message.interface';
import { OperationRequest } from '../../common/operation-request.interface';
import { OperationResponse } from '../../common/operation-response.interface';

@Injectable()
export class CommunicationService {
  // Observable string sources
  private alertSource = new Subject<AlertMessage>();
  private askOperationSource = new Subject<OperationRequest>();
  private confirmOperationSource = new Subject<OperationResponse>();
  
  // Observable string streams
  alert$ = this.alertSource.asObservable();
  askOperation$ = this.askOperationSource.asObservable();
  confirmOperation$ = this.confirmOperationSource.asObservable();
  // Service message commands
  alert(alertMessage: AlertMessage) {
    this.alertSource.next(alertMessage);
  }
  askOperation(request: OperationRequest) {
    this.askOperationSource.next(request);
  }
  confirmOperation(response: OperationResponse) {
    this.confirmOperationSource.next(response);
  }
}
