import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,
    
      ViewChild } from '@angular/core';
    // import { ArticleService } from './article.service';
    import {CreditAchats} from './credit-achats.model';
    import { MODES_GENERATION_TRANCHES} from './credit-achats.model';
    import {CreditAchatsService} from './credit-achats.service';
    import {FormControl} from '@angular/forms';
    import {map, startWith} from 'rxjs/operators';
    import {Observable} from 'rxjs';
    import { Router, ActivatedRoute, ParamMap } from '@angular/router';
    import {ActionData} from './action-data.interface';
    import { FournisseurService } from '../fournisseurs/fournisseur.service';
    @Component({
        selector: 'app-credit-achats-home',
        templateUrl: './credit-achats-home.component.html',
        // styleUrls: ['./players.component.css']
      })
 export class CreditAchatsHomeComponent implements OnInit {

    @Input()
    private actionData: ActionData;

    
    private homeMessage = 'Here I will display the list of : last credits updated' +
    'last credits created ; last credits ...';

        constructor( private route: ActivatedRoute,
            private router: Router, private creditAchatsService: CreditAchatsService, private fournisseurService: FournisseurService) {
          }

          ngOnInit() {
        }
        
        }
