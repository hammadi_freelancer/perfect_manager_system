
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {CreditAchatsService} from '../credit-achats.service';
import {PayementCreditAchats} from '../payement-credit-achats.model';
import { PayementCreditAchatsElement } from '../payement-credit-achats.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-payements-credits-achats',
    templateUrl: 'dialog-list-payements-credits-achats.component.html',
  })
  export class DialogListPayementsCreditsAchatsComponent implements OnInit {
     private listPayementsCreditsAchats: PayementCreditAchats[] = [];
     private  dataPayementsCreditsAchatsSource: MatTableDataSource<PayementCreditAchatsElement>;
     private selection = new SelectionModel<PayementCreditAchatsElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private creditAchatsService: CreditAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
     //  console.log('code credit in dialog component is :', this.data.codeCreditAchats);
      this.creditAchatsService.getPayementsCreditsAchatsByCodeCredit(this.data.codeCreditAchats).subscribe((listPayementsCreditsAchats) => {
        this.listPayementsCreditsAchats = listPayementsCreditsAchats;
// console.log('recieving data from backend', this.listPayementsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedPayementsCreditsAchats: PayementCreditAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((payementCreditAElement) => {
    return payementCreditAElement.code;
  });
  this.listPayementsCreditsAchats.forEach(payementCreditA => {
    if (codes.findIndex(code => code === payementCreditA.code) > -1) {
      listSelectedPayementsCreditsAchats.push(payementCreditA);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listPayementsCreditsAchats !== undefined) {

      this.listPayementsCreditsAchats.forEach(payementCreditAch => {
             listElements.push( {code: payementCreditAch.code ,
          dateOperation: payementCreditAch.dateOperation,
          montantPaye: payementCreditAch.montantPaye,
          modePayement: payementCreditAch.modePayement,
          numeroCheque: payementCreditAch.numeroCheque,
          dateCheque: payementCreditAch.dateCheque,
          compte: payementCreditAch.compte,
          compteAdversaire: payementCreditAch.compteAdversaire,
          banque: payementCreditAch.banque,
          banqueAdversaire: payementCreditAch.banqueAdversaire,
          description: payementCreditAch.description
        } );
      });
    }
      this.dataPayementsCreditsAchatsSource = new MatTableDataSource<PayementCreditAchatsElement>(listElements);
      this.dataPayementsCreditsAchatsSource.sort = this.sort;
      this.dataPayementsCreditsAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'}, 
     {name: 'montantPaye' , displayTitle: 'Montant Payé'},
     {name: 'modePayement' , displayTitle: 'Mode Payement'},
      {name: 'numeroCheque' , displayTitle: 'N°Chèque'},
     {name: 'dateCheque' , displayTitle: 'Date Chèque'}, 
     {name: 'compte' , displayTitle: 'Compte'},
     {name: 'compteAdversaire' , displayTitle: 'Compte Adv.'},
      {name: 'banque' , displayTitle: 'Banque'},
       { name : 'banqueAdversaire' , displayTitle : 'Banque Adv.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataPayementsCreditsAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataPayementsCreditsAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataPayementsCreditsAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataPayementsCreditsAchatsSource.paginator) {
      this.dataPayementsCreditsAchatsSource.paginator.firstPage();
    }
  }

}
