
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {CreditAchatsService} from '../credit-achats.service';
import {AjournementCreditAchats} from '../ajournement-credit-achats.model';

@Component({
    selector: 'app-dialog-edit-ajournement-credit-achats',
    templateUrl: 'dialog-edit-ajournement-credit-achats.component.html',
  })
  export class DialogEditAjournementCreditAchatsComponent implements OnInit {
     private ajournementCreditAchats: AjournementCreditAchats;
     private updateEnCours = false;
     
     constructor(  private creditAchatsService: CreditAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {

        this.creditAchatsService.getAjournementCreditAchats(this.data.codeAjournementCreditAchats).subscribe((ajour) => {
            this.ajournementCreditAchats = ajour;
     });


    }
    updateAjournementCreditAchats() 
    {
     this.updateEnCours = true;
       this.creditAchatsService.updateAjournementCreditAchats(this.ajournementCreditAchats).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.openSnackBar(  'Ajournement Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
