
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {CreditAchatsService} from '../credit-achats.service';
import {DocumentCreditAchats} from '../document-credit-achats.model';
import { DocumentCreditAchatsElement } from '../document-credit-achats.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-documents-credits-achats',
    templateUrl: 'dialog-list-documents-credits-achats.component.html',
  })
  export class DialogListDocumentsCreditsAchatsComponent implements OnInit {
     private listDocumentsCreditsAchats: DocumentCreditAchats[] = [];
     private  dataDocumentsCreditsAchatsSource: MatTableDataSource<DocumentCreditAchatsElement>;
     private selection = new SelectionModel<DocumentCreditAchatsElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private creditAchatsService: CreditAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      console.log('code credit in dialog component is :', this.data.codeCreditAchats);
      this.creditAchatsService.getDocumentsCreditsAchatsByCodeCredit(this.data.codeCreditAchats).subscribe((listDocumentsCreditsAchats) => {
        this.listDocumentsCreditsAchats = listDocumentsCreditsAchats;
// console.log('recieving data from backend', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedDocumentsCreditsAchats: DocumentCreditAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentCreditAElement) => {
    return documentCreditAElement.code;
  });
  this.listDocumentsCreditsAchats.forEach(documentCreditAchats => {
    if (codes.findIndex(code => code === documentCreditAchats.code) > -1) {
      listSelectedDocumentsCreditsAchats.push(documentCreditAchats);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsCreditsAchats !== undefined) {

      this.listDocumentsCreditsAchats.forEach(docCreditA => {
             listElements.push( {code: docCreditA.code ,
          dateReception: docCreditA.dateReception,
          numeroDocument: docCreditA.numeroDocument,
          typeDocument: docCreditA.typeDocument,
          description: docCreditA.description
        } );
      });
    }
      this.dataDocumentsCreditsAchatsSource= new MatTableDataSource<DocumentCreditAchatsElement>(listElements);
      this.dataDocumentsCreditsAchatsSource.sort = this.sort;
      this.dataDocumentsCreditsAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsCreditsAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsCreditsAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsCreditsAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsCreditsAchatsSource.paginator) {
      this.dataDocumentsCreditsAchatsSource.paginator.firstPage();
    }
  }

}
