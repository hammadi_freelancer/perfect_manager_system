
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {CreditAchatsService} from '../credit-achats.service';
import {AjournementCreditAchats} from '../ajournement-credit-achats.model';
import { AjournementCreditAchatsElement } from '../ajournement-credit-achats.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-ajournements-credits-achats',
    templateUrl: 'dialog-list-ajournements-credits-achats.component.html',
  })
  export class DialogListAjournementsCreditsAchatsComponent implements OnInit {
     private listAjournementCreditsAchats: AjournementCreditAchats[] = [];
     private  dataAjournementsCreditsAchatsSource: MatTableDataSource<AjournementCreditAchatsElement>;
     private selection = new SelectionModel<AjournementCreditAchatsElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private creditAchatsService: CreditAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      console.log('code credit in dialog component is :', this.data.codeCreditAchats);
      this.creditAchatsService.
      getAjournementsCreditsAchatsByCodeCredit(this.data.codeCreditAchats).subscribe((listAjournementsCreditsAchats) => {
        this.listAjournementCreditsAchats = listAjournementsCreditsAchats;
// console.log('recieving data from backend', this.listAjournementCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedAjournementsCreditsAchats: AjournementCreditAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((payementCreditAchatsElement) => {
    return payementCreditAchatsElement.code;
  });
  this.listAjournementCreditsAchats.forEach(ajournementCA => {
    if (codes.findIndex(code => code === ajournementCA.code) > -1) {
      listSelectedAjournementsCreditsAchats.push(ajournementCA);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listAjournementCreditsAchats !== undefined) {

      this.listAjournementCreditsAchats.forEach(ajournementCreditA => {
             listElements.push( {code: ajournementCreditA.code ,
          dateOperation: ajournementCreditA.dateOperation,
          nouvelleDatePayement: ajournementCreditA.nouvelleDatePayement,
          motif: ajournementCreditA.motif,
          description: ajournementCreditA.description
        } );
      });
    }
      this.dataAjournementsCreditsAchatsSource = new MatTableDataSource<AjournementCreditAchatsElement>(listElements);
      this.dataAjournementsCreditsAchatsSource.sort = this.sort;
      this.dataAjournementsCreditsAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'},
     {name: 'nouvelleDatePayement' , displayTitle: 'N°elle Date Pay.'},
     {name: 'motif' , displayTitle: 'Motif'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataAjournementsCreditsAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataAjournementsCreditsAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataAjournementsCreditsAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataAjournementsCreditsAchatsSource.paginator) {
      this.dataAjournementsCreditsAchatsSource.paginator.firstPage();
    }
  }

}
