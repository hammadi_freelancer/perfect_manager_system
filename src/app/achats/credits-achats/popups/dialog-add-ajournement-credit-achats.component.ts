
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {CreditAchatsService} from '../credit-achats.service';
import {AjournementCreditAchats} from '../ajournement-credit-achats.model';

@Component({
    selector: 'app-dialog-add-ajournement-credit-achats',
    templateUrl: 'dialog-add-ajournement-credit-achats.component.html',
  })
  export class DialogAddAjournementCreditAchatsComponent implements OnInit {
     private ajournementCreditAchats: AjournementCreditAchats;
     private saveEnCours = false;
     constructor(  private creditAchatsService: CreditAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.ajournementCreditAchats = new AjournementCreditAchats();
    }
    saveAjournementCreditAchats() 
    {
      this.ajournementCreditAchats.codeCreditAchats = this.data.codeCreditAchats;
      this.saveEnCours = true;
       console.log(this.ajournementCreditAchats);
      //  this.saveEnCours = true;
       this.creditAchatsService.addAjournementCreditAchats(this.ajournementCreditAchats).subscribe((response) => {
        this.saveEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.ajournementCreditAchats = new AjournementCreditAchats();
             this.openSnackBar(  'Ajournement Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
