
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {CreditAchatsService} from '../credit-achats.service';
// import {LigneCredit} from '../ligne-credit.model';

@Component({
    selector: 'app-dialog-display-stat-credits-achats',
    templateUrl: 'dialog-display-stat-credits-achats.component.html',
  })
  export class DialogDisplayStatistiquesCreditsAchatsComponent implements OnInit {
     private loadEnCours = false;
     private dateDepartObject ;
     private dateFinObject ;
     private montantRegleAchats;
     private montantNonRegleAchats;
     private montantRegleCredit;
     private montantNonRegleCredit;
     constructor(  private creditAchatsService: CreditAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
          // this.ligneCredit = new LigneCredit();
    }
    recuperateMontants() 
    {
     
       this.loadEnCours = true;
       
      //  this.saveEnCours = true;
       this.creditAchatsService.recuperateMontants(this.dateDepartObject, this.dateFinObject).subscribe((response) => {
        this.loadEnCours = false;
        if (response.error_message ===  undefined) {
             this.montantNonRegleCredit = response.montantNonRegleCredit;
             this.montantRegleCredit = response.montantRegleCredit;
             this.montantRegleAchats = response.montantRegleAchats;
             this.montantNonRegleAchats = response.montantNonRegleAchats;
             
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
