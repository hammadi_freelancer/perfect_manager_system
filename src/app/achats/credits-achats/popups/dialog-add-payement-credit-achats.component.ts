
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {CreditAchatsService} from '../credit-achats.service';
import {PayementCreditAchats} from '../payement-credit-achats.model';

@Component({
    selector: 'app-dialog-add-payement-credit-achats',
    templateUrl: 'dialog-add-payement-credit-achats.component.html',
  })
  export class DialogAddPayementCreditAchatsComponent implements OnInit {
     private payementCreditAchats: PayementCreditAchats;
     private saveEnCours = false;
     
     constructor(  private creditAchatsService: CreditAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.payementCreditAchats = new PayementCreditAchats();
    }
    savePayementCreditAchats() 
    {
      this.payementCreditAchats.codeCreditAchats = this.data.codeCreditAchats;
       //console.log(this.payementCredit);
       this.saveEnCours = true;
       
      //  this.saveEnCours = true;
       this.creditAchatsService.addPayementCreditAchats(this.payementCreditAchats).subscribe((response) => {
        this.saveEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.payementCreditAchats = new PayementCreditAchats();
             this.openSnackBar(  'Payement Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
