
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {CreditAchatsService} from '../credit-achats.service';
import {PayementCreditAchats} from '../payement-credit-achats.model';

@Component({
    selector: 'app-dialog-edit-payement-credit-achats',
    templateUrl: 'dialog-edit-payement-credit-achats.component.html',
  })
  export class DialogEditPayementCreditAchatsComponent implements OnInit {
     private payementCreditAchats: PayementCreditAchats;
     private updateEnCours = false;
     constructor(  private creditAchatsService: CreditAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit() {
        this.creditAchatsService.getPayementCreditAchats(this.data.codePayementCreditAchats).subscribe((payement) => {
            this.payementCreditAchats = payement;
     });
   }
    updatePayementCreditAchats() {
      //  this.saveEnCours = true;
      this.updateEnCours = true;
       this.creditAchatsService.updatePayementCreditAchats(this.payementCreditAchats).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             // this.payementCredit = new PayementCredit();
             this.openSnackBar(  'Payement Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
