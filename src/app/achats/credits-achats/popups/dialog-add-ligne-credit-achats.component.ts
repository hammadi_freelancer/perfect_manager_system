
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {CreditAchatsService} from '../credit-achats.service';
import {LigneCreditAchats} from '../ligne-credit-achats.model';

@Component({
    selector: 'app-dialog-add-ligne-credit-achats',
    templateUrl: 'dialog-add-ligne-credit-achats.component.html',
  })
  export class DialogAddLigneCreditAchatsComponent implements OnInit {
     private ligneCreditAchats: LigneCreditAchats;
     private saveEnCours = false;
     
     constructor(  private creditAchatsService: CreditAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.ligneCreditAchats = new LigneCreditAchats();
    }
    saveLigneCreditAchats() 
    {
      this.ligneCreditAchats.codeCreditAchats = this.data.codeCreditAchats;
       console.log(this.ligneCreditAchats);
       this.saveEnCours = true;
       
      //  this.saveEnCours = true;
       this.creditAchatsService.addLigneCreditAchats(this.ligneCreditAchats).subscribe((response) => {
        this.saveEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.ligneCreditAchats = new LigneCreditAchats();
             this.openSnackBar(  'Ligne Enregistrée');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
