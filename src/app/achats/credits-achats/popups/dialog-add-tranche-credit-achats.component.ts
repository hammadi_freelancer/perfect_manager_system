
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {CreditAchatsService} from '../credit-achats.service';
import {TrancheCreditAchats} from '../tranche-credit-achats.model';

@Component({
    selector: 'app-dialog-add-tranche-credit-achats',
    templateUrl: 'dialog-add-tranche-credit-achats.component.html',
  })
  export class DialogAddTrancheCreditAchatsComponent implements OnInit {
     private trancheCreditAchats: TrancheCreditAchats;
     private saveEnCours = false;
     
     constructor(  private creditAchatsService: CreditAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.trancheCreditAchats = new TrancheCreditAchats();
    }
    saveTrancheCreditAchats() 
    {
      this.trancheCreditAchats.codeCreditAchats = this.data.codeCreditAchats;
      //  console.log(this.trancheCredit);
       this.saveEnCours = true;
       
      //  this.saveEnCours = true;
       this.creditAchatsService.addTrancheCreditAchats(this.trancheCreditAchats).subscribe((response) => {
        this.saveEnCours = false;
        
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.trancheCreditAchats = new TrancheCreditAchats();
             this.openSnackBar(  'Tranche Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
