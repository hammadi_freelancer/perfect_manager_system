
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {CreditAchatsService} from '../credit-achats.service';
import {TrancheCreditAchats} from '../tranche-credit-achats.model';

@Component({
    selector: 'app-dialog-edit-tranche-credit-achats',
    templateUrl: 'dialog-edit-tranche-credit-achats.component.html',
  })
  export class DialogEditTrancheCreditAchatsComponent implements OnInit {
     private trancheCreditAchats: TrancheCreditAchats;
     private updateEnCours = false;
     constructor(  private creditAchatsService: CreditAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.creditAchatsService.getTrancheCreditAchats(this.data.codeTrancheCreditAchats).subscribe((tranche) => {
            console.log('Tranche Credit Recuperated is :');
            console.log(tranche);
            
                  this.trancheCreditAchats = tranche;
           });
    }
    updateTrancheCreditAchats() 
    {
       console.log(this.trancheCreditAchats);
        this.updateEnCours = true;
      //  this.saveEnCours = true;
       this.creditAchatsService.updateTrancheCreditAchats(this.trancheCreditAchats).subscribe((response) => {
           this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             //this.trancheCredit = new TrancheCredit();
             this.openSnackBar(  'Tranche Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
