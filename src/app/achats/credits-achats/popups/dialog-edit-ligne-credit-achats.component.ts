
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {CreditAchatsService} from '../credit-achats.service';
import {LigneCreditAchats} from '../ligne-credit-achats.model';

@Component({
    selector: 'app-dialog-edit-ligne-credit-achats',
    templateUrl: 'dialog-edit-ligne-credit-achats.component.html',
  })
  export class DialogEditLigneCreditAchatsComponent implements OnInit {
     private ligneCreditAchats: LigneCreditAchats;
     private etatsLigne: any[] = [
      {value: 'NonReglee', viewValue: 'Non Réglée'},
      {value: 'Regle', viewValue: 'Réglée'}
    ];
     private updateEnCours = false;
     constructor(  private creditAchatsService: CreditAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit() {
        this.creditAchatsService.getLigneCreditAchats(this.data.codeLigneCreditAchats).subscribe((ligne) => {
            this.ligneCreditAchats = ligne;
     });
   }
    updateLigneCreditAchats() {
      //  this.saveEnCours = true;
      this.updateEnCours = true;
       this.creditAchatsService.updateLigneCreditAchats(this.ligneCreditAchats).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             // this.payementCredit = new PayementCredit();
             this.openSnackBar(  'Ligne Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
