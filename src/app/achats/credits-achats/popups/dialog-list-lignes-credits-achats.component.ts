
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {CreditAchatsService} from '../credit-achats.service';
import {LigneCreditAchats} from '../ligne-credit-achats.model';
import { LigneCreditAchatsElement } from '../ligne-credit-achats.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-lignes-credits-achats',
    templateUrl: 'dialog-list-lignes-credits-achats.component.html',
  })
  export class DialogListLignesCreditsAchatsComponent implements OnInit {
     private listLignesCreditsAchats: LigneCreditAchats[] = [];
     private  dataLignesCreditsAchatsSource: MatTableDataSource<LigneCreditAchatsElement>;
     private selection = new SelectionModel<LigneCreditAchatsElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private creditAchatsService: CreditAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
    this.creditAchatsService.getLignesCreditsAchatsByCodeCredit(this.data.codeCreditAchats).subscribe((listLignesCreditsAch) => {
        this.listLignesCreditsAchats = listLignesCreditsAch;
// console.log('recieving data from backend', this.listPayementsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedLignesCreditsAchats: LigneCreditAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((ligneCreditAElement) => {
    return ligneCreditAElement.code;
  });
  this.listLignesCreditsAchats.forEach(ligneCreditA=> {
    if (codes.findIndex(code => code === ligneCreditA.code) > -1) {
      listSelectedLignesCreditsAchats.push(ligneCreditA);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listLignesCreditsAchats !== undefined) {

      this.listLignesCreditsAchats.forEach(ligneCreditAch => {
             listElements.push( {code: ligneCreditAch.code ,
          dateOperation: ligneCreditAch.dateOperation,
          montantAPaye: ligneCreditAch.montantAPayer,
          description: ligneCreditAch.description
        } );
      });
    }
      this.dataLignesCreditsAchatsSource= new MatTableDataSource<LigneCreditAchatsElement>(listElements);
      this.dataLignesCreditsAchatsSource.sort = this.sort;
      this.dataLignesCreditsAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'}, 
     {name: 'montantAPayer' , displayTitle: 'Montant A Payer'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataLignesCreditsAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataLignesCreditsAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataLignesCreditsAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataLignesCreditsAchatsSource.paginator) {
      this.dataLignesCreditsAchatsSource.paginator.firstPage();
    }
  }

}
