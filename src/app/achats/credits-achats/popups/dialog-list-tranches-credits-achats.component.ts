
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {CreditAchatsService} from '../credit-achats.service';
import {TrancheCreditAchats} from '../tranche-credit-achats.model';
import { TrancheCreditAchatsElement } from '../tranche-credit-achats.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-tranches-credits-achats',
    templateUrl: 'dialog-list-tranches-credits-achats.component.html',
  })
  export class DialogListTranchesCreditsAchatsComponent implements OnInit {
     private listTranchesCreditsAchats: TrancheCreditAchats[] = [];
     private  dataTranchesCreditsAchatsSource: MatTableDataSource<TrancheCreditAchatsElement>;
     private selection = new SelectionModel<TrancheCreditAchatsElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private creditAchatsService: CreditAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      console.log('code credit in dialog component is :', this.data.codeCredit);
      this.creditAchatsService.getTranchesCreditsAchatsByCodeCredit(this.data.codeCredit).subscribe((listTranchesCreditsAchats) => {
        this.listTranchesCreditsAchats = listTranchesCreditsAchats;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedTranchesCreditsAchats: TrancheCreditAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((trancheCreditAchatsElement) => {
    return trancheCreditAchatsElement.code;
  });
  this.listTranchesCreditsAchats.forEach(trancheCreditAElement => {
    if (codes.findIndex(code => code === trancheCreditAElement.code) > -1) {
      listSelectedTranchesCreditsAchats.push(trancheCreditAElement);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listTranchesCreditsAchats !== undefined) {

      this.listTranchesCreditsAchats.forEach(trancheCreditA => {
             listElements.push( {code: trancheCreditA.code ,
            datePayementProgramme: trancheCreditA.datePayementProgramme,
          montantPaye: trancheCreditA.montantPaye,
          etat: trancheCreditA.etat,
          description: trancheCreditA.description
        
        } );
      });
    }
      this.dataTranchesCreditsAchatsSource = new MatTableDataSource<TrancheCreditAchatsElement>(listElements);
      this.dataTranchesCreditsAchatsSource.sort = this.sort;
      this.dataTranchesCreditsAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'datePayementProgramme' , displayTitle: 'Date de Payement Prévue'},
     {name: 'montantPaye' , displayTitle: 'Montant Prévu'},
     {name: 'etat' , displayTitle: 'Etat'},
      {name: 'description' , displayTitle: 'description'}
    
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataTranchesCreditsAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataTranchesCreditsAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataTranchesCreditsAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataTranchesCreditsAchatsSource.paginator) {
      this.dataTranchesCreditsAchatsSource.paginator.firstPage();
    }
  }

}
