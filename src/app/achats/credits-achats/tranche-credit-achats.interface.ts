


export interface TrancheCreditAchatsElement {
         code: string ;
         montantProgramme: number;
         montantPaye: number;
          datePayementProgramme?: string;
          datePayement?: string;
          etat?: string;
          description?: string ;
        }
