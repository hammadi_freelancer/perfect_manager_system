
import { BaseEntity } from '../../common/base-entity.model' ;
type Etat = 'Initial' | 'Valide' | 'Annule' | 'Regle' | 'PartiellementRegle';

export class TrancheCreditAchats extends BaseEntity {
    constructor(public code ?: string,
        public codeCreditAchats?: string,
         public datePayementProgrammeObject?: Date,
         public datePayementObject?: Date,
         public datePayement?: string,
         public datePayementProgramme?: string,
     public montantProgramme?: number,
     public montantPaye?: number,
     public etat?: Etat,
    ) {
         super();
        this.etat =  'Initial';
    }
}
