import { Component, OnInit, Output,ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {CreditAchats} from './credit-achats.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {MatDialog, MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {CreditAchatsService} from './credit-achats.service';
import { ActionData } from './action-data.interface';
import { Fournisseur } from '../fournisseurs/fournisseur.model';
import { CreditAchatsElement } from './credit-achats.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import { FournisseurService } from '../fournisseurs/fournisseur.service';
import { DialogAddFournisseurComponent } from '../fournisseurs/dialog-add-fournisseur.component';
import { DialogAddPayementCreditAchatsComponent } from './popups/dialog-add-payement-credit-achats.component';
import { DialogAddAjournementCreditAchatsComponent } from './popups/dialog-add-ajournement-credit-achats.component';
import { DialogAddTrancheCreditAchatsComponent} from './popups/dialog-add-tranche-credit-achats.component';
import { DialogAddDocumentCreditAchatsComponent} from './popups/dialog-add-document-credit-achats.component';
import { DialogDisplayStatistiquesCreditsAchatsComponent} from './popups/dialog-display-stat-credits-achats.component';
import { CreditAchatsDataFilter} from './credit-achats-data.filter';
import { SelectItem } from 'primeng/api';

@Component({
  selector: 'app-credits-achats-grid',
  templateUrl: './credits-achats-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class CreditsAchatsGridComponent implements OnInit {

  private creditAchats: CreditAchats =  new CreditAchats();
  private showFilter = false;
  private showSelectColumnsMultiSelect = false;
  
  private creditAchatsDataFilter = new CreditAchatsDataFilter();
  private selection = new SelectionModel<CreditAchatsElement>(true, []);
  
@Output()
select: EventEmitter<CreditAchats[]> = new EventEmitter<CreditAchats[]>();

//private lastClientAdded: Client;

 selectedCreditAchats: CreditAchats ;

 @Input()
 private actionData: ActionData;

 @Input()
 private listCreditsAchats: any = [] ;
 // listCreditsOrgin: any = [];
 private checkedAll: false;
 private showParticulier = false;
 private showEntreprise = false;
 private showFilterFournisseur = false;
 private  dataCreditsAchatsSource: MatTableDataSource<CreditAchatsElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
 
 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay :ColumnDefinition[] = [];

 private columnsToSelect : SelectItem[];
  private  selectedColumnsDefinitions: string[];

 private selectedColumnsNames: string[];
 private etatsCreditAchats: any[] = [
  {value: 'Initial', viewValue: 'Initial'},
  {value: 'Valide', viewValue: 'Validé'}
];
private periodesCreditsAchats: any[] = [
  {value: 'Semaine', viewValue: 'Semaine'},
  {value: 'Mois', viewValue: 'Mois'},
  {value: 'Trimestre', viewValue: 'Trimestre'},
  {value: 'Simestre', viewValue: 'Simestre'},
  {value: 'Annee', viewValue: 'Annéé'}
  
  
];
  constructor( private route: ActivatedRoute, private creditsAchatsService: CreditAchatsService,
    private router: Router, private matSnackBar: MatSnackBar,
    public dialog: MatDialog  ) {
  }
  ngOnInit() {
   
    this.columnsToSelect = [
      {label: 'N° Crédit', value: 'numeroCreditAchats', title: 'N°Crédit'},
      {label: 'Date Crédit', value: 'dateCreditAchats', title: 'Date Crédit'},
      {label: 'Montant à Payer', value: 'montantAPayer', title: 'Montant à Payer'},
      {label: 'Montant Touché', value: 'montantTouche', title: 'Montant Touché'},
      {label: 'Montant Resté', value: 'montantReste', title: 'Montant Resté'},
      {label: 'Rs.Soc.Fournisseur', value:  'raisonSociale', title: 'Rs.Soc.Fournisseur' },
      {label: 'Matr.Fournisseur', value:  'matriculeFiscale', title: 'Matr.Fournisseur' },
      {label: 'Reg.Fournisseur', value:  'registreCommerce', title: 'Reg.Fournisseur' },
      {label: 'CIN Fournisseur', value: 'cin', title: 'CIN Fournisseur'},  
      {label: 'Nom Fournisseur', value: 'nom', title: 'Prénom Fournisseur'},
      {label: 'Prénom Fournisseur', value: 'prenom', title: 'Prénom Fournisseur'},
      {label: 'Date Début Pai.', value: 'dateDebutPayement', title: 'Date Début Pai.' },
      {label: 'Date Prochain Pai.', value: 'dateNextPayement', title: 'Date Prochain Pai.' },
      
      {label: 'Nbre Tranches', value: 'nbreTranches', title: 'Nbre Tranches'},
      {label: 'Valeur Tranche', value: 'valeurTranche', title: 'Valeur Tranche'},
      {label: 'Période', value: 'periodTranche', title: 'Période'},
      {label: 'Déscription', value: 'descriptionEchange', title: 'Déscription' },
      {label: 'Etat', value:  'etat', title: 'Etat' }
  
  ];
  this.selectedColumnsDefinitions = [
    'numeroCreditAchats',
    'dateCreditAchats' , 
    'montantAPayer', 
    'raisonSociale' , 
    'etat'
    ];
    this.defaultColumnsDefinitions = [
      {name: 'numeroCreditAchats' , displayTitle: 'N°Crédit'},
      {name: 'dateCreditAchats' , displayTitle: 'Date Crédit'},
       {name: 'nom' , displayTitle: 'Nom Cl.'},
      {name: 'prenom' , displayTitle: 'Prénom Cl.'},
      {name: 'cin' , displayTitle: 'CIN Cl.'},
      {name: 'montantAPayer' , displayTitle: 'Valeur à Payer'},
      {name: 'montantReste' , displayTitle: 'Valeur Resté'},
      {name: 'montantTouche' , displayTitle: 'Valeur Touché'},
      {name: 'matriculeFiscale' , displayTitle: 'Matr. Four.'},
      {name: 'raisonSociale' , displayTitle: 'Raison Four.'},
      {name: 'registreCommerce' , displayTitle: 'Registre Four.'},
      {name: 'periodTranche' , displayTitle: 'Période '},
      {name: 'nbreTranches' , displayTitle: 'Nbre Tranches'},
      {name: 'valeurTranche' , displayTitle: 'Valeur Tranche'},
      {name: 'descriptionEchange' , displayTitle: 'Déscription'},
      {name: 'dateDebutPayement' , displayTitle: 'Date Début Pai.'},
      {name: 'dateNextPayement' , displayTitle: 'Date Prochaine Pai.'},
       {name: 'etat', displayTitle: 'Etat' }
 
       
      ];
    this.creditsAchatsService.getCreditsAchats(0, 5 , null).subscribe((creditsAchats) => {
      this.listCreditsAchats = creditsAchats;
      // this.listClients[this.listClients.length] = this.lastClientAdded;
      // this.listClientsOrgin = this.listClients ;
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });
  

  }
  deleteCreditAchats(creditAchats) {
      console.log('call delete !', creditAchats );
    this.creditsAchatsService.deleteCreditAchats(creditAchats.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData(null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.creditsAchatsService.getCreditsAchats(0, 5, filter).
  subscribe((creditsAchats) => {
    this.listCreditsAchats = creditsAchats;
    this.transformDataToByConsumedByMatTable();
   });
  // this.listClientsOrgin = this.listClients ;
}


checkOneActionInvoked() {
  const listSelectedCreditsAchats: CreditAchats[] = [];
console.log('selected credits are ');
console.log(this.selection.selected);
if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((creditAchatsElement) => {
  return creditAchatsElement.code;
});
this.listCreditsAchats.forEach(creditAchats => {
  if (codes.findIndex(code => code === creditAchats.code) > -1) {
    listSelectedCreditsAchats.push(creditAchats);
  }
  });
}
this.select.emit(listSelectedCreditsAchats);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listCreditsAchats !== undefined) {
    this.listCreditsAchats.forEach(creditAchats => {
      listElements.push( {code: creditAchats.code ,
         numeroCreditAchats: creditAchats.numeroCreditAchats,
         dateCreditAchats: creditAchats.dateOperation, cin: creditAchats.fournisseur.cin, nom: creditAchats.fournisseur.nom,
        prenom: creditAchats.fournisseur.prenom,
        matriculeFiscale: creditAchats.fournisseur.matriculeFiscale, raisonSociale: creditAchats.fournisseur.raisonSociale ,
         responsableCommerciale: creditAchats.fournisseur.responsableCommerciale, montantAPayer: creditAchats.montantAPayer,
        categroyClient: creditAchats.fournisseur.categroyClient, descriptionEchange: creditAchats.descriptionEchange,
        avance: creditAchats.avance, montantReste: creditAchats.montantReste,   periodTranche: creditAchats.periodTranche,
        dateDebutPayement: creditAchats.dateDebutPayement,
        etat: creditAchats.etat === 'Valide' ?   'Validé' : creditAchats.etat === 'Annule' ? 'Annulé' : 'Initial',
        valeurTranche: creditAchats.valeurTranche, nbreTranches: creditAchats.nbreTranches} );
    });
  }
    this.dataCreditsAchatsSource = new MatTableDataSource<CreditAchatsElement>(listElements);
    this.dataCreditsAchatsSource.sort = this.sort;
    this.dataCreditsAchatsSource.paginator = this.paginator;
    
    
}
specifyListColumnsToBeDisplayed() {
  
   console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
   this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
 ?
    this.defaultColumnsDefinitions : [];
    this.selectedColumnsNames = [];
    
    if (this.selectedColumnsDefinitions !== undefined)
     {
       this.selectedColumnsDefinitions.forEach((colName) => { 
            const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
            const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
            const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
            this.columnsDefinitionsToDisplay.push(colDef);
           });
     }
    this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
   this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
              return colDef.displayTitle;
    });
    console.log('columns to disp:', this.columnsDefinitionsToDisplay);
    this.columnsNames = [];
    this.columnsNames[0] = 'select';
    this.columnsDefinitionsToDisplay .map((colDef) => {
     this.columnsNames.push(colDef.name);
    if (this.selectedColumnsDefinitions !== undefined) {
     this.selectedColumnsNames.push(colDef.displayTitle);
    }
 });
    this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
     return colDef.displayTitleAr;
 });
 }
masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataCreditsAchatsSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataCreditsAchatsSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataCreditsAchatsSource.filter = filterValue.trim().toLowerCase();
  if (this.dataCreditsAchatsSource.paginator) {
    this.dataCreditsAchatsSource.paginator.firstPage();
  }
}

loadAddCreditAchatsComponent() {
  this.router.navigate(['/pms/achats/ajouterCreditAchats']) ;

}
loadEditCreditAchatsComponent() {
  this.router.navigate(['/pms/achats/editerCreditAchats', this.selection.selected[ this.selection.selected.length - 1].code]) ;

}
supprimerCreditsAchats()
{
  console.log('NBRE OF ELEMENTS SELECTED');
  console.log(this.selection.selected.length);
  console.log(' ELEMENTS SELECTED');
  console.log(this.selection.selected);
  if ( this.selection.selected.length !== 0) {
      this.selection.selected.forEach(function(creditAchats, index) {
          this.creditsAchatsService.deleteCreditAchats(creditAchats.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.openSnackBar( this.selection.selected.length + ' Crédit(s) Supprimé(s)');
             this.loadData();
            }
      }, this);
    } else {
      this.openSnackBar('Aucun Crédit Séléctionné!');
    }
}
refresh() {
  this.loadData(null);
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

 addPayementCreditAchats()
 {
  if ( this.selection.selected.length !== 0) {
    
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
    top : '0'
  };
  dialogConfig.data = {
    codeCreditAchats : this.selection.selected[0].code
  };

  const dialogRef = this.dialog.open(DialogAddPayementCreditAchatsComponent,
    dialogConfig
  );
} else {
  this.openSnackBar('Aucun Crédit Séléctionné!');
}
 }
 history()
 {
   
 }
 ajournerCreditAchats()
 {
  if ( this.selection.selected.length !== 0) {
    
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
    top : '0'
  };
  dialogConfig.data = {
    codeCreditAchats : this.selection.selected[0].code
  };

  const dialogRef = this.dialog.open(DialogAddAjournementCreditAchatsComponent,
    dialogConfig
  );
} else {
  this.openSnackBar('Aucun Crédit Séléctionné!');
}
 }
 addFournisseur()
 {
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
    top : '0'
  };
  const dialogRef = this.dialog.open(DialogAddFournisseurComponent,
    dialogConfig
  );
 }
 attacherDocumentCreditAchats()
 {
  if ( this.selection.selected.length !== 0) {
    
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
    top : '0'
  };
  dialogConfig.data = {
    codeCreditAchats : this.selection.selected[0].code
  };

  const dialogRef = this.dialog.open(DialogAddDocumentCreditAchatsComponent,
    dialogConfig
  );
} else {
  this.openSnackBar('Aucun Crédit Séléctionné');
}
 }
 
  addTrancheCreditAchats()
  {
   if ( this.selection.selected.length !== 0) {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeCreditAchats : this.selection.selected[0].code
   };
 
   const dialogRef = this.dialog.open(DialogAddTrancheCreditAchatsComponent,
     dialogConfig
   );
 } else {
   this.openSnackBar('Aucun Crédit Séléctionné!');
 }
 
 }
 showStatistiquesDialog()
 {
 /* if ( this.selection.selected.length !== 0) {*/
    
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
    top : '0'
  };
 /* dialogConfig.data = {
    codeCreditAchats : this.selection.selected[0].code
  };*/

  const dialogRef = this.dialog.open(DialogDisplayStatistiquesCreditsAchatsComponent,
    dialogConfig
  );
/*} else {
  this.openSnackBar('Aucun Crédit Séléctionné!');
}*/
 }

 changePage($event) {
  console.log('page event is ', $event);
 this.creditsAchatsService.getCreditsAchats($event.pageIndex, $event.pageSize, this.creditAchatsDataFilter ).subscribe((creditsAchats) => {
    this.listCreditsAchats = creditsAchats;
    console.log(this.listCreditsAchats);
   // this.transformDataToByConsumedByMatTable();
   const listElements = [];
     console.log('diffe of undefined');
     this.listCreditsAchats.forEach(credit => {
            listElements.push( {code: credit.code ,
             numeroCreditAchats: credit.numeroCreditAchats,
             dateCreditAchats: credit.dateOperation,
          cin: credit.client.cin, nom: credit.client.nom, prenom: credit.client.prenom,
         matriculeFiscale: credit.client.matriculeFiscale, raisonSociale: credit.client.raisonSociale,
          montantAPayer: credit.montantAPayer,
          descriptionEchange: credit.descriptionEchange,
         avance: credit.avance, montantReste: credit.montantReste,   periodTranche: credit.periodTranche,
         dateDebutPayement: credit.dateDebutPayement, valeurTranche: credit.valeurTranche, 
         nbreTranches: credit.nbreTranches,
         etat: credit.etat === 'Valide' ?   'Validé' : credit.etat === 'Annule' ? 'Annulé' : 'Initial'
 
       } );
     });
    // const pagin = this.dataCreditsSource.paginator;
    // const sor = this.dataCreditsSource.sort;
     this.dataCreditsAchatsSource = new MatTableDataSource<CreditAchatsElement>(listElements);
     console.log('creditSource');
     console.log(this.dataCreditsAchatsSource );
     // this.dataCreditsSource.sort = sor;
     // this.dataCreditsSource.paginator = pagin;
     this.creditsAchatsService.getTotalCreditsAchats(this.creditAchatsDataFilter).subscribe((response) => {
      console.log('Total credits is ', response);
       this.dataCreditsAchatsSource.paginator.length = response.totalCreditsAchats;
     });

    });
}
toggleFilterPanel() {
  this.showFilter = !this.showFilter;
}
toggleShowColumnsMultiSelect() {
 this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
}
loadFiltredData() {
 console.log('the filter is ', this.creditAchatsDataFilter);
 this.loadData(this.creditAchatsDataFilter);
}
activateFilterFournisseur()
{
  this.showFilterFournisseur = !this.showFilterFournisseur ;
  
}
}







