
import { Component, OnInit, Inject, ViewChild, Input, AfterViewChecked, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {CreditAchatsService} from './credit-achats.service';
import { CreditAchats } from './credit-achats.model';
import {PayementCreditAchats} from './payement-credit-achats.model';
import { PayementCreditAchatsElement } from './payement-credit-achats.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DialogAddPayementCreditAchatsComponent } from './popups/dialog-add-payement-credit-achats.component';
import { DialogEditPayementCreditAchatsComponent } from './popups/dialog-edit-payement-credit-achats.component';
@Component({
    selector: 'app-payements-credits-achats-grid',
    templateUrl: 'payements-credits-achats-grid.component.html',
  })
  export class PayementsCreditsAchatsGridComponent implements OnInit, OnChanges {
     private listPayementsCreditsAchats: PayementCreditAchats[] = [];
     private  dataPayementsCreditsAchatsSource: MatTableDataSource<PayementCreditAchatsElement>;
     private selection = new SelectionModel<PayementCreditAchatsElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];

     @Input()
     private creditAchats: CreditAchats;

     constructor(  private creditAchatsService: CreditAchatsService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.creditAchatsService.getPayementsCreditsAchatsByCodeCredit(this.creditAchats.code).subscribe((listPayementsCreditsA) => {
        this.listPayementsCreditsAchats = listPayementsCreditsA;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }

  ngOnChanges() {
    this.creditAchatsService.getPayementsCreditsAchatsByCodeCredit(this.creditAchats.code).subscribe((listPayementsCreditsA) => {
      this.listPayementsCreditsAchats = listPayementsCreditsA;
// console.log('recieving data from backend', this.listPayementsCredits  );
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }
  checkOneActionInvoked() {
    const listSelectedPayementsCreditsAchats: PayementCreditAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((payementCreditAElement) => {
    return payementCreditAElement.code;
  });
  this.listPayementsCreditsAchats.forEach(payementCreditA => {
    if (codes.findIndex(code => code === payementCreditA.code) > -1) {
      listSelectedPayementsCreditsAchats.push(payementCreditA);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listPayementsCreditsAchats !== undefined) {

      this.listPayementsCreditsAchats.forEach(payementCreditAc => {
             listElements.push( {code: payementCreditAc.code ,
          dateOperation: payementCreditAc.dateOperation,
          montantPaye: payementCreditAc.montantPaye,
          modePayement: payementCreditAc.modePayement,
          numeroCheque: payementCreditAc.numeroCheque,
          dateCheque: payementCreditAc.dateCheque,
          compte: payementCreditAc.compte,
          compteAdversaire: payementCreditAc.compteAdversaire,
          banque: payementCreditAc.banque,
          banqueAdversaire: payementCreditAc.banqueAdversaire,
          description: payementCreditAc.description
        } );
      });
    }
      this.dataPayementsCreditsAchatsSource = new MatTableDataSource<PayementCreditAchatsElement>(listElements);
      this.dataPayementsCreditsAchatsSource.sort = this.sort;
      this.dataPayementsCreditsAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'}, 
     {name: 'montantPaye' , displayTitle: 'Montant Payé'},
     {name: 'modePayement' , displayTitle: 'Mode Payement'},
      {name: 'numeroCheque' , displayTitle: 'N°Chèque'},
     {name: 'dateCheque' , displayTitle: 'Date Chèque'}, 
     {name: 'compte' , displayTitle: 'Compte'},
     {name: 'compteAdversaire' , displayTitle: 'Compte Adv.'},
      {name: 'banque' , displayTitle: 'Banque'},
       { name : 'banqueAdversaire' , displayTitle : 'Banque Adv.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataPayementsCreditsAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataPayementsCreditsAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataPayementsCreditsAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataPayementsCreditsAchatsSource.paginator) {
      this.dataPayementsCreditsAchatsSource.paginator.firstPage();
    }
  }

  showAddPayementDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeCreditAchats : this.creditAchats.code
   };
 
   const dialogRef = this.dialog.open(DialogAddPayementCreditAchatsComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(payAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditPayementDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codePayementCreditAchats : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditPayementCreditAchatsComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(payAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Paiement Séléctionnée!');
    }
    }
    supprimerPayements()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(pay, index) {
              this.creditAchatsService.deletePayementCreditAchats(pay.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( this.selection.selected.length + ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Paiement Séléctionnée!');
        }
    }
    refresh() {
      this.creditAchatsService.getPayementsCreditsAchatsByCodeCredit(this.creditAchats.code).subscribe((listPayementsCreditsAc) => {
        this.listPayementsCreditsAchats = listPayementsCreditsAc;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
















}
