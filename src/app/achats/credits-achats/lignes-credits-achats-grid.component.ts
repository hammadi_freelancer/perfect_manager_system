
import { Component, OnInit, Inject, ViewChild, Input, AfterViewChecked, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {CreditAchatsService} from './credit-achats.service';
import { CreditAchats } from './credit-achats.model';
import {LigneCreditAchats} from './ligne-credit-achats.model';
import { LigneCreditAchatsElement } from './ligne-credit-achats.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DialogAddLigneCreditAchatsComponent } from './popups/dialog-add-ligne-credit-achats.component';
import { DialogEditLigneCreditAchatsComponent } from './popups/dialog-edit-ligne-credit-achats.component';
@Component({
    selector: 'app-lignes-credits-achats-grid',
    templateUrl: 'lignes-credits-achats-grid.component.html',
  })
  export class LignesCreditsAchatsGridComponent implements OnInit, OnChanges {
     private listLignesCreditsAchats: LigneCreditAchats[] = [];
     private  dataLignesCreditsAchatsSource: MatTableDataSource<LigneCreditAchatsElement>;
     private selection = new SelectionModel<LigneCreditAchatsElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];

     @Input()
     private creditAchats: CreditAchats;

     constructor(  private creditAchatsService: CreditAchatsService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
     //  console.log('credit code after dipslay tab ', this.credit.code);
      this.creditAchatsService.getLignesCreditsAchatsByCodeCredit(this.creditAchats.code).subscribe((listLignesCreditsAch) => {
        this.listLignesCreditsAchats = listLignesCreditsAch;
// console.log('recieving data from backend', this.listPayementsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }

  ngOnChanges() {
    this.creditAchatsService.getLignesCreditsAchatsByCodeCredit(this.creditAchats.code).subscribe((listLignesCreditsAch) => {
      this.listLignesCreditsAchats = listLignesCreditsAch;
//console.log('recieving data from backend', this.listPayementsCredits  );
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }
  checkOneActionInvoked() {
    const listSelectedLignesCreditsAchats: LigneCreditAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((ligneCreditAElement) => {
    return ligneCreditAElement.code;
  });
  this.listLignesCreditsAchats.forEach(ligneCreditAc => {
    if (codes.findIndex(code => code === ligneCreditAc.code) > -1) {
      listSelectedLignesCreditsAchats.push(ligneCreditAc);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listLignesCreditsAchats !== undefined) {

      this.listLignesCreditsAchats.forEach(ligneCreditAch => {
             listElements.push( {code: ligneCreditAch.code ,
          dateOperation: ligneCreditAch.dateOperation,
          montantAPaye: ligneCreditAch.montantAPayer,
          description: ligneCreditAch.description,
          etat: ligneCreditAch.etat
        } );
      });
    }
      this.dataLignesCreditsAchatsSource = new MatTableDataSource<LigneCreditAchatsElement>(listElements);
      this.dataLignesCreditsAchatsSource.sort = this.sort;
      this.dataLignesCreditsAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'},
     {name: 'montantAPaye' , displayTitle: 'Montant A Payer'},
       { name : 'description' , displayTitle : 'Déscription'},
       { name: 'etat', displayTitle: 'Etat'}
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataLignesCreditsAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataLignesCreditsAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataLignesCreditsAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataLignesCreditsAchatsSource.paginator) {
      this.dataLignesCreditsAchatsSource.paginator.firstPage();
    }
  }

  showAddLigneDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeCreditAchats : this.creditAchats.code
   };
 
   const dialogRef = this.dialog.open(DialogAddLigneCreditAchatsComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(lAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditLigneDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeLigneCreditAchats : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditLigneCreditAchatsComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(lAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucune Ligne Séléctionnée!');
    }
    }
    supprimerLignes()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(pay, index) {
              this.creditAchatsService.deleteLigneCreditAchats(pay.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( this.selection.selected.length + ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucune Ligne Séléctionnée!');
        }
    }
    refresh() {
      this.creditAchatsService.getLignesCreditsAchatsByCodeCredit(this.creditAchats.code).subscribe((listLignesCreditsA) => {
        this.listLignesCreditsAchats = listLignesCreditsA;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
















}
