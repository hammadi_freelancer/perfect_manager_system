import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,
  AfterViewInit , AfterContentInit,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {CreditAchats} from './credit-achats.model';
import {TrancheCreditAchats} from './tranche-credit-achats.model';
import { MODES_GENERATION_TRANCHES} from './credit-achats.model';

import {CreditAchatsService} from './credit-achats.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import {ActionData} from './action-data.interface';
// import { CommunicationService} from './communication.service';
import { MatDatepickerInput } from '@angular/material/datepicker';
import { MatSnackBar } from '@angular/material';
import { CreditAchatsFilter } from './credit-achats.filter';
import {Fournisseur} from '../fournisseurs/fournisseur.model';
import { MatDatepicker } from '@angular/material/datepicker';
// import { DialogAddPayementCreditComponent } from './dialog-add-payement-credit.component';
// import { DialogListPayementsCreditsComponent} from './dialog-list-payements-credits.component ';
// import { DialogListAjournementsCreditsComponent} from './dialog-list-ajournements-credits.component';
// import { DialogListTranchesCreditsComponent } from './dialog-list-tranches-credits.component';
// import { DialogListDocumentsCreditsComponent } from './dialog-list-documents-credits.component';
// import { DialogAddTrancheCreditComponent } from './dialog-add-tranche-credit.component';
// import { DialogAddAjournementCreditComponent } from './dialog-add-ajournement-credit.component';
// import { DialogAddDocumentCreditComponent } from './dialog-add-document-credit.component';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-credit-achats-edit',
  templateUrl: './credit-achats-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class CreditAchatsEditComponent implements OnInit  {

  private creditAchats: CreditAchats =  CreditAchats.constructDefaultInstance();
  private totalPayements = 0;
  private totalReste  = 0;
@Input()
private listCreditsAchats: any = [] ;


private creditAchatsFilter: CreditAchatsFilter;
private listFournisseurs: Fournisseur[];
private formControlDate = new FormControl();
@ViewChild('DateOper') dateOper: ElementRef;
private updateEnCours = false;
private tabsDisabled = true;
private etatsCreditsAchats: any[] = [
  {value: 'Annule', viewValue: 'Annulé'},
  {value: 'Initial', viewValue: 'Initial'},
  {value: 'Valide', viewValue: 'Validé'}
];
private periodesCreditsAchats: any[] = [
  {value: 'Semaine', viewValue: 'Semaine'},
  {value: 'Mois', viewValue: 'Mois'},
  {value: 'Trimestre', viewValue: 'Trimestre'},
  {value: 'Simestre', viewValue: 'Simestre'},
  {value: 'Annee', viewValue: 'Annéé'}
  
  
];
  constructor( private route: ActivatedRoute,
    private router: Router, private creditAchatsService: CreditAchatsService, private matSnackBar: MatSnackBar,
    public dialog: MatDialog
   ) {
  }
  ngOnInit() {
    this.listFournisseurs = this.route.snapshot.data.fournisseurs;
    this.creditAchatsFilter = new CreditAchatsFilter(this.listFournisseurs);
    const codeCreditAchats = this.route.snapshot.paramMap.get('codeCreditAchats');
    this.creditAchatsService.getCreditAchats(codeCreditAchats).subscribe((creditAc) => {
             this.creditAchats = creditAc;
             if (this.creditAchats.etat === 'Valide') {
                this.tabsDisabled = false;
             }
             if (this.creditAchats.etat === 'Valide' || this.creditAchats.etat === 'Annule') {
                this.etatsCreditsAchats.splice(1, 1);
              }
              if (this.creditAchats.etat === 'Initial') {
                  this.etatsCreditsAchats.splice(0, 1);
                }

              });
  }
 
  showDate($event)
  {
    console.log($event);
  }
  loadAddCreditAchatsComponent() {
    this.router.navigate(['/pms/achats/ajouterCreditAchats']) ;
  }
  loadGridCreditsAchats() {
    this.router.navigate(['/pms/achats/creditsAchats']) ;
  }
  supprimerCredit() {
      this.creditAchatsService.deleteCreditAchats(this.creditAchats.code).subscribe((response) => {

        this.openSnackBar( ' Crédit Supprimé');
        this.loadGridCreditsAchats() ;
            });
  }
updateCreditAchats() {
  let noFournisseur = false;
  let noMontant = false;
  if (this.creditAchats.etat === 'Valide' ) {
    if ( this.creditAchats.fournisseur.cin === null ||  this.creditAchats.fournisseur.cin === undefined)  {
      noFournisseur = true;
        
      }

  }
  if (this.creditAchats.montantAPayer === 0 || this.creditAchats.montantAPayer === undefined ) {
      noMontant = true;
    }
    if (noFournisseur)
      {
        this.openSnackBar( ' Vous Venez de valider le crédit sans préciser les informations de fournisseur! ');
        this.creditAchats.etat = 'Initial';
      } else if (noMontant)  {
          this.openSnackBar( 'Un Crédit Crée sans un Montant bien Précis !');
          
        } else {
     this.updateEnCours = true;          
  this.creditAchatsService.updateCreditAchats(this.creditAchats).subscribe((response) => {
    this.updateEnCours = false; 
    if (response.error_message ===  undefined) {
      if (this.creditAchats.etat === 'Valide')
        {
          this.tabsDisabled = false;
          this.etatsCreditsAchats = [
            {value: 'Annule', viewValue: 'Annulé'},
            {value: 'Valide', viewValue: 'Validé'}
  
          ];
        }
        if (this.creditAchats.etat === 'Annule')
          {
            this.tabsDisabled = true;
         
          }
      console.log('the response of update is ', response );
      this.openSnackBar( ' Crédit mis à jour ');

      //  this.save.emit(response);
      // this.client = new Client();
      // this.modeUpdate = false;
    } else {
      this.openSnackBar( ' Erreurs:Opération Echouée ');
     // show error message
    }
});
        }
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

 /*listerTranchesCredits()
 {
  if ( this.credit.etat === 'Valide') {
    const dialogConfig = new MatDialogConfig();
    
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.position = {
      top : '90'
    };
    dialogConfig.data = {
      codeCredit : this.credit.code
    };
   
    const dialogRef = this.dialog.open(DialogListTranchesCreditsComponent,
      dialogConfig
    );
   } else {
    this.openSnackBar('Le Crédit n\'est pas encore validé ');
   }
 }
 addTrancheCredit()
 {
  if ( this.credit.etat === 'Valide') {
    const dialogConfig = new MatDialogConfig();
    
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.position = {
      top : '0'
    };
    dialogConfig.data = {
      codeCredit : this.credit.code
    };
   
    const dialogRef = this.dialog.open(DialogAddTrancheCreditComponent,
      dialogConfig
    );
   } else {
    this.openSnackBar('Le Crédit de Vente  n\'est pas encore validé ');
   }
 }

listerPayementsCredit()
{
  if ( this.credit.etat === 'Valide') {
    const dialogConfig = new MatDialogConfig();
    
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.position = {
      top : '90'
    };
    dialogConfig.data = {
      codeCredit : this.credit.code
    };
   
    const dialogRef = this.dialog.open(DialogListPayementsCreditsComponent,
      dialogConfig
    );
   } else {
    this.openSnackBar('Le Crédit n\'est pas encore validé ');
   }
}
listerDocumentsCredit()
{
  if ( this.credit.etat === 'Valide') {
    const dialogConfig = new MatDialogConfig();
    
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.position = {
      top : '90'
    };
    dialogConfig.data = {
      codeCredit : this.credit.code
    };
   
    const dialogRef = this.dialog.open(DialogListDocumentsCreditsComponent,
      dialogConfig
    );
   } else {
    this.openSnackBar('Le Crédit n\'est pas encore validé ');
   }
}
attacherDocumentCredit()
{
 if ( this.credit.etat === 'Valide') {
 const dialogConfig = new MatDialogConfig();
 
 dialogConfig.disableClose = false;
 dialogConfig.autoFocus = true;
 dialogConfig.position = {
   top : '0'
 };
 dialogConfig.data = {
   codeCredit : this.credit.code
 };

 const dialogRef = this.dialog.open(DialogAddDocumentCreditComponent,
   dialogConfig
 );
} else {
 this.openSnackBar('Le Crédit n\'est pas encore validé ');
}
}
listerAjournementsCredit()
{
  if ( this.credit.etat === 'Valide') {
    const dialogConfig = new MatDialogConfig();
    
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.position = {
      top : '90'
    };
    dialogConfig.data = {
      codeCredit : this.credit.code
    };
   
    const dialogRef = this.dialog.open(DialogListAjournementsCreditsComponent,
      dialogConfig
    );
   } else {
    this.openSnackBar('Le Crédit n\'est pas encore validé ');
   }
}


addPayementCredit()
{
 if ( this.credit.etat === 'Valide') {
 const dialogConfig = new MatDialogConfig();
 
 dialogConfig.disableClose = false;
 dialogConfig.autoFocus = true;
 dialogConfig.position = {
   top : '0'
 };
 dialogConfig.data = {
   codeCredit : this.credit.code
 };

 const dialogRef = this.dialog.open(DialogAddPayementCreditComponent,
   dialogConfig
 );
} else {
 this.openSnackBar('Le Crédit n\'est pas encore validé ');
}
}
ajournerCredit()
{
 if ( this.credit.etat === 'Valide') {
 const dialogConfig = new MatDialogConfig();
 
 dialogConfig.disableClose = false;
 dialogConfig.autoFocus = true;
 dialogConfig.position = {
   top : '0'
 };
 dialogConfig.data = {
   codeCredit : this.credit.code
 };

 const dialogRef = this.dialog.open(DialogAddAjournementCreditComponent,
   dialogConfig
 );
} else {
 this.openSnackBar('Le Crédit n\'est pas encore validé ');
}
}*/
history()
{

}
fillDataNomPrenom() {
  const listFiltred  = this.listFournisseurs.filter((fournisseur) => (fournisseur.cin === this.creditAchats.fournisseur.cin));
  this.creditAchats.fournisseur.nom = listFiltred[0].nom;
  this.creditAchats.fournisseur.prenom = listFiltred[0].prenom;
}
fillDataMatriculeRaison()
{
  const listFiltred  = this.listFournisseurs.filter((fournisseur) => 
  (fournisseur.registreCommerce === this.creditAchats.fournisseur.registreCommerce));
  this.creditAchats.fournisseur.matriculeFiscale = listFiltred[0].matriculeFiscale;
  this.creditAchats.fournisseur.raisonSociale = listFiltred[0].raisonSociale;
}
selectedTabChanged($event) {
  if ($event.index === 4) { // Composition tab was selected
   // this.loadListArticles();
   } else if ($event.index === 1) {
    //  this.loadListSchemasConfiguration() ;
   }
  }
}
