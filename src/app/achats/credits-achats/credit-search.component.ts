import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,
    
      ViewChild } from '@angular/core';
    // import { ArticleService } from './article.service';
    import {CreditAchats} from './credit-achats.model';
   //  import { MODES_GENERATION_TRANCHES} from './credit-achats.model';
    import {CreditAchatsService} from './credit-achats.service';
    import {FormControl} from '@angular/forms';
    import { MatExpansionPanel } from '@angular/material/expansion';
    import {map, startWith} from 'rxjs/operators';
    import {Observable} from 'rxjs';
    import { Router, ActivatedRoute, ParamMap } from '@angular/router';
    import {ActionData} from './action-data.interface';
    import { ClientService } from '../../ventes/clients/client.service';
    import { Category} from '../../common/category.interface';
    import { CreditAchatsFilter } from './credit-achats.filter';
    import { Client } from '../../ventes/clients/client.model';
    
    @Component({
        selector: 'app-credit-achats-search',
        templateUrl: './credit-achats-search.component.html',
        // styleUrls: ['./players.component.css']
      })
 export class CreditAchatsSearchComponent implements OnInit {

    @Input()
    private actionData: ActionData;

    @Input()
    private listClients: Client[];
    private categorySelected = 'N/A';
    private homeMessage = 'Here I will display the list of : last credits updated' +
    'last credits created ; last credits ...';
    private categories: Category[] = [{value: 'UNKNOWN', viewValue : 'N/A'},
        {value: 'PERSONNE_MORALE', viewValue: 'Entreprise'}, {value: 'PERSONNE_PHYSIQUE', viewValue : 'Client'}
      
      ];
      // private panelPersonnePhysique: MatExpansionPanel ;
      // private panelPersonneMorale: MatExpansionPanel;
      panelOpenState = false;
      private creditAchatsFilter: CreditAchatsFilter;
      // private creditFilterValues: CreditFilter;
      
        constructor( private route: ActivatedRoute,
            private router: Router, private creditAchatsService: CreditAchatsService, private clientService: ClientService) {
         
              
         
            }
          ngOnInit() {
            this.creditAchatsFilter = new CreditAchatsFilter(this.listClients);
           // this.clientService.getClients().subscribe((clients) => {
            //  this.creditFilter = new CreditFilter(clients);
         // });
          }
          show() {
            console.log('the catge selected is :');
            console.log(this.categorySelected);
            
           /* if ( this.panelPersonnePhysique !== undefined) {
            this.panelPersonnePhysique.close();
            }
            if ( this.panelPersonneMorale !== undefined) {
           this.panelPersonneMorale.close();
            }*/
          }

        }
    

