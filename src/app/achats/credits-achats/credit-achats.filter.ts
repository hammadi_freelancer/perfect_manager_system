
import { BaseEntity } from '../../common/base-entity.model' ;
import { Fournisseur } from '../fournisseurs/fournisseur.model' ;
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

export const MODES_GENERATION_TRANCHES = ['AUTOMATIQUE', 'MANUELLE', ];

export class CreditAchatsFilter  {
    public selectedCin: string;
    public selectedNom: string;
    public selectedPrenom: string;
    public selectedRaisonSociale: string;
    public selectedMatriculeFiscale: string;
    
    public selectedRegistreCommerce: string;
    public selectedAdresse: string;
    private controlNoms = new FormControl();
    private controlPrenoms = new FormControl();
    private controlCins = new FormControl();
    private controlRaisonSociale = new FormControl();
    private controlMatriculeFiscale = new FormControl();
    private controlRegistreCommerce = new FormControl();
    
    private controlAdresse = new FormControl();
    private listNoms: string[] = [];
    private listPrenoms: string[] = [];
    private listCins: string[] = [];
    private listRaisonsSociales: string[] = [];
    private listMatriculesFiscales: string[] = [];
    private listAdresses: string[] = [];
    private listRegistreCommerce: string [] = [];
    private filteredNoms: Observable<string[]>;
    private filteredPrenoms: Observable<string[]>;
    private filteredCins: Observable<string[]>;
    private filteredAdresses: Observable<string[]>;
    private filteredRaisonsSociales: Observable<string[]>;
    private filteredMatriculesFiscales: Observable<string[]>;
    private filteredRegistresCommerces: Observable<string[]>;
    
    constructor (fournisseurs: Fournisseur[]) {
      if (fournisseurs !== null) {
       //  const listClientsMorals = clients.filter((client) => (client.categoryClient === 'PERSONNE_MORALE'));
        // const listClientsPhysiques = clients.filter((client) => (client.categoryClient === 'PERSONNE_PHYSIQUE'));
        this.listCins = fournisseurs.filter((fournisseur) =>
        (fournisseur.cin !== undefined && fournisseur.cin != null )).map((fournisseur) => (fournisseur.cin ));
         this.listNoms = fournisseurs.map((fournisseur) => (fournisseur.nom));
        this.listPrenoms = fournisseurs.map((fournisseur) => (fournisseur.prenom));
         this.listRaisonsSociales = fournisseurs.map((fournisseur) => (fournisseur.raisonSociale));
         this.listMatriculesFiscales = fournisseurs.map((fournisseur) => (fournisseur.matriculeFiscale));
         this.listRegistreCommerce = fournisseurs.filter((fournisseur) =>
         (fournisseur.registreCommerce !== undefined && fournisseur.registreCommerce != null )).map(
           (fournisseur) => (fournisseur.registreCommerce ));
      this.filteredNoms = this.controlNoms.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterNoms(value))
        );
        this.filteredPrenoms = this.controlPrenoms.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterPrenoms(value))
        );
        this.filteredCins = this.controlCins.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterCins(value))
        );
        this.filteredMatriculesFiscales = this.controlMatriculeFiscale.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterMatriculesFiscales(value))
        );
        this.filteredRaisonsSociales = this.controlRaisonSociale.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterRaisonsSociales(value))
        );
        this.filteredRegistresCommerces = this.controlRegistreCommerce.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterRegistresCommerces(value))
        );
        this.filteredAdresses = this.controlAdresse.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterAdresses(value))
        );
    //  }
    }
  }
    private _filterNoms(value: string): string[] {
        const filterValue = value.toLowerCase();
        return this.listNoms.filter(nom => nom.toLowerCase().includes(filterValue));
      }
      private _filterCins(value: string): string[] {
        const filterValue = value.toLowerCase();
        return this.listCins.filter(cin => cin.toLowerCase().includes(filterValue));
      }
      private _filterPrenoms(value: string): string[] {
        const filterValue = value.toLowerCase();
        return this.listPrenoms.filter(prenom => prenom.toLowerCase().includes(filterValue));
      }
      private _filterRaisonsSociales(value: string): string[] {
        const filterValue = value.toLowerCase();
        return this.listRaisonsSociales.filter(raison => raison.toLowerCase().includes(filterValue));
      }
      private _filterMatriculesFiscales(value: string): string[] {
        const filterValue = value.toLowerCase();
        return this.listMatriculesFiscales.filter(matricule => matricule.toLowerCase().includes(filterValue));
      }
      private _filterAdresses(value: string): string[] {
        const filterValue = value.toLowerCase();
        return this.listAdresses.filter(adresse => adresse.toLowerCase().includes(filterValue));
      }
      private _filterRegistresCommerces(value: string): string[] {
        const filterValue = value.toLowerCase();
        // if (this.listRegistreCommerce !== undefined && this.listRegistreCommerce.length !== 0
        // && this.listRegistreCommerce[0] !== undefined) {
        return this.listRegistreCommerce.filter((registre) => {
          if (registre !== undefined ) {
            return  registre.toLowerCase().includes(filterValue) ;
        } else  {
          return true;
        }
      });
         //  }
      }
      public getControlPrenoms(): FormControl {
        return this.controlPrenoms;
      }
      public getControlNoms(): FormControl {
        return this.controlNoms;
      }
      public getControlCins(): FormControl {
        return this.controlCins;
      }
      public getControlAdresses(): FormControl {
        return this.controlAdresse;
      }
      public getControlRaisonSociale(): FormControl {
        return this.controlRaisonSociale;
      }
      public getControlMatriculeFiscale(): FormControl {
        return this.controlMatriculeFiscale;
      }
      public getControlRegistreCommerce(): FormControl {
        return this.controlRegistreCommerce;
      }
      public getFiltredPrenoms(): Observable<string[]> {
        return this.filteredPrenoms;
      }
      public getFiltredNoms(): Observable<string[]> {
        return this.filteredNoms;
      }
      public getFiltredCins(): Observable<string[]> {
        return this.filteredCins;
      }
      public getFiltredRaisonsSociales(): Observable<string[]> {
        return this.filteredRaisonsSociales;
      }
      public getFiltredMatriculesFiscales(): Observable<string[]> {
        return this.filteredMatriculesFiscales;
      }
      public getFiltredAdresses(): Observable<string[]> {
        return this.filteredAdresses;
      }
      public getFiltredRegistreCommerce(): Observable<string[]> {

        return this.filteredRegistresCommerces;
      }
    /*  public getSelectedCin(): string {
        return this.selectedCin;
      }
      public getSelectedNom(): string {
        return this.selectedNom;
      }
      public getSelectedPrenom(): string {
        return this.selectedPrenom;
      }

      public getSelectedRaisonSociale(): string {
        return this.selectedRaisonSociale;
      }
      public getSelectedMatriculeFiscale(): string {
        return this.selectedMatriculeFiscale;
      }*/


}





