
import { BaseEntity } from '../../common/base-entity.model' ;

type Etat = 'Initial' | 'Valide' | 'Annule' 
type ModePayement = 'Espece' | 'Cheque' | 'Virement'

export class PayementCreditAchats extends BaseEntity {
    constructor(
        public code?: string,
         public codeCreditAchats?: string,
         public montantPaye?: number,
        public dateOperationObject?: Date,
        public modePayement?: ModePayement,
        public numeroCheque?: string,
        public dateChequeObject?: Date,
        public dateCheque?: string,
        public dateOperation?: string,
        
        public compte?: string,
        public compteAdversaire?: string,
        public banque?: string,
        public banqueAdversaire?: string,
     ) {
         super();
        
       }
  
}
