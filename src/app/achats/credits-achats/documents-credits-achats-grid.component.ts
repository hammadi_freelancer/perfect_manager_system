
import { Component, OnInit, Inject,Input, ViewChild, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {CreditAchatsService} from './credit-achats.service';
import {CreditAchats} from './credit-achats.model';

import {DocumentCreditAchats} from './document-credit-achats.model';
import { DocumentCreditAchatsElement } from './document-credit-achats.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogAddDocumentCreditAchatsComponent } from './popups/dialog-add-document-credit-achats.component';
import { DialogEditDocumentCreditAchatsComponent } from './popups/dialog-edit-document-credit-achats.component';
@Component({
    selector: 'app-documents-credits-achats-grid',
    templateUrl: 'documents-credits-achats-grid.component.html',
  })
  export class DocumentsCreditsAchatsGridComponent implements OnInit, OnChanges {
     private listDocumentsCreditsAchats: DocumentCreditAchats[] = [];
     private  dataDocumentsCreditsAchatsSource: MatTableDataSource<DocumentCreditAchatsElement>;
     private selection = new SelectionModel<DocumentCreditAchatsElement>(true, []);
    
     @Input()
     private creditAchats : CreditAchats;

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private creditAchatsService: CreditAchatsService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.creditAchatsService.getDocumentsCreditsAchatsByCodeCredit(this.creditAchats.code).subscribe((listDocumentsCreditsA) => {
        this.listDocumentsCreditsAchats = listDocumentsCreditsA;
  console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsCreditsAchats  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
  }
  ngOnChanges()
  {
    this.specifyListColumnsToBeDisplayed();
    
    this.creditAchatsService.getDocumentsCreditsAchatsByCodeCredit(this.creditAchats.code).subscribe((listDocumentsCredits) => {
      this.listDocumentsCreditsAchats = listDocumentsCredits;
// console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsCredits  );
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }

  checkOneActionInvoked() {
    const listSelectedDocumentsCreditsAchats: DocumentCreditAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentCreditAElement) => {
    return documentCreditAElement.code;
  });
  this.listDocumentsCreditsAchats.forEach(documentCreditAchats => {
    if (codes.findIndex(code => code === documentCreditAchats.code) > -1) {
      listSelectedDocumentsCreditsAchats.push(documentCreditAchats);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsCreditsAchats !== undefined) {

      this.listDocumentsCreditsAchats.forEach(docCreditA => {
             listElements.push( {code: docCreditA.code ,
          dateReception: docCreditA.dateReception,
          numeroDocument: docCreditA.numeroDocument,
          typeDocument: docCreditA.typeDocument,
          description: docCreditA.description
        } );
      });
    }
      this.dataDocumentsCreditsAchatsSource = new MatTableDataSource<DocumentCreditAchatsElement>(listElements);
      this.dataDocumentsCreditsAchatsSource.sort = this.sort;
      this.dataDocumentsCreditsAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     console.log('calling specifyListColumnsToBeDisplayed documents grid');
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
    
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
      console.log(this.columnsNames);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsCreditsAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsCreditsAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsCreditsAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsCreditsAchatsSource.paginator) {
      this.dataDocumentsCreditsAchatsSource.paginator.firstPage();
    }
  }
  showAddDocumentDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeCreditAchats : this.creditAchats.code
   };
 
   const dialogRef = this.dialog.open(DialogAddDocumentCreditAchatsComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(docAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditDocumentDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeDocumentCreditAchats : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditDocumentCreditAchatsComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(docAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Document Séléctionnée!');
    }
    }
    supprimerDocuments()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(doc, index) {
              this.creditAchatsService.deleteDocumentCreditAchats(doc.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( this.selection.selected.length + ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Document Séléctionnée!');
        }
    }
    refresh() {
      this.creditAchatsService.getDocumentsCreditsAchatsByCodeCredit(this.creditAchats.code).subscribe((listDocsCreditsAchats) => {
        this.listDocumentsCreditsAchats = listDocsCreditsAchats;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
