export interface CreditAchatsElement {
         code: string ;
        numeroCreditAchats: string;
         dateCreditAchats: string ;
         cin: string ;
         nom: string;
         prenom: string;
         matriculeFiscale: string;
         responsableCommerciale: string;
         raisonSociale: string;
         registreCommerce: string;
         
         categoryClient: string;
         descriptionEchange: string;
         montantAPayer: number;
         avance: number;
          montantReste?: number;
           montantTouche?: number;
          
          periodTranche?: string;
          dateDebutPayement?: string;
         valeurTranche?: number;
         nbreTranches?: number ;
         etat: string;
        // public modeGenerationTranches = MODES_GENERATION_TRANCHES[1],
         // public listTranches?: Tranche[]
        }
