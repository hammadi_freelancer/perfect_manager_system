import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { MagasinService } from '../stocks/magasins/magasin.service';
import { Magasin } from '../stocks/magasins/magasin.model';

@Injectable()
export class MagasinsResolver implements Resolve<Observable<Magasin[]>> {
  constructor(private magasinsService: MagasinService) { }

  resolve() {
     return this.magasinsService.getMagasins();
}
}
