
import { BaseEntity } from '../../common/base-entity.model' ;
// import { Tranche } from './tranche.model' ;
import { Fournisseur } from '../fournisseurs/fournisseur.model' ;
import { PayementData } from './payement-data.model';
 import { LigneFactureAchats } from './ligne-facture-achats.model';
import { CreditAchats } from '../credits-achats/credit-achats.model';

type  EtatFacture= 'Reglee'| 'Non Reglee' | 'Partiellement Reglee' |'Initial' |'Valide' | 'Annule' | '';


export class FactureAchats extends BaseEntity {
    constructor(public code?: string, 
        public fournisseur?: Fournisseur,
         public montantAPayer?: number,
        public montantTTC?: number, public montantHT?: number,
        public  montantRemise?: number,
        public montantTVA?: number,
        public numeroFactureAchats?: string,
        public payementData?: PayementData,
        public creditAchats?: CreditAchats,
        public dateFacturationObject?: Date,
         public dateFacturation?: string,
         public dateAchatsObject?: Date,
         public dateAchats?: string,
          public etat?: EtatFacture,
         public montantRecu?: number,
         public montantAPayerApresRemise?: number,
         public timbreFiscale?: number ,

     public listLignesFacturesAchats?: LigneFactureAchats[]
     ) {
         super();
         this.fournisseur = new Fournisseur();
         this.creditAchats = new CreditAchats();
         this.montantAPayer =  Number(0);
         this.montantAPayerApresRemise = Number(0);
         this.montantHT = Number( 0);
         this.montantTTC  = Number(0);
         this.montantTVA = Number(0);
         this.montantRemise = Number(0);
         this.timbreFiscale = Number(0);
         this.payementData = new PayementData();
         this.listLignesFacturesAchats = [] ;
         // this.categoryClient = 'PERSONNE_PHYSIQUE';
    }
}
