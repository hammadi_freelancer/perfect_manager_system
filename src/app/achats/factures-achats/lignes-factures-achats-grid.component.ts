
import { Component, OnInit, Inject, ViewChild, Input, AfterViewChecked, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {FactureAchatsService} from './facture-achats.service';
import { FactureAchats } from './facture-achats.model';
import {LigneFactureAchats} from './ligne-facture-achats.model';
import { LigneFactureAchatsElement } from './ligne-facture-achats.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
 import { DialogAddLigneFactureAchatsComponent} from './popups/dialog-add-ligne-facture-achats.component';
 import { DialogEditLigneFactureAchatsComponent } from './popups/dialog-edit-ligne-facture-achats.component';
@Component({
    selector: 'app-lignes-factures-achats-grid',
    templateUrl: 'lignes-factures-achats-grid.component.html',
  })
  export class LignesFacturesAchatsGridComponent implements OnInit, OnChanges {
     private listLignesFacturesAchats: LigneFactureAchats[] = [];
     private  dataLignesFacturesAchatsSource: MatTableDataSource<LigneFactureAchatsElement>;
     private selection = new SelectionModel<LigneFactureAchatsElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];

     @Input()
     private factureAchats: FactureAchats;

     constructor(  private factureAchatsService: FactureAchatsService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      // console.log('credit code after dipslay tab ', this.credit.code);
      this.factureAchatsService.getLignesFacturesAchatsByCodeFacture(this.factureAchats.code).subscribe((listLignesFactAchats) => {
        this.listLignesFacturesAchats= listLignesFactAchats;
// console.log('recieving data from backend', this.listPayementsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }

  ngOnChanges() {
    this.factureAchatsService.getLignesFacturesAchatsByCodeFacture(this.factureAchats.code).subscribe((listLignesFactAchats) => {
      this.listLignesFacturesAchats= listLignesFactAchats;
// console.log('recieving data from backend', this.listPayementsCredits  );
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }
  checkOneActionInvoked() {
    const listSelectedLignesFactAchats: LigneFactureAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((lFactElement) => {
    return lFactElement.code;
  });
  this.listLignesFacturesAchats.forEach(ligneF => {
    if (codes.findIndex(code => code === ligneF.code) > -1) {
      listSelectedLignesFactAchats.push(ligneF);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listLignesFacturesAchats !== undefined) {
 
      this.listLignesFacturesAchats.forEach(ligneFactureAchats => {
             listElements.push( {code: ligneFactureAchats.code ,
              numeroLigneFactureAchats: ligneFactureAchats.numeroLigneFactureAchats,
          article: ligneFactureAchats.article.code,
          magasin: ligneFactureAchats.magasin.code,
          quantite: ligneFactureAchats.quantite,
          prixUnitaire: ligneFactureAchats.prixUnitaire,
          unMesure: ligneFactureAchats.unMesure,
          
          montantHT: ligneFactureAchats.montantHT,
          fodec: ligneFactureAchats.fodec,
          montantFodec: ligneFactureAchats.montantFodec,
          otherTaxe1: ligneFactureAchats.otherTaxe1,
          montantOtherTaxe1: ligneFactureAchats.montantOtherTaxe1,
          otherTaxe2: ligneFactureAchats.otherTaxe2,
          montantOtherTaxe2: ligneFactureAchats.montantOtherTaxe2,
          tva: ligneFactureAchats.tva,
          montantTVA: ligneFactureAchats.montantTVA,
          montantTTC: ligneFactureAchats.montantTTC,
          remise: ligneFactureAchats.remise,
          montantRemise: ligneFactureAchats.montantRemise,
          montantAPayer: ligneFactureAchats.montantAPayer,
          benefice: ligneFactureAchats.beneficeEstime,
          montantBenefice: ligneFactureAchats.montantBeneficeEstime,
          etat: ligneFactureAchats.etat,
          prixVente: ligneFactureAchats.prixVente,
          
          
        } );
      });
    }
      this.dataLignesFacturesAchatsSource = new MatTableDataSource<LigneFactureAchatsElement>(listElements);
      this.dataLignesFacturesAchatsSource.sort = this.sort;
      this.dataLignesFacturesAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'numeroLigneFactureAchats' , displayTitle: 'N°'},
     {name: 'article' , displayTitle: 'Art'}, 
     {name: 'magasin' , displayTitle: 'Mag'},
     {name: 'quantite' , displayTitle: 'Qte'},
     {name: 'unMesure' , displayTitle: 'UM'},
      {name: 'prixUnitaire' , displayTitle: 'P.UN'},
     {name: 'montantHT' , displayTitle: 'M.HT'}, 
     {name: 'montantTVA' , displayTitle: 'M.TVA'},
     {name: 'montantTTC' , displayTitle: 'M.TTC'},
     {name: 'montantRemise' , displayTitle: 'M.Remise'},
      {name: 'montantBenefice' , displayTitle: 'M.Benefice'},
      {name: 'montantAPayer' , displayTitle: 'M.APayer'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataLignesFacturesAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataLignesFacturesAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataLignesFacturesAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataLignesFacturesAchatsSource.paginator) {
      this.dataLignesFacturesAchatsSource.paginator.firstPage();
    }
  }
  showAddLigneDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeFactureAchats : this.factureAchats.code
   };
 
   const dialogRef = this.dialog.open(DialogAddLigneFactureAchatsComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(lAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditLigneDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeLigneFactureAchats: this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditLigneFactureAchatsComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(lAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucune Ligne Séléctionnée!');
    }
    }
    supprimerLignes()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(pay, index) {
              this.factureAchatsService.deleteLigneFactureAchats(pay.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar(  ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucune Ligne Séléctionnée!');
        }
    }
    refresh() {
      this.factureAchatsService.getLignesFacturesAchatsByCodeFacture(this.factureAchats.code).subscribe((listLignesFacs) => {
        this.listLignesFacturesAchats = listLignesFacs;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
















}
