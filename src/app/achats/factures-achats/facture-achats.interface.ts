export interface FactureAchatsElement {
        code: string;
         numeroFactureAchats: string ;
         modePayement: string ;
         dateFacturation: string ;
         dateAchats: string;
         cin: string ;
         nom: string;
         prenom: string;
         matriculeFiscale: string;
         raisonSociale: string;
         montantAPayer: number;
         montantTTC: number;
         montantHT: number;
         montantTVA: number;
         etat: string;
        }
