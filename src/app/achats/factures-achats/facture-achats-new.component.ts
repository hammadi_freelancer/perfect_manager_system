import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {FactureAchats } from './facture-achats.model';

import {FactureAchatsService} from './facture-achats.service';
import {AchatsService} from '../achats.service';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
// import {ActionData} from './action-data.interface';
import { FournisseurService } from '../fournisseurs/fournisseur.service';
//import { FactureVenteFilter } from './facture-vente.filter';
import { ArticleFilter } from './article.filter';

import {Fournisseur} from '../fournisseurs/fournisseur.model';
import {Magasin} from '../../stocks/magasins/magasin.model';
import {UnMesure} from '../../stocks/un-mesures/un-mesure.model';
import {MagasinFilter} from '../../stocks/magasins/magasin.filter';
import {UnMesureFilter} from '../../stocks/un-mesures/un-mesures.filter';
import {LigneFactureAchats} from '../factures-achats/ligne-facture-achats.model';
import {LigneFactureAchatsElement} from '../factures-achats/ligne-facture-achats.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';

import {MatSnackBar} from '@angular/material';
import { MatExpansionPanel } from '@angular/material';
import {ArticleService} from '../../stocks/articles/article.service';
import {Article} from '../../stocks/articles/article.model';
import {ModePayementLibelle} from './mode-payement-libelle.interface';
import {SelectionModel} from '@angular/cdk/collections';
import { FournisseurFilter } from '../fournisseurs/fournisseur.filter';

type LibelleModePayement = 'Credit'| 'Credit_Espece' | 'Cheque' | 'Espece';

@Component({
  selector: 'app-facture-achats-new',
  templateUrl: './facture-achats-new.component.html',
  // styleUrls: ['./players.component.css']
})
export class FactureAchatsNewComponent implements OnInit {

 private factureAchats: FactureAchats=  new FactureAchats();
 private  dataLignesFactureAchatsSource: MatTableDataSource<LigneFactureAchatsElement>;
 
@Input()
private listFacturesAchats: any = [] ;

private saveEnCours = false;
private managedLigneFactureAchats = new LigneFactureAchats();

private selection = new SelectionModel<LigneFactureAchatsElement>(true, []);
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
 
/*@Output()
save: EventEmitter<FactureVente> = new EventEmitter<FactureVente>();*/

@ViewChild('firstMatExpansionPanel')
 private firstMatExpansionPanel : MatExpansionPanel;

 @ViewChild('manageLigneMatExpansionPanel')
 private manageLigneMatExpansionPanel : MatExpansionPanel;

 @ViewChild('listLignesMatExpansionPanel')
 private listLignesMatExpansionPanel : MatExpansionPanel;

//  private factureVenteFilter: FactureVenteFilter;
private articleFilter: ArticleFilter;
private magasinFilter: MagasinFilter;
private unMesureFilter: UnMesureFilter;

private fournisseursFilter: FournisseurFilter;

private listFournisseurs: Fournisseur[];
private listMagasins:Magasin[];
private listArticles:Article[];

private payementModes: any[] = [
  {value: 'Comptant', viewValue: 'Comptant'},
  {value: 'Cheque', viewValue: 'Chèque'},
  {value: 'Credit', viewValue: 'Crédit'},
  {value: 'Avance', viewValue: 'Avance'},
  {value: 'Comptant_Cheque', viewValue: 'Comptant/Chèque'},
  {value: 'Comptant_Avance', viewValue: 'Comptant/Avance'},
  {value: 'Credit_Comptant', viewValue: 'Crédit/Comptant'},
  {value: 'Credit_Cheque', viewValue: 'Crédit/Chèque'},
  {value: 'Credit_Avance', viewValue: 'Crédit/Avance'},
  {value: 'Donnation', viewValue: 'Amicalité'}
];
private etatsFactureAchats: any[] = [
  {value: 'Initial', viewValue: 'Initial'}
 // {value: 'Valide', viewValue: 'Validé'}
];

  constructor( private route: ActivatedRoute,
    private router: Router, private factureAchatsService: FactureAchatsService, private fournisseurService: FournisseurService,
    private articleService: ArticleService, private achatsService: AchatsService,
   private matSnackBar: MatSnackBar, private elRef: ElementRef) {
  }




  ngOnInit() {

   this.listFournisseurs = this.route.snapshot.data.fournisseurs;
   this.fournisseursFilter = new FournisseurFilter(this.listFournisseurs);
   this.unMesureFilter = new UnMesureFilter(this.route.snapshot.data.unMesures);
   this.magasinFilter = new MagasinFilter(this.route.snapshot.data.magasins);
   this.listArticles = this.route.snapshot.data.articles;
   this.listMagasins = this.route.snapshot.data.magasins;
   
   //  this.factureVenteFilter = new FactureVenteFilter(this.listClients, []);
   this.articleFilter = new ArticleFilter(this.route.snapshot.data.articles,
    [], [] );
  
   this.firstMatExpansionPanel.open();
   this.manageLigneMatExpansionPanel.opened.subscribe(openedPanel => {
    this.articleFilter = new ArticleFilter(this.route.snapshot.data.articles,
      [], []);
     });
   this.specifyListColumnsToBeDisplayed();
   this.transformDataToByConsumedByMatTable();
  // this.initializeListLignesFactures();
  /*this.lignesFactures.forEach(function(ligne, index) {
                 ligne.code =  String(index + 1);
  });*/
  this.factureAchats.etat = 'Initial';
 
  
}
  constructClassName(i)  {
    return  'designationDivs_' + i;
   //  this.elRef.nativeElement.querySelector('.designationDivs_' + i )  ;
  }
  calculMontants()
  {
    
  }

  recalculMontants()
  {
   /* if ( this.pagesLignesFacures[this.currentPage - 1] === undefined) {
      this.pagesLignesFacures[this.currentPage - 1]  =  this.lignesFactures;
    }*/
      
    this.factureAchats.montantHT = Number(0);
    this.factureAchats.montantTVA = Number(0);
    this.factureAchats.montantTTC = Number(0);
    this.factureAchats.montantRemise = Number(0);
    this.factureAchats.montantAPayer = Number(0);
    this.factureAchats.montantAPayerApresRemise = Number(0);
    this.factureAchats.listLignesFacturesAchats.forEach(function(ligne , index) {
        this.factureAchats.montantHT = +this.factureAchats.montantHT + ligne.montantHT ;
        this.factureAchats.montantTVA = +this.factureAchats.montantTVA + ligne.montantTVA ;
        this.factureAchats.montantTTC = +this.factureAchats.montantTTC  + ligne.montantTTC ;
        this.factureAchats.montantRemise = +this.factureAchats.montantRemise  + ligne.montantRemise ;
        this.factureAchats.montantAPayer = this.factureAchats.montantTTC ;
        this.factureAchats.montantAPayerApresRemise = +this.factureAchats.montantAPayer - this.factureAchats.montantRemise;
        // this.editLignesMode[i] = true;
       // this.readOnlyLignes[i] = true;
  }, this);

  }

    calculMontantAPayer()
    {
    this.factureAchats.montantAPayer = +this.factureAchats.timbreFiscale + this.factureAchats.montantTTC;
    
    return this.factureAchats.montantAPayer ;
    }
    calculMontantApayerApresRemise()
    {
      
    this.factureAchats.montantAPayerApresRemise = +this.factureAchats.montantAPayer - this.factureAchats.montantRemise;
      return this.factureAchats.montantAPayerApresRemise;
    }
  claculMontantRemise() {
    const prixVente   =  +(+this.managedLigneFactureAchats.montantTTC / +this.managedLigneFactureAchats.quantite)
    ;
    if (!isNaN(prixVente)) {
    console.log(prixVente);
  this.managedLigneFactureAchats.montantRemise = +this.managedLigneFactureAchats.remise  * ( +prixVente / 100) *
   this.managedLigneFactureAchats.quantite;
  // console.log(this.lignesFactures[i].montantRemise);
    }
  return this.managedLigneFactureAchats.montantRemise;
}
calculMontantHT()
{
  this.managedLigneFactureAchats.montantHT = +this.managedLigneFactureAchats.prixUnitaire * this.managedLigneFactureAchats.quantite;
  return this.managedLigneFactureAchats.montantHT;
}
  calculMontantTTC()
  {
    this.managedLigneFactureAchats.montantTTC = +(+this.managedLigneFactureAchats.prixUnitaire * this.managedLigneFactureAchats.quantite)
    + this.managedLigneFactureAchats.montantTVA;
    
    return  this.managedLigneFactureAchats.montantTTC;
  }
  calculMontantTVA() {

    this.managedLigneFactureAchats.montantTVA =  (this.managedLigneFactureAchats.prixUnitaire / 100) *
    this.managedLigneFactureAchats.tva * this.managedLigneFactureAchats.quantite;
    this.managedLigneFactureAchats.montantTTC = +(+this.managedLigneFactureAchats.prixUnitaire * this.managedLigneFactureAchats.quantite)
    + this.managedLigneFactureAchats.montantTVA;
    // console.log('montant tva');
   // console.log(this.factureVente.montantTVA + this.listLignesFactureVentes[i].montantTVA);
   if(this.managedLigneFactureAchats.remise !== 0) {
    this.claculMontantRemise() ;
  }  
    return  this.managedLigneFactureAchats.montantTVA;
  }

  calculMontantBenefice() {
    
        this.managedLigneFactureAchats.montantBeneficeEstime =  (this.managedLigneFactureAchats.prixUnitaire / 100) *
        this.managedLigneFactureAchats.beneficeEstime * this.managedLigneFactureAchats.quantite;
        return  this.managedLigneFactureAchats.montantBeneficeEstime;
      }

 
 calculMontant4thTaxe() {
        
            this.managedLigneFactureAchats.montantOtherTaxe2 =  (this.managedLigneFactureAchats.prixUnitaire / 100) *
            this.managedLigneFactureAchats.otherTaxe2 * this.managedLigneFactureAchats.quantite;
            if(this.managedLigneFactureAchats.montantTTC === 0 ) {
              
       this.managedLigneFactureAchats.montantTTC = +(+this.managedLigneFactureAchats.prixUnitaire * this.managedLigneFactureAchats.quantite)
            + this.managedLigneFactureAchats.montantOtherTaxe2;
            } else {
              this.managedLigneFactureAchats.montantTTC = + this.managedLigneFactureAchats.montantTTC
              + this.managedLigneFactureAchats.montantOtherTaxe2;
            }

            if(this.managedLigneFactureAchats.remise !== 0) {
              this.claculMontantRemise() ;
            }  
            return  this.managedLigneFactureAchats.montantOtherTaxe2;
          }

 calculMontant3rdTaxe() {
            
                this.managedLigneFactureAchats.montantOtherTaxe1 =  (this.managedLigneFactureAchats.prixUnitaire / 100) *
                this.managedLigneFactureAchats.otherTaxe1 * this.managedLigneFactureAchats.quantite;
               if(this.managedLigneFactureAchats.montantTTC === 0 ) {
        this.managedLigneFactureAchats.montantTTC = +(+this.managedLigneFactureAchats.prixUnitaire
          * this.managedLigneFactureAchats.quantite)
                + this.managedLigneFactureAchats.montantOtherTaxe1;
                 } else {
                  this.managedLigneFactureAchats.montantTTC = + this.managedLigneFactureAchats.montantTTC
                          + this.managedLigneFactureAchats.montantOtherTaxe1;
                 }
                 if(this.managedLigneFactureAchats.remise !== 0) {
                  this.claculMontantRemise() ;
                }  
                return  this.managedLigneFactureAchats.montantOtherTaxe1;
              }

      calculMontantFodec() {
                
             this.managedLigneFactureAchats.montantFodec =  (this.managedLigneFactureAchats.prixUnitaire / 100) *
                    this.managedLigneFactureAchats.fodec * this.managedLigneFactureAchats.quantite;
                    if(this.managedLigneFactureAchats.montantTTC === 0 ) {
                      
                    this.managedLigneFactureAchats.montantTTC = +(+this.managedLigneFactureAchats.prixUnitaire *
                       this.managedLigneFactureAchats.quantite)
                    + this.managedLigneFactureAchats.montantFodec;
                    } else  {
                      this.managedLigneFactureAchats.montantTTC = + this.managedLigneFactureAchats.montantTTC
                      + this.managedLigneFactureAchats.montantFodec;
                    }
                    if(this.managedLigneFactureAchats.remise !== 0) {
                      this.claculMontantRemise() ;
                    }  
                    return  this.managedLigneFactureAchats.montantFodec;
                  }
  /*selectedArticleCode(i, $event) {
     console.log(i);
     console.log($event);
     this.listLignesFactureVentes[i].article.code = $event.option.value;
     this.listLignesFactureVentes[i].article.libelle  = this.listArticles.find((article) => (article.code === $event.option.value)).libelle;
    
     const  div  : HTMLDivElement = this.elRef.nativeElement.querySelector('.designationDivs_' + i )  ;
     div.innerText = this.listLignesFactureVentes[i].article.libelle;
     console.log(div);


  }*/

  saveFactureAchats() {

    /*if (this.modeUpdate ) {
      this.updateCredit();
    } else {*/
      if (this.factureAchats.etat === '' || this.factureAchats.etat === undefined) {
        this.factureAchats.etat = 'Initial';
      }
      //this.fillDataLignesFactures();
    console.log(this.factureAchats);
    // this.saveEnCours = true;
    this.saveEnCours = true;
    
    this.factureAchatsService.addFactureAchats(this.factureAchats).subscribe((response) => {
      this.saveEnCours = false;
      
        if (response.error_message ===  undefined) {
          console.log('undefined error message');
          // this.save.emit(response);
          this.factureAchats = new  FactureAchats();
          this.factureAchats.listLignesFacturesAchats = [];
          this.openSnackBar(  'Facture Achats Enregistrée');
        } else {
         // show error message
         this.openSnackBar(  'Erreurs:Opération Echouée');
         
        }
    });
  }
  fillDataNomPrenom()
  {
    const listFiltred  = this.listFournisseurs.filter((fournisseur) => (fournisseur.cin === this.factureAchats.fournisseur.cin));
    this.factureAchats.fournisseur.nom = listFiltred[0].nom;
    this.factureAchats.fournisseur.prenom = listFiltred[0].prenom;
  }
  fillDataMatriculeRaison()
  {
    const listFiltred  = this.listFournisseurs.filter((fournisseur) => 
    (fournisseur.registreCommerce === this.factureAchats.fournisseur.registreCommerce));
    this.factureAchats.fournisseur.matriculeFiscale = listFiltred[0].matriculeFiscale;
    this.factureAchats.fournisseur.raisonSociale = listFiltred[0].raisonSociale;
  }
  vider() {
    this.factureAchats =  new FactureAchats();
  }
  openSnackBar(messageToDisplay) {
   this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
       verticalPosition: 'top', panelClass: 'snackBarClass'});
  }
loadGridFacturesAchats() {
  this.router.navigate(['/pms/achats/facturesAchats']) ;
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.factureAchats.listLignesFacturesAchats !== undefined) {
    this.factureAchats.listLignesFacturesAchats.forEach(ligneFA => {
           listElements.push( {code: ligneFA.code ,
            numeroLigneFactureAchats: ligneFA.numeroLigneFactureAchats,
            article: ligneFA.article.code,
             magasin: ligneFA.magasin.code,
             unMesure: ligneFA.unMesure.code,
             quantite: ligneFA.quantite,
             prixUnitaire: ligneFA.prixUnitaire,
             tva: ligneFA.tva,
             montantTVA: ligneFA.montantTVA,
             remise: ligneFA.remise,
             montantRemise: ligneFA.montantRemise,
             montantHT: ligneFA.montantHT,
             montantTTC: ligneFA.montantTTC,
             etat: ligneFA.etat
      } );
    });
  }
    this.dataLignesFactureAchatsSource= new MatTableDataSource<LigneFactureAchatsElement>(listElements);
    this.dataLignesFactureAchatsSource.sort = this.sort;
    this.dataLignesFactureAchatsSource.paginator = this.paginator;
    
    
}
 specifyListColumnsToBeDisplayed() {
   // console.log('calling specifyListColumnsToBeDisplayed documents grid');
  this.columnsTitles = [];
  this.columnsNames = [];
  this.columnsTitlesAr  = [];
  
   this.columnsDefinitions = [{name: 'numeroLigneFactureAchats' , displayTitle: 'N°'},
   {name: 'article' , displayTitle: 'Article'},
   {name: 'magasin' , displayTitle: 'Magasin'},
   {name: 'unMesure' , displayTitle: 'Un Mesure'},
   {name: 'quantite' , displayTitle: 'Quantite'},
   {name: 'prixUnitaire' , displayTitle: 'Prix Unitaire'},
   {name: 'tva' , displayTitle: 'TVA'},
   {name: 'montantTVA' , displayTitle: 'Montant TVA'},
   {name: 'montantHT' , displayTitle: 'Montant HT'},
   { name : 'montantTTC' , displayTitle : 'Montant TTC'},
  
    ];

   this.columnsTitles = this.columnsDefinitions.map((colDef) => {
             return colDef.displayTitle;
   });
   
   this.columnsNames[0] = 'select';
   this.columnsDefinitions.map((colDef) => {
    this.columnsNames.push(colDef.name);
    console.log(this.columnsNames);
});
   this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
    return colDef.displayTitleAr;
});
}

applyFilter(filterValue: string) {
  this.dataLignesFactureAchatsSource.filter = filterValue.trim().toLowerCase();
  if (this.dataLignesFactureAchatsSource.paginator) {
    this.dataLignesFactureAchatsSource.paginator.firstPage();
  }
}

checkLigneFactureAchats()
{

}
getIndexOfRow(row)
{

    for ( let i = 0; i < this.factureAchats.listLignesFacturesAchats.length ; i++) {
      if (this.factureAchats.listLignesFacturesAchats[i].numeroLigneFactureAchats === row.numeroLigneFactureAchats)
       {
             return i;
        }
    }
    return -1;
}
editLigneFactureAchats(row)
{
  this.managedLigneFactureAchats.numeroLigneFactureAchats = row.numeroLigneFactureAchats;
  this.managedLigneFactureAchats.article.code =  row.article;
  this.managedLigneFactureAchats.magasin.code =  row.magasin;
  this.managedLigneFactureAchats.unMesure.code =  row.unMesure;
  this.managedLigneFactureAchats.quantite =  row.quantite;
  this.managedLigneFactureAchats.prixUnitaire =  row.prixUnitaire;
  this.managedLigneFactureAchats.montantHT =  row.montantHT;
  this.managedLigneFactureAchats.montantTTC =  row.montantTTC;
  this.managedLigneFactureAchats.tva =  row.tva; 
  this.managedLigneFactureAchats.montantTVA =  row.montantTVA;
  this.managedLigneFactureAchats.fodec =  row.fodec;
  this.managedLigneFactureAchats.montantFodec =  row.montantFodec;
  this.managedLigneFactureAchats.remise =  row.remise;
  this.managedLigneFactureAchats.montantRemise=  row.montantRemise;
 
  this.managedLigneFactureAchats.otherTaxe1 =  row.otherTaxe1;
  this.managedLigneFactureAchats.montantOtherTaxe1 =  row.montantOtherTaxe1;
  this.managedLigneFactureAchats.otherTaxe2 =  row.otherTaxe2;
  this.managedLigneFactureAchats.montantOtherTaxe2=  row.montantOtherTaxe2;
  this.managedLigneFactureAchats.montantBeneficeEstime=  row.montantBeneficeEstime;
  this.managedLigneFactureAchats.beneficeEstime =  row.beneficeEstime;

  if(row.magasin === undefined)
    {
      this.managedLigneFactureAchats.magasin = new Magasin();
    } else
    {
      this.fillDataLibelleMagasin();
    }
    if(row.unMesure === undefined)
      {
        this.managedLigneFactureAchats.unMesure = new UnMesure();
      }
      if(row.article === undefined)
        {
          this.managedLigneFactureAchats.article = new Article();
        } else {
          this.fillDataLibelleArticle();
        }
        console.log('row to edit is ', row);

  this.manageLigneMatExpansionPanel.open();
  
}
deleteLigneFactureAchats(row)
{
 // console.log('index is ');
  //console.log(index);
  const indexOfRow = this.getIndexOfRow(row);
  this.factureAchats.listLignesFacturesAchats.splice(indexOfRow , 1 );
  this.transformDataToByConsumedByMatTable();
  
}
addLigneFactureAchats()
{
  if (this.managedLigneFactureAchats.quantite === undefined
    || this.managedLigneFactureAchats.quantite === 0 ||
    this.managedLigneFactureAchats.quantite === null ||
    this.managedLigneFactureAchats.prixUnitaire === undefined
      || this.managedLigneFactureAchats.prixUnitaire === 0 ||
      this.managedLigneFactureAchats.prixUnitaire === null  ||
      this.managedLigneFactureAchats.article.code === undefined  ||
      this.managedLigneFactureAchats.article.code === '' ||
      this.managedLigneFactureAchats.article.code === null 
  ) {
        this.openSnackBar('Donnnées Manquées (QTE/ART/PUNIT!');
    } else if (this.managedLigneFactureAchats.numeroLigneFactureAchats === undefined
  || this.managedLigneFactureAchats.numeroLigneFactureAchats === '' ||
  this.managedLigneFactureAchats.numeroLigneFactureAchats=== null ) {
      this.openSnackBar('Numéro de Ligne Non Spécifié !');
    } else {
  this.managedLigneFactureAchats.numeroFactureAchats = this.factureAchats.numeroFactureAchats;
  if (this.factureAchats.listLignesFacturesAchats.length === 0) {
  this.factureAchats.listLignesFacturesAchats.push(this.managedLigneFactureAchats);
  } else  {
    console.log('look for index:');
     const indexOfRow = this.getIndexOfRow(this.managedLigneFactureAchats);
     console.log(indexOfRow);
           if (indexOfRow === -1) {
    this.factureAchats.listLignesFacturesAchats.push(this.managedLigneFactureAchats);

         } else  {
     this.factureAchats.listLignesFacturesAchats[indexOfRow] = this.managedLigneFactureAchats;

       }
      
  }
    }
  this.transformDataToByConsumedByMatTable() ;
  this.managedLigneFactureAchats = new LigneFactureAchats();
  this.manageLigneMatExpansionPanel.close();
  this.listLignesMatExpansionPanel.open();

}


  fillDataLibelleUnMesure()
  {

  }
  fillDataLibelleMagasin()
  {
    this.listMagasins.forEach((magasin, index) => {
      if (this.managedLigneFactureAchats.magasin.code === magasin.code) {
       this.managedLigneFactureAchats.magasin.description = magasin.description;
       this.managedLigneFactureAchats.magasin.adresse = magasin.adresse;
       
       }

 } , this);
  }
  fillDataLibelleArticle()  {
    
            this.listArticles.forEach((article, index) => {
                 if (this.managedLigneFactureAchats.article.code === article.code) {
                  this.managedLigneFactureAchats.article.designation = article.designation;
                  this.managedLigneFactureAchats.article.libelle = article.libelle;
                  
                  }
    
            } , this);
   }
}
