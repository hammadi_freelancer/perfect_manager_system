import { Article } from '../../stocks/articles/article.model';
import { Magasin } from '../../stocks/magasins/magasin.model';
import { UnMesure } from '../../stocks/un-mesures/un-mesure.model';

import { BaseEntity } from '../../common/base-entity.model' ;
// type LibelleModePayement = 'Credit'| 'Credit_Espece' | 'Cheque' | 'Espece';
type Etat = 'Initial' | 'Valide' | 'Annule' | '' | 'Regle' | 'NonRegle';
export class LigneFactureAchats extends BaseEntity {
    constructor(
        public code?: string,
        public numeroLigneFactureAchats?: string,
        public codeFactureAchats?: string,
        public numeroFactureAchats?: string,
        public article?: Article,
        public unMesure?: UnMesure,
        public quantite?: number,
        public prixUnitaire?: number,
         public tva?: number,
         public montantTVA?: number,
         public fodec?: number,
         public montantFodec?: number,
         public otherTaxe1?: number,
         public montantOtherTaxe1?: number,
         public otherTaxe2?: number,
         public montantOtherTaxe2?: number,
        public remise?: number ,
         public montantRemise?: number,
         public prixVente?: number,
         public beneficeEstime?: number,
         public montantBeneficeEstime?: number,
         public magasin?: Magasin,
        public montantHT?: number,
        public montantTTC?: number ,
         public montantAPayer?: number,
         public etat?: Etat,
     ) {
        super();
         this.article = new Article();
         this.magasin = new Magasin();
         this.unMesure = new UnMesure();
         this.remise = Number(0);
         this.montantRemise = Number(0);
         this.montantTVA = Number(0);
         this.prixUnitaire = Number(0);
         this.quantite = Number(0);
         this.montantTTC = Number(0);
         this.montantHT = Number(0);
         this.tva = Number(0);
         this.montantAPayer =  Number(0);
         this.fodec = Number(0);
         this.montantFodec = Number(0);
         this.otherTaxe1 = Number(0);
         this.otherTaxe2 = Number(0);
         this.montantOtherTaxe1 = Number(0);
         this.montantOtherTaxe2 = Number(0);
         this.beneficeEstime = Number(0);
         this.montantBeneficeEstime = Number(0);
         
         this.prixVente = Number(0);
         
         
    }
}
