import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import {FactureAchats} from './facture-achats.model' ;
import { DocumentFactureAchats } from './document-facture-achats.model';
import {PayementFactureAchats} from './payement-facture-achats.model' ;
import {AjournementFactureAchats} from './ajournement-facture-achats.model' ;
import {CreditAchats} from '../credits-achats/credit-achats.model' ;
import {LigneFactureAchats} from './ligne-facture-achats.model' ;

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_FACTURES_ACHATS = 'http://localhost:8082/achats/facturesAchats/getFacturesAchats';
const URL_GET_PAGE_FACTURES_ACHATS = 'http://localhost:8082/achats/facturesAchats/getPageFacturesAchats';

const URL_GET_FACTURE_ACHATS = 'http://localhost:8082/achats/facturesAchats/getFactureAchats';
const  URL_ADD_FACTURE_ACHATS  = 'http://localhost:8082/achats/facturesAchats/addFactureAchats';
const  URL_UPDATE_FACTURE_ACHATS  = 'http://localhost:8082/achats/facturesAchats/updateFactureAchats';
const  URL_DELETE_FACTURE_ACHATS = 'http://localhost:8082/achats/facturesAchats/deleteFactureAchats';
const  URL_GET_TOTAL_FACTURE_ACHATS = 'http://localhost:8082/achats/facturesAchats/getTotalFacturesAchats';

const  URL_ADD_PAYEMENT_FACTURE_ACHATS = 'http://localhost:8082/finance/payementsFacturesAchats/addPayementFactureAchats';
const  URL_GET_LIST_PAYEMENT_FACTURE_ACHATS  = 'http://localhost:8082/finance/payementsFacturesAchats/getPayementsFacturesAchats';
const  URL_DELETE_PAYEMENT_FACTURE_ACHATS  = 'http://localhost:8082/finance/payementsFacturesAchats/deletePayementFactureAchats';
const  URL_UPDATE_PAYEMENT_FACTURE_ACHATS  = 'http://localhost:8082/finance/payementsFacturesAchats/updatePayementFactureAchats';
const URL_GET_PAYEMENT_FACTURE_ACHATS  = 'http://localhost:8082/finance/payementsFacturesAchats/getPayementFactureAchats';
const URL_GET_LIST_PAYEMENT_FACTURE_ACHATS_BY_CODE_FACTURE =
'http://localhost:8082/finance/payementsFacturesAchats/getPayementsFacturesAchatsByCodeFactureAchats';


const  URL_ADD_DOCUMENT_FACTURE_ACHATS = 'http://localhost:8082/achats/documentsFacturesAchats/addDocumentFactureAchats';
const  URL_GET_LIST_DOCUMENTS_FACTURE_ACHATS = 'http://localhost:8082/achats/documentsFacturesAchats/getDocumentsFactureAchats';
const  URL_DELETE_DOCUMENT_FACTURE_ACHATS = 'http://localhost:8082/achats/documentsFacturesAchats/deleteDocumentFactureAchats';
const  URL_UPDATE_DOCUMENT_FACTURE_ACHATS = 'http://localhost:8082/achats/documentsFacturesAchats/updateDocumentFactureAchats';
const URL_GET_DOCUMENT_FACTURE_ACHATS = 'http://localhost:8082/achats/documentsFacturesAchats/getDocumentFactureAchats';
const URL_GET_LIST_DOCUMENTS_FACTURE_ACHATS_BY_CODE_FACTURE_ACHATS =
 'http://localhost:8082/achats/documentsFacturesAchats/getDocumentsFactureAchatsByCodeFactureAchats';

 const  URL_ADD_AJOURNEMENT_FACTURE_ACHATS = 'http://localhost:8082/achats/ajournementsFacturesAchats/addAjournementFactureAchats';
 const  URL_GET_LIST_AJOURNEMENT_FACTURE_ACHATS = 'http://localhost:8082/achats/ajournementsFacturesAchats/getAjournementsFactureAchats';
 const  URL_DELETE_AJOURNEMENT_FACTURE_ACHATS= 'http://localhost:8082/achats/ajournementsFacturesAchats/deleteAjournementFactureAchats';
 const  URL_UPDATE_AJOURNEMENT_FACTURE_ACHATS = 'http://localhost:8082/achats/ajournementsFacturesAchats/updateAjournementFactureAchats';
 const URL_GET_AJOURNEMENT_FACTURE_ACHATS = 'http://localhost:8082/achats/ajournementsFacturesAchats/getAjournementFactureAchats';
 const URL_GET_LIST_AJOURNEMENT_FACTURE_ACHATS_BY_CODE_FACTURE_ACHATS =
  'http://localhost:8082/achats/ajournementsFacturesAchats/getAjournementsFacturesAchatsByCodeFactureAchats';



  const  URL_ADD_LIGNE_FACTURE_ACHATS = 'http://localhost:8082/achats/lignesFacturesAchats/addLigneFactureAchats';
  const  URL_GET_LIST_LIGNES_FACTURE_ACHATS = 'http://localhost:8082/achats/lignesFacturesAchats/getLignesFactureAchats';
  const  URL_DELETE_LIGNE_FACTURE_ACHATS = 'http://localhost:8082/achats/lignesFacturesAchats/deleteLigneFactureAchats';
  const  URL_UPDATE_LIGNE_FACTURE_ACHATS = 'http://localhost:8082/achats/lignesFacturesAchats/updateLigneFactureAchats';
  const URL_GET_LIGNE_FACTURE_ACHATS = 'http://localhost:8082/achats/lignesFacturesAchats/getLigneFactureAchats';
  const URL_GET_LIST_LIGNES_FACTURE_ACHATS_BY_CODE_FACTURE_ACHATS =
   'http://localhost:8082/achats/lignesFacturesAchats/getLignesFactureAchatsByCodeFactureAchats';

@Injectable({
  providedIn: 'root'
})
export class FactureAchatsService {

  constructor(private httpClient: HttpClient) {

   }
   getTotalFacturesAchats(filter): Observable<any> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('filter', filterStr);
    return this.httpClient
    .get<any>(URL_GET_TOTAL_FACTURE_ACHATS, {params});
   }


   getPageFacturesAchats(pageNumber, nbElementsPerPage, filter): Observable<FactureAchats[]> {
   
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('pageNumber', pageNumber)
    .set('nbElementsPerPage', nbElementsPerPage)
    .set('filter', filterStr);
    return this.httpClient
    .get<FactureAchats[]>(URL_GET_PAGE_FACTURES_ACHATS, {params});
   }
   getFacturesAchats( ): Observable<FactureAchats[]> {
 
    return this.httpClient
    .get<FactureAchats[]>(URL_GET_LIST_FACTURES_ACHATS);
   }

   addFactureAchats(factureAchats: FactureAchats): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_FACTURE_ACHATS, factureAchats);
  }
  updateFactureAchats(factureAchats: FactureAchats): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_FACTURE_ACHATS, factureAchats);
  }
  deleteFactureAchats(codeFactureAchats): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_FACTURE_ACHATS + '/' + codeFactureAchats);
  }
  getFactureAchats(codeFactureAchats): Observable<FactureAchats> {
    return this.httpClient.get<FactureAchats>(URL_GET_FACTURE_ACHATS + '/' + codeFactureAchats);
  }


  addPayementFactureAchats(payementFactureAchats: PayementFactureAchats): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_PAYEMENT_FACTURE_ACHATS, payementFactureAchats);
    
  }
  getPayementsFacturesAchats(): Observable<PayementFactureAchats[]> {
    return this.httpClient
    .get<PayementFactureAchats[]>(URL_GET_LIST_PAYEMENT_FACTURE_ACHATS);
   }
   getPayementFactureAchats(codePayementFactureAchats): Observable<PayementFactureAchats> {
    return this.httpClient.get<PayementFactureAchats>(URL_GET_PAYEMENT_FACTURE_ACHATS +
       '/' + codePayementFactureAchats);
  }
  deletePayementFactureAchats(codePayementFactureAchats): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_PAYEMENT_FACTURE_ACHATS + '/' + codePayementFactureAchats);
  }
  getPayementsFacturesAchatsByCodeFacture(codeFactureAchats): Observable<PayementFactureAchats[]> {
    return this.httpClient.get<PayementFactureAchats[]>(URL_GET_LIST_PAYEMENT_FACTURE_ACHATS_BY_CODE_FACTURE + '/' + codeFactureAchats);
  }

  updatePayementFactureAchats(payFactureAchats: PayementFactureAchats): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_PAYEMENT_FACTURE_ACHATS, payFactureAchats);
  }

  addDocumentFactureAchats(docFactureAchats: DocumentFactureAchats): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_DOCUMENT_FACTURE_ACHATS, docFactureAchats);
    
  }
  getDocumentsFacturesAchats(): Observable<DocumentFactureAchats[]> {
    return this.httpClient
    .get<DocumentFactureAchats[]>(URL_GET_LIST_DOCUMENTS_FACTURE_ACHATS);
   }
   getDocumentFactureAchats(codeDocumentFactureAchats): Observable<DocumentFactureAchats> {
    return this.httpClient.get<DocumentFactureAchats>(URL_GET_DOCUMENT_FACTURE_ACHATS + '/' + codeDocumentFactureAchats);
  }
  deleteDocumentFactureAchats(codeDocumentFactureAchats): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_DOCUMENT_FACTURE_ACHATS + '/' + codeDocumentFactureAchats);
  }
  updateDocumentFactureAchats(docFactureAchats: DocumentFactureAchats): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_DOCUMENT_FACTURE_ACHATS, docFactureAchats);
  }


  getDocumentsFacturesAchatsByCodeFacture(codeFactureAchats): Observable<DocumentFactureAchats[]> {
    return this.httpClient.get<DocumentFactureAchats[]>( URL_GET_LIST_DOCUMENTS_FACTURE_ACHATS_BY_CODE_FACTURE_ACHATS +
       '/' + codeFactureAchats);
  }

  addAjournementFactureAchats(ajournFactureAchats: AjournementFactureAchats): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_AJOURNEMENT_FACTURE_ACHATS, ajournFactureAchats);
    
  }
  updateAjournementFactureAchats(ajournFactureAchats: AjournementFactureAchats): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_AJOURNEMENT_FACTURE_ACHATS, ajournFactureAchats);
  }
  getAjournementsFacturesAchatsByCodeFacture(codeFactureAchats): Observable<AjournementFactureAchats[]> {
    return this.httpClient.get<AjournementFactureAchats[]>( URL_GET_LIST_AJOURNEMENT_FACTURE_ACHATS_BY_CODE_FACTURE_ACHATS +
       '/' + codeFactureAchats);
  }
  getAjournementsFacturesAchats(): Observable<AjournementFactureAchats[]> {
    return this.httpClient
    .get<AjournementFactureAchats[]>(URL_GET_LIST_AJOURNEMENT_FACTURE_ACHATS);
   }
   getAjournementFactureAchats(codeAjournementFactureAchats): Observable<AjournementFactureAchats> {
    return this.httpClient.get<AjournementFactureAchats>(URL_GET_AJOURNEMENT_FACTURE_ACHATS + '/' + codeAjournementFactureAchats);
  }
  deleteAjournementFactureAchats(codeAjournementFactureAchats): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_AJOURNEMENT_FACTURE_ACHATS + '/' + codeAjournementFactureAchats);
  }


  addLigneFactureAchats(ligneFactureAchats: LigneFactureAchats): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_LIGNE_FACTURE_ACHATS, ligneFactureAchats);
    
  }
  updateLigneFactureAchats(ligneFactureAchats: LigneFactureAchats): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_LIGNE_FACTURE_ACHATS, ligneFactureAchats);
  }
  getLignesFacturesAchatsByCodeFacture(codeFactureAchats): Observable<LigneFactureAchats[]> {
    return this.httpClient.get<LigneFactureAchats[]>( URL_GET_LIST_LIGNES_FACTURE_ACHATS_BY_CODE_FACTURE_ACHATS +
       '/' + codeFactureAchats);
  }
  getLignesFacturesAchats(): Observable<LigneFactureAchats[]> {
    return this.httpClient
    .get<LigneFactureAchats[]>(URL_GET_LIST_LIGNES_FACTURE_ACHATS);
   }
   getLigneFactureAchats(codeLigneFactureAchats): Observable<LigneFactureAchats> {
    return this.httpClient.get<LigneFactureAchats>(URL_GET_LIST_LIGNES_FACTURE_ACHATS + '/' + codeLigneFactureAchats);
  }
  deleteLigneFactureAchats(codeLigneFactureAchats): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_LIGNE_FACTURE_ACHATS+ '/' + codeLigneFactureAchats);
  }
}
