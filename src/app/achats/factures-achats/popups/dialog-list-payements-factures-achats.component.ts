
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {FactureAchatsService} from '../facture-achats.service';
import {PayementFactureAchats} from '../payement-facture-achats.model';
import { PayementFactureAchatsElement } from '../payement-facture-achats.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-payements-factures-achats',
    templateUrl: 'dialog-list-payements-factures-achats.component.html',
  })
  export class DialogListPayementsFactureAchatsComponent implements OnInit {
     private listPayementsFactureAchats: PayementFactureAchats[] = [];
     private  dataPayementsFactureAchatsSource: MatTableDataSource<PayementFactureAchatsElement>;
     private selection = new SelectionModel<PayementFactureAchatsElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private factureAchatsService: FactureAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      // console.log('code credit in dialog component is :', this.data.codeFactureVentes);
      this.factureAchatsService.getPayementsFacturesAchatsByCodeFacture(this.data.codeFactureAchats)
      .subscribe((listPayementsFactAchats) => {
        this.listPayementsFactureAchats = listPayementsFactAchats;
// console.log('recieving data from backend', this.listPayementsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedPayementsFactureAchats: PayementFactureAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((payementFactureAchatsElement) => {
    return payementFactureAchatsElement.code;
  });
  this.listPayementsFactureAchats.forEach(payementFactureAchats => {
    if (codes.findIndex(code => code === payementFactureAchats.code) > -1) {
      listSelectedPayementsFactureAchats.push(payementFactureAchats);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listPayementsFactureAchats !== undefined) {

      this.listPayementsFactureAchats.forEach(payementFactureAchats => {
             listElements.push( {code: payementFactureAchats.code ,
          dateOperation: payementFactureAchats.dateOperation,
          montantPaye: payementFactureAchats.montantPaye,
          modePayement: payementFactureAchats.modePayement,
          numeroCheque: payementFactureAchats.numeroCheque,
          dateCheque: payementFactureAchats.dateCheque,
          compte: payementFactureAchats.compte,
          compteAdversaire: payementFactureAchats.compteAdversaire,
          banque: payementFactureAchats.banque,
          banqueAdversaire: payementFactureAchats.banqueAdversaire,
          description: payementFactureAchats.description
        } );
      });
    }
      this.dataPayementsFactureAchatsSource = new MatTableDataSource<PayementFactureAchatsElement>(listElements);
      this.dataPayementsFactureAchatsSource.sort = this.sort;
      this.dataPayementsFactureAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'}, 
     {name: 'montantPaye' , displayTitle: 'Montant Payé'},
     {name: 'modePayement' , displayTitle: 'Mode Payement'},
      {name: 'numeroCheque' , displayTitle: 'N°Chèque'},
     {name: 'dateCheque' , displayTitle: 'Date Chèque'}, 
     {name: 'compte' , displayTitle: 'Compte'},
     {name: 'compteAdversaire' , displayTitle: 'Compte Adv.'},
      {name: 'banque' , displayTitle: 'Banque'},
       { name : 'banqueAdversaire' , displayTitle : 'Banque Adv.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataPayementsFactureAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataPayementsFactureAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataPayementsFactureAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataPayementsFactureAchatsSource.paginator) {
      this.dataPayementsFactureAchatsSource.paginator.firstPage();
    }
  }

}
