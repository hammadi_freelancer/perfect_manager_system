
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {FactureAchatsService} from '../facture-achats.service';
import {AjournementFactureAchats} from '../ajournement-facture-achats.model';
import { AjournementFactureAchatsElement } from '../ajournement-facture-achats.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-ajournements-factures-achats',
    templateUrl: 'dialog-list-ajournements-factures-achats.component.html',
  })
  export class DialogListAjournementsFacturesAchatsComponent implements OnInit {
     private listAjournementFacturesAchats: AjournementFactureAchats[] = [];
     private  dataAjournementsFacturesAchatsSource: MatTableDataSource<AjournementFactureAchatsElement>;
     private selection = new SelectionModel<AjournementFactureAchatsElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private factureAchatsService: FactureAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      // console.log('code credit in dialog component is :', this.data.codeCredit);
      this.factureAchatsService.getAjournementsFacturesAchatsByCodeFacture(this.data.codeFactureAchats)
      .subscribe((listAjournementsFacturesAchats) => {
        this.listAjournementFacturesAchats = listAjournementsFacturesAchats;
// console.log('recieving data from backend', this.listAjournementCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedAjournementsFactureAchats: AjournementFactureAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((ajouFactVElement) => {
    return ajouFactVElement.code;
  });
  this.listAjournementFacturesAchats.forEach(ajournementFactureAchats => {
    if (codes.findIndex(code => code === ajournementFactureAchats.code) > -1) {
      listSelectedAjournementsFactureAchats.push(ajournementFactureAchats);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listAjournementFacturesAchats !== undefined) {

      this.listAjournementFacturesAchats.forEach(ajournementFactureAchats=> {
             listElements.push( {code: ajournementFactureAchats.code ,
          dateOperation: ajournementFactureAchats.dateOperation,
          nouvelleDatePayement: ajournementFactureAchats.nouvelleDatePayement,
          motif: ajournementFactureAchats.motif,
          description: ajournementFactureAchats.description
        } );
      });
    }
      this.dataAjournementsFacturesAchatsSource = new MatTableDataSource<AjournementFactureAchatsElement>(listElements);
      this.dataAjournementsFacturesAchatsSource.sort = this.sort;
      this.dataAjournementsFacturesAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'},
     {name: 'nouvelleDatePayement' , displayTitle: 'N°elle Date Pay.'},
     {name: 'motif' , displayTitle: 'Motif'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataAjournementsFacturesAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataAjournementsFacturesAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataAjournementsFacturesAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataAjournementsFacturesAchatsSource.paginator) {
      this.dataAjournementsFacturesAchatsSource.paginator.firstPage();
    }
  }

}
