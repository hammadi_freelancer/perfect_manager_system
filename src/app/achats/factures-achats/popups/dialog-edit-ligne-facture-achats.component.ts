
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {FactureAchatsService} from '../facture-achats.service';
import {FactureAchats} from '../facture-achats.model';

import { LigneFactureAchats} from '../ligne-facture-achats.model';
@Component({
    selector: 'app-dialog-edit-ligne-facture-achats',
    templateUrl: 'dialog-edit-ligne-facture-achats.component.html',
  })
  export class DialogEditLigneFactureAchatsComponent implements OnInit {

     private ligneFactureAchats: LigneFactureAchats;
     private updateEnCours = false;
     private listCodesArticles: string[];
     private listCodesMagasins: string[];
     private listCodesUnMesures: string[];
     
     private etatsLignes: any[] = [
      {value: 'Annule', viewValue: 'Annulé'},
      {value: 'Initial', viewValue: 'Initial'},
      {value: 'Valide', viewValue: 'Validé'}
    ];
     constructor(  private factureAchatsService: FactureAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit() {
        this.factureAchatsService.getLigneFactureAchats(this.data.codeLigneFactureAchats).subscribe((ligne) => {
            this.ligneFactureAchats = ligne;
            this.listCodesArticles = this.data.listCodesArticles;
            // this.listCodesMagasins = this.data.listCodesMagasins;
            
     });
   }
    updateLigneFactureAchats() {
      //  this.saveEnCours = true;
      this.updateEnCours = true;
       this.factureAchatsService.updateLigneFactureAchats(this.ligneFactureAchats).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             // this.payementCredit = new PayementCredit();
             this.openSnackBar(  'Ligne Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
