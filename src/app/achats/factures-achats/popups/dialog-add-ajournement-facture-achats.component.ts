
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {FactureAchatsService} from '../facture-achats.service';
import {AjournementFactureAchats} from '../ajournement-facture-achats.model';

@Component({
    selector: 'app-dialog-add-ajournement-facture-achats',
    templateUrl: 'dialog-add-ajournement-facture-achats.component.html',
  })
  export class DialogAddAjournementFactureAchatsComponent implements OnInit {
     private ajournementFactureAchats: AjournementFactureAchats;
     private saveEnCours = false;
     constructor(  private factureAchatsService: FactureAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.ajournementFactureAchats = new AjournementFactureAchats();
    }
    saveAjournementFactureAchats() 
    {
      this.ajournementFactureAchats.codeFactureAchats = this.data.codeFactureAchats;
      this.saveEnCours = true;
      // console.log(this.ajournementCredit);
      //  this.saveEnCours = true;
       this.factureAchatsService.addAjournementFactureAchats(this.ajournementFactureAchats).subscribe((response) => {
        this.saveEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.ajournementFactureAchats = new AjournementFactureAchats();
             this.openSnackBar(  'Ajournement Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
