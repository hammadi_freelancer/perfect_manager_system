
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {FactureAchatsService} from '../facture-achats.service';
import {DocumentFactureAchats} from '../document-facture-achats.model';
import { DocumentFactureAchatsElement } from '../document-facture-achats.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-documents-factures-achats',
    templateUrl: 'dialog-list-documents-factures-achats.component.html',
  })
  export class DialogListDocumentsFacturesAchatsComponent implements OnInit {
     private listDocumentsFacturesAchats: DocumentFactureAchats[] = [];
     private  dataDocumentsFacturesAchats: MatTableDataSource<DocumentFactureAchatsElement>;
     private selection = new SelectionModel<DocumentFactureAchatsElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private factureAchatsService: FactureAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
     // console.log('code credit in dialog component is :', this.data.codeCredit);
      this.factureAchatsService.getDocumentsFacturesAchatsByCodeFacture(this.data.codeFactureAchats).subscribe((listDocumentFacAchats) => {
        this.listDocumentsFacturesAchats = listDocumentFacAchats;
//console.log('recieving data from backend', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedDocumentsFactAchats: DocumentFactureAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentFactElement) => {
    return documentFactElement.code;
  });
  this.listDocumentsFacturesAchats.forEach(documentF => {
    if (codes.findIndex(code => code === documentF.code) > -1) {
      listSelectedDocumentsFactAchats.push(documentF);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsFacturesAchats !== undefined) {

      this.listDocumentsFacturesAchats.forEach(docFAchat => {
             listElements.push( {code: docFAchat.code ,
          dateReception: docFAchat.dateReception,
          numeroDocument: docFAchat.numeroDocument,
          typeDocument: docFAchat.typeDocument,
          description: docFAchat.description
        } );
      });
    }
      this.dataDocumentsFacturesAchats= new MatTableDataSource<DocumentFactureAchatsElement>(listElements);
      this.dataDocumentsFacturesAchats.sort = this.sort;
      this.dataDocumentsFacturesAchats.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsFacturesAchats.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsFacturesAchats.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsFacturesAchats.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsFacturesAchats.paginator) {
      this.dataDocumentsFacturesAchats.paginator.firstPage();
    }
  }

}
