
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {FactureAchatsService} from '../facture-achats.service';
import {FactureAchats} from '../facture-achats.model';
import { DocumentFactureAchats } from '../document-facture-achats.model';
@Component({
    selector: 'app-dialog-add-document-facture-achats',
    templateUrl: 'dialog-add-document-facture-achats.component.html',
  })
  export class DialogAddDocumentFactureAchatsComponent implements OnInit {
     private documentFactureAchats: DocumentFactureAchats;
     private saveEnCours = false;
     private typesDoc: any[] = [
      {value: 'Cheque', viewValue: 'Chèque'},
      {value: 'Traite', viewValue: 'Traite'},
      {value: 'FactureAchats', viewValue: 'Facture Achats'},
      {value: 'BonReception', viewValue: 'Bon Réception'},
      {value: 'ReçuPayement', viewValue: 'Reçu Payement'},
      {value: 'Autre', viewValue: 'Autre'},

    ];
     constructor(  private factureAchatsService: FactureAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.documentFactureAchats = new DocumentFactureAchats();
    }
    saveDocumentFactureAchats() 
    {
      this.documentFactureAchats.codeFactureAchats = this.data.codeFactureAchats;
       // console.log(this.documentCredit);
        this.saveEnCours = true;
       this.factureAchatsService.addDocumentFactureAchats(this.documentFactureAchats).subscribe((response) => {
         this.saveEnCours = false;
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.documentFactureAchats = new DocumentFactureAchats();
             this.openSnackBar(  'Document Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
     imageFace1Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentFactureAchats.imageFace1 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imageFace2Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentFactureAchats.imageFace2 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
  }
