
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {FactureAchatsService} from '../facture-achats.service';
import {PayementFactureAchats} from '../payement-facture-achats.model';
import {LigneFactureAchats} from '../ligne-facture-achats.model';

@Component({
    selector: 'app-dialog-add-ligne-facture-achats',
    templateUrl: 'dialog-add-ligne-facture-achats.component.html',
  })
  export class DialogAddLigneFactureAchatsComponent implements OnInit {
     private ligneFactureAchats: LigneFactureAchats;
     private listCodesArticles: string[];
     private listCodesMagasins: string[];
     private listCodesUnMesures: string[];
     
     private etatsLignes: any[] = [
      {value: 'Annule', viewValue: 'Annulé'},
      {value: 'Initial', viewValue: 'Initial'},
      {value: 'Valide', viewValue: 'Validé'}
    ];
     constructor(  private  factureAchatsService: FactureAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.ligneFactureAchats = new LigneFactureAchats();
           this.listCodesArticles = this.data.listCodesArticles;
           // this.listCodesMagasins = this.data.listCodesMagasins;
           
           this.ligneFactureAchats.etat = 'Initial';
           
    }
    saveLigneFactureAchats() 
    {
      this.ligneFactureAchats.codeFactureAchats = this.data.codeFactureAchats;
       console.log(this.ligneFactureAchats);
      //  this.saveEnCours = true;
       this.factureAchatsService.addLigneFactureAchats(this.ligneFactureAchats).subscribe((response) => {
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.ligneFactureAchats = new LigneFactureAchats();
             this.openSnackBar(  'Ligne Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
