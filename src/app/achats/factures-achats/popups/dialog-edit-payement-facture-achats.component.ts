
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {FactureAchatsService} from '../facture-achats.service';
import {FactureAchats} from '../facture-achats.model';
import { PayementFactureAchats } from '../payement-facture-achats.model';
@Component({
    selector: 'app-dialog-edit-payement-facture-achats',
    templateUrl: 'dialog-edit-payement-facture-achats.component.html',
  })
  export class DialogEditPayementFactureAchatsComponent implements OnInit {

     private payementFactureAchats: PayementFactureAchats;
     private updateEnCours = false;
     constructor(  private factureAchatsService: FactureAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit() {
        this.factureAchatsService.getPayementFactureAchats(this.data.codePayementFactureAchats).subscribe((payement) => {
            this.payementFactureAchats = payement;
     });
   }
    updatePayementFactureAchats() {
      //  this.saveEnCours = true;
      this.updateEnCours = true;
       this.factureAchatsService.updatePayementFactureAchats(this.payementFactureAchats).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             // this.payementCredit = new PayementCredit();
             this.openSnackBar(  'Payement Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
