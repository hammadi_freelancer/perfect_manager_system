
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {FactureAchatsService} from '../facture-achats.service';
import {LigneFactureAchats} from '../ligne-facture-achats.model';
import { LigneFactureAchatsElement } from '../ligne-facture-achats.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-lignes-factures-achats',
    templateUrl: 'dialog-list-lignes-factures-achats.component.html',
  })
  export class DialogListLignesFactureAchatsComponent implements OnInit {
     private listLignesFactureAchats: LigneFactureAchats[] = [];
     private  dataLignesFactureAchatsSource: MatTableDataSource<LigneFactureAchatsElement>;
     private selection = new SelectionModel<LigneFactureAchatsElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private factureAchatsService: FactureAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      // console.log('code credit in dialog component is :', this.data.codeFactureVentes);
      this.factureAchatsService.getLignesFacturesAchatsByCodeFacture(this.data.codeFactureAchats)
      .subscribe((listLignesFactAchats) => {
        this.listLignesFactureAchats= listLignesFactAchats;
// console.log('recieving data from backend', this.listPayementsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedLignesFactureAchats: LigneFactureAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((ligneFactureAchatsElement) => {
    return ligneFactureAchatsElement.code;
  });
  this.listLignesFactureAchats.forEach(ligneFactureAchats => {
    if (codes.findIndex(code => code === ligneFactureAchats.code) > -1) {
      listSelectedLignesFactureAchats.push(ligneFactureAchats);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listLignesFactureAchats !== undefined) {
 
      this.listLignesFactureAchats.forEach(ligneFactureAchats => {
             listElements.push( {code: ligneFactureAchats.code ,
              numeroLigneFactureAchats: ligneFactureAchats.numeroLigneFactureAchats,
          article: ligneFactureAchats.article.code,
          magasin: ligneFactureAchats.magasin.code,
          quantite: ligneFactureAchats.quantite,
          prixUnitaire: ligneFactureAchats.prixUnitaire,
          unMesure: ligneFactureAchats.unMesure,
          
          montantHT: ligneFactureAchats.montantHT,
          fodec: ligneFactureAchats.fodec,
          montantFodec: ligneFactureAchats.montantFodec,
          otherTaxe1: ligneFactureAchats.otherTaxe1,
          montantOtherTaxe1: ligneFactureAchats.montantOtherTaxe1,
          otherTaxe2: ligneFactureAchats.otherTaxe2,
          montantOtherTaxe2: ligneFactureAchats.montantOtherTaxe2,
          tva: ligneFactureAchats.tva,
          montantTVA: ligneFactureAchats.montantTVA,
          montantTTC: ligneFactureAchats.montantTTC,
          remise: ligneFactureAchats.remise,
          montantRemise: ligneFactureAchats.montantRemise,
          montantAPayer: ligneFactureAchats.montantAPayer,
          benefice: ligneFactureAchats.beneficeEstime,
          montantBenefice: ligneFactureAchats.montantBeneficeEstime,
          etat: ligneFactureAchats.etat,
          prixVente: ligneFactureAchats.prixVente,
          
          
        } );
      });
    }
      this.dataLignesFactureAchatsSource = new MatTableDataSource<LigneFactureAchatsElement>(listElements);
      this.dataLignesFactureAchatsSource.sort = this.sort;
      this.dataLignesFactureAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'numeroLigneFactureAchats' , displayTitle: 'N°'},
     {name: 'article' , displayTitle: 'Art'}, 
     {name: 'magasin' , displayTitle: 'Mag'},
     {name: 'quantite' , displayTitle: 'Qte'},
     {name: 'unMesure' , displayTitle: 'UM'},
      {name: 'prixUnitaire' , displayTitle: 'P.UN'},
     {name: 'montantHT' , displayTitle: 'M.HT'}, 
     {name: 'montantTVA' , displayTitle: 'M.TVA'},
     {name: 'montantTTC' , displayTitle: 'M.TTC'},
     {name: 'montantRemise' , displayTitle: 'M.Remise'},
      {name: 'montantBenefice' , displayTitle: 'M.Benefice'},
      {name: 'montantAPayer' , displayTitle: 'M.APayer'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataLignesFactureAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataLignesFactureAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataLignesFactureAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataLignesFactureAchatsSource.paginator) {
      this.dataLignesFactureAchatsSource.paginator.firstPage();
    }
  }

}
