
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {FactureAchatsService} from '../facture-achats.service';
import {PayementFactureAchats} from '../payement-facture-achats.model';

@Component({
    selector: 'app-dialog-add-payement-facture-achats',
    templateUrl: 'dialog-add-payement-facture-achats.component.html',
  })
  export class DialogAddPayementFactureAchatsComponent implements OnInit {
     private payementFactureAchats: PayementFactureAchats;
     
     constructor(  private  factureAchatsService: FactureAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.payementFactureAchats = new PayementFactureAchats();
    }
    savePayementFactureAchats() 
    {
      this.payementFactureAchats.codeFactureAchats = this.data.codeFactureAchats;
       console.log(this.payementFactureAchats);
      //  this.saveEnCours = true;
       this.factureAchatsService.addPayementFactureAchats(this.payementFactureAchats).subscribe((response) => {
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.payementFactureAchats = new PayementFactureAchats();
             this.openSnackBar(  'Payement Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
