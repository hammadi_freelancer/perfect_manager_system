
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {FactureAchatsService} from '../facture-achats.service';
import {AjournementFactureAchats} from '../ajournement-facture-achats.model';

@Component({
    selector: 'app-dialog-edit-ajournement-facture-achats',
    templateUrl: 'dialog-edit-ajournement-facture-achats.component.html',
  })
  export class DialogEditAjournementFactureAchatsComponent implements OnInit {
     private ajournementFactureAchats: AjournementFactureAchats;
     private updateEnCours = false;
     
     constructor(  private factureAchatsService: FactureAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {

        this.factureAchatsService.getAjournementFactureAchats(this.data.codeAjournementFactureAchats).subscribe((ajour) => {
            this.ajournementFactureAchats = ajour;
     });


    }
    updateAjournementFactureAchats() 
    {
     this.updateEnCours = true;
       this.factureAchatsService.updateAjournementFactureAchats(this.ajournementFactureAchats).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.openSnackBar(  'Ajournement Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
