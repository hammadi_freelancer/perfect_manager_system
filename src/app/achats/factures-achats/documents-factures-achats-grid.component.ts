
import { Component, OnInit, Inject,Input, ViewChild, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {FactureAchatsService} from './facture-achats.service';
import {FactureAchats} from './facture-achats.model';

import {DocumentFactureAchats} from './document-facture-achats.model';
import { DocumentFactureAchatsElement } from './document-facture-achats.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogAddDocumentFactureAchatsComponent } from './popups/dialog-add-document-facture-achats.component';
 import { DialogEditDocumentFactureAchatsComponent } from './popups/dialog-edit-document-facture-achats.component';
@Component({
    selector: 'app-documents-factures-achats-grid',
    templateUrl: 'documents-factures-achats-grid.component.html',
  })
  export class DocumentsFactureAchatsGridComponent implements OnInit, OnChanges {
     private listDocumentsFacturesAchats: DocumentFactureAchats[] = [];
     private  dataDocumentsFacturesAchatsSource: MatTableDataSource<DocumentFactureAchatsElement>;
     private selection = new SelectionModel<DocumentFactureAchatsElement>(true, []);
    
     @Input()
     private factureAchats : FactureAchats;

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private factureAchatsService: FactureAchatsService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.factureAchatsService.getDocumentsFacturesAchatsByCodeFacture(this.factureAchats.code).subscribe((listDocumentsFactAchats) => {
        this.listDocumentsFacturesAchats= listDocumentsFactAchats;
 //  console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsFacturesVentes  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
  }
  ngOnChanges()
  {
    this.specifyListColumnsToBeDisplayed();
    
    this.factureAchatsService.getDocumentsFacturesAchatsByCodeFacture(this.factureAchats.code).subscribe((listDocumentsFactAchats) => {
      this.listDocumentsFacturesAchats = listDocumentsFactAchats;
// console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsCredits  );
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }

  checkOneActionInvoked() {
    const listSelectedDocumentsFactAchats: DocumentFactureAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentFactElement) => {
    return documentFactElement.code;
  });
  this.listDocumentsFacturesAchats.forEach(documentF => {
    if (codes.findIndex(code => code === documentF.code) > -1) {
      listSelectedDocumentsFactAchats.push(documentF);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsFacturesAchats!== undefined) {

      this.listDocumentsFacturesAchats.forEach(docFact => {
             listElements.push( {code: docFact.code ,
          dateReception: docFact.dateReception,
          numeroDocument: docFact.numeroDocument,
          typeDocument: docFact.typeDocument,
          description: docFact.description
        } );
      });
    }
      this.dataDocumentsFacturesAchatsSource = new MatTableDataSource<DocumentFactureAchatsElement>(listElements);
      this.dataDocumentsFacturesAchatsSource.sort = this.sort;
      this.dataDocumentsFacturesAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     console.log('calling specifyListColumnsToBeDisplayed documents grid');
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
    
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
      console.log(this.columnsNames);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsFacturesAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsFacturesAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsFacturesAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsFacturesAchatsSource.paginator) {
      this.dataDocumentsFacturesAchatsSource.paginator.firstPage();
    }
  }
  showAddDocumentDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeFactureAchats: this.factureAchats.code
   };
 
   const dialogRef = this.dialog.open(DialogAddDocumentFactureAchatsComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(docAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditDocumentDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeDocumentFactureAchats : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditDocumentFactureAchatsComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(docAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Document Séléctionnée!');
    }
    }
    supprimerDocuments()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(doc, index) {
              this.factureAchatsService.deleteDocumentFactureAchats(doc.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Document Séléctionnée!');
        }
    }
    refresh() {
      this.factureAchatsService.getDocumentsFacturesAchatsByCodeFacture(this.factureAchats.code).subscribe((listDocsFactAchats) => {
        this.listDocumentsFacturesAchats = listDocsFactAchats;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
