export interface LigneFactureAchatsElement {
     code: string;
     numeroLigneFactureAchats: string;
     article: string;
     magasin: string;
     unMesure: string;
     fodec: number;
      montantFodec?: number;
      otherTaxe1?: number;
      montantOtherTaxe1: number;
      otherTaxe2: number;
      montantOtherTaxe2: number;
      beneficeEstime: number;
      montantBeneficeEstime?: number;
     quantite?: number;
     prixUnitaire?: number;
      tva?: number;
      montantTVA?: number;
     remise?: number;
      montantRemise?: number;
     montantHT?: number;
     montantTTC?: number;
      montantAPayer?: number;
      prixVente?: number;
      etat?: string;
    }
