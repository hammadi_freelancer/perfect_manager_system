import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {FactureAchats} from './facture-achats.model';

import {FactureAchatsService} from './facture-achats.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
// import { CommunicationService} from './communication.service';
import { MatDatepickerInput } from '@angular/material/datepicker';
import { MatSnackBar } from '@angular/material';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import { ColumnDefinition } from '../../common/column-definition.interface';
// import { FactureVenteFilter } from './facture-vente.filter';
import {Fournisseur} from '../fournisseurs/fournisseur.model';
import {ArticleService} from '../../stocks/articles/article.service';
import {Article} from '../../stocks/articles/article.model';
import { MatExpansionPanel } from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { LigneFactureAchats} from './ligne-facture-achats.model';
import {LigneFactureAchatsElement} from '../factures-achats/ligne-facture-achats.interface';
import {SelectionModel} from '@angular/cdk/collections';
import { ArticleFilter } from './article.filter';
import { FournisseurFilter } from '../fournisseurs/fournisseur.filter';

import {Magasin} from '../../stocks/magasins/magasin.model';
import {UnMesure} from '../../stocks/un-mesures/un-mesure.model';
import {ModePayementLibelle} from './mode-payement-libelle.interface';
import {MagasinFilter} from '../../stocks/magasins/magasin.filter';
import {UnMesureFilter} from '../../stocks/un-mesures/un-mesures.filter';

@Component({
  selector: 'app-facture-achats-edit',
  templateUrl: './facture-achats-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class FactureAchatsEditComponent implements OnInit  {

  private factureAchats: FactureAchats =  new FactureAchats();
  private updateEnCours = false;
  private tabsDisabled = true;
  private managedLigneFactureAchats = new LigneFactureAchats();
  private selection = new SelectionModel<LigneFactureAchatsElement>(true, []);
  @ViewChild(MatPaginator) paginator: MatPaginator; 
  @ViewChild(MatSort) sort: MatSort;
  private columnsDefinitions: ColumnDefinition[] = [];
  private columnsTitles: string[] = [];
  private columnsTitlesAr: string[] = [];
  private columnsNames: string[] = [];
  
@Input()
private listFacturesAchats: any = [] ;

@ViewChild('firstMatExpansionPanel')
private firstMatExpansionPanel : MatExpansionPanel;

@ViewChild('manageLigneMatExpansionPanel')
private manageLigneMatExpansionPanel : MatExpansionPanel;

@ViewChild('listLignesMatExpansionPanel')
private listLignesMatExpansionPanel : MatExpansionPanel;

// private listCodesArticles: string[];
private lignesFacturesAchats: LigneFactureAchats[] = [ ];
// private listTranches: Tranche[] = [] ;
// private factureAchatsFilter: FactureAchatsFilter;
private articleFilter: ArticleFilter;
private magasinFilter: MagasinFilter;
private unMesureFilter: UnMesureFilter;
private listFournisseurs: Fournisseur[];
private fournisseursFilter: FournisseurFilter;
private listMagasins:Magasin[];
private listArticles:Article[];
// private listArticles: Article[];
private listLignesFactureAchats: LigneFactureAchats[] = [ ];
private  dataLignesFactureAchatsSource: MatTableDataSource<LigneFactureAchatsElement>;

// private editLignesMode: boolean [] = [];

private etatsFactureAchats: any[] = [
  {value: 'Annule', viewValue: 'Annulé'},
  {value: 'Initial', viewValue: 'Initial'},
  {value: 'Valide', viewValue: 'Validé'}
];
private etatsLigneFactureAchats: any[] = [
  {value: 'Regle', viewValue: 'Réglée'},
  {value: 'NonRegle', viewValue: 'NonRéglée'},
  {value: 'PartiellementRegle', viewValue: 'Partiellement Réglée'}
];
private payementModes: any[] = [
  {value: 'Comptant', viewValue: 'Comptant'},
  {value: 'Cheque', viewValue: 'Chèque'},
  {value: 'Credit', viewValue: 'Crédit'},
  {value: 'Avance', viewValue: 'Avance'},
  {value: 'Comptant_Cheque', viewValue: 'Comptant/Chèque'},
  {value: 'Comptant_Avance', viewValue: 'Comptant/Avance'},
  {value: 'Credit_Comptant', viewValue: 'Crédit/Comptant'},
  {value: 'Credit_Cheque', viewValue: 'Crédit/Chèque'},
  {value: 'Credit_Avance', viewValue: 'Crédit/Avance'},
  {value: 'Donnation', viewValue: 'Amicalité'}
];
  constructor( private route: ActivatedRoute,
    private router: Router, private factureAchatsService: FactureAchatsService,
    private articleService: ArticleService, private dialog: MatDialog,
    private matSnackBar: MatSnackBar, private elRef: ElementRef,
   ) {
  }
  ngOnInit() {
    this.listFournisseurs = this.route.snapshot.data.fournisseurs;
   this.fournisseursFilter = new FournisseurFilter(this.listFournisseurs);

      this.unMesureFilter = new UnMesureFilter(this.route.snapshot.data.unMesures);
      this.magasinFilter = new MagasinFilter(this.route.snapshot.data.magasins);
      
      //  this.factureVenteFilter = new FactureVenteFilter(this.listClients, []);
      this.articleFilter = new ArticleFilter(this.route.snapshot.data.articles,
       [], [] );
       this.listArticles = this.route.snapshot.data.articles;
       this.listMagasins = this.route.snapshot.data.magasins;
       
    const codeFactureAchats = this.route.snapshot.paramMap.get('codeFactureAchats');
    this.factureAchatsService.getFactureAchats(codeFactureAchats).subscribe((factureAchats) => {
             this.factureAchats = factureAchats;
             if (this.factureAchats.etat === 'Valide')
              {
                this.tabsDisabled = false;
              }
             if (this.factureAchats.etat === 'Valide' || this.factureAchats.etat === 'Annule')
              {
                this.etatsFactureAchats.splice(1, 1);
              }
              if (this.factureAchats.etat === 'Initial')
                {
                  this.etatsFactureAchats.splice(0, 1);
                }
            });
            this.specifyListColumnsToBeDisplayed();
            this.transformDataToByConsumedByMatTable();
            this.firstMatExpansionPanel.open();
            
  }
  loadGridFacturesAchats() {
    this.router.navigate(['/pms/achats/facturesAchats']) ;
  }
updateFactureAchats() {

 //  console.log(this.factureVente);
  let noClient = false;
  let noRow = false;
  if (this.factureAchats.etat === 'Valide' )  {
    if ( this.factureAchats.fournisseur.cin === null ||  this.factureAchats.fournisseur.cin === undefined)  {
        noClient = true;
        
      }
  if (this.factureAchats.listLignesFacturesAchats[0] === undefined
    || this.factureAchats.listLignesFacturesAchats[0].quantite === 0  ) {
      noRow = true;
    }
  }
    if (noClient)
      {
        this.openSnackBar( 'Vous Venez de valider la facture sans préciser les informations de client! ');
        this.factureAchats.etat = 'Initial';
      } else if (noRow)  {
          this.openSnackBar( 'La Facture doit avoir au moins une ligne valide !');
          
        } else {
  this.updateEnCours = true;
  this.factureAchatsService.updateFactureAchats(this.factureAchats).subscribe((response) => {
    this.updateEnCours = false;
    if (response.error_message ===  undefined) {
      console.log('the response of update is ', response );
      if (this.factureAchats.etat === 'Valide')
        {
          this.tabsDisabled = false;
          this.etatsFactureAchats = [
            {value: 'Annule', viewValue: 'Annulé'},
            {value: 'Valide', viewValue: 'Validé'},
            {value: 'Reglee', viewValue: 'Réglée'},
            {value: 'Valide', viewValue: 'Partiellement Réglée'},
            
  
          ];
        }
        if (this.factureAchats.etat === 'Annule')
          {
            this.tabsDisabled = true;
         
          }

      this.openSnackBar( 'Facture d\'achats mise à jour ');
      //  this.save.emit(response);
      // this.client = new Client();
      // this.modeUpdate = false;
    } else {
      this.openSnackBar( ' Erreurs:Opération Echouée ');
     // show error message
    }
});
        }
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 


  calculMontantAPayer()
  {
  this.factureAchats.montantAPayer = +this.factureAchats.timbreFiscale + this.factureAchats.montantTTC;
  
  return this.factureAchats.montantAPayer ;
  }
  calculMontantApayerApresRemise()
  {
    
  this.factureAchats.montantAPayerApresRemise = +this.factureAchats.montantAPayer - this.factureAchats.montantRemise;
    return this.factureAchats.montantAPayerApresRemise;
  }
claculMontantRemise() {
  const prixVente   =  +(+this.managedLigneFactureAchats.montantTTC / +this.managedLigneFactureAchats.quantite)
  ;
  if (!isNaN(prixVente)) {
  console.log(prixVente);
this.managedLigneFactureAchats.montantRemise = +this.managedLigneFactureAchats.remise  * ( +prixVente / 100) *
 this.managedLigneFactureAchats.quantite;
// console.log(this.lignesFactures[i].montantRemise);
  }
return this.managedLigneFactureAchats.montantRemise;
}
calculMontantHT()
{
this.managedLigneFactureAchats.montantHT = +this.managedLigneFactureAchats.prixUnitaire * this.managedLigneFactureAchats.quantite;
return this.managedLigneFactureAchats.montantHT;
}
calculMontantTTC()
{
  this.managedLigneFactureAchats.montantTTC = +(+this.managedLigneFactureAchats.prixUnitaire * this.managedLigneFactureAchats.quantite)
  + this.managedLigneFactureAchats.montantTVA;
  
  return  this.managedLigneFactureAchats.montantTTC;
}
calculMontantTVA() {

  this.managedLigneFactureAchats.montantTVA =  (this.managedLigneFactureAchats.prixUnitaire / 100) *
  this.managedLigneFactureAchats.tva * this.managedLigneFactureAchats.quantite;
  this.managedLigneFactureAchats.montantTTC = +(+this.managedLigneFactureAchats.prixUnitaire * this.managedLigneFactureAchats.quantite)
  + this.managedLigneFactureAchats.montantTVA;
  // console.log('montant tva');
 // console.log(this.factureVente.montantTVA + this.listLignesFactureVentes[i].montantTVA);
 if(this.managedLigneFactureAchats.remise !== 0) {
  this.claculMontantRemise() ;
}  
  return  this.managedLigneFactureAchats.montantTVA;
}

calculMontantBenefice() {
  
      this.managedLigneFactureAchats.montantBeneficeEstime =  (this.managedLigneFactureAchats.prixUnitaire / 100) *
      this.managedLigneFactureAchats.beneficeEstime * this.managedLigneFactureAchats.quantite;
      return  this.managedLigneFactureAchats.montantBeneficeEstime;
    }


calculMontant4thTaxe() {
      
    this.managedLigneFactureAchats.montantOtherTaxe2 =  (this.managedLigneFactureAchats.prixUnitaire / 100) *
          this.managedLigneFactureAchats.otherTaxe2 * this.managedLigneFactureAchats.quantite;
          if(this.managedLigneFactureAchats.montantTTC === 0 ) {
            
     this.managedLigneFactureAchats.montantTTC = +(+this.managedLigneFactureAchats.prixUnitaire * this.managedLigneFactureAchats.quantite)
          + this.managedLigneFactureAchats.montantOtherTaxe2;
          } else {
            this.managedLigneFactureAchats.montantTTC = + this.managedLigneFactureAchats.montantTTC
            + this.managedLigneFactureAchats.montantOtherTaxe2;
          }

          if(this.managedLigneFactureAchats.remise !== 0) {
            this.claculMontantRemise() ;
          }  
          return  this.managedLigneFactureAchats.montantOtherTaxe2;
        }

calculMontant3rdTaxe() {
          
       this.managedLigneFactureAchats.montantOtherTaxe1 =  (this.managedLigneFactureAchats.prixUnitaire / 100) *
              this.managedLigneFactureAchats.otherTaxe1 * this.managedLigneFactureAchats.quantite;
             if(this.managedLigneFactureAchats.montantTTC === 0 ) {
      this.managedLigneFactureAchats.montantTTC = +(+this.managedLigneFactureAchats.prixUnitaire
        * this.managedLigneFactureAchats.quantite)
              + this.managedLigneFactureAchats.montantOtherTaxe1;
               } else {
                this.managedLigneFactureAchats.montantTTC = + this.managedLigneFactureAchats.montantTTC
                        + this.managedLigneFactureAchats.montantOtherTaxe1;
               }
               if(this.managedLigneFactureAchats.remise !== 0) {
                this.claculMontantRemise() ;
              }  
              return  this.managedLigneFactureAchats.montantOtherTaxe1;
            }

    calculMontantFodec() {
              
           this.managedLigneFactureAchats.montantFodec =  (this.managedLigneFactureAchats.prixUnitaire / 100) *
                  this.managedLigneFactureAchats.fodec * this.managedLigneFactureAchats.quantite;
                  if(this.managedLigneFactureAchats.montantTTC === 0 ) {
                    
                  this.managedLigneFactureAchats.montantTTC = +(+this.managedLigneFactureAchats.prixUnitaire *
                     this.managedLigneFactureAchats.quantite)
                  + this.managedLigneFactureAchats.montantFodec;
                  } else  {
                    this.managedLigneFactureAchats.montantTTC = + this.managedLigneFactureAchats.montantTTC
                    + this.managedLigneFactureAchats.montantFodec;
                  }
                  if(this.managedLigneFactureAchats.remise !== 0) {
                    this.claculMontantRemise() ;
                  }  
                  return  this.managedLigneFactureAchats.montantFodec;
                }
                transformDataToByConsumedByMatTable() {
                  const listElements = [];
                
                  if (this.factureAchats.listLignesFacturesAchats !== undefined) {
                    this.factureAchats.listLignesFacturesAchats.forEach(ligneFactA => {
                           listElements.push( {code: ligneFactA.code ,
                            numeroLigneFactureAchats: ligneFactA.numeroLigneFactureAchats,
                            article: ligneFactA.article.code,
                             magasin: ligneFactA.magasin.code,
                             unMesure: ligneFactA.unMesure.code,
                             quantite: ligneFactA.quantite,
                             prixUnitaire: ligneFactA.prixUnitaire,
                             tva: ligneFactA.tva,
                             montantTVA: ligneFactA.montantTVA,
                             remise: ligneFactA.remise,
                             montantRemise: ligneFactA.montantRemise,
                             montantHT: ligneFactA.montantHT,
                             montantTTC: ligneFactA.montantTTC,
                             etat: ligneFactA.etat
                      } );
                    });
                  }
                  console.log('in transformDataToByConsumedByMatTable listLignesFacturesAchats is:');
                  console.log(this.factureAchats.listLignesFacturesAchats);
                    this.dataLignesFactureAchatsSource= new MatTableDataSource<LigneFactureAchatsElement>(listElements);
                    this.dataLignesFactureAchatsSource.sort = this.sort;
                    this.dataLignesFactureAchatsSource.paginator = this.paginator;
                    
                    
                }
        specifyListColumnsToBeDisplayed() {
                   // console.log('calling specifyListColumnsToBeDisplayed documents grid');
                  this.columnsTitles = [];
                  this.columnsNames = [];
                  this.columnsTitlesAr  = [];
                  
                   this.columnsDefinitions = [{name: 'numeroLigneFactureAchats' , displayTitle: 'N°'},
                   {name: 'article' , displayTitle: 'Article'},
                   {name: 'magasin' , displayTitle: 'Magasin'},
                   {name: 'unMesure' , displayTitle: 'Un Mesure'},
                   {name: 'quantite' , displayTitle: 'Quantite'},
                   {name: 'prixUnitaire' , displayTitle: 'Prix Unitaire'},
                   {name: 'tva' , displayTitle: 'TVA'},
                   {name: 'montantTVA' , displayTitle: 'Montant TVA'},
                   {name: 'montantHT' , displayTitle: 'Montant HT'},
                   { name : 'montantTTC' , displayTitle : 'Montant TTC'},
                  
                    ];
                
                   this.columnsTitles = this.columnsDefinitions.map((colDef) => {
                             return colDef.displayTitle;
                   });
                   
                   this.columnsNames[0] = 'select';
                   this.columnsDefinitions.map((colDef) => {
                    this.columnsNames.push(colDef.name);
                    console.log(this.columnsNames);
                });
                   this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
                    return colDef.displayTitleAr;
                });
                }

 
      getIndexOfRow(row): number
      {
      
          for ( let i = 0; i < this.factureAchats.listLignesFacturesAchats.length ; i++) {
            if (this.factureAchats.listLignesFacturesAchats[i].numeroLigneFactureAchats
               === row.numeroLigneFactureAchats)
             {
                   return i;
              }
          }
          return -1;
      }
      reglerLigneFactureAchats(row)
      {
        this.managedLigneFactureAchats= this.mapRowGridToObjectLigneFacture(row);
        this.managedLigneFactureAchats.etat = 'Regle';
        const index = this.getIndexOfRow(row);
        this.factureAchats.listLignesFacturesAchats[index] = this.managedLigneFactureAchats;
      }

      mapRowGridToObjectLigneFacture(rowGrid): LigneFactureAchats
      {
        const ligneFA  = new  LigneFactureAchats() ;
        ligneFA.numeroLigneFactureAchats = rowGrid.numeroLigneFactureAchats;
        ligneFA.article.code =  rowGrid.article;
        ligneFA.magasin.code =  rowGrid.magasin;
        ligneFA.unMesure.code =  rowGrid.unMesure;
        ligneFA.quantite =  rowGrid.quantite;
        ligneFA.prixUnitaire =  rowGrid.prixUnitaire;
        ligneFA.montantHT =  rowGrid.montantHT;
        ligneFA.montantTTC =  rowGrid.montantTTC;
        ligneFA.tva =  rowGrid.tva;
        ligneFA.montantTVA =  rowGrid.montantTVA;
        ligneFA.fodec =  rowGrid.fodec;
        ligneFA.montantFodec =  rowGrid.montantFodec;
        ligneFA.remise =  rowGrid.remise;
        ligneFA.montantRemise =  rowGrid.montantRemise;
       
        ligneFA.otherTaxe1 =  rowGrid.otherTaxe1;
        ligneFA.montantOtherTaxe1 =  rowGrid.montantOtherTaxe1;
        ligneFA.otherTaxe2 =  rowGrid.otherTaxe2;
        ligneFA.montantOtherTaxe2 =  rowGrid.montantOtherTaxe2;
        ligneFA.montantBeneficeEstime =  rowGrid.montantBeneficeEstime;
        ligneFA.beneficeEstime =  rowGrid.beneficeEstime;
        return ligneFA;
      }
      editLigneFactureAchats(row)
      {
        this.managedLigneFactureAchats= this.mapRowGridToObjectLigneFacture(row);
      
        if(row.magasin === undefined)
          {
            this.managedLigneFactureAchats.magasin = new Magasin();
          }
          if(row.unMesure === undefined)
            {
              this.managedLigneFactureAchats.unMesure = new UnMesure();
            }
            if(row.article === undefined)
              {
                this.managedLigneFactureAchats.article = new Article();
              }
              console.log('row to edit is ', row);
      
        this.manageLigneMatExpansionPanel.open();
        
      }
      deleteLigneFactureAchats(row)
      {
      
        const indexOfRow = this.getIndexOfRow(row);
        this.factureAchats.listLignesFacturesAchats.splice(indexOfRow , 1 );
        this.transformDataToByConsumedByMatTable();
        
      }
   
      addLigneFactureAchats()
      {
        if (this.managedLigneFactureAchats.quantite === undefined
          || this.managedLigneFactureAchats.quantite === 0 ||
          this.managedLigneFactureAchats.quantite === null ||
          this.managedLigneFactureAchats.prixUnitaire === undefined
            || this.managedLigneFactureAchats.prixUnitaire === 0 ||
            this.managedLigneFactureAchats.prixUnitaire === null  ||
            this.managedLigneFactureAchats.article.code === undefined  ||
            this.managedLigneFactureAchats.article.code === '' ||
            this.managedLigneFactureAchats.article.code === null 
        ) {
              this.openSnackBar('Donnnées Manquées (QTE/ART/PUNIT!');
          } else if (this.managedLigneFactureAchats.numeroLigneFactureAchats === undefined
        || this.managedLigneFactureAchats.numeroLigneFactureAchats === '' ||
        this.managedLigneFactureAchats.numeroLigneFactureAchats=== null ) {
            this.openSnackBar('Numéro de Ligne Non Spécifié !');
          } else {
        this.managedLigneFactureAchats.numeroFactureAchats = this.factureAchats.numeroFactureAchats;

        if (this.factureAchats.listLignesFacturesAchats.length === 0) {
        this.factureAchats.listLignesFacturesAchats.push(this.managedLigneFactureAchats);
        } else  {
          console.log('look for index:');
           const indexOfRow = this.getIndexOfRow(this.managedLigneFactureAchats);
           console.log(indexOfRow);
                 if (indexOfRow === -1) {
          this.factureAchats.listLignesFacturesAchats.push(this.managedLigneFactureAchats);
      
               } else  {
           this.factureAchats.listLignesFacturesAchats[indexOfRow] = this.managedLigneFactureAchats;
      
             }
            }
            
        }
      
        this.transformDataToByConsumedByMatTable() ;
        this.managedLigneFactureAchats = new LigneFactureAchats();
        this.manageLigneMatExpansionPanel.close();
        this.listLignesMatExpansionPanel.open();
      
      }


      applyFilter(filterValue: string) {
        this.dataLignesFactureAchatsSource.filter = filterValue.trim().toLowerCase();
        if (this.dataLignesFactureAchatsSource.paginator) {
          this.dataLignesFactureAchatsSource.paginator.firstPage();
        }
      }

      selectedTabChanged($event) {
        if ($event.index === 4) { // Composition tab was selected
         // this.loadListArticles();
         } else if ($event.index === 1) {
          //  this.loadListSchemasConfiguration() ;
         }
        }

        fillDataLibelleMagasin()
        {
          this.listMagasins.forEach((magasin, index) => {
            if (this.managedLigneFactureAchats.magasin.code === magasin.code) {
             this.managedLigneFactureAchats.magasin.description = magasin.description;
             this.managedLigneFactureAchats.magasin.adresse = magasin.adresse;
             
             }
      
       } , this);
        }
        fillDataLibelleArticle()  {
          
                  this.listArticles.forEach((article, index) => {
                       if (this.managedLigneFactureAchats.article.code === article.code) {
                        this.managedLigneFactureAchats.article.designation = article.designation;
                        this.managedLigneFactureAchats.article.libelle = article.libelle;
                        
                        }
          
                  } , this);
         }
}
