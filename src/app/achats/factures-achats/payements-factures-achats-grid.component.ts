
import { Component, OnInit, Inject, ViewChild, Input, AfterViewChecked, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {FactureAchatsService} from './facture-achats.service';
import { FactureAchats} from './facture-achats.model';
import {PayementFactureAchats} from './payement-facture-achats.model';
import { PayementFactureAchatsElement } from './payement-facture-achats.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
 import { DialogAddPayementFactureAchatsComponent} from './popups/dialog-add-payement-facture-achats.component';
 import { DialogEditPayementFactureAchatsComponent } from './popups/dialog-edit-payement-facture-achats.component';
@Component({
    selector: 'app-payements-factures-achats-grid',
    templateUrl: 'payements-factures-achats-grid.component.html',
  })
  export class PayementsFacturesAchatsGridComponent implements OnInit, OnChanges {
     private listPayementsFacturesAchats: PayementFactureAchats[] = [];
     private  dataPayementsFacturesAchatsSource: MatTableDataSource<PayementFactureAchatsElement>;
     private selection = new SelectionModel<PayementFactureAchatsElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];

     @Input()
     private factureAchats: FactureAchats;

     constructor(  private factureAchatsService: FactureAchatsService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      // console.log('credit code after dipslay tab ', this.credit.code);
      this.factureAchatsService.getPayementsFacturesAchatsByCodeFacture(this.factureAchats.code).subscribe((listPayementsFactAchats) => {
        this.listPayementsFacturesAchats = listPayementsFactAchats;
// console.log('recieving data from backend', this.listPayementsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }

  ngOnChanges() {
    this.factureAchatsService.getPayementsFacturesAchatsByCodeFacture(this.factureAchats.code).subscribe((listPayementsFactAchats) => {
      this.listPayementsFacturesAchats = listPayementsFactAchats;
// console.log('recieving data from backend', this.listPayementsCredits  );
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }
  checkOneActionInvoked() {
    const listSelectedPayementsFactAchats: PayementFactureAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((payementFactElement) => {
    return payementFactElement.code;
  });
  this.listPayementsFacturesAchats.forEach(payementF => {
    if (codes.findIndex(code => code === payementF.code) > -1) {
      listSelectedPayementsFactAchats.push(payementF);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listPayementsFacturesAchats !== undefined) {

      this.listPayementsFacturesAchats.forEach(payementFactAchats => {
             listElements.push( {code: payementFactAchats.code ,
          dateOperation: payementFactAchats.dateOperation,
          montantPaye: payementFactAchats.montantPaye,
          modePayement: payementFactAchats.modePayement,
          numeroCheque: payementFactAchats.numeroCheque,
          dateCheque: payementFactAchats.dateCheque,
          compte: payementFactAchats.compte,
          compteAdversaire: payementFactAchats.compteAdversaire,
          banque: payementFactAchats.banque,
          banqueAdversaire: payementFactAchats.banqueAdversaire,
          description: payementFactAchats.description
        } );
      });
    }
      this.dataPayementsFacturesAchatsSource = new MatTableDataSource<PayementFactureAchatsElement>(listElements);
      this.dataPayementsFacturesAchatsSource.sort = this.sort;
      this.dataPayementsFacturesAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'}, 
     {name: 'montantPaye' , displayTitle: 'Montant Payé'},
     {name: 'modePayement' , displayTitle: 'Mode Payement'},
      {name: 'numeroCheque' , displayTitle: 'N°Chèque'},
     {name: 'dateCheque' , displayTitle: 'Date Chèque'}, 
     {name: 'compte' , displayTitle: 'Compte'},
     {name: 'compteAdversaire' , displayTitle: 'Compte Adv.'},
      {name: 'banque' , displayTitle: 'Banque'},
       { name : 'banqueAdversaire' , displayTitle : 'Banque Adv.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataPayementsFacturesAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataPayementsFacturesAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataPayementsFacturesAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataPayementsFacturesAchatsSource.paginator) {
      this.dataPayementsFacturesAchatsSource.paginator.firstPage();
    }
  }

  showAddPayementDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeFactureAchats : this.factureAchats.code
   };
 
   const dialogRef = this.dialog.open(DialogAddPayementFactureAchatsComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(payAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditPayementDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codePayementFactureAchats: this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditPayementFactureAchatsComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(payAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Paiement Séléctionnée!');
    }
    }
    supprimerPayements()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(pay, index) {
              this.factureAchatsService.deletePayementFactureAchats(pay.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar(  ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Paiement Séléctionnée!');
        }
    }
    refresh() {
      this.factureAchatsService.getPayementsFacturesAchatsByCodeFacture(this.factureAchats.code).subscribe((listPayementsFacs) => {
        this.listPayementsFacturesAchats = listPayementsFacs;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
















}
