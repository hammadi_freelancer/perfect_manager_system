
import { Component, OnInit, Inject, ViewChild, Input, OnChanges} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {FactureAchatsService} from './facture-achats.service';
import { FactureAchats} from './facture-achats.model';
import {AjournementFactureAchats} from './ajournement-facture-achats.model';
import { AjournementFactureAchatsElement } from './ajournement-facture-achats.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import { DialogAddAjournementFactureAchatsComponent } from './popups/dialog-add-ajournement-facture-achats.component';
import { DialogEditAjournementFactureAchatsComponent} from './popups/dialog-edit-ajournement-facture-achats.component';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-ajournements-factures-achats-grid',
    templateUrl: 'ajournements-factures-achats-grid.component.html',
  })
  export class AjournementsFacturesAchatsGridComponent implements OnInit, OnChanges {
     private listAjournementFacturesAchats: AjournementFactureAchats[] = [];
     private  dataAjournementsFacturesAchatsSource: MatTableDataSource<AjournementFactureAchatsElement>;
     private selection = new SelectionModel<AjournementFactureAchatsElement>(true, []);

     @Input()
     private factureAchats : FactureAchats;

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private factureAchatsService: FactureAchatsService,
     private matSnackBar: MatSnackBar, public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.factureAchatsService.getAjournementsFacturesAchatsByCodeFacture(this.factureAchats.code).
      subscribe((listAjournementsFactureAchats) => {
        this.listAjournementFacturesAchats = listAjournementsFactureAchats;
// console.log('recieving data from backend', this.listAjournementsFactureVentes  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }

ngOnChanges()
{
  this.factureAchatsService.getAjournementsFacturesAchatsByCodeFacture
  (this.factureAchats.code).subscribe((listAjournementsFactureAchats) => {
    this.listAjournementFacturesAchats = listAjournementsFactureAchats;
//console.log('recieving data from backend', this.listAjournementCredits  );
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
     });
}
  checkOneActionInvoked() {
    const listSelectedAjournementsFacturesAchats: AjournementFactureAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((ajournementFactureAchatsElement) => {
    return ajournementFactureAchatsElement.code;
  });
  this.listAjournementFacturesAchats.forEach(ajournementFactAc => {
    if (codes.findIndex(code => code === ajournementFactAc.code) > -1) {
      listSelectedAjournementsFacturesAchats.push(ajournementFactAc);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listAjournementFacturesAchats !== undefined) {

      this.listAjournementFacturesAchats.forEach(ajournementAch => {
             listElements.push( {code: ajournementAch.code ,
          dateOperation: ajournementAch.dateOperation,
          nouvelleDatePayement: ajournementAch.nouvelleDatePayement,
          motif: ajournementAch.motif,
          description: ajournementAch.description
        } );
      });
    }
      this.dataAjournementsFacturesAchatsSource = new MatTableDataSource<AjournementFactureAchatsElement>(listElements);
      this.dataAjournementsFacturesAchatsSource.sort = this.sort;
      this.dataAjournementsFacturesAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'},
     {name: 'nouvelleDatePayement' , displayTitle: 'N°elle Date Pay.'},
     {name: 'motif' , displayTitle: 'Motif'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataAjournementsFacturesAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataAjournementsFacturesAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataAjournementsFacturesAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataAjournementsFacturesAchatsSource.paginator) {
      this.dataAjournementsFacturesAchatsSource.paginator.firstPage();
    }
  }

  showAddAjournementDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeFactureAchats: this.factureAchats.code
   };
 
   const dialogRef = this.dialog.open(DialogAddAjournementFactureAchatsComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(ajAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditAjournementDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeAjournementFactureAchats : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditAjournementFactureAchatsComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(ajAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Ajournement Séléctionnée!');
    }
    }
    supprimerAjournements()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(ajour, index) {
              this.factureAchatsService.deleteAjournementFactureAchats(ajour.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Ajournement Séléctionnée!');
        }
    }
    refresh() {
      this.factureAchatsService.getAjournementsFacturesAchats().
      subscribe((listAjourFact) => {
        this.listAjournementFacturesAchats= listAjourFact;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }


}
