
export class FactureAchatsDataFilter {
    numeroFactureAchats: string ;
    nomFournisseur: string ;
    prenomFournisseur: string;
    cinFournisseur: string;
    raisonFournisseur: string;
    matriculeFournisseur : string;
    registreFournisseur : string;
    dateFacturationFromObject : Date;
    dateFacturationToObject : Date;
    dateAchatsFromObject : Date;
    dateAchatsToObject : Date;
    montantAPayer : number;
    etat : string;
        }



        