import { Component, OnInit, Output,ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {FactureAchats} from './facture-achats.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {FactureAchatsService} from './facture-achats.service';
// import { ActionData } from './action-data.interface';
import { Fournisseur } from '../../achats/fournisseurs/fournisseur.model';
import { FactureAchatsElement } from './facture-achats.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DialogAddFournisseurComponent } from '../fournisseurs/dialog-add-fournisseur.component';
import { DialogAddPayementFactureAchatsComponent} from './popups/dialog-add-payement-facture-achats.component';
import { FactureAchatsDataFilter} from './facture-achats-data.filter';
import { SelectItem } from 'primeng/api';

@Component({
  selector: 'app-factures-achats-grid',
  templateUrl: './factures-achats-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class FacturesAchatsGridComponent implements OnInit {

  private factureAchats: FactureAchats =  new FactureAchats();
  private factureAchatsDataFilter = new FactureAchatsDataFilter();
  private  showSelectColumnsMultiSelect = false;
  private  showFilter = false;
  private showFilterFournisseur = false;
  private selection = new SelectionModel<FactureAchatsElement>(true, []);
  
@Output()
select: EventEmitter<FactureAchats[]> = new EventEmitter<FactureAchats[]>();

//private lastFournisseurAdded: Fournisseur;

 selectedFactureAchats: FactureAchats;

 

 @Input()
 private listFacturesAchats: any = [] ;
 listFacturesAchatsOrgin: any = [];
 private checkedAll: false;

 private  dataFacturesAchatsSource: MatTableDataSource<FactureAchatsElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
 
 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay :ColumnDefinition[] = [];

 private columnsToSelect : SelectItem[];
  private  selectedColumnsDefinitions: string[];

 private selectedColumnsNames: string[];

 private etatsFactureAchats: any[] = [
  {value: 'Annule', viewValue: 'Annulé'},
  {value: 'Initial', viewValue: 'Initial'},
  {value: 'Valide', viewValue: 'Validé'}
];
private payementModes: any[] = [
  {value: 'Comptant', viewValue: 'Comptant'},
  {value: 'Cheque', viewValue: 'Chèque'},
  {value: 'Credit', viewValue: 'Crédit'},
  {value: 'Avance', viewValue: 'Avance'},
  {value: 'Comptant_Cheque', viewValue: 'Comptant/Chèque'},
  {value: 'Comptant_Avance', viewValue: 'Comptant/Avance'},
  {value: 'Credit_Comptant', viewValue: 'Crédit/Comptant'},
  {value: 'Credit_Cheque', viewValue: 'Crédit/Chèque'},
  {value: 'Credit_Avance', viewValue: 'Crédit/Avance'},
  {value: 'Donnation', viewValue: 'Amicalité'}
];
  constructor( private route: ActivatedRoute, private factureAchatsService: FactureAchatsService,
    private router: Router, private matSnackBar: MatSnackBar,
  private  dialog: MatDialog ) {
  }
  ngOnInit() {
    this.columnsToSelect = [
      {label: 'N° Facture ', value: 'numeroFactureAchats', title: 'N°Facture'},
      {label: 'Date Achats', value: 'dateAchats', title: 'Date Achats'},
      {label: 'Date Facturation', value: 'dateFacturation', title: 'Date Facturation'},
      {label: 'Montant à Payer', value: 'montantAPayer', title: 'Montant à Payer'},
      {label: 'Montant TTC', value: 'montantTTC', title: 'Montant TTC'},
      {label: 'Montant HT', value: 'montantHT', title: 'Montant HT'},
      {label: 'Montant TVA', value: 'montantTVA', title: 'Montant TVA'},
      {label: 'Mode Paiement', value: 'modePayement', title: 'Mode Paiement'},
      {label: 'Rs.Soc.Fournisseur', value:  'raisonSociale', title: 'Rs.Soc.Fournisseur' },
      {label: 'Matr.Fournisseur', value:  'matriculeFiscale', title: 'Matr.Fournisseur' },
      {label: 'Reg.Fournisseur', value:  'registreCommerce', title: 'Reg.Fournisseur' },
      {label: 'CIN Fournisseur', value: 'cin', title: 'CIN Fournisseur'},  
      {label: 'Nom Fournisseur', value: 'nom', title: 'Nom Fournisseur'},
      {label: 'Prénom Fournisseur', value: 'prenom', title: 'Prénom Fournisseur'},
      {label: 'Etat', value:  'etat', title: 'Etat' },
  
  ];
  this.selectedColumnsDefinitions = [
    'numeroFactureAchats',
    'dateAchats' , 
    'montantAPayer', 
    'nom' ,
    'prenom',
    'cin', 
    'etat'
    ];
    this.defaultColumnsDefinitions = [
      {name: 'numeroFactureAchats' , displayTitle: 'N°Facture'},
      {name: 'dateAchats' , displayTitle: 'Date Achats'},
      {name: 'dateFacturation' , displayTitle: 'Date Facturation'},
      {name: 'montantAPayer' , displayTitle: 'Montant à Payer'},
      {name: 'montantTTC' , displayTitle: 'Montant TTC'},
      {name: 'montantHT' , displayTitle: 'Montant HT'},
      {name: 'montantTVA' , displayTitle: 'Montant TVA'},
      {name: 'modePayement' , displayTitle: 'Mode Paiement'},
       {name: 'nom' , displayTitle: 'Nom Four.'},
      {name: 'prenom' , displayTitle: 'Prénom Four.'},
      {name: 'cin' , displayTitle: 'CIN Four.'},
      {name: 'matriculeFiscale' , displayTitle: 'Matr. Four.'},
      {name: 'raisonSociale' , displayTitle: 'Raison Four.'},
      {name: 'registreCommerce' , displayTitle: 'Registre Four.'},
       {name: 'etat', displayTitle: 'Etat' },
      ];

    this.factureAchatsService.getPageFacturesAchats(0, 5, null).subscribe((facturesAchats) => {
      this.listFacturesAchats = facturesAchats;
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });


  }
  deleteFactureAchats(factureAchats) {
      // console.log('call delete !', factureVente );
    this.factureAchatsService.deleteFactureAchats(factureAchats.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Fournisseur();
        console.log('call delete !', response);
        this.loadData(null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.factureAchatsService.getPageFacturesAchats(0, 5,filter).subscribe((facturesAchats) => {
    this.listFacturesAchats = facturesAchats;
    this.transformDataToByConsumedByMatTable();
   });
}


checkOneActionInvoked() {
  const listSelectedFacturesAchats: FactureAchats[] = [];
console.log('selected clients are ');
console.log(this.selection.selected);
if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((factureAchatsElement) => {
  return factureAchatsElement.code;
});
this.listFacturesAchats.forEach(factureAchats => {
  if (codes.findIndex(code => code === factureAchats.code) > -1) {
    listSelectedFacturesAchats.push(factureAchats);
  }
  });
}
this.select.emit(listSelectedFacturesAchats);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listFacturesAchats !== undefined) {
    this.listFacturesAchats.forEach(factureAchats => {
      listElements.push( {code: factureAchats.code ,
         numeroFactureAchats: factureAchats.numeroFactureAchats,
        dateAchats: factureAchats.dateAchats,
         cin: factureAchats.fournisseur.cin,
        nom: factureAchats.fournisseur.nom, 
        prenom: factureAchats.fournisseur.prenom,
        matriculeFiscale: factureAchats.fournisseur.matriculeFiscale, 
        raisonSociale: factureAchats.fournisseur.raisonSociale ,
        montantAPayer: factureAchats.montantAPayer, 
        montantTTC: factureAchats.montantTTC,
        montantHT: factureAchats.montantHT,
         montantTVA: factureAchats.montantTVA,
        etat: factureAchats.etat
       
    });
  });
    this.dataFacturesAchatsSource= new MatTableDataSource<FactureAchatsElement>(listElements);
    this.dataFacturesAchatsSource.sort = this.sort;
    this.dataFacturesAchatsSource.paginator = this.paginator;
    this.factureAchatsService.getTotalFacturesAchats(this.factureAchatsDataFilter).subscribe((response) => {
      console.log('Total credits is ', response);
       this.dataFacturesAchatsSource.paginator.length = response.totalFacturesAchats;
     });
}
}
specifyListColumnsToBeDisplayed() {
  
   console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
   this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
 ?
    this.defaultColumnsDefinitions : [];
    this.selectedColumnsNames = [];
    
    if (this.selectedColumnsDefinitions !== undefined)
     {
       this.selectedColumnsDefinitions.forEach((colName) => { 
            const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
            const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
            const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
            this.columnsDefinitionsToDisplay.push(colDef);
           });
     }
    this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
   this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
              return colDef.displayTitle;
    });
    console.log('columns to disp:', this.columnsDefinitionsToDisplay);
    this.columnsNames = [];
    this.columnsNames[0] = 'select';
    this.columnsDefinitionsToDisplay .map((colDef) => {
     this.columnsNames.push(colDef.name);
    if (this.selectedColumnsDefinitions !== undefined) {
     this.selectedColumnsNames.push(colDef.displayTitle);
    }
 });
    this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
     return colDef.displayTitleAr;
 });
 }
masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataFacturesAchatsSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataFacturesAchatsSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataFacturesAchatsSource.filter = filterValue.trim().toLowerCase();
  if (this.dataFacturesAchatsSource.paginator) {
    this.dataFacturesAchatsSource.paginator.firstPage();
  }
}

loadAddFactureAchatsComponent() {
  this.router.navigate(['/pms/achats/ajouterFactureAchats']) ;

}
loadEditFactureAchatsComponent() {
  this.router.navigate(['/pms/achats/editerFactureAchats', this.selection.selected[ this.selection.selected.length - 1].code]) ;

}
supprimerFacturesAchats()
{
  
  if ( this.selection.selected.length !== 0) {
      this.selection.selected.forEach(function(factureAchats, index) {

          this.factureAchatsService.deleteFactureAchats(factureAchats.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.openSnackBar( this.selection.selected.length + ' Facture(s) Achats Supprimé(s)');
            this.selection.selected = [];
            this.loadData();
            }
      }, this);
    } else {
      this.openSnackBar('Aucune Facture Séléctionnée !');
    }
}
refresh() {
  this.loadData(null);
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 addPayement()
 {
  if ( this.selection.selected.length !== 0) {
    
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
    top: '0'
  };
  dialogConfig.data = {
    codeFactureAchats : this.selection.selected[0].code
  };

  const dialogRef = this.dialog.open(DialogAddPayementFactureAchatsComponent,
    dialogConfig
  );
} else {
  this.openSnackBar('Aucune Facture Séléctionnée !');
}
 }
 history()
 {
   
 }
 addFournisseur()
 {
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
  };

  const dialogRef = this.dialog.open(DialogAddFournisseurComponent,
    dialogConfig
  );
 }
 attacherDocuments()
 {

 }


 changePage($event) {
  console.log('page event is ', $event);
 this.factureAchatsService.getPageFacturesAchats($event.pageIndex, $event.pageSize,
  this.factureAchatsDataFilter ).subscribe((facturesAchats) => {
    this.listFacturesAchats = facturesAchats;
   //  console.log(this.listFacturesVentes);
   // this.transformDataToByConsumedByMatTable();
   const listElements = [];
   this.listFacturesAchats.forEach(factureA => {
    listElements.push( {code: factureA.code ,
       numeroFactureAchats: factureA.numeroFactureAchats,
       dateAchats: factureA.dateAchats, 
       cin: factureA.fournisseur.cin,
      nom: factureA.fournisseur.nom, prenom: factureA.fournisseur.prenom,
      matriculeFiscale: factureA.fournisseur.matriculeFiscale, raisonSociale: factureA.fournisseur.raisonSociale ,
      montantAPayer: factureA.montantAPayer, montantTTC: factureA.montantTTC,
      montantHT: factureA.montantHT, montantTVA: factureA.montantTVA,
      etat: factureA.etat
     
  });
});
     this.dataFacturesAchatsSource = new MatTableDataSource<FactureAchatsElement>(listElements);
     this.factureAchatsService.getTotalFacturesAchats(this.factureAchatsDataFilter).subscribe((response) => {
      console.log('Total credits is ', response);
       this.dataFacturesAchatsSource.paginator.length = response.totalFacturesAchats;
     });

    });
}
toggleFilterPanel() {
  this.showFilter = !this.showFilter;
}
toggleShowColumnsMultiSelect() {
 this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
}
loadFiltredData() {
 console.log('the filter is ', this.factureAchatsDataFilter);
 this.loadData(this.factureAchatsDataFilter);
}

activateFilterFournisseur()
{
  this.showFilterFournisseur = !this.showFilterFournisseur;
}


}
