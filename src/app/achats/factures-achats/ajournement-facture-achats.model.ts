
import { BaseEntity } from '../../common/base-entity.model' ;

type Etat = 'Initial' | 'Valide' | 'Annule' 
type ModePayement = 'Espece' | 'Cheque' | 'Virement'

export class AjournementFactureAchats extends BaseEntity {
    constructor(
        public code?: string,
         public codeFactureAchats?: string,
        public dateOperationObject?: Date,
        public nouvelleDatePayementObject?: Date,
        public motif?: string,
        public nouvelleDatePayement?: string,
        public dateOperation?: string,
        
     ) {
         super();
        
       }
  
}
