import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,
    
      ViewChild } from '@angular/core';
    // import { ArticleService } from './article.service';
    import {Fournisseur} from './fournisseur.model';
    // import { MODES_GENERATION_TRANCHES} from './fournisseur.model';
    import {FournisseurService} from './fournisseur.service';
    import {FormControl} from '@angular/forms';
    import {map, startWith} from 'rxjs/operators';
    import {Observable} from 'rxjs';
    import { Router, ActivatedRoute, ParamMap } from '@angular/router';
    import {ActionData} from './action-data.interface';
    @Component({
        selector: 'app-fournisseur-home',
        templateUrl: './fournisseur-home.component.html',
        // styleUrls: ['./players.component.css']
      })
 export class FournisseurHomeComponent implements OnInit {

    @Input()
    private actionData: ActionData;

    
    private homeMessage = 'Here I will display the list of : last fournisseur updated' +
    'last fournisseur created ; last fournisseur ...';

        constructor( private route: ActivatedRoute,
            private router: Router, private fournisseurService: FournisseurService) {
          }

          ngOnInit() {
        }
        
        }
