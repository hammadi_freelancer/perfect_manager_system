import { Fournisseur } from './fournisseur.model';
export interface ActionData {
       actionName?: string;
     selectedFournisseurs?: Fournisseur[];
}

