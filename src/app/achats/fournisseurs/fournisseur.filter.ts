
import { BaseEntity } from '../../common/base-entity.model' ;
// import { Tranche } from './tranche.model' ;
import { Fournisseur } from '../../achats/fournisseurs/fournisseur.model' ;
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
// import { Service } from '../../stocks/articles/article.service';

export const MODES_GENERATION_TRANCHES = ['AUTOMATIQUE', 'MANUELLE', ];

export class FournisseurFilter  {
    public selectedCin: string;
    public selectedCode: string;
    
    public selectedNom: string;
    public selectedPrenom: string;
    public selectedRaisonSociale: string;
    public selectedMatriculeFiscale: string;
    
    public selectedRegistreCommerce: string;
    public selectedAdresse: string;
    private controlNoms = new FormControl();
    private controlPrenoms = new FormControl();
    private controlCins = new FormControl();
    private controlRaisonSociale = new FormControl();
    private controlMatriculeFiscale = new FormControl();
    private controlRegistreCommerce = new FormControl();
    
    private listNoms: string[] = [];
    private listPrenoms: string[] = [];
    private listCins: string[] = [];
    private listRaisonsSociales: string[] = [];
    private listMatriculesFiscales: string[] = [];
    
    private listRegistreCommerce: string [] = [];
    private filteredNoms: Observable<string[]>;
    private filteredPrenoms: Observable<string[]>;
    private filteredCins: Observable<string[]>;
 

    private filteredRaisonsSociales: Observable<string[]>;
    private filteredMatriculesFiscales: Observable<string[]>;
    private filteredRegistresCommerces: Observable<string[]>;
    
    constructor (fournisseurs: Fournisseur[]) {
      if (fournisseurs !== null) {
      
        this.listCins = fournisseurs.filter((fournisseur) =>
        (fournisseur.cin !== undefined && fournisseur.cin != null )).map((fournisseur) => (fournisseur.cin ));


         this.listNoms = fournisseurs.filter((fournisseur) =>
         (fournisseur.nom !== undefined && fournisseur.nom != null )).map((fournisseur) => (fournisseur.nom));

         this.listPrenoms = fournisseurs.filter((fournisseur) =>
         (fournisseur.prenom !== undefined && fournisseur.prenom != null )).map((fournisseur) => (fournisseur.prenom));

         this.listRaisonsSociales = fournisseurs.filter((fournisseur) =>
         (fournisseur.raisonSociale !== undefined && fournisseur.raisonSociale != null )).map((fournisseur) => (fournisseur.raisonSociale));

          this.listMatriculesFiscales = fournisseurs.filter((fournisseur) =>
         (fournisseur.matriculeFiscale !== undefined && fournisseur.matriculeFiscale != null )).
         map((fournisseur) => (fournisseur.matriculeFiscale));

         this.listRegistreCommerce = fournisseurs.filter((fournisseur) =>
         (fournisseur.registreCommerce !== undefined && fournisseur.registreCommerce != null )).map(
           (fournisseur) => (fournisseur.registreCommerce ));
    

      this.filteredNoms = this.controlNoms.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterNoms(value))
        );
        this.filteredPrenoms = this.controlPrenoms.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterPrenoms(value))
        );
        this.filteredCins = this.controlCins.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterCins(value))
        );
        this.filteredMatriculesFiscales = this.controlMatriculeFiscale.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterMatriculesFiscales(value))
        );
        this.filteredRaisonsSociales = this.controlRaisonSociale.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterRaisonsSociales(value))
        );
        this.filteredRegistresCommerces = this.controlRegistreCommerce.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterRegistresCommerces(value))
        );
     

    }

  }
    private _filterNoms(value: string): string[] {
      if(value !== undefined) {
        const filterValue = value.toLowerCase();
        return this.listNoms.filter(nom => nom.toLowerCase().includes(filterValue));
      } else {
        return this.listNoms;
      }
      }
      private _filterCins(value: string): string[] {
        
        let  filterValue = '';
        if (value !== undefined) {
          filterValue = value.toLowerCase();
        }
        return this.listCins.filter(cin => cin.toLowerCase().includes(filterValue));

      }
      private _filterPrenoms(value: string): string[] {
        if(value !== undefined) {
        const filterValue = value.toLowerCase();
        return this.listPrenoms.filter(prenom => prenom.toLowerCase().includes(filterValue));
        } else {
          return this.listPrenoms;
        }
      }
      private _filterRaisonsSociales(value: string): string[] {
        if (value !== undefined) {
        const filterValue = value.toLowerCase();
        return this.listRaisonsSociales.filter(raison => raison.toLowerCase().includes(filterValue));
        } else {
          return this.listRaisonsSociales;
        }
      }
      private _filterMatriculesFiscales(value: string): string[] {
        if (value !== undefined) {
        const filterValue = value.toLowerCase();
        return this.listMatriculesFiscales.filter(matricule => matricule.toLowerCase().includes(filterValue));
        }  else {
          return this.listMatriculesFiscales;
        }

      }

   
      private _filterRegistresCommerces(value: string): string[] {
        if (value !== undefined) {
        const filterValue = value.toLowerCase();
      
        return this.listRegistreCommerce.filter((registre) => {
          if (registre !== undefined ) {
            return  registre.toLowerCase().includes(filterValue) ;
        } else  {
          return true;
        }
      });
    }  else {
      return this.listRegistreCommerce;
    }
         //  }
      }
      

      public getControlPrenoms(): FormControl {
        return this.controlPrenoms;
      }
      public getControlNoms(): FormControl {
        return this.controlNoms;
      }
      public getControlCins(): FormControl {
        return this.controlCins;
      }
 
      public getControlRaisonSociale(): FormControl {
        return this.controlRaisonSociale;
      }
      public getControlMatriculeFiscale(): FormControl {
        return this.controlMatriculeFiscale;
      }
      public getControlRegistreCommerce(): FormControl {
        return this.controlRegistreCommerce;
      }
      public getFiltredPrenoms(): Observable<string[]> {
        return this.filteredPrenoms;
      }
      public getFiltredNoms(): Observable<string[]> {
        return this.filteredNoms;
      }
      public getFiltredCins(): Observable<string[]> {
        return this.filteredCins;
      }
      public getFiltredRaisonsSociales(): Observable<string[]> {
        return this.filteredRaisonsSociales;
      }
      public getFiltredMatriculesFiscales(): Observable<string[]> {
        return this.filteredMatriculesFiscales;
      }

      public getFiltredRegistreCommerce(): Observable<string[]> {

        return this.filteredRegistresCommerces;
      }
}





