import { Component, OnInit, Input, EventEmitter, Output, AfterContentChecked,
  AfterViewChecked
  } from '@angular/core';
  // import { ArticleService } from './article.service';
  import {Fournisseur} from './fournisseur.model';
  import {FournisseurService} from './fournisseur.service';
  import {FormControl} from '@angular/forms';
  import {map, startWith} from 'rxjs/operators';
  import {Observable} from 'rxjs';
  import { Router, ActivatedRoute, ParamMap } from '@angular/router';
  import {ActionData} from './action-data.interface';
  import {MatSnackBar} from '@angular/material';
  
  @Component({
    selector: 'app-fournisseur-new',
    templateUrl: './fournisseur-new.component.html',
    // styleUrls: ['./players.component.css']
  })
export class FournisseurNewComponent implements OnInit, AfterContentChecked {
  private fournisseur: Fournisseur = new Fournisseur();
  @Input()
  private listFournisseurs: any = [] ;
  @Input()
  private actionData: ActionData;
    private saveEnCours = false;
  private file: any;
    constructor( private route: ActivatedRoute,
      private router: Router , private fournisseurService: FournisseurService, private matSnackBar: MatSnackBar ) {
    }
    ngOnInit() {
    
    }
  saveFournisseur() {
    this.saveEnCours = true;
      this.fournisseurService.addFournisseur(this.fournisseur).subscribe((response) => {
          if (response.error_message ===  undefined) {
            this.saveEnCours = false;
            this.fournisseur = new Fournisseur();
            this.openSnackBar(  'Fournisseur Enregistré');
          } else {
           // show error message
           this.openSnackBar(  'Erreurs!');
           
          }
      });
  }
  loadGridFournisseurs() {
    this.router.navigate(['/pms/achats/fournisseurs']) ;
  }
  fileChanged($event) {
      this.file = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.fournisseur.image = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(this.file);
     }
     vider() {
       this.fournisseur = new Fournisseur();
     }
     openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  ngAfterContentChecked() {

  }
  }
