

import { BaseEntity } from '../../common/base-entity.model' ;
type Etat = 'Initial' | 'Valide' | 'Annule' | ''

export class Fournisseur extends BaseEntity {
   constructor(
     public code?: string, 
    public nom?: string, 
    public numeroFournisseur?: string, 
    public prenom?: string,
     public adresse?: string,

   public email?: string,
public cin?: string, public ville?: string, public codePostale?: string , public gouvernorat?: string,
public mobile?: string, public fidele?: boolean, public image?: any,
 public raisonSociale?: string, public matriculeFiscale?: string, public registreCommerce?: string,
 public etat?: Etat
 
) {
    super();
      this.image = undefined;
}
}
