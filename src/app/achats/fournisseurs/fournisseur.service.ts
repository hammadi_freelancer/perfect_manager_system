import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import {Fournisseur} from './fournisseur.model';
import {DocumentFournisseur} from './document-fournisseur.model';

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_FOURNISSEURS = 'http://localhost:8082/achats/fournisseurs/getFournisseurs';
const URL_GET_PAGE_FOURNISSEURS = 'http://localhost:8082/achats/fournisseurs/getPageFournisseurs';
const URL_ADD_FOURNISSEUR = 'http://localhost:8082/achats/fournisseurs/addFournisseur';
const URL_GET_FOURNISSEUR = 'http://localhost:8082/achats/fournisseurs/getFournisseur';
const URL_GET_TOTAL_FOURNISSEUR = 'http://localhost:8082/achats/fournisseurs/getTotalFournisseurs';

const URL_UPDATE_FOURNISSEUR = 'http://localhost:8082/achats/fournisseurs/updateFournisseur';
const URL_DELETE_FOURNISSEUR = 'http://localhost:8082/achats/fournisseurs/deleteFournisseur';
const URL_GET_FILTRED_LIST_FOURNISSEURS  = 'http://localhost:8082/achats/fournisseurs/getFiltredFournisseurs';

const  URL_ADD_DOCUMENT_FOURNISSEUR = 'http://localhost:8082/achats/documentsFournisseurs/addDocumentFournisseur';
const  URL_GET_LIST_DOCUMENT_FOURNISSEUR = 'http://localhost:8082/achats/documentsFournisseurs/getDocumentsFournisseurs';
const  URL_DELETE_DOCUCMENT_FOURNISSEUR = 'http://localhost:8082/achats/documentsFournisseurs/deleteDocumentFournisseur';
const  URL_UPDATE_DOCUMENT_FOURNISSEUR = 'http://localhost:8082/achats/documentsFournisseurs/updateDocumentFournisseur';
const URL_GET_DOCUMENT_FOURNISSEUR = 'http://localhost:8082/achats/documentsFournisseurs/getDocumentFournisseur';
const URL_GET_LIST_DOCUMENT_FOURNISSEUR_BY_CODE_FOURNISSEUR =
 'http://localhost:8082/achats/documentsFournisseurs/getDocumentsFournisseursByCodeFournisseur';


@Injectable({
  providedIn: 'root'
})
export class FournisseurService {



  constructor(private httpClient: HttpClient) {

   }
   getTotalFournisseurs(filter): Observable<any> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('filter', filterStr);
    return this.httpClient
    .get<any>(URL_GET_TOTAL_FOURNISSEUR, {params});
   }
   getFournisseurs(): Observable<Fournisseur[]> {
    return this.httpClient
    .get<Fournisseur[]>(URL_GET_LIST_FOURNISSEURS);
   }
   getPageFournisseurs(pageNumber, nbElementsPerPage, filter): Observable<Fournisseur[]> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('pageNumber', pageNumber)
    .set('nbElementsPerPage', nbElementsPerPage)
    .set('filter', filterStr);
    return this.httpClient
    .get<Fournisseur[]>(URL_GET_PAGE_FOURNISSEURS, {params});
   }
   /*getFiltredFournisseurs(filterFournisseur): Observable<Fournisseur[]> {
    return this.httpClient
    .post<Fournisseur[]>(URL_GET_FILTRED_LIST_FOURNISSEURS, filterFournisseur);
   }*/
   addFournisseur(fournisseur: Fournisseur): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_FOURNISSEUR, fournisseur);
  }
  updateFournisseur(fournisseur: Fournisseur): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_FOURNISSEUR, fournisseur);
  }
  deleteFournisseur(codeFournisseur): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_FOURNISSEUR + '/' + codeFournisseur);
  }
  getFournisseur(codeFournisseur): Observable<Fournisseur> {
    return this.httpClient.get<Fournisseur>(URL_GET_FOURNISSEUR + '/' + codeFournisseur);
  }



  addDocumentFournisseur(documentFournisseur: DocumentFournisseur): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_DOCUMENT_FOURNISSEUR, documentFournisseur);
    
  }

  getDocumentsFournisseurs(): Observable<DocumentFournisseur[]> {
    return this.httpClient
    .get<DocumentFournisseur[]>(URL_GET_LIST_DOCUMENT_FOURNISSEUR);
   }
   getDocumentFournisseur(codeDocumentFournisseur): Observable<DocumentFournisseur> {
    return this.httpClient.get<DocumentFournisseur>(URL_GET_DOCUMENT_FOURNISSEUR + '/' + codeDocumentFournisseur);
  }
  deleteDocumentFournisseur(codeDocumentFournisseur): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_DOCUCMENT_FOURNISSEUR + '/' + codeDocumentFournisseur);
  }
  getDocumentsFournisseursByCodeFournisseur(codeDocumentFournisseur): Observable<DocumentFournisseur[]> {
    return this.httpClient.get<DocumentFournisseur[]>(URL_GET_LIST_DOCUMENT_FOURNISSEUR_BY_CODE_FOURNISSEUR
       + '/' + codeDocumentFournisseur);
  }
  updateDocumentFournisseur(doc: DocumentFournisseur): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_DOCUMENT_FOURNISSEUR, doc);
  }
}
