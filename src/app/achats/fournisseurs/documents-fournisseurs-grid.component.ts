
import { Component, OnInit, Inject,Input, ViewChild, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {FournisseurService} from './fournisseur.service';
import {Fournisseur} from './fournisseur.model';

import {DocumentFournisseur} from './document-fournisseur.model';
import { DocumentFournisseurElement} from './document-fournisseur.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogAddDocumentFournisseurComponent } from './popups/dialog-add-document-fournisseur.component';
import { DialogEditDocumentFournisseurComponent } from './popups/dialog-edit-document-fournisseur.component';
@Component({
    selector: 'app-documents-fournisseurs-grid',
    templateUrl: 'documents-fournisseurs-grid.component.html',
  })
  export class DocumentsFournisseursGridComponent implements OnInit, OnChanges {
     private listDocumentsFournisseurs: DocumentFournisseur[] = [];
     private  dataDocumentsFournisseursSource: MatTableDataSource<DocumentFournisseurElement>;
     private selection = new SelectionModel<DocumentFournisseurElement>(true, []);
    
     @Input()
     private fournisseur : Fournisseur;

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private fournisseurService: FournisseurService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.fournisseurService.getDocumentsFournisseursByCodeFournisseur(this.fournisseur.code).subscribe((listDocumentsFournisseurs) => {
        this.listDocumentsFournisseurs = listDocumentsFournisseurs;
  // console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
  }
  ngOnChanges()
  {
    this.specifyListColumnsToBeDisplayed();
    
    this.fournisseurService.getDocumentsFournisseursByCodeFournisseur(this.fournisseur.code).subscribe((listDocumentsFournisseurs) => {
      this.listDocumentsFournisseurs = listDocumentsFournisseurs;
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }

  checkOneActionInvoked() {
    const listSelectedDocumentsFournisseurs: DocumentFournisseur[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentFElement) => {
    return documentFElement.code;
  });
  this.listDocumentsFournisseurs.forEach(documentFR => {
    if (codes.findIndex(code => code === documentFR.code) > -1) {
      listSelectedDocumentsFournisseurs.push(documentFR);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsFournisseurs !== undefined) {

      this.listDocumentsFournisseurs.forEach(docFR => {
             listElements.push( {code: docFR.code ,
          dateReception: docFR.dateReception,
          numeroDocument: docFR.numeroDocument,
          typeDocument: docFR.typeDocument,
          description: docFR.description
        } );
      });
    }
      this.dataDocumentsFournisseursSource = new MatTableDataSource<DocumentFournisseurElement>(listElements);
      this.dataDocumentsFournisseursSource.sort = this.sort;
      this.dataDocumentsFournisseursSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    //  console.log('calling specifyListColumnsToBeDisplayed documents grid');
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
    
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
      console.log(this.columnsNames);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsFournisseursSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsFournisseursSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsFournisseursSource.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsFournisseursSource.paginator) {
      this.dataDocumentsFournisseursSource.paginator.firstPage();
    }
  }
  showAddDocumentDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeFournisseur : this.fournisseur.code
   };
 
   const dialogRef = this.dialog.open(DialogAddDocumentFournisseurComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(docAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditDocumentDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeDocumentFournisseur: this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditDocumentFournisseurComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(docAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Document Séléctionnée!');
    }
    }
    supprimerDocuments()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(doc, index) {
              this.fournisseurService.deleteDocumentFournisseur(doc.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( this.selection.selected.length + ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Document Séléctionnée!');
        }
    }
    refresh() {
      this.fournisseurService.getDocumentsFournisseursByCodeFournisseur(this.fournisseur.code).subscribe((listDocumentsFournisseurs) => {
        this.listDocumentsFournisseurs = listDocumentsFournisseurs;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
