import { Component, OnInit, Output, Input, EventEmitter  } from '@angular/core';

import {MatSnackBar} from '@angular/material';
import { FournisseurService } from '../fournisseurs/fournisseur.service';
import { Fournisseur } from '../fournisseurs/fournisseur.model';


@Component({
    selector: 'app-dialog-add-fournisseur',
    templateUrl: 'dialog-add-fournisseur.component.html',
  })
  export class DialogAddFournisseurComponent implements OnInit {
     private fournisseur: Fournisseur;
     constructor(  private fournisseurService: FournisseurService,
     private matSnackBar: MatSnackBar,
      ) {
    }
    ngOnInit()
    {
           this.fournisseur = new Fournisseur();
    }
    saveFournisseur()
    {
      
       console.log(this.fournisseur);
      //  this.saveEnCours = true;
       this.fournisseurService.addFournisseur(this.fournisseur).subscribe((response) => {
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.fournisseur = new Fournisseur();
             this.openSnackBar(  'Fournisseur Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }

    }
