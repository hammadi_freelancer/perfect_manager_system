export interface FournisseurElement {
         code: string ;
         numeroFournisseur: string ;
         cin: string ;
         nom: string;
         prenom: string;
         matriculeFiscale: string;
         responsableCommerciale: string;
         raisonSociale: string;
         mobile: string;
         adresse: string;
         email: string;
         ville: string;
         gouvernorat: string;
         codePostal: string;

         // descriptionEchange: string;
        // montantAPayer: number;
        // avance: number;
         // montantReste?: number;
          // periodTranche?: string;
        //  dateDebut?: string;
        //  valeurTranche?: number;
        //  nbreTranches?: number ;
        // public modeGenerationTranches = MODES_GENERATION_TRANCHES[1],
         // public listTranches?: Tranche[]
        }
