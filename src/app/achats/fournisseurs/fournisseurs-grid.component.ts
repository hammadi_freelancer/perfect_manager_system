import { Component, OnInit, Output, ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {Fournisseur} from './fournisseur.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {FournisseurService} from './fournisseur.service';
import { ActionData } from './action-data.interface';
// import { Fournisseur } from '../../ventes/clients/client.model';
import { FournisseurElement } from './fournisseur.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import { AlertMessage } from '../../common/alert-message.interface';
import { OperationRequest } from '../../common/operation-request.interface';
import { OperationResponse } from '../../common/operation-response.interface';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import { FournisseurDataFilter } from './fournisseur-data.filter';
import { SelectItem } from 'primeng/api';

@Component({
  selector: 'app-fournisseurs-grid',
  templateUrl: './fournisseurs-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class FournisseursGridComponent implements OnInit {

  private fournisseur: Fournisseur =  new Fournisseur();
  private fournisseurDataFilter = new FournisseurDataFilter();
  private selection = new SelectionModel<Fournisseur>(true, []);
  private showSelectColumnsMultiSelect = false;
  private showFilter = false;
  
// @Output()
// select: EventEmitter<Client[]> = new EventEmitter<Client[]>();

//private lastClientAdded: Client;

 // selectedClient: Client ;

 @Input()
 private actionData: ActionData;

 @Input()
  private listFournisseurs: any = [] ;
 // listClientsOrgin: any = [];
 private checkedAll: false;

 private  dataFournisseursSource: MatTableDataSource<FournisseurElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];

 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay :ColumnDefinition[] = [];

 private columnsToSelect : SelectItem[];
  private  selectedColumnsDefinitions: string[];

 private selectedColumnsNames: string[];
  constructor( private route: ActivatedRoute, private fournisseursService: FournisseurService,
    private router: Router, private matSnackBar: MatSnackBar ) {
  }
  ngOnInit() {
    this.defaultColumnsDefinitions = [
      {name: 'numeroFournisseur' , displayTitle: 'N°Fournisseur'},
    {name: 'cin' , displayTitle: 'CIN'},
    {name: 'nom' , displayTitle: 'Nom'},
     {name: 'prenom' , displayTitle: 'Prénom'},
    {name: 'raisonSociale' , displayTitle: 'Raison Soc.'},
    {name: 'matriculeFiscale' , displayTitle: 'Mat.Fiscale'},
    {name: 'registreCommerce' , displayTitle: 'R.Commerce'},
    {name: 'adresse' , displayTitle: 'Adresse'},
    {name: 'mobile' , displayTitle: 'Mobile'},
    {name: 'description' , displayTitle: 'Déscr.'},
  ];
    
    this.columnsToSelect = [
      {label: 'N°Fournisseur', value: 'numeroFournisseur', title: 'N°Fournisseur'},
      {label: 'CIN', value: 'cin', title: 'CIN'},
      {label: 'Nom', value: 'nom', title: 'Nom'},
      {label: 'Prénom', value: 'prenom', title: 'Prénom'},
      {label: 'R.Sociale', value: 'raisonSociale', title: 'R.Sociale'},
      {label: 'Matr.Fiscale', value: 'matriculeFiscale', title: 'Matr. Fiscale'},
      {label: 'R.Commerce', value: 'registreCommerce', title: 'R.Commerce'},
      {label: 'Adresse', value: 'adresse', title: 'Adresse'},
      {label: 'Mobile', value: 'mobile', title: 'Mobile'},
      {label: 'Déscription', value: 'description', title: 'Déscription'},
      
  ];
  this.selectedColumnsDefinitions = [
    'numeroFournisseur',
   'cin',
   'nom' ,
   'prenom', 
   'mobile'
    ];








    this.fournisseursService.getPageFournisseurs(0, 5, null).
    subscribe((fournisseurs) => {
      this.listFournisseurs = fournisseurs;
      // this.listClients[this.listClients.length] = this.lastClientAdded;
      // this.listClientsOrgin = this.listClients ;
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });
   /* this.creditsService.getCredits().subscribe((credits) => {
      console.log('list credits ', credits);
      // this.listCredits = credits;
      this.listCredits =  this.listCredits.map((credit) => {
        if (credit.client === 'undefined' || credit.client === null ) {
           credit.client = new Client();
         }
         return credit;
   });      this.listCreditsOrgin = this.listCredits ;
     });*/
    // this.listClientsOrgin = this.listClients ;

  }
  deleteFournisseur(fournisseur) {
      console.log('call delete !', fournisseur );
    this.fournisseursService.deleteFournisseur(fournisseur.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData(null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.fournisseursService.getPageFournisseurs(0, 5, filter).
  subscribe((fournisseurs) => {
    this.listFournisseurs = fournisseurs;
    // this.listClients[this.listClients.length] = this.lastClientAdded;
    // this.listClientsOrgin = this.listClients;
    this.transformDataToByConsumedByMatTable();
   });
  // this.listClientsOrgin = this.listClients ;
}


checkOneActionInvoked() {
  const listSelectedFournisseurs: Fournisseur[] = [];
console.log('selected fourns are ');
console.log(this.selection.selected);
if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((fournisseurElement) => {
  return fournisseurElement.code;
});
this.listFournisseurs.forEach(fournisseur => {
  if (codes.findIndex(code => code === fournisseur.code) > -1) {
    listSelectedFournisseurs.push(fournisseur);
  }
  });
}
// this.select.emit(listSelectedClients);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listFournisseurs !== undefined) {
    this.listFournisseurs.forEach(fournisseur => {
      listElements.push( {
        code: fournisseur.code,
        numeroFournisseur: fournisseur.numeroFournisseur,
        cin: fournisseur.cin, nom: fournisseur.nom, prenom: fournisseur.prenom,
        matriculeFiscale: fournisseur.matriculeFiscale, raisonSociale: fournisseur.raisonSociale ,
         
        adresse: fournisseur.adresse,   mobile: fournisseur.mobile} );
    });
  }
    this.dataFournisseursSource = new MatTableDataSource<FournisseurElement>(listElements);
    this.dataFournisseursSource.sort = this.sort;
    this.dataFournisseursSource.paginator = this.paginator;
}

masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataFournisseursSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataFournisseursSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataFournisseursSource.filter = filterValue.trim().toLowerCase();
  if (this.dataFournisseursSource.paginator) {
    this.dataFournisseursSource.paginator.firstPage();
  }
}
loadAddFournisseurComponent() {
  this.router.navigate(['/pms/achats/ajouterFournisseur']) ;

}
loadEditFournisseurComponent() {

  if (this.selection.selected.length === 0) {
    this.openSnackBar(' Aucun élément sélectionné' );
    // this.loadData();
    } else {
      this.router.navigate(['/pms/achats/editerFournisseur', this.selection.selected[ this.selection.selected.length - 1].code]) ;
    }
}
supprimerFournisseurs()
{
  console.log('NBRE OF ELEMENTS SELECTED');
  console.log(this.selection.selected.length);
  console.log(' ELEMENTS SELECTED');
  console.log(this.selection.selected);
  if ( this.selection.selected.length !== 0) {
      this.selection.selected.forEach(function(fournisseur, index) {
          this.fournisseursService.deleteFournisseur(fournisseur.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.openSnackBar( ' Element(s) Supprimé(s)');
             this.loadData(null);
            }
      }, this);
    } else {
      this.openSnackBar('No Elements Selected');
    }
}
refresh()
{
  this.loadData(null);
  
}

  openSnackBar(messageToDisplay) {
 this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
     verticalPosition: 'top'});
}

changePage($event) {
  console.log('page event is ', $event);
 this.fournisseursService.getPageFournisseurs($event.pageIndex, $event.pageSize,
  this.fournisseurDataFilter ).subscribe((fournisseurs) => {
    this.listFournisseurs = fournisseurs;
    console.log(this.listFournisseurs);
   // this.transformDataToByConsumedByMatTable();
   const listElements = [];
   this.listFournisseurs.forEach(fournisseur => {
    listElements.push( {
      code: fournisseur.code ,
      numeroFournisseur: fournisseur.numeroFournisseur ,
      
      cin: fournisseur.cin, nom: fournisseur.nom, prenom: fournisseur.prenom,
      matriculeFiscale: fournisseur.matriculeFiscale, raisonSociale: fournisseur.raisonSociale ,
       
      adresse: fournisseur.adresse,   mobile: fournisseur.mobile} );
  });
    // const pagin = this.dataCreditsSource.paginator;
    // const sor = this.dataCreditsSource.sort;
     this.dataFournisseursSource = new MatTableDataSource<FournisseurElement>(listElements);
     this.fournisseursService.getTotalFournisseurs(this.fournisseurDataFilter).subscribe((response) => {
      console.log('Total credits is ', response);
       this.dataFournisseursSource.paginator.length = response.totalCreditsAchats;
     });

    });
}
toggleFilterPanel() {
  this.showFilter = !this.showFilter;
}
toggleShowColumnsMultiSelect() {
 this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
}
loadFiltredData() {
 console.log('the filter is ', this.fournisseurDataFilter);
 this.loadData(this.fournisseurDataFilter);
}
specifyListColumnsToBeDisplayed() {
  
   console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
   this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
 ?
    this.defaultColumnsDefinitions : [];
    this.selectedColumnsNames = [];
    
    if (this.selectedColumnsDefinitions !== undefined)
     {
       this.selectedColumnsDefinitions.forEach((colName) => { 
            const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
            const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
            const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
            this.columnsDefinitionsToDisplay.push(colDef);
           });
     }
    this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
   this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
              return colDef.displayTitle;
    });
    console.log('columns to disp:', this.columnsDefinitionsToDisplay);
    this.columnsNames = [];
    this.columnsNames[0] = 'select';
    this.columnsDefinitionsToDisplay .map((colDef) => {
     this.columnsNames.push(colDef.name);
    if (this.selectedColumnsDefinitions !== undefined) {
     this.selectedColumnsNames.push(colDef.displayTitle);
    }
 });
    this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
     return colDef.displayTitleAr;
 });
 }




}
