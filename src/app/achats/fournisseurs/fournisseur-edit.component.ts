import { Component, OnInit, Input, EventEmitter, Output, AfterContentChecked,
  AfterViewChecked
  } from '@angular/core';
  // import { ArticleService } from './article.service';
  import {Fournisseur} from './fournisseur.model';
  import {FournisseurService} from './fournisseur.service';
  import {FormControl} from '@angular/forms';
  import {map, startWith} from 'rxjs/operators';
  import {Observable} from 'rxjs';
  import { Router, ActivatedRoute, ParamMap } from '@angular/router';
  import {ActionData} from './action-data.interface';
  import {MatSnackBar} from '@angular/material';
  
  
  
  @Component({
    selector: 'app-fournisseur-edit',
    templateUrl: './fournisseur-edit.component.html',
    // styleUrls: ['./players.component.css']
  })
  export class FournisseurEditComponent implements OnInit, AfterContentChecked {
  
  // private client: Client = new Client();
  
  
  private fournisseur: Fournisseur = new Fournisseur();
  private updateEnCours = false;
  
  // @Input()
  // private listClients: any = [] ;
  
  @Input()
  private actionData: ActionData;
  
  private file: any;
  private tabsDisabled = false;
  private etatsFournisseurs: any[] = [
    {value: 'Annule', viewValue: 'Annulé'},
    {value: 'Initial', viewValue: 'Initial'},
    {value: 'Valide', viewValue: 'Validé'}
  ];
    constructor( private route: ActivatedRoute,
      private router: Router , private fournisseurService: FournisseurService, private matSnackBar: MatSnackBar,
     ) {
    }
    ngOnInit() {
   
        const codeFournisseur = this.route.snapshot.paramMap.get('codeFournisseur');
        this.fournisseurService.getFournisseur(codeFournisseur).subscribe((fournisseur) => {
                 this.fournisseur = fournisseur;
                /* if (this.fournisseur.etat === 'Valide') {
                  this.tabsDisabled = false;
                 }
                 if (this.fournisseur.etat === 'Valide' || this.fournisseur.etat === 'Annule')
                  {
                    this.etatsFournisseurs.splice(1, 1);
                  }
                  if (this.fournisseur.etat === 'Initial')
                    {
                      this.etatsFournisseurs.splice(0, 1);
                    }*/
        });
        // this.client = this.actionData.selectedClients[0];
       
    }
    loadAddFournisseurComponent() {
      this.router.navigate(['/pms/achats/ajouterFournisseur']) ;
    
    }
    loadGridFournisseurs() {
      this.router.navigate(['/pms/achats/fournisseurs']) ;
    }
    supprimerFournisseur() {
       
        this.fournisseurService.deleteFournisseur(this.fournisseur.code).subscribe((response) => {
  
          this.openSnackBar( ' Element Supprimé');
          this.loadGridFournisseurs() ;
              });
    }
  updateFournisseur() {
    console.log(this.fournisseur);
    this.updateEnCours = true;
    
    this.fournisseurService.updateFournisseur(this.fournisseur).subscribe((response) => {
      this.updateEnCours = false;
      
      if (response.error_message ===  undefined) {
        console.log('the response of update is ', response );
        this.openSnackBar( ' Fournisseur mis à jour ');
      /*  if (this.fournisseur.etat === 'Valide')
          {
            this.tabsDisabled = false;
            this.etatsFournisseurs = [
              {value: 'Annule', viewValue: 'Annulé'},
              {value: 'Valide', viewValue: 'Validé'}
    
            ];
          }
          if (this.fournisseur.etat === 'Annule')
            {
              this.tabsDisabled = true;
           
            }*/
        
        //  this.save.emit(response);
        // this.client = new Client();
        // this.modeUpdate = false;
      } else {
        this.openSnackBar( ' Erreurs.. ');
        
       // show error message
      }
  });
  }
  fileChanged($event) {
    this.file = $event.target.files[0];
    const fileReader = new FileReader();
    fileReader.onload = (e) => {
      console.log(fileReader.result);
       this.fournisseur.image = fileReader.result;
      // this.filesData[indexFile] = fileData;
    };
    fileReader.readAsDataURL(this.file);
   }
   openSnackBar(messageToDisplay) {
    this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
        verticalPosition: 'top'});
   }
  
  ngAfterContentChecked() {
    // console.log('selected client', this.client);
  
    /*if (this.client.code !==  undefined ) {
      let  existCode = false;
      const currentIntsance = this;
      if (this.listClients !== undefined) {
     existCode = this.listClients.some(function(client, index) {
        return client.code === currentIntsance.client.code;
    });
      }
        if (existCode) {
        this.modeUpdate = true;
            console.log('selected client', this.modeUpdate);
        }
    }
    if (this.modeUpdate === false && (this.client.code !==  undefined) && (this.client.code !== '') ) {
        this.userStartWriting = true;
      } else {
        this.userStartWriting = false;
      }*/
  }
  selectedTabChanged($event) {
    if ($event.index === 4) { // Composition tab was selected
     // this.loadListArticles();
     } else if ($event.index === 1) {
      //  this.loadListSchemasConfiguration() ;
     }
    }
  }
  
  
  