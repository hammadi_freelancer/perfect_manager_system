
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {FournisseurService} from '../fournisseur.service';
import {DocumentFournisseur} from '../document-fournisseur.model';

@Component({
    selector: 'app-dialog-edit-document-fournisseur',
    templateUrl: 'dialog-edit-document-fournisseur.component.html',
  })
  export class DialogEditDocumentFournisseurComponent implements OnInit {
     private documentFournisseur: DocumentFournisseur;
     private updateEnCours = false;
     private typesDoc: any[] = [
      {value: 'CIN', viewValue: 'Carte Identité'},
      {value: 'Passport', viewValue: 'Passport'},
      {value: 'RegistreCommerce', viewValue: 'Registre de Commerce'},
      {value: 'RIB', viewValue: 'RIB'},
      {value: 'AttestationTravail', viewValue: 'Attestation de Travail'},
      {value: 'ExtraitNaissance', viewValue: 'Extrait de Naissance'},
      {value: 'ReleveCompte', viewValue: 'Relevée de Compte'},
      {value: 'CertificatRésidence', viewValue: 'Certificat de Résidence'},
      {value: 'Cheque', viewValue: 'Chèque'},
      {value: 'Traite', viewValue: 'Traite'},
      {value: 'Autre', viewValue: 'Autre'},

    ];
     constructor(  private fournisseurService: FournisseurService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {

        this.fournisseurService.getDocumentFournisseur(this.data.codeDocumentFournisseur).subscribe((doc) => {
            this.documentFournisseur = doc;
     });
    }
    
    updateDocumentFournisseur() 
    {
      this.updateEnCours = true;
       this.fournisseurService.updateDocumentFournisseur(this.documentFournisseur).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.openSnackBar(  'Document Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
     imageFace1Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentFournisseur.imageFace1 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imageFace2Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentFournisseur.imageFace2 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
  }
