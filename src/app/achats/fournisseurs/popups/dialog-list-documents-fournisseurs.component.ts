
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {FournisseurService} from '../fournisseur.service';
import {DocumentFournisseur} from '../document-fournisseur.model';
import { DocumentFournisseurElement } from '../document-fournisseur.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-documents-fournisseurs',
    templateUrl: 'dialog-list-documents-fournisseurs.component.html',
  })
  export class DialogListDocumentsFournisseursComponent implements OnInit {
     private listDocumentsFournisseurs: DocumentFournisseur[] = [];
     private  dataDocumentsFournisseursSource: MatTableDataSource<DocumentFournisseurElement>;
     private selection = new SelectionModel<DocumentFournisseurElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private fournisseurService: FournisseurService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      console.log('code credit in dialog component is :', this.data.codeFournisseur);
      this.fournisseurService.getDocumentsFournisseursByCodeFournisseur(this.data.codeFournisseur).subscribe((listDocumentsFours) => {
        this.listDocumentsFournisseurs = listDocumentsFours;
// console.log('recieving data from backend', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedDocumentsFournisseurs: DocumentFournisseur[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentFElement) => {
    return documentFElement.code;
  });
  this.listDocumentsFournisseurs.forEach(documentFR => {
    if (codes.findIndex(code => code === documentFR.code) > -1) {
      listSelectedDocumentsFournisseurs.push(documentFR);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsFournisseurs !== undefined) {

      this.listDocumentsFournisseurs.forEach(docF => {
             listElements.push( {code: docF.code ,
          dateReception: docF.dateReception,
          numeroDocument: docF.numeroDocument,
          typeDocument: docF.typeDocument,
          description: docF.description
        } );
      });
    }
      this.dataDocumentsFournisseursSource = new MatTableDataSource<DocumentFournisseurElement>(listElements);
      this.dataDocumentsFournisseursSource.sort = this.sort;
      this.dataDocumentsFournisseursSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsFournisseursSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsFournisseursSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsFournisseursSource.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsFournisseursSource.paginator) {
      this.dataDocumentsFournisseursSource.paginator.firstPage();
    }
  }

}
