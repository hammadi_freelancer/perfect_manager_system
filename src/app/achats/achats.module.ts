import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
 import { AppComponent } from '../app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ReactiveFormsModule } from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatMenuModule} from '@angular/material/menu';
import {MatSelectModule} from '@angular/material/select';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatTabsModule} from '@angular/material/tabs';
import {MatBadgeModule} from '@angular/material/badge';
import {MultiSelectModule} from 'primeng/multiselect';

import {MatIconModule} from '@angular/material/icon';
import {MatSortModule} from '@angular/material/sort';
import {MatRadioModule} from '@angular/material/radio';
import {MatPaginatorModule} from '@angular/material/paginator';
import { SharedComponentModule } from '../shared-components/shared-components.module';
import { RouterModule, provideRoutes} from '@angular/router';
import { FournisseursGridComponent } from './fournisseurs/fournisseurs-grid.component';
import { FournisseurNewComponent } from './fournisseurs/fournisseur-new.component';
import { FournisseurEditComponent } from './fournisseurs/fournisseur-edit.component';
import { FournisseurHomeComponent } from './fournisseurs/fournisseur-home.component';
import { CommunicationService } from './fournisseurs/communication.service';

import { HomeAchatsComponent } from './home-achats.component';
import { CreditAchatsNewComponent } from './credits-achats/credit-achats-new.component';
import { CreditAchatsEditComponent } from './credits-achats/credit-achats-edit.component';
import { CreditAchatsHomeComponent } from './credits-achats/credit-achats-home.component';
import { CreditsAchatsGridComponent } from './credits-achats/credits-achats-grid.component';
import { FournisseursResolver } from './fournisseurs-resolver';


import { FacturesAchatsGridComponent } from './factures-achats/factures-achats-grid.component';
import { DialogAddFournisseurComponent } from './fournisseurs/dialog-add-fournisseur.component';
// import { ParametresAchatsComponent } from './parametres-achats.component';
import { DialogDisplayStatistiquesCreditsAchatsComponent } from './credits-achats/popups/dialog-display-stat-credits-achats.component';
import { DialogAddPayementCreditAchatsComponent } from './credits-achats/popups/dialog-add-payement-credit-achats.component';
import { DialogAddAjournementCreditAchatsComponent } from './credits-achats/popups/dialog-add-ajournement-credit-achats.component';
import { DialogAddDocumentCreditAchatsComponent } from './credits-achats/popups/dialog-add-document-credit-achats.component';
import { DialogAddTrancheCreditAchatsComponent } from './credits-achats/popups/dialog-add-tranche-credit-achats.component';
import { DialogAddLigneCreditAchatsComponent } from './credits-achats/popups/dialog-add-ligne-credit-achats.component';

import { DialogEditPayementCreditAchatsComponent } from './credits-achats/popups/dialog-edit-payement-credit-achats.component';
import { DialogEditAjournementCreditAchatsComponent } from './credits-achats/popups/dialog-edit-ajournement-credit-achats.component';
import { DialogEditDocumentCreditAchatsComponent } from './credits-achats/popups/dialog-edit-document-credit-achats.component';
import { DialogEditTrancheCreditAchatsComponent } from './credits-achats/popups/dialog-edit-tranche-credit-achats.component';
import { DialogEditLigneCreditAchatsComponent } from './credits-achats/popups/dialog-edit-ligne-credit-achats.component';


import { PayementsCreditsAchatsGridComponent } from './credits-achats/payements-credits-achats-grid.component';
import { AjournementsCreditsAchatsGridComponent } from './credits-achats/ajournements-credits-achats-grid.component';
import { DocumentsCreditsAchatsGridComponent } from './credits-achats/documents-credits-achats-grid.component';
import {TranchesCreditsAchatsGridComponent } from './credits-achats/tranches-credits-achats-grid.component';
import {LignesCreditsAchatsGridComponent } from './credits-achats/lignes-credits-achats-grid.component';


import { DialogListPayementsCreditsAchatsComponent } from './credits-achats/popups/dialog-list-payements-credits-achats.component';
import { DialogListAjournementsCreditsAchatsComponent } from './credits-achats/popups/dialog-list-ajournements-credits-achats.component';

import { DialogListTranchesCreditsAchatsComponent } from './credits-achats/popups/dialog-list-tranches-credits-achats.component';
import { DialogListDocumentsCreditsAchatsComponent } from './credits-achats/popups/dialog-list-documents-credits-achats.component';
import { DialogListLignesCreditsAchatsComponent } from './credits-achats/popups/dialog-list-lignes-credits-achats.component';


import { DialogAddDocumentFournisseurComponent} from './fournisseurs/popups/dialog-add-document-fournisseur.component';
import { DialogEditDocumentFournisseurComponent} from './fournisseurs/popups/dialog-edit-document-fournisseur.component';
import { DialogListDocumentsFournisseursComponent} from './fournisseurs/popups/dialog-list-documents-fournisseurs.component';

import { DocumentsFournisseursGridComponent} from './fournisseurs/documents-fournisseurs-grid.component';


import { DialogAddPayementFactureAchatsComponent } from './factures-achats/popups/dialog-add-payement-facture-achats.component';
import { DialogAddAjournementFactureAchatsComponent } from './factures-achats/popups/dialog-add-ajournement-facture-achats.component';
import { DialogAddDocumentFactureAchatsComponent } from './factures-achats/popups/dialog-add-document-facture-achats.component';

import { DialogEditPayementFactureAchatsComponent } from './factures-achats/popups/dialog-edit-payement-facture-achats.component';
import { DialogEditAjournementFactureAchatsComponent } from './factures-achats/popups/dialog-edit-ajournement-facture-achats.component';
import { DialogEditDocumentFactureAchatsComponent } from './factures-achats/popups/dialog-edit-document-facture-achats.component';
import { DialogListPayementsFactureAchatsComponent} from './factures-achats/popups/dialog-list-payements-factures-achats.component';
import { DialogListDocumentsFacturesAchatsComponent} from './factures-achats/popups/dialog-list-documents-factures-achats.component';
import { DialogListAjournementsFacturesAchatsComponent} from './factures-achats/popups/dialog-list-ajournements-factures-achats.component';
import { DialogEditLigneFactureAchatsComponent } from './factures-achats/popups/dialog-edit-ligne-facture-achats.component';
import { DialogAddLigneFactureAchatsComponent } from './factures-achats/popups/dialog-add-ligne-facture-achats.component';
import { DialogListLignesFactureAchatsComponent} from './factures-achats/popups/dialog-list-lignes-factures-achats.component';

import { LignesFacturesAchatsGridComponent} from './factures-achats/lignes-factures-achats-grid.component';

import { PayementsFacturesAchatsGridComponent } from './factures-achats/payements-factures-achats-grid.component';
import { AjournementsFacturesAchatsGridComponent} from './factures-achats/ajournements-factures-achats-grid.component';
import { DocumentsFactureAchatsGridComponent} from './factures-achats/documents-factures-achats-grid.component';

import { FactureAchatsNewComponent } from './factures-achats/facture-achats-new.component';
import { FactureAchatsEditComponent } from './factures-achats/facture-achats-edit.component';

import { ReleveAchatsNewComponent } from './releves-achats/releve-achats-new.component';
import { ReleveAchatsEditComponent } from './releves-achats/releve-achats-edit.component';
import { RelevesAchatsGridComponent } from './releves-achats/releves-achats-grid.component';

import { DialogAddPayementReleveAchatsComponent } from './releves-achats/popups/dialog-add-payement-releve-achats.component';
import { DialogAddAjournementReleveAchatsComponent } from './releves-achats/popups/dialog-add-ajournement-releve-achats.component';
import { DialogAddDocumentReleveAchatsComponent } from './releves-achats/popups/dialog-add-document-releve-achats.component';

import { DialogEditPayementReleveAchatsComponent } from './releves-achats/popups/dialog-edit-payement-releve-achats.component';
import { DialogEditAjournementReleveAchatsComponent } from './releves-achats/popups/dialog-edit-ajournement-releve-achats.component';
import { DialogEditDocumentReleveAchatsComponent } from './releves-achats/popups/dialog-edit-document-releve-achats.component';
import { DialogListPayementsRelevesAchatsComponent} from './releves-achats/popups/dialog-list-payements-releves-achats.component';
import { DialogListDocumentsRelevesAchatsComponent} from './releves-achats/popups/dialog-list-documents-releves-achats.component';
import { DialogListAjournementsRelevesAchatsComponent} from './releves-achats/popups/dialog-list-ajournements-releves-achats.component';
import { DialogEditLigneReleveAchatsComponent } from './releves-achats/popups/dialog-edit-ligne-releve-achats.component';
import { DialogAddLigneReleveAchatsComponent } from './releves-achats/popups/dialog-add-ligne-releve-achats.component';
import { DialogListLignesRelevesAchatsComponent} from './releves-achats/popups/dialog-list-lignes-releves-achats.component';
import { LignesRelevesAchatsGridComponent} from './releves-achats/lignes-releves-achats-grid.component';

import { PayementsRelevesAchatsGridComponent } from './releves-achats/payements-releves-achats-grid.component';
import { AjournementsRelevesAchatsGridComponent} from './releves-achats/ajournements-releves-achats-grid.component';
import { DocumentsReleveAchatsGridComponent} from './releves-achats/documents-releves-achats-grid.component';



import { DemandeAchatsNewComponent } from './demandes-achats/demande-achats-new.component';
import { DemandeAchatsEditComponent } from './demandes-achats/demande-achats-edit.component';
import { DemandesAchatsGridComponent } from './demandes-achats/demandes-achats-grid.component';

import { DialogAddPayementDemandeAchatsComponent } from './demandes-achats/popups/dialog-add-payement-demande-achats.component';
import { DialogAddAjournementDemandeAchatsComponent } from './demandes-achats/popups/dialog-add-ajournement-demande-achats.component';
import { DialogAddDocumentDemandeAchatsComponent } from './demandes-achats/popups/dialog-add-document-demande-achats.component';

import { DialogEditPayementDemandeAchatsComponent } from './demandes-achats/popups/dialog-edit-payement-demande-achats.component';
import { DialogEditAjournementDemandeAchatsComponent } from './demandes-achats/popups/dialog-edit-ajournement-demande-achats.component';
import { DialogEditDocumentDemandeAchatsComponent } from './demandes-achats/popups/dialog-edit-document-demande-achats.component';
import { DialogListPayementsDemandesAchatsComponent} from './demandes-achats/popups/dialog-list-payements-demandes-achats.component';
import { DialogListDocumentsDemandesAchatsComponent} from './demandes-achats/popups/dialog-list-documents-demandes-achats.component';
import { DialogListAjournementsDemandesAchatsComponent} from './demandes-achats/popups/dialog-list-ajournements-demandes-achats.component';
import { DialogEditLigneDemandeAchatsComponent } from './demandes-achats/popups/dialog-edit-ligne-demande-achats.component';
import { DialogAddLigneDemandeAchatsComponent } from './demandes-achats/popups/dialog-add-ligne-demande-achats.component';
import { DialogListLignesDemandesAchatsComponent} from './demandes-achats/popups/dialog-list-lignes-demandes-achats.component';
import { LignesDemandesAchatsGridComponent} from './demandes-achats/lignes-demandes-achats-grid.component';

import { PayementsDemandesAchatsGridComponent } from './demandes-achats/payements-demandes-achats-grid.component';
import { AjournementsDemandesAchatsGridComponent} from './demandes-achats/ajournements-demandes-achats-grid.component';
import { DocumentsDemandeAchatsGridComponent} from './demandes-achats/documents-demandes-achats-grid.component';


import { ArticlesResolver } from './articles-resolver';

import { MagasinsResolver } from './magasins-resolver';
import { UnMesuresResolver } from './unMesures-resolver';

import {MatDialogModule} from '@angular/material/dialog';
import {FormsModule } from '@angular/forms';
 import {APP_BASE_HREF} from '@angular/common';

@NgModule({
  declarations: [
    FournisseursGridComponent,
    FournisseurNewComponent,
    FournisseurEditComponent,
    FournisseurHomeComponent,
    FactureAchatsNewComponent,
    FactureAchatsEditComponent,
    FacturesAchatsGridComponent,
    PayementsFacturesAchatsGridComponent,
    AjournementsFacturesAchatsGridComponent,
    DocumentsFactureAchatsGridComponent,
    LignesFacturesAchatsGridComponent,
    CreditsAchatsGridComponent,
    CreditAchatsNewComponent,
    CreditAchatsEditComponent,
    CreditAchatsHomeComponent,
   
    FacturesAchatsGridComponent,

    ReleveAchatsNewComponent,
    ReleveAchatsEditComponent,
    RelevesAchatsGridComponent,

    DemandeAchatsNewComponent,
    DemandeAchatsEditComponent,
    DemandesAchatsGridComponent,


    PayementsDemandesAchatsGridComponent,
    AjournementsDemandesAchatsGridComponent,
    DocumentsDemandeAchatsGridComponent,

    PayementsRelevesAchatsGridComponent,
    AjournementsRelevesAchatsGridComponent,
    DocumentsReleveAchatsGridComponent,
    LignesRelevesAchatsGridComponent,
    DocumentsFournisseursGridComponent,
    DialogAddFournisseurComponent,
    DialogAddDocumentFournisseurComponent,
    DialogEditDocumentFournisseurComponent,
    DialogListDocumentsFournisseursComponent,




  // popups credits ajournements

  AjournementsCreditsAchatsGridComponent,
  DialogEditAjournementCreditAchatsComponent,
  DialogAddAjournementCreditAchatsComponent,
  DialogListAjournementsCreditsAchatsComponent,
 // popups credits paiements

   DialogAddPayementCreditAchatsComponent,
   DialogListPayementsCreditsAchatsComponent,
   DialogEditPayementCreditAchatsComponent,
   PayementsCreditsAchatsGridComponent,

// popups credits  tranches    
   DialogAddTrancheCreditAchatsComponent,
   DialogListTranchesCreditsAchatsComponent,
   DialogEditTrancheCreditAchatsComponent,
   TranchesCreditsAchatsGridComponent,

 // popups credits documents
 
   DialogAddDocumentCreditAchatsComponent,
   DialogListDocumentsCreditsAchatsComponent,
   DocumentsCreditsAchatsGridComponent,
   DialogEditDocumentCreditAchatsComponent,
// popups credits lignes
 
DialogAddLigneCreditAchatsComponent,
 DialogListLignesCreditsAchatsComponent,
LignesCreditsAchatsGridComponent,
DialogEditLigneCreditAchatsComponent,
DialogDisplayStatistiquesCreditsAchatsComponent,
    // popups facture achats

  DialogAddPayementFactureAchatsComponent ,
  DialogAddAjournementFactureAchatsComponent ,
   DialogAddDocumentFactureAchatsComponent ,
  DialogEditPayementFactureAchatsComponent ,
    DialogEditAjournementFactureAchatsComponent ,
    DialogEditDocumentFactureAchatsComponent ,
   DialogListPayementsFactureAchatsComponent,
    DialogListDocumentsFacturesAchatsComponent,
    DialogListAjournementsFacturesAchatsComponent,
     DialogEditLigneFactureAchatsComponent,
    DialogAddLigneFactureAchatsComponent ,
    DialogListLignesFactureAchatsComponent,


    // popups releves achats


    DialogAddPayementReleveAchatsComponent ,
    DialogAddAjournementReleveAchatsComponent ,
     DialogAddDocumentReleveAchatsComponent ,
    DialogEditPayementReleveAchatsComponent ,
      DialogEditAjournementReleveAchatsComponent ,
      DialogEditDocumentReleveAchatsComponent ,
     DialogListPayementsRelevesAchatsComponent,
      DialogListDocumentsRelevesAchatsComponent,
      DialogListAjournementsRelevesAchatsComponent,
       DialogEditLigneReleveAchatsComponent,
      DialogAddLigneReleveAchatsComponent ,
      DialogListLignesRelevesAchatsComponent,


      // popups demandes achats

      DialogAddPayementFactureAchatsComponent ,
      DialogAddAjournementFactureAchatsComponent ,
       DialogAddDocumentFactureAchatsComponent ,
      DialogEditPayementFactureAchatsComponent ,
        DialogEditAjournementFactureAchatsComponent ,
        DialogEditDocumentFactureAchatsComponent ,
       DialogListPayementsFactureAchatsComponent,
        DialogListDocumentsFacturesAchatsComponent,
        DialogListAjournementsFacturesAchatsComponent,
         DialogEditLigneFactureAchatsComponent,
        DialogAddLigneFactureAchatsComponent ,
        DialogListLignesFactureAchatsComponent,

    
    HomeAchatsComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, MatDatepickerModule, MatNativeDateModule,
     BrowserAnimationsModule, ReactiveFormsModule, MatAutocompleteModule,
    MatInputModule, MatCardModule, MatSortModule, MatPaginatorModule, MatTableModule,
    MatCheckboxModule, MatRadioModule, MatIconModule, SharedComponentModule, MatSelectModule,
    MatTabsModule, MatBadgeModule, MultiSelectModule,
    FormsModule, MatMenuModule, MatExpansionModule, MatDialogModule, MatButtonModule, MatProgressBarModule
],
  providers: [MatNativeDateModule, FournisseursResolver,
    MagasinsResolver,
    ArticlesResolver,
    UnMesuresResolver
      // BASE_URL
  ],
  exports : [
    RouterModule,
    HomeAchatsComponent,
  ],
  entryComponents : [
    DialogAddFournisseurComponent,
    // popups credits ajournements

  AjournementsCreditsAchatsGridComponent,
  DialogEditAjournementCreditAchatsComponent,
  DialogAddAjournementCreditAchatsComponent,
  DialogListAjournementsCreditsAchatsComponent,
 // popups credits paiements

   DialogAddPayementCreditAchatsComponent,
   DialogListPayementsCreditsAchatsComponent,
   DialogEditPayementCreditAchatsComponent,
   PayementsCreditsAchatsGridComponent,

// popups credits  tranches    
   DialogAddTrancheCreditAchatsComponent,
   DialogListTranchesCreditsAchatsComponent,
   DialogEditTrancheCreditAchatsComponent,
   TranchesCreditsAchatsGridComponent,

 // popups credits documents
 
   DialogAddDocumentCreditAchatsComponent,
   DialogListDocumentsCreditsAchatsComponent,
   DocumentsCreditsAchatsGridComponent,
   DialogEditDocumentCreditAchatsComponent,
// popups credits lignes
 
DialogAddLigneCreditAchatsComponent,
DialogListLignesCreditsAchatsComponent,
LignesCreditsAchatsGridComponent,
DialogEditLigneCreditAchatsComponent,
DialogDisplayStatistiquesCreditsAchatsComponent,
DialogAddDocumentFournisseurComponent,
DialogEditDocumentFournisseurComponent,
DialogListDocumentsFournisseursComponent,

// popus facture achats


    // popups facture achats

    DialogAddPayementFactureAchatsComponent ,
    DialogAddAjournementFactureAchatsComponent ,
     DialogAddDocumentFactureAchatsComponent ,
    DialogEditPayementFactureAchatsComponent ,
      DialogEditAjournementFactureAchatsComponent ,
      DialogEditDocumentFactureAchatsComponent ,
     DialogListPayementsFactureAchatsComponent,
      DialogListDocumentsFacturesAchatsComponent,
      DialogListAjournementsFacturesAchatsComponent,
       DialogEditLigneFactureAchatsComponent,
      DialogAddLigneFactureAchatsComponent ,
      DialogListLignesFactureAchatsComponent,

      // popups releves achats

      DialogAddPayementReleveAchatsComponent ,
      DialogAddAjournementReleveAchatsComponent ,
       DialogAddDocumentReleveAchatsComponent ,
      DialogEditPayementReleveAchatsComponent ,
        DialogEditAjournementReleveAchatsComponent ,
        DialogEditDocumentReleveAchatsComponent ,
       DialogListPayementsRelevesAchatsComponent,
        DialogListDocumentsRelevesAchatsComponent,
        DialogListAjournementsRelevesAchatsComponent,
         DialogEditLigneReleveAchatsComponent,
        DialogAddLigneReleveAchatsComponent ,
        DialogListLignesRelevesAchatsComponent,

        // popups demandes achats
        DialogAddPayementFactureAchatsComponent ,
        DialogAddAjournementFactureAchatsComponent ,
         DialogAddDocumentFactureAchatsComponent ,
        DialogEditPayementFactureAchatsComponent ,
          DialogEditAjournementFactureAchatsComponent ,
          DialogEditDocumentFactureAchatsComponent ,
         DialogListPayementsFactureAchatsComponent,
          DialogListDocumentsFacturesAchatsComponent,
          DialogListAjournementsFacturesAchatsComponent,
           DialogEditLigneFactureAchatsComponent,
          DialogAddLigneFactureAchatsComponent ,
          DialogListLignesFactureAchatsComponent,


   
  ],
  bootstrap: [AppComponent]
})
export class AchatsModule { }
