import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
// import { CommunicationService} from './communication.service';
import { MatDatepickerInput } from '@angular/material/datepicker';
import { MatSnackBar } from '@angular/material';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {ArticleService} from '../../stocks/articles/article.service';
import {Article} from '../../stocks/articles/article.model';
import { MatExpansionPanel } from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {LigneFactureAchatsElement} from '../factures-achats/ligne-facture-achats.interface';
import {SelectionModel} from '@angular/cdk/collections';
import {Magasin} from '../../stocks/magasins/magasin.model';
import {ModePayementLibelle} from './mode-payement-libelle.interface';
import {FactureAchats} from '../factures-achats/facture-achats.model';

import {FactureAchatsService} from '../factures-achats/facture-achats.service';
import {DemandeAchatsService} from './demande-achats.service';

import {AchatsService} from '../achats.service';
import { FournisseurService } from '../fournisseurs/fournisseur.service';
import { FournisseurFilter } from '../fournisseurs/fournisseur.filter';
import { ArticleFilter } from './article.filter';
import { UnMesureFilter } from '../../stocks/un-mesures/un-mesures.filter';

import {Fournisseur} from '../fournisseurs/fournisseur.model';
import {UnMesure} from '../../stocks/un-mesures/un-mesure.model';

import {DemandeAchats} from '../demandes-achats/demande-achats.model';
import {LigneDemandeAchats} from '../demandes-achats/ligne-demande-achats.model';

import {LigneDemandeAchatsElement} from '../demandes-achats/ligne-demande-achats.interface';

@Component({
  selector: 'app-demande-achats-edit',
  templateUrl: './demande-achats-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class DemandeAchatsEditComponent implements OnInit  {

  private demandeAchats: DemandeAchats =  new DemandeAchats();
  private updateEnCours = false;
  private tabsDisabled = true;
  private managedLigneDemandeAchats = new LigneDemandeAchats();
  private selection = new SelectionModel<LigneDemandeAchatsElement>(true, []);
  @ViewChild(MatPaginator) paginator: MatPaginator; 
  @ViewChild(MatSort) sort: MatSort;
  private columnsDefinitions: ColumnDefinition[] = [];
  private columnsTitles: string[] = [];
  private columnsTitlesAr: string[] = [];
  private columnsNames: string[] = [];
  
@Input()
private listDemandesAchats: any = [] ;

@ViewChild('firstMatExpansionPanel')
private firstMatExpansionPanel : MatExpansionPanel;

@ViewChild('manageLigneMatExpansionPanel')
private manageLigneMatExpansionPanel : MatExpansionPanel;

@ViewChild('listLignesMatExpansionPanel')
private listLignesMatExpansionPanel : MatExpansionPanel;

// private listCodesArticles: string[];
// private lignesFactures: LigneFactureVente[] = [ ];
// private listTranches: Tranche[] = [] ;
// private factureVenteFilter: FactureVenteFilter;
private articleFilter: ArticleFilter;
private listFournisseurs: Fournisseur[];
private fournisseurFilter: FournisseurFilter;
private unMesureFilter : UnMesureFilter;
// private listArticles: Article[];
// private listLignesFactureVentes: LigneFactureVente[] = [ ];
private  dataLignesDemandeAchatsSource: MatTableDataSource<LigneDemandeAchatsElement>;

// private editLignesMode: boolean [] = [];

private etatsDemandeAchats: any[] = [
  {value: 'Annule', viewValue: 'Annulé'},
  {value: 'Initial', viewValue: 'Initial'},
  {value: 'Valide', viewValue: 'Validé'}
];

private payementModes: ModePayementLibelle[] = [
  {value: 'Comptant', viewValue: 'Comptant'},
  {value: 'Cheque', viewValue: 'Chèque'},
  {value: 'Credit', viewValue: 'Crédit'},
  {value: 'Credit_Comptant', viewValue: 'Crédit/Comptant'}
];
  constructor( private route: ActivatedRoute,
    private router: Router, private demandeAchatsService: DemandeAchatsService,
    private articleService: ArticleService, private dialog: MatDialog,
    private matSnackBar: MatSnackBar, private elRef: ElementRef,
   ) {
  }
  ngOnInit() {
    this.listFournisseurs = this.route.snapshot.data.fournisseurs;
   
  this.fournisseurFilter = new FournisseurFilter(this.listFournisseurs);
    this.articleFilter = new ArticleFilter(this.route.snapshot.data.articles,
    [], [] );
     this.unMesureFilter = new UnMesureFilter(this.route.snapshot.data.unMesures);
 
    this.articleFilter = new ArticleFilter(this.route.snapshot.data.articles,
      [], []);
  
    const codeDemandeAchats = this.route.snapshot.paramMap.get('codeDemandeAchats');


    this.demandeAchatsService.getDemandeAchats(codeDemandeAchats).subscribe((demandeAchats) => {
             this.demandeAchats = demandeAchats;
             if (this.demandeAchats.etat === 'Valide')
              {
                this.tabsDisabled = false;
              }
             if (this.demandeAchats.etat === 'Valide' || this.demandeAchats.etat === 'Annule')
              {
                this.etatsDemandeAchats.splice(1, 1);
              }
              if (this.demandeAchats.etat === 'Initial')
                {
                  this.etatsDemandeAchats.splice(0, 1);
                }
            });
            this.firstMatExpansionPanel.open();

            this.specifyListColumnsToBeDisplayed();
            this.transformDataToByConsumedByMatTable();
        
           this.demandeAchats.etat = 'Initial';
  




            
  }
  loadGridDemandesAchats() {
    this.router.navigate(['/pms/achats/demandesAchats']) ;
  }
updateDemandeAchats() {

  //console.log(this.factureVente);
  let noFournisseur = false;
  let noRow = false;
  if (this.demandeAchats.etat === 'Valide' )  {
    if ( this.demandeAchats.fournisseur.cin === null ||  this.demandeAchats.fournisseur.cin === undefined)  {
      noFournisseur = true;
        
      }
  if (this.demandeAchats.listLignesDemandeAchats[0] === undefined
    || this.demandeAchats.listLignesDemandeAchats[0].quantite === 0  ) {
      noRow = true;
    }
  }
        if (noRow)  {
          this.openSnackBar( 'Le Fournisseur doit avoir au moins une ligne valide !');
          
        } else {
  this.updateEnCours = true;
  this.demandeAchatsService.updateDemandeAchats(this.demandeAchats).subscribe((response) => {
    this.updateEnCours = false;
    if (response.error_message ===  undefined) {
      console.log('the response of update is ', response );
      if (this.demandeAchats.etat === 'Valide')
        {
          this.tabsDisabled = false;
          this.etatsDemandeAchats= [
            {value: 'Annule', viewValue: 'Annulé'},
            {value: 'Valide', viewValue: 'Validé'},
            {value: 'Reglee', viewValue: 'Réglée'},
            {value: 'Valide', viewValue: 'Partiellement Réglée'},
            
  
          ];
        }
        if (this.demandeAchats.etat === 'Annule')
          {
            this.tabsDisabled = true;
         
          }

      this.openSnackBar( ' Demande Achats mise à jour ');
      //  this.save.emit(response);
      // this.client = new Client();
      // this.modeUpdate = false;
    } else {
      this.openSnackBar( ' Erreurs:Opération Echouée ');
     // show error message
    }
});
        }
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 
 fillDataNomPrenom()
 {
   const listFiltred  = this.listFournisseurs.filter((fournisseur) => (fournisseur.cin === this.demandeAchats.fournisseur.cin));
   this.demandeAchats.fournisseur.nom = listFiltred[0].nom;
   this.demandeAchats.fournisseur.prenom = listFiltred[0].prenom;
 }
 fillDataMatriculeRaison()
 {
   const listFiltred  = this.listFournisseurs.filter((fournisseur) =>
    (fournisseur.registreCommerce === this.demandeAchats.fournisseur.registreCommerce));
   this.demandeAchats.fournisseur.matriculeFiscale = listFiltred[0].matriculeFiscale;
   this.demandeAchats.fournisseur.raisonSociale = listFiltred[0].raisonSociale;
 }


 transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.demandeAchats.listLignesDemandeAchats!== undefined) {
    this.demandeAchats.listLignesDemandeAchats.forEach(ligneDem => {
           listElements.push( {code: ligneDem.code ,
            numLigneDemandeAchats: ligneDem.numLigneDemandeAchats,
            article: ligneDem.article.code,
             unMesure: ligneDem.unMesure.code,
             quantite: ligneDem.quantite,
             prixDesire: ligneDem.prixDesire,
             besoinUrgent: ligneDem.besoinUrgent,
             traite: ligneDem.traite,
             dateDisponibilite: ligneDem.dateDisponibilite,
      } );
    });
  }
    this.dataLignesDemandeAchatsSource= new MatTableDataSource<LigneDemandeAchatsElement>(listElements);
    this.dataLignesDemandeAchatsSource.sort = this.sort;
    this.dataLignesDemandeAchatsSource.paginator = this.paginator;
    
    
}
 specifyListColumnsToBeDisplayed() {
   // console.log('calling specifyListColumnsToBeDisplayed documents grid');
  this.columnsTitles = [];
  this.columnsNames = [];
  this.columnsTitlesAr  = [];
  
  this.columnsDefinitions = [{name: 'numLigneDemandeAchats' , displayTitle: 'N°'},
  {name: 'article' , displayTitle: 'Art.'},
  {name: 'unMesure' , displayTitle: 'Un.Mes.'},
  {name: 'quantite' , displayTitle: 'Qté'},
  {name: 'prixDesire' , displayTitle: 'P.Désiré'},
  {name: 'besoinUrgent' , displayTitle: 'Prioritaire'},
  {name: 'traite' , displayTitle: 'Traite'},
  {name: 'dateDisponibilite' , displayTitle: 'Date Disp.'},
   ];

   this.columnsTitles = this.columnsDefinitions.map((colDef) => {
             return colDef.displayTitle;
   });
   
   this.columnsNames[0] = 'select';
   this.columnsDefinitions.map((colDef) => {
    this.columnsNames.push(colDef.name);
    console.log(this.columnsNames);
});
   this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
    return colDef.displayTitleAr;
});
}

applyFilter(filterValue: string) {
  this.dataLignesDemandeAchatsSource.filter = filterValue.trim().toLowerCase();
  if (this.dataLignesDemandeAchatsSource.paginator) {
    this.dataLignesDemandeAchatsSource.paginator.firstPage();
  }
}

checkLigneDemandeAchats()
{

}
getIndexOfRow(row)
{

    for ( let i = 0; i < this.demandeAchats.listLignesDemandeAchats.length ; i++) {
      if (this.demandeAchats.listLignesDemandeAchats[i].numLigneDemandeAchats === row.numLigneDemandeAchats)
       {
             return i;
        }
    }
    return -1;
}
mapRowGridToObjectLigneDemandeAchats(rowGrid): LigneDemandeAchats
{
  
  const ligneDA = new  LigneDemandeAchats() ;
  ligneDA.numeroDemandeAchats = rowGrid.numeroDemandeAchats;
  ligneDA.numLigneDemandeAchats = rowGrid.numLigneDemandeAchats;
  ligneDA.article.code =  rowGrid.article;
  ligneDA.unMesure.code =  rowGrid.unMesure;
  ligneDA.quantite =  rowGrid.quantite;
  ligneDA.prixDesire =  rowGrid.prixDesire;
  ligneDA.besoinUrgent =  rowGrid.besoinUrgent;
  ligneDA.dateDisponibilite =  rowGrid.dateDisponibilite;
  ligneDA.traite =  rowGrid.traite;
  return ligneDA;
}
editLigneDemandeAchats(row)
{
  this.managedLigneDemandeAchats = this.mapRowGridToObjectLigneDemandeAchats(row);


    if(row.unMesure === undefined)
      {
        this.managedLigneDemandeAchats.unMesure = new UnMesure();
      }
      if(row.article === undefined)
        {
          this.managedLigneDemandeAchats.article = new Article();
        }
        console.log('row to edit is ', row);

  this.manageLigneMatExpansionPanel.open();
  
}
deleteLigneDemandeAchats(row)
{

  const indexOfRow = this.getIndexOfRow(row);
  this.demandeAchats.listLignesDemandeAchats.splice(indexOfRow , 1 );
  this.transformDataToByConsumedByMatTable();
  
}
addLigneDemandeAchats()
{
  if (this.managedLigneDemandeAchats.quantite === undefined
    || this.managedLigneDemandeAchats.quantite === 0 ||
    this.managedLigneDemandeAchats.quantite === null ||
      this.managedLigneDemandeAchats.article.code === undefined  ||
      this.managedLigneDemandeAchats.article.code === '' ||
      this.managedLigneDemandeAchats.article.code === null 
  ) {
        this.openSnackBar('Donnnées Manquées (QTE/ART!');
    } else if (this.managedLigneDemandeAchats.numLigneDemandeAchats === undefined
  || this.managedLigneDemandeAchats.numLigneDemandeAchats === '' ||
  this.managedLigneDemandeAchats.numLigneDemandeAchats=== null ) {
      this.openSnackBar('Numéro de Ligne Non Spécifié !');
    } else {
  this.managedLigneDemandeAchats.numeroDemandeAchats = this.demandeAchats.numeroDemandeAchats;
  if (this.demandeAchats.listLignesDemandeAchats.length === 0) {
  this.demandeAchats.listLignesDemandeAchats.push(this.managedLigneDemandeAchats);
  } else  {
    console.log('look for index:');
     const indexOfRow = this.getIndexOfRow(this.managedLigneDemandeAchats);
     console.log(indexOfRow);
     
     {
           if (indexOfRow === -1) {
    this.demandeAchats.listLignesDemandeAchats.push(this.managedLigneDemandeAchats);

         } else  {
     this.demandeAchats.listLignesDemandeAchats[indexOfRow] = this.managedLigneDemandeAchats;

       }
      }
  }

  this.transformDataToByConsumedByMatTable() ;
  this.managedLigneDemandeAchats = new LigneDemandeAchats();
  this.manageLigneMatExpansionPanel.close();
  this.listLignesMatExpansionPanel.open();
}
}
/*calculPrixTotal()
{
  // this.managedLigneDemandeVentes.prixTotal = this.
  this.managedLigneDemandeAchats.prixTotal  = +  this.managedLigneDemandeAchats.prixUnt *
  this.managedLigneDemandeAchats.quantite  ;
  
}*/
selectedTabChanged($event) {
  if ($event.index === 4) { // Composition tab was selected
   // this.loadListArticles();
   } else if ($event.index === 1) {
    //  this.loadListSchemasConfiguration() ;
   }
  }
}
