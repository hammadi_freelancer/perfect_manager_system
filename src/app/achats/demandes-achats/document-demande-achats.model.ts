
import { BaseEntity } from '../../common/base-entity.model' ;

type TypeDocument =   'Cheque' | 'Traite' |'FactureAchats' | 'BonReception'
 | 'ReçuPayement' | 'BonCommande' |  'Autre' ;

export class DocumentDemandeAchats extends BaseEntity {
    constructor(
        public code?: string,
         public codeDemandeAchats?: string,
         public numeroDocument?: string,
        public dateReceptionObject?: Date,
        public dateReception?: string,
        public typeDocument?: TypeDocument,
        public fileExtension?: string,
        public imageFace1?: any,
        public imageFace2?: any,
     ) {
         super();
        
       }
  
}
