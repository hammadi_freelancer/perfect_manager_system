


export interface PayementDemandeAchatsElement {
         code: string ;
         dateOperation: string ;
         montantPaye: number;
          modePayement?: string;
          description?: string;
          numeroCheque?: string;
          dateCheque?: string;
          compte?: string;
          compteAdversaire?: string;
          banque?: string ;
          banqueAdversaire?: string ;
        }
