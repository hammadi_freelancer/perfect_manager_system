import { Component, OnInit, Output,ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {DemandeAchats} from './demande-achats.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {DemandeAchatsService} from './demande-achats.service';
// import { ActionData } from './action-data.interface';
import { Fournisseur } from '../../achats/fournisseurs/fournisseur.model';
import { DemandeAchatsElement } from './demande-achats.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DialogAddFournisseurComponent } from '../fournisseurs/dialog-add-fournisseur.component';
import { DialogAddPayementDemandeAchatsComponent} from './popups/dialog-add-payement-demande-achats.component';
import { DemandeAchatsDataFilter } from './demande-achats-data.filter';
import { SelectItem } from 'primeng/api';

@Component({
  selector: 'app-demandes-achats-grid',
  templateUrl: './demandes-achats-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class DemandesAchatsGridComponent implements OnInit {

  private demandeAchats: DemandeAchats =  new DemandeAchats();
  private selection = new SelectionModel<DemandeAchatsElement>(true, []);
  private demandeAchatsDataFilter = new DemandeAchatsDataFilter();
  private showSelectColumnsMultiSelect = false;
  private showFilter = false;
  private showFilterFournisseur = false;
@Output()
select: EventEmitter<DemandeAchats[]> = new EventEmitter<DemandeAchats[]>();

//private lastFournisseurAdded: Fournisseur;

 selectedDemandeAchats: DemandeAchats ;

 

 @Input()
 private listDemandesAchats: any = [] ;
 private checkedAll: false;

 private  dataDemandesAchatsSource: MatTableDataSource<DemandeAchatsElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay :ColumnDefinition[] = [];

 private columnsToSelect : SelectItem[];
  private  selectedColumnsDefinitions: string[];

 private selectedColumnsNames: string[];

 private modesPaiementsDemandeAchats: any[] = [
  {value: 'Comptant', viewValue: 'Comptant'},
  {value: 'Cheque', viewValue: 'Chèque'},
  {value: 'Credit', viewValue: 'Crédit'},
  {value: 'Avance', viewValue: 'Avance'},
  {value: 'Comptant_Cheque', viewValue: 'Comptant/Chèque'},
  {value: 'Comptant_Avance', viewValue: 'Comptant/Avance'},
  {value: 'Credit_Comptant', viewValue: 'Crédit/Comptant'},
  {value: 'Credit_Cheque', viewValue: 'Crédit/Chèque'},
  {value: 'Credit_Avance', viewValue: 'Crédit/Avance'},
  {value: 'Donnation', viewValue: 'Amicalité'}
];
  constructor( private route: ActivatedRoute, private demandeAchatsService: DemandeAchatsService,
    private router: Router, private matSnackBar: MatSnackBar,
  private  dialog: MatDialog ) {
  }
  ngOnInit() {
    this.columnsToSelect = [
      {label: 'N° Demande', value: 'numeroDemandeAchats', title: 'N° Demande'},
      {label: 'Date Demande', value: 'dateDemandeAchats', title: 'Date Demande'},
      {label: 'Déscription', value: 'description', title: 'Déscription'},
      {label: 'Budget Préparé', value: 'budgetPrepare', title: 'Budget Préparé'},
      {label: 'Besoin Urgent', value: 'besoinUrgentToutes', title: 'Besoin Urgent'},
      {label: 'Mode Paiement', value: 'modePayementDesire', title: 'Mode Paiement'},
      {label: 'Disponibilité', value: 'dateDisponibilite', title: 'Disponibilité'},
      {label: 'Traité', value: 'traiteToutes', title: 'Traité'},
      {label: 'Rs.Soc.Fournisseur', value:  'raisonSociale', title: 'Rs.Soc.Fournisseur' },
      {label: 'Matr.Fournisseur', value:  'matriculeFiscale', title: 'Matr.Fournisseur' },
      {label: 'Reg.Fournisseur', value:  'registreCommerce', title: 'Reg.Fournisseur' },
      {label: 'CIN Fournisseur', value: 'cin', title: 'CIN Fournisseur'},
      {label: 'Nom Fournisseur', value: 'nom', title: 'Nom Fournisseur'},
      {label: 'Prénom Fournisseur', value: 'prenom', title: 'Prénom Fournisseur'},
      {label: 'Etat', value:  'etat', title: 'Etat' },
  
  ];
  this.selectedColumnsDefinitions = [
    'numeroDemandeAchats',
    'dateDemandeAchats' , 
    'budgetPrepare', 
    'nom' ,
    'prenom',
    'cin', 
    'etat'
    ];
    this.defaultColumnsDefinitions = [
      {name: 'numeroDemandeAchats' , displayTitle: 'N°Demande'},
      {name: 'dateDemandeAchats' , displayTitle: 'Date Demande'},
      {name: 'budgetPrepare' , displayTitle: 'Budget Préparé'},
      {name: 'besoinUrgentToutes' , displayTitle: 'Besoin Urgent'},
      {name: 'traiteToutes' , displayTitle: 'Traitée'},
      {name: 'modePayementDesire' , displayTitle: 'Mode Paiement'},
      {name: 'dateDisponibilite' , displayTitle: 'Disponibilité'},
       {name: 'nom' , displayTitle: 'Nom Four.'},
      {name: 'prenom' , displayTitle: 'Prénom Four.'},
      {name: 'cin' , displayTitle: 'CIN Four.'},
      {name: 'matriculeFiscale' , displayTitle: 'Matr. Four.'},
      {name: 'raisonSociale' , displayTitle: 'Raison Four.'},
      {name: 'registreCommerce' , displayTitle: 'Registre Four.'},
       {name: 'etat', displayTitle: 'Etat' },
      ];
    this.demandeAchatsService.getPageDemandesAchats(0, 5, null).subscribe((demandeAchats) => {
      this.listDemandesAchats = demandeAchats;
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });


  }
  deleteDemandeAchats(demandeAchats) {
     //  console.log('call delete !', demandeAchat );
    this.demandeAchatsService.deleteDemandeAchats(demandeAchats.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Fournisseur();
        console.log('call delete !', response);
        this.loadData(null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.demandeAchatsService.getPageDemandesAchats(0, 5, filter).subscribe((demandeAchats) => {
    this.listDemandesAchats = demandeAchats;
    this.transformDataToByConsumedByMatTable();
   });
}


checkOneActionInvoked() {
  const listSelectedDemandesAchats: DemandeAchats[] = [];
console.log('selected clients are ');
console.log(this.selection.selected);
if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((demandeAchatsElement) => {
  return demandeAchatsElement.code;
});
this.listDemandesAchats.forEach(dAchats => {
  if (codes.findIndex(code => code === dAchats.code) > -1) {
    listSelectedDemandesAchats.push(dAchats);
  }
  });
}
this.select.emit(listSelectedDemandesAchats);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listDemandesAchats !== undefined) {
    this.listDemandesAchats.forEach(demandeAchats => {
      listElements.push( {
         code: demandeAchats.code ,
         numeroDemandeAchats: demandeAchats.numeroDemandeAchats,
         dateDemandeAchats: demandeAchats.dateDemandeAchats,
        cin: demandeAchats.fournisseur.cin,
        nom: demandeAchats.fournisseur.nom,
         prenom: demandeAchats.fournisseur.prenom,
        matriculeFiscale: demandeAchats.fournisseur.matriculeFiscale,
        raisonSociale: demandeAchats.fournisseur.raisonSociale ,
        budgetPrepare: demandeAchats.budgetPrepare,
       //  montantBeneficeEstime: demandeAchats.montantBeneficeEstime,
        besoinUrgentToutes: demandeAchats.besoinUrgentToutes,
        modePayementDesire: demandeAchats.modePayementDesire,
        dateDisponibilite: demandeAchats.dateDisponibilite,
        traiteToutes: demandeAchats.traiteToutes,
        etat: demandeAchats.etat,
        
    });
  });
    this.dataDemandesAchatsSource = new MatTableDataSource<DemandeAchatsElement>(listElements);
    this.dataDemandesAchatsSource.sort = this.sort;
    this.dataDemandesAchatsSource.paginator = this.paginator;
    this.demandeAchatsService.getTotalDemandesAchats(null).subscribe((response) => {
      console.log('Total credits is ', response);
       this.dataDemandesAchatsSource.paginator.length = response.totalDemandesAchats;
     });
}
}
specifyListColumnsToBeDisplayed() {
  
   console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
   this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
 ?
    this.defaultColumnsDefinitions : [];
    this.selectedColumnsNames = [];
    
    if (this.selectedColumnsDefinitions !== undefined)
     {
       this.selectedColumnsDefinitions.forEach((colName) => { 
            const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
            const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
            const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
            this.columnsDefinitionsToDisplay.push(colDef);
           });
     }
    this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
   this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
              return colDef.displayTitle;
    });
    console.log('columns to disp:', this.columnsDefinitionsToDisplay);
    this.columnsNames = [];
    this.columnsNames[0] = 'select';
    this.columnsDefinitionsToDisplay .map((colDef) => {
     this.columnsNames.push(colDef.name);
    if (this.selectedColumnsDefinitions !== undefined) {
     this.selectedColumnsNames.push(colDef.displayTitle);
    }
 });
    this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
     return colDef.displayTitleAr;
 });
 }
masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataDemandesAchatsSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataDemandesAchatsSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataDemandesAchatsSource.filter = filterValue.trim().toLowerCase();
  if (this.dataDemandesAchatsSource.paginator) {
    this.dataDemandesAchatsSource.paginator.firstPage();
  }
}

loadAddDemandeAchatsComponent() {
  this.router.navigate(['/pms/achats/ajouterDemandeAchats']) ;

}
loadEditDemandeAchatsComponent() {
  this.router.navigate(['/pms/achats/editerDemandeAchats', this.selection.selected[ this.selection.selected.length - 1].code]) ;

}
supprimerDemandesAchats()
{

  if ( this.selection.selected.length !== 0) {
      this.selection.selected.forEach(function(demandeAchats, index) {

   this.demandeAchatsService.deleteDemandeAchats(demandeAchats.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.openSnackBar(  ' Demande(s) Achats Supprimé(s)');
            this.selection.selected = [];
            this.loadData();
            }
      }, this);
    } else {
      this.openSnackBar('Aucune Demande Séléctionnée !');
    }
}
refresh() {
  this.loadData(null);
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 addPayement()
 {
  if ( this.selection.selected.length !== 0) {
    
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
    top: '0'
  };
  dialogConfig.data = {
    codeDemandeAchats : this.selection.selected[0].code
  };

  const dialogRef = this.dialog.open(DialogAddPayementDemandeAchatsComponent,
    dialogConfig
  );
} else {
  this.openSnackBar('Aucune Demande Séléctionnée !');
}
 }
 history()
 {
   
 }
 addFournisseur()
 {
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
  };

  const dialogRef = this.dialog.open(DialogAddFournisseurComponent,
    dialogConfig
  );
 }
 attacherDocuments()
 {

 }


 changePage($event) {
  console.log('page event is ', $event);
 this.demandeAchatsService.getPageDemandesAchats($event.pageIndex, $event.pageSize,
this.demandeAchatsDataFilter ).subscribe((relvesAchats) => {
    this.listDemandesAchats= relvesAchats;
   //  console.log(this.listFacturesAchats);
   // this.transformDataToByConsumedByMatTable();
   const listElements = [];
   this.listDemandesAchats.forEach(demandeAchats => {
    listElements.push( {
      code: demandeAchats.code ,
      numeroDemandeAchats: demandeAchats.numeroDemandeAchats,
      dateDemandeAchats: demandeAchats.dateDemandeAchats,
     cin: demandeAchats.fournisseur.cin,
     nom: demandeAchats.fournisseur.nom,
      prenom: demandeAchats.fournisseur.prenom,
     matriculeFiscale: demandeAchats.fournisseur.matriculeFiscale,
     raisonSociale: demandeAchats.fournisseur.raisonSociale ,
     budgetPrepare: demandeAchats.budgetPrepare,
    //  montantBeneficeEstime: demandeAchats.montantBeneficeEstime,
     besoinUrgentToutes: demandeAchats.besoinUrgentToutes,
     modePayementDesire: demandeAchats.modePayementDesire,
     dateDisponibilite: demandeAchats.dateDisponibilite,
     traiteToutes: demandeAchats.traiteToutes,
     etat: demandeAchats.etat,
  });
});
     this.dataDemandesAchatsSource = new MatTableDataSource<DemandeAchatsElement>(listElements);
  
     this.demandeAchatsService.getTotalDemandesAchats(this.demandeAchatsDataFilter).subscribe((response) => {
      console.log('Total credits is ', response);
      if(this.dataDemandesAchatsSource.paginator !== undefined) {
       this.dataDemandesAchatsSource.paginator.length = response.totalDemandesAchats;
      }
     });

    });
}
toggleFilterPanel() {
  this.showFilter = !this.showFilter;
}
toggleShowColumnsMultiSelect() {
 this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
}
loadFiltredData() {
 console.log('the filter is ', this.demandeAchatsDataFilter);
 this.loadData(this.demandeAchatsDataFilter);
}
activateFilterFournisseur() {
  this.showFilterFournisseur = !this.showFilterFournisseur;
}

}
