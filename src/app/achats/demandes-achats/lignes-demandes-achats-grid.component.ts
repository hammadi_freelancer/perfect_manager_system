
import { Component, OnInit, Inject, ViewChild, Input, AfterViewChecked, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {DemandeAchatsService} from './demande-achats.service';
import { DemandeAchats } from './demande-achats.model';
import {LigneDemandeAchats} from './ligne-demande-achats.model';
import { LigneDemandeAchatsElement } from './ligne-demande-achats.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
 import { DialogAddLigneDemandeAchatsComponent} from './popups/dialog-add-ligne-demande-achats.component';
 import { DialogEditLigneDemandeAchatsComponent } from './popups/dialog-edit-ligne-demande-achats.component';
@Component({
    selector: 'app-lignes-demandes-achats-grid',
    templateUrl: 'lignes-demandes-achats-grid.component.html',
  })
  export class LignesDemandesAchatsGridComponent implements OnInit, OnChanges {
     private listLignesDemandesAchats: LigneDemandeAchats[] = [];
     private  dataLignesDemandesAchatsSource: MatTableDataSource<LigneDemandeAchatsElement>;
     private selection = new SelectionModel<LigneDemandeAchatsElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];

     @Input()
     private demandeAchats: DemandeAchats;

     constructor(  private demandeAchatsService: DemandeAchatsService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      // console.log('credit code after dipslay tab ', this.credit.code);
      /*this.demandeAchatsService.getLignesR(this.factureAchats.code).subscribe((listLignesFactAchats) => {
        this.listLignesFacturesAchats= listLignesFactAchats;
// console.log('recieving data from backend', this.listPayementsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });*/

  }

  ngOnChanges() {
   /* this.factureAchatsService.getLignesFacturesAchatsByCodeFacture(this.factureAchats.code).subscribe((listLignesFactAchats) => {
      this.listLignesFacturesAchats= listLignesFactAchats;
// console.log('recieving data from backend', this.listPayementsCredits  );
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });*/
  }
  checkOneActionInvoked() {
    const listSelectedLignesRelAchats: LigneDemandeAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((lFactElement) => {
    return lFactElement.code;
  });
  this.listLignesDemandesAchats.forEach(ligneF => {
    if (codes.findIndex(code => code === ligneF.code) > -1) {
      listSelectedLignesRelAchats.push(ligneF);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listLignesDemandesAchats !== undefined) {
 
      if (this.demandeAchats.listLignesDemandeAchats!== undefined) {
        this.demandeAchats.listLignesDemandeAchats.forEach(ligneDem => {
               listElements.push( {code: ligneDem.code ,
                numLigneDemandeAchats: ligneDem.numLigneDemandeAchats,
                article: ligneDem.article.code,
                 unMesure: ligneDem.unMesure.code,
                 quantite: ligneDem.quantite,
                 prixDesire: ligneDem.prixDesire,
                 besoinUrgent: ligneDem.besoinUrgent,
                 traite: ligneDem.traite,
                 dateDisponibilite: ligneDem.dateDisponibilite,
          } );
        });
      }
    }
      this.dataLignesDemandesAchatsSource = new MatTableDataSource<LigneDemandeAchatsElement>(listElements);
      this.dataLignesDemandesAchatsSource.sort = this.sort;
      this.dataLignesDemandesAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsDefinitions = [{name: 'numLigneDemandeAchats' , displayTitle: 'N°'},
    {name: 'article' , displayTitle: 'Art.'},
    {name: 'unMesure' , displayTitle: 'Un.Mes.'},
    {name: 'quantite' , displayTitle: 'Qté'},
    {name: 'prixDesire' , displayTitle: 'P.Désiré'},
    {name: 'besoinUrgent' , displayTitle: 'Prioritaire'},
    {name: 'traite' , displayTitle: 'Traite'},
    {name: 'dateDisponibilite' , displayTitle: 'Date Disp.'},
     ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataLignesDemandesAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataLignesDemandesAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataLignesDemandesAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataLignesDemandesAchatsSource.paginator) {
      this.dataLignesDemandesAchatsSource.paginator.firstPage();
    }
  }
  showAddLigneDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeDemandeAchats : this.demandeAchats.code
   };
 
   const dialogRef = this.dialog.open(DialogAddLigneDemandeAchatsComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(lAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditLigneDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeLigneDemandeAchats: this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditLigneDemandeAchatsComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(lAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucune Ligne Séléctionnée!');
    }
    }
    supprimerLignes()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(pay, index) {
              this.demandeAchatsService.deleteLigneDemandeAchats(pay.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar(  ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucune Ligne Séléctionnée!');
        }
    }
    refresh() {
     /* this.demandeAchatsService.getLignes(this.factureAchats.code).subscribe((listLignesFacs) => {
        this.listLignesFacturesAchats = listLignesFacs;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });*/
    }
















}
