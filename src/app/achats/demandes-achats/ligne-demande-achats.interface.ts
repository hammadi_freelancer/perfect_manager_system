export interface LigneDemandeAchatsElement {
   code: string ;
   numLigneDemandeAchats: string ;
   article: string;
   unMesure: string;
   quantite: number;
   prixDesire: number;
   traite: boolean;
   besoinUrgent: boolean;
    dateDisponibilite: string;
    }
