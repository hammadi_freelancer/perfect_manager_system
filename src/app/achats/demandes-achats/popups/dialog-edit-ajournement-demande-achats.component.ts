
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {DemandeAchatsService} from '../demande-achats.service';
import {AjournementDemandeAchats} from '../ajournement-demande-achats.model';

@Component({
    selector: 'app-dialog-edit-ajournement-demande-achats',
    templateUrl: 'dialog-edit-ajournement-demande-achats.component.html',
  })
  export class DialogEditAjournementDemandeAchatsComponent implements OnInit {
     private ajournementDemandeAchats: AjournementDemandeAchats;
     private updateEnCours = false;
     
     constructor(  private demandeAchatsService: DemandeAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {

        this.demandeAchatsService.getAjournementDemandeAchats(this.data.codeAjournementDemandeAchats).subscribe((ajour) => {
            this.ajournementDemandeAchats = ajour;
     });


    }
    updateAjournementDemandeAchats() 
    {
     this.updateEnCours = true;
       this.demandeAchatsService.updateAjournementDemandeAchats(this.ajournementDemandeAchats).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.openSnackBar(  'Ajournement Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
