
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {DemandeAchatsService} from '../demande-achats.service';
import {PayementDemandeAchats} from '../payement-demande-achats.model';
import {LigneDemandeAchats} from '../ligne-demande-achats.model';

@Component({
    selector: 'app-dialog-add-ligne-demande-achats',
    templateUrl: 'dialog-add-ligne-demande-achats.component.html',
  })
  export class DialogAddLigneDemandeAchatsComponent implements OnInit {
     private ligneDemandeAchats: LigneDemandeAchats;
     private listCodesArticles: string[];
     private listCodesMagasins: string[];
     private listCodesUnMesures: string[];
     
     private etatsLignes: any[] = [
      {value: 'Annule', viewValue: 'Annulé'},
      {value: 'Initial', viewValue: 'Initial'},
      {value: 'Valide', viewValue: 'Validé'}
    ];
     constructor(  private  demandeAchatsService: DemandeAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.ligneDemandeAchats = new LigneDemandeAchats();
           this.listCodesArticles = this.data.listCodesArticles;
           // this.listCodesMagasins = this.data.listCodesMagasins;
           
          // this.ligneDemandeVentes.etat = 'Initial';
           
    }
    saveLigneDemandeAchats() 
    {
      this.ligneDemandeAchats.codeDemandeAchats = this.data.codeDemandeAchats;
      //  console.log(this.ligneFactureVentes);
      //  this.saveEnCours = true;
/*    this.demandeVentesService.addLigne(this.ligneDemandeVentes).subscribe((response) => {
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.ligneDemandeVentes = new LigneDemandeVente();
             this.openSnackBar(  'Ligne Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });*/
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
