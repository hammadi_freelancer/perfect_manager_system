
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {DemandeAchatsService} from '../demande-achats.service';
import {DemandeAchats} from '../demande-achats.model';

import { LigneDemandeAchats } from '../ligne-demande-achats.model';
@Component({
    selector: 'app-dialog-edit-ligne-demande-achats',
    templateUrl: 'dialog-edit-ligne-demande-achats.component.html',
  })
  export class DialogEditLigneDemandeAchatsComponent implements OnInit {

     private ligneDemandeAchats: LigneDemandeAchats;
     private updateEnCours = false;
     private listCodesArticles: string[];
     private listCodesMagasins: string[];
     private listCodesUnMesures: string[];
     
     private etatsLignes: any[] = [
      {value: 'Annule', viewValue: 'Annulé'},
      {value: 'Initial', viewValue: 'Initial'},
      {value: 'Valide', viewValue: 'Validé'}
    ];
     constructor(  private demandeAchatsService: DemandeAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit() {
       /* this.demandeVentesService.get(this.data.codeLigneFactureVentes).subscribe((ligne) => {
            this.ligneFactureVentes = ligne;
            this.listCodesArticles = this.data.listCodesArticles;
            // this.listCodesMagasins = this.data.listCodesMagasins;
            
     });*/
   }
    updateLigneDemandeAchats() {
      //  this.saveEnCours = true;
    /*  this.updateEnCours = true;
       this.factureVentesService.updateLigneFactureVentes(this.ligneFactureVentes).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             // this.payementCredit = new PayementCredit();
             this.openSnackBar(  'Ligne Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });*/
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
