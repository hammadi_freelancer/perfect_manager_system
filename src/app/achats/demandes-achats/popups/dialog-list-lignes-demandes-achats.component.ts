
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {DemandeAchatsService} from '../demande-achats.service';
import {LigneDemandeAchats} from '../ligne-demande-achats.model';
import { LigneDemandeAchatsElement } from '../ligne-demande-achats.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-lignes-demande-achats',
    templateUrl: 'dialog-list-lignes-demandes-achats.component.html',
  })
  export class DialogListLignesDemandesAchatsComponent implements OnInit {
     private listLignesDemandesAchats: LigneDemandeAchats[] = [];
     private  dataLignesDemandeAchatsSource: MatTableDataSource<LigneDemandeAchatsElement>;
     private selection = new SelectionModel<LigneDemandeAchatsElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private demandeAchatsService: DemandeAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      // console.log('code credit in dialog component is :', this.data.codeFactureVentes);
    /*  this.demandeVentesService.get(this.data.codeFactureVentes)
      .subscribe((listLignesFactVentes) => {
        this.listLignesFactureVentes = listLignesFactVentes;
// console.log('recieving data from backend', this.listPayementsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });*/

  }


  checkOneActionInvoked() {
    const listSelectedLignesDemandeAchats: LigneDemandeAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((ligneDemandeAchatsElement) => {
    return ligneDemandeAchatsElement.code;
  });
  this.listLignesDemandesAchats.forEach(ligneRAchats => {
    if (codes.findIndex(code => code === ligneRAchats.code) > -1) {
      listSelectedLignesDemandeAchats.push(ligneRAchats);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listLignesDemandesAchats!== undefined) {
      this.listLignesDemandesAchats.forEach(ligneDem => {
             listElements.push( {code: ligneDem.code ,
              numLigneDemandeAchats: ligneDem.numLigneDemandeAchats,
              article: ligneDem.article.code,
               unMesure: ligneDem.unMesure.code,
               quantite: ligneDem.quantite,
               prixDesire: ligneDem.prixDesire,
               besoinUrgent: ligneDem.besoinUrgent,
               traite: ligneDem.traite,
               dateDisponibilite: ligneDem.dateDisponibilite,
        } );
      });
    }
      this.dataLignesDemandeAchatsSource= new MatTableDataSource<LigneDemandeAchatsElement>(listElements);
      this.dataLignesDemandeAchatsSource.sort = this.sort;
      this.dataLignesDemandeAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsDefinitions = [{name: 'numLigneDemandeAchats' , displayTitle: 'N°'},
    {name: 'article' , displayTitle: 'Art.'},
    {name: 'unMesure' , displayTitle: 'Un.Mes.'},
    {name: 'quantite' , displayTitle: 'Qté'},
    {name: 'prixDesire' , displayTitle: 'P.Désiré'},
    {name: 'besoinUrgent' , displayTitle: 'Prioritaire'},
    {name: 'traite' , displayTitle: 'Traite'},
    {name: 'dateDisponibilite' , displayTitle: 'Date Disp.'},
     ];
 
     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataLignesDemandeAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataLignesDemandeAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataLignesDemandeAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataLignesDemandeAchatsSource.paginator) {
      this.dataLignesDemandeAchatsSource.paginator.firstPage();
    }
  }

}
