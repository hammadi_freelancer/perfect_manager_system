
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {DemandeAchatsService} from '../demande-achats.service';
import {AjournementDemandeAchats} from '../ajournement-demande-achats.model';
import { AjournementDemandeAchatsElement } from '../ajournement-demande-achats.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-ajournements-demandes-achats',
    templateUrl: 'dialog-list-ajournements-demandes-achats.component.html',
  })
  export class DialogListAjournementsDemandesAchatsComponent implements OnInit {
     private listAjournementDemandesAchats: AjournementDemandeAchats[] = [];
     private  dataAjournementsDemandesAchatsSource: MatTableDataSource<AjournementDemandeAchatsElement>;
     private selection = new SelectionModel<AjournementDemandeAchatsElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private demandeAchatsService: DemandeAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      // console.log('code credit in dialog component is :', this.data.codeCredit);
      this.demandeAchatsService.getAjournementsDemandesAchatsByCodeDemande(this.data.codeDemandeAchats)
      .subscribe((listAjournementsRelAchats) => {
        this.listAjournementDemandesAchats = listAjournementsRelAchats;
// console.log('recieving data from backend', this.listAjournementCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedAjournementsDemandeAchats: AjournementDemandeAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((ajouRelElement) => {
    return ajouRelElement.code;
  });
  this.listAjournementDemandesAchats.forEach(ajournementRelAchats => {
    if (codes.findIndex(code => code === ajournementRelAchats.code) > -1) {
      listSelectedAjournementsDemandeAchats.push(ajournementRelAchats);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listAjournementDemandesAchats !== undefined) {

      this.listAjournementDemandesAchats.forEach(ajournementRAchats=> {
             listElements.push( {code: ajournementRAchats.code ,
          dateOperation: ajournementRAchats.dateOperation,
          nouvelleDatePayement: ajournementRAchats.nouvelleDatePayement,
          motif: ajournementRAchats.motif,
          description: ajournementRAchats.description
        } );
      });
    }
      this.dataAjournementsDemandesAchatsSource = new MatTableDataSource<AjournementDemandeAchatsElement>(listElements);
      this.dataAjournementsDemandesAchatsSource.sort = this.sort;
      this.dataAjournementsDemandesAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'},
     {name: 'nouvelleDatePayement' , displayTitle: 'N°elle Date Pay.'},
     {name: 'motif' , displayTitle: 'Motif'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataAjournementsDemandesAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataAjournementsDemandesAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataAjournementsDemandesAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataAjournementsDemandesAchatsSource.paginator) {
      this.dataAjournementsDemandesAchatsSource.paginator.firstPage();
    }
  }

}
