
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {DemandeAchatsService} from '../demande-achats.service';
import {PayementDemandeAchats} from '../payement-demande-achats.model';
import { PayementDemandeAchatsElement } from '../payement-demande-achats.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-payements-demandes-achats',
    templateUrl: 'dialog-list-payements-demandes-achats.component.html',
  })
  export class DialogListPayementsDemandesAchatsComponent implements OnInit {
     private listPayementsDemandeAchats: PayementDemandeAchats[] = [];
     private  dataPayementsDemandeAchatsSource: MatTableDataSource<PayementDemandeAchatsElement>;
     private selection = new SelectionModel<PayementDemandeAchatsElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private demandeAchatsService: DemandeAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      // console.log('code credit in dialog component is :', this.data.codeFactureVentes);
      this.demandeAchatsService.getPayementsDemandesAchatsByCodeDemande(this.data.codeDemandeAchats)
      .subscribe((listPayementsRelAchats) => {
        this.listPayementsDemandeAchats= listPayementsRelAchats;
// console.log('recieving data from backend', this.listPayementsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedPayementsDemandeAchats: PayementDemandeAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((payementRelAchatsElement) => {
    return payementRelAchatsElement.code;
  });
  this.listPayementsDemandeAchats.forEach(payementRAchats => {
    if (codes.findIndex(code => code === payementRAchats.code) > -1) {
      listSelectedPayementsDemandeAchats.push(payementRAchats);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listPayementsDemandeAchats !== undefined) {

      this.listPayementsDemandeAchats.forEach(payementRelAchats => {
             listElements.push( {code: payementRelAchats.code ,
          dateOperation: payementRelAchats.dateOperation,
          montantPaye: payementRelAchats.montantPaye,
          modePayement: payementRelAchats.modePayement,
          numeroCheque: payementRelAchats.numeroCheque,
          dateCheque: payementRelAchats.dateCheque,
          compte: payementRelAchats.compte,
          compteAdversaire: payementRelAchats.compteAdversaire,
          banque: payementRelAchats.banque,
          banqueAdversaire: payementRelAchats.banqueAdversaire,
          description: payementRelAchats.description
        } );
      });
    }
      this.dataPayementsDemandeAchatsSource = new MatTableDataSource<PayementDemandeAchatsElement>(listElements);
      this.dataPayementsDemandeAchatsSource.sort = this.sort;
      this.dataPayementsDemandeAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'}, 
     {name: 'montantPaye' , displayTitle: 'Montant Payé'},
     {name: 'modePayement' , displayTitle: 'Mode Payement'},
      {name: 'numeroCheque' , displayTitle: 'N°Chèque'},
     {name: 'dateCheque' , displayTitle: 'Date Chèque'}, 
     {name: 'compte' , displayTitle: 'Compte'},
     {name: 'compteAdversaire' , displayTitle: 'Compte Adv.'},
      {name: 'banque' , displayTitle: 'Banque'},
       { name : 'banqueAdversaire' , displayTitle : 'Banque Adv.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataPayementsDemandeAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataPayementsDemandeAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataPayementsDemandeAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataPayementsDemandeAchatsSource.paginator) {
      this.dataPayementsDemandeAchatsSource.paginator.firstPage();
    }
  }

}
