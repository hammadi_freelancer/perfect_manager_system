
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {DemandeAchatsService} from '../demande-achats.service';
import {DocumentDemandeAchats} from '../document-demande-achats.model';
import { DocumentDemandeAchatsElement } from '../document-demande-achats.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-documents-demandes-achats',
    templateUrl: 'dialog-list-documents-demandes-achats.component.html',
  })
  export class DialogListDocumentsDemandesAchatsComponent implements OnInit {
     private listDocumentsDemandesAchats: DocumentDemandeAchats[] = [];
     private  dataDocumentsDemandesAchats: MatTableDataSource<DocumentDemandeAchatsElement>;
     private selection = new SelectionModel<DocumentDemandeAchatsElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private demandeAchatsService: DemandeAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
     // console.log('code credit in dialog component is :', this.data.codeCredit);
      this.demandeAchatsService.getDocumentsDemandeAchatsByCodeDemande(this.data.codeDemandeAchats).subscribe((listDocumentRelAchats) => {
        this.listDocumentsDemandesAchats = listDocumentRelAchats;
//console.log('recieving data from backend', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedDocumentsRelAchats: DocumentDemandeAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentRElement) => {
    return documentRElement.code;
  });
  this.listDocumentsDemandesAchats.forEach(documentRA => {
    if (codes.findIndex(code => code === documentRA.code) > -1) {
      listSelectedDocumentsRelAchats.push(documentRA);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsDemandesAchats!== undefined) {

      this.listDocumentsDemandesAchats.forEach(docRAchats => {
             listElements.push( {code: docRAchats.code ,
          dateReception: docRAchats.dateReception,
          numeroDocument: docRAchats.numeroDocument,
          typeDocument: docRAchats.typeDocument,
          description: docRAchats.description
        } );
      });
    }
      this.dataDocumentsDemandesAchats= new MatTableDataSource<DocumentDemandeAchatsElement>(listElements);
      this.dataDocumentsDemandesAchats.sort = this.sort;
      this.dataDocumentsDemandesAchats.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsDemandesAchats.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsDemandesAchats.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsDemandesAchats.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsDemandesAchats.paginator) {
      this.dataDocumentsDemandesAchats.paginator.firstPage();
    }
  }

}
