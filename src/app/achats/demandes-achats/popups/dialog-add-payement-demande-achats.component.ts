
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {DemandeAchatsService} from '../demande-achats.service';
import {PayementDemandeAchats} from '../payement-demande-achats.model';

@Component({
    selector: 'app-dialog-add-payement-demande-achats',
    templateUrl: 'dialog-add-payement-demande-achats.component.html',
  })
  export class DialogAddPayementDemandeAchatsComponent implements OnInit {
     private payementDemandeAchats: PayementDemandeAchats;
     
     constructor(  private  demandeAchatsService: DemandeAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.payementDemandeAchats = new PayementDemandeAchats();
    }
    savePayementDemandeAchats() 
    {
      this.payementDemandeAchats.codeDemandeAchats= this.data.codeDemandeAchats;
      // console.log(this.payementFactureVentes);
      //  this.saveEnCours = true;
       this.demandeAchatsService.addPayementDemandeAchats(this.payementDemandeAchats).subscribe((response) => {
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.payementDemandeAchats = new PayementDemandeAchats();
             this.openSnackBar(  'Payement Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
