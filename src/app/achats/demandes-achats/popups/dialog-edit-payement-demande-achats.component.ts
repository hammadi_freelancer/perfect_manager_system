
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {DemandeAchatsService} from '../demande-achats.service';
import {DemandeAchats} from '../demande-achats.model';
import { PayementDemandeAchats } from '../payement-demande-achats.model';
@Component({
    selector: 'app-dialog-edit-payement-demande-achats',
    templateUrl: 'dialog-edit-payement-demande-achats.component.html',
  })
  export class DialogEditPayementDemandeAchatsComponent implements OnInit {

     private payementDemandeAchats: PayementDemandeAchats;
     private updateEnCours = false;
     constructor(  private demandeAchatsService: DemandeAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit() {
        this.demandeAchatsService.getPayementDemandeAchats(this.data.codePayementDemandeAchats).subscribe((payement) => {
            this.payementDemandeAchats = payement;
     });
   }
    updatePayementDemandeAchats() {
      //  this.saveEnCours = true;
      this.updateEnCours = true;
       this.demandeAchatsService.updatePayementDemandeAchats(this.payementDemandeAchats).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             // this.payementCredit = new PayementCredit();
             this.openSnackBar(  'Payement Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
