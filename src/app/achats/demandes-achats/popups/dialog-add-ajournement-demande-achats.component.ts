
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {DemandeAchatsService} from '../demande-achats.service';
import {AjournementDemandeAchats} from '../ajournement-demande-achats.model';

@Component({
    selector: 'app-dialog-add-ajournement-demande-achats',
    templateUrl: 'dialog-add-ajournement-demande-achats.component.html',
  })
  export class DialogAddAjournementDemandeAchatsComponent implements OnInit {
     private ajournementDemandeAchats: AjournementDemandeAchats;
     private saveEnCours = false;
     constructor(  private demandeAchatsService: DemandeAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.ajournementDemandeAchats = new AjournementDemandeAchats;
    }
    saveAjournementDemandeAchats() 
    {
      this.ajournementDemandeAchats.codeDemandeAchats = this.data.codeDemandeAchats;
      this.saveEnCours = true;
      // console.log(this.ajournementCredit);
      //  this.saveEnCours = true;
       this.demandeAchatsService.addAjournementDemandeAchats(this.ajournementDemandeAchats).subscribe((response) => {
        this.saveEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.ajournementDemandeAchats = new AjournementDemandeAchats();
             this.openSnackBar(  'Ajournement Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
