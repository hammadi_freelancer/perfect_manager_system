import { Article } from '../../stocks/articles/article.model';
import { Magasin } from '../../stocks/magasins/magasin.model';
import { UnMesure } from '../../stocks/un-mesures/un-mesure.model';
import { BaseEntity } from '../../common/base-entity.model' ;
// type LibelleModePayement = 'Credit'| 'Credit_Espece' | 'Cheque' | 'Espece';
// type Etat = 'Initial' | 'Valide' | 'Annule' | '' | 'Regle' | 'NonRegle';
type TypeModePayement = 'Credit'| 'Comptant' | 'Cheque' | '';

type  EtatDemandeAchats= |'Initial' |'Valide' | 'Annule' | '';

export class LigneDemandeAchats extends BaseEntity {
    constructor(
        public code?: string,
        public numLigneDemandeAchats?: string,
        public codeDemandeAchats?: string,
        public numeroDemandeAchats?: string,
        public article?: Article,
        public unMesure?: UnMesure,
        public quantite?: number,
        public prixDesire?: number,
         public traite?: boolean,
         public besoinUrgent?: boolean,
         public dateDisponibiliteObject?: Date,
         public dateDisponibilite?: string,
         
     ) {
        super();
         this.article = new Article();
         this.unMesure = new UnMesure();
         this.prixDesire = Number(0);
         this.quantite = Number(0);
         
    }
}
