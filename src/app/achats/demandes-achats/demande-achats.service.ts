import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import {DemandeAchats} from './demande-achats.model' ;
import {DocumentDemandeAchats} from './document-demande-achats.model' ;
import {PayementDemandeAchats} from './payement-demande-achats.model' ;
import {AjournementDemandeAchats} from './ajournement-demande-achats.model' ;

import {CreditAchats} from '../credits-achats/credit-achats.model' ;
import {LigneDemandeAchats} from './ligne-demande-achats.model' ;

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_DEMANDE_ACHATS = 'http://localhost:8082/achats/demandesAchats/getDemandesAchats';
const  URL_GET_PAGE_DEMANDE_ACHATS = 'http://localhost:8082/achats/demandesAchats/getPageDemandesAchats';

const URL_GET_DEMANDE_ACHATS = 'http://localhost:8082/achats/demandesAchats/getDemandeAchats';
const  URL_ADD_DEMANDE_ACHATS  = 'http://localhost:8082/achats/demandesAchats/addDemandeAchats';
const  URL_UPDATE_DEMANDE_ACHATS  = 'http://localhost:8082/achats/demandesAchats/updateDemandeAchats';
const  URL_DELETE_DEMANDE_ACHATS  = 'http://localhost:8082/achats/demandesAchats/deleteDemandeAchats';
const  URL_GET_TOTAL_DEMANDE_ACHATS = 'http://localhost:8082/achats/demandesAchats/getTotalDemandesAchats';

const  URL_ADD_PAYEMENT_DEMANDE_ACHATS = 'http://localhost:8082/finance/payementsDemandesAchats/addPayementDemandeAchats';
const  URL_GET_LIST_PAYEMENT_DEMANDE_ACHATS  = 'http://localhost:8082/finance/payementsDemandesAchats/getPayementsDemandeAchats';
const  URL_DELETE_PAYEMENT_DEMANDE_ACHATS  = 'http://localhost:8082/finance/payementsDemandesAchats/deletePayementDemandeAchats';
const  URL_UPDATE_PAYEMENT_DEMANDE_ACHATS  = 'http://localhost:8082/finance/payementsDemandesAchats/updatePayementDemandeAchats';
const URL_GET_PAYEMENT_DEMANDE_ACHATS  = 'http://localhost:8082/finance/payementsDemandesAchats/getPayementDemandeAchats';
const URL_GET_LIST_PAYEMENT_DEMANDE_ACHATS_BY_CODE_DEMANDE =
'http://localhost:8082/finance/payementsDemandesAchats/getPayementsDemandeAchatsByCodeDemande';


const  URL_ADD_DOCUMENT_DEMANDE_ACHATS = 'http://localhost:8082/achats/documentsDemandesAchats/addDocumentDemandeAchats';
const  URL_GET_LIST_DOCUMENTS_DEMANDE_ACHATS = 'http://localhost:8082/achats/documentsDemandesAchats/getDocumentsDemandeAchats';
const  URL_DELETE_DOCUMENT_DEMANDE_ACHATS= 'http://localhost:8082/achats/documentsDemandesAchats/deleteDocumentDemandeAchats';
const  URL_UPDATE_DOCUMENT_DEMANDE_ACHATS = 'http://localhost:8082/achats/documentsDemandesAchats/updateDocumentDemandeAchats';
const URL_GET_DOCUMENT_DEMANDE_ACHATS = 'http://localhost:8082/achats/documentsDemandesAchats/getDocumentDemandeAchats';
const URL_GET_LIST_DOCUMENTS_DEMANDE_ACHATS_BY_CODE_DEMANDE =
 'http://localhost:8082/achats/documentsDemandesAchats/getDocumentsDemandeAchatsByCodeDemande';

 const  URL_ADD_AJOURNEMENT_DEMANDE_ACHATS = 'http://localhost:8082/achats/ajournementsDemandesAchats/addAjournementDemandeAchats';
 const  URL_GET_LIST_AJOURNEMENT_DEMANDE_ACHATS = 'http://localhost:8082/achats/ajournementsDemandesAchats/getAjournementsDemandeAchats';
 const  URL_DELETE_AJOURNEMENT_DEMANDE_ACHATS= 'http://localhost:8082/achats/ajournementsDemandesAchats/deleteAjournementDemandeAchats';
 const  URL_UPDATE_AJOURNEMENT_DEMANDE_ACHATS = 'http://localhost:8082/achats/ajournementsDemandesAchats/updateAjournementDemandeAchats';
 const URL_GET_AJOURNEMENT_DEMANDE_ACHATS = 'http://localhost:8082/achats/ajournementsDemandesAchats/getAjournementDemandeAchats';
 const URL_GET_LIST_AJOURNEMENT_DEMANDE_ACHATS_BY_CODE_DEMANDE =
  'http://localhost:8082/achats/ajournementsDemandesAchats/getAjournementsDemandeAchatsByCodeDemande';



  /*const  URL_ADD_LIGNE_FACTURE_VENTES = 'http://localhost:8082/ventes/lignesFacturesVentes/addLigneFactureVentes';
  const  URL_GET_LIST_LIGNES_FACTURE_VENTES = 'http://localhost:8082/ventes/lignesFacturesVentes/getLignesFactureVentes';
  const  URL_DELETE_LIGNE_FACTURE_VENTES = 'http://localhost:8082/ventes/lignesFacturesVentes/deleteLigneFactureVentes';
  const  URL_UPDATE_LIGNE_FACTURE_VENTES = 'http://localhost:8082/ventes/lignesFacturesVentes/updateLigneFactureVentes';
  const URL_GET_LIGNE_FACTURE_VENTES = 'http://localhost:8082/ventes/lignesFacturesVentes/getLigneFactureVentes';
  const URL_GET_LIST_LIGNES_FACTURE_VENTES_BY_CODE_FACTURE_VENTES =
   'http://localhost:8082/ventes/lignesFacturesVentes/getLignesFactureVentesByCodeFactureVente';*/

@Injectable({
  providedIn: 'root'
})
export class DemandeAchatsService {

  constructor(private httpClient: HttpClient) {

   }
   getTotalDemandesAchats(filter): Observable<any> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('filter', filterStr);
    return this.httpClient
    .get<any>(URL_GET_TOTAL_DEMANDE_ACHATS, {params});
   }


   getPageDemandesAchats(pageNumber, nbElementsPerPage, filter): Observable<DemandeAchats[]> {
    const filterStr = JSON.stringify(filter);
    
    const params = new HttpParams()
    .set('pageNumber', pageNumber)
    .set('nbElementsPerPage', nbElementsPerPage)
    .set('filter', filterStr);
    return this.httpClient
    .get<DemandeAchats[]>(URL_GET_PAGE_DEMANDE_ACHATS, {params});
   }

   getDemandesAchats(): Observable<DemandeAchats[]> {
   
    return this.httpClient
    .get<DemandeAchats[]>(URL_GET_LIST_DEMANDE_ACHATS);
   }
   addDemandeAchats(releveAchats: DemandeAchats): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_DEMANDE_ACHATS, releveAchats);
  }
  updateDemandeAchats(releveAchats: DemandeAchats): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_DEMANDE_ACHATS, releveAchats);
  }
  deleteDemandeAchats(codeDemandeAchats): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_DEMANDE_ACHATS+ '/' + codeDemandeAchats);
  }
  getDemandeAchats(codeDemandeAchats): Observable<DemandeAchats> {
    return this.httpClient.get<DemandeAchats>(URL_GET_DEMANDE_ACHATS+ '/' + codeDemandeAchats);
  }


  addPayementDemandeAchats(payementDemandeAchats: PayementDemandeAchats): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_PAYEMENT_DEMANDE_ACHATS, payementDemandeAchats);
    
  }
  getPayementsDemandesAchats(): Observable<PayementDemandeAchats[]> {
    return this.httpClient
    .get<PayementDemandeAchats[]>(URL_GET_LIST_DEMANDE_ACHATS);
   }
   getPayementDemandeAchats(codePayementDemandeAchats): Observable<PayementDemandeAchats> 
   {
    return this.httpClient.get<PayementDemandeAchats>(URL_GET_PAYEMENT_DEMANDE_ACHATS+ '/' + codePayementDemandeAchats);
  }
  deletePayementDemandeAchats(codePayementDemandeAchats): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_PAYEMENT_DEMANDE_ACHATS + '/' + codePayementDemandeAchats);
  }
  getPayementsDemandesAchatsByCodeDemande(codeDemandeAchats): Observable<PayementDemandeAchats[]> {
    return this.httpClient.get<PayementDemandeAchats[]>(URL_GET_LIST_PAYEMENT_DEMANDE_ACHATS_BY_CODE_DEMANDE
       + '/' + codeDemandeAchats);
  }

  updatePayementDemandeAchats(payDemAchats: PayementDemandeAchats): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_PAYEMENT_DEMANDE_ACHATS, payDemAchats);
  }

  addDocumentDemandeAchats(docDemandeAchats: DocumentDemandeAchats): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_DOCUMENT_DEMANDE_ACHATS, docDemandeAchats);
    
  }
  getDocumentsDemandesAchats(): Observable<DocumentDemandeAchats[]> {
    return this.httpClient
    .get<DocumentDemandeAchats[]>(URL_GET_LIST_DOCUMENTS_DEMANDE_ACHATS);
   }
   getDocumentDemandeAchats(codeDocumentDemandeAchats): Observable<DocumentDemandeAchats> {
    return this.httpClient.get<DocumentDemandeAchats>(URL_GET_DOCUMENT_DEMANDE_ACHATS + '/' + codeDocumentDemandeAchats);
  }
  deleteDocumentDemandeAchats(codeDocumentDemandeAchats): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_DOCUMENT_DEMANDE_ACHATS + '/' + codeDocumentDemandeAchats);
  }
  updateDocumentDemandeAchats(docDemAchats: DocumentDemandeAchats): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_DOCUMENT_DEMANDE_ACHATS, docDemAchats);
  }


  getDocumentsDemandeAchatsByCodeDemande(codeDemandeAchats): Observable<DocumentDemandeAchats[]> {
    return this.httpClient.get<DocumentDemandeAchats[]>( URL_GET_LIST_DOCUMENTS_DEMANDE_ACHATS_BY_CODE_DEMANDE +
       '/' + codeDemandeAchats);
  }

  addAjournementDemandeAchats(ajournDemandeAchats: AjournementDemandeAchats): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_AJOURNEMENT_DEMANDE_ACHATS, ajournDemandeAchats);
    
  }
  updateAjournementDemandeAchats(ajournDemandeAchats: AjournementDemandeAchats): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_AJOURNEMENT_DEMANDE_ACHATS, ajournDemandeAchats);
  }
  getAjournementsDemandesAchatsByCodeDemande(codeDemAchats): Observable<AjournementDemandeAchats[]> {
    return this.httpClient.get<AjournementDemandeAchats[]>(
      URL_GET_LIST_AJOURNEMENT_DEMANDE_ACHATS_BY_CODE_DEMANDE +
       '/' + codeDemAchats);
  }
  getAjournementsDemandesAchats(): Observable<AjournementDemandeAchats[]> {
    return this.httpClient
    .get<AjournementDemandeAchats[]>(URL_GET_LIST_AJOURNEMENT_DEMANDE_ACHATS);
   }
   getAjournementDemandeAchats(codeAjournementDemandeAchats): Observable<AjournementDemandeAchats> {
    return this.httpClient.get<AjournementDemandeAchats>(URL_GET_AJOURNEMENT_DEMANDE_ACHATS + '/' + codeAjournementDemandeAchats);
  }
  deleteAjournementDemandeAchats(codeAjournementDemandeAchats): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_AJOURNEMENT_DEMANDE_ACHATS + '/' + codeAjournementDemandeAchats);
  }


  /*addLigneFactureVentes(ligneFactureVente: LigneFactureVente): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_LIGNE_FACTURE_VENTES, ligneFactureVente);
    
  }
  updateLigneFactureVentes(ligneFactureVente: LigneFactureVente): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_LIGNE_FACTURE_VENTES, ligneFactureVente);
  }
  getLignesFacturesVentesByCodeFacture(codeLigneFactureVentes): Observable<LigneFactureVente[]> {
    return this.httpClient.get<LigneFactureVente[]>( URL_GET_LIST_LIGNES_FACTURE_VENTES_BY_CODE_FACTURE_VENTES +
       '/' + codeLigneFactureVentes);
  }
  getLignesFacturesVentes(): Observable<LigneFactureVente[]> {
    return this.httpClient
    .get<LigneFactureVente[]>(URL_GET_LIST_LIGNES_FACTURE_VENTES);
   }
   getLigneFactureVentes(codeLigneFactureVentes): Observable<LigneFactureVente> {
    return this.httpClient.get<LigneFactureVente>(URL_GET_LIST_LIGNES_FACTURE_VENTES + '/' + codeLigneFactureVentes);
  }
  deleteLigneFactureVentes(codeLigneFactureVentes): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_LIGNE_FACTURE_VENTES + '/' + codeLigneFactureVentes);
  }*/
}
