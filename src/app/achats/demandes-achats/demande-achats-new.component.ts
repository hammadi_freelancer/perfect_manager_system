import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {FactureAchats} from '../factures-achats/facture-achats.model';
import {FactureAchatsService} from '../factures-achats/facture-achats.service';
import {DemandeAchatsService} from './demande-achats.service';

import {AchatsService} from '../achats.service';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
// import {ActionData} from './action-data.interface';
import { FournisseurService } from '../fournisseurs/fournisseur.service';
import { FournisseurFilter } from '../fournisseurs/fournisseur.filter';
import { ArticleFilter } from './article.filter';
import { UnMesureFilter } from '../../stocks/un-mesures/un-mesures.filter';

import {Fournisseur} from '../fournisseurs/fournisseur.model';
import {Magasin} from '../../stocks/magasins/magasin.model';
import {UnMesure} from '../../stocks/un-mesures/un-mesure.model';

import {DemandeAchats} from '../demandes-achats/demande-achats.model';
import {LigneDemandeAchats} from '../demandes-achats/ligne-demande-achats.model';

import {LigneDemandeAchatsElement} from '../demandes-achats/ligne-demande-achats.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';

import {MatSnackBar} from '@angular/material';
import { MatExpansionPanel } from '@angular/material';
import {ArticleService} from '../../stocks/articles/article.service';
import {Article} from '../../stocks/articles/article.model';
import {ModePayementLibelle} from './mode-payement-libelle.interface';
import {SelectionModel} from '@angular/cdk/collections';

type LibelleModePayement = 'Credit'| 'Comptant' | 'Cheque' ;

@Component({
  selector: 'app-demande-achats-new',
  templateUrl: './demande-achats-new.component.html',
  // styleUrls: ['./players.component.css']
})
export class DemandeAchatsNewComponent implements OnInit {

 private demandeAchats: DemandeAchats =  new DemandeAchats();
 private  dataLignesDemandeAchatsSource: MatTableDataSource<LigneDemandeAchatsElement>;
 
//@Input()
// private listVentes: any = [] ;

private saveEnCours = false;
private managedLigneDemandeAchats = new LigneDemandeAchats();

private selection = new SelectionModel<LigneDemandeAchatsElement>(true, []);
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
 
/*@Output()
save: EventEmitter<FactureVente> = new EventEmitter<FactureVente>();*/

@ViewChild('firstMatExpansionPanel')
 private firstMatExpansionPanel : MatExpansionPanel;

 @ViewChild('manageLigneMatExpansionPanel')
 private manageLigneMatExpansionPanel : MatExpansionPanel;

 @ViewChild('listLignesMatExpansionPanel')
 private listLignesMatExpansionPanel : MatExpansionPanel;

private fournisseurFilter: FournisseurFilter;
private articleFilter: ArticleFilter;
private unMesureFilter : UnMesureFilter;
private listFournisseurs: Fournisseur[];


private payementModes: ModePayementLibelle[] = [
  {value: 'Comptant', viewValue: 'Comptant'},
  {value: 'Cheque', viewValue: 'Chèque'},
  {value: 'Credit', viewValue: 'Crédit'},
  {value: 'Credit_Comptant', viewValue: 'Crédit/Comptant'}
];
private etatsDemandeAchats: any[] = [
  {value: 'Initial', viewValue: 'Initial'}
 // {value: 'Valide', viewValue: 'Validé'}
];

  constructor( private route: ActivatedRoute,
    private router: Router, private demandeAchatsService: DemandeAchatsService, private fournisseurService: FournisseurService,
    private articleService: ArticleService, private achatsService: AchatsService,
   private matSnackBar: MatSnackBar, private elRef: ElementRef) {
  }


  // myControl = new FormControl();
  // options: string[] = ['One', 'Two', 'Three'];
  // filteredOptions: Observable<string[]>;

  ngOnInit() {

   this.listFournisseurs = this.route.snapshot.data.fournisseurs;
   // this.listArticles = this.route.snapshot.data.articles;
  // this.listCodesArticles  = this.listArticles.map((article) => article.code);
   this.fournisseurFilter = new FournisseurFilter(this.listFournisseurs);
   this.articleFilter = new ArticleFilter(this.route.snapshot.data.articles,
    [], [] );
     this.unMesureFilter = new UnMesureFilter(this.route.snapshot.data.unMesures);

   this.firstMatExpansionPanel.open();
   this.manageLigneMatExpansionPanel.opened.subscribe(openedPanel => {
   
     });
   this.specifyListColumnsToBeDisplayed();
   this.transformDataToByConsumedByMatTable();
  // this.initializeListLignesFactures();
  /*this.lignesFactures.forEach(function(ligne, index) {
                 ligne.code =  String(index + 1);
  });*/
  this.demandeAchats.etat = 'Initial';
}
 
  /*calculPrixTotal()
  {
    // this.managedLigneDemandeVentes.prixTotal = this.
    this.managedLigneDemandeAchats.prixTotal  = +  this.managedLigneDemandeAchats.prixUnt *
    this.managedLigneDemandeAchats.quantite  ;
    
  }*/



  constructClassName(i)  {
    return  'designationDivs_' + i;
   //  this.elRef.nativeElement.querySelector('.designationDivs_' + i )  ;
  }
 
  /*selectedArticleCode(i, $event) {
     console.log(i);
     console.log($event);
     this.listLignesFactureVentes[i].article.code = $event.option.value;
     this.listLignesFactureVentes[i].article.libelle  = this.listArticles.find((article) => (article.code === $event.option.value)).libelle;
    
     const  div  : HTMLDivElement = this.elRef.nativeElement.querySelector('.designationDivs_' + i )  ;
     div.innerText = this.listLignesFactureVentes[i].article.libelle;
     console.log(div);


  }*/

  saveDemandeAchats() {

    /*if (this.modeUpdate ) {
      this.updateCredit();
    } else {*/
      if (this.demandeAchats.etat === '' || this.demandeAchats.etat === undefined) {
        this.demandeAchats.etat = 'Initial';
      }

    this.saveEnCours = true;
    
    this.demandeAchatsService.addDemandeAchats(this.demandeAchats).subscribe((response) => {
      this.saveEnCours = false;
      
        if (response.error_message ===  undefined) {
          console.log('undefined error message');
          // this.save.emit(response);
          this.demandeAchats = new  DemandeAchats();
          this.demandeAchats.listLignesDemandeAchats = [];
          this.openSnackBar(  'Demande Achats Enregistrée');
        } else {
         // show error message
         this.openSnackBar(  'Erreurs:Opération Echouée');
         
        }
    });
  }
  fillDataNomPrenom()
  {
    const listFiltred  = this.listFournisseurs.filter((fournisseur) => (fournisseur.cin === this.demandeAchats.fournisseur.cin));
    this.demandeAchats.fournisseur.nom = listFiltred[0].nom;
    this.demandeAchats.fournisseur.prenom = listFiltred[0].prenom;
  }
  fillDataMatriculeRaison()
  {
    const listFiltred  = this.listFournisseurs.filter((fournisseur) => (fournisseur.registreCommerce
       === this.demandeAchats.fournisseur.registreCommerce));
    this.demandeAchats.fournisseur.matriculeFiscale = listFiltred[0].matriculeFiscale;
    this.demandeAchats.fournisseur.raisonSociale = listFiltred[0].raisonSociale;
  }
  vider() {
    this.demandeAchats =  new DemandeAchats();
  }
  openSnackBar(messageToDisplay) {
   this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
       verticalPosition: 'top', panelClass: 'snackBarClass'});
  }
loadGridDemandesAchats() {
  this.router.navigate(['/pms/achats/demandesAchats']) ;
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.demandeAchats.listLignesDemandeAchats!== undefined) {
    this.demandeAchats.listLignesDemandeAchats.forEach(ligneDem => {
           listElements.push( {code: ligneDem.code ,
            numLigneDemandeAchats: ligneDem.numLigneDemandeAchats,
            article: ligneDem.article.code,
             unMesure: ligneDem.unMesure.code,
             quantite: ligneDem.quantite,
             prixDesire: ligneDem.prixDesire,
             besoinUrgent: ligneDem.besoinUrgent,
             traite: ligneDem.traite,
             dateDisponibilite: ligneDem.dateDisponibilite,
      } );
    });
  }
    this.dataLignesDemandeAchatsSource= new MatTableDataSource<LigneDemandeAchatsElement>(listElements);
    this.dataLignesDemandeAchatsSource.sort = this.sort;
    this.dataLignesDemandeAchatsSource.paginator = this.paginator;
    
    
}
 specifyListColumnsToBeDisplayed() {
   // console.log('calling specifyListColumnsToBeDisplayed documents grid');
  this.columnsTitles = [];
  this.columnsNames = [];
  this.columnsTitlesAr  = [];
  
   this.columnsDefinitions = [{name: 'numLigneDemandeAchats' , displayTitle: 'N°'},
   {name: 'article' , displayTitle: 'Art.'},
   {name: 'unMesure' , displayTitle: 'Un.Mes.'},
   {name: 'quantite' , displayTitle: 'Qté'},
   {name: 'prixDesire' , displayTitle: 'P.Désiré'},
   {name: 'besoinUrgent' , displayTitle: 'Prioritaire'},
   {name: 'traite' , displayTitle: 'Traite'},
   {name: 'dateDisponibilite' , displayTitle: 'Date Disp.'},
    ];

   this.columnsTitles = this.columnsDefinitions.map((colDef) => {
             return colDef.displayTitle;
   });
   
   this.columnsNames[0] = 'select';
   this.columnsDefinitions.map((colDef) => {
    this.columnsNames.push(colDef.name);
    console.log(this.columnsNames);
});
   this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
    return colDef.displayTitleAr;
});
}

applyFilter(filterValue: string) {
  this.dataLignesDemandeAchatsSource.filter = filterValue.trim().toLowerCase();
  if (this.dataLignesDemandeAchatsSource.paginator) {
    this.dataLignesDemandeAchatsSource.paginator.firstPage();
  }
}

checkLigneDemandeAchats()
{

}
getIndexOfRow(row)
{

    for ( let i = 0; i < this.demandeAchats.listLignesDemandeAchats.length ; i++) {
      if (this.demandeAchats.listLignesDemandeAchats[i].numLigneDemandeAchats === row.numLigneDemandeAchats)
       {
             return i;
        }
    }
    return -1;
}
mapRowGridToObjectLigneDemandeAchats(rowGrid): LigneDemandeAchats
{
  const ligneDA = new  LigneDemandeAchats() ;
  ligneDA.numeroDemandeAchats = rowGrid.numeroDemandeAchats;
  ligneDA.numLigneDemandeAchats = rowGrid.numLigneDemandeAchats;
  ligneDA.article.code =  rowGrid.article;
  ligneDA.unMesure.code =  rowGrid.unMesure;
  ligneDA.quantite =  rowGrid.quantite;
  ligneDA.prixDesire =  rowGrid.prixDesire;
  ligneDA.besoinUrgent =  rowGrid.besoinUrgent;
  ligneDA.dateDisponibilite =  rowGrid.dateDisponibilite;
  ligneDA.traite =  rowGrid.traite;
  return ligneDA;
}
editLigneDemandeAchats(row)
{
  this.managedLigneDemandeAchats = this.mapRowGridToObjectLigneDemandeAchats(row);


    if(row.unMesure === undefined)
      {
        this.managedLigneDemandeAchats.unMesure = new UnMesure();
      }
      if(row.article === undefined)
        {
          this.managedLigneDemandeAchats.article = new Article();
        }
        console.log('row to edit is ', row);

  this.manageLigneMatExpansionPanel.open();
  
}
deleteLigneDemandeAchats(row)
{
 // console.log('index is ');
  //console.log(index);
  const indexOfRow = this.getIndexOfRow(row);
  this.demandeAchats.listLignesDemandeAchats.splice(indexOfRow , 1 );
  this.transformDataToByConsumedByMatTable();
  
}
addLigneDemandeAchats()
{
  if (this.managedLigneDemandeAchats.quantite === undefined
    || this.managedLigneDemandeAchats.quantite === 0 ||
    this.managedLigneDemandeAchats.quantite === null ||
      this.managedLigneDemandeAchats.article.code === undefined  ||
      this.managedLigneDemandeAchats.article.code === '' ||
      this.managedLigneDemandeAchats.article.code === null 
  ) {
        this.openSnackBar('Donnnées Manquées (QTE/ART!');
    } else if (this.managedLigneDemandeAchats.numLigneDemandeAchats === undefined
  || this.managedLigneDemandeAchats.numLigneDemandeAchats === '' ||
  this.managedLigneDemandeAchats.numLigneDemandeAchats=== null ) {
      this.openSnackBar('Numéro de Ligne Non Spécifié !');
    } else {
  this.managedLigneDemandeAchats.numeroDemandeAchats = this.demandeAchats.numeroDemandeAchats;
  if (this.demandeAchats.listLignesDemandeAchats.length === 0) {
  this.demandeAchats.listLignesDemandeAchats.push(this.managedLigneDemandeAchats);
  } else  {
    console.log('look for index:');
     const indexOfRow = this.getIndexOfRow(this.managedLigneDemandeAchats);
     console.log(indexOfRow);
     
     {
           if (indexOfRow === -1) {
    this.demandeAchats.listLignesDemandeAchats.push(this.managedLigneDemandeAchats);

         } else  {
     this.demandeAchats.listLignesDemandeAchats[indexOfRow] = this.managedLigneDemandeAchats;

       }
      }
  }

  this.transformDataToByConsumedByMatTable() ;
  this.managedLigneDemandeAchats = new LigneDemandeAchats();
  this.manageLigneMatExpansionPanel.close();
  this.listLignesMatExpansionPanel.open();
}
}

/*masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataLignesFactureVentesSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataLignesFactureVentesSource.data.length;
  return numSelected === numRows;
}*/
 /* deleteRow(i)
  {
    
    this.factureVente.montantHT = +this.factureVente.montantHT - this.lignesFactures[i].montantHT;
    this.factureVente.montantTVA = +this.factureVente.montantTVA - this.lignesFactures[i].montantTVA ;
    this.factureVente.montantTTC = +this.factureVente.montantTTC  - this.lignesFactures[i].montantTTC ;

    this.factureVente.montantRemise = +this.factureVente.montantRemise  - this.lignesFactures[i].montantRemise ;
      this.factureVente.montantAPayer = this.factureVente.montantTTC ;
      this.factureVente.montantAPayerApresRemise = +this.factureVente.montantAPayer - this.factureVente.montantRemise;
      console.log('after delete');
      console.log( this.factureVente.montantHT);
      console.log( this.factureVente.montantTTC);
      this.lignesFactures.splice(i, 1 );
      /*this.lignesFactures.forEach(function(ligne, index) {
                  ligne.code = index + 1;         
      },
      this);*/
      /*this.editLignesMode.splice(i, 1); 
      this.readOnlyLignes.splice(i, 1); 
      
      this.lignesFactures.push(new LigneFactureVente());
      this.editLignesMode.push(false);
      this.readOnlyLignes.push(false);
      
         
}*/

/*

  validateRow(i)
  {
     this.factureVente.montantHT = +this.factureVente.montantHT + this.lignesFactures[i].montantHT ;
     this.factureVente.montantTVA = +this.factureVente.montantTVA + this.lignesFactures[i].montantTVA ;
     this.factureVente.montantTTC = +this.factureVente.montantTTC  + this.lignesFactures[i].montantTTC ;
     this.factureVente.montantRemise = +this.factureVente.montantRemise  + this.lignesFactures[i].montantRemise ;
     this.factureVente.montantAPayer = this.factureVente.montantTTC ;
     this.factureVente.montantAPayerApresRemise = +this.factureVente.montantAPayer - this.factureVente.montantRemise;
     this.editLignesMode[i] = true;
     this.readOnlyLignes[i] = true;
    }
    editRow(i)
    {
      if (this.readOnlyLignes[i])
      {
        this.readOnlyLignes[i] = false;
      }  else {
       // this.validateRow(i);

        this.recalculMontants();
        this.readOnlyLignes[i] = true;
        
      }
       
    }*/

/*
nextPage()
  {
    this.pagesLignesFacures[this.currentPage - 1] = this.lignesFactures;

    this.pagesReadOnlyLignesFacures[this.currentPage - 1] = this.readOnlyLignes;
    this.pagesEditModeLignesFacures[this.currentPage - 1] = this.editLignesMode;
    
    this.currentPage++;
    if ( this.pagesLignesFacures[this.currentPage - 1] === undefined) {
    this.initializeListLignesFactures();

console.log('Next Page lignes Factures ');
console.log(this.lignesFactures);
this.pagesLignesFacures[this.currentPage - 1 ] = this.lignesFactures;
this.pagesReadOnlyLignesFacures[this.currentPage - 1] = this.readOnlyLignes;
this.pagesEditModeLignesFacures[this.currentPage - 1] = this.editLignesMode;


} else {
console.log('Next Page lignes Factures ');
this.lignesFactures = this.pagesLignesFacures[this.currentPage - 1 ];
this.readOnlyLignes = this.pagesReadOnlyLignesFacures[this.currentPage - 1 ];
this.editLignesMode = this.pagesEditModeLignesFacures[this.currentPage - 1 ];

}
this.factureVenteFilter = new FactureVenteFilter(this.listClients, this.listArticles);
const  button: HTMLButtonElement = this.elRef.nativeElement.querySelector('.paginationLabel' )  ;
button.innerText = 'Page ' + this.currentPage + ' / ' + this.pagesLignesFacures.length;
}

previousPage()
{
this.pagesLignesFacures[this.currentPage - 1] = this.lignesFactures ;
this.pagesReadOnlyLignesFacures[this.currentPage - 1] = this.readOnlyLignes;
this.pagesEditModeLignesFacures[this.currentPage - 1] = this.editLignesMode;

this.currentPage--;
this.lignesFactures =  this.pagesLignesFacures[this.currentPage - 1];
this.readOnlyLignes = this.pagesReadOnlyLignesFacures[this.currentPage - 1];
this.editLignesMode = this.pagesEditModeLignesFacures[this.currentPage - 1];

this.factureVenteFilter = new FactureVenteFilter(this.listClients, this.listArticles);
const  button: HTMLButtonElement = this.elRef.nativeElement.querySelector('.paginationLabel' )  ;
button.innerText = 'Page ' + this.currentPage + ' / ' + this.pagesLignesFacures.length;

}*/

  /*fillDataLignesFactures()
  {
    this.factureVente.listLignesFactures = [];
     this.listLignesFactureVentes.forEach(function(ligne, index) {
           if ( ligne.montantHT !== 0 ) {
             if (ligne.code === undefined || ligne.code === '')
              {
                ligne.code = this.factureVente.listLignesFactures.length + 1;
              }
            this.factureVente.listLignesFactures.push(ligne);
           }
    
     }, this);
  }*/
  /*checkOneActionInvoked() {
  const listSelectedLignesFactureVentes: LigneFactureVente[] = [];
console.log(this.selection.selected);
if (this.selection.selected.length !== 0 ) {
 this.managedLigneFactureVentes.numeroLigneFactureVentes =  this.selection.selected[this.selection.selected.length - 1]
 .numeroLigneFactureVentes;
 this.managedLigneFactureVentes.article.code =  this.selection.selected[this.selection.selected.length - 1].article;
 this.managedLigneFactureVentes.magasin.code =  this.selection.selected[this.selection.selected.length - 1].magasin;
 this.managedLigneFactureVentes.unMesure.code =  this.selection.selected[this.selection.selected.length - 1].unMesure;
 this.managedLigneFactureVentes.quantite =  this.selection.selected[this.selection.selected.length - 1].quantite;
 this.managedLigneFactureVentes.prixUnitaire=  this.selection.selected[this.selection.selected.length - 1].prixUnitaire;
 this.managedLigneFactureVentes.montantHT =  this.selection.selected[this.selection.selected.length - 1].montantHT;
 this.managedLigneFactureVentes.montantTTC =  this.selection.selected[this.selection.selected.length - 1].montantTTC;
 this.managedLigneFactureVentes.tva =  this.selection.selected[this.selection.selected.length - 1].tva; 
 this.managedLigneFactureVentes.montantTVA =  this.selection.selected[this.selection.selected.length - 1].montantTVA;
 this.managedLigneFactureVentes.fodec =  this.selection.selected[this.selection.selected.length - 1].fodec;
 this.managedLigneFactureVentes.montantFodec =  this.selection.selected[this.selection.selected.length - 1].montantFodec;
 this.managedLigneFactureVentes.remise =  this.selection.selected[this.selection.selected.length - 1].remise;
 this.managedLigneFactureVentes.montantRemise=  this.selection.selected[this.selection.selected.length - 1].montantRemise;

 this.managedLigneFactureVentes.otherTaxe1 =  this.selection.selected[this.selection.selected.length - 1].otherTaxe1;
 this.managedLigneFactureVentes.montantOtherTaxe1 =  this.selection.selected[this.selection.selected.length - 1].montantOtherTaxe1;
 
 this.managedLigneFactureVentes.otherTaxe2 =  this.selection.selected[this.selection.selected.length - 1].otherTaxe2;
 this.managedLigneFactureVentes.montantOtherTaxe2=  this.selection.selected[this.selection.selected.length - 1].montantOtherTaxe2;

 this.managedLigneFactureVentes.montantBenefice=  this.selection.selected[this.selection.selected.length - 1].montantBenefice;
 this.managedLigneFactureVentes.benefice=  this.selection.selected[this.selection.selected.length - 1].benefice;
 
 
const codes: string[] = this.selection.selected.map((ligneFactElement) => {
  return ligneFactElement.code;
});
this.factureVente.listLignesFactures.forEach(LigneF => {
  if (codes.findIndex(code => code === LigneF.code) > -1) {
    listSelectedLignesFactureVentes.push(LigneF);
  }
  });
}
}*/
  fillDataLibelleUnMesure()
  {

  }
  fillDataLibelleMagasins()
  {
    
  }
  fillDataLibelleArticles()
  {
    
  }
}
