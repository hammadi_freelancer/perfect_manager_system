export interface DemandeAchatsElement {
            code: string;
            numeroDemandeAchats: string;
            description: string;
         cin: string;
         nom: string;
          prenom: string;
         matriculeFiscale: string ;
         raisonSociale: string;
         budgetPrepare: number;
         besoinUrgentToutes : boolean;
         modePayementDesire: string;
          dateDemandeAchats: string;
         
         dateDisponibilite?: string;
         traiteToutes: boolean;
         etat: string;
        }
     