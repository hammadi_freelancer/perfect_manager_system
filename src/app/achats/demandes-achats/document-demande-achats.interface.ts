


export interface DocumentDemandeAchatsElement {
         code: string ;
         dateReception: string ;
         numeroDocument: number;
         typeDocument?: string;
          description?: string;
}
