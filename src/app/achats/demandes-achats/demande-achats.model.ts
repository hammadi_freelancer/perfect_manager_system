
import { BaseEntity } from '../../common/base-entity.model' ;
// import { Tranche } from './tranche.model' ;
import { Fournisseur } from '../fournisseurs/fournisseur.model' ;
import { PayementData } from './payement-data.model';
import { LigneDemandeAchats } from './ligne-demande-achats.model';
//import { Credit } from '../credits/credit.model';
type TypeModePayement = 'Credit'| 'Comptant' | 'Cheque' | '';

type  EtatDemandeAchats= |'Initial' |'Valide' | 'Annule' | '';


export class DemandeAchats extends BaseEntity {
    constructor(
        public code?: string,
        public numeroDemandeAchats?: string,
        public dateDemandeAchats?: string,
        public dateDemandeAchatsObject?: string,
        
        public fournisseur?: Fournisseur,
         public budgetPrepare?: number,
        public dateDisponibiliteObject?: Date,
        public dateDisponibilite?: string,
        public besoinUrgentToutes?: boolean,        
        public modePayementDesire?: TypeModePayement,
         public traiteToutes?: boolean,
          public etat?: EtatDemandeAchats,
     public listLignesDemandeAchats?: LigneDemandeAchats[]
     ) {
         super();
         this.fournisseur = new Fournisseur();
         this.budgetPrepare = Number(0);
       //  this.montantBeneficeEstime = Number(0);
        // this.payementData = new PayementData();
         this.listLignesDemandeAchats = [] ;
         // this.categoryClient = 'PERSONNE_PHYSIQUE';
    }
}
