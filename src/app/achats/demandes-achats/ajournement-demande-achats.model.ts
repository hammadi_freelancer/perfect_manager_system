
import { BaseEntity } from '../../common/base-entity.model' ;

type Etat = 'Initial' | 'Valide' | 'Annule' 
type ModePayement = 'Comptant' | 'Cheque' | 'Virement'

export class AjournementDemandeAchats extends BaseEntity {
    constructor(
        public code?: string,
         public codeDemandeAchats?: string,
        public dateOperationObject?: Date,
        public nouvelleDatePayementObject?: Date,
        public motif?: string,
        public nouvelleDatePayement?: string,
        public dateOperation?: string,
        
     ) {
         super();
        
       }
  
}
