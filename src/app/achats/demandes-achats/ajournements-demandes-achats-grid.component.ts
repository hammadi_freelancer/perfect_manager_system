
import { Component, OnInit, Inject, ViewChild, Input, OnChanges} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {DemandeAchatsService} from './demande-achats.service';
import { DemandeAchats} from './demande-achats.model';
import {AjournementDemandeAchats} from './ajournement-demande-achats.model';
import { AjournementDemandeAchatsElement } from './ajournement-demande-achats.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import { DialogAddAjournementDemandeAchatsComponent } from './popups/dialog-add-ajournement-demande-achats.component';
import { DialogEditAjournementDemandeAchatsComponent} from './popups/dialog-edit-ajournement-demande-achats.component';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-ajournements-demandes-achats-grid',
    templateUrl: 'ajournements-demandes-achats-grid.component.html',
  })
  export class AjournementsDemandesAchatsGridComponent implements OnInit, OnChanges {
     private listAjournementDemandeAchats: AjournementDemandeAchats[] = [];
     private  dataAjournementsDemandesAchatsSource: MatTableDataSource<AjournementDemandeAchatsElement>;
     private selection = new SelectionModel<AjournementDemandeAchatsElement>(true, []);

     @Input()
     private demandeAchats : DemandeAchats;

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private demandeAchatsService: DemandeAchatsService,
     private matSnackBar: MatSnackBar, public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.demandeAchatsService.getAjournementsDemandesAchatsByCodeDemande(this.demandeAchats.code).
      subscribe((listAjournementsRelAchats) => {
        this.listAjournementDemandeAchats = listAjournementsRelAchats;
// console.log('recieving data from backend', this.listAjournementsFactureVentes  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }

ngOnChanges()
{
  this.demandeAchatsService.getAjournementsDemandesAchatsByCodeDemande
  (this.demandeAchats.code).subscribe((listAjournementsRelAchats) => {
    this.listAjournementDemandeAchats = listAjournementsRelAchats;
//console.log('recieving data from backend', this.listAjournementCredits  );
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
     });
}
  checkOneActionInvoked() {
    const listSelectedAjournementsDemandeAchats: AjournementDemandeAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((ajournementRAchatsElement) => {
    return ajournementRAchatsElement.code;
  });
  this.listAjournementDemandeAchats.forEach(ajournementRAch => {
    if (codes.findIndex(code => code === ajournementRAch.code) > -1) {
      listSelectedAjournementsDemandeAchats.push(ajournementRAch);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listAjournementDemandeAchats !== undefined) {

      this.listAjournementDemandeAchats.forEach(ajournementRAchats => {
             listElements.push( {code: ajournementRAchats.code ,
          dateOperation: ajournementRAchats.dateOperation,
          nouvelleDatePayement: ajournementRAchats.nouvelleDatePayement,
          motif: ajournementRAchats.motif,
          description: ajournementRAchats.description
        } );
      });
    }
      this.dataAjournementsDemandesAchatsSource = new MatTableDataSource<AjournementDemandeAchatsElement>(listElements);
      this.dataAjournementsDemandesAchatsSource.sort = this.sort;
      this.dataAjournementsDemandesAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'},
     {name: 'nouvelleDatePayement' , displayTitle: 'N°elle Date Pay.'},
     {name: 'motif' , displayTitle: 'Motif'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataAjournementsDemandesAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataAjournementsDemandesAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataAjournementsDemandesAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataAjournementsDemandesAchatsSource.paginator) {
      this.dataAjournementsDemandesAchatsSource.paginator.firstPage();
    }
  }

  showAddAjournementDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeDemandeAchats: this.demandeAchats.code
   };
 
   const dialogRef = this.dialog.open(DialogAddAjournementDemandeAchatsComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(ajAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditAjournementDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeAjournementDemandeAchats : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditAjournementDemandeAchatsComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(ajAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Ajournement Séléctionnée!');
    }
    }
    supprimerAjournements()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(ajour, index) {
              this.demandeAchatsService.deleteAjournementDemandeAchats(ajour.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar(  ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Ajournement Séléctionnée!');
        }
    }
    refresh() {
      this.demandeAchatsService.getAjournementsDemandesAchatsByCodeDemande(this.demandeAchats.code).subscribe((listAjourRel) => {
        this.listAjournementDemandeAchats= listAjourRel;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }


}
