
import { Component, OnInit, Inject, ViewChild, Input, AfterViewChecked, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {DemandeAchatsService} from './demande-achats.service';
import { DemandeAchats } from './demande-achats.model';
import {PayementDemandeAchats} from './payement-demande-achats.model';
import { PayementDemandeAchatsElement } from './payement-demande-achats.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
 import { DialogAddPayementDemandeAchatsComponent} from './popups/dialog-add-payement-demande-achats.component';
 import { DialogEditPayementDemandeAchatsComponent } from './popups/dialog-edit-payement-demande-achats.component';
@Component({
    selector: 'app-payements-demandes-achats-grid',
    templateUrl: 'payements-demandes-achats-grid.component.html',
  })
  export class PayementsDemandesAchatsGridComponent implements OnInit, OnChanges {
     private listPayementsDemandesAchats: PayementDemandeAchats[] = [];
     private  dataPayementsDemandesAchatsSource: MatTableDataSource<PayementDemandeAchatsElement>;
     private selection = new SelectionModel<PayementDemandeAchatsElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];

     @Input()
     private demandeAchats: DemandeAchats;

     constructor(  private demandeAchatsService: DemandeAchatsService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      // console.log('credit code after dipslay tab ', this.credit.code);
      this.demandeAchatsService.getPayementsDemandesAchatsByCodeDemande(this.demandeAchats.code).subscribe((listPayementsRAchats) => {
        this.listPayementsDemandesAchats= listPayementsRAchats;
// console.log('recieving data from backend', this.listPayementsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }

  ngOnChanges() {
    this.demandeAchatsService.getPayementsDemandesAchatsByCodeDemande(this.demandeAchats.code).subscribe((listPayementsRAchats) => {
      this.listPayementsDemandesAchats = listPayementsRAchats;
// console.log('recieving data from backend', this.listPayementsCredits  );
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }
  checkOneActionInvoked() {
    const listSelectedPayementsRelAchats: PayementDemandeAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((payementRelElement) => {
    return payementRelElement.code;
  });
  this.listPayementsDemandesAchats.forEach(payementR => {
    if (codes.findIndex(code => code === payementR.code) > -1) {
      listSelectedPayementsRelAchats.push(payementR);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listPayementsDemandesAchats !== undefined) {

      this.listPayementsDemandesAchats.forEach(payementRelAchats => {
             listElements.push( {code: payementRelAchats.code ,
          dateOperation: payementRelAchats.dateOperation,
          montantPaye: payementRelAchats.montantPaye,
          modePayement: payementRelAchats.modePayement,
          numeroCheque: payementRelAchats.numeroCheque,
          dateCheque: payementRelAchats.dateCheque,
          compte: payementRelAchats.compte,
          compteAdversaire: payementRelAchats.compteAdversaire,
          banque: payementRelAchats.banque,
          banqueAdversaire: payementRelAchats.banqueAdversaire,
          description: payementRelAchats.description
        } );
      });
    }
      this.dataPayementsDemandesAchatsSource = new MatTableDataSource<PayementDemandeAchatsElement>(listElements);
      this.dataPayementsDemandesAchatsSource.sort = this.sort;
      this.dataPayementsDemandesAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'}, 
     {name: 'montantPaye' , displayTitle: 'Montant Payé'},
     {name: 'modePayement' , displayTitle: 'Mode Payement'},
      {name: 'numeroCheque' , displayTitle: 'N°Chèque'},
     {name: 'dateCheque' , displayTitle: 'Date Chèque'}, 
     {name: 'compte' , displayTitle: 'Compte'},
     {name: 'compteAdversaire' , displayTitle: 'Compte Adv.'},
      {name: 'banque' , displayTitle: 'Banque'},
       { name : 'banqueAdversaire' , displayTitle : 'Banque Adv.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataPayementsDemandesAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataPayementsDemandesAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataPayementsDemandesAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataPayementsDemandesAchatsSource.paginator) {
      this.dataPayementsDemandesAchatsSource.paginator.firstPage();
    }
  }

  showAddPayementDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeDemandeAchats : this.demandeAchats.code
   };
 
   const dialogRef = this.dialog.open(DialogAddPayementDemandeAchatsComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(payAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditPayementDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codePayementDemandeAchats: this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditPayementDemandeAchatsComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(payAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Paiement Séléctionnée!');
    }
    }
    supprimerPayements()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(pay, index) {
              this.demandeAchatsService.deletePayementDemandeAchats(pay.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Paiement Séléctionnée!');
        }
    }
    refresh() {
      this.demandeAchatsService.getPayementsDemandesAchatsByCodeDemande(this.demandeAchats.code).subscribe((listPayementsRels) => {
        this.listPayementsDemandesAchats = listPayementsRels;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
















}
