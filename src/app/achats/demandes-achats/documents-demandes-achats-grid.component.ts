
import { Component, OnInit, Inject,Input, ViewChild, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {DemandeAchatsService} from './demande-achats.service';
import {DemandeAchats} from './demande-achats.model';

import {DocumentDemandeAchats} from './document-demande-achats.model';
import { DocumentDemandeAchatsElement } from './document-demande-achats.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogAddDocumentDemandeAchatsComponent } from './popups/dialog-add-document-demande-achats.component';
 import { DialogEditDocumentDemandeAchatsComponent } from './popups/dialog-edit-document-demande-achats.component';
@Component({
    selector: 'app-documents-demandes-achats-grid',
    templateUrl: 'documents-demandes-achats-grid.component.html',
  })
  export class DocumentsDemandeAchatsGridComponent implements OnInit, OnChanges {
     private listDocumentsDemandesAchats: DocumentDemandeAchats[] = [];
     private  dataDocumentsDemandesAchatsSource: MatTableDataSource<DocumentDemandeAchatsElement>;
     private selection = new SelectionModel<DocumentDemandeAchatsElement>(true, []);
    
     @Input()
     private demandeAchats : DemandeAchats;

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private demandeAchatsService: DemandeAchatsService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.demandeAchatsService.getDocumentsDemandeAchatsByCodeDemande(this.demandeAchats.code).subscribe((listDocumentsRelAchats) => {
        this.listDocumentsDemandesAchats= listDocumentsRelAchats;
 //  console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsFacturesVentes  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
  }
  ngOnChanges()
  {
    this.specifyListColumnsToBeDisplayed();
    
    this.demandeAchatsService.getDocumentsDemandeAchatsByCodeDemande(this.demandeAchats.code).subscribe((listDocumentsRelAchats) => {
      this.listDocumentsDemandesAchats = listDocumentsRelAchats;
// console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsCredits  );
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }

  checkOneActionInvoked() {
    const listSelectedDocumentsRelAchats: DocumentDemandeAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentRElement) => {
    return documentRElement.code;
  });
  this.listDocumentsDemandesAchats.forEach(documentR => {
    if (codes.findIndex(code => code === documentR.code) > -1) {
      listSelectedDocumentsRelAchats.push(documentR);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsDemandesAchats!== undefined) {

      this.listDocumentsDemandesAchats.forEach(docRel=> {
             listElements.push( {code: docRel.code ,
          dateReception: docRel.dateReception,
          numeroDocument: docRel.numeroDocument,
          typeDocument: docRel.typeDocument,
          description: docRel.description
        } );
      });
    }
      this.dataDocumentsDemandesAchatsSource = new MatTableDataSource<DocumentDemandeAchatsElement>(listElements);
      this.dataDocumentsDemandesAchatsSource.sort = this.sort;
      this.dataDocumentsDemandesAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     console.log('calling specifyListColumnsToBeDisplayed documents grid');
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
    
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
      console.log(this.columnsNames);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsDemandesAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsDemandesAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsDemandesAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsDemandesAchatsSource.paginator) {
      this.dataDocumentsDemandesAchatsSource.paginator.firstPage();
    }
  }
  showAddDocumentDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeDemandeAchats : this.demandeAchats.code
   };
 
   const dialogRef = this.dialog.open(DialogAddDocumentDemandeAchatsComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(docAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditDocumentDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeDocumentDemandeAchats : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditDocumentDemandeAchatsComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(docAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Document Séléctionnée!');
    }
    }
    supprimerDocuments()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(doc, index) {
              this.demandeAchatsService.deleteDocumentDemandeAchats(doc.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Document Séléctionnée!');
        }
    }
    refresh() {
      this.demandeAchatsService.getDocumentsDemandeAchatsByCodeDemande(this.demandeAchats.code).subscribe((listDocsRelAchats) => {
        this.listDocumentsDemandesAchats = listDocsRelAchats;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
