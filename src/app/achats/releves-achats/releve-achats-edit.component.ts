import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
// import { CommunicationService} from './communication.service';
import { MatDatepickerInput } from '@angular/material/datepicker';
import { MatSnackBar } from '@angular/material';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {ArticleService} from '../../stocks/articles/article.service';
import {Article} from '../../stocks/articles/article.model';
import { MatExpansionPanel } from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {LigneFactureAchatsElement} from '../factures-achats/ligne-facture-achats.interface';
import {SelectionModel} from '@angular/cdk/collections';
import {Magasin} from '../../stocks/magasins/magasin.model';
import {ModePayementLibelle} from './mode-payement-libelle.interface';
import {FactureAchats} from '../factures-achats/facture-achats.model';

import {FactureAchatsService} from '../factures-achats/facture-achats.service';
import {ReleveAchatsService} from './releve-achats.service';

import {AchatsService} from '../achats.service';
import { FournisseurService } from '../fournisseurs/fournisseur.service';
import { FournisseurFilter } from '../fournisseurs/fournisseur.filter';
import { ArticleFilter } from './article.filter';
import { UnMesureFilter } from '../../stocks/un-mesures/un-mesures.filter';

import {Fournisseur} from '../fournisseurs/fournisseur.model';
import {UnMesure} from '../../stocks/un-mesures/un-mesure.model';

import {ReleveAchats} from '../releves-achats/releve-achats.model';
import {LigneReleveAchats} from '../releves-achats/ligne-releve-achats.model';

import {LigneReleveAchatsElement} from '../releves-achats/ligne-releve-achats.interface';

@Component({
  selector: 'app-releve-achats-edit',
  templateUrl: './releve-achats-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class ReleveAchatsEditComponent implements OnInit  {

  private releveAchats: ReleveAchats =  new ReleveAchats();
  private updateEnCours = false;
  private tabsDisabled = true;
  private managedLigneReleveAchats = new LigneReleveAchats();
  private selection = new SelectionModel<LigneReleveAchatsElement>(true, []);
  @ViewChild(MatPaginator) paginator: MatPaginator; 
  @ViewChild(MatSort) sort: MatSort;
  private columnsDefinitions: ColumnDefinition[] = [];
  private columnsTitles: string[] = [];
  private columnsTitlesAr: string[] = [];
  private columnsNames: string[] = [];
  
@Input()
private listRelevesAchats: any = [] ;

@ViewChild('firstMatExpansionPanel')
private firstMatExpansionPanel : MatExpansionPanel;

@ViewChild('manageLigneMatExpansionPanel')
private manageLigneMatExpansionPanel : MatExpansionPanel;

@ViewChild('listLignesMatExpansionPanel')
private listLignesMatExpansionPanel : MatExpansionPanel;

// private listCodesArticles: string[];
// private lignesFactures: LigneFactureVente[] = [ ];
// private listTranches: Tranche[] = [] ;
// private factureVenteFilter: FactureVenteFilter;
private articleFilter: ArticleFilter;
private listFournisseurs: Fournisseur[];
private fournisseurFilter: FournisseurFilter;
private unMesureFilter : UnMesureFilter;
// private listArticles: Article[];
// private listLignesFactureVentes: LigneFactureVente[] = [ ];
private  dataLignesReleveAchatsSource: MatTableDataSource<LigneReleveAchatsElement>;

// private editLignesMode: boolean [] = [];

private etatsReleveAchats: any[] = [
  {value: 'Annule', viewValue: 'Annulé'},
  {value: 'Initial', viewValue: 'Initial'},
  {value: 'Valide', viewValue: 'Validé'}
];

private payementModes: ModePayementLibelle[] = [
  {value: 'Comptant', viewValue: 'Comptant'},
  {value: 'Cheque', viewValue: 'Chèque'},
  {value: 'Credit', viewValue: 'Crédit'},
  {value: 'Credit_Comptant', viewValue: 'Crédit/Comptant'}
];
  constructor( private route: ActivatedRoute,
    private router: Router, private releveAchatsService: ReleveAchatsService,
    private articleService: ArticleService, private dialog: MatDialog,
    private matSnackBar: MatSnackBar, private elRef: ElementRef,
   ) {
  }
  ngOnInit() {
    this.listFournisseurs = this.route.snapshot.data.fournisseurs;
    // this.listArticles = this.route.snapshot.data.articles;
   // this.listCodesArticles  = this.listArticles.map((article) => article.code);
    this.fournisseurFilter = new FournisseurFilter(this.listFournisseurs);
    this.articleFilter = new ArticleFilter(this.route.snapshot.data.articles,
    [], [] );
     this.unMesureFilter = new UnMesureFilter(this.route.snapshot.data.unMesures);
 
    this.articleFilter = new ArticleFilter(this.route.snapshot.data.articles,
      [], []);
  
    const codeReleveAchats = this.route.snapshot.paramMap.get('codeReleveAchats');


    this.releveAchatsService.getReleveAchats(codeReleveAchats).subscribe((releveAchats) => {
             this.releveAchats = releveAchats;
             if (this.releveAchats.etat === 'Valide')
              {
                this.tabsDisabled = false;
              }
             if (this.releveAchats.etat === 'Valide' || this.releveAchats.etat === 'Annule')
              {
                this.etatsReleveAchats.splice(1, 1);
              }
              if (this.releveAchats.etat === 'Initial')
                {
                  this.etatsReleveAchats.splice(0, 1);
                }
            });
            this.firstMatExpansionPanel.open();

            this.specifyListColumnsToBeDisplayed();
            this.transformDataToByConsumedByMatTable();
        
           this.releveAchats.etat = 'Initial';
  




            
  }
  loadGridRelevesAchats() {
    this.router.navigate(['/pms/achats/relevesAchats']) ;
  }
updateReleveAchats() {

  //console.log(this.factureVente);
  let noClient = false;
  let noRow = false;
  if (this.releveAchats.etat === 'Valide' )  {
    if ( this.releveAchats.fournisseur.cin === null ||  this.releveAchats.fournisseur.cin === undefined)  {
        noClient = true;
        
      }
  if (this.releveAchats.listLignesRelAchats[0] === undefined
    || this.releveAchats.listLignesRelAchats[0].quantite === 0  ) {
      noRow = true;
    }
  }
        if (noRow)  {
          this.openSnackBar( 'La Relevée doit avoir au moins une ligne valide !');
          
        } else {
  this.updateEnCours = true;
  this.releveAchatsService.updateReleveAchats(this.releveAchats).subscribe((response) => {
    this.updateEnCours = false;
    if (response.error_message ===  undefined) {
      console.log('the response of update is ', response );
      if (this.releveAchats.etat === 'Valide')
        {
          this.tabsDisabled = false;
          this.etatsReleveAchats= [
            {value: 'Annule', viewValue: 'Annulé'},
            {value: 'Valide', viewValue: 'Validé'},
            {value: 'Reglee', viewValue: 'Réglée'},
            {value: 'Valide', viewValue: 'Partiellement Réglée'},
            
  
          ];
        }
        if (this.releveAchats.etat === 'Annule')
          {
            this.tabsDisabled = true;
         
          }

      this.openSnackBar( ' Relevée Achats mise à jour ');
      //  this.save.emit(response);
      // this.client = new Client();
      // this.modeUpdate = false;
    } else {
      this.openSnackBar( ' Erreurs:Opération Echouée ');
     // show error message
    }
});
        }
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 
 fillDataNomPrenom()
 {
   const listFiltred  = this.listFournisseurs.filter((fournisseur) => (fournisseur.cin === this.releveAchats.fournisseur.cin));
   this.releveAchats.fournisseur.nom = listFiltred[0].nom;
   this.releveAchats.fournisseur.prenom = listFiltred[0].prenom;
 }
 fillDataMatriculeRaison()
 {
   const listFiltred  = this.listFournisseurs.filter((fournisseur) =>
    (fournisseur.registreCommerce === this.releveAchats.fournisseur.registreCommerce));
   this.releveAchats.fournisseur.matriculeFiscale = listFiltred[0].matriculeFiscale;
   this.releveAchats.fournisseur.raisonSociale = listFiltred[0].raisonSociale;
 }


 transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.releveAchats.listLignesRelAchats !== undefined) {
    this.releveAchats.listLignesRelAchats.forEach(ligneRel => {
           listElements.push( {code: ligneRel.code ,
            numeroLigneRelAchats: ligneRel.numeroLigneRelAchats,
            article: ligneRel.article.code,
             unMesure: ligneRel.unMesure.code,
             quantite: ligneRel.quantite,
             prixUnt: ligneRel.prixUnt,
             prixTotal: ligneRel.prixTotal,
             beneficeTotalEstime: ligneRel.beneficeTotalEstime,
             regle: ligneRel.regle,
             livre: ligneRel.livre
      } );
    });
  }
    this.dataLignesReleveAchatsSource= new MatTableDataSource<LigneReleveAchatsElement>(listElements);
    this.dataLignesReleveAchatsSource.sort = this.sort;
    this.dataLignesReleveAchatsSource.paginator = this.paginator;
    
    
}
 specifyListColumnsToBeDisplayed() {
   // console.log('calling specifyListColumnsToBeDisplayed documents grid');
  this.columnsTitles = [];
  this.columnsNames = [];
  this.columnsTitlesAr  = [];
  
   this.columnsDefinitions = [{name: 'numeroLigneRelAchats' , displayTitle: 'N°'},
   {name: 'article' , displayTitle: 'Art.'},
   {name: 'unMesure' , displayTitle: 'Un.Mes.'},
   {name: 'quantite' , displayTitle: 'Qté'},
   {name: 'prixUnt' , displayTitle: 'P.Unt(TTC)'},
   {name: 'prixTotal' , displayTitle: 'P.Tot(TTC)'},
   {name: 'beneficeTotalEstime' , displayTitle: 'Bénef.Tot Est.'},
   {name: 'regle' , displayTitle: 'Réglé'},
   {name: 'livre' , displayTitle: 'Livré'},
    ];

   this.columnsTitles = this.columnsDefinitions.map((colDef) => {
             return colDef.displayTitle;
   });
   
   this.columnsNames[0] = 'select';
   this.columnsDefinitions.map((colDef) => {
    this.columnsNames.push(colDef.name);
    console.log(this.columnsNames);
});
   this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
    return colDef.displayTitleAr;
});
}

applyFilter(filterValue: string) {
  this.dataLignesReleveAchatsSource.filter = filterValue.trim().toLowerCase();
  if (this.dataLignesReleveAchatsSource.paginator) {
    this.dataLignesReleveAchatsSource.paginator.firstPage();
  }
}

checkLigneReleveAchats()
{

}
getIndexOfRow(row)
{

    for ( let i = 0; i < this.releveAchats.listLignesRelAchats.length ; i++) {
      if (this.releveAchats.listLignesRelAchats[i].numeroLigneRelAchats === row.numeroLigneRelAchats)
       {
             return i;
        }
    }
    return -1;
}
mapRowGridToObjectLigneReleveAchats(rowGrid): LigneReleveAchats
{
  const ligneRV  = new  LigneReleveAchats() ;
  
  ligneRV.numeroReleveAchats = rowGrid.numeroReleveAchats;
  ligneRV.numeroLigneRelAchats = rowGrid.numeroLigneRelAchats;
  ligneRV.article.code =  rowGrid.article;
  ligneRV.unMesure.code =  rowGrid.unMesure;
  ligneRV.quantite =  rowGrid.quantite;
  ligneRV.prixUnt =  rowGrid.prixUnt;
  ligneRV.prixTotal =  rowGrid.prixTotal;
  ligneRV.beneficeTotalEstime =  rowGrid.beneficeTotalEstime;
  ligneRV.livre =  rowGrid.livre;
  ligneRV.regle =  rowGrid.regle;
  return ligneRV;
}
editLigneReleveAchats(row)
{
  this.managedLigneReleveAchats = this.mapRowGridToObjectLigneReleveAchats(row);


    if(row.unMesure === undefined)
      {
        this.managedLigneReleveAchats.unMesure = new UnMesure();
      }
      if(row.article === undefined)
        {
          this.managedLigneReleveAchats.article = new Article();
        }
        console.log('row to edit is ', row);

  this.manageLigneMatExpansionPanel.open();
  
}
deleteLigneReleveAchats(row)
{

  const indexOfRow = this.getIndexOfRow(row);
  this.releveAchats.listLignesRelAchats.splice(indexOfRow , 1 );
  this.transformDataToByConsumedByMatTable();
  
}
addLigneReleveAchats()
{
  if (this.managedLigneReleveAchats.quantite === undefined
    || this.managedLigneReleveAchats.quantite === 0 ||
    this.managedLigneReleveAchats.quantite === null ||
    this.managedLigneReleveAchats.prixUnt === undefined
      || this.managedLigneReleveAchats.prixUnt === 0 ||
      this.managedLigneReleveAchats.prixUnt === null  ||
      this.managedLigneReleveAchats.article.code === undefined  ||
      this.managedLigneReleveAchats.article.code === '' ||
      this.managedLigneReleveAchats.article.code === null 
  ) {
        this.openSnackBar('Donnnées Manquées (QTE/ART/PUNIT!');
    } else if (this.managedLigneReleveAchats.numeroLigneRelAchats === undefined
  || this.managedLigneReleveAchats.numeroLigneRelAchats === '' ||
  this.managedLigneReleveAchats.numeroLigneRelAchats=== null ) {
      this.openSnackBar('Numéro de Ligne Non Spécifié !');
    } else {
  this.managedLigneReleveAchats.numeroReleveAchats= this.releveAchats.numeroReleveAchats;
  if (this.releveAchats.listLignesRelAchats.length === 0) {
  this.releveAchats.listLignesRelAchats.push(this.managedLigneReleveAchats);
  } else  {
    console.log('look for index:');
     const indexOfRow = this.getIndexOfRow(this.managedLigneReleveAchats);
     console.log(indexOfRow);
     
     {
           if (indexOfRow === -1) {
    this.releveAchats.listLignesRelAchats.push(this.managedLigneReleveAchats);

         } else  {
     this.releveAchats.listLignesRelAchats[indexOfRow] = this.managedLigneReleveAchats;

       }
      }
  }

  this.transformDataToByConsumedByMatTable() ;
  this.managedLigneReleveAchats = new LigneReleveAchats();
  this.manageLigneMatExpansionPanel.close();
  this.listLignesMatExpansionPanel.open();
}
}
calculPrixTotal()
{
  // this.managedLigneReleveVentes.prixTotal = this.
  this.managedLigneReleveAchats.prixTotal  = +  this.managedLigneReleveAchats.prixUnt *
  this.managedLigneReleveAchats.quantite  ;
  
}
selectedTabChanged($event) {
  if ($event.index === 4) { // Composition tab was selected
   // this.loadListArticles();
   } else if ($event.index === 1) {
    //  this.loadListSchemasConfiguration() ;
   }
  }
}
