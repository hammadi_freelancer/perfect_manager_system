
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ReleveAchatsService} from '../releve-achats.service';
import {AjournementReleveAchats} from '../ajournement-releve-achats.model';

@Component({
    selector: 'app-dialog-add-ajournement-releve-achats',
    templateUrl: 'dialog-add-ajournement-releve-achats.component.html',
  })
  export class DialogAddAjournementReleveAchatsComponent implements OnInit {
     private ajournementReleveAchats: AjournementReleveAchats;
     private saveEnCours = false;
     constructor(  private releveAchatsService: ReleveAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.ajournementReleveAchats = new AjournementReleveAchats;
    }
    saveAjournementReleveAchats() 
    {
      this.ajournementReleveAchats.codeReleveAchats = this.data.codeReleveAchats;
      this.saveEnCours = true;
      // console.log(this.ajournementCredit);
      //  this.saveEnCours = true;
       this.releveAchatsService.addAjournementReleveAchats(this.ajournementReleveAchats).subscribe((response) => {
        this.saveEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.ajournementReleveAchats = new AjournementReleveAchats();
             this.openSnackBar(  'Ajournement Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
