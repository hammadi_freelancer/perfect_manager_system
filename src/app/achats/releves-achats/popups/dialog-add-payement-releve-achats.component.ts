
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ReleveAchatsService} from '../releve-achats.service';
import {PayementReleveAchats} from '../payement-releve-achats.model';

@Component({
    selector: 'app-dialog-add-payement-releve-achats',
    templateUrl: 'dialog-add-payement-releve-achats.component.html',
  })
  export class DialogAddPayementReleveAchatsComponent implements OnInit {
     private payementReleveAchats: PayementReleveAchats;
     
     constructor(  private  releveAchatsService: ReleveAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.payementReleveAchats = new PayementReleveAchats();
    }
    savePayementReleveAchats() 
    {
      this.payementReleveAchats.codeReleveAchats= this.data.codeReleveAchats;
      // console.log(this.payementFactureVentes);
      //  this.saveEnCours = true;
       this.releveAchatsService.addPayementReleveAchats(this.payementReleveAchats).subscribe((response) => {
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.payementReleveAchats = new PayementReleveAchats();
             this.openSnackBar(  'Payement Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
