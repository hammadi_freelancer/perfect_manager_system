
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ReleveAchatsService} from '../releve-achats.service';
import {PayementReleveAchats} from '../payement-releve-achats.model';
import { PayementReleveAchatsElement } from '../payement-releve-achats.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-payements-releves-achats',
    templateUrl: 'dialog-list-payements-releves-achats.component.html',
  })
  export class DialogListPayementsRelevesAchatsComponent implements OnInit {
     private listPayementsReleveAchats: PayementReleveAchats[] = [];
     private  dataPayementsReleveAchatsSource: MatTableDataSource<PayementReleveAchatsElement>;
     private selection = new SelectionModel<PayementReleveAchatsElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private releveAchatsService: ReleveAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      // console.log('code credit in dialog component is :', this.data.codeFactureVentes);
      this.releveAchatsService.getPayementsRelevesAchatsByCodeReleve(this.data.codeReleveAchats)
      .subscribe((listPayementsRelAchats) => {
        this.listPayementsReleveAchats= listPayementsRelAchats;
// console.log('recieving data from backend', this.listPayementsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedPayementsReleveAchats: PayementReleveAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((payementRelAchatsElement) => {
    return payementRelAchatsElement.code;
  });
  this.listPayementsReleveAchats.forEach(payementRAchats => {
    if (codes.findIndex(code => code === payementRAchats.code) > -1) {
      listSelectedPayementsReleveAchats.push(payementRAchats);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listPayementsReleveAchats !== undefined) {

      this.listPayementsReleveAchats.forEach(payementRelAchats => {
             listElements.push( {code: payementRelAchats.code ,
          dateOperation: payementRelAchats.dateOperation,
          montantPaye: payementRelAchats.montantPaye,
          modePayement: payementRelAchats.modePayement,
          numeroCheque: payementRelAchats.numeroCheque,
          dateCheque: payementRelAchats.dateCheque,
          compte: payementRelAchats.compte,
          compteAdversaire: payementRelAchats.compteAdversaire,
          banque: payementRelAchats.banque,
          banqueAdversaire: payementRelAchats.banqueAdversaire,
          description: payementRelAchats.description
        } );
      });
    }
      this.dataPayementsReleveAchatsSource = new MatTableDataSource<PayementReleveAchatsElement>(listElements);
      this.dataPayementsReleveAchatsSource.sort = this.sort;
      this.dataPayementsReleveAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'}, 
     {name: 'montantPaye' , displayTitle: 'Montant Payé'},
     {name: 'modePayement' , displayTitle: 'Mode Payement'},
      {name: 'numeroCheque' , displayTitle: 'N°Chèque'},
     {name: 'dateCheque' , displayTitle: 'Date Chèque'}, 
     {name: 'compte' , displayTitle: 'Compte'},
     {name: 'compteAdversaire' , displayTitle: 'Compte Adv.'},
      {name: 'banque' , displayTitle: 'Banque'},
       { name : 'banqueAdversaire' , displayTitle : 'Banque Adv.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataPayementsReleveAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataPayementsReleveAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataPayementsReleveAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataPayementsReleveAchatsSource.paginator) {
      this.dataPayementsReleveAchatsSource.paginator.firstPage();
    }
  }

}
