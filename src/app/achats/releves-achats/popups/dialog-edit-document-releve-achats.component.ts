
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ReleveAchatsService} from '../releve-achats.service';
import {DocumentReleveAchats} from '../document-releve-achats.model';

@Component({
    selector: 'app-dialog-edit-document-releve-achats',
    templateUrl: 'dialog-edit-document-releve-achats.component.html',
  })
  export class DialogEditDocumentReleveAchatsComponent implements OnInit {
     private documentReleveAchats: DocumentReleveAchats;
     private updateEnCours = false;
     private typesDoc: any[] = [
      {value: 'Cheque', viewValue: 'Chèque'},
      {value: 'Traite', viewValue: 'Traite'},
      {value: 'FactureAchats', viewValue: 'Facture Achats'},
      {value: 'BonReception', viewValue: 'Bon Réception'},
      {value: 'ReçuPayement', viewValue: 'Reçu Payement'},
      {value: 'Autre', viewValue: 'Autre'},

    ];
     constructor(  private releveAchatsService: ReleveAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {

        this.releveAchatsService.getDocumentReleveAchats(this.data.codeDocumentReleveAchats).subscribe((doc) => {
            this.documentReleveAchats = doc;
     });
    }
    
    updateDocumentReleveAchats() 
    {
      this.updateEnCours = true;
       this.releveAchatsService.updateDocumentReleveAchats(this.documentReleveAchats).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.openSnackBar(  'Document Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
     imageFace1Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentReleveAchats.imageFace1 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imageFace2Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentReleveAchats.imageFace2 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
  }
