
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ReleveAchatsService} from '../releve-achats.service';
import {LigneReleveAchats} from '../ligne-releve-achats.model';
import { LigneReleveAchatsElement } from '../ligne-releve-achats.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-lignes-releve-achats',
    templateUrl: 'dialog-list-lignes-releves-achats.component.html',
  })
  export class DialogListLignesRelevesAchatsComponent implements OnInit {
     private listLignesRelevesAchats: LigneReleveAchats[] = [];
     private  dataLignesReleveAchatsSource: MatTableDataSource<LigneReleveAchatsElement>;
     private selection = new SelectionModel<LigneReleveAchatsElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private releveAchatsService: ReleveAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      // console.log('code credit in dialog component is :', this.data.codeFactureVentes);
    /*  this.releveVentesService.get(this.data.codeFactureVentes)
      .subscribe((listLignesFactVentes) => {
        this.listLignesFactureVentes = listLignesFactVentes;
// console.log('recieving data from backend', this.listPayementsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });*/

  }


  checkOneActionInvoked() {
    const listSelectedLignesReleveAchats: LigneReleveAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((ligneReleveAchatsElement) => {
    return ligneReleveAchatsElement.code;
  });
  this.listLignesRelevesAchats.forEach(ligneRAchats => {
    if (codes.findIndex(code => code === ligneRAchats.code) > -1) {
      listSelectedLignesReleveAchats.push(ligneRAchats);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listLignesRelevesAchats !== undefined) {
 
      this.listLignesRelevesAchats.forEach(ligneRelAchats => {
             listElements.push( 
               {code: ligneRelAchats.code ,
                numeroLigneRelAchats: ligneRelAchats.numeroLigneRelAchats,
          article: ligneRelAchats.article.code,
          quantite: ligneRelAchats.quantite,
          prixUnitaire: ligneRelAchats.prixUnt,
          prixTotal: ligneRelAchats.prixTotal,
          unMesure: ligneRelAchats.unMesure.code,
          beneficeTotalEstime: ligneRelAchats.beneficeTotalEstime,
          livre: ligneRelAchats.livre,
          regle: ligneRelAchats.regle,
          
          
        } );
      });
    }
      this.dataLignesReleveAchatsSource= new MatTableDataSource<LigneReleveAchatsElement>(listElements);
      this.dataLignesReleveAchatsSource.sort = this.sort;
      this.dataLignesReleveAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsDefinitions = [{name: 'numeroLigneRelAchats' , displayTitle: 'N°'},
    {name: 'article' , displayTitle: 'Article'},
    {name: 'unMesure' , displayTitle: 'Un Mesure'},
    {name: 'quantite' , displayTitle: 'Quantite'},
    {name: 'prixUnt' , displayTitle: 'P.Unt(TTC)'},
    {name: 'prixTotal' , displayTitle: 'P.TOT(TTC)'},
    {name: 'beneficeTotalEstime' , displayTitle: 'Benefice TOT Est.'},
    {name: 'regle' , displayTitle: 'Réglé'},
    {name: 'livre' , displayTitle: 'Livré'},
     ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataLignesReleveAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataLignesReleveAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataLignesReleveAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataLignesReleveAchatsSource.paginator) {
      this.dataLignesReleveAchatsSource.paginator.firstPage();
    }
  }

}
