
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ReleveAchatsService} from '../releve-achats.service';
import {PayementReleveAchats} from '../payement-releve-achats.model';
import {LigneReleveAchats} from '../ligne-releve-achats.model';

@Component({
    selector: 'app-dialog-add-ligne-releve-achats',
    templateUrl: 'dialog-add-ligne-releve-achats.component.html',
  })
  export class DialogAddLigneReleveAchatsComponent implements OnInit {
     private ligneReleveAchats: LigneReleveAchats;
     private listCodesArticles: string[];
     private listCodesMagasins: string[];
     private listCodesUnMesures: string[];
     
     private etatsLignes: any[] = [
      {value: 'Annule', viewValue: 'Annulé'},
      {value: 'Initial', viewValue: 'Initial'},
      {value: 'Valide', viewValue: 'Validé'}
    ];
     constructor(  private  releveAchatsService: ReleveAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.ligneReleveAchats = new LigneReleveAchats();
           this.listCodesArticles = this.data.listCodesArticles;
           // this.listCodesMagasins = this.data.listCodesMagasins;
           
          // this.ligneReleveVentes.etat = 'Initial';
           
    }
    saveLigneReleveAchats() 
    {
      this.ligneReleveAchats.codeReleveAchats = this.data.codeReleveAchats;
      //  console.log(this.ligneFactureVentes);
      //  this.saveEnCours = true;
/*    this.releveVentesService.addLigne(this.ligneReleveVentes).subscribe((response) => {
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.ligneReleveVentes = new LigneReleveVente();
             this.openSnackBar(  'Ligne Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });*/
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
