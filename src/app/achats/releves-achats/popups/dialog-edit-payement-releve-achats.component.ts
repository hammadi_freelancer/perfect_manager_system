
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ReleveAchatsService} from '../releve-achats.service';
import {ReleveAchats} from '../releve-achats.model';
import { PayementReleveAchats } from '../payement-releve-achats.model';
@Component({
    selector: 'app-dialog-edit-payement-releve-achats',
    templateUrl: 'dialog-edit-payement-releve-achats.component.html',
  })
  export class DialogEditPayementReleveAchatsComponent implements OnInit {

     private payementReleveAchats: PayementReleveAchats;
     private updateEnCours = false;
     constructor(  private releveAchatsService: ReleveAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit() {
      this.payementReleveAchats = new PayementReleveAchats();
        this.releveAchatsService.getPayementReleveAchats(this.data.codePayementReleveAchats).subscribe((payement) => {
            this.payementReleveAchats = payement;
     });
   }
    updatePayementReleveAchats() {
      //  this.saveEnCours = true;
      this.updateEnCours = true;
       this.releveAchatsService.updatePayementReleveAchats(this.payementReleveAchats).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             // this.payementCredit = new PayementCredit();
             this.openSnackBar(  'Payement Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
