
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ReleveAchatsService} from '../releve-achats.service';
import {AjournementReleveAchats} from '../ajournement-releve-achats.model';
import { AjournementReleveAchatsElement } from '../ajournement-releve-achats.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-ajournements-releves-achats',
    templateUrl: 'dialog-list-ajournements-releves-achats.component.html',
  })
  export class DialogListAjournementsRelevesAchatsComponent implements OnInit {
     private listAjournementRelevesAchats: AjournementReleveAchats[] = [];
     private  dataAjournementsRelevesAchatsSource: MatTableDataSource<AjournementReleveAchatsElement>;
     private selection = new SelectionModel<AjournementReleveAchatsElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private releveAchatsService: ReleveAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      // console.log('code credit in dialog component is :', this.data.codeCredit);
      this.releveAchatsService.getAjournementsRelevesAchatsByCodeReleve(this.data.codeReleveAchats)
      .subscribe((listAjournementsRelAchats) => {
        this.listAjournementRelevesAchats = listAjournementsRelAchats;
// console.log('recieving data from backend', this.listAjournementCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedAjournementsReleveAchats: AjournementReleveAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((ajouRelElement) => {
    return ajouRelElement.code;
  });
  this.listAjournementRelevesAchats.forEach(ajournementRelAchats => {
    if (codes.findIndex(code => code === ajournementRelAchats.code) > -1) {
      listSelectedAjournementsReleveAchats.push(ajournementRelAchats);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listAjournementRelevesAchats !== undefined) {

      this.listAjournementRelevesAchats.forEach(ajournementRAchats=> {
             listElements.push( {code: ajournementRAchats.code ,
          dateOperation: ajournementRAchats.dateOperation,
          nouvelleDatePayement: ajournementRAchats.nouvelleDatePayement,
          motif: ajournementRAchats.motif,
          description: ajournementRAchats.description
        } );
      });
    }
      this.dataAjournementsRelevesAchatsSource = new MatTableDataSource<AjournementReleveAchatsElement>(listElements);
      this.dataAjournementsRelevesAchatsSource.sort = this.sort;
      this.dataAjournementsRelevesAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'},
     {name: 'nouvelleDatePayement' , displayTitle: 'N°elle Date Pay.'},
     {name: 'motif' , displayTitle: 'Motif'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataAjournementsRelevesAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataAjournementsRelevesAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataAjournementsRelevesAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataAjournementsRelevesAchatsSource.paginator) {
      this.dataAjournementsRelevesAchatsSource.paginator.firstPage();
    }
  }

}
