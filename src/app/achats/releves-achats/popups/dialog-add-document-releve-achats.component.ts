
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ReleveAchatsService} from '../releve-achats.service';
import {ReleveAchats} from '../releve-achats.model';
import { DocumentReleveAchats } from '../document-releve-achats.model';
@Component({
    selector: 'app-dialog-add-document-releve-achats',
    templateUrl: 'dialog-add-document-releve-achats.component.html',
  })
  export class DialogAddDocumentReleveAchatsComponent implements OnInit {
     private documentReleveAchats: DocumentReleveAchats;
     private saveEnCours = false;
     private typesDoc: any[] = [
      {value: 'Cheque', viewValue: 'Chèque'},
      {value: 'Traite', viewValue: 'Traite'},
      {value: 'FactureAchats', viewValue: 'Facture Achats'},
      {value: 'BonLivraison', viewValue: 'Bon Livraison'},
      {value: 'ReçuPayement', viewValue: 'Reçu Payement'},
      {value: 'Autre', viewValue: 'Autre'},

    ];
     constructor(  private releveAchatsService: ReleveAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.documentReleveAchats = new DocumentReleveAchats();
    }
    saveDocumentReleveAchats() 
    {
      this.documentReleveAchats.codeReleveAchats = this.data.codeReleveAchats;
       // console.log(this.documentCredit);
        this.saveEnCours = true;
       this.releveAchatsService.addDocumentReleveAchats(this.documentReleveAchats).
       subscribe((response) => {
         this.saveEnCours = false;
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.documentReleveAchats = new DocumentReleveAchats();
             this.openSnackBar(  'Document Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
     imageFace1Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentReleveAchats.imageFace1 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imageFace2Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentReleveAchats.imageFace2 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
  }
