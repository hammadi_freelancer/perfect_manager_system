
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ReleveAchatsService} from '../releve-achats.service';
import {AjournementReleveAchats} from '../ajournement-releve-achats.model';

@Component({
    selector: 'app-dialog-edit-ajournement-releve-achats',
    templateUrl: 'dialog-edit-ajournement-releve-achats.component.html',
  })
  export class DialogEditAjournementReleveAchatsComponent implements OnInit {
     private ajournementReleveAchats: AjournementReleveAchats;
     private updateEnCours = false;
     
     constructor(  private releveAchatsService: ReleveAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {

        this.releveAchatsService.getAjournementReleveAchats(this.data.codeAjournementReleveAchats).subscribe((ajour) => {
            this.ajournementReleveAchats = ajour;
     });


    }
    updateAjournementReleveAchats() 
    {
     this.updateEnCours = true;
       this.releveAchatsService.updateAjournementReleveAchats(this.ajournementReleveAchats).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.openSnackBar(  'Ajournement Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
