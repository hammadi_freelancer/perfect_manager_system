
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ReleveAchatsService} from '../releve-achats.service';
import {DocumentReleveAchats} from '../document-releve-achats.model';
import { DocumentReleveAchatsElement } from '../document-releve-achats.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-documents-releves-achats',
    templateUrl: 'dialog-list-documents-releves-achats.component.html',
  })
  export class DialogListDocumentsRelevesAchatsComponent implements OnInit {
     private listDocumentsRelevesAchats: DocumentReleveAchats[] = [];
     private  dataDocumentsRelevesAchats: MatTableDataSource<DocumentReleveAchatsElement>;
     private selection = new SelectionModel<DocumentReleveAchatsElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private releveAchatsService: ReleveAchatsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
     // console.log('code credit in dialog component is :', this.data.codeCredit);
      this.releveAchatsService.getDocumentsReleveAchatsByCodeReleve(this.data.codeReleveAchats).subscribe((listDocumentRelAchats) => {
        this.listDocumentsRelevesAchats = listDocumentRelAchats;
//console.log('recieving data from backend', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedDocumentsRelAchats: DocumentReleveAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentRElement) => {
    return documentRElement.code;
  });
  this.listDocumentsRelevesAchats.forEach(documentRA => {
    if (codes.findIndex(code => code === documentRA.code) > -1) {
      listSelectedDocumentsRelAchats.push(documentRA);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsRelevesAchats!== undefined) {

      this.listDocumentsRelevesAchats.forEach(docRAchats => {
             listElements.push( {code: docRAchats.code ,
          dateReception: docRAchats.dateReception,
          numeroDocument: docRAchats.numeroDocument,
          typeDocument: docRAchats.typeDocument,
          description: docRAchats.description
        } );
      });
    }
      this.dataDocumentsRelevesAchats= new MatTableDataSource<DocumentReleveAchatsElement>(listElements);
      this.dataDocumentsRelevesAchats.sort = this.sort;
      this.dataDocumentsRelevesAchats.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsRelevesAchats.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsRelevesAchats.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsRelevesAchats.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsRelevesAchats.paginator) {
      this.dataDocumentsRelevesAchats.paginator.firstPage();
    }
  }

}
