import { Article } from '../../stocks/articles/article.model';
import { Magasin } from '../../stocks/magasins/magasin.model';
import { UnMesure } from '../../stocks/un-mesures/un-mesure.model';
import { BaseEntity } from '../../common/base-entity.model' ;
// type LibelleModePayement = 'Credit'| 'Credit_Espece' | 'Cheque' | 'Espece';
// type Etat = 'Initial' | 'Valide' | 'Annule' | '' | 'Regle' | 'NonRegle';
export class LigneReleveAchats extends BaseEntity {
    constructor(
        public code?: string,
        public numeroLigneRelAchats?: string,
        public codeReleveAchats?: string,
        public numeroReleveAchats?: string,
        public article?: Article,
        public unMesure?: UnMesure,
        public quantite?: number,
        public prixUnt?: number,
         public prixTotal?: number,
         public beneficeTotalEstime?: number,
         public regle?: boolean,
         public livre?: boolean,
         
     ) {
        super();
         this.article = new Article();
         this.unMesure = new UnMesure();
         this.prixUnt = Number(0);
         this.quantite = Number(0);
         this.prixTotal =  Number(0);
         this.beneficeTotalEstime =  Number(0);
         
    }
}
