import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {FactureAchats} from '../factures-achats/facture-achats.model';

import {FactureAchatsService} from '../factures-achats/facture-achats.service';
import {ReleveAchatsService} from './releve-achats.service';

import {AchatsService} from '../achats.service';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
// import {ActionData} from './action-data.interface';
import { FournisseurService } from '../fournisseurs/fournisseur.service';
import { FournisseurFilter } from '../fournisseurs/fournisseur.filter';
import { ArticleFilter } from './article.filter';
import { UnMesureFilter } from '../../stocks/un-mesures/un-mesures.filter';

import {Fournisseur} from '../fournisseurs/fournisseur.model';
import {Magasin} from '../../stocks/magasins/magasin.model';
import {UnMesure} from '../../stocks/un-mesures/un-mesure.model';

import {ReleveAchats} from '../releves-achats/releve-achats.model';
import {LigneReleveAchats} from '../releves-achats/ligne-releve-achats.model';

import {LigneReleveAchatsElement} from '../releves-achats/ligne-releve-achats.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';

import {MatSnackBar} from '@angular/material';
import { MatExpansionPanel } from '@angular/material';
import {ArticleService} from '../../stocks/articles/article.service';
import {Article} from '../../stocks/articles/article.model';
import {ModePayementLibelle} from './mode-payement-libelle.interface';
import {SelectionModel} from '@angular/cdk/collections';

type LibelleModePayement = 'Credit'| 'Comptant' | 'Cheque' ;

@Component({
  selector: 'app-releve-achats-new',
  templateUrl: './releve-achats-new.component.html',
  // styleUrls: ['./players.component.css']
})
export class ReleveAchatsNewComponent implements OnInit {

 private releveAchats: ReleveAchats =  new ReleveAchats();
 // private parametresAchats = new ParametresAchats();
 private  dataLignesReleveAchatsSource: MatTableDataSource<LigneReleveAchatsElement>;
 
//@Input()
// private listVentes: any = [] ;

private saveEnCours = false;
private managedLigneReleveAchats = new LigneReleveAchats();

private selection = new SelectionModel<LigneReleveAchatsElement>(true, []);
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
 
/*@Output()
save: EventEmitter<FactureVente> = new EventEmitter<FactureVente>();*/

@ViewChild('firstMatExpansionPanel')
 private firstMatExpansionPanel : MatExpansionPanel;

 @ViewChild('manageLigneMatExpansionPanel')
 private manageLigneMatExpansionPanel : MatExpansionPanel;

 @ViewChild('listLignesMatExpansionPanel')
 private listLignesMatExpansionPanel : MatExpansionPanel;

private fournisseurFilter: FournisseurFilter;
private articleFilter: ArticleFilter;
private unMesureFilter : UnMesureFilter;
private listFournisseurs: Fournisseur[];


private payementModes: ModePayementLibelle[] = [
  {value: 'Comptant', viewValue: 'Comptant'},
  {value: 'Cheque', viewValue: 'Chèque'},
  {value: 'Credit', viewValue: 'Crédit'},
  {value: 'Credit_Comptant', viewValue: 'Crédit/Comptant'}
];
private etatsReleveAchats: any[] = [
  {value: 'Initial', viewValue: 'Initial'}
 // {value: 'Valide', viewValue: 'Validé'}
];

  constructor( private route: ActivatedRoute,
    private router: Router, private releveAchatsService: ReleveAchatsService, private fournisseurService: FournisseurService,
    private articleService: ArticleService, private achatsService: AchatsService,
   private matSnackBar: MatSnackBar, private elRef: ElementRef) {
  }


  // myControl = new FormControl();
  // options: string[] = ['One', 'Two', 'Three'];
  // filteredOptions: Observable<string[]>;

  ngOnInit() {

   this.listFournisseurs = this.route.snapshot.data.fournisseurs;
   // this.listArticles = this.route.snapshot.data.articles;
  // this.listCodesArticles  = this.listArticles.map((article) => article.code);
   this.fournisseurFilter = new FournisseurFilter(this.listFournisseurs);
   this.articleFilter = new ArticleFilter(this.route.snapshot.data.articles,
    [], [] );
     this.unMesureFilter = new UnMesureFilter(this.route.snapshot.data.unMesures);

   this.firstMatExpansionPanel.open();
   this.manageLigneMatExpansionPanel.opened.subscribe(openedPanel => {
   
     });
   this.specifyListColumnsToBeDisplayed();
   this.transformDataToByConsumedByMatTable();
  // this.initializeListLignesFactures();
  /*this.lignesFactures.forEach(function(ligne, index) {
                 ligne.code =  String(index + 1);
  });*/
  this.releveAchats.etat = 'Initial';
 
  }
 
  calculPrixTotal()
  {
    // this.managedLigneReleveVentes.prixTotal = this.
    this.managedLigneReleveAchats.prixTotal  = +  this.managedLigneReleveAchats.prixUnt *
    this.managedLigneReleveAchats.quantite  ;
    
  }



  constructClassName(i)  {
    return  'designationDivs_' + i;
   //  this.elRef.nativeElement.querySelector('.designationDivs_' + i )  ;
  }
 
  /*selectedArticleCode(i, $event) {
     console.log(i);
     console.log($event);
     this.listLignesFactureVentes[i].article.code = $event.option.value;
     this.listLignesFactureVentes[i].article.libelle  = this.listArticles.find((article) => (article.code === $event.option.value)).libelle;
    
     const  div  : HTMLDivElement = this.elRef.nativeElement.querySelector('.designationDivs_' + i )  ;
     div.innerText = this.listLignesFactureVentes[i].article.libelle;
     console.log(div);


  }*/

  saveReleveAchats() {

    /*if (this.modeUpdate ) {
      this.updateCredit();
    } else {*/
      if (this.releveAchats.etat === '' || this.releveAchats.etat === undefined) {
        this.releveAchats.etat = 'Initial';
      }

    this.saveEnCours = true;
    
    this.releveAchatsService.addReleveAchats(this.releveAchats).subscribe((response) => {
      this.saveEnCours = false;
      
        if (response.error_message ===  undefined) {
          console.log('undefined error message');
          // this.save.emit(response);
          this.releveAchats = new  ReleveAchats();
          this.releveAchats.listLignesRelAchats = [];
          this.openSnackBar(  'Relevée Achats Enregistrée');
        } else {
         // show error message
         this.openSnackBar(  'Erreurs:Opération Echouée');
         
        }
    });
  }
  fillDataNomPrenom()
  {
    const listFiltred  = this.listFournisseurs.filter((fournisseur) => (fournisseur.cin === this.releveAchats.fournisseur.cin));
    this.releveAchats.fournisseur.nom = listFiltred[0].nom;
    this.releveAchats.fournisseur.prenom = listFiltred[0].prenom;
  }
  fillDataMatriculeRaison()
  {
    const listFiltred  = this.listFournisseurs.filter((fournisseur) => (fournisseur.registreCommerce
       === this.releveAchats.fournisseur.registreCommerce));
    this.releveAchats.fournisseur.matriculeFiscale = listFiltred[0].matriculeFiscale;
    this.releveAchats.fournisseur.raisonSociale = listFiltred[0].raisonSociale;
  }
  vider() {
    this.releveAchats =  new ReleveAchats();
  }
  openSnackBar(messageToDisplay) {
   this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
       verticalPosition: 'top', panelClass: 'snackBarClass'});
  }
loadGridRelevesAchats() {
  this.router.navigate(['/pms/achats/relevesAchats']) ;
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.releveAchats.listLignesRelAchats !== undefined) {
    this.releveAchats.listLignesRelAchats.forEach(ligneRel => {
           listElements.push( {code: ligneRel.code ,
            numeroLigneRelAchats: ligneRel.numeroLigneRelAchats,
            article: ligneRel.article.code,
             unMesure: ligneRel.unMesure.code,
             quantite: ligneRel.quantite,
             prixUnt: ligneRel.prixUnt,
             prixTotal: ligneRel.prixTotal,
             beneficeTotalEstime: ligneRel.beneficeTotalEstime,
             regle: ligneRel.regle,
             livre: ligneRel.livre
      } );
    });
  }
    this.dataLignesReleveAchatsSource= new MatTableDataSource<LigneReleveAchatsElement>(listElements);
    this.dataLignesReleveAchatsSource.sort = this.sort;
    this.dataLignesReleveAchatsSource.paginator = this.paginator;
    
    
}
 specifyListColumnsToBeDisplayed() {
   // console.log('calling specifyListColumnsToBeDisplayed documents grid');
  this.columnsTitles = [];
  this.columnsNames = [];
  this.columnsTitlesAr  = [];
  
   this.columnsDefinitions = [{name: 'numeroLigneRelAchats' , displayTitle: 'N°'},
   {name: 'article' , displayTitle: 'Art.'},
   {name: 'unMesure' , displayTitle: 'Un.Mes.'},
   {name: 'quantite' , displayTitle: 'Qté'},
   {name: 'prixUnt' , displayTitle: 'P.Unt(TTC)'},
   {name: 'prixTotal' , displayTitle: 'P.Tot(TTC)'},
   {name: 'beneficeTotalEstime' , displayTitle: 'Bénef.Tot.Est.'},
   {name: 'regle' , displayTitle: 'Réglé'},
   {name: 'livre' , displayTitle: 'Livré'},
    ];

   this.columnsTitles = this.columnsDefinitions.map((colDef) => {
             return colDef.displayTitle;
   });
   
   this.columnsNames[0] = 'select';
   this.columnsDefinitions.map((colDef) => {
    this.columnsNames.push(colDef.name);
    console.log(this.columnsNames);
});
   this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
    return colDef.displayTitleAr;
});
}

applyFilter(filterValue: string) {
  this.dataLignesReleveAchatsSource.filter = filterValue.trim().toLowerCase();
  if (this.dataLignesReleveAchatsSource.paginator) {
    this.dataLignesReleveAchatsSource.paginator.firstPage();
  }
}

checkLigneReleveAchats()
{

}
getIndexOfRow(row)
{

    for ( let i = 0; i < this.releveAchats.listLignesRelAchats.length ; i++) {
      if (this.releveAchats.listLignesRelAchats[i].numeroLigneRelAchats === row.numeroLigneRelAchats)
       {
             return i;
        }
    }
    return -1;
}
mapRowGridToObjectLigneReleveAchats(rowGrid): LigneReleveAchats
{
  const ligneRV  = new  LigneReleveAchats() ;
  ligneRV.numeroReleveAchats = rowGrid.numeroReleveAchats;
  ligneRV.numeroLigneRelAchats = rowGrid.numeroLigneRelAchats;
  ligneRV.article.code =  rowGrid.article;
  ligneRV.unMesure.code =  rowGrid.unMesure;
  ligneRV.quantite =  rowGrid.quantite;
  ligneRV.prixUnt =  rowGrid.prixUnt;
  ligneRV.prixTotal =  rowGrid.prixTotal;
  ligneRV.beneficeTotalEstime =  rowGrid.beneficeTotalEstime;
  ligneRV.livre =  rowGrid.livre;
  ligneRV.regle =  rowGrid.regle;
  return ligneRV;
}
editLigneReleveAchats(row)
{
  this.managedLigneReleveAchats = this.mapRowGridToObjectLigneReleveAchats(row);


    if(row.unMesure === undefined)
      {
        this.managedLigneReleveAchats.unMesure = new UnMesure();
      }
      if(row.article === undefined)
        {
          this.managedLigneReleveAchats.article = new Article();
        }
        console.log('row to edit is ', row);

  this.manageLigneMatExpansionPanel.open();
  
}
deleteLigneReleveAchats(row)
{
 // console.log('index is ');
  //console.log(index);
  const indexOfRow = this.getIndexOfRow(row);
  this.releveAchats.listLignesRelAchats.splice(indexOfRow , 1 );
  this.transformDataToByConsumedByMatTable();
  
}
addLigneReleveAchats()
{
  if (this.managedLigneReleveAchats.quantite === undefined
    || this.managedLigneReleveAchats.quantite === 0 ||
    this.managedLigneReleveAchats.quantite === null ||
    this.managedLigneReleveAchats.prixUnt === undefined
      || this.managedLigneReleveAchats.prixUnt === 0 ||
      this.managedLigneReleveAchats.prixUnt === null  ||
      this.managedLigneReleveAchats.article.code === undefined  ||
      this.managedLigneReleveAchats.article.code === '' ||
      this.managedLigneReleveAchats.article.code === null 
  ) {
        this.openSnackBar('Donnnées Manquées (QTE/ART/PUNIT!');
    } else if (this.managedLigneReleveAchats.numeroLigneRelAchats === undefined
  || this.managedLigneReleveAchats.numeroLigneRelAchats === '' ||
  this.managedLigneReleveAchats.numeroLigneRelAchats=== null ) {
      this.openSnackBar('Numéro de Ligne Non Spécifié !');
    } else {
  this.managedLigneReleveAchats.numeroReleveAchats = this.releveAchats.numeroReleveAchats;
  if (this.releveAchats.listLignesRelAchats.length === 0) {
  this.releveAchats.listLignesRelAchats.push(this.managedLigneReleveAchats);
  } else  {
    console.log('look for index:');
     const indexOfRow = this.getIndexOfRow(this.managedLigneReleveAchats);
     console.log(indexOfRow);
     
     {
           if (indexOfRow === -1) {
    this.releveAchats.listLignesRelAchats.push(this.managedLigneReleveAchats);

         } else  {
     this.releveAchats.listLignesRelAchats[indexOfRow] = this.managedLigneReleveAchats;

       }
      }
  }

  this.transformDataToByConsumedByMatTable() ;
  this.managedLigneReleveAchats = new LigneReleveAchats();
  this.manageLigneMatExpansionPanel.close();
  this.listLignesMatExpansionPanel.open();
}
}

/*masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataLignesFactureVentesSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataLignesFactureVentesSource.data.length;
  return numSelected === numRows;
}*/
 /* deleteRow(i)
  {
    
    this.factureVente.montantHT = +this.factureVente.montantHT - this.lignesFactures[i].montantHT;
    this.factureVente.montantTVA = +this.factureVente.montantTVA - this.lignesFactures[i].montantTVA ;
    this.factureVente.montantTTC = +this.factureVente.montantTTC  - this.lignesFactures[i].montantTTC ;

    this.factureVente.montantRemise = +this.factureVente.montantRemise  - this.lignesFactures[i].montantRemise ;
      this.factureVente.montantAPayer = this.factureVente.montantTTC ;
      this.factureVente.montantAPayerApresRemise = +this.factureVente.montantAPayer - this.factureVente.montantRemise;
      console.log('after delete');
      console.log( this.factureVente.montantHT);
      console.log( this.factureVente.montantTTC);
      this.lignesFactures.splice(i, 1 );
      /*this.lignesFactures.forEach(function(ligne, index) {
                  ligne.code = index + 1;         
      },
      this);*/
      /*this.editLignesMode.splice(i, 1); 
      this.readOnlyLignes.splice(i, 1); 
      
      this.lignesFactures.push(new LigneFactureVente());
      this.editLignesMode.push(false);
      this.readOnlyLignes.push(false);
      
         
}*/

/*

  validateRow(i)
  {
     this.factureVente.montantHT = +this.factureVente.montantHT + this.lignesFactures[i].montantHT ;
     this.factureVente.montantTVA = +this.factureVente.montantTVA + this.lignesFactures[i].montantTVA ;
     this.factureVente.montantTTC = +this.factureVente.montantTTC  + this.lignesFactures[i].montantTTC ;
     this.factureVente.montantRemise = +this.factureVente.montantRemise  + this.lignesFactures[i].montantRemise ;
     this.factureVente.montantAPayer = this.factureVente.montantTTC ;
     this.factureVente.montantAPayerApresRemise = +this.factureVente.montantAPayer - this.factureVente.montantRemise;
     this.editLignesMode[i] = true;
     this.readOnlyLignes[i] = true;
    }
    editRow(i)
    {
      if (this.readOnlyLignes[i])
      {
        this.readOnlyLignes[i] = false;
      }  else {
       // this.validateRow(i);

        this.recalculMontants();
        this.readOnlyLignes[i] = true;
        
      }
       
    }*/

/*
nextPage()
  {
    this.pagesLignesFacures[this.currentPage - 1] = this.lignesFactures;

    this.pagesReadOnlyLignesFacures[this.currentPage - 1] = this.readOnlyLignes;
    this.pagesEditModeLignesFacures[this.currentPage - 1] = this.editLignesMode;
    
    this.currentPage++;
    if ( this.pagesLignesFacures[this.currentPage - 1] === undefined) {
    this.initializeListLignesFactures();

console.log('Next Page lignes Factures ');
console.log(this.lignesFactures);
this.pagesLignesFacures[this.currentPage - 1 ] = this.lignesFactures;
this.pagesReadOnlyLignesFacures[this.currentPage - 1] = this.readOnlyLignes;
this.pagesEditModeLignesFacures[this.currentPage - 1] = this.editLignesMode;


} else {
console.log('Next Page lignes Factures ');
this.lignesFactures = this.pagesLignesFacures[this.currentPage - 1 ];
this.readOnlyLignes = this.pagesReadOnlyLignesFacures[this.currentPage - 1 ];
this.editLignesMode = this.pagesEditModeLignesFacures[this.currentPage - 1 ];

}
this.factureVenteFilter = new FactureVenteFilter(this.listClients, this.listArticles);
const  button: HTMLButtonElement = this.elRef.nativeElement.querySelector('.paginationLabel' )  ;
button.innerText = 'Page ' + this.currentPage + ' / ' + this.pagesLignesFacures.length;
}

previousPage()
{
this.pagesLignesFacures[this.currentPage - 1] = this.lignesFactures ;
this.pagesReadOnlyLignesFacures[this.currentPage - 1] = this.readOnlyLignes;
this.pagesEditModeLignesFacures[this.currentPage - 1] = this.editLignesMode;

this.currentPage--;
this.lignesFactures =  this.pagesLignesFacures[this.currentPage - 1];
this.readOnlyLignes = this.pagesReadOnlyLignesFacures[this.currentPage - 1];
this.editLignesMode = this.pagesEditModeLignesFacures[this.currentPage - 1];

this.factureVenteFilter = new FactureVenteFilter(this.listClients, this.listArticles);
const  button: HTMLButtonElement = this.elRef.nativeElement.querySelector('.paginationLabel' )  ;
button.innerText = 'Page ' + this.currentPage + ' / ' + this.pagesLignesFacures.length;

}*/

  /*fillDataLignesFactures()
  {
    this.factureVente.listLignesFactures = [];
     this.listLignesFactureVentes.forEach(function(ligne, index) {
           if ( ligne.montantHT !== 0 ) {
             if (ligne.code === undefined || ligne.code === '')
              {
                ligne.code = this.factureVente.listLignesFactures.length + 1;
              }
            this.factureVente.listLignesFactures.push(ligne);
           }
    
     }, this);
  }*/
  /*checkOneActionInvoked() {
  const listSelectedLignesFactureVentes: LigneFactureVente[] = [];
console.log(this.selection.selected);
if (this.selection.selected.length !== 0 ) {
 this.managedLigneFactureVentes.numeroLigneFactureVentes =  this.selection.selected[this.selection.selected.length - 1]
 .numeroLigneFactureVentes;
 this.managedLigneFactureVentes.article.code =  this.selection.selected[this.selection.selected.length - 1].article;
 this.managedLigneFactureVentes.magasin.code =  this.selection.selected[this.selection.selected.length - 1].magasin;
 this.managedLigneFactureVentes.unMesure.code =  this.selection.selected[this.selection.selected.length - 1].unMesure;
 this.managedLigneFactureVentes.quantite =  this.selection.selected[this.selection.selected.length - 1].quantite;
 this.managedLigneFactureVentes.prixUnitaire=  this.selection.selected[this.selection.selected.length - 1].prixUnitaire;
 this.managedLigneFactureVentes.montantHT =  this.selection.selected[this.selection.selected.length - 1].montantHT;
 this.managedLigneFactureVentes.montantTTC =  this.selection.selected[this.selection.selected.length - 1].montantTTC;
 this.managedLigneFactureVentes.tva =  this.selection.selected[this.selection.selected.length - 1].tva; 
 this.managedLigneFactureVentes.montantTVA =  this.selection.selected[this.selection.selected.length - 1].montantTVA;
 this.managedLigneFactureVentes.fodec =  this.selection.selected[this.selection.selected.length - 1].fodec;
 this.managedLigneFactureVentes.montantFodec =  this.selection.selected[this.selection.selected.length - 1].montantFodec;
 this.managedLigneFactureVentes.remise =  this.selection.selected[this.selection.selected.length - 1].remise;
 this.managedLigneFactureVentes.montantRemise=  this.selection.selected[this.selection.selected.length - 1].montantRemise;

 this.managedLigneFactureVentes.otherTaxe1 =  this.selection.selected[this.selection.selected.length - 1].otherTaxe1;
 this.managedLigneFactureVentes.montantOtherTaxe1 =  this.selection.selected[this.selection.selected.length - 1].montantOtherTaxe1;
 
 this.managedLigneFactureVentes.otherTaxe2 =  this.selection.selected[this.selection.selected.length - 1].otherTaxe2;
 this.managedLigneFactureVentes.montantOtherTaxe2=  this.selection.selected[this.selection.selected.length - 1].montantOtherTaxe2;

 this.managedLigneFactureVentes.montantBenefice=  this.selection.selected[this.selection.selected.length - 1].montantBenefice;
 this.managedLigneFactureVentes.benefice=  this.selection.selected[this.selection.selected.length - 1].benefice;
 
 
const codes: string[] = this.selection.selected.map((ligneFactElement) => {
  return ligneFactElement.code;
});
this.factureVente.listLignesFactures.forEach(LigneF => {
  if (codes.findIndex(code => code === LigneF.code) > -1) {
    listSelectedLignesFactureVentes.push(LigneF);
  }
  });
}
}*/
  fillDataLibelleUnMesure()
  {

  }
  fillDataLibelleMagasins()
  {
    
  }
  fillDataLibelleArticle()
  {
    
  }
}
