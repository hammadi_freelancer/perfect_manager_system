
import { Component, OnInit, Inject, ViewChild, Input, OnChanges} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ReleveAchatsService} from './releve-achats.service';
import { ReleveAchats} from './releve-achats.model';
import {AjournementReleveAchats} from './ajournement-releve-achats.model';
import { AjournementReleveAchatsElement } from './ajournement-releve-achats.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import { DialogAddAjournementReleveAchatsComponent } from './popups/dialog-add-ajournement-releve-achats.component';
import { DialogEditAjournementReleveAchatsComponent} from './popups/dialog-edit-ajournement-releve-achats.component';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-ajournements-releves-achats-grid',
    templateUrl: 'ajournements-releves-achats-grid.component.html',
  })
  export class AjournementsRelevesAchatsGridComponent implements OnInit, OnChanges {
     private listAjournementReleveAchats: AjournementReleveAchats[] = [];
     private  dataAjournementsRelevesAchatsSource: MatTableDataSource<AjournementReleveAchatsElement>;
     private selection = new SelectionModel<AjournementReleveAchatsElement>(true, []);

     @Input()
     private releveAchats : ReleveAchats;

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private releveAchatsService: ReleveAchatsService,
     private matSnackBar: MatSnackBar, public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.releveAchatsService.getAjournementsRelevesAchatsByCodeReleve(this.releveAchats.code).
      subscribe((listAjournementsRelAchats) => {
        this.listAjournementReleveAchats = listAjournementsRelAchats;
// console.log('recieving data from backend', this.listAjournementsFactureVentes  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }

ngOnChanges()
{
  this.releveAchatsService.getAjournementsRelevesAchatsByCodeReleve
  (this.releveAchats.code).subscribe((listAjournementsRelAchats) => {
    this.listAjournementReleveAchats = listAjournementsRelAchats;
//console.log('recieving data from backend', this.listAjournementCredits  );
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
     });
}
  checkOneActionInvoked() {
    const listSelectedAjournementsReleveAchats: AjournementReleveAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((ajournementRAchatsElement) => {
    return ajournementRAchatsElement.code;
  });
  this.listAjournementReleveAchats.forEach(ajournementRAch => {
    if (codes.findIndex(code => code === ajournementRAch.code) > -1) {
      listSelectedAjournementsReleveAchats.push(ajournementRAch);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listAjournementReleveAchats !== undefined) {

      this.listAjournementReleveAchats.forEach(ajournementRAchats => {
             listElements.push( {code: ajournementRAchats.code ,
          dateOperation: ajournementRAchats.dateOperation,
          nouvelleDatePayement: ajournementRAchats.nouvelleDatePayement,
          motif: ajournementRAchats.motif,
          description: ajournementRAchats.description
        } );
      });
    }
      this.dataAjournementsRelevesAchatsSource = new MatTableDataSource<AjournementReleveAchatsElement>(listElements);
      this.dataAjournementsRelevesAchatsSource.sort = this.sort;
      this.dataAjournementsRelevesAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'},
     {name: 'nouvelleDatePayement' , displayTitle: 'N°elle Date Pay.'},
     {name: 'motif' , displayTitle: 'Motif'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataAjournementsRelevesAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataAjournementsRelevesAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataAjournementsRelevesAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataAjournementsRelevesAchatsSource.paginator) {
      this.dataAjournementsRelevesAchatsSource.paginator.firstPage();
    }
  }

  showAddAjournementDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeReleveAchats: this.releveAchats.code
   };
 
   const dialogRef = this.dialog.open(DialogAddAjournementReleveAchatsComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(ajAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditAjournementDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeAjournementReleveAchats : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditAjournementReleveAchatsComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(ajAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Ajournement Séléctionnée!');
    }
    }
    supprimerAjournements()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(ajour, index) {
              this.releveAchatsService.deleteAjournementReleveAchats(ajour.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar(  ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Ajournement Séléctionnée!');
        }
    }
    refresh() {
      this.releveAchatsService.getAjournementsRelevesAchatsByCodeReleve(this.releveAchats.code).subscribe((listAjourRel) => {
        this.listAjournementReleveAchats= listAjourRel;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }


}
