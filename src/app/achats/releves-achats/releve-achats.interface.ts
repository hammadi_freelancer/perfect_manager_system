export interface ReleveAchatsElement {
            code: string;
         numeroReleveAchats: string;
         cin: string;
         nom: string;
          prenom: string;
         matriculeFiscale: string ;
         raisonSociale: string;
          montantBeneficeEstime: number;
          montantAPayer: number;
         // payementData: PayementData;
         dateAchats?: string;
         regleToutes?: boolean;
         livreToutes?: boolean;
         etat: string;
        }
