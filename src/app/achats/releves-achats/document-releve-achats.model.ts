
import { BaseEntity } from '../../common/base-entity.model' ;

type TypeDocument =   'Cheque' | 'Traite' |'FactureAchats' | 'BonReception'
 | 'ReçuPayement' | 'Autre' ;

export class DocumentReleveAchats extends BaseEntity {
    constructor(
        public code?: string,
         public codeReleveAchats?: string,
         public numeroDocument?: string,
        public dateReceptionObject?: Date,
        public dateReception?: string,
        public typeDocument?: TypeDocument,
        public fileExtension?: string,
        public imageFace1?: any,
        public imageFace2?: any,
     ) {
         super();
        
       }
  
}
