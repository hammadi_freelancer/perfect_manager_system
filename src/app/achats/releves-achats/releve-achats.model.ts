
import { BaseEntity } from '../../common/base-entity.model' ;
// import { Tranche } from './tranche.model' ;
import { Fournisseur } from '../fournisseurs/fournisseur.model' ;
import { PayementData } from './payement-data.model';
import { LigneReleveAchats } from './ligne-releve-achats.model';
//import { Credit } from '../credits/credit.model';

type  EtatReleveAchats= |'Initial' |'Valide' | 'Annule' | '';


export class ReleveAchats extends BaseEntity {
    constructor(
        public code?: string,
        public numeroReleveAchats?: string,
        public fournisseur?: Fournisseur,
         public montantBeneficeEstime?: number,
        public montantAPayer?: number,
        public payementData?: PayementData,
        public dateAchatsObject?: Date,
        public dateAchats?: string,
        public regleToutes?: boolean,
        public livreToutes?: boolean,
          public etat?: EtatReleveAchats,
     public listLignesRelAchats?: LigneReleveAchats[]
     ) {
         super();
         this.fournisseur = new Fournisseur();
         this.montantAPayer = Number(0);
         this.montantBeneficeEstime = Number(0);
         this.payementData = new PayementData();
         this.listLignesRelAchats= [] ;
         // this.categoryClient = 'PERSONNE_PHYSIQUE';
    }
}
