import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import {ReleveAchats} from './releve-achats.model' ;
import {DocumentReleveAchats} from './document-releve-achats.model' ;
import {PayementReleveAchats} from './payement-releve-achats.model' ;
import {AjournementReleveAchats} from './ajournement-releve-achats.model' ;

import {CreditAchats} from '../credits-achats/credit-achats.model' ;
import {LigneReleveAchats} from './ligne-releve-achats.model' ;

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_RELEVE_ACHATS = 'http://localhost:8082/achats/relevesAchats/getRelevesAchats';
const  URL_GET_PAGE_RELEVE_ACHATS = 'http://localhost:8082/achats/relevesAchats/getPageRelevesAchats';

const URL_GET_RELEVE_ACHATS = 'http://localhost:8082/achats/relevesAchats/getReleveAchats';
const  URL_ADD_RELEVE_ACHATS  = 'http://localhost:8082/achats/relevesAchats/addReleveAchats';
const  URL_UPDATE_RELEVE_ACHATS  = 'http://localhost:8082/achats/relevesAchats/updateReleveAchats';
const  URL_DELETE_RELEVE_ACHATS  = 'http://localhost:8082/achats/relevesAchats/deleteReleveAchats';
const  URL_GET_TOTAL_RELEVE_ACHATS = 'http://localhost:8082/achats/relevesAchats/getTotalRelevesAchats';

const  URL_ADD_PAYEMENT_RELEVE_ACHATS = 'http://localhost:8082/finance/payementsRelevesAchats/addPayementReleveAchats';
const  URL_GET_LIST_PAYEMENT_RELEVE_ACHATS  = 'http://localhost:8082/finance/payementsRelevesAchats/getPayementsReleveAchats';
const  URL_DELETE_PAYEMENT_RELEVE_ACHATS  = 'http://localhost:8082/finance/payementsRelevesAchats/deletePayementReleveAchats';
const  URL_UPDATE_PAYEMENT_RELEVE_ACHATS  = 'http://localhost:8082/finance/payementsRelevesAchats/updatePayementReleveAchats';
const URL_GET_PAYEMENT_RELEVE_ACHATS  = 'http://localhost:8082/finance/payementsRelevesAchats/getPayementReleveAchats';
const URL_GET_LIST_PAYEMENT_RELEVE_ACHATS_BY_CODE_RELEVE =
'http://localhost:8082/finance/payementsRelevesAchats/getPayementsReleveAchatsByCodeReleve';


const  URL_ADD_DOCUMENT_RELEVE_ACHATS = 'http://localhost:8082/achats/documentsRelevesAchats/addDocumentReleveAchats';
const  URL_GET_LIST_DOCUMENTS_RELEVE_ACHATS = 'http://localhost:8082/achats/documentsRelevesAchats/getDocumentsReleveAchats';
const  URL_DELETE_DOCUMENT_RELEVE_ACHATS= 'http://localhost:8082/achats/documentsRelevesAchats/deleteDocumentReleveAchats';
const  URL_UPDATE_DOCUMENT_RELEVE_ACHATS = 'http://localhost:8082/achats/documentsRelevesAchats/updateDocumentReleveAchats';
const URL_GET_DOCUMENT_RELEVE_ACHATS = 'http://localhost:8082/achats/documentsRelevesAchats/getDocumentReleveAchats';
const URL_GET_LIST_DOCUMENTS_RELEVE_ACHATS_BY_CODE_RELEVE =
 'http://localhost:8082/achats/documentsRelevesAchats/getDocumentsReleveAchatsByCodeReleve';

 const  URL_ADD_AJOURNEMENT_RELEVE_ACHATS = 'http://localhost:8082/achats/ajournementsRelevesAchats/addAjournementReleveAchats';
 const  URL_GET_LIST_AJOURNEMENT_RELEVE_ACHATS = 'http://localhost:8082/achats/ajournementsRelevesAchats/getAjournementsReleveAchats';
 const  URL_DELETE_AJOURNEMENT_RELEVE_ACHATS= 'http://localhost:8082/achats/ajournementsRelevesAchats/deleteAjournementReleveAchats';
 const  URL_UPDATE_AJOURNEMENT_RELEVE_ACHATS = 'http://localhost:8082/achats/ajournementsRelevesAchats/updateAjournementReleveAchats';
 const URL_GET_AJOURNEMENT_RELEVE_ACHATS = 'http://localhost:8082/achats/ajournementsRelevesAchats/getAjournementReleveAchats';
 const URL_GET_LIST_AJOURNEMENT_RELEVE_ACHATS_BY_CODE_RELEVE =
  'http://localhost:8082/achats/ajournementsRelevesAchats/getAjournementsReleveAchatsByCodeReleve';



  /*const  URL_ADD_LIGNE_FACTURE_VENTES = 'http://localhost:8082/ventes/lignesFacturesVentes/addLigneFactureVentes';
  const  URL_GET_LIST_LIGNES_FACTURE_VENTES = 'http://localhost:8082/ventes/lignesFacturesVentes/getLignesFactureVentes';
  const  URL_DELETE_LIGNE_FACTURE_VENTES = 'http://localhost:8082/ventes/lignesFacturesVentes/deleteLigneFactureVentes';
  const  URL_UPDATE_LIGNE_FACTURE_VENTES = 'http://localhost:8082/ventes/lignesFacturesVentes/updateLigneFactureVentes';
  const URL_GET_LIGNE_FACTURE_VENTES = 'http://localhost:8082/ventes/lignesFacturesVentes/getLigneFactureVentes';
  const URL_GET_LIST_LIGNES_FACTURE_VENTES_BY_CODE_FACTURE_VENTES =
   'http://localhost:8082/ventes/lignesFacturesVentes/getLignesFactureVentesByCodeFactureVente';*/

@Injectable({
  providedIn: 'root'
})
export class ReleveAchatsService {

  constructor(private httpClient: HttpClient) {

   }
   getTotalRelevesAchats(filter): Observable<any> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('filter', filterStr);
    return this.httpClient
    .get<any>(URL_GET_TOTAL_RELEVE_ACHATS, {params});
   }


   getPageRelevesAchats(pageNumber, nbElementsPerPage, filter ): Observable<ReleveAchats[]> {
  
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('pageNumber', pageNumber)
    .set('nbElementsPerPage', nbElementsPerPage)
    .set('filter', filterStr);
    return this.httpClient
    .get<ReleveAchats[]>(URL_GET_PAGE_RELEVE_ACHATS, {params});
   }

   getRelevesAchats(): Observable<ReleveAchats[]> {
   
    return this.httpClient
    .get<ReleveAchats[]>(URL_GET_LIST_RELEVE_ACHATS);
   }
   addReleveAchats(releveAchats: ReleveAchats): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_RELEVE_ACHATS, releveAchats);
  }
  updateReleveAchats(releveAchats: ReleveAchats): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_RELEVE_ACHATS, releveAchats);
  }
  deleteReleveAchats(codeReleveAchats): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_RELEVE_ACHATS+ '/' + codeReleveAchats);
  }
  getReleveAchats(codeReleveAchats): Observable<ReleveAchats> {
    return this.httpClient.get<ReleveAchats>(URL_GET_RELEVE_ACHATS+ '/' + codeReleveAchats);
  }


  addPayementReleveAchats(payementReleveAchats: PayementReleveAchats): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_PAYEMENT_RELEVE_ACHATS, payementReleveAchats);
    
  }
  getPayementsRelevesAchats(): Observable<PayementReleveAchats[]> {
    return this.httpClient
    .get<PayementReleveAchats[]>(URL_GET_LIST_RELEVE_ACHATS);
   }
   getPayementReleveAchats(codePayementReleveAchats): Observable<PayementReleveAchats> 
   {
    return this.httpClient.get<PayementReleveAchats>(URL_GET_PAYEMENT_RELEVE_ACHATS+ '/' + codePayementReleveAchats);
  }
  deletePayementReleveAchats(codePayementReleveAchats): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_PAYEMENT_RELEVE_ACHATS + '/' + codePayementReleveAchats);
  }
  getPayementsRelevesAchatsByCodeReleve(codeReleveAchats): Observable<PayementReleveAchats[]> {
    return this.httpClient.get<PayementReleveAchats[]>(URL_GET_LIST_PAYEMENT_RELEVE_ACHATS_BY_CODE_RELEVE
       + '/' + codeReleveAchats);
  }

  updatePayementReleveAchats(payRelAchats: PayementReleveAchats): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_PAYEMENT_RELEVE_ACHATS, payRelAchats);
  }

  addDocumentReleveAchats(docReleveAchats: DocumentReleveAchats): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_DOCUMENT_RELEVE_ACHATS, docReleveAchats);
    
  }
  getDocumentsRelevesAchats(): Observable<DocumentReleveAchats[]> {
    return this.httpClient
    .get<DocumentReleveAchats[]>(URL_GET_LIST_DOCUMENTS_RELEVE_ACHATS);
   }
   getDocumentReleveAchats(codeDocumentReleveAchats): Observable<DocumentReleveAchats> {
    return this.httpClient.get<DocumentReleveAchats>(URL_GET_DOCUMENT_RELEVE_ACHATS + '/' + codeDocumentReleveAchats);
  }
  deleteDocumentReleveAchats(codeDocumentReleveAchats): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_DOCUMENT_RELEVE_ACHATS + '/' + codeDocumentReleveAchats);
  }
  updateDocumentReleveAchats(docRelAchats: DocumentReleveAchats): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_DOCUMENT_RELEVE_ACHATS, docRelAchats);
  }


  getDocumentsReleveAchatsByCodeReleve(codeReleveAchats): Observable<DocumentReleveAchats[]> {
    return this.httpClient.get<DocumentReleveAchats[]>( URL_GET_LIST_DOCUMENTS_RELEVE_ACHATS_BY_CODE_RELEVE +
       '/' + codeReleveAchats);
  }

  addAjournementReleveAchats(ajournReleveAchats: AjournementReleveAchats): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_AJOURNEMENT_RELEVE_ACHATS, ajournReleveAchats);
    
  }
  updateAjournementReleveAchats(ajournReleveAchats: AjournementReleveAchats): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_AJOURNEMENT_RELEVE_ACHATS, ajournReleveAchats);
  }
  getAjournementsRelevesAchatsByCodeReleve(codeRelAchats): Observable<AjournementReleveAchats[]> {
    return this.httpClient.get<AjournementReleveAchats[]>(
      URL_GET_LIST_AJOURNEMENT_RELEVE_ACHATS_BY_CODE_RELEVE +
       '/' + codeRelAchats);
  }
  getAjournementsRelevesAchats(): Observable<AjournementReleveAchats[]> {
    return this.httpClient
    .get<AjournementReleveAchats[]>(URL_GET_LIST_AJOURNEMENT_RELEVE_ACHATS);
   }
   getAjournementReleveAchats(codeAjournementReleveAchats): Observable<AjournementReleveAchats> {
    return this.httpClient.get<AjournementReleveAchats>(URL_GET_AJOURNEMENT_RELEVE_ACHATS + '/' + codeAjournementReleveAchats);
  }
  deleteAjournementReleveAchats(codeAjournementReleveAchats): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_AJOURNEMENT_RELEVE_ACHATS + '/' + codeAjournementReleveAchats);
  }


  /*addLigneFactureVentes(ligneFactureVente: LigneFactureVente): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_LIGNE_FACTURE_VENTES, ligneFactureVente);
    
  }
  updateLigneFactureVentes(ligneFactureVente: LigneFactureVente): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_LIGNE_FACTURE_VENTES, ligneFactureVente);
  }
  getLignesFacturesVentesByCodeFacture(codeLigneFactureVentes): Observable<LigneFactureVente[]> {
    return this.httpClient.get<LigneFactureVente[]>( URL_GET_LIST_LIGNES_FACTURE_VENTES_BY_CODE_FACTURE_VENTES +
       '/' + codeLigneFactureVentes);
  }
  getLignesFacturesVentes(): Observable<LigneFactureVente[]> {
    return this.httpClient
    .get<LigneFactureVente[]>(URL_GET_LIST_LIGNES_FACTURE_VENTES);
   }
   getLigneFactureVentes(codeLigneFactureVentes): Observable<LigneFactureVente> {
    return this.httpClient.get<LigneFactureVente>(URL_GET_LIST_LIGNES_FACTURE_VENTES + '/' + codeLigneFactureVentes);
  }
  deleteLigneFactureVentes(codeLigneFactureVentes): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_LIGNE_FACTURE_VENTES + '/' + codeLigneFactureVentes);
  }*/
}
