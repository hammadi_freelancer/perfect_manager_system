
import { BaseEntity } from '../../common/base-entity.model' ;

type Etat = 'Initial' | 'Valide' | 'Annule' 
type ModePayement = 'Comptant' | 'Cheque' | 'Virement'

export class AjournementReleveAchats extends BaseEntity {
    constructor(
        public code?: string,
         public codeReleveAchats?: string,
        public dateOperationObject?: Date,
        public nouvelleDatePayementObject?: Date,
        public motif?: string,
        public nouvelleDatePayement?: string,
        public dateOperation?: string,
        
     ) {
         super();
        
       }
  
}
