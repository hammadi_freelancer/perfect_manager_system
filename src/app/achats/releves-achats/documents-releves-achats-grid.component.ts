
import { Component, OnInit, Inject,Input, ViewChild, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {ReleveAchatsService} from './releve-achats.service';
import {ReleveAchats} from './releve-achats.model';

import {DocumentReleveAchats} from './document-releve-achats.model';
import { DocumentReleveAchatsElement } from './document-releve-achats.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogAddDocumentReleveAchatsComponent } from './popups/dialog-add-document-releve-achats.component';
 import { DialogEditDocumentReleveAchatsComponent } from './popups/dialog-edit-document-releve-achats.component';
@Component({
    selector: 'app-documents-releves-achats-grid',
    templateUrl: 'documents-releves-achats-grid.component.html',
  })
  export class DocumentsReleveAchatsGridComponent implements OnInit, OnChanges {
     private listDocumentsRelevesAchats: DocumentReleveAchats[] = [];
     private  dataDocumentsRelevesAchatsSource: MatTableDataSource<DocumentReleveAchatsElement>;
     private selection = new SelectionModel<DocumentReleveAchatsElement>(true, []);
    
     @Input()
     private releveAchats : ReleveAchats;

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private releveAchatsService: ReleveAchatsService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.releveAchatsService.getDocumentsReleveAchatsByCodeReleve(this.releveAchats.code).subscribe((listDocumentsRelAchats) => {
        this.listDocumentsRelevesAchats= listDocumentsRelAchats;
 //  console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsFacturesVentes  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
  }
  ngOnChanges()
  {
    this.specifyListColumnsToBeDisplayed();
    
    this.releveAchatsService.getDocumentsReleveAchatsByCodeReleve(this.releveAchats.code).subscribe((listDocumentsRelAchats) => {
      this.listDocumentsRelevesAchats = listDocumentsRelAchats;
// console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsCredits  );
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }

  checkOneActionInvoked() {
    const listSelectedDocumentsRelAchats: DocumentReleveAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentRElement) => {
    return documentRElement.code;
  });
  this.listDocumentsRelevesAchats.forEach(documentR => {
    if (codes.findIndex(code => code === documentR.code) > -1) {
      listSelectedDocumentsRelAchats.push(documentR);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsRelevesAchats!== undefined) {

      this.listDocumentsRelevesAchats.forEach(docRel=> {
             listElements.push( {code: docRel.code ,
          dateReception: docRel.dateReception,
          numeroDocument: docRel.numeroDocument,
          typeDocument: docRel.typeDocument,
          description: docRel.description
        } );
      });
    }
      this.dataDocumentsRelevesAchatsSource = new MatTableDataSource<DocumentReleveAchatsElement>(listElements);
      this.dataDocumentsRelevesAchatsSource.sort = this.sort;
      this.dataDocumentsRelevesAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     console.log('calling specifyListColumnsToBeDisplayed documents grid');
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
    
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
      console.log(this.columnsNames);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsRelevesAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsRelevesAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsRelevesAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsRelevesAchatsSource.paginator) {
      this.dataDocumentsRelevesAchatsSource.paginator.firstPage();
    }
  }
  showAddDocumentDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeReleveAchats : this.releveAchats.code
   };
 
   const dialogRef = this.dialog.open(DialogAddDocumentReleveAchatsComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(docAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditDocumentDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeDocumentReleveAchats : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditDocumentReleveAchatsComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(docAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Document Séléctionnée!');
    }
    }
    supprimerDocuments()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(doc, index) {
              this.releveAchatsService.deleteDocumentReleveAchats(doc.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Document Séléctionnée!');
        }
    }
    refresh() {
      this.releveAchatsService.getDocumentsReleveAchatsByCodeReleve(this.releveAchats.code).subscribe((listDocsRelAchats) => {
        this.listDocumentsRelevesAchats = listDocsRelAchats;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
