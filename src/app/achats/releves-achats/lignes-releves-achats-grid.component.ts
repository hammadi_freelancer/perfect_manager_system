
import { Component, OnInit, Inject, ViewChild, Input, AfterViewChecked, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {ReleveAchatsService} from './releve-achats.service';
import { ReleveAchats } from './releve-achats.model';
import {LigneReleveAchats} from './ligne-releve-achats.model';
import { LigneReleveAchatsElement } from './ligne-releve-achats.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
 import { DialogAddLigneReleveAchatsComponent} from './popups/dialog-add-ligne-releve-achats.component';
 import { DialogEditLigneReleveAchatsComponent } from './popups/dialog-edit-ligne-releve-achats.component';
@Component({
    selector: 'app-lignes-releves-achats-grid',
    templateUrl: 'lignes-releves-achats-grid.component.html',
  })
  export class LignesRelevesAchatsGridComponent implements OnInit, OnChanges {
     private listLignesRelevesAchats: LigneReleveAchats[] = [];
     private  dataLignesRelevesAchatsSource: MatTableDataSource<LigneReleveAchatsElement>;
     private selection = new SelectionModel<LigneReleveAchatsElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];

     @Input()
     private releveAchats: ReleveAchats;

     constructor(  private releveAchatsService: ReleveAchatsService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      // console.log('credit code after dipslay tab ', this.credit.code);
      /*this.releveAchatsService.getLignesR(this.factureAchats.code).subscribe((listLignesFactAchats) => {
        this.listLignesFacturesAchats= listLignesFactAchats;
// console.log('recieving data from backend', this.listPayementsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });*/

  }

  ngOnChanges() {
   /* this.factureAchatsService.getLignesFacturesAchatsByCodeFacture(this.factureAchats.code).subscribe((listLignesFactAchats) => {
      this.listLignesFacturesAchats= listLignesFactAchats;
// console.log('recieving data from backend', this.listPayementsCredits  );
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });*/
  }
  checkOneActionInvoked() {
    const listSelectedLignesRelAchats: LigneReleveAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((lFactElement) => {
    return lFactElement.code;
  });
  this.listLignesRelevesAchats.forEach(ligneF => {
    if (codes.findIndex(code => code === ligneF.code) > -1) {
      listSelectedLignesRelAchats.push(ligneF);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listLignesRelevesAchats !== undefined) {
 
      this.listLignesRelevesAchats.forEach(ligneReleveAchats => {
             listElements.push( {
               code: ligneReleveAchats.code ,
              numeroLigneRelAchats: ligneReleveAchats.numeroLigneRelAchats,
          article: ligneReleveAchats.article.code,
          quantite: ligneReleveAchats.quantite,
          prixUnt: ligneReleveAchats.prixUnt,
          unMesure: ligneReleveAchats.unMesure,
          beneficeTotalEstime: ligneReleveAchats.beneficeTotalEstime,
          prixTotal: ligneReleveAchats.prixTotal,
          regle: ligneReleveAchats.regle,
          livre: ligneReleveAchats.livre,
          
        } );
      });
    }

      this.dataLignesRelevesAchatsSource = new MatTableDataSource<LigneReleveAchatsElement>(listElements);
      this.dataLignesRelevesAchatsSource.sort = this.sort;
      this.dataLignesRelevesAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'numeroLigneRelAchats' , displayTitle: 'N°'},
     {name: 'article' , displayTitle: 'Art'}, 
     {name: 'quantite' , displayTitle: 'Qte'},
     {name: 'unMesure' , displayTitle: 'UM'},
      {name: 'prixUnt' , displayTitle: 'P.UN'},
     {name: 'prixTotal' , displayTitle: 'P.Tot'}, 
     {name: 'beneficeTotalEstime' , displayTitle: 'Bene.Est'},
     {name: 'regle' , displayTitle: 'Réglé'},
     {name: 'livre' , displayTitle: 'Livré'},
    
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataLignesRelevesAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataLignesRelevesAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataLignesRelevesAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataLignesRelevesAchatsSource.paginator) {
      this.dataLignesRelevesAchatsSource.paginator.firstPage();
    }
  }
  showAddLigneDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeReleveAchats : this.releveAchats.code
   };
 
   const dialogRef = this.dialog.open(DialogAddLigneReleveAchatsComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(lAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditLigneDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeLigneReleveAchats: this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditLigneReleveAchatsComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(lAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucune Ligne Séléctionnée!');
    }
    }
    supprimerLignes()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(pay, index) {
              this.releveAchatsService.deleteLigneReleveAchats(pay.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar(  ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucune Ligne Séléctionnée!');
        }
    }
    refresh() {
     /* this.releveAchatsService.getLignes(this.factureAchats.code).subscribe((listLignesFacs) => {
        this.listLignesFacturesAchats = listLignesFacs;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });*/
    }
















}
