import { Component, OnInit, Output,ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {ReleveAchats} from './releve-achats.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {ReleveAchatsService} from './releve-achats.service';
// import { ActionData } from './action-data.interface';
import { Fournisseur } from '../../achats/fournisseurs/fournisseur.model';
import { ReleveAchatsElement } from './releve-achats.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DialogAddFournisseurComponent } from '../fournisseurs/dialog-add-fournisseur.component';
import { DialogAddPayementReleveAchatsComponent} from './popups/dialog-add-payement-releve-achats.component';
import { ReleveAchatsDataFilter} from './releve-achats-data.filter';
import { SelectItem } from 'primeng/api';

@Component({
  selector: 'app-releves-achats-grid',
  templateUrl: './releves-achats-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class RelevesAchatsGridComponent implements OnInit {

  private releveAchats: ReleveAchats =  new ReleveAchats();
  private releveAchatsDataFilter = new ReleveAchatsDataFilter();
  private selection = new SelectionModel<ReleveAchatsElement>(true, []);
  private showSelectColumnsMultiSelect = false;
  private showFilter = false;
  private showFilterFournisseur = false;
  
@Output()
select: EventEmitter<ReleveAchats[]> = new EventEmitter<ReleveAchats[]>();

//private lastClientAdded: Client;

 selectedReleveAchats: ReleveAchats ;

 

 @Input()
 private listRelevesAchats: any = [] ;
 private checkedAll: false;

 private  dataRelevesAchatsSource: MatTableDataSource<ReleveAchatsElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
 
 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay :ColumnDefinition[] = [];

 private columnsToSelect : SelectItem[];
  private  selectedColumnsDefinitions: string[];

 private selectedColumnsNames: string[];

  constructor( private route: ActivatedRoute, private releveAchatsService: ReleveAchatsService,
    private router: Router, private matSnackBar: MatSnackBar,
  private  dialog: MatDialog ) {
  }
  ngOnInit() {

    this.columnsToSelect = [
      {label: 'N° Relevé', value: 'numeroReleveAchats', title: 'N° Relevé'},
      {label: 'Date Achats', value: 'dateAchats', title: 'Date Achats'},
      {label: 'Montant à Payer', value: 'montantAPayer', title: 'Montant à Payer'},
      {label: 'Montant Bénéfice', value: 'montantBeneficeEstime', title: 'Montant Bénéfice'},
      {label: 'Réglée', value: 'regleToutes', title: 'Réglée'},
      {label: 'Livrée', value: 'livreToutes', title: 'Livrée'},
      {label: 'Rs.Soc.Fournisseur', value:  'raisonSociale', title: 'Rs.Soc.Fournisseur' },
      {label: 'Matr.Fournisseur', value:  'matriculeFiscale', title: 'Matr.Fournisseur' },
      {label: 'Reg.Fournisseur', value:  'registreCommerce', title: 'Reg.Fournisseur' },
      {label: 'CIN Fournisseur', value: 'cin', title: 'CIN Fournisseur'},  
      {label: 'Nom Fournisseur', value: 'nom', title: 'Nom Fournisseur'},
      {label: 'Prénom Fournisseur', value: 'prenom', title: 'Prénom Fournisseur'},
      {label: 'Etat', value:  'etat', title: 'Etat' },
  
  ];
  this.selectedColumnsDefinitions = [
    'numeroReleveAchats',
    'dateAchats' , 
    'montantAPayer', 
    'nom' ,
    'prenom',
    'cin', 
    'etat'
    ];
    this.defaultColumnsDefinitions = [
      {name: 'numeroReleveAchats' , displayTitle: 'N°Relevé'},
      {name: 'dateAchats' , displayTitle: 'Date Achats'},
      {name: 'montantAPayer' , displayTitle: 'Montant à Payer'},
      {name: 'montantBeneficeEstime' , displayTitle: 'Montant Bénéfice'},
      {name: 'regleToutes' , displayTitle: 'Réglée'},
      {name: 'livreToutes' , displayTitle: 'Livrée'},
       {name: 'nom' , displayTitle: 'Nom Four.'},
      {name: 'prenom' , displayTitle: 'Prénom Four.'},
      {name: 'cin' , displayTitle: 'CIN Four.'},
      {name: 'matriculeFiscale' , displayTitle: 'Matr. Four.'},
      {name: 'raisonSociale' , displayTitle: 'Raison Four.'},
      {name: 'registreCommerce' , displayTitle: 'Registre Four.'},
       {name: 'etat', displayTitle: 'Etat' },
      ];

    this.releveAchatsService.getPageRelevesAchats(0, 5, null).subscribe((releveAchats) => {
      this.listRelevesAchats = releveAchats;
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });


  }
  deleteReleveAchats(releveAchats) {
     //  console.log('call delete !', releveVente );
    this.releveAchatsService.deleteReleveAchats(releveAchats.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData(null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.releveAchatsService.getPageRelevesAchats(0, 5,filter).subscribe((releveAchats) => {
    this.listRelevesAchats = releveAchats;
    this.transformDataToByConsumedByMatTable();
   });
}


checkOneActionInvoked() {
  const listSelectedRelevesAchats: ReleveAchats[] = [];
console.log('selected clients are ');
console.log(this.selection.selected);
if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((relAchatsElement) => {
  return relAchatsElement.code;
});
this.listRelevesAchats.forEach(rAchats => {
  if (codes.findIndex(code => code === rAchats.code) > -1) {
    listSelectedRelevesAchats.push(rAchats);
  }
  });
}
this.select.emit(listSelectedRelevesAchats);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listRelevesAchats !== undefined) {
    this.listRelevesAchats.forEach(releveAchats => {
      listElements.push( {
         code: releveAchats.code ,
         numeroReleveAchats: releveAchats.numeroReleveAchats,
        dateAchats: releveAchats.dateAchats,
        cin: releveAchats.fournisseur.cin,
        nom: releveAchats.fournisseur.nom,
         prenom: releveAchats.fournisseur.prenom,
        matriculeFiscale: releveAchats.fournisseur.matriculeFiscale,
        raisonSociale: releveAchats.fournisseur.raisonSociale ,
        montantAPayer: releveAchats.montantAPayer,
        montantBeneficeEstime: releveAchats.montantBeneficeEstime,
        regleToutes: releveAchats.regleToutes,
        livreToutes: releveAchats.livreToutes,
        etat: releveAchats.etat,
        
    });
  });
    this.dataRelevesAchatsSource = new MatTableDataSource<ReleveAchatsElement>(listElements);
    this.dataRelevesAchatsSource.sort = this.sort;
    this.dataRelevesAchatsSource.paginator = this.paginator;
    this.releveAchatsService.getTotalRelevesAchats(this.releveAchatsDataFilter).subscribe((response) => {
      console.log('Total credits is ', response);
       this.dataRelevesAchatsSource.paginator.length = response.totalRelevesAchats;
     });
}
}
specifyListColumnsToBeDisplayed() {
  
   console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
   this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
 ?
    this.defaultColumnsDefinitions : [];
    this.selectedColumnsNames = [];
    
    if (this.selectedColumnsDefinitions !== undefined)
     {
       this.selectedColumnsDefinitions.forEach((colName) => { 
            const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
            const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
            const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
            this.columnsDefinitionsToDisplay.push(colDef);
           });
     }
    this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
   this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
              return colDef.displayTitle;
    });
    console.log('columns to disp:', this.columnsDefinitionsToDisplay);
    this.columnsNames = [];
    this.columnsNames[0] = 'select';
    this.columnsDefinitionsToDisplay .map((colDef) => {
     this.columnsNames.push(colDef.name);
    if (this.selectedColumnsDefinitions !== undefined) {
     this.selectedColumnsNames.push(colDef.displayTitle);
    }
 });
    this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
     return colDef.displayTitleAr;
 });
 }
masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataRelevesAchatsSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataRelevesAchatsSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataRelevesAchatsSource.filter = filterValue.trim().toLowerCase();
  if (this.dataRelevesAchatsSource.paginator) {
    this.dataRelevesAchatsSource.paginator.firstPage();
  }
}

loadAddReleveAchatsComponent() {
  this.router.navigate(['/pms/achats/ajouterReleveAchats']) ;

}
loadEditReleveAchatsComponent() {
  this.router.navigate(['/pms/achats/editerReleveAchats', this.selection.selected[ this.selection.selected.length - 1].code]) ;

}
supprimerRelevesAchats()
{

  if ( this.selection.selected.length !== 0) {
      this.selection.selected.forEach(function(releveAchats, index) {

   this.releveAchatsService.deleteReleveAchats(releveAchats.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.openSnackBar(  ' Relevé(s) Achats Supprimé(s)');
            this.selection.selected = [];
            this.loadData();
            }
      }, this);
    } else {
      this.openSnackBar('Aucun Relevé Séléctionnée !');
    }
}
refresh() {
  this.loadData(null);
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 addPayement()
 {
  if ( this.selection.selected.length !== 0) {
    
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
    top: '0'
  };
  dialogConfig.data = {
    codeReleveAchats : this.selection.selected[0].code
  };

  const dialogRef = this.dialog.open(DialogAddPayementReleveAchatsComponent,
    dialogConfig
  );
} else {
  this.openSnackBar('Aucun Relevé Séléctionnée !');
}
 }
 history()
 {
   
 }
 addFournisseur()
 {
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
  };

  const dialogRef = this.dialog.open(DialogAddFournisseurComponent,
    dialogConfig
  );
 }
 attacherDocuments()
 {

 }


 changePage($event) {
  console.log('page event is ', $event);
 this.releveAchatsService.getPageRelevesAchats($event.pageIndex, $event.pageSize,
  this.releveAchatsDataFilter ).subscribe((relvesAchats) => {
    this.listRelevesAchats= relvesAchats;
   //  console.log(this.listFacturesVentes);
   // this.transformDataToByConsumedByMatTable();
   const listElements = [];
   this.listRelevesAchats.forEach(relAchats => {
    listElements.push( {code: relAchats.code ,
       numeroReleveAchats: relAchats.numeroReleveAchats,
      dateAchats: relAchats.dateAchats, 
      cin: relAchats.fournisseur.cin,
      nom: relAchats.fournisseur.nom, 
      prenom: relAchats.fournisseur.prenom,
      matriculeFiscale: relAchats.fournisseur.matriculeFiscale, raisonSociale: relAchats.fournisseur.raisonSociale ,
      montantAPayer: relAchats.montantAPayer,
      montantBenefice: relAchats.montantBenefice,
      regleToutes: relAchats.regleToutes,
      livreToutes: relAchats.regleLivre,
      etat: relAchats.etat
  });
});
     this.dataRelevesAchatsSource = new MatTableDataSource<ReleveAchatsElement>(listElements);
  
     this.releveAchatsService.getTotalRelevesAchats(this.releveAchatsDataFilter).subscribe((response) => {
      console.log('Total credits is ', response);
       this.dataRelevesAchatsSource.paginator.length = response.totalRelevesAchats;
     });

    });
}
toggleFilterPanel() {
  this.showFilter = !this.showFilter;
}
toggleShowColumnsMultiSelect() {
 this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
}
loadFiltredData() {
 console.log('the filter is ', this.releveAchatsDataFilter);
 this.loadData(this.releveAchatsDataFilter);
}
activateFilterFournisseur()
{
  this.showFilterFournisseur = !this.showFilterFournisseur;
}


}
