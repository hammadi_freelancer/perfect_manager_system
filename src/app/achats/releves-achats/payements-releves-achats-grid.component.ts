
import { Component, OnInit, Inject, ViewChild, Input, AfterViewChecked, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {ReleveAchatsService} from './releve-achats.service';
import { ReleveAchats } from './releve-achats.model';
import {PayementReleveAchats} from './payement-releve-achats.model';
import { PayementReleveAchatsElement } from './payement-releve-achats.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
 import { DialogAddPayementReleveAchatsComponent} from './popups/dialog-add-payement-releve-achats.component';
 import { DialogEditPayementReleveAchatsComponent } from './popups/dialog-edit-payement-releve-achats.component';
@Component({
    selector: 'app-payements-releves-achats-grid',
    templateUrl: 'payements-releves-achats-grid.component.html',
  })
  export class PayementsRelevesAchatsGridComponent implements OnInit, OnChanges {
     private listPayementsRelevesAchats: PayementReleveAchats[] = [];
     private  dataPayementsRelevesAchatsSource: MatTableDataSource<PayementReleveAchatsElement>;
     private selection = new SelectionModel<PayementReleveAchatsElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];

     @Input()
     private releveAchats: ReleveAchats;

     constructor(  private releveAchatsService: ReleveAchatsService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      // console.log('credit code after dipslay tab ', this.credit.code);
      console.log('the code releve achats is',this.releveAchats.code);
      this.releveAchatsService.getPayementsRelevesAchatsByCodeReleve(this.releveAchats.code).subscribe((listPayementsRAchats) => {
        this.listPayementsRelevesAchats= listPayementsRAchats;
// console.log('recieving data from backend', this.listPayementsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }

  ngOnChanges() {
    console.log('the code releve achats is in ngOnChanges', this.releveAchats.code);
    this.releveAchatsService.getPayementsRelevesAchatsByCodeReleve(this.releveAchats.code).subscribe((listPayementsRAchats) => {
      this.listPayementsRelevesAchats = listPayementsRAchats;
// console.log('recieving data from backend', this.listPayementsCredits  );
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }
  checkOneActionInvoked() {
    const listSelectedPayementsRelAchats: PayementReleveAchats[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((payementRelElement) => {
    return payementRelElement.code;
  });
  this.listPayementsRelevesAchats.forEach(payementR => {
    if (codes.findIndex(code => code === payementR.code) > -1) {
      listSelectedPayementsRelAchats.push(payementR);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listPayementsRelevesAchats !== undefined) {

      this.listPayementsRelevesAchats.forEach(payementRelAchats => {
             listElements.push( {code: payementRelAchats.code ,
          dateOperation: payementRelAchats.dateOperation,
          montantPaye: payementRelAchats.montantPaye,
          modePayement: payementRelAchats.modePayement,
          numeroCheque: payementRelAchats.numeroCheque,
          dateCheque: payementRelAchats.dateCheque,
          compte: payementRelAchats.compte,
          compteAdversaire: payementRelAchats.compteAdversaire,
          banque: payementRelAchats.banque,
          banqueAdversaire: payementRelAchats.banqueAdversaire,
          description: payementRelAchats.description
        } );
      });
    }
      this.dataPayementsRelevesAchatsSource = new MatTableDataSource<PayementReleveAchatsElement>(listElements);
      this.dataPayementsRelevesAchatsSource.sort = this.sort;
      this.dataPayementsRelevesAchatsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'}, 
     {name: 'montantPaye' , displayTitle: 'Montant Payé'},
     {name: 'modePayement' , displayTitle: 'Mode Payement'},
      {name: 'numeroCheque' , displayTitle: 'N°Chèque'},
     {name: 'dateCheque' , displayTitle: 'Date Chèque'}, 
     {name: 'compte' , displayTitle: 'Compte'},
     {name: 'compteAdversaire' , displayTitle: 'Compte Adv.'},
      {name: 'banque' , displayTitle: 'Banque'},
       { name : 'banqueAdversaire' , displayTitle : 'Banque Adv.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataPayementsRelevesAchatsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataPayementsRelevesAchatsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataPayementsRelevesAchatsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataPayementsRelevesAchatsSource.paginator) {
      this.dataPayementsRelevesAchatsSource.paginator.firstPage();
    }
  }

  showAddPayementDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeReleveAchats : this.releveAchats.code
   };
 
   const dialogRef = this.dialog.open(DialogAddPayementReleveAchatsComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(payAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditPayementDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codePayementReleveAchats: this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditPayementReleveAchatsComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(payAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Paiement Séléctionnée!');
    }
    }
    supprimerPayements()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(pay, index) {
              this.releveAchatsService.deletePayementReleveAchats(pay.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Paiement Séléctionnée!');
        }
    }
    refresh() {
      this.releveAchatsService.getPayementsRelevesAchatsByCodeReleve(this.releveAchats.code).subscribe((listPayementsRels) => {
        this.listPayementsRelevesAchats = listPayementsRels;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
















}
