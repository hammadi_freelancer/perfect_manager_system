export interface LigneReleveAchatsElement {
   code: string ;
   numeroLigneRelAchats: string ;
   article: string;
   unMesure: string;
   quantite: number;
   prixUnt: number;
    prixTotal: number;
    beneficeTotalEstime: number;
    regle: boolean;
    livre: boolean;
    }
