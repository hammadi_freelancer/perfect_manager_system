export class ReleveAchatsDataFilter {
    numeroReleveAchats : string;
    montantBenefice : number;
    montantAPayer : number;
    regleToutes : boolean;
    livreToutes: boolean;
    etat : string;
    dateAchatsFromObject : Date;
    dateAchatsToObject : Date;
    nomFournisseur: string;
    prenomFournisseur: string;
    cinFournisseur: string;
    raisonFournisseur: string;
    registreFournisseur: string;
    matriculeFournisseur: string;
    
}


