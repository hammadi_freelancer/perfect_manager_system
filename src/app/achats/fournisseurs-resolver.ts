import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { FournisseurService } from './fournisseurs/fournisseur.service';
import { Fournisseur } from './fournisseurs/fournisseur.model';

@Injectable()
export class FournisseursResolver implements Resolve<Observable<Fournisseur[]>> {
  constructor(private fournisseursService: FournisseurService) { }

  resolve() {
     return this.fournisseursService.getFournisseurs();
}
}
