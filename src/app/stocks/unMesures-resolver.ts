import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { UnMesureService } from './un-mesures/un-mesure.service';
import { UnMesure } from './un-mesures/un-mesure.model';

@Injectable()
export class UnMesuresResolver implements Resolve<Observable<UnMesure[]>> {
  constructor(private unMesuresService: UnMesureService) { }

  resolve() {
     return this.unMesuresService.getUnMesures();
}
}
