// type TypeArticle = 'Marchandise'| 'Produit fini' | 'Matière primaire' | 'Service' | 'Produit semi fini';
type TypeAttribute = 'Color'| 'Chaine' | 'Numero' | '';

export interface SchemaConfigurationElement {
         code: string ;
         description: string;
          attribute1Name?: string;
          attribute2Name?: string;
          attribute3Name?: string;
          attribute4Name?: string;
          attribute1Type?: TypeAttribute;
          attribute2Type?: TypeAttribute;
          attribute3Type?: TypeAttribute;
          attribute4Type?: TypeAttribute;
         configAttributes: string ;
                }
