import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';

import {SchemaConfiguration} from './schema-configuration.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_SCHEMAS_CONFIGURATION = 'http://localhost:8082/stocks/schemasConfiguration/getSchemasConfiguration';
const URL_ADD_SCHEMA_CONFIGURATION = 'http://localhost:8082/stocks/schemasConfiguration/addSchemaConfiguration';
const URL_GET_SCHEMA_CONFIGURATION = 'http://localhost:8082/stocks/schemasConfiguration/getSchemaConfiguration';

const URL_UPDATE_SCHEMA_CONFIGURATION = 'http://localhost:8082/stocks/schemasConfiguration/updateSchemaConfiguration';
const URL_DELETE_SCHEMA_CONFIGURATION = 'http://localhost:8082/stocks/schemasConfiguration/deleteSchemaConfiguration';
// const URL_GET_FILTRED_LIST_MAGASINS  = 'http://localhost:8082/stocks/magasins/getFiltredArticles';
@Injectable({
  providedIn: 'root'
})
export class SchemaConfigurationService {



  constructor(private httpClient: HttpClient) {

   }

   getSchemasConfiguration(): Observable<SchemaConfiguration[]> {
    return this.httpClient
    .get<SchemaConfiguration[]>(URL_GET_LIST_SCHEMAS_CONFIGURATION);
   }
   /*getFiltredArticles(filterArticle): Observable<Article[]> {
    return this.httpClient
    .post<Article[]>(URL_GET_FILTRED_LIST_ARTICLES, filterArticle);
   }*/
   addSchemaConfiguration(schemaConfiguration: SchemaConfiguration): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_SCHEMA_CONFIGURATION, schemaConfiguration);
  }
  updateSchemaConfiguration(schemaConfiguration: SchemaConfiguration): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_SCHEMA_CONFIGURATION, schemaConfiguration);
  }
  deleteSchemaConfiguration(codeSchemaConfiguration): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_SCHEMA_CONFIGURATION + '/' + codeSchemaConfiguration);
  }
  getSchemaConfiguration(codeSchemaConfiguration): Observable<SchemaConfiguration> {
    return this.httpClient.get<SchemaConfiguration>(URL_GET_SCHEMA_CONFIGURATION + '/' + codeSchemaConfiguration);
  }

}
