
import { BaseEntity } from '../../common/base-entity.model' ;
// import { Tranche } from './tranche.model' ;
import { Client } from '../../ventes/clients/client.model' ;
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { ArticleService } from '../../stocks/articles/article.service';
import { SchemaConfiguration } from '../../stocks/schemas-configuration/schema-configuration.model';
// import { UnMesure } from '../../stocks/un-mesures/un-mesure.model';
// import { Magasin } from '../../stocks/magasins/magasin.model';


export class SchemaConfigurationFilter  {
    public selectedCodeSchemaConfiguration: string;
    

    private controlCodesSchemasConfiguration = new FormControl();
    private controlLibellesSchemaConfiguration = new FormControl();

    
   
    
   //  private controlAdresse = new FormControl();
    private listCodesSchemasConfiguration: string[] = [];
 
    
    private listLibellesSchemasConfigurations: string[] = [];
  

    private filteredCodesSchemasConfigurations: Observable<string[]>;
    private filteredLibellesSchemasConfigurations: Observable<string[]>;
    
   
    
    constructor ( schemasConfigurations: SchemaConfiguration[], ) {
      if (schemasConfigurations !== null && schemasConfigurations !== undefined ) {
       
        this.listCodesSchemasConfiguration = schemasConfigurations.map((schemaConfiguration) => (schemaConfiguration.code));
        this.listLibellesSchemasConfigurations = schemasConfigurations.filter((schemaConfiguration) =>
        (schemaConfiguration.libelle !== undefined && schemaConfiguration.libelle != null )).
        map((schemaConfiguration) => (schemaConfiguration.libelle ));
      }

      this.filteredCodesSchemasConfigurations = this.controlCodesSchemasConfiguration.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterCodesSchemasConfiguration(value))
        );
        this.filteredLibellesSchemasConfigurations = this.controlLibellesSchemaConfiguration.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterLibellesSchemasConfiguration(value))
        );
       
     
 
    }
  
    private _filterCodesSchemasConfiguration(value: string): string[] {
      if(value !== undefined) {
        const filterValue = value.toLowerCase();
        return this.listCodesSchemasConfiguration.filter(codeSch => codeSch.toLowerCase().includes(filterValue));
      } else {
      return this.listCodesSchemasConfiguration;
      }
      
      }
      private _filterLibellesSchemasConfiguration(value: string): string[] {
        if(value !== undefined) {
        const filterValue = value.toLowerCase();
        return this.listLibellesSchemasConfigurations.filter(libelleSch => libelleSch.toLowerCase().includes(filterValue));
        } else {
          return this.listLibellesSchemasConfigurations;
          }
      }
     
 
      public getControlCodesSchemasConfigurations(): FormControl {
        return this.controlCodesSchemasConfiguration;
      }
   
      public getControlLibelleSchemasConfiguration(): FormControl {
        return this.controlLibellesSchemaConfiguration;
      }
  


      public getFiltredCodesSchemasConfigurations(): Observable<string[]> {
        return this.filteredCodesSchemasConfigurations;
      }
      public getFiltredLibellesSchemasConfiguration(): Observable<string[]> {
        return this.filteredLibellesSchemasConfigurations;
      }
      
       
}





