import { SchemaConfiguration } from './schema-configuration.model';
export interface ActionData {
       actionName?: string;
     selectedSchemasConfiguration?: SchemaConfiguration[];
}

