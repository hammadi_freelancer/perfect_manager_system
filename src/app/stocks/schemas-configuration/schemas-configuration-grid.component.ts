import { Component, OnInit, Output, ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {SchemaConfiguration} from './schema-configuration.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {SchemaConfigurationService} from './schema-configuration.service';
import { ActionData } from './action-data.interface';
// import { Fournisseur } from '../../ventes/clients/client.model';
import { SchemaConfigurationElement } from './schema-configuration.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
// import { AlertMessage } from '../../common/alert-message.interface';
// import { OperationRequest } from '../../common/operation-request.interface';
// import { OperationResponse } from '../../common/operation-response.interface';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-schemas-configuration-grid',
  templateUrl: './schemas-configuration-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class SchemasConfigurationGridComponent implements OnInit {

  private schemaConfiguration: SchemaConfiguration =  new SchemaConfiguration();
  private selection = new SelectionModel<SchemaConfigurationElement>(true, []);
  
@Output()
select: EventEmitter<SchemaConfiguration[]> = new EventEmitter<SchemaConfiguration[]>();

//private lastClientAdded: Client;

 selectedSchemaConfiguration: SchemaConfiguration ;

 @Input()
 private actionData: ActionData;

 @Input()
 private listSchemasConfiguration: any = [] ;
 listSchemasConfigurationOrgin: any = [];
 private checkedAll: false;
 private  dataSchemasConfigurationSource: MatTableDataSource<SchemaConfigurationElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
  constructor( private route: ActivatedRoute, private schemaConfigurationService: SchemaConfigurationService,
    private router: Router, private matSnackBar: MatSnackBar ) {
  }
  ngOnInit() {
    this.schemaConfigurationService.getSchemasConfiguration().subscribe((schemasConfiguration) => {
      this.listSchemasConfiguration = schemasConfiguration;
      // this.listClients[this.listClients.length] = this.lastClientAdded;
      // this.listClientsOrgin = this.listClients ;
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });

  }
  deleteSchemaConfiguration(schemaConfiguration) {
      console.log('call delete !', schemaConfiguration );
    this.schemaConfigurationService.deleteSchemaConfiguration(schemaConfiguration.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData();

      } else {
       // show error message
      }
     });

  }
loadData() {
  this.schemaConfigurationService.getSchemasConfiguration().subscribe((schemasConfiguration) => {
    this.listSchemasConfiguration = schemasConfiguration;
    // this.listClients[this.listClients.length] = this.lastClientAdded;
    this.listSchemasConfigurationOrgin = this.listSchemasConfiguration;
    this.transformDataToByConsumedByMatTable();
   });
  // this.listClientsOrgin = this.listClients ;
}


checkOneActionInvoked() {
  const listSelectedSchemasConfiguration: SchemaConfiguration[] = [];
console.log('selected unMesures are ');
console.log(this.selection.selected);
if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((schemaConfigurationElement) => {
  return schemaConfigurationElement.code;
});
this.listSchemasConfiguration.forEach(schemaConfiguration => {
  if (codes.findIndex(code => code === schemaConfiguration.code) > -1) {
    listSelectedSchemasConfiguration.push(schemaConfiguration);
  }
  });
}
this.select.emit(listSelectedSchemasConfiguration);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listSchemasConfiguration !== undefined) {
    this.listSchemasConfiguration.forEach(schemaConfiguration => {
      listElements.push( {code: schemaConfiguration.code ,
       description: schemaConfiguration.description,
       configAttributes: schemaConfiguration.configAttributes
     } );
    });
  }
    this.dataSchemasConfigurationSource = new MatTableDataSource<SchemaConfigurationElement>(listElements);
    this.dataSchemasConfigurationSource.sort = this.sort;
    this.dataSchemasConfigurationSource.paginator = this.paginator;
}
 specifyListColumnsToBeDisplayed() {
   this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
   {name: 'description' , displayTitle: 'Déscription'},
   {name: 'configAttributes' , displayTitle: 'Caractéristiques'}
   ];
   this.columnsTitles = this.columnsDefinitions.map((colDef) => {
             return colDef.displayTitle;
   });
   this.columnsNames[0] = 'select';
   this.columnsDefinitions.map((colDef) => {
    this.columnsNames.push(colDef.name);
});
   this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
    return colDef.displayTitleAr;
});
}
masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataSchemasConfigurationSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataSchemasConfigurationSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataSchemasConfigurationSource.filter = filterValue.trim().toLowerCase();
  if (this.dataSchemasConfigurationSource.paginator) {
    this.dataSchemasConfigurationSource.paginator.firstPage();
  }
}
loadAddSchemaConfigurationComponent() {
  this.router.navigate(['/pms/stocks/ajouterSchemaConfiguration']) ;

}
loadEditSchemaConfigurationComponent() {
  if (this.selection.selected.length === 0) {
    this.openSnackBar(' Aucun élément sélectionné' );
    // this.loadData();
    } else
   {
  this.router.navigate(['/pms/stocks/editerSchemaConfiguration', this.selection.selected[ this.selection.selected.length - 1].code]) ;
    }
}
supprimerSchemasConfiguration()
{
  console.log('NBRE OF ELEMENTS SELECTED');
  console.log(this.selection.selected.length);
  console.log(' ELEMENTS SELECTED');
  console.log(this.selection.selected);
  if ( this.selection.selected.length !== 0) {
      this.selection.selected.forEach(function(schemaConfiguration, index) {
          this.schemaConfigurationService.deleteSchemaConfiguration(schemaConfiguration.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.openSnackBar( this.selection.selected.length + ' Element(s) Supprimé(s)');
             this.loadData();
            }
      }, this);
    } else {
      this.openSnackBar('No Elements Selected');
    }
}
refresh()
{
  this.loadData();

}
  openSnackBar(messageToDisplay) {
 this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
     verticalPosition: 'top'});
}
}
