

import { BaseEntity } from '../../common/base-entity.model' ;

// type TypeArticle = 'Marchandise'| 'ProduitFini' | 'MatierePrimaire' | 'Service' | 'ProduitSemiFini';
 type TypeAttribute = 'Color'| 'Chaine' | 'Numero' | '';

export class SchemaConfiguration extends BaseEntity {
   constructor(public code?: string,  
    public libelle?: string,
    public configAttributes?: string,
    public attribute1Name?: string,
    public attribute2Name?: string,
    public attribute3Name?: string,
    public attribute4Name?: string,
    public attribute1Type?: TypeAttribute,
    public attribute2Type?: TypeAttribute,
    public attribute3Type?: TypeAttribute,
    public attribute4Type?: TypeAttribute,
    
) {
    super();
   //  this.categoryClient = 'PERSONNE_PHYSIQUE'; // other possible value : PERSONNE_MORALE
    // this.checked = false;
}
}
