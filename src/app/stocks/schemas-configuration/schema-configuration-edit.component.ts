import { Component, OnInit, Input, EventEmitter, Output, AfterContentChecked,
AfterViewChecked
} from '@angular/core';
// import { ArticleService } from './article.service';
import {SchemaConfiguration} from './schema-configuration.model';
import {SchemaConfigurationService} from './schema-configuration.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {ActionData} from './action-data.interface';
import {MatSnackBar} from '@angular/material';



@Component({
  selector: 'app-schema-configuration-edit',
  templateUrl: './schema-configuration-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class SchemaConfigurationEditComponent implements OnInit, AfterContentChecked {

// private client: Client = new Client();


private schemaConfiguration: SchemaConfiguration = new SchemaConfiguration();

// @Input()
// private listClients: any = [] ;

@Input()
private actionData: ActionData;


private file: any;

  constructor( private route: ActivatedRoute,
    private router: Router , private schemaConfigurationService: SchemaConfigurationService, private matSnackBar: MatSnackBar,
   ) {
  }
  ngOnInit() {
 
      const codeSchemaConfiguration = this.route.snapshot.paramMap.get('codeSchemaConfiguration');
      this.schemaConfigurationService.getSchemaConfiguration(codeSchemaConfiguration).subscribe((schemaConfiguration) => {
               this.schemaConfiguration = schemaConfiguration;
      });
      // this.client = this.actionData.selectedClients[0];
     
  }
  loadAddSchemaConfigurationComponent() {
    this.router.navigate(['/pms/stocks/ajouterSchemaConfiguration']) ;
  
  }
  loadGridSchemasConfiguration() {
    this.router.navigate(['/pms/stocks/schemasConfiguration']) ;
  }
  supprimerSchemaConfiguration() {
     
      this.schemaConfigurationService.deleteSchemaConfiguration(this.schemaConfiguration.code).subscribe((response) => {

        this.openSnackBar( ' Element Supprimé');
        this.loadGridSchemasConfiguration() ;
            });
  }
updateSchemaConfiguration() {
  console.log(this.schemaConfiguration);
  this.schemaConfigurationService.updateSchemaConfiguration(this.schemaConfiguration).subscribe((response) => {
    if (response.error_message ===  undefined) {
      console.log('the response of update is ', response );
      this.openSnackBar( ' Schémas mis à jour ');
      
      //  this.save.emit(response);
      // this.client = new Client();
      // this.modeUpdate = false;
    } else {
      this.openSnackBar( ' Erreurs.. ');
      
     // show error message
    }
});
}
fileChanged($event) {
  this.file = $event.target.files[0];
  const fileReader = new FileReader();
  fileReader.onload = (e) => {
    console.log(fileReader.result);
     // this.unMesure.image = fileReader.result;
    // this.filesData[indexFile] = fileData;
  };
  fileReader.readAsDataURL(this.file);
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

ngAfterContentChecked() {

}
}


