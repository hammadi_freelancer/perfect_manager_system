import { Component, OnInit, Input, EventEmitter, Output,
    AfterViewChecked
    } from '@angular/core';
    // import { ArticleService } from './article.service';
    import {SchemaConfiguration} from './schema-configuration.model';
    import {SchemaConfigurationService} from './schema-configuration.service';
    import {FormControl} from '@angular/forms';
    import {map, startWith} from 'rxjs/operators';
    import {Observable} from 'rxjs';
    import { Router, ActivatedRoute, ParamMap } from '@angular/router';
    import {ActionData} from './action-data.interface';
    import {MatSnackBar} from '@angular/material';
   //  import { TypeArticle } from './type-article.interface';
    
    @Component({
      selector: 'app-schema-configuration-new',
      templateUrl: './schema-configuration-new.component.html',
      // styleUrls: ['./players.component.css']
    })
export class  SchemaConfigurationNewComponent implements OnInit {
    private schemaConfiguration: SchemaConfiguration = new SchemaConfiguration();
    @Input()
    private listSchemasConfiguration: any = [] ;
    @Input()
    private actionData: ActionData;
    private file: any;

   /* private typesArticle: TypeArticle[] = [
      {value: 'Marchandise', viewValue: 'Marchandise'},
      {value: 'ProduitFini', viewValue: 'Produit Fini'},
      {value: 'ProduitSemiFini', viewValue: 'Produit Semi Fini'},
      {value: 'Service', viewValue: 'Service'},
      {value: 'MatierePrimaire', viewValue: 'Matière Primaire'}
    ];*/
      constructor( private route: ActivatedRoute,
        private router: Router , private schemaConfigurationService: SchemaConfigurationService, private matSnackBar: MatSnackBar ) {
      }
      ngOnInit() {
     
          console.log('the action data is ');
          console.log(this.actionData);
      }
    saveSchemaConfiguration() {
       
        console.log(this.schemaConfiguration);
        this.schemaConfigurationService.addSchemaConfiguration(this.schemaConfiguration).subscribe((response) => {
            if (response.error_message ===  undefined) {
              // this.save.emit(response);
              this.schemaConfiguration = new SchemaConfiguration();
              this.openSnackBar(  'Schémas Enregistré');
            } else {
             // show error message
             this.openSnackBar(  'Erreurs!');
             
            }
        });
    }
    loadGridSchemasConfiguration() {
      this.router.navigate(['/pms/stocks/schemasConfiguration']) ;
    }
    fileChanged($event) {
        this.file = $event.target.files[0];
        const fileReader = new FileReader();
        fileReader.onload = (e) => {
          console.log(fileReader.result);
          // this.schemaConfiguration.image = fileReader.result;
          // this.filesData[indexFile] = fileData;
        };
        fileReader.readAsDataURL(this.file);
       }
       vider() {
         this.schemaConfiguration = new SchemaConfiguration();
       }
       openSnackBar(messageToDisplay) {
        this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
            verticalPosition: 'top'});
       }
 
    }
