

import { BaseEntity } from '../../common/base-entity.model' ;
import { SchemaConfiguration } from '../schemas-configuration/schema-configuration.model';
import { ClasseArticle } from '../classes-articles/classe-article.model';
import { Magasin } from '../magasins/magasin.model';
import { UnMesure } from '../un-mesures/un-mesure.model';
import { DataStock } from './data-stock.model';

type TypeArticle = 'Marchandise'| 'ProduitFini' | 'MatierePrimaire' | 'Service' | 'ProduitSemiFini' |'';
type TypeModeAchat = 'Credit'| 'Espece' | 'Cheque' | 'Mixte'  |'';
type TypeModeVente = 'Credit'| 'Espece' | 'Cheque' | 'Mixte' | '';
type TypeModeDisponibilite = 'Immediate' | 'CourtTerme ' | 'LongTerme'  | 'SurCommande'  |'' ; 
type TypeAcquisition = 'Interne' | 'Externe' |'';
type TypeVivialite = 'ArticleMort' | 'ArticleCourant'  |'ArticleRisque' | '';
type TypeLegetimite = 'Legetime' | 'NonLegetime' | '';

type TypeSecteur = 'Organise' |'Noir' |'';
export class Article extends BaseEntity {
   constructor(
    public code?: string, 
    public libelle?: string,
    public designation?: string,
    public barCode?: string,

    public dateExpiration?: string,
    public dateExpirationObject?: Date,
    public dateFabrication?: string, 
    public  dateFabricationObject?: Date,
    public dateAlimentationStock?: string,
    public dateAlimentationStockObject?: Date, 
    public dateAchats?: string,
    public dateAchatsObject?: Date,
    public dateVentes?: string,
    public dateVentesObject?: Date, 
    public dateProduction?: string,
    public dateProductionObject?: Date,

    public marque?: string,

     public importe?: boolean,
    public origine?: string,
    public disponibilite?: TypeModeDisponibilite,
    public acquisition?: TypeAcquisition,
    public vivialite?: TypeVivialite,
    public legetimite?: TypeLegetimite,
    public conditionsStockage?: string,
    public schemaConfiguration?: SchemaConfiguration,
    public classe?: ClasseArticle,
    public sousClasse?: ClasseArticle,

    public magasins?: Magasin[],
    public lots?: ClasseArticle,
    public unMesures?: ClasseArticle,
    
    
   /* public quantiteVendue?: number,
    public quantiteAchetee?: number,
    public quantiteStocke?: number,
    
    public quantiteRecieved?: number,
    public quantiteCommande?: number,
    public attenteReception?: number,
    public quantiteLivre?: number,
    public quantiteCritique?: number,
    public attenteLivraison?: number,
    public quantiteDemande?: number,*/
    public dataStock?: DataStock,
    public prixVentes?: number,
    public prixAchats?: number,
    
    
     public gestionLots?: boolean, 
     public type?: TypeArticle,
    public composed?: boolean, public gestionMagasins?: boolean,
     public imageFace1?: any,
     public imageFace2?: any,
    public codesMagasins?: string[], public codesUnMesures?: string[],
   // public codeSchemaConfiguration?: string,
    public configuration?: string,
    public codesArticlesSecondaires?: string[],
    public maxQuantiteAchete?: number,
    public modeAchat?: TypeModeAchat,
    public modeVente?: TypeModeVente,
    public fournisseurFidele?: boolean,
    public clientFidele?: boolean,
    public modeDisponibiliteAchat?: TypeModeDisponibilite,
    public secteur?:  TypeSecteur,
  
    public maxQuantiteVendue?: number,
    public pourcentageTVAVentes?: number,
    public montantBenefice?: number,
    public pourcentageBenefice?: number,
    public pourcentageBeneficeEstime?: number,
    
    public pourcentageTVAAchats?: number,
    
    public listClientsInclus?: string[],
    public listClientsExclus?: string[],
    public listFournisseursInclus?: string[],
    public listFournisseursExclus?: string[],


) {
    super();
   //  this.categoryClient = 'PERSONNE_PHYSIQUE'; // other possible value : PERSONNE_MORALE
    // this.checked = false;
    this.imageFace1 = undefined;
    this.imageFace2 = undefined;
     this.classe = new ClasseArticle();
     this.sousClasse = new ClasseArticle();
     this.schemaConfiguration = new SchemaConfiguration();
     this.dataStock = new DataStock();
     this.pourcentageTVAVentes = Number(0);
     this.pourcentageTVAAchats = Number(0);
     
    /* this.quantiteVendue = Number(0);
     this.quantiteAchetee = Number(0);
     this.quantiteVendue = Number(0);
     this.quantiteStocke = Number(0);
     this.quantiteRecieved = Number(0);
     this.quantiteCommande = Number(0);
     this.quantiteDemande = Number(0);
     
     this.quantiteLivre = Number(0);
     this.quantiteCritique = Number(0);
     this.attenteLivraison = Number(0);
     this.attenteReception = Number(0);*/

     this.prixVentes = Number(0);
     this.prixAchats = Number(0);

    // this.codesMagasins = [];
}
}
