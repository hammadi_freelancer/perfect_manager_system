

type TypeArticle = 'Marchandise'| 'Produit fini' | 'Matière primaire' | 'Service' | 'Produit semi fini';

export class ArticleDataFilter {
         code: string ;
         libelle: string ;
          designation: string;
          barCode: string;
          codeClasse: string;
          pourcentageBenefice: number;
          marque: string;
          reference: string;
          etat: string;
          prixVentes: number;
         type: string;
        }




