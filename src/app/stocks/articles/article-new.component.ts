import { Component, OnInit, Input, EventEmitter, Output, ViewChild,
    AfterViewChecked
    } from '@angular/core';
    // import { ArticleService } from './article.service';
    import {Article} from './article.model';
    import {ArticleService} from './article.service';
    import {FormControl} from '@angular/forms';
    import {map, startWith} from 'rxjs/operators';
    import {Observable} from 'rxjs';
    import { Router, ActivatedRoute, ParamMap } from '@angular/router';
    import {MatSnackBar} from '@angular/material';
    import { TypeArticle } from './type-article.interface';
    import { ClasseArticleFilter } from '../classes-articles/classe-article.filter';
    import { SchemaConfigurationFilter } from '../schemas-configuration/schemas-configuration.filter';
    import { UnMesureFilter } from '../un-mesures/un-mesures.filter';
    import { MatExpansionPanel } from '@angular/material';
    
    @Component({
      selector: 'app-article-new',
      templateUrl: './article-new.component.html',
      // styleUrls: ['./players.component.css']
    })
export class ArticleNewComponent implements OnInit {
    private article: Article = new Article();
    @Input()
    private listArticles: any = [] ;

    @ViewChild('1stPanel')
    private firstMatExpansionPanel : MatExpansionPanel;

    private file: any;

    private classeArticleFilter : ClasseArticleFilter;
    private schemaConfigurationFilter : SchemaConfigurationFilter;
    private unMesureFilter : UnMesureFilter;
    
    private typesArticle: TypeArticle[] = [
      {value: 'Marchandise', viewValue: 'Marchandise'},
      {value: 'ProduitFini', viewValue: 'Produit Fini'},
      {value: 'ProduitSemiFini', viewValue: 'Produit Semi Fini'},
      {value: 'Service', viewValue: 'Service'},
      {value: 'MatierePrimaire', viewValue: 'Matière Primaire'}
    ];
    private disponibilitesArticles  : any[] = [
      {value: 'Immediate', viewValue: 'Immédiate'},
      {value: 'CourtTerme', viewValue: 'à Court Terme'},
      {value: 'LongTerme', viewValue: 'à Long Terme'},
      {value: 'SurCommande', viewValue: 'Sur Commande'},
    ];
    
    private vivialitesArticles : any[] = [
      {value: 'ArticleMort', viewValue: 'Article Mort'},
      {value: 'ArticleCourant', viewValue: 'Article Courant'},
      {value: 'ArticleRisque', viewValue: 'Article Risqué'},
      {value: 'ArticleDomage', viewValue: 'Article Domagé'}
    ];
    
    
    private legetimitesArticles : any[] = [
      {value: 'Legetime', viewValue: 'Légétime'},
      {value: 'NonLegetime', viewValue: 'NonLégétime'},
    ];
   
      constructor( private route: ActivatedRoute,
        private router: Router , private articleService: ArticleService, private matSnackBar: MatSnackBar ) {
      }
      ngOnInit() {
        this.classeArticleFilter = new 
        ClasseArticleFilter(this.route.snapshot.data.classesArticles);
        this.schemaConfigurationFilter= new 
        SchemaConfigurationFilter(this.route.snapshot.data.schemasConfiguration);
        this.unMesureFilter= new 
        UnMesureFilter(this.route.snapshot.data.unMesures);
        this.firstMatExpansionPanel.open();
        
      }
    saveArticle() {
       
        console.log(this.article);
        this.articleService.addArticle(this.article).subscribe((response) => {
            if (response.error_message ===  undefined) {
              // this.save.emit(response);
              this.article = new Article();
              this.openSnackBar(  'Article Enregistré');
            } else {
             // show error message
             this.openSnackBar(  'Erreurs!');
             
            }
        });
    }
    loadGridArticles() {
      this.router.navigate(['/pms/stocks/articles']) ;
    }
    fileChangedF1($event) {
        this.file = $event.target.files[0];
        const fileReader = new FileReader();
        fileReader.onload = (e) => {
          console.log(fileReader.result);
           this.article.imageFace1 = fileReader.result;
          // this.filesData[indexFile] = fileData;
        };
        fileReader.readAsDataURL(this.file);
       }
       fileChangedF2($event) {
        this.file = $event.target.files[0];
        const fileReader = new FileReader();
        fileReader.onload = (e) => {
          console.log(fileReader.result);
           this.article.imageFace2 = fileReader.result;
          // this.filesData[indexFile] = fileData;
        };
        fileReader.readAsDataURL(this.file);
       }
       vider() {
         this.article = new Article();
       }
       openSnackBar(messageToDisplay) {
        this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
            verticalPosition: 'top'});
       }
 
    }
