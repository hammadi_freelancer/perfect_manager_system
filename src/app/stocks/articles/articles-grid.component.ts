import { Component, OnInit, Output, ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {Article} from './article.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {ArticleService} from './article.service';
import { ActionData } from './action-data.interface';
// import { Fournisseur } from '../../ventes/clients/client.model';
import { ArticleElement } from './article.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
// import { AlertMessage } from '../../common/alert-message.interface';
// import { OperationRequest } from '../../common/operation-request.interface';
// import { OperationResponse } from '../../common/operation-response.interface';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import { ArticleDataFilter } from './article-data.filter';
import { SelectItem } from 'primeng/api';

@Component({
  selector: 'app-articles-grid',
  templateUrl: './articles-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class ArticlesGridComponent implements OnInit {

  private article: Article =  new Article();
  private showFilter = false;
  private showSelectColumnsMultiSelect = false;
  private articleDataFilter = new ArticleDataFilter();
  private selection = new SelectionModel<ArticleElement>(true, []);
  
@Output()
select: EventEmitter<Article[]> = new EventEmitter<Article[]>();

//private lastClientAdded: Client;

 selectedArticle: Article ;

 @Input()
 private actionData: ActionData;

 @Input()
 private listArticles: any = [] ;
 listArticlesOrgin: any = [];
 private checkedAll: false;
 private  dataArticlesSource: MatTableDataSource<ArticleElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay :ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
 
 private columnsToSelect : SelectItem[];
  private  selectedColumnsDefinitions: string[];
 private selectedColumnsNames: string[];



  constructor( private route: ActivatedRoute, private articlesService: ArticleService,
    private router: Router, private matSnackBar: MatSnackBar ) {
  }
  ngOnInit() {
    this.columnsToSelect = [
      {label: 'Code', value: 'code', title: 'Code'},
      {label: 'Libéllé', value: 'libelle', title: 'Libéllé'},
      {label: 'Désignation', value: 'designation', title: 'Désignation'},
      {label: 'Marque', value: 'marque', title: 'Marque'},
      {label: 'Référence', value: 'reference', title: 'Référence'},
      {label: 'Déscription', value: 'description', title: 'Déscription'},
      {label: 'Type', value: 'type', title: 'type'},
  ];
  this.selectedColumnsDefinitions = [
    'code',
   'libelle',
   'designation' ,
   'marque', 
   'reference', 
   'description',
    'type',
    ];
    this.defaultColumnsDefinitions = [
      {name: 'code' , displayTitle: 'Code'},
      {name: 'libelle' , displayTitle: 'Libéllé'},
       {name: 'designation' , displayTitle: 'Désignation'},
      {name: 'marque' , displayTitle: 'Marque'},
      {name: 'reference' , displayTitle: 'Référence'},
      {name: 'description' , displayTitle: 'Déscription'},
      {name: 'type' , displayTitle: 'Type'}
    ] ;

    this.articlesService.getPageArticles(0, 5, null).subscribe((articles) => {
      this.listArticles = articles;
      // this.listClients[this.listClients.length] = this.lastClientAdded;
      // this.listClientsOrgin = this.listClients ;
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
   
  });
   /* this.creditsService.getCredits().subscribe((credits) => {
      console.log('list credits ', credits);
      // this.listCredits = credits;
      this.listCredits =  this.listCredits.map((credit) => {
        if (credit.client === 'undefined' || credit.client === null ) {
           credit.client = new Client();
         }
         return credit;
   });      this.listCreditsOrgin = this.listCredits ;
     });*/
    // this.listClientsOrgin = this.listClients ;

  }
  deleteArticle(article) {
      console.log('call delete !', article );
    this.articlesService.deleteArticle(article.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData(null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.articlesService.getPageArticles(0,5,filter).subscribe((articles) => {
    this.listArticles = articles;
    // this.listClients[this.listClients.length] = this.lastClientAdded;
    this.listArticlesOrgin = this.listArticles;
    this.transformDataToByConsumedByMatTable();
   });
  // this.listClientsOrgin = this.listClients ;
}


checkOneActionInvoked() {
  const listSelectedArticles: Article[] = [];
console.log('selected articles are ');
console.log(this.selection.selected);
if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((articleElement) => {
  return articleElement.code;
});
this.listArticles.forEach(article => {
  if (codes.findIndex(code => code === article.code) > -1) {
    listSelectedArticles.push(article);
  }
  });
}
this.select.emit(listSelectedArticles);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listArticles !== undefined) {
    this.listArticles.forEach(article => {
      listElements.push(
         {code: article.code ,
        libelle: article.libelle,
        description: article.description,
        designation: article.designation,
        marque: article.marque,
        reference: article.reference,
        type: article.type} );
    });
  }
    this.dataArticlesSource = new MatTableDataSource<ArticleElement>(listElements);
    this.dataArticlesSource.sort = this.sort;
    this.dataArticlesSource.paginator = this.paginator;
}

specifyListColumnsToBeDisplayed() {
  
   console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
   this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
 ?
    this.defaultColumnsDefinitions : [];
    this.selectedColumnsNames = [];
    
    if (this.selectedColumnsDefinitions !== undefined)
     {
       this.selectedColumnsDefinitions.forEach((colName) => { 
            const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
            const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
            const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
            this.columnsDefinitionsToDisplay.push(colDef);
           });
     }
    this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
   this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
              return colDef.displayTitle;
    });
    console.log('columns to disp:', this.columnsDefinitionsToDisplay);
    this.columnsNames = [];
    this.columnsNames[0] = 'select';
    this.columnsDefinitionsToDisplay .map((colDef) => {
     this.columnsNames.push(colDef.name);
    if (this.selectedColumnsDefinitions !== undefined) {
     this.selectedColumnsNames.push(colDef.displayTitle);
    }
 });
    this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
     return colDef.displayTitleAr;
 });
 }






masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataArticlesSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataArticlesSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataArticlesSource.filter = filterValue.trim().toLowerCase();
  if (this.dataArticlesSource.paginator) {
    this.dataArticlesSource.paginator.firstPage();
  }
}
loadAddArticleComponent() {
  this.router.navigate(['/pms/stocks/ajouterArticle']) ;

}
loadEditArticleComponent() {
  if (this.selection.selected.length === 0) {
    this.openSnackBar(' Aucun élément sélectionné' );
    // this.loadData();
    } else
   {
  this.router.navigate(['/pms/stocks/editerArticle', this.selection.selected[ this.selection.selected.length - 1].code]) ;
    }
}
supprimerArticles()
{
  console.log('NBRE OF ELEMENTS SELECTED');
  console.log(this.selection.selected.length);
  console.log(' ELEMENTS SELECTED');
  console.log(this.selection.selected);
  if ( this.selection.selected.length !== 0) {
      this.selection.selected.forEach(function(article, index) {
          this.articlesService.deleteArticle(article.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.openSnackBar( this.selection.selected.length + ' Element(s) Supprimé(s)');
             this.loadData();
            }
      }, this);
    } else {
      this.openSnackBar('Aucun Article Séléctionné');
    }
}
refresh()
{
  this.loadData(null);

}
  openSnackBar(messageToDisplay) {
 this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
     verticalPosition: 'top'});
}


changePage($event) {
 this.articlesService.getPageArticles($event.pageIndex, $event.pageSize,this.articleDataFilter ).
 subscribe((articles) => {
    this.listArticles = articles;
   //  console.log(this.listFournisseurs);
   // this.transformDataToByConsumedByMatTable();
   const listElements = [];
 if (this.listArticles !== undefined) {
    this.listArticles.forEach(article => {
      listElements.push(
         {code: article.code ,
        libelle: article.libelle,
        description: article.description,
        designation: article.designation,
        marque: article.marque,
        reference: article.reference,
        type: article.type} );
    }
  );
  }
     this.dataArticlesSource = new MatTableDataSource<ArticleElement>(listElements);
     this.articlesService.getTotalArticles(this.articleDataFilter).subscribe((response) => {
     //  console.log('Total credits is ', response);
      this.dataArticlesSource.paginator = this.paginator;
       this.dataArticlesSource.paginator.length = response.totalClients;
     });

    });
}



toggleFilterPanel() {
   this.showFilter = !this.showFilter;
}
toggleShowColumnsMultiSelect() {
  this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
}
loadFiltredData() {
  console.log('the filter is ', this.articleDataFilter);
  this.loadData(this.articleDataFilter);
}


addMagasin()
{

}

addUnMesure()
{
  
}

}
