import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';
import { HttpParams} from '@angular/common/http';

import {Article} from './article.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_ARTICLES = 'http://localhost:8082/stocks/articles/getArticles';
const URL_ADD_ARTICLE = 'http://localhost:8082/stocks/articles/addArticle';
const URL_GET_ARTICLE = 'http://localhost:8082/stocks/articles/getArticle';
const URL_GET_PAGE_ARTICLES = 'http://localhost:8082/stocks/articles/getPageArticles';
const URL_GET_TOTAL_ARTICLES = 'http://localhost:8082/stocks/articles/getTotalArticles';

const URL_UPDATE_ARTICLE = 'http://localhost:8082/stocks/articles/updateArticle';
const URL_DELETE_ARTICLE = 'http://localhost:8082/stocks/articles/deleteArticle';
const URL_GET_FILTRED_LIST_ARTICLES  = 'http://localhost:8082/stocks/articles/getFiltredArticles';
@Injectable({
  providedIn: 'root'
})
export class ArticleService {



  constructor(private httpClient: HttpClient) {

   }

   getTotalArticles(filter): Observable<any> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('filter', filterStr);
    return this.httpClient
    .get<any>(URL_GET_TOTAL_ARTICLES, {params});
   }

   getPageArticles(pageNumber, nbElementsPerPage, filter): Observable<Article[]> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('pageNumber', pageNumber)
    .set('nbElementsPerPage', nbElementsPerPage)
    .set('filter', filterStr);
    return this.httpClient
    .get<Article[]>(URL_GET_PAGE_ARTICLES, {params});
   }
   getArticles(): Observable<Article[]> {
    return this.httpClient
    .get<Article[]>(URL_GET_LIST_ARTICLES);
   }
   getFiltredArticles(filterArticle): Observable<Article[]> {
    return this.httpClient
    .post<Article[]>(URL_GET_FILTRED_LIST_ARTICLES, filterArticle);
   }
   addArticle(article: Article): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_ARTICLE, article);
  }
  updateArticle(article: Article): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_ARTICLE, article);
  }
  deleteArticle(codeArticle): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_ARTICLE + '/' + codeArticle);
  }
  getArticle(codeArticle): Observable<Article> {
    return this.httpClient.get<Article>(URL_GET_ARTICLE + '/' + codeArticle);
  }
}
