type TypeArticle = 'Marchandise'| 'Produit fini' | 'Matière primaire' | 'Service' | 'Produit semi fini';

export interface ArticleElement {
         code: string ;
         libelle: string ;
          designation: string;
          marque: string;
          reference: string;
          
        // gestionConfiguration: string;
        // gestionUnMesures: string;
        // composed: string;
        // gestionMagasins: string;
        // gestionLots: string;
         description: string;
         // dateExpiration: string;
         type: string;
        }
