import { Component, OnInit, Input,
  ViewChild, EventEmitter, Output, AfterContentChecked,
AfterViewChecked
} from '@angular/core';
// import { ArticleService } from './article.service';
import {Article} from './article.model';
import {Magasin} from '../magasins/magasin.model';
import { UnMesure } from '../un-mesures/un-mesure.model';
import {ArticleService} from './article.service';
import {MagasinService} from '../magasins/magasin.service';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {ActionData} from './action-data.interface';
import {MatSnackBar} from '@angular/material';
import { TypeArticle } from './type-article.interface';
import { SchemaConfiguration } from '../schemas-configuration/schema-configuration.model';
import { SchemaConfigurationService } from '../schemas-configuration/schema-configuration.service';
import { ConfigurationData } from '../configuration-data';
import { ClasseArticleFilter } from '../classes-articles/classe-article.filter';
import { SchemaConfigurationFilter } from '../schemas-configuration/schemas-configuration.filter';
import { UnMesureFilter } from '../un-mesures/un-mesures.filter';
import { MatExpansionPanel } from '@angular/material';
import { DataStock } from '../articles/data-stock.model';

@Component({
  selector: 'app-article-edit',
  templateUrl: './article-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class ArticleEditComponent implements OnInit, AfterContentChecked {

// private client: Client = new Client();


private article: Article = new Article();
private sourceListMagasins: Magasin[];
private targetListMagasins: Magasin[] = [];
private sourceListArticles: Article[] = [];
private targetListArticles: Article[] = [];
private sourceListUnMesures: UnMesure[];
private targetListUnMesures: UnMesure[] = [];
private listSchemasConfiguration: SchemaConfiguration[] = [];
private controlCodesSchemasConfiguration = new FormControl();
private descriptionSchemaConfiguration: string;
private listConfigurationData: ConfigurationData[];
private classeArticleFilter : ClasseArticleFilter;
private schemaConfigurationFilter : SchemaConfigurationFilter;
private unMesureFilter : UnMesureFilter;

// @Input()
// private listClients: any = [] ;

@Input()
private actionData: ActionData;

@ViewChild('1stPanel')
private firstMatExpansionPanel : MatExpansionPanel;

private file: any;
private typesArticle: TypeArticle[] = [
  {value: 'Marchandise', viewValue: 'Marchandise'},
  {value: 'ProduitFini', viewValue: 'Produit Fini'},
  {value: 'ProduitSemiFini', viewValue: 'Produit Semi Fini'},
  {value: 'Service', viewValue: 'Service'},
  {value: 'MatierePrimaire', viewValue: 'Matière Primaire'}
];

private disponibilitesArticles  : any[] = [
  {value: 'Immediate', viewValue: 'Immédiate'},
  {value: 'CourtTerme', viewValue: 'à Court Terme'},
  {value: 'LongTerme', viewValue: 'à Long Terme'},
  {value: 'SurCommande', viewValue: 'Sur Commande'},
];

private vivialitesArticles : any[] = [
  {value: 'ArticleMort', viewValue: 'Article Mort'},
  {value: 'ArticleCourant', viewValue: 'Article Courant'},
  {value: 'ArticleRisque', viewValue: 'Article Risqué'},
  {value: 'ArticleDomage', viewValue: 'Article Domagé'}
];


private legetimitesArticles : any[] = [
  {value: 'Legetime', viewValue: 'Légétime'},
  {value: 'NonLegetime', viewValue: 'NonLégétime'},
];



  constructor( private route: ActivatedRoute,
    private router: Router , private articleService: ArticleService, private matSnackBar: MatSnackBar,
    private magasinService: MagasinService, private schemaConfigurationService: SchemaConfigurationService
   ) {
  }
  ngOnInit() {
    this.sourceListMagasins = this.route.snapshot.data.magasins;
    this.sourceListUnMesures = this.route.snapshot.data.unMesures;
    this.classeArticleFilter = new 
    ClasseArticleFilter(this.route.snapshot.data.classesArticles);
    this.schemaConfigurationFilter= new 
    SchemaConfigurationFilter(this.route.snapshot.data.schemasConfiguration);
   /* this.unMesureFilter= new 
    UnMesureFilter(this.route.snapshot.data.unMesures);*/
    console.log('mag recieved ...');
    console.log(this.sourceListMagasins);
    console.log(this.sourceListUnMesures);
    console.log(this.article);
      const codeArticle = this.route.snapshot.paramMap.get('codeArticle');
      this.articleService.getArticle(codeArticle).subscribe((article) => {
        article.dataStock  =  article.dataStock === undefined ?new DataStock(): article.dataStock;

        this.firstMatExpansionPanel.open();
        
               this.article = article;
               console.log('article recievd is ', this.article );
         if (this.article.configuration !== undefined && this.article.configuration !== null) {
                this.listConfigurationData = JSON.parse(this.article.configuration );
                console.log('List config objects.....');
                console.log( this.listConfigurationData);

         }
               const selectedMagasins = [];
               const selectedUnMesures = [];
               
               if (this.article.codesMagasins !== undefined && this.article.codesMagasins.length !== 0) {
                 console.log(' codes ...');
                 console.log(this.article.codesMagasins);
                 this.article.codesMagasins.forEach(function(codeMagasin, index) {
       
                     const mag = this.sourceListMagasins.filter((mg) => mg.code === codeMagasin)[0];
                     if ( mag !== undefined ) {
                     this.targetListMagasins.push(mag);
                     this.sourceListMagasins =  this.sourceListMagasins.filter( (magasin) => magasin.code !== mag.code);
                     }
                 }, this);
                 }

                 if (this.article.codesUnMesures !== undefined && this.article.codesUnMesures.length !== 0) {
                  console.log(' codes ...');
                  console.log(this.article.codesUnMesures);
                  this.article.codesUnMesures.forEach(function(codeUnMesure, index) {
        
                      const unM = this.sourceListUnMesures.filter((un) => un.code === codeUnMesure)[0];
                      if ( unM !== undefined ) {
                      this.targetListUnMesures.push(unM);
                      this.sourceListUnMesures =  this.sourceListUnMesures.filter( (unMesure) => unMesure.code !== unM.code);
                      }
                  }, this);
                  }
      });

     // if (this.article.gestionMagasins) {
    
        
       
      
    
      // this.client = this.actionData.selectedClients[0];
     
  }
  loadAddArticleComponent() {
    this.router.navigate(['/pms/stocks/ajouterArticle']) ;
  
  }
  loadGridArticles() {
    this.router.navigate(['/pms/stocks/articles']) ;
  }
  supprimerArticle() {
     
      this.articleService.deleteArticle(this.article.code).subscribe((response) => {

        this.openSnackBar( ' Element Supprimé');
        this.loadGridArticles() ;
            });
  }
updateArticle() {
  this.article.codesMagasins = [];
  this.article.codesUnMesures = [];
  this.article.codesArticlesSecondaires = [];
  
  
if (this.targetListMagasins.length !== 0) {
     this.targetListMagasins.forEach(function(magasin, index) {
      this.article.codesMagasins.push(magasin.code) ;
     }, this);
  }
  if (this.targetListUnMesures.length !== 0) {
    this.targetListUnMesures.forEach(function(unMesure, index) {
     this.article.codesUnMesures.push(unMesure.code) ;
    }, this);
 }
 if (this.targetListArticles.length !== 0) {
  this.targetListArticles.forEach(function(article, index) {
   this.article.codesArticlesSecondaires.push(article.code) ;
  }, this);
}
/*if (this.listConfigurationData.length !== 0) {
    this.article.configuration = JSON.stringify(this.listConfigurationData);
}*/
  console.log('The article just before sending to the server :');
  console.log(this.article);
  this.articleService.updateArticle(this.article).subscribe((response) => {
    if (response.error_message ===  undefined) {
      console.log('the response of update is ', response );
      this.openSnackBar( ' Article mis à jour ');
      
      //  this.save.emit(response);
      // this.client = new Client();
      // this.modeUpdate = false;
    } else {
      this.openSnackBar( ' Erreurs.. ');
      
     // show error message
    }
});
}
fileChangedF1($event) {
  this.file = $event.target.files[0];
  const fileReader = new FileReader();
  fileReader.onload = (e) => {
    console.log(fileReader.result);
     this.article.imageFace1 = fileReader.result;
    // this.filesData[indexFile] = fileData;
  };
  fileReader.readAsDataURL(this.file);
 }
 fileChangedF2($event) {
  this.file = $event.target.files[0];
  const fileReader = new FileReader();
  fileReader.onload = (e) => {
    console.log(fileReader.result);
     this.article.imageFace2 = fileReader.result;
    // this.filesData[indexFile] = fileData;
  };
  fileReader.readAsDataURL(this.file);
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

ngAfterContentChecked() {

}
loadListArticles() {
  this.sourceListArticles = [];
         this.articleService.getArticles().subscribe((articles) => {
             this.sourceListArticles = articles;
             this.sourceListArticles =  this.sourceListArticles.filter( (article) => article.code !== this.article.code);
             if (this.article.codesArticlesSecondaires !== undefined && this.article.codesArticlesSecondaires.length !== 0) {
              console.log(' codes ...art second');
              console.log(this.article.codesArticlesSecondaires);
              this.article.codesArticlesSecondaires.forEach(function(codeArticle, index) {
                  const art = this.sourceListArticles.filter((at) => at.code === codeArticle)[0];
                  if ( art !== undefined ) {
                  this.targetListArticles.push(art);
                  this.sourceListArticles =  this.sourceListArticles.filter( (article) =>
                  article.code !== art.code  && article.code !== this.article.code);
                  }
              }, this);
              }

         });
}
loadListSchemasConfiguration() {
  this.schemaConfigurationService.getSchemasConfiguration().subscribe((schemasConfiguration) =>
  ( this.listSchemasConfiguration = schemasConfiguration));
}
selectedTabChanged($event) {
  if ($event.index === 4) { // Composition tab was selected
    this.loadListArticles();
   } else if ($event.index === 1) {
     this.loadListSchemasConfiguration() ;
   }
  }

fillDataSchemaConfiguration()
{
  const schemaConfigurationSelected = this.listSchemasConfiguration.filter((schemaConfig) =>
         (this.article.schemaConfiguration.code ===  schemaConfig.code))[0];
  this.descriptionSchemaConfiguration = schemaConfigurationSelected.description;
 const attributes =  schemaConfigurationSelected.configAttributes.split(';');
this.listConfigurationData = [];
 attributes.forEach(function(attribute, index) {
  this.listConfigurationData.push({field: attribute , value: ''});
 } , this);
}
}


