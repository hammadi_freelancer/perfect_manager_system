import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { ClasseArticleService } from './classes-articles/classe-article.service';
import { ClasseArticle } from './classes-articles/classe-article.model';

@Injectable()
export class ClasseArticleResolver implements Resolve<Observable<ClasseArticle[]>> {
  constructor(private classeArticleService: ClasseArticleService) { }

  resolve() {
     return this.classeArticleService.getClassesArticles();
}
}
