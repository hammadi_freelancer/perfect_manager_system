// type TypeArticle = 'Marchandise'| 'Produit fini' | 'Matière primaire' | 'Service' | 'Produit semi fini';

export interface MagasinElement {
         code: string ;
         description: string;
         adresse: string;
         contact: string;
                }
