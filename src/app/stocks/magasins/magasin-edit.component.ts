import { Component, OnInit, Input, EventEmitter, Output, AfterContentChecked,
AfterViewChecked
} from '@angular/core';
// import { ArticleService } from './article.service';
import {Magasin} from './magasin.model';
import {MagasinService} from './magasin.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {ActionData} from './action-data.interface';
import {MatSnackBar} from '@angular/material';



@Component({
  selector: 'app-magasin-edit',
  templateUrl: './magasin-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class MagasinEditComponent implements OnInit, AfterContentChecked {

// private client: Client = new Client();


private magasin: Magasin = new Magasin();

// @Input()
// private listClients: any = [] ;

@Input()
private actionData: ActionData;


private file: any;

  constructor( private route: ActivatedRoute,
    private router: Router , private magasinService: MagasinService, private matSnackBar: MatSnackBar,
   ) {
  }
  ngOnInit() {
 
      const codeMagasin = this.route.snapshot.paramMap.get('codeMagasin');
      this.magasinService.getMagasin(codeMagasin).subscribe((magasin) => {
               this.magasin = magasin;
      });
      // this.client = this.actionData.selectedClients[0];
     
  }
  loadAddMagasinComponent() {
    this.router.navigate(['/pms/stocks/ajouterMagasin']) ;
  
  }
  loadGridMagasins() {
    this.router.navigate(['/pms/stocks/magasins']) ;
  }
  supprimerMagasin() {
     
      this.magasinService.deleteMagasin(this.magasin.code).subscribe((response) => {

        this.openSnackBar( ' Element Supprimé');
        this.loadGridMagasins() ;
            });
  }
updateMagasin() {
  console.log(this.magasin);
  this.magasinService.updateMagasin(this.magasin).subscribe((response) => {
    if (response.error_message ===  undefined) {
      console.log('the response of update is ', response );
      this.openSnackBar( ' Magasin mis à jour ');
      
      //  this.save.emit(response);
      // this.client = new Client();
      // this.modeUpdate = false;
    } else {
      this.openSnackBar( ' Erreurs.. ');
      
     // show error message
    }
});
}
fileChanged($event) {
  this.file = $event.target.files[0];
  const fileReader = new FileReader();
  fileReader.onload = (e) => {
    console.log(fileReader.result);
     this.magasin.image = fileReader.result;
    // this.filesData[indexFile] = fileData;
  };
  fileReader.readAsDataURL(this.file);
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

ngAfterContentChecked() {

}
}


