import { Component, OnInit, Output, ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {Magasin} from './magasin.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {MagasinService} from './magasin.service';
import { ActionData } from './action-data.interface';
// import { Fournisseur } from '../../ventes/clients/client.model';
import { MagasinElement } from './magasin.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
// import { AlertMessage } from '../../common/alert-message.interface';
// import { OperationRequest } from '../../common/operation-request.interface';
// import { OperationResponse } from '../../common/operation-response.interface';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-magasins-grid',
  templateUrl: './magasins-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class MagasinsGridComponent implements OnInit {

  private magasin: Magasin =  new Magasin();
  private selection = new SelectionModel<MagasinElement>(true, []);
  
@Output()
select: EventEmitter<Magasin[]> = new EventEmitter<Magasin[]>();

//private lastClientAdded: Client;

 selectedMagasin: Magasin ;

 @Input()
 private actionData: ActionData;

 @Input()
 private listMagasins: any = [] ;
 listMagasinsOrgin: any = [];
 private checkedAll: false;
 private  dataMagasinsSource: MatTableDataSource<MagasinElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
  constructor( private route: ActivatedRoute, private magasinsService: MagasinService,
    private router: Router, private matSnackBar: MatSnackBar ) {
  }
  ngOnInit() {
    this.magasinsService.getMagasins().subscribe((magasins) => {
      this.listMagasins = magasins;
      // this.listClients[this.listClients.length] = this.lastClientAdded;
      // this.listClientsOrgin = this.listClients ;
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });
   /* this.creditsService.getCredits().subscribe((credits) => {
      console.log('list credits ', credits);
      // this.listCredits = credits;
      this.listCredits =  this.listCredits.map((credit) => {
        if (credit.client === 'undefined' || credit.client === null ) {
           credit.client = new Client();
         }
         return credit;
   });      this.listCreditsOrgin = this.listCredits ;
     });*/
    // this.listClientsOrgin = this.listClients ;

  }
  deleteMagasin(magasin) {
      console.log('call delete !', magasin );
    this.magasinsService.deleteMagasin(magasin.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData();

      } else {
       // show error message
      }
     });

  }
loadData() {
  this.magasinsService.getMagasins().subscribe((magasins) => {
    this.listMagasins = magasins;
    // this.listClients[this.listClients.length] = this.lastClientAdded;
    this.listMagasinsOrgin = this.listMagasins;
    this.transformDataToByConsumedByMatTable();
   });
  // this.listClientsOrgin = this.listClients ;
}


checkOneActionInvoked() {
  const listSelectedMagasins: Magasin[] = [];
console.log('selected magasins are ');
console.log(this.selection.selected);
if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((magasinElement) => {
  return magasinElement.code;
});
this.listMagasins.forEach(magasin => {
  if (codes.findIndex(code => code === magasin.code) > -1) {
    listSelectedMagasins.push(magasin);
  }
  });
}
this.select.emit(listSelectedMagasins);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listMagasins !== undefined) {
    this.listMagasins.forEach(magasin => {
      listElements.push( {code: magasin.code ,
       description: magasin.description,
        adresse: magasin.adresse ,
        contact: magasin.contact ,
     } );
    });
  }
    this.dataMagasinsSource = new MatTableDataSource<MagasinElement>(listElements);
    this.dataMagasinsSource.sort = this.sort;
    this.dataMagasinsSource.paginator = this.paginator;
}
 specifyListColumnsToBeDisplayed() {
   this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
   {name: 'description' , displayTitle: 'Déscription'},
   {name: 'adresse' , displayTitle: 'Adresse'},
    {name: 'contact' , displayTitle: 'Contact'} ];
   this.columnsTitles = this.columnsDefinitions.map((colDef) => {
             return colDef.displayTitle;
   });
   this.columnsNames[0] = 'select';
   this.columnsDefinitions.map((colDef) => {
    this.columnsNames.push(colDef.name);
});
   this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
    return colDef.displayTitleAr;
});
}
masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataMagasinsSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataMagasinsSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataMagasinsSource.filter = filterValue.trim().toLowerCase();
  if (this.dataMagasinsSource.paginator) {
    this.dataMagasinsSource.paginator.firstPage();
  }
}
loadAddMagasinComponent() {
  this.router.navigate(['/pms/stocks/ajouterMagasin']) ;

}
loadEditMagasinComponent() {
  if (this.selection.selected.length === 0) {
    this.openSnackBar(' Aucun élément sélectionné' );
    // this.loadData();
    } else
   {
  this.router.navigate(['/pms/stocks/editerMagasin', this.selection.selected[ this.selection.selected.length - 1].code]) ;
    }
}
supprimerMagasins()
{
  console.log('NBRE OF ELEMENTS SELECTED');
  console.log(this.selection.selected.length);
  console.log(' ELEMENTS SELECTED');
  console.log(this.selection.selected);
  if ( this.selection.selected.length !== 0) {
      this.selection.selected.forEach(function(magasin, index) {
          this.magasinsService.deleteMagasin(magasin.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.openSnackBar( this.selection.selected.length + ' Element(s) Supprimé(s)');
             this.loadData();
            }
      }, this);
    } else {
      this.openSnackBar('No Elements Selected');
    }
}
refresh()
{
  this.loadData();

}
  openSnackBar(messageToDisplay) {
 this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
     verticalPosition: 'top'});
}
}
