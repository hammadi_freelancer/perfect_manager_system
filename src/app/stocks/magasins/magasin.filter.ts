
import { BaseEntity } from '../../common/base-entity.model' ;
// import { Tranche } from './tranche.model' ;
import { Client } from '../../ventes/clients/client.model' ;
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

import { Magasin } from '../../stocks/magasins/magasin.model';


export class MagasinFilter  {
    public selectedCodesMagasins: string;
    

    private controlCodesMagasins= new FormControl();
    private controlLibellesMagasins= new FormControl();

    
   
    
   //  private controlAdresse = new FormControl();
    private listCodesMagasins: string[] = [];
 
    
    private listLibellesMagasins: string[] = [];
  

    private filteredCodesMagasins: Observable<string[]>;
    private filteredLibellesMagasins: Observable<string[]>;

    constructor ( magasins: Magasin[], ) {
      if (magasins !== null && magasins !== undefined) {
       this.listCodesMagasins = magasins.map((mg) => (mg.code));

        this.listLibellesMagasins = magasins.filter((mg) =>
        (mg.libelle !== undefined && mg.libelle != null )).
        map((mg) => (mg.libelle ));
      }

      this.filteredCodesMagasins = this.controlCodesMagasins.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterCodesMagasins(value))
        );
        this.filteredLibellesMagasins = this.controlLibellesMagasins.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterLibellesMagasins(value))
        );
       
     
 
    }
  
    private _filterCodesMagasins(value: string): string[] {
      if ( value !== undefined) {
        const filterValue = value.toLowerCase();
        return this.listCodesMagasins.filter(codeMG=> codeMG.toLowerCase().includes(filterValue));
      } else {
        return this.listCodesMagasins;
      }
      }
      private _filterLibellesMagasins(value: string): string[] {
        if ( value !== undefined) {
        const filterValue = value.toLowerCase();
        return this.listLibellesMagasins.filter(libelleMG => libelleMG.toLowerCase().includes(filterValue));
        } else {
          return this.listLibellesMagasins;
        }
      }
     
 
      public getControlCodesMagasins(): FormControl {
        return this.controlCodesMagasins;
      }
   
      public getControlLibelleMagasins(): FormControl {
        return this.controlLibellesMagasins;
      }

      public getFiltredCodesMagasins(): Observable<string[]> {
        return this.filteredCodesMagasins;
      }
      public getFiltredLibellesMagasins(): Observable<string[]> {
        return this.filteredLibellesMagasins;
      }


}





