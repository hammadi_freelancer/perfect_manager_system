import { Component, OnInit, Input, EventEmitter, Output,
    AfterViewChecked
    } from '@angular/core';
    // import { ArticleService } from './article.service';
    import {Magasin} from './magasin.model';
    import {MagasinService} from './magasin.service';
    import {FormControl} from '@angular/forms';
    import {map, startWith} from 'rxjs/operators';
    import {Observable} from 'rxjs';
    import { Router, ActivatedRoute, ParamMap } from '@angular/router';
    import {ActionData} from './action-data.interface';
    import {MatSnackBar} from '@angular/material';
   //  import { TypeArticle } from './type-article.interface';
    
    @Component({
      selector: 'app-magasin-new',
      templateUrl: './magasin-new.component.html',
      // styleUrls: ['./players.component.css']
    })
export class MagasinNewComponent implements OnInit {
    private magasin: Magasin = new Magasin();
    @Input()
    private listMagasins: any = [] ;
    @Input()
    private actionData: ActionData;
    private file: any;

   /* private typesArticle: TypeArticle[] = [
      {value: 'Marchandise', viewValue: 'Marchandise'},
      {value: 'ProduitFini', viewValue: 'Produit Fini'},
      {value: 'ProduitSemiFini', viewValue: 'Produit Semi Fini'},
      {value: 'Service', viewValue: 'Service'},
      {value: 'MatierePrimaire', viewValue: 'Matière Primaire'}
    ];*/
      constructor( private route: ActivatedRoute,
        private router: Router , private magasinService: MagasinService, private matSnackBar: MatSnackBar ) {
      }
      ngOnInit() {
     
          console.log('the action data is ');
          console.log(this.actionData);
      }
    saveMagasin() {
       
        console.log(this.magasin);
        this.magasinService.addMagasin(this.magasin).subscribe((response) => {
            if (response.error_message ===  undefined) {
              // this.save.emit(response);
              this.magasin = new Magasin();
              this.openSnackBar(  'Magasin Enregistré');
            } else {
             // show error message
             this.openSnackBar(  'Erreurs!');
             
            }
        });
    }
    loadGridMagasins() {
      this.router.navigate(['/pms/stocks/magasins']) ;
    }
    fileChanged($event) {
        this.file = $event.target.files[0];
        const fileReader = new FileReader();
        fileReader.onload = (e) => {
          console.log(fileReader.result);
           this.magasin.image = fileReader.result;
          // this.filesData[indexFile] = fileData;
        };
        fileReader.readAsDataURL(this.file);
       }
       vider() {
         this.magasin = new Magasin();
       }
       openSnackBar(messageToDisplay) {
        this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
            verticalPosition: 'top'});
       }
 
    }
