

import { BaseEntity } from '../../common/base-entity.model' ;

// type TypeArticle = 'Marchandise'| 'ProduitFini' | 'MatierePrimaire' | 'Service' | 'ProduitSemiFini';

export class Magasin extends BaseEntity {
   constructor(public code?: string, 
    public adresse?: string, 
    public libelle?: string, 
    public contact?: string,
    public image?: any,
) {
    super();
   //  this.categoryClient = 'PERSONNE_PHYSIQUE'; // other possible value : PERSONNE_MORALE
    // this.checked = false;
    this.image = undefined;
}
}
