import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';

import {Magasin} from './magasin.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_MAGASINS = 'http://localhost:8082/stocks/magasins/getMagasins';
const URL_ADD_MAGASIN = 'http://localhost:8082/stocks/magasins/addMagasin';
const URL_GET_MAGASIN = 'http://localhost:8082/stocks/magasins/getMagasin';

const URL_UPDATE_MAGASIN = 'http://localhost:8082/stocks/magasins/updateMagasin';
const URL_DELETE_MAGASIN = 'http://localhost:8082/stocks/magasins/deleteMagasin';
// const URL_GET_FILTRED_LIST_MAGASINS  = 'http://localhost:8082/stocks/magasins/getFiltredArticles';
@Injectable({
  providedIn: 'root'
})
export class MagasinService {



  constructor(private httpClient: HttpClient) {

   }

   getMagasins(): Observable<Magasin[]> {
    return this.httpClient
    .get<Magasin[]>(URL_GET_LIST_MAGASINS);
   }
   /*getFiltredArticles(filterArticle): Observable<Article[]> {
    return this.httpClient
    .post<Article[]>(URL_GET_FILTRED_LIST_ARTICLES, filterArticle);
   }*/
   addMagasin(magasin: Magasin): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_MAGASIN, magasin);
  }
  updateMagasin(magasin: Magasin): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_MAGASIN, magasin);
  }
  deleteMagasin(codeMagasin): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_MAGASIN + '/' + codeMagasin);
  }
  getMagasin(codeMagasin): Observable<Magasin> {
    return this.httpClient.get<Magasin>(URL_GET_MAGASIN + '/' + codeMagasin);
  }

}
