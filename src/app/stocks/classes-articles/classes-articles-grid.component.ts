import { Component, OnInit, Output, ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {ClasseArticle} from './classe-article.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {ClasseArticleService} from './classe-article.service';
// import { ActionData } from './action-data.interface';
// import { Fournisseur } from '../../ventes/clients/client.model';
import { ClasseArticleElement } from './classe-article.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
// import { AlertMessage } from '../../common/alert-message.interface';
// import { OperationRequest } from '../../common/operation-request.interface';
// import { OperationResponse } from '../../common/operation-response.interface';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-classes-articles-grid',
  templateUrl: './classes-articles-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class ClassesArticlesGridComponent implements OnInit {

  private classeArticle: ClasseArticle =  new ClasseArticle();
  private selection = new SelectionModel<ClasseArticleElement>(true, []);
  private deleteEnCours = false;
@Output()
select: EventEmitter<ClasseArticle[]> = new EventEmitter<ClasseArticle[]>();

//private lastClientAdded: Client;

 selectedClasseArticle: ClasseArticle ;


 @Input()
 private listClassesArticles: any = [] ;
 listClassesArticlesOrgin: any = [];
 // private checkedAll: false;
 private  dataClassesArticlesSource: MatTableDataSource<ClasseArticleElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
  constructor( private route: ActivatedRoute, private classeArticleService: ClasseArticleService,
    private router: Router, private matSnackBar: MatSnackBar ) {
  }
  ngOnInit() {
    this.specifyListColumnsToBeDisplayed();
    
    this.classeArticleService.getClassesArticles().subscribe((classesArticles) => {
      this.listClassesArticles = classesArticles;
      // this.listClients[this.listClients.length] = this.lastClientAdded;
      // this.listClientsOrgin = this.listClients ;
    this.transformDataToByConsumedByMatTable();
  });


  }
  deleteClasseArticle(classeArticle) {
     //  console.log('call delete !', unMesure );
    this.classeArticleService.deleteClasseArticle(classeArticle.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData();

      } else {
       // show error message
      }
     });

  }
loadData() {
  this.classeArticleService.getClassesArticles().subscribe((classeArticle) => {
    this.listClassesArticles = classeArticle;
    // this.listClients[this.listClients.length] = this.lastClientAdded;
    // this.listUnMesuresOrgin = this.listUnMesures;
    this.transformDataToByConsumedByMatTable();
   });
  // this.listClientsOrgin = this.listClients ;
}


checkOneActionInvoked() {
  const listSelectedClasseArticle: ClasseArticle[] = [];
console.log('selected unMesures are ');
console.log(this.selection.selected);
if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((classeArtElement) => {
  return classeArtElement.code;
});
this.listClassesArticles.forEach(ClsArticle => {
  if (codes.findIndex(code => code === ClsArticle.code) > -1) {
    listSelectedClasseArticle.push(ClsArticle);
  }
  });
}
this.select.emit(listSelectedClasseArticle);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listClassesArticles !== undefined) {
    this.listClassesArticles.forEach(clsArt => {
      listElements.push( {code: clsArt.code ,
        libelle: clsArt.libelle,
       description: clsArt.description,
     } );
    });
  }
    this.dataClassesArticlesSource = new MatTableDataSource<ClasseArticleElement>(listElements);
    this.dataClassesArticlesSource.sort = this.sort;
    this.dataClassesArticlesSource.paginator = this.paginator;
}
 specifyListColumnsToBeDisplayed() {
   this.columnsDefinitions = [
     {name: 'code' , displayTitle: 'Code'},
     {name: 'libelle' , displayTitle: 'Libéllé'},
     {name: 'description' , displayTitle: 'Déscription'},
  
  ];
   this.columnsTitles = this.columnsDefinitions.map((colDef) => {
             return colDef.displayTitle;
   });
   this.columnsNames[0] = 'select';
   this.columnsDefinitions.map((colDef) => {
    this.columnsNames.push(colDef.name);
});
   this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
    return colDef.displayTitleAr;
});
}
masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataClassesArticlesSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataClassesArticlesSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataClassesArticlesSource.filter = filterValue.trim().toLowerCase();
  if (this.dataClassesArticlesSource.paginator) {
    this.dataClassesArticlesSource.paginator.firstPage();
  }
}
loadAddClasseArticleComponent() {
  this.router.navigate(['/pms/stocks/ajouterClasseArticle']) ;

}
loadEditClasseArticleComponent() {
  if (this.selection.selected.length === 0) {
    this.openSnackBar(' Aucune Classe sélectionnée!' );
    // this.loadData();
    } else
   {
  this.router.navigate(['/pms/stocks/editerClasseArticle', this.selection.selected[ this.selection.selected.length - 1].code]) ;
    }
}
supprimerClassesArticles()
{

  if ( this.selection.selected.length !== 0) {
      this.selection.selected.forEach(function(classeArticle, index) {
        this.deleteEnCours = true;
          this.classeArticleService.deleteClasseArticle(classeArticle.code).subscribe((response) => {
           
            if (this.selection.selected.length - 1 === index) {
              this.deleteEnCours = false;
              
              this.openSnackBar( ' Element(s) Supprimé(s)');
               this.loadData();
              }
          });
       
      }, this);
    } else {
      this.openSnackBar('Aucune Classe Séléctionnée!');
    }
}
refresh()
{
  this.loadData();

}
  openSnackBar(messageToDisplay) {
 this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
     verticalPosition: 'top'});
}
}
