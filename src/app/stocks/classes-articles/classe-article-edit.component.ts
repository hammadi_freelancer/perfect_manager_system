import { Component, OnInit, Input, EventEmitter, Output, AfterContentChecked,
AfterViewChecked
} from '@angular/core';
// import { ArticleService } from './article.service';
import {ClasseArticle} from './classe-article.model';
import {ClasseArticleService} from './classe-article.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {MatSnackBar} from '@angular/material';



@Component({
  selector: 'app-classe-article-edit',
  templateUrl: './classe-article-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class ClasseArticleEditComponent implements OnInit, AfterContentChecked {

// private client: Client = new Client();


private classeArticle: ClasseArticle = new ClasseArticle();




private file: any;

  constructor( private route: ActivatedRoute,
    private router: Router , private classeArticleService: ClasseArticleService, private matSnackBar: MatSnackBar,
   ) {
  }
  ngOnInit() {
 
      const code = this.route.snapshot.paramMap.get('codeClasseArticle');
      this.classeArticleService.getClasseArticle(code).subscribe((classeArticle) => {
               this.classeArticle = classeArticle;
      });
      // this.client = this.actionData.selectedClients[0];
     
  }
  /*loadAddUnMesureComponent() {
    this.router.navigate(['/pms/stocks/ajouterUnMesure']) ;
  
  }*/
  loadGridClassesArticles() {
    this.router.navigate(['/pms/stocks/classesArticles']) ;
  }
 /* supprimerUnMesur() {
     
      this.unMesureService.deleteUnMesure(this.unMesure.code).subscribe((response) => {

        this.openSnackBar( ' Element Supprimé');
        this.loadGridUnMesures() ;
            });
  }*/
updateClasseArticle() {
 // console.log(this.unMesure);
  this.classeArticleService.updateClasseArticle(this.classeArticle).subscribe((response) => {
    if (response.error_message ===  undefined) {
      console.log('the response of update is ', response );
      this.openSnackBar( ' Classe d\'article mis à jour ');
      
      //  this.save.emit(response);
      // this.client = new Client();
      // this.modeUpdate = false;
    } else {
      this.openSnackBar( ' Erreurs.. ');
      
     // show error message
    }
});
}
fileChanged($event) {
  this.file = $event.target.files[0];
  const fileReader = new FileReader();
  fileReader.onload = (e) => {
    console.log(fileReader.result);
     this.classeArticle.image = fileReader.result;
    // this.filesData[indexFile] = fileData;
  };
  fileReader.readAsDataURL(this.file);
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

ngAfterContentChecked() {

}
}


