import { Component, OnInit, Input, EventEmitter, Output,
    AfterViewChecked
    } from '@angular/core';
    // import { ArticleService } from './article.service';
    import {ClasseArticle} from './classe-article.model';
    import {ClasseArticleService} from './classe-article.service';
    import {FormControl} from '@angular/forms';
    import {map, startWith} from 'rxjs/operators';
    import {Observable} from 'rxjs';
    import { Router, ActivatedRoute, ParamMap } from '@angular/router';
    // import {ActionData} from './action-data.interface';
    import {MatSnackBar} from '@angular/material';
   //  import { TypeArticle } from './type-article.interface';
    
    @Component({
      selector: 'app-classe-article-new',
      templateUrl: './classe-article-new.component.html',
      // styleUrls: ['./players.component.css']
    })
export class ClasseArticleNewComponent implements OnInit {
    private classeArticle: ClasseArticle = new ClasseArticle();
   private saveEnCours= false;
   
   
    private file: any;

   /* private typesArticle: TypeArticle[] = [
      {value: 'Marchandise', viewValue: 'Marchandise'},
      {value: 'ProduitFini', viewValue: 'Produit Fini'},
      {value: 'ProduitSemiFini', viewValue: 'Produit Semi Fini'},
      {value: 'Service', viewValue: 'Service'},
      {value: 'MatierePrimaire', viewValue: 'Matière Primaire'}
    ];*/
      constructor( private route: ActivatedRoute,
        private router: Router , private classeArticleService: ClasseArticleService, private matSnackBar: MatSnackBar ) {
      }
      ngOnInit() {
     
      }
    saveClasseArticle() {
       
        //console.log(this.unMesure);
    this.saveEnCours = true;
     this.classeArticleService.addClasseArticle(this.classeArticle).subscribe((response) => {
      this.saveEnCours = false;
      
            if (response.error_message ===  undefined) {
              // this.save.emit(response);
              this.classeArticle = new ClasseArticle();
              this.openSnackBar(  'Classe d\'Article Enregistrée');
            } else {
             // show error message
             this.openSnackBar(  'Erreurs!');
             
            }
        });
    }
    loadGridClassesArticles() {
      this.router.navigate(['/pms/stocks/classesArticles']) ;
    }
    fileChanged($event) {
        this.file = $event.target.files[0];
        const fileReader = new FileReader();
        fileReader.onload = (e) => {
          console.log(fileReader.result);
           this.classeArticle.image = fileReader.result;
          // this.filesData[indexFile] = fileData;
        };
        fileReader.readAsDataURL(this.file);
       }
       vider() {
         this.classeArticle = new ClasseArticle();
       }
       openSnackBar(messageToDisplay) {
        this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
            verticalPosition: 'top'});
       }
 
    }
