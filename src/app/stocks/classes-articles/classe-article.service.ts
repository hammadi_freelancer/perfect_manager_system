import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';

import {ClasseArticle} from './classe-article.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_CLASSES_ARTICLES = 'http://localhost:8082/stocks/classesArticles/getClassesArticles';
const URL_ADD_CLASSE_ARTICLE = 'http://localhost:8082/stocks/classesArticles/addClasseArticle';
const URL_GET_CLASSE_ARTICLE = 'http://localhost:8082/stocks/classesArticles/getClasseArticle';

const URL_UPDATE_CLASSE_ARTICLE = 'http://localhost:8082/stocks/classesArticles/updateClasseArticle';
const URL_DELETE_CLASSE_ARTICLE = 'http://localhost:8082/stocks/classesArticles/deleteClasseArticle';
// const URL_GET_FILTRED_LIST_MAGASINS  = 'http://localhost:8082/stocks/magasins/getFiltredArticles';
@Injectable({
  providedIn: 'root'
})
export class ClasseArticleService {



  constructor(private httpClient: HttpClient) {

   }

   getClassesArticles(): Observable<ClasseArticle[]> {
    return this.httpClient
    .get<ClasseArticle[]>(URL_GET_LIST_CLASSES_ARTICLES);
   }
   /*getFiltredArticles(filterArticle): Observable<Article[]> {
    return this.httpClient
    .post<Article[]>(URL_GET_FILTRED_LIST_ARTICLES, filterArticle);
   }*/
   addClasseArticle(classeArticle: ClasseArticle): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_CLASSE_ARTICLE, classeArticle);
  }
  updateClasseArticle(classArticle: ClasseArticle): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_CLASSE_ARTICLE, classArticle);
  }
  deleteClasseArticle(codeArt): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_CLASSE_ARTICLE + '/' + codeArt);
  }
  getClasseArticle(codeClasseArticle): Observable<ClasseArticle> {
    return this.httpClient.get<ClasseArticle>(URL_GET_CLASSE_ARTICLE + '/' + codeClasseArticle);
  }

}
