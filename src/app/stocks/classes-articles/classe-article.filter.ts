
import { BaseEntity } from '../../common/base-entity.model' ;
// import { Tranche } from './tranche.model' ;
import { Client } from '../../ventes/clients/client.model' ;
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { ClasseArticleService } from '../../stocks/classes-articles/classe-article.service';
import { ClasseArticle } from '../../stocks/classes-articles/classe-article.model';
// import { UnMesure } from '../../stocks/un-mesures/un-mesure.model';
// import { Magasin } from '../../stocks/magasins/magasin.model';


export class ClasseArticleFilter  {
    public selectedCodesClassesArticle: string;
    

    private controlCodesClassesArticle = new FormControl();
    private controlLibellesClassesArticle= new FormControl();

    
   
    
   //  private controlAdresse = new FormControl();
    private listCodesClassesArticle: string[] = [];
 
    
    private listLibellesClassesArticle: string[] = [];
  

    private filteredCodesClassesArticle: Observable<string[]>;
    private filteredLibellesClassesArticle: Observable<string[]>;

    constructor ( classesArticle: ClasseArticle[], ) {
      if (ClasseArticle !== null && ClasseArticle !== undefined) {
       this.listCodesClassesArticle = classesArticle.map((clsArt) => (clsArt.code));

        this.listLibellesClassesArticle = classesArticle.filter((clsArt) =>
        (clsArt.libelle !== undefined && clsArt.libelle != null )).
        map((clsArt) => (clsArt.libelle ));
      }

      this.filteredCodesClassesArticle = this.controlCodesClassesArticle.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterCodesClassesArticle(value))
        );
        this.filteredLibellesClassesArticle = this.controlLibellesClassesArticle.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterLibellesClassesArticle(value))
        );
       
     
 
    }
  
    private _filterCodesClassesArticle(value: string): string[] {
      if(value !== undefined) {
        
        const filterValue = value.toLowerCase();
        return this.listCodesClassesArticle.filter(codeCA=> codeCA.toLowerCase().includes(filterValue));
      } else
      {
        return this.listCodesClassesArticle;
      }
      }
      private _filterLibellesClassesArticle(value: string): string[] {
        if(value !== undefined) {
          
        const filterValue = value.toLowerCase();
        return this.listLibellesClassesArticle.filter(libelleCA=> libelleCA.toLowerCase().includes(filterValue));
        } else {
          return this.listLibellesClassesArticle;
        }
      }
     
 
      public getControlCodesClassesArticles(): FormControl {
        return this.controlCodesClassesArticle;
      }
   
      public getControlLibelleClassesArticle(): FormControl {
        return this.controlLibellesClassesArticle;
      }

      public getFiltredCodesClassesArticles(): Observable<string[]> {
        return this.filteredCodesClassesArticle;
      }
      public getFiltredLibelleClassesArticles(): Observable<string[]> {
        return this.filteredLibellesClassesArticle;
      }


}





