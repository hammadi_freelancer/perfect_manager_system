import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { SchemaConfigurationService } from './schemas-configuration/schema-configuration.service';
import { Magasin } from './magasins/magasin.model';

@Injectable()
export class SchemasConfigurationResolver implements Resolve<Observable<Magasin[]>> {
  constructor(private schemaConfigurationService: SchemaConfigurationService) { }

  resolve() {
     return this.schemaConfigurationService.getSchemasConfiguration();
}
}
