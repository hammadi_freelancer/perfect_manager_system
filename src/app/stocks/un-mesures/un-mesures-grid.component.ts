import { Component, OnInit, Output, ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {UnMesure} from './un-mesure.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {UnMesureService} from './un-mesure.service';
import { ActionData } from './action-data.interface';
// import { Fournisseur } from '../../ventes/clients/client.model';
import { UnMesureElement } from './un-mesure.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
// import { AlertMessage } from '../../common/alert-message.interface';
// import { OperationRequest } from '../../common/operation-request.interface';
// import { OperationResponse } from '../../common/operation-response.interface';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-un-mesures-grid',
  templateUrl: './un-mesures-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class UnMesuresGridComponent implements OnInit {

  private unMesure: UnMesure =  new UnMesure();
  private selection = new SelectionModel<UnMesureElement>(true, []);
  
@Output()
select: EventEmitter<UnMesure[]> = new EventEmitter<UnMesure[]>();

//private lastClientAdded: Client;

 selectedUnMesure: UnMesure ;

 @Input()
 private actionData: ActionData;

 @Input()
 private listUnMesures: any = [] ;
 listUnMesuresOrgin: any = [];
 private checkedAll: false;
 private  dataUnMesuresSource: MatTableDataSource<UnMesureElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
  constructor( private route: ActivatedRoute, private unMesuresService: UnMesureService,
    private router: Router, private matSnackBar: MatSnackBar ) {
  }
  ngOnInit() {
    this.unMesuresService.getUnMesures().subscribe((unMesures) => {
      this.listUnMesures = unMesures;
      // this.listClients[this.listClients.length] = this.lastClientAdded;
      // this.listClientsOrgin = this.listClients ;
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });
   /* this.creditsService.getCredits().subscribe((credits) => {
      console.log('list credits ', credits);
      // this.listCredits = credits;
      this.listCredits =  this.listCredits.map((credit) => {
        if (credit.client === 'undefined' || credit.client === null ) {
           credit.client = new Client();
         }
         return credit;
   });      this.listCreditsOrgin = this.listCredits ;
     });*/
    // this.listClientsOrgin = this.listClients ;

  }
  deleteUnMesure(unMesure) {
      console.log('call delete !', unMesure );
    this.unMesuresService.deleteUnMesure(unMesure.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData();

      } else {
       // show error message
      }
     });

  }
loadData() {
  this.unMesuresService.getUnMesures().subscribe((unMesures) => {
    this.listUnMesures = unMesures;
    // this.listClients[this.listClients.length] = this.lastClientAdded;
    this.listUnMesuresOrgin = this.listUnMesures;
    this.transformDataToByConsumedByMatTable();
   });
  // this.listClientsOrgin = this.listClients ;
}


checkOneActionInvoked() {
  const listSelectedUnMesures: UnMesure[] = [];
console.log('selected unMesures are ');
console.log(this.selection.selected);
if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((unMesureElement) => {
  return unMesureElement.code;
});
this.listUnMesures.forEach(unMesure => {
  if (codes.findIndex(code => code === unMesure.code) > -1) {
    listSelectedUnMesures.push(unMesure);
  }
  });
}
this.select.emit(listSelectedUnMesures);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listUnMesures !== undefined) {
    this.listUnMesures.forEach(unMesure => {
      listElements.push( {code: unMesure.code ,
       description: unMesure.description,
       unitePrincipale: unMesure.unitePrincipale === undefined ? '' : unMesure.unitePrincipale === true ? 'Oui' : 'Non',
       uniteSecondaire: unMesure.uniteSecondaire === undefined ? '' : unMesure.uniteSecondaire === true ? 'Oui' : 'Non',
     } );
    });
  }
    this.dataUnMesuresSource = new MatTableDataSource<UnMesureElement>(listElements);
    this.dataUnMesuresSource.sort = this.sort;
    this.dataUnMesuresSource.paginator = this.paginator;
}
 specifyListColumnsToBeDisplayed() {
   this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
   {name: 'description' , displayTitle: 'Déscription'},
   {name: 'unitePrincipale' , displayTitle: 'Unité Principale'},
    {name: 'uniteSecondaire' , displayTitle: 'Unité Secondaire'} ];
   this.columnsTitles = this.columnsDefinitions.map((colDef) => {
             return colDef.displayTitle;
   });
   this.columnsNames[0] = 'select';
   this.columnsDefinitions.map((colDef) => {
    this.columnsNames.push(colDef.name);
});
   this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
    return colDef.displayTitleAr;
});
}
masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataUnMesuresSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataUnMesuresSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataUnMesuresSource.filter = filterValue.trim().toLowerCase();
  if (this.dataUnMesuresSource.paginator) {
    this.dataUnMesuresSource.paginator.firstPage();
  }
}
loadAddUnMesureComponent() {
  this.router.navigate(['/pms/stocks/ajouterUnMesure']) ;

}
loadEditUnMesureComponent() {
  if (this.selection.selected.length === 0) {
    this.openSnackBar(' Aucun élément sélectionné' );
    // this.loadData();
    } else
   {
  this.router.navigate(['/pms/stocks/editerUnMesure', this.selection.selected[ this.selection.selected.length - 1].code]) ;
    }
}
supprimerUnMesures()
{
  console.log('NBRE OF ELEMENTS SELECTED');
  console.log(this.selection.selected.length);
  console.log(' ELEMENTS SELECTED');
  console.log(this.selection.selected);
  if ( this.selection.selected.length !== 0) {
      this.selection.selected.forEach(function(unMesure, index) {
          this.unMesuresService.deleteUnMesure(unMesure.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.openSnackBar( this.selection.selected.length + ' Element(s) Supprimé(s)');
             this.loadData();
            }
      }, this);
    } else {
      this.openSnackBar('No Elements Selected');
    }
}
refresh()
{
  this.loadData();

}
  openSnackBar(messageToDisplay) {
 this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
     verticalPosition: 'top'});
}
}
