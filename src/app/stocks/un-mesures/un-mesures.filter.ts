
import { BaseEntity } from '../../common/base-entity.model' ;
// import { Tranche } from './tranche.model' ;
import { Client } from '../../ventes/clients/client.model' ;
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { ArticleService } from '../../stocks/articles/article.service';
import { UnMesure } from '../../stocks/un-mesures/un-mesure.model';
// import { UnMesure } from '../../stocks/un-mesures/un-mesure.model';
// import { Magasin } from '../../stocks/magasins/magasin.model';


export class UnMesureFilter  {
    public selectedCodesUnMesure: string;
    

    private controlCodesUnMesure = new FormControl();
    private controlLibellesUnMesure= new FormControl();

    
   
    
   //  private controlAdresse = new FormControl();
    private listCodesUnMesure: string[] = [];
 
    
    private listLibellesUnMesure: string[] = [];
  

    private filteredCodesUnMesure: Observable<string[]>;
    private filteredLibellesUnMesures: Observable<string[]>;

    constructor ( unMesures: UnMesure[], ) {
      if (unMesures !== null && unMesures !== undefined ) {
       this.listCodesUnMesure = unMesures.map((unM) => (unM.code));

        this.listLibellesUnMesure = unMesures.filter((unM) =>
        (unM.libelle !== undefined && unM.libelle != null )).
        map((unM) => (unM.libelle ));
      }

      this.filteredCodesUnMesure = this.controlCodesUnMesure.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterCodesUnMesures(value))
        );
        this.filteredLibellesUnMesures = this.controlLibellesUnMesure.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterLibellesUnMesures(value))
        );
       
     
 
    }
  
    private _filterCodesUnMesures(value: string): string[] {
      if (value !== undefined) {
        const filterValue = value.toLowerCase();
        return this.listCodesUnMesure.filter(codeUN => codeUN.toLowerCase().includes(filterValue));
      } else {
        return this.listCodesUnMesure;
      }
      }
      private _filterLibellesUnMesures(value: string): string[] {
        if (value !== undefined) {
        const filterValue = value.toLowerCase();
        return this.listLibellesUnMesure.filter(libelleUnM => libelleUnM.toLowerCase().includes(filterValue));
        }  else {
          return this.listLibellesUnMesure;
        }
      }
     
 
      public getControlCodesUnMesures(): FormControl {
        return this.controlCodesUnMesure;
      }
   
      public getControlLibelleUnMesure(): FormControl {
        return this.controlLibellesUnMesure;
      }

      public getFiltredCodesUnMesures(): Observable<string[]> {
        return this.filteredCodesUnMesure;
      }
      public getFiltredLibellesUnMesure(): Observable<string[]> {
        return this.filteredLibellesUnMesures;
      }


}





