// type TypeArticle = 'Marchandise'| 'Produit fini' | 'Matière primaire' | 'Service' | 'Produit semi fini';

export interface UnMesureElement {
         code: string ;
         description: string;
         uniteSecondaire: boolean ;
         unitePrincipale: boolean ;
                }
