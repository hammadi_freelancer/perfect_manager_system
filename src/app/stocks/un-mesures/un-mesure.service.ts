import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';

import {UnMesure} from './un-mesure.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_UNMESURES = 'http://localhost:8082/stocks/unMesures/getUnMesures';
const URL_ADD_UNMESURE = 'http://localhost:8082/stocks/unMesures/addUnMesure';
const URL_GET_UNMESURE = 'http://localhost:8082/stocks/unMesures/getUnMesure';

const URL_UPDATE_UNMESURE = 'http://localhost:8082/stocks/unMesures/updateUnMesure';
const URL_DELETE_UNMESURE = 'http://localhost:8082/stocks/unMesures/deleteUnMesure';
// const URL_GET_FILTRED_LIST_MAGASINS  = 'http://localhost:8082/stocks/magasins/getFiltredArticles';
@Injectable({
  providedIn: 'root'
})
export class UnMesureService {



  constructor(private httpClient: HttpClient) {

   }

   getUnMesures(): Observable<UnMesure[]> {
    return this.httpClient
    .get<UnMesure[]>(URL_GET_LIST_UNMESURES);
   }
   /*getFiltredArticles(filterArticle): Observable<Article[]> {
    return this.httpClient
    .post<Article[]>(URL_GET_FILTRED_LIST_ARTICLES, filterArticle);
   }*/
   addUnMesure(unMesure: UnMesure): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_UNMESURE, unMesure);
  }
  updateUnMesure(unMesure: UnMesure): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_UNMESURE, unMesure);
  }
  deleteUnMesure(codeUnMesure): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_UNMESURE + '/' + codeUnMesure);
  }
  getUnMesure(codeUnMesure): Observable<UnMesure> {
    return this.httpClient.get<UnMesure>(URL_GET_UNMESURE + '/' + codeUnMesure);
  }

}
