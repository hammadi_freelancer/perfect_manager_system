import { Component, OnInit, Input, EventEmitter, Output, AfterContentChecked,
AfterViewChecked
} from '@angular/core';
// import { ArticleService } from './article.service';
import {UnMesure} from './un-mesure.model';
import {UnMesureService} from './un-mesure.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {ActionData} from './action-data.interface';
import {MatSnackBar} from '@angular/material';



@Component({
  selector: 'app-un-mesure-edit',
  templateUrl: './un-mesure-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class UnMesureEditComponent implements OnInit, AfterContentChecked {

// private client: Client = new Client();


private unMesure: UnMesure = new UnMesure();

// @Input()
// private listClients: any = [] ;

@Input()
private actionData: ActionData;


private file: any;

  constructor( private route: ActivatedRoute,
    private router: Router , private unMesureService: UnMesureService, private matSnackBar: MatSnackBar,
   ) {
  }
  ngOnInit() {
 
      const codeUnMesure = this.route.snapshot.paramMap.get('codeUnMesure');
      this.unMesureService.getUnMesure(codeUnMesure).subscribe((unMesure) => {
               this.unMesure = unMesure;
      });
      // this.client = this.actionData.selectedClients[0];
     
  }
  loadAddUnMesureComponent() {
    this.router.navigate(['/pms/stocks/ajouterUnMesure']) ;
  
  }
  loadGridUnMesures() {
    this.router.navigate(['/pms/stocks/unMesures']) ;
  }
  supprimerUnMesure() {
     
      this.unMesureService.deleteUnMesure(this.unMesure.code).subscribe((response) => {

        this.openSnackBar( ' Element Supprimé');
        this.loadGridUnMesures() ;
            });
  }
updateUnMesure() {
  console.log(this.unMesure);
  this.unMesureService.updateUnMesure(this.unMesure).subscribe((response) => {
    if (response.error_message ===  undefined) {
      console.log('the response of update is ', response );
      this.openSnackBar( ' Unité mis à jour ');
      
      //  this.save.emit(response);
      // this.client = new Client();
      // this.modeUpdate = false;
    } else {
      this.openSnackBar( ' Erreurs.. ');
      
     // show error message
    }
});
}
fileChanged($event) {
  this.file = $event.target.files[0];
  const fileReader = new FileReader();
  fileReader.onload = (e) => {
    console.log(fileReader.result);
     this.unMesure.image = fileReader.result;
    // this.filesData[indexFile] = fileData;
  };
  fileReader.readAsDataURL(this.file);
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

ngAfterContentChecked() {

}
}


