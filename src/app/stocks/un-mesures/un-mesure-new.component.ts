import { Component, OnInit, Input, EventEmitter, Output,
    AfterViewChecked
    } from '@angular/core';
    // import { ArticleService } from './article.service';
    import {UnMesure} from './un-mesure.model';
    import {UnMesureService} from './un-mesure.service';
    import {FormControl} from '@angular/forms';
    import {map, startWith} from 'rxjs/operators';
    import {Observable} from 'rxjs';
    import { Router, ActivatedRoute, ParamMap } from '@angular/router';
    import {ActionData} from './action-data.interface';
    import {MatSnackBar} from '@angular/material';
   //  import { TypeArticle } from './type-article.interface';
    
    @Component({
      selector: 'app-un-mesure-new',
      templateUrl: './un-mesure-new.component.html',
      // styleUrls: ['./players.component.css']
    })
export class UnMesureNewComponent implements OnInit {
    private unMesure: UnMesure = new UnMesure();
    @Input()
    private listUnMesures: any = [] ;
    @Input()
    private actionData: ActionData;
    private file: any;

   /* private typesArticle: TypeArticle[] = [
      {value: 'Marchandise', viewValue: 'Marchandise'},
      {value: 'ProduitFini', viewValue: 'Produit Fini'},
      {value: 'ProduitSemiFini', viewValue: 'Produit Semi Fini'},
      {value: 'Service', viewValue: 'Service'},
      {value: 'MatierePrimaire', viewValue: 'Matière Primaire'}
    ];*/
      constructor( private route: ActivatedRoute,
        private router: Router , private unMesureService: UnMesureService, private matSnackBar: MatSnackBar ) {
      }
      ngOnInit() {
     
          console.log('the action data is ');
          console.log(this.actionData);
      }
    saveUnMesure() {
       
        console.log(this.unMesure);
        this.unMesureService.addUnMesure(this.unMesure).subscribe((response) => {
            if (response.error_message ===  undefined) {
              // this.save.emit(response);
              this.unMesure = new UnMesure();
              this.openSnackBar(  'Unité Enregistré');
            } else {
             // show error message
             this.openSnackBar(  'Erreurs!');
             
            }
        });
    }
    loadGridUnMesures() {
      this.router.navigate(['/pms/stocks/unMesures']) ;
    }
    fileChanged($event) {
        this.file = $event.target.files[0];
        const fileReader = new FileReader();
        fileReader.onload = (e) => {
          console.log(fileReader.result);
           this.unMesure.image = fileReader.result;
          // this.filesData[indexFile] = fileData;
        };
        fileReader.readAsDataURL(this.file);
       }
       vider() {
         this.unMesure = new UnMesure();
       }
       openSnackBar(messageToDisplay) {
        this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
            verticalPosition: 'top'});
       }
 
    }
