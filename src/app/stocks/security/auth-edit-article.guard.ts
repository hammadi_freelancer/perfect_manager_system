import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import jwt_decode from 'jwt-decode';

@Injectable({ providedIn: 'root' })
export class AuthGuardEditArticle implements CanActivate {

    constructor(private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (localStorage.getItem('currentUser')) {
            // logged in so return true
            let  decoded = jwt_decode(localStorage.getItem('currentUser'));
            let hasRight = true;
            console.log('access rights in token are ');
            console.log(decoded.userData.accessRights);
            if(decoded.userData.accessRights !== undefined && decoded.userData.accessRights !== null)
                {
                  let accessRightsArticle =  decoded.userData.accessRights.filter( (accessRight)=>
                    (accessRight.entityName ==='Article') );
    
                    if(accessRightsArticle !== null && accessRightsArticle !== undefined)
             {
                let accessRightsEdit=  accessRightsArticle.filter(
                     (accessRightArt)=>
            (
                accessRightArt.rights.filter((right)=>(right==='Modification')).length !== 0) 
            );
            if(accessRightsEdit.length === 0){
                hasRight = false;
    
             }
                }
            }
    

            return hasRight;
        }

        // not logged in so redirect to login page with the return url
       // this.router.navigate(['/pms'], { queryParams: { returnUrl: state.url }});
        this.router.navigate(['/pms'], { });
        
        return false;
    }
}
