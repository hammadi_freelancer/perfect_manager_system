import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import jwt_decode from 'jwt-decode';

@Injectable({ providedIn: 'root' })
export class AuthGuardGridArticle implements CanActivate {

    constructor(private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (localStorage.getItem('currentUser')) {
            // logged in so return true
            let  decoded = jwt_decode(localStorage.getItem('currentUser'));
            let hasRight = true;
            if(decoded.userData.accessRights !== undefined && decoded.userData.accessRights !== null)
                {
                  let accessRightsArticle =  decoded.userData.accessRights.filter( (accessRight)=>
                    (accessRight.entityName ==='Article') );
    
                    if(accessRightsArticle !== null && accessRightsArticle !== undefined)
             {
                let accessRightsGrid=  accessRightsArticle.filter(
                     (accessRightArt)=>
            (
                accessRightArt.rights.filter((right)=>(right==='Visualisation')).length !== 0) 
            );
            if(accessRightsGrid.length === 0){
                hasRight = false;
    
             }
                }
            }
    

            return hasRight;
        }

        // not logged in so redirect to login page with the return url
       // this.router.navigate(['/pms'], { queryParams: { returnUrl: state.url }});
        this.router.navigate(['/pms'], { });
        
        return false;
    }
}
