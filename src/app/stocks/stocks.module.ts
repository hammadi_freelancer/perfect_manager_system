import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
 import { AppComponent } from '../app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ReactiveFormsModule } from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatMenuModule} from '@angular/material/menu';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {TableModule} from 'primeng/table';
import {MatSortModule} from '@angular/material/sort';
import {MatSelectModule} from '@angular/material/select';
import {MatTabsModule} from '@angular/material/tabs';
import {PickListModule} from 'primeng/picklist';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatDialogModule} from '@angular/material/dialog';
import {MultiSelectModule} from 'primeng/multiselect';
import { MAT_DIALOG_DATA} from '@angular/material';
import {MatPaginatorModule} from '@angular/material/paginator';
import { RouterModule, provideRoutes} from '@angular/router';
// import { CommunicationService } from './magasins/communication.service';
import { CommonModule } from '../common/common.module';
import { SharedComponentModule } from '../shared-components/shared-components.module';

import {MatSidenavModule} from '@angular/material/sidenav';
import {MatRadioModule} from '@angular/material/radio';
import {MatExpansionModule} from '@angular/material/expansion';





// import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

//  import { ArticleComponent } from './articles/article.component';
// import { ClientComponent } from './clients/client.component';
// import { PayementComponent } from './payements/payement.component';
// import { PayementsGridComponent} from './payements/payements-grid.component';
// import { ClientsGridComponent} from './clients/clients-grid.component';
// import { ArticlesGridComponent} from './articles/articles-grid.component';
// import { GestionClientsComponent} from './clients/gestion-clients.component';
 
// import { PayementService } from './payements/payement.service';
// import { UtilisateurComponent } from './utilisateurs/utilisateur.component';
import {FormsModule } from '@angular/forms';
// import { commonRoute } from './common.route';
 import {APP_BASE_HREF} from '@angular/common';
 import { stocksRoute} from './stocks.routing' ;
 import { HomeStocksComponent } from './home-stocks.component' ;
 
  import { ArticleNewComponent } from './articles/article-new.component';
  import { ArticleEditComponent } from './articles/article-edit.component';
  import { ArticleHomeComponent } from './articles/article-home.component';
  import { ArticlesGridComponent } from './articles/articles-grid.component';
 import { MagasinsGridComponent } from './magasins/magasins-grid.component';
  import { MagasinHomeComponent } from './magasins/magasin-home.component';
  import { MagasinEditComponent } from './magasins/magasin-edit.component';
  import { MagasinNewComponent } from './magasins/magasin-new.component';
  import { UnMesuresGridComponent } from './un-mesures/un-mesures-grid.component';
  import { UnMesureHomeComponent } from './un-mesures/un-mesure-home.component';
  import { UnMesureEditComponent } from './un-mesures/un-mesure-edit.component';
  import { UnMesureNewComponent } from './un-mesures/un-mesure-new.component';
  import { SchemasConfigurationGridComponent } from './schemas-configuration/schemas-configuration-grid.component';
  import { SchemaConfigurationHomeComponent } from './schemas-configuration/schema-configuration-home.component';
  import { SchemaConfigurationEditComponent } from './schemas-configuration/schema-configuration-edit.component';
  import { SchemaConfigurationNewComponent } from './schemas-configuration/schema-configuration-new.component';
  import { ClassesArticlesGridComponent } from './classes-articles/classes-articles-grid.component';
  import { ClasseArticleHomeComponent } from './classes-articles/classe-article-home.component';
  import { ClasseArticleEditComponent } from './classes-articles/classe-article-edit.component';
  import {  ClasseArticleNewComponent } from './classes-articles/classe-article-new.component';
  
  import { LotsGridComponent } from './lots/lots-grid.component';
  import { LotHomeComponent } from './lots/lot-home.component';
  import { LotEditComponent } from './lots/lot-edit.component';
  import {  LotNewComponent } from './lots/lot-new.component';

  import { MainComponent } from './main.component' ;
 import { MagasinsResolver } from './magasins-resolver';
 import { UnMesuresResolver } from './unMesures-resolver';
 import { ClasseArticleResolver } from './classes-articles-resolver';
 import { SchemasConfigurationResolver } from './schemas-configuration-resolver';



 import { ConfigurationsGridComponent } from './configurations/configurations-grid.component';
 import { DialogAddConfigurationComponent } from './configurations/dialog-add-configuration.component';
 import { DialogEditConfigurationComponent } from './configurations/dialog-edit-configuration.component';
 import { AuthGuardAddArticle  } from './security/auth-add-article.guard';
 import { AuthGuardEditArticle  } from './security/auth-edit-article.guard';
 import { AuthGuardGridArticle  } from './security/auth-grid-article.guard';
 
/*const ENTITY_STATES = [
  ...commonRoute
];*/

 // const BASE_URL = [{provide: APP_BASE_HREF, useValue: '/common'}];
@NgModule({
  declarations: [
    ArticleNewComponent,
    ArticleEditComponent,
    ArticleHomeComponent,
    ArticlesGridComponent,
    MagasinEditComponent,
    MagasinHomeComponent,
    MagasinsGridComponent,
    MagasinNewComponent,
    UnMesureEditComponent,
    UnMesureHomeComponent,
    UnMesuresGridComponent,
    UnMesureNewComponent,
    SchemasConfigurationGridComponent,
    SchemaConfigurationNewComponent,
    SchemaConfigurationEditComponent,
    SchemaConfigurationHomeComponent,
    ClassesArticlesGridComponent,
    ClasseArticleHomeComponent,
    ClasseArticleNewComponent,
    ClasseArticleEditComponent,
    LotsGridComponent,
    LotNewComponent,
    LotEditComponent,
    LotHomeComponent,
    DialogAddConfigurationComponent,
    DialogEditConfigurationComponent,
    ConfigurationsGridComponent,
    HomeStocksComponent
    
  ],
  imports: [
    BrowserModule, HttpClientModule, MatDatepickerModule, MatNativeDateModule,
     BrowserAnimationsModule, ReactiveFormsModule, MatAutocompleteModule,
    MatInputModule, MatCardModule, MatCheckboxModule, MatButtonModule, MatIconModule,
    MatMenuModule, MatProgressSpinnerModule, MatSortModule, MatPaginatorModule, MatTableModule,
    FormsModule, SharedComponentModule, MatSelectModule, MatDatepickerModule, MatTabsModule,
    PickListModule, MatExpansionModule,
    MultiSelectModule,
     MatDialogModule,
     
    MatProgressBarModule,
   //  stocksRoutingModule,
    CommonModule, TableModule, MatSidenavModule, MatRadioModule
],
  providers: [MatNativeDateModule , AuthGuardAddArticle, AuthGuardEditArticle ,
    AuthGuardGridArticle,
     MagasinsResolver, UnMesuresResolver,SchemasConfigurationResolver,
    ClasseArticleResolver  // BASE_URL
  ],
  exports : [
    RouterModule,
    HomeStocksComponent
    // GestionClientsComponent,
    //  GestionDocumentsTypesComponent,
    //  DocumentTypesGridComponent,
   //  DocumentTypeComponent,

  ],
  entryComponents :
   [
    DialogAddConfigurationComponent,
    DialogEditConfigurationComponent,
  ],
  bootstrap: [AppComponent]
})
export class StocksModule { }
