import { Component, OnInit, Output, ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {Lot} from './lot.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {LotService} from './lot.service';
// import { ActionData } from './action-data.interface';
// import { Fournisseur } from '../../ventes/clients/client.model';
import { LotElement } from './lot.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
// import { AlertMessage } from '../../common/alert-message.interface';
// import { OperationRequest } from '../../common/operation-request.interface';
// import { OperationResponse } from '../../common/operation-response.interface';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-lots-grid',
  templateUrl: './lots-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class LotsGridComponent implements OnInit {

  private lot: Lot =  new Lot();
  private selection = new SelectionModel<LotElement>(true, []);
  
@Output()
select: EventEmitter<Lot[]> = new EventEmitter<Lot[]>();

//private lastClientAdded: Client;

 selectedLot: Lot ;

 @Input()
 private listLots: any = [] ;
 listLotsOrgin: any = [];

 private checkedAll: false;

 private  dataLotsSource: MatTableDataSource<LotElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
  constructor( private route: ActivatedRoute, private lotsService: LotService,
    private router: Router, private matSnackBar: MatSnackBar ) {
  }
  ngOnInit() {
    this.lotsService.getLots().subscribe((lots) => {
      this.listLots = lots;
      // this.listClients[this.listClients.length] = this.lastClientAdded;
      // this.listClientsOrgin = this.listClients ;
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });
   /* this.creditsService.getCredits().subscribe((credits) => {
      console.log('list credits ', credits);
      // this.listCredits = credits;
      this.listCredits =  this.listCredits.map((credit) => {
        if (credit.client === 'undefined' || credit.client === null ) {
           credit.client = new Client();
         }
         return credit;
   });      this.listCreditsOrgin = this.listCredits ;
     });*/
    // this.listClientsOrgin = this.listClients ;

  }
  deleteLot(lot) {
    this.lotsService.deleteLot(lot.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
     
        this.loadData();

      } else {
       // show error message
      }
     });

  }
loadData() {
  this.lotsService.getLots().subscribe((lots) => {
    this.listLots = lots;
    // this.listClients[this.listClients.length] = this.lastClientAdded;
    this.listLotsOrgin = this.listLots;
    this.transformDataToByConsumedByMatTable();
   });
  // this.listClientsOrgin = this.listClients ;
}


checkOneActionInvoked() {
  const listSelectedLots: Lot[] = [];
console.log('selected unMesures are ');
console.log(this.selection.selected);
if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((lotElement) => {
  return lotElement.code;
});
this.listLots.forEach(lot => {
  if (codes.findIndex(code => code === lot.code) > -1) {
    listSelectedLots.push(lot);
  }
  });
}
this.select.emit(listSelectedLots);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listLots !== undefined) {
    this.listLots.forEach(lot => {

      listElements.push( {
        code: lot.code ,
        libelle: lot.libelle,
       description: lot.description,
     //   unitePrincipale: unMesure.unitePrincipale === undefined ? '' : unMesure.unitePrincipale === true ? 'Oui' : 'Non',
      //  uniteSecondaire: unMesure.uniteSecondaire === undefined ? '' : unMesure.uniteSecondaire === true ? 'Oui' : 'Non',
     } );
    });
  }
    this.dataLotsSource = new MatTableDataSource<LotElement>(listElements);
    this.dataLotsSource.sort = this.sort;
    this.dataLotsSource.paginator = this.paginator;
}
 specifyListColumnsToBeDisplayed() {
   this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
   {name: 'description' , displayTitle: 'Déscription'},
   {name: 'libelle' , displayTitle: 'libelle'}];
   this.columnsTitles = this.columnsDefinitions.map((colDef) => {
             return colDef.displayTitle;
   });
   this.columnsNames[0] = 'select';
   this.columnsDefinitions.map((colDef) => {
    this.columnsNames.push(colDef.name);
});
   this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
    return colDef.displayTitleAr;
});
}
masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataLotsSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataLotsSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataLotsSource.filter = filterValue.trim().toLowerCase();
  if (this.dataLotsSource.paginator) {
    this.dataLotsSource.paginator.firstPage();
  }
}
loadAddLotComponent() {
  this.router.navigate(['/pms/stocks/ajouterLot']) ;

}
loadEditLotComponent() {
  if (this.selection.selected.length === 0) {
    this.openSnackBar(' Aucun Lot sélectionné' );
    // this.loadData();
    } else
   {
  this.router.navigate(['/pms/stocks/editerLot', this.selection.selected[ this.selection.selected.length - 1].code]) ;
    }
}
supprimerLots()
{

  if ( this.selection.selected.length !== 0) {
      this.selection.selected.forEach(function(lot, index) {
          this.lotsService.deleteLot(lot.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.openSnackBar( ' Lot(s) Supprimé(s)');
             this.loadData();
            }
      }, this);
    } else {
      this.openSnackBar('Aucun Lot Séléctionné');
    }
}
refresh()
{
  this.loadData();

}
  openSnackBar(messageToDisplay) {
 this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
     verticalPosition: 'top'});
}
}
