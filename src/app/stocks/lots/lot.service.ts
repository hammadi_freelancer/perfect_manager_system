import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';

import {Lot} from './lot.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_LOTS = 'http://localhost:8082/stocks/lots/getLots';
const URL_ADD_LOT = 'http://localhost:8082/stocks/lots/addLot';
const URL_GET_LOT = 'http://localhost:8082/stocks/lots/getLot';

const URL_UPDATE_LOT = 'http://localhost:8082/stocks/lots/updateLot';
const URL_DELETE_LOT = 'http://localhost:8082/stocks/lots/deleteLot';
// const URL_GET_FILTRED_LIST_MAGASINS  = 'http://localhost:8082/stocks/magasins/getFiltredArticles';
@Injectable({
  providedIn: 'root'
})
export class LotService {



  constructor(private httpClient: HttpClient) {

   }

   getLots(): Observable<Lot[]> {
    return this.httpClient
    .get<Lot[]>(URL_GET_LIST_LOTS);
   }
   /*getFiltredArticles(filterArticle): Observable<Article[]> {
    return this.httpClient
    .post<Article[]>(URL_GET_FILTRED_LIST_ARTICLES, filterArticle);
   }*/
   addLot(lot: Lot): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_LOT, lot);
  }
  updateLot(lot: Lot): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_LOT, lot);
  }
  deleteLot(codeLot): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_LOT + '/' + codeLot);
  }
  getLot(codeLot): Observable<Lot> {
    return this.httpClient.get<Lot>(URL_GET_LOT + '/' + codeLot);
  }

}
