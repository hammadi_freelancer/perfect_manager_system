
import { BaseEntity } from '../../common/base-entity.model' ;
// import { Tranche } from './tranche.model' ;
import { Client } from '../../ventes/clients/client.model' ;
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { ArticleService } from '../../stocks/articles/article.service';
import { Lot } from '../../stocks/lots/lot.model';
// import { UnMesure } from '../../stocks/un-mesures/un-mesure.model';
// import { Magasin } from '../../stocks/magasins/magasin.model';


export class LotFilter  {
    public selectedCodesLots: string;
    

    private controlCodesLots = new FormControl();
    private controlLibellesLots = new FormControl();

    
   
    
   //  private controlAdresse = new FormControl();
    private listCodesLots: string[] = [];
 
    
    private listLibellesLots: string[] = [];
  

    private filteredCodesLots: Observable<string[]>;
    private filteredLibellesLots: Observable<string[]>;

    constructor ( lots: Lot[], ) {
      if (lots !== null) {
       this.listCodesLots = lots.map((lot) => (lot.code));
        this.listLibellesLots = lots.filter((lot) =>
        (lot.libelle !== undefined && lot.libelle != null )).
        map((lot) => (lot.libelle ));
      }

      this.filteredCodesLots = this.controlCodesLots.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterCodesLots(value))
        );
        this.filteredLibellesLots = this.controlLibellesLots.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterLibellesLots(value))
        );
       
     
 
    }
  
    private _filterCodesLots(value: string): string[] {
        const filterValue = value.toLowerCase();
        return this.listCodesLots.filter(codeUN => codeUN.toLowerCase().includes(filterValue));
      }
      private _filterLibellesLots(value: string): string[] {
        const filterValue = value.toLowerCase();
        return this.listLibellesLots.filter(libelleUnM => libelleUnM.toLowerCase().includes(filterValue));
      }
     
 
      public getControlCodesLots(): FormControl {
        return this.controlCodesLots;
      }
   
      public getControlLibellesLots(): FormControl {
        return this.controlLibellesLots;
      }

      public getFiltredCodesLots(): Observable<string[]> {
        return this.filteredCodesLots;
      }
      public getFiltredLibellesLots(): Observable<string[]> {
        return this.filteredLibellesLots;
      }


}





