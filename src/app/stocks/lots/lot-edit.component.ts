import { Component, OnInit, Input, EventEmitter, Output, AfterContentChecked,
AfterViewChecked
} from '@angular/core';
// import { ArticleService } from './article.service';
import {Lot} from './lot.model';
import {LotService} from './lot.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
// import {ActionData} from './action-data.interface';
import {MatSnackBar} from '@angular/material';



@Component({
  selector: 'app-lot-edit',
  templateUrl: './lot-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class LotEditComponent implements OnInit, AfterContentChecked {

// private client: Client = new Client();


private lot: Lot = new Lot();




private file: any;

  constructor( private route: ActivatedRoute,
    private router: Router , private lotService: LotService, private matSnackBar: MatSnackBar,
   ) {
  }
  ngOnInit() {
 
      const codeLot= this.route.snapshot.paramMap.get('codeLot');
      this.lotService.getLot(codeLot).subscribe((lot) => {
               this.lot = lot;
      });
      // this.client = this.actionData.selectedClients[0];
     
  }

  loadGridLots() {
    this.router.navigate(['/pms/stocks/lots']) ;
  }

updateLot() {
  console.log(this.lot);
  this.lotService.updateLot(this.lot).subscribe((response) => {
    if (response.error_message ===  undefined) {
      console.log('the response of update is ', response );
      this.openSnackBar( ' Lot mis à jour ');
      
      //  this.save.emit(response);
      // this.client = new Client();
      // this.modeUpdate = false;
    } else {
      this.openSnackBar( ' Erreurs.. ');
      
     // show error message
    }
});
}
fileChanged($event) {
  this.file = $event.target.files[0];
  const fileReader = new FileReader();
  fileReader.onload = (e) => {
    console.log(fileReader.result);
     this.lot.image = fileReader.result;
    // this.filesData[indexFile] = fileData;
  };
  fileReader.readAsDataURL(this.file);
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

ngAfterContentChecked() {

}
}


