import { Component, OnInit, Input, EventEmitter, Output,
    AfterViewChecked
    } from '@angular/core';
    // import { ArticleService } from './article.service';
    import {Lot} from './lot.model';
    import {LotService} from './lot.service';
    import {FormControl} from '@angular/forms';
    import {map, startWith} from 'rxjs/operators';
    import {Observable} from 'rxjs';
    import { Router, ActivatedRoute, ParamMap } from '@angular/router';
  //   import {ActionData} from './action-data.interface';
    import {MatSnackBar} from '@angular/material';
   //  import { TypeArticle } from './type-article.interface';
    
    @Component({
      selector: 'app-lot-new',
      templateUrl: './lot-new.component.html',
      // styleUrls: ['./players.component.css']
    })
export class LotNewComponent implements OnInit {
    private lot: Lot = new Lot();
    @Input()
    private listLots: any = [] ;
  
    private file: any;

      constructor( private route: ActivatedRoute,
        private router: Router , private lotService: LotService, private matSnackBar: MatSnackBar ) {
      }
      ngOnInit() {
     
      
      }
    saveLot() {
       
        this.lotService.addLot(this.lot).subscribe((response) => {
            if (response.error_message ===  undefined) {
              // this.save.emit(response);
              this.lot = new Lot();
              this.openSnackBar(  'Lot Enregistré');
            } else {
             // show error message
             this.openSnackBar(  'Erreurs!');
             
            }
        });
    }
    loadGridLots() {
      this.router.navigate(['/pms/stocks/lots']) ;
    }
    fileChanged($event) {
        this.file = $event.target.files[0];
        const fileReader = new FileReader();
        fileReader.onload = (e) => {
          console.log(fileReader.result);
           this.lot.image = fileReader.result;
          // this.filesData[indexFile] = fileData;
        };
        fileReader.readAsDataURL(this.file);
       }
       vider() {
         this.lot = new Lot();
       }
       openSnackBar(messageToDisplay) {
        this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
            verticalPosition: 'top'});
       }
 
    }
