

import { BaseEntity } from '../../common/base-entity.model' ;

// type TypeArticle = 'Marchandise'| 'ProduitFini' | 'MatierePrimaire' | 'Service' | 'ProduitSemiFini';

export class Lot extends BaseEntity {
   constructor(
       public code?: string,
    public libelle?: string,
    public image?: any,
) {
    super();
   //  this.categoryClient = 'PERSONNE_PHYSIQUE'; // other possible value : PERSONNE_MORALE
    // this.checked = false;
    this.image = undefined;
}
}
