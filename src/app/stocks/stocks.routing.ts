import { Injectable } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { ArticlesGridComponent } from './articles/articles-grid.component';
import { ArticleNewComponent } from './articles/article-new.component';
import { ArticleEditComponent } from './articles/article-edit.component';
import { MagasinsGridComponent } from './magasins/magasins-grid.component';
import { MagasinNewComponent } from './magasins/magasin-new.component';
import { MagasinEditComponent } from './magasins/magasin-edit.component';
 import { MagasinsResolver } from './magasins-resolver';
 import { UnMesuresResolver } from './unMesures-resolver';
 import { ClasseArticleResolver } from './classes-articles-resolver';
 import { SchemasConfigurationResolver } from './schemas-configuration-resolver';
 
 import { UnMesuresGridComponent } from './un-mesures/un-mesures-grid.component';
 import { UnMesureHomeComponent } from './un-mesures/un-mesure-home.component';
 import { UnMesureEditComponent } from './un-mesures/un-mesure-edit.component';
 import { UnMesureNewComponent } from './un-mesures/un-mesure-new.component';

 import { SchemasConfigurationGridComponent } from './schemas-configuration/schemas-configuration-grid.component';
 import { SchemaConfigurationHomeComponent } from './schemas-configuration/schema-configuration-home.component';
 import { SchemaConfigurationEditComponent } from './schemas-configuration/schema-configuration-edit.component';
 import { SchemaConfigurationNewComponent } from './schemas-configuration/schema-configuration-new.component';

 import { ClassesArticlesGridComponent } from './classes-articles/classes-articles-grid.component';
 import { ClasseArticleHomeComponent } from './classes-articles/classe-article-home.component';
 import { ClasseArticleEditComponent } from './classes-articles/classe-article-edit.component';
 import {  ClasseArticleNewComponent } from './classes-articles/classe-article-new.component';


 import { LotsGridComponent } from './lots/lots-grid.component';
 import { LotHomeComponent } from './lots/lot-home.component';
 import { LotEditComponent } from './lots/lot-edit.component';
 import {  LotNewComponent } from './lots/lot-new.component';


 import { AuthGuardAddArticle  } from './security/auth-add-article.guard';
 import { AuthGuardEditArticle  } from './security/auth-edit-article.guard';
 import { AuthGuardGridArticle  } from './security/auth-grid-article.guard';

 import {ModuleWithProviders  } from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';

 export const stocksRoute: Routes = [
    {
        path: 'articles',
        component: ArticlesGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
     canActivate: [AuthGuardGridArticle]
    },
    {
        path: 'ajouterArticle',
        component: ArticleNewComponent,
        resolve: { magasins: MagasinsResolver,
             unMesures : UnMesuresResolver,
            classesArticles: ClasseArticleResolver,
            schemasConfiguration : SchemasConfigurationResolver
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
     canActivate: [AuthGuardAddArticle]
    },
    {
        path: 'editerArticle/:codeArticle',
        component: ArticleEditComponent,
        resolve: { magasins: MagasinsResolver, unMesures : UnMesuresResolver,
            classesArticles: ClasseArticleResolver,
            schemasConfiguration : SchemasConfigurationResolver
            
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
     canActivate: [AuthGuardEditArticle]
    },
    {
        path: 'magasins',
        component: MagasinsGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterMagasin',
        component: MagasinNewComponent,
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerMagasin/:codeMagasin',
        component: MagasinEditComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'unMesures',
        component: UnMesuresGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterUnMesure',
        component: UnMesureNewComponent,
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerUnMesure/:codeUnMesure',
        component: UnMesureEditComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'schemasConfiguration',
        component: SchemasConfigurationGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterSchemaConfiguration',
        component: SchemaConfigurationNewComponent,
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerSchemaConfiguration/:codeSchemaConfiguration',
        component: SchemaConfigurationEditComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },

    {
        path: 'classesArticles',
        component: ClassesArticlesGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterClasseArticle',
        component: ClasseArticleNewComponent,
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        //canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerClasseArticle/:codeClasseArticle',
        component: ClasseArticleEditComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'lots',
        component: LotsGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterLot',
        component: LotNewComponent,
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerLot/:codeLot',
        component: LotEditComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    }
];



