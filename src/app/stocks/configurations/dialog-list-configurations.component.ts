
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ConfigurationService} from './configuration.service';
import {Configuration} from './configuration.model';
import { ConfigurationElement } from './configuration.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-configurations',
    templateUrl: 'dialog-list-configurations.component.html',
  })
  export class DialogListConfigurationsComponent implements OnInit {
     private listConfigurations: Configuration[] = [];
     private  dataConfigurationsSource: MatTableDataSource<ConfigurationElement>;
     private selection = new SelectionModel<ConfigurationElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private configurationService: ConfigurationService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      this.configurationService.getConfigurationsByCodeArticle(this.data.codeArticle)
      .subscribe((listCfgs) => {
        this.listConfigurations = listCfgs;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedConfigurations: Configuration[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((cfgElement) => {
    return cfgElement.code;
  });
  this.listConfigurations.forEach(cfg => {
    if (codes.findIndex(code => code === cfg.code) > -1) {
      listSelectedConfigurations.push(cfg);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listConfigurations !== undefined) {
    
      this.listConfigurations.forEach(cfg => {

        let caracts = '';
                   for (const prop in cfg) {
                      if ( prop !== 'code' &&  prop !== 'schemaConfiguration' &&
                        prop !== 'codeArticle' && prop !== 'description'
                      && prop !== 'imageFace1'
                      && prop !== 'imageFace2'
                      ) {
                        caracts = caracts + prop + ':' + cfg[prop] + ';';
                      } 
                }
             listElements.push( {code: cfg.code ,
          schemaConfiguration: cfg.schemaConfiguration.code,
          caracteristiques : caracts,
          
          description: cfg.description
        } );
      });
    }
      this.dataConfigurationsSource = new MatTableDataSource<ConfigurationElement>(listElements);
      this.dataConfigurationsSource.sort = this.sort;
      this.dataConfigurationsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
     this.columnsDefinitions = [
       {name: 'code' , displayTitle: 'Code'},
     {name: 'schemaConfiguration' , displayTitle: 'Sch. Config.'},
     {name: 'caracteristiques' , displayTitle: 'Caractéristiques'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataConfigurationsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataConfigurationsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataConfigurationsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataConfigurationsSource.paginator) {
      this.dataConfigurationsSource.paginator.firstPage();
    }
  }


}
