

import { BaseEntity } from '../../common/base-entity.model' ;
import { SchemaConfiguration } from '../schemas-configuration/schema-configuration.model' ;

// type TypeArticle = 'Marchandise'| 'ProduitFini' | 'MatierePrimaire' | 'Service' | 'ProduitSemiFini';

export class Configuration extends BaseEntity {
   constructor(
    public code?: string,
    public schemaConfiguration?: SchemaConfiguration,
    public codeArticle?: string,
    public imageFace1?: any,
    public imageFace2?: any,
    
) {
    super();
   //  this.categoryClient = 'PERSONNE_PHYSIQUE'; // other possible value : PERSONNE_MORALE
    // this.checked = false;
    this.imageFace1 = undefined;
    this.imageFace2 = undefined;
    
}
}
