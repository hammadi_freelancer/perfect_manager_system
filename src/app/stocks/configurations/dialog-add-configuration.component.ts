
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ConfigurationService} from './configuration.service';
import {Configuration} from './configuration.model';
import {SchemaConfiguration} from '../schemas-configuration/schema-configuration.model';

@Component({
    selector: 'app-dialog-add-configuration',
    templateUrl: 'dialog-add-configuration.component.html',
  })
  export class DialogAddConfigurationComponent implements OnInit {
     private configuration: Configuration;
     private schemaConfiguration : SchemaConfiguration;
     
     private saveEnCours = false;
     constructor(  private configurationService: ConfigurationService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.configuration = new Configuration();
           this.schemaConfiguration = this.data.schemaConfiguration;
    }
    saveConfiguration() 
    {
      this.configuration.codeArticle = this.data.codeArticle;
      this.configuration.schemaConfiguration = this.data.schemaConfiguration;
      
      this.saveEnCours = true;
    
       this.configurationService.addConfiguration(this.configuration).subscribe((response) => {
        this.saveEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.configuration = new Configuration();
             this.openSnackBar(  'Configuration Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
