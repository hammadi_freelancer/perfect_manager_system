// type TypeArticle = 'Marchandise'| 'Produit fini' | 'Matière primaire' | 'Service' | 'Produit semi fini';

export interface ConfigurationElement {
         code: string ;
         codeArticle: string;
         description: string;
         schemaConfiguration: string;
         caracteristiques : string;
        }
