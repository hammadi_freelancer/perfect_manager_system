import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';

import {Configuration} from './configuration.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_CONFIGURATIONS = 'http://localhost:8082/stocks/configurations/getConfigurations';
const URL_ADD_CONFIGURATION = 'http://localhost:8082/stocks/configurations/addConfiguration';
const URL_GET_CONFIGURATION = 'http://localhost:8082/stocks/configurations/getConfiguration';

const URL_UPDATE_CONFIGURATION = 'http://localhost:8082/stocks/configurations/updateConfiguration';
const URL_DELETE_CONFIGURATION = 'http://localhost:8082/stocks/configurations/deleteConfiguration';
const URL_GET_LIST_CONFIGURATIONS_BY_CODE_ARTICLE =
'http://localhost:8082/stocks/configurations/getConfigurationsByCodeArticle';
// const URL_GET_FILTRED_LIST_MAGASINS  = 'http://localhost:8082/stocks/magasins/getFiltredArticles';
@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {



  constructor(private httpClient: HttpClient) {

   }

   getConfigurations(): Observable<Configuration[]> {
    return this.httpClient
    .get<Configuration[]>(URL_GET_LIST_CONFIGURATIONS);
   }
   /*getFiltredArticles(filterArticle): Observable<Article[]> {
    return this.httpClient
    .post<Article[]>(URL_GET_FILTRED_LIST_ARTICLES, filterArticle);
   }*/
   addConfiguration(configuration: Configuration): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_CONFIGURATION, configuration);
  }
  updateConfiguration(configuration: Configuration): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_CONFIGURATION, configuration);
  }
  deleteConfiguration(codeCfg): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_CONFIGURATION + '/' + codeCfg);
  }
  getConfiguration(codeConfiguration): Observable<Configuration> {
    return this.httpClient.get<Configuration>(URL_GET_CONFIGURATION+ '/' + codeConfiguration);
  }
  getConfigurationsByCodeArticle(codeArticle): Observable<Configuration[]> {
    return this.httpClient
    .get<Configuration[]>(URL_GET_LIST_CONFIGURATIONS_BY_CODE_ARTICLE + '/' + codeArticle);
   }
}
