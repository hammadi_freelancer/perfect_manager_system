
import { Component, OnInit, Inject, ViewChild, Input, OnChanges} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ConfigurationService} from './configuration.service';
import { Configuration} from './configuration.model';
import { Article} from '../articles/article.model';

import { ConfigurationElement } from './configuration.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import { DialogAddConfigurationComponent } from './dialog-add-configuration.component';
import { DialogEditConfigurationComponent} from './dialog-edit-configuration.component';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-configurations-grid',
    templateUrl: 'configurations-grid.component.html',
  })
  export class ConfigurationsGridComponent implements OnInit, OnChanges {
     private listConfigurations: Configuration[] = [];
     private  dataConfigurationsSource: MatTableDataSource<ConfigurationElement>;
     private selection = new SelectionModel<ConfigurationElement>(true, []);

     @Input()
     private article : Article;

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private configurationService: ConfigurationService,
     private matSnackBar: MatSnackBar, public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.configurationService.getConfigurationsByCodeArticle(this.article.code).
      subscribe((listConfgs) => {
        this.listConfigurations = listConfgs;
        this.transformDataToByConsumedByMatTable();
         });

  }

ngOnChanges()
{
  this.configurationService.getConfigurationsByCodeArticle
  (this.article.code).subscribe((listConfgs) => {
    this.listConfigurations = listConfgs;
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
     });
}
  checkOneActionInvoked() {
    const listSelectedConfigurations: Configuration[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((cfgElement) => {
    return cfgElement.code;
  });
  this.listConfigurations.forEach(cgf => {
    if (codes.findIndex(code => code === cgf.code) > -1) {
      listSelectedConfigurations.push(cgf);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listConfigurations !== undefined) {
    
      this.listConfigurations.forEach(cfg => {

        let caracts = '';
                   for (const prop in cfg) {
                      if ( prop !== 'code' &&  prop !== 'schemaConfiguration' &&
                        prop !== 'codeArticle' && prop !== 'description'
                      && prop !== 'imageFace1'
                      && prop !== 'imageFace2'
                      ) {
                        caracts = caracts + prop + ':' + cfg[prop] + ';';
                      } 
                }
             listElements.push( {code: cfg.code ,
          schemaConfiguration: cfg.schemaConfiguration.code,
          caracteristiques : caracts,
          
          description: cfg.description
        } );
      });
    }
      this.dataConfigurationsSource = new MatTableDataSource<ConfigurationElement>(listElements);
      this.dataConfigurationsSource.sort = this.sort;
      this.dataConfigurationsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
     this.columnsDefinitions = [
       {name: 'code' , displayTitle: 'Code'},
     {name: 'schemaConfiguration' , displayTitle: 'Sch. Config.'},
     {name: 'caracteristiques' , displayTitle: 'Caractéristiques'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataConfigurationsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataConfigurationsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataConfigurationsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataConfigurationsSource.paginator) {
      this.dataConfigurationsSource.paginator.firstPage();
    }
  }

  showAddConfigurationDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeArticle: this.article.code,
     schemaConfiguration: this.article.schemaConfiguration
   };
 
   const dialogRef = this.dialog.open(DialogAddConfigurationComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(cfgAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditConfigurationDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeConfiguration : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditConfigurationComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(cfgAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucune Configuration Séléctionnée!');
    }
    }
    supprimerConfigurations()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(config, index) {
              this.configurationService.deleteConfiguration(config.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Configuration Séléctionnée!');
        }
    }
    refresh() {
      this.configurationService.getConfigurationsByCodeArticle(this.article.code).subscribe((listCfg) => {
        this.listConfigurations= listCfg;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }


}
