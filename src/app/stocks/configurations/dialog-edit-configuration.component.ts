
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ConfigurationService} from './configuration.service';
import {Configuration} from './configuration.model';

@Component({
    selector: 'app-dialog-edit-configuration',
    templateUrl: 'dialog-edit-configuration.component.html',
  })
  export class DialogEditConfigurationComponent implements OnInit {
     private configuration: Configuration;
     private updateEnCours = false;
     
     constructor(  private configurationService: ConfigurationService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {

        this.configurationService.getConfiguration(this.data.codeConfiguration).subscribe((cfg) => {
            this.configuration = cfg;
     });


    }
    updateConfiguration() 
    {
     this.updateEnCours = true;
       this.configurationService.updateConfiguration(this.configuration).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.openSnackBar(  'Configuration Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
