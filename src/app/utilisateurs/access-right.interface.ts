// type Motif = 'VenteEspece'| 'VenteVirement' | 'PayementCredit' | 'VirementCredit' | 'FinancementInterne' | 'FinancementExterne';
 type ModuleNames = 'Stocks'| 'Utilisateurs' | 'Achats' | 'Ventes' |
  'Production' | 'Caisse' | 'RH';

  type EntityNames = 'Articles'| 'Magasins' | 'UnMesures' | 'SchemasConfigurations' |
  'Entrees' | 'Sorties' | 'Utilisateurs' | 'DroitsAcces' |  'Groupes'  |   'FacturesAchats' |
  'FacturesVentes' | 'CreditsAchats' | 'CreditsVentes' | 'Fournisseurs' | 'Clients' |
   'Journaux' | 'OperationsCourantes' | 'ComptesFournisseurs' | 'ComptesClients' | 'RelevesAchats'
   | 'RelevesVentes' | 'DemandesAchats' | 'DemandesVentes' | 'BonsLivraisons' |'BonsReceptions' ;

  type RightsNames = 'Ajouter'| 'Editer' | 'Visualiser' | 'Supprimer' ;
export interface AccessRightElement {
         code: string ;
         entityName: string ;
         rights: RightsNames[];
        }
