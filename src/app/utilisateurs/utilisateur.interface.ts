// type Motif = 'VenteEspece'| 'VenteVirement' | 'PayementCredit' | 'VirementCredit' | 'FinancementInterne' | 'FinancementExterne';

export interface UtilisateurElement {
         code: string ;
         description: string;
         nom: string;
         cin: string;
         dateNaissance : string;
         dateInscription : string;
         prenom: string;
        email : string;
        adresse: string;
        mobile : string;
         role: string;
         login: string;
         etat: string;
        }
