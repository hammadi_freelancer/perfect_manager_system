
import { BaseEntity } from '../common/base-entity.model' ;
type ELEMENT_SI =
 'EXCEL_ACCESS' | 'LOGICELS_DESKTOP' | 'SERVICES_WEB' | 'AUTRES' | 'PAPIERS';

 type ETAT_INSCRIPTION = 'VALIDE' | 'ANNULE' | 'CONFIRME';
export class Inscription extends BaseEntity {
  constructor(  public code?: string,
    public numeroInscription?: string,
    public raisonSociale?: string,
    public chiffreAffaires?: number,
    public adresse?: string,
    public contact?: string,
    public  elementsSI?: string[],
    public budgetPrepare?: number,
     public besoinUrgent?: boolean,
     public logo?: any,
     public map?: any,
     public email?: string,
     public password?: string,
     public dateInscription?: string,
     public etat?: ETAT_INSCRIPTION,

  ) {
    super();
  }
}
