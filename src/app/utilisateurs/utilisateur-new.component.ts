import { Component, OnInit, Input, EventEmitter, Output,
    AfterViewChecked
    } from '@angular/core';
    // import { ArticleService } from './article.service';
    import {Utilisateur} from './utilisateur.model';
    import {UtilisateurService} from './utilisateur.service';
    import {ProfilesService} from '../profiles/profiles.service';
    
    import {FormControl} from '@angular/forms';
    import {map, startWith} from 'rxjs/operators';
    import {Observable} from 'rxjs';
    import { Router, ActivatedRoute, ParamMap } from '@angular/router';
    import {MatSnackBar} from '@angular/material';
    import { SelectValues } from './select-values.interface';
    import {ProfileAchatsFilter} from './profiles-achats.filter';
    import {ProfileVentesFilter} from './profiles-ventes.filter';
    
    @Component({
      selector: 'app-utilisateur-new',
      templateUrl: './utilisateur-new.component.html',
      // styleUrls: ['./players.component.css']
    })
export class UtilisateurNewComponent implements OnInit {
    private utilisateur: Utilisateur = new Utilisateur();
    @Input()
    private listUtilisateurs: any = [] ;
    private file: any;
    private profileAchatsFilter : ProfileAchatsFilter;
    private profileVentesFilter : ProfileVentesFilter;
    private listProfilesAchats: any;
    private listProfilesVentes: any;
    
    private rolesUtilisateur: SelectValues[] = [
      {value: 'SuperAdmin', viewValue: 'Super Administrateur'},
      
      {value: 'Admin', viewValue: 'Administrateur'},
      {value: 'Uilisateur', viewValue: 'Uilisateur'},
      {value: 'Visiteur', viewValue: 'Visiteur'},
    
    ];
    private etatsUtilisateur: SelectValues[] = [
      {value: 'Active', viewValue: 'Activé'},
      {value: 'Desactive', viewValue: 'Désactivé'},
      {value: 'EnCours', viewValue: 'En Cours'}
    
    ];
      constructor( private route: ActivatedRoute,
        private router: Router , private profilesService: ProfilesService,
        private utilisateurService: UtilisateurService, private matSnackBar: MatSnackBar ) {
      }
      ngOnInit() {
     
          console.log('the action data is ');
          this.listProfilesAchats= this.route.snapshot.data.profilesAchats;
          this.listProfilesVentes= this.route.snapshot.data.profilesVentes;
          this.profileAchatsFilter = new ProfileAchatsFilter( this.listProfilesAchats);
          this.profileVentesFilter = new ProfileVentesFilter(this.listProfilesVentes);
      }
    saveUtilisateur() {
       if (this.utilisateur.confirmPassword !== this.utilisateur.password)
        {
          this.openSnackBar(  'Le mot de passe ne correspond pas à la confirmation');
        } else {
        console.log(this.utilisateur);
        this.utilisateurService.addUtilisateur(this.utilisateur).subscribe((response) => {
            if (response.error_message ===  undefined) {
              // this.save.emit(response);
              this.utilisateur = new Utilisateur();
              this.openSnackBar(  'Utilisateur Enregistré');
            } else {
             // show error message
             this.openSnackBar(  'Erreurs!');
             
            }
        });
      }
    }
    loadGridUtilisateurs() {
      this.router.navigate(['/pms/administration/utilisateurs']) ;
    }
    fileChanged($event) {
        this.file = $event.target.files[0];
        const fileReader = new FileReader();
        fileReader.onload = (e) => {
          console.log(fileReader.result);
          // this.article.image = fileReader.result;
          // this.filesData[indexFile] = fileData;
          this.utilisateur.image = fileReader.result;
          
        };
        fileReader.readAsDataURL(this.file);
       }
       vider() {
         this.utilisateur = new Utilisateur();
       }
       openSnackBar(messageToDisplay) {
        this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
            verticalPosition: 'top'});
       }
       fillDataDescriptionProfileAchats(){
         this.listProfilesAchats.forEach((profileAc, index) => {
                       if (this.utilisateur.profileAchats.code === profileAc.code) {
                        this.utilisateur.profileAchats.description = profileAc.description;
                        }
          
                  } , this);
         
       }
       fillDataDescriptionProfileVentes()
       {
        this.listProfilesVentes.forEach((profileV, index) => {
          if (this.utilisateur.profileVentes.code === profileV.code) {
           this.utilisateur.profileVentes.description = profileV.description;
           }

     } , this);
       }
    }
