
import { BaseEntity } from '../common/base-entity.model' ;
import {AccessRight } from './access-right.model';
type TypeEtat = 'Active' | 'En Cours' | 'Desactive';
import {ProfileAchats } from '../profiles/achats/profile-achats.model';
import {ProfileVentes } from '../profiles/ventes/profile-ventes.model';

type USER_ROLE = 'ADMIN' | 'INVITE' | 'UTILISATEUR';
export class Utilisateur extends BaseEntity {
  constructor(  public code?: string,
    public nom?: string,
    public prenom?: string,
    public cin?: string,
    public adresse?: string,
    public mobile?: string,
    public profileAchats?: ProfileAchats,
    public profileVentes?: ProfileVentes,
    public login?: string,
    public email?: string,
    
    public accessRights?: AccessRight[],
    public role?: USER_ROLE,
    public password?: string,
    public etat?: TypeEtat,
     public confirmPassword?: string,
     public image?: any
  ) {
    super();
    this.accessRights = [];
    this.profileAchats = new ProfileAchats();
    this.profileVentes = new ProfileVentes();
    // this.identite = {adresse: {}, { } }
  }
}
