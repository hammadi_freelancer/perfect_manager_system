import { Component, OnInit, Input, EventEmitter, Output,
  AfterViewChecked
  } from '@angular/core';
  // import { ArticleService } from './article.service';
  import {AccessRight} from './access-right.model';
  import {UtilisateurService } from './utilisateur.service';
  import {FormControl} from '@angular/forms';
  import {map, startWith} from 'rxjs/operators';
  import {Observable} from 'rxjs';
  import { Router, ActivatedRoute, ParamMap } from '@angular/router';
  import {MatSnackBar} from '@angular/material';
  import { SelectItem } from 'primeng/api';
  import { ColumnDefinition } from '../common/column-definition.interface';
  
  @Component({
    selector: 'app-access-right-edit',
    templateUrl: './access-right-edit.component.html',
    // styleUrls: ['./players.component.css']
  })
export class AccessRightEditComponent implements OnInit {
  private accessRight: AccessRight = new AccessRight();
  private columnsToSelect : SelectItem[];
  private columnsDefinitions: ColumnDefinition[] = [];
  private defaultColumnsDefinitions: ColumnDefinition[] = [];
  private columnsDefinitionsToDisplay :ColumnDefinition[] = [];
 
   private  selectedColumnsDefinitions: string[];
 
  private selectedColumnsNames: string[];
  private columnsTitles: string[] = [];
  private columnsTitlesAr: string[] = [];
  private columnsNames: string[] = [];
 
 private entityNames: any[] = [
   {value: 'Articles', viewValue: 'Articles'},
   {value: 'Magasins', viewValue: 'Magasins'},
   {value: 'UnMesures', viewValue: 'Unités Mesures'},
   {value: 'SchemasConfigurations', viewValue: 'Schemas Configurations'},
   {value: 'Lots', viewValue: 'Lots'},
   {value: 'Classes', viewValue: 'Classes'},
   {value: 'Entrees', viewValue: 'Entrees'},
   {value: 'Sorties', viewValue: 'Sorties'},
   {value: 'Utilisateurs', viewValue: 'Utilisateurs'},
   {value: 'DroitsAcces', viewValue: 'Droits Accés'},
   {value: 'Groupes', viewValue: 'Groupes'},
   {value: 'FacturesAchats', viewValue: 'Factures Achats'},
   {value: 'FacturesVentes', viewValue: 'Factures Ventes'},
   {value: 'CreditsAchats', viewValue: 'Credits Achats'},
   {value: 'CreditsVentes', viewValue: 'Credits Ventes'},
   {value: 'Fournisseurs', viewValue: 'Fournisseurs'},
   {value: 'Clients', viewValue: 'Clients'},
   {value: 'Journaux', viewValue: 'Journaux'},
   {value: 'OperationsCourantes', viewValue: 'Opérations Courantes'},
   {value: 'ComptesFournisseurs', viewValue: 'Comptes Fournisseurs'},
   {value: 'ComptesClients', viewValue: 'Comptes Clients'},
   {value: 'RelevesAchats', viewValue: 'Releves Achats'},
   {value: 'RelevesVentes', viewValue: 'Releves Ventes'},
   {value: 'DemandesVentes', viewValue: 'Demandes Ventes'},
   {value: 'DemandesAchats', viewValue: 'Demandes Achats'},
   {value: 'BonsLivraisons', viewValue: 'Bons Livraisons'},
   {value: 'BonsReceptions', viewValue: 'Bons Réceptions'},
   {value: 'Organisations', viewValue: 'Organisations'}
 ];
 private rightNames: any[] = [
   {value: 'Ajout', viewValue: 'Ajout'},
   {value: 'Modification', viewValue: 'Modification'},
   {value: 'Visualisation', viewValue: 'Visualisation'},
   {value: 'Suppression', viewValue: 'Suppression'},
   {value: 'SuppressionDefenitive', viewValue: 'Suppression Définitive'},
   {value: 'VisualisationFiltrée', viewValue: 'Visualisation Filtrée'},
   {value: 'VisualisationPersonalise', viewValue: 'Visualisation Personalisée'},
   {value: 'Historique', viewValue: 'Historique'}      
 ];
    constructor( private route: ActivatedRoute,
      private router: Router , private utilisateurService: UtilisateurService, private matSnackBar: MatSnackBar,
     ) {
    }
    ngOnInit() {
      this.columnsToSelect = [
        {label: 'Ajout', value: 'Ajout', title: 'Ajout'},
        {label: 'Modification', value: 'Modification', title: 'Modification'},
        {label: 'Visualisation', value: 'Visualisation', title: 'Visualisation'},
        {label: 'Visualisation Filtrée', value: 'VisualisationFiltrée', title: 'Visualisation Filtrée'},
        {label: 'Suppression', value: 'Suppression', title: 'Suppression'},
        {label: 'Suppression Defenitive', value: 'SuppressionDefenitive', title: 'Suppression Defenitive'},
        {label: 'Visualisation Personalise', value: 'Visualisation Personalise', title: 'Visualisation Personalise'},
        {label: 'Historique', value: 'Historique', title: 'Historique'}
        
    ];
    this.selectedColumnsDefinitions = [
      
      ];
        const codeAccessRight = this.route.snapshot.paramMap.get('codeAccessRight');
        this.utilisateurService.getAccessRight(codeAccessRight).subscribe((accessRight) => {
            this.accessRight = accessRight;
            this.accessRight.rights.forEach( (right, index) => {
                  this.selectedColumnsDefinitions[index] = right;

            } , this);
            this.specifyListColumnsToBeDisplayed();
            
          });

          this.defaultColumnsDefinitions = [
            {name: 'Ajout' , displayTitle: 'Ajout'},
          {name: 'Modification' , displayTitle: 'Modification'},
          {name: 'Visualisation' , displayTitle: 'Visualisation'},
          {name: 'VisualisationFiltrée' , displayTitle: 'Visualisation Filtrée'},
          {name: 'VisualisationPersonalise' , displayTitle: 'Visualisation Personalisé'},
          {name: 'SuppressionDefenitive' , displayTitle: 'Suppression Defenitive'},
          {name: 'Suppression' , displayTitle: 'Suppression'},
          {name: 'Historique' , displayTitle: 'Historique'}
        ];
    }
    loadAddAccessRightComponent() {
      this.router.navigate(['/pms/administration/ajouterAccessRight']) ;
    
    }
    loadGridAccessRights() {
      this.router.navigate(['/pms/administration/accessRights']) ;
    }
    supprimerAccessRight() {
       
        this.utilisateurService.deleteAccessRight(this.accessRight.code).subscribe((response) => {
  
          this.openSnackBar( ' Element Supprimé');
          this.loadGridAccessRights() ;
              });
    }
  updateAccessRight() {
  
    console.log(this.accessRight);
    if (this.selectedColumnsDefinitions.length !== 0) {
      this.accessRight.rights = [];
    this.selectedColumnsDefinitions.forEach( (right, index) => {
      this.accessRight.rights[index] = right;
} , this);
    } else {
      this.accessRight.rights = [];
    }
    this.utilisateurService.updateAccessRight(this.accessRight).subscribe((response) => {
      if (response.error_message ===  undefined) {
        console.log('the response of update is ', response );
        this.openSnackBar( ' Droit d\'Accés Mis à Jour ');
        
        //  this.save.emit(response);
        // this.client = new Client();
        // this.modeUpdate = false;
      } else {
        this.openSnackBar( ' Erreurs.. ');
        
       // show error message
      }
  });
  }

   openSnackBar(messageToDisplay) {
    this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
        verticalPosition: 'top'});
   }
  
   specifyListColumnsToBeDisplayed() {
    
    //  console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
     this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined?
      this.defaultColumnsDefinitions : [];
      this.selectedColumnsNames = [];
      
      if (this.selectedColumnsDefinitions !== undefined)
       {
         this.selectedColumnsDefinitions.forEach((colName) => { 
              const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
              const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
              const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
              this.columnsDefinitionsToDisplay.push(colDef);
             });
       }
      this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
     this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
                return colDef.displayTitle;
      });
      console.log('columns to disp:', this.columnsDefinitionsToDisplay);
      this.columnsNames = [];
      this.columnsNames[0] = 'select';
      this.columnsDefinitionsToDisplay .map((colDef) => {
       this.columnsNames.push(colDef.name);
      if (this.selectedColumnsDefinitions !== undefined) {
       this.selectedColumnsNames.push(colDef.displayTitle);
      }
   });
      this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
       return colDef.displayTitleAr;
   });
   }
  
  
  selectedTabChanged($event) {
    if ($event.index === 4) { // Composition tab was selected
     // this.loadListArticles();
     } else if ($event.index === 1) {
      // this.loadListSchemasConfiguration() ;
     }
    }
  
  
  }