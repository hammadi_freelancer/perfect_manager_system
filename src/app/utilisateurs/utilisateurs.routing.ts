import { Injectable } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { UtilisateursGridComponent } from './utilisateurs-grid.component';
import { AccessRightsGridComponent} from './access-rights-grid.component';

import { UtilisateurNewComponent } from './utilisateur-new.component';
import { AccessRightNewComponent} from './access-right-new.component';

import { UtilisateurEditComponent } from './utilisateur-edit.component';
import { AccessRightEditComponent} from './access-right-edit.component';

import { AccessRightsResolver } from './accessRights-resolver';
import { InscriptionEditComponent } from './inscription-edit.component';
import { InscriptionsGridComponent } from './inscriptions-grid.component';
import { SignupComponent } from './signup.component';
import {ModuleWithProviders  } from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';
import { ProfilesVentesResolver } from './profiles-ventes-resolver';
import { ProfilesAchatsResolver } from './profiles-achats-resolver';

 export const utilisateursRoute: Routes = [
    {
        path: 'utilisateurs',
        component: UtilisateursGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterUtilisateur',
        component: UtilisateurNewComponent,
        resolve: { profilesAchats: ProfilesAchatsResolver,
            profilesVentes: ProfilesVentesResolver,
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerUtilisateur/:codeUtilisateur',
        component: UtilisateurEditComponent,
        resolve: { accessRights: AccessRightsResolver,
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'accessRights',
        component: AccessRightsGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterAccessRight',
        component: AccessRightNewComponent,
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerAccessRight/:codeAccessRight',
        component: AccessRightEditComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerInscription/:codeInscription',
        component: InscriptionEditComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'inscriptions',
        component: InscriptionsGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },

];



