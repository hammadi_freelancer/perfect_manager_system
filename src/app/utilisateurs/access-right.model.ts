
import { BaseEntity } from '../common/base-entity.model' ;


type EntityNames = 'Articles'| 'Magasins' | 'UnMesures' | 'SchemasConfigurations' |
'Entrees' | 'Sorties' | 'Utilisateurs' | 'DroitsAcces' |  'Groupes'  |   'FacturesAchats' |
'FacturesVentes' | 'CreditsAchats' | 'CreditsVentes' | 'Fournisseurs' | 'Clients' |
 'Journaux' | 'OperationsCourantes' | 'ComptesFournisseurs' | 'ComptesClients' | 'RelevesAchats'
 | 'RelevesVentes' | 'DemandesAchats' | 'DemandesVentes' | 'BonsLivraisons' |'BonsReceptions' ;

type RightsNames = 'Ajouter'| 'Editer' | 'Visualiser' | 'Supprimer' ;
export class AccessRight extends BaseEntity {
  constructor(  
    public code?: string,
    public entityName?: EntityNames,
    public rights?: string[] ,
  ) {
    super();
    // this.identite = {adresse: {}, { } }
  }
}
