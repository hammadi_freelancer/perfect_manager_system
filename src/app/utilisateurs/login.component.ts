import { Component, OnInit , Output, EventEmitter } from '@angular/core';
// import { ArticleService } from './article.service';
import {Utilisateur} from './utilisateur.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { UtilisateurService } from './utilisateur.service';
import {MatSnackBar} from '@angular/material';
import { JwtHelperService } from '@auth0/angular-jwt';
import { JwtModule, JWT_OPTIONS  } from '@auth0/angular-jwt';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  // styleUrls: ['./players.component.css']
})
export class LoginComponent implements OnInit {

private user: Utilisateur = new Utilisateur();

@Output()
userAuthenticated: EventEmitter<Utilisateur> = new EventEmitter<Utilisateur>();
  constructor( private route: ActivatedRoute, private matSnackBar: MatSnackBar ,
    private router: Router , private utilisateurService: UtilisateurService,
  ) {
  }
  ngOnInit() {

  }
  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
  }
login() {
    console.log(this.user);
  //  if(this.user.login !== 'admin') {
    this.utilisateurService.authenticate(this.user).subscribe((userData) => {
      console.log('decoded token ');
      
      if (userData && userData.token) {
        const jwtHelperService = new JwtHelperService();
        
        const decodedToken = jwtHelperService.decodeToken(userData.token);
        const expirationDate = jwtHelperService.getTokenExpirationDate(userData.token);
        console.log('decoded token ');
        console.log(decodedToken);
        if(decodedToken.userData.etat !=='Active')
          {
            this.openSnackBar('Le compte est désactivé');
            
          } else
          {    console.log('user data in token:', userData.token);
               localStorage.setItem('currentUser', JSON.stringify(userData));

             this.userAuthenticated.emit(decodedToken.userData);
             this.router.navigate(['/pms/suivie/ajouterJournal']) ;
             

          }
        // const isExpired = helper.isTokenExpired(myRawToken);
        // this.client = new Client();
       //  this.modeUpdate = false;
      } else {
       // show error message
       this.openSnackBar('Login/Mot de passe incorrecte(s)');
      }
    });
 // } 
  //else {
   // this.userAuthenticated.emit(this.user);
 // }
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
}
