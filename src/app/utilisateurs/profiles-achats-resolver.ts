import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { ProfilesService } from '../profiles/profiles.service';
import { ProfileAchats } from '../profiles/achats/profile-achats.model';

@Injectable()
export class ProfilesAchatsResolver implements Resolve<Observable<ProfileAchats[]>> {
  constructor(private profilesAchatsService: ProfilesService) { }

  resolve() {
     return this.profilesAchatsService.getListProfilesAchats(null);
}
}
