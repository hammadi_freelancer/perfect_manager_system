

export interface InscriptionElement  {
     code: string ;
     numeroInscription: string;
     raisonSociale: string ;
     chiffreAffaires: number;
     adresse?: string;
     contact?: string;
      elementsSI?: string;
     budgetPrepare?: number;
      besoinUrgent?: boolean;
    dateInscription: string;
     logo?: any;
    map?: any;
    email: string ;
    etat: string;
}
