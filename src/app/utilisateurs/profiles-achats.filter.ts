
// import { Tranche } from './tranche.model' ;
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
// import { ProfileAchats } from '../profiles/achats/profile-achats.model';
import { ProfileAchats } from '../profiles/achats/profile-achats.model';

export class ProfileAchatsFilter  {
    public selectedCodeProfileAchats: string;
 
    

    private controlCodesProfilesAchats = new FormControl();
    private controlDescriptionsProfilesAchats = new FormControl();
   //  private controlAdresse = new FormControl();
    private listCodesProfilesAchats: string[] = [];
 
    private listDescriptionsProfilesAchats: string[] = [];


    private filteredCodesProfilesAchats: Observable<string[]>;
    private filteredDescriptionsProfilesAchats: Observable<string[]>;

    
   
    
    constructor ( profilesAchats: ProfileAchats[]) {
      if (profilesAchats !== null && profilesAchats !== undefined && profilesAchats.length !== 0) {
      
        this.listCodesProfilesAchats = profilesAchats.map((profilesAchat) => (profilesAchat.code));
        this.listDescriptionsProfilesAchats = profilesAchats.filter(
          (profAchats) =>
        (profAchats.description !== undefined && profAchats.description != null )).map(
          (profAchats) => (profAchats.description ));
      }
      this.filteredCodesProfilesAchats = this.controlCodesProfilesAchats.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterCodesProfilesAchats(value))
        );
        this.filteredDescriptionsProfilesAchats= this.controlDescriptionsProfilesAchats.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterDescriptionsProfilesAchats(value))
        );
    }

  
    private _filterCodesProfilesAchats(value: string): string[] {
      if (value !== undefined) {
        const filterValue = value.toLowerCase();
        return this.listCodesProfilesAchats.filter(codeProfileAchats => codeProfileAchats.toLowerCase().includes(filterValue));
      } else {
        return this.listCodesProfilesAchats;
      }
      }
      private _filterDescriptionsProfilesAchats(value: string): string[] {
        if (value !== undefined) {
          const filterValue = value.toLowerCase();
        return this.listDescriptionsProfilesAchats.filter(description => description.toLowerCase().includes(filterValue));
        }   else {
          return this.listDescriptionsProfilesAchats;
        }
      }


      public getControlCodesProfilesAchats(): FormControl {
        return this.controlCodesProfilesAchats;
      }

      public getControlDescriptionsProfilesAchats(): FormControl {
        return this.controlDescriptionsProfilesAchats;
      }
 
      public getFiltredCodesProfilesAchats(): Observable<string[]> {
        return this.filteredCodesProfilesAchats;
      }
      public getFiltredDescriptionsProfilesAchats(): Observable<string[]> {
        return this.filteredDescriptionsProfilesAchats;
      }
 

}





