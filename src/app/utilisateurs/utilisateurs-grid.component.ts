import { Component, OnInit, Output, ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {AccessRight} from './access-right.model';
import {Utilisateur} from './utilisateur.model';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {UtilisateurService} from './utilisateur.service';
import { AccessRightElement } from './access-right.interface';
import { UtilisateurElement } from './utilisateur.interface';

import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
// import { DialogAddDocumentClientComponent} from './popups';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {UtilisateurDataFilter } from './utilisateur-data.filter';
import { SelectItem } from 'primeng/api';
@Component({
  selector: 'app-utilisateurs-grid',
  templateUrl: './utilisateurs-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class UtilisateursGridComponent implements OnInit {

  private utilisateur: Utilisateur =  new Utilisateur();
  private showFilter = false;
  private showSelectColumnsMultiSelect = false;
  private utilisateurDataFilter = new UtilisateurDataFilter();
  private selection = new SelectionModel<Utilisateur>(true, []);

  @Input()
  private listUtilisateurs: any = [] ;


 selectedUtilisateur: Utilisateur ;

private deleteEnCours = false;



 // listClientsOrgin: any = [];
 private checkedAll: false;
 private showParticulier = false;
 private showEntreprise = false;
 private  dataUtilisateursSource: MatTableDataSource<UtilisateurElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay :ColumnDefinition[] = [];

 private columnsToSelect : SelectItem[];
  private  selectedColumnsDefinitions: string[];

 private selectedColumnsNames: string[];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
 private rolesUtilisateurs: any[] = [
  {value: 'SuperAdmin', viewValue: 'Super Administrateur'},
  
  {value: 'Admin', viewValue: 'Administrateur'},
  {value: 'Uilisateur', viewValue: 'Uilisateur'},
  {value: 'Visiteur', viewValue: 'Visiteur'},

];
private etatsUtilisateurs: any[] = [
  {value: 'Active', viewValue: 'Activé'},
  {value: 'Desactive', viewValue: 'Désactivé'},
  {value: 'EnCours', viewValue: 'En Cours'}

]
  constructor( private route: ActivatedRoute, private utilisateursService: UtilisateurService,
    private router: Router, private matSnackBar: MatSnackBar, private dialog: MatDialog ) {
  }
  ngOnInit() {
    this.defaultColumnsDefinitions = [
      {name: 'code' , displayTitle: 'Code'},
    {name: 'cin' , displayTitle: 'CIN'},
    {name: 'nom' , displayTitle: 'Nom'},
    {name: 'prenom' , displayTitle: 'Prénom'},
    {name: 'mobile' , displayTitle: 'Mobile'},
    {name: 'adresse' , displayTitle: 'Adresse'},
    {name: 'email' , displayTitle: 'Email'}, 
     {name: 'login' , displayTitle: 'Login'},
     {name: 'password' , displayTitle: 'Mot de Passe'},
     {name: 'etat' , displayTitle: 'Etat'},
     {name: 'dateNaissance' , displayTitle: 'Date Naissance'},
     {name: 'dateInscription' , displayTitle: 'Date Inscription'},
     {name: 'role' , displayTitle: 'Rôle'},
     {name: 'description' , displayTitle: 'Déscription'}
  ];
    
    this.columnsToSelect = [
      {label: 'Code', value: 'code', title: 'Code'},
      {label: 'CIN', value: 'cin', title: 'CIN'},
      {label: 'Nom', value: 'nom', title: 'Nom'},
      {label: 'Prénom', value: 'prenom', title: 'Prénom'},
      {label: 'Date Naissance', value: 'dateNaissance', title: 'Date Naissance'},
      {label: 'Date Inscription', value: 'dateInscription', title: 'Date Inscription'},
      {label: 'Rôle', value: 'role', title: 'Rôle'},
      {label: 'Déscription', value: 'description', title: 'Déscription'},
      
      {label: 'Adresse', value: 'adresse', title: 'Adresse'},
      {label: 'Email', value: 'email', title: 'Email'},
      {label: 'Mobile', value: 'mobile', title: 'Mobile'},
      {label: 'Login', value: 'login', title: 'Login'},
      {label: 'Mot de Passe', value: 'password', title: 'Mot de Passe'},
      {label: 'Etat', value: 'etat', title: 'Etat'},
      
      
  ];
  this.selectedColumnsDefinitions = [
    'code',
   'cin',
   'nom',
   'prenom',
   'login'
    ];
    this.utilisateursService.getPageUtilisateurs(0, 5, null).subscribe((utilisateurs) => {
      this.listUtilisateurs= utilisateurs;
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });
  

  }
  deleteAccessRight(accessRight) {
     // console.log('call delete !', client );
    this.utilisateursService.deleteAccessRight(accessRight.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData(null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.utilisateursService.getPageAccessRights(0, 5, filter).subscribe((utilisateurs) => {
    this.listUtilisateurs = utilisateurs;
    // this.listClients[this.listClients.length] = this.lastClientAdded;
    this.transformDataToByConsumedByMatTable();
   });
  // this.listClientsOrgin = this.listClients ;
}


checkOneActionInvoked() {
  const listSelectedUtilsiateurs: Utilisateur[] = [];

if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((uElement) => {
  return uElement.code;
});
this.listUtilisateurs.forEach(r => {
  if (codes.findIndex(code => code === r.code) > -1) {
    listSelectedUtilsiateurs.push(r);
  }
  });
}
// this.select.emit(listSelectedClients);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listUtilisateurs !== undefined) {
    this.listUtilisateurs.forEach(utilisateur => {
      listElements.push(
        {
          code: utilisateur.code ,
          cin: utilisateur.cin ,
          nom: utilisateur.nom ,
          prenom: utilisateur.prenom ,
          adresse: utilisateur.adresse ,
          mobile: utilisateur.mobile ,
          email: utilisateur.email ,
          login: utilisateur.login ,
          password: utilisateur.password ,
          dateNaissance: utilisateur.dateNaissance ,
          dateInscription: utilisateur.dateInscription ,
          etat: utilisateur.etat,
          role: utilisateur.role
      } );
    });
  }
    this.dataUtilisateursSource = new MatTableDataSource<UtilisateurElement>(listElements);
    this.dataUtilisateursSource.sort = this.sort;
    this.dataUtilisateursSource.paginator = this.paginator;
    this.utilisateursService.getTotalUtilisateurs(this.utilisateurDataFilter).subscribe((response) => {
     //  console.log('Total Clients   is ', response);
     if (this.dataUtilisateursSource.paginator !== undefined) {
       this.dataUtilisateursSource.paginator.length = response['totalUtilsateurs'];
     }
     });
}

masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
    //   this.dataAccessRightsSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataUtilisateursSource.data.length;
  return numSelected === numRows;
}
/*applyFilter(filterValue: string) {
  this.dataAccessRightsSource.filter = filterValue.trim().toLowerCase();
  if (this.dataAccessRightsSource.paginator) {
    this.dataAccessRightsSource.paginator.firstPage();
  }
}*/
loadAddUtilisateurComponent() {
  this.router.navigate(['/pms/administration/ajouterUtilisateur']) ;

}
loadEditUtilisateurComponent() {
  if (this.selection.selected.length === 0) {
    this.openSnackBar(' Aucun élément sélectionné' );
    // this.loadData();
    } else
   {
  this.router.navigate(['/pms/administration/editerUtilisateur', this.selection.selected[ this.selection.selected.length - 1].code]) ;
    }
}
supprimerUtilisateurs()
{
  if ( this.selection.selected.length !== 0) {
    this.deleteEnCours = true;
      this.selection.selected.forEach(function(utilisateur, index) {
          this.utilisateursService.deleteUtilisateur(utilisateur.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.deleteEnCours = false;
            this.openSnackBar( ' Utilisateur(s) Supprimé(s)');
             this.loadData(null);
            }
      }, this);
    } else {
      this.openSnackBar('Aucun Utilisateur Séléctionné!');
    }
}
refresh()
{
  this.loadData(null);
  
}
  openSnackBar(messageToDisplay) {
 this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
     verticalPosition: 'top'});
}

changePage($event) {
  console.log('page event is ', $event);
 this.utilisateursService.getPageUtilisateurs($event.pageIndex, $event.pageSize,
  this.utilisateurDataFilter).subscribe((utilisateurs) => {
    this.listUtilisateurs = utilisateurs;
    const listElements = [];
 if (this.listUtilisateurs !== undefined) {
    this.listUtilisateurs.forEach(utilisateur => {
      listElements.push( {
        code: utilisateur.code ,
        cin: utilisateur.cin ,
        nom: utilisateur.nom ,
        prenom: utilisateur.prenom ,
        adresse: utilisateur.adresse ,
        mobile: utilisateur.mobile ,
        email: utilisateur.email ,
        login: utilisateur.login ,
        password: utilisateur.password ,
        dateNaissance: utilisateur.dateNaissance ,
        dateInscription: utilisateur.dateInscription ,
        etat: utilisateur.etat,
        role: utilisateur.role
      }
      );
    });
  }
     this.dataUtilisateursSource= new MatTableDataSource<UtilisateurElement>(listElements);
     this.utilisateursService.getTotalUtilisateurs(this.utilisateurDataFilter).
     subscribe((response) => {
      console.log('Total clients is ', response);
      // this.dataClientsSource.paginator = this.paginator;
      if (this.dataUtilisateursSource.paginator) {
        
       this.dataUtilisateursSource.paginator.length = response.totalUtilsateurs;
      }
     });

    });
}
toggleFilterPanel() {
  this.showFilter = !this.showFilter;
}
toggleShowColumnsMultiSelect() {
 this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
}
loadFiltredData() {
 console.log('the filter is ', this.utilisateurDataFilter);
 this.loadData(this.utilisateurDataFilter);
}

specifyListColumnsToBeDisplayed() {
  
   console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
   this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
 ?
    this.defaultColumnsDefinitions : [];
    this.selectedColumnsNames = [];
    
    if (this.selectedColumnsDefinitions !== undefined)
     {
       this.selectedColumnsDefinitions.forEach((colName) => { 
            const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
            const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
            const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
            this.columnsDefinitionsToDisplay.push(colDef);
           });
     }
    this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
   this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
              return colDef.displayTitle;
    });
    console.log('columns to disp:', this.columnsDefinitionsToDisplay);
    this.columnsNames = [];
    this.columnsNames[0] = 'select';
    this.columnsDefinitionsToDisplay .map((colDef) => {
     this.columnsNames.push(colDef.name);
    if (this.selectedColumnsDefinitions !== undefined) {
     this.selectedColumnsNames.push(colDef.displayTitle);
    }
 });
    this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
     return colDef.displayTitleAr;
 });
 }






}
