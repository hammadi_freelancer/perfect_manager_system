import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { ProfilesService } from '../profiles/profiles.service';
import { ProfileVentes } from '../profiles/ventes/profile-ventes.model';

@Injectable()
export class ProfilesVentesResolver implements Resolve<Observable<ProfileVentes[]>> {
  constructor(private profilesVentesService: ProfilesService) { }

  resolve() {
     return this.profilesVentesService.getListProfilesVentes(null);
}
}
