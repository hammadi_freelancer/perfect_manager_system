
export class UtilisateurDataFilter {
    code: string ;
    description: string;
    nom: string;
    prenom: string;
    cin : string;
    dateNaissanceFromObject : Date;
    dateNaissanceTobject : Date;
    dateInscriptionFromObject : Date;
    dateInscriptionTobject : Date;
     email : string;
     mobile : string;
     adresse : string;
     
    role: string;
    login: string;
    etat: string;
   }
