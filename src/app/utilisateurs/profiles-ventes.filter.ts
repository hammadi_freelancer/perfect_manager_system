
// import { Tranche } from './tranche.model' ;
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
// import { ProfileAchats } from '../profiles/achats/profile-achats.model';
import { ProfileVentes } from '../profiles/ventes/profile-ventes.model';

export class ProfileVentesFilter  {
    public selectedCodeProfileVentes: string;
 
    

    private controlCodesProfilesVentes = new FormControl();
    private controlDescriptionsProfilesVentes = new FormControl();
   //  private controlAdresse = new FormControl();
    private listCodesProfilesVentes: string[] = [];
 
    private listDescriptionsProfilesVentes: string[] = [];


    private filteredCodesProfilesVentes: Observable<string[]>;
    private filteredDescriptionsProfilesVentes: Observable<string[]>;

    
   
    
    constructor ( profilesVentes: ProfileVentes[]) {
      if (profilesVentes !== null && profilesVentes !== undefined && profilesVentes.length !== 0) {
      
        this.listCodesProfilesVentes = profilesVentes.map((profileVentes) => (profileVentes.code));
        this.listDescriptionsProfilesVentes = profilesVentes.filter(
          (profileVentes) =>
        (profileVentes.description !== undefined && profileVentes.description != null )).map(
          (profileVentes) => (profileVentes.description ));
      }
      this.filteredCodesProfilesVentes = this.controlCodesProfilesVentes.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterCodesProfilesVentes(value))
        );
        this.filteredDescriptionsProfilesVentes = this.controlDescriptionsProfilesVentes.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterDescriptionsProfilesVentes(value))
        );
    }

  
    private _filterCodesProfilesVentes(value: string): string[] {
      if (value !== undefined) {
        const filterValue = value.toLowerCase();
        return this.listCodesProfilesVentes.filter(codeProfileVentes => codeProfileVentes.toLowerCase().includes(filterValue));
      } else {
        return this.listCodesProfilesVentes;
      }
      }
      private _filterDescriptionsProfilesVentes(value: string): string[] {
        if (value !== undefined) {
          const filterValue = value.toLowerCase();
        return this.listDescriptionsProfilesVentes.filter(description => description.toLowerCase().includes(filterValue));
        }   else {
          return this.listDescriptionsProfilesVentes;
        }
      }


      public getControlCodesProfilesVentes(): FormControl {
        return this.controlCodesProfilesVentes;
      }

      public getControlDescriptionsProfilesVentes(): FormControl {
        return this.controlDescriptionsProfilesVentes;
      }
 
      public getFiltredCodesProfilesVentes(): Observable<string[]> {
        return this.filteredCodesProfilesVentes;
      }
      public getFiltredDescriptionsProfilesVentes(): Observable<string[]> {
        return this.filteredDescriptionsProfilesVentes;
      }
 

}





