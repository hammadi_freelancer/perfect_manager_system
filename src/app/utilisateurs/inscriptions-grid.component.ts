import { Component, OnInit, Output,ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {Inscription} from './inscription.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {UtilisateurService} from './utilisateur.service';
import { InscriptionElement } from './inscription.interface';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
// import { DialogAddDocumentCreditComponent } from './dialog-add-document-credit.component';
@Component({
  selector: 'app-inscriptions-grid',
  templateUrl: './inscriptions-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class InscriptionsGridComponent implements OnInit {

  private inscription: Inscription =  new Inscription();
  private selection = new SelectionModel<InscriptionElement>(true, []);
  
@Output()
select: EventEmitter<InscriptionElement[]> = new EventEmitter<InscriptionElement[]>();

//private lastClientAdded: Client;

 selectedInscription: Inscription;


 @Input()
 private listInscriptions: Inscription[] = [] ;
 private checkedAll: false;
 private  dataIncriptionsSource: MatTableDataSource<InscriptionElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
  constructor( private route: ActivatedRoute, private utilisateursService: UtilisateurService,
    private router: Router, private matSnackBar: MatSnackBar ) {
  }
  ngOnInit() {
    this.utilisateursService.getListInscriptions(0, 5).subscribe((inscriptions) => {
      this.listInscriptions = inscriptions;
      // this.listClients[this.listClients.length] = this.lastClientAdded;
      // this.listClientsOrgin = this.listClients ;
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });
   /* this.creditsService.getCredits().subscribe((credits) => {
      console.log('list credits ', credits);
      // this.listCredits = credits;
      this.listCredits =  this.listCredits.map((credit) => {
        if (credit.client === 'undefined' || credit.client === null ) {
           credit.client = new Client();
         }
         return credit;
   });      this.listCreditsOrgin = this.listCredits ;
     });*/
    // this.listClientsOrgin = this.listClients ;

  }
  deleteInscription(inscription) {
     //  console.log('call delete !', credit );
    this.utilisateursService.deleteInscription(inscription.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData();

      } else {
       // show error message
      }
     });

  }
loadData() {
  this.utilisateursService.getListInscriptions(0, 5 ).subscribe((inscriptions) => {
    this.listInscriptions = inscriptions;
    this.transformDataToByConsumedByMatTable();
   });
  // this.listClientsOrgin = this.listClients ;
}


checkOneActionInvoked() {
  const listSelectedInscriptions: Inscription[] = [];
if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((InscriElement) => {
  return InscriElement.code;
});
this.listInscriptions.forEach(ins => {
  if (codes.findIndex(code => code === ins.code) > -1) {
    listSelectedInscriptions.push(ins);
  }
  });
}
// this.select.emit(listSelectedInscriptions);

}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  this.dataIncriptionsSource = null;
   this.listInscriptions.forEach (inscri => {
      listElements.push( {code: inscri.code ,
        numeroInscription: inscri.numeroInscription,
        dateInscription: inscri.dateInscription,
        raisonSociale: inscri.raisonSociale,
        chiffreAffaires : inscri.chiffreAffaires,
        adresse : inscri.adresse,
        contact: inscri.contact,
        email: inscri.email,
        elementsSI: inscri.elementsSI,
        budgetPrepare: inscri.budgetPrepare,
         besoinUrgent: inscri.besoinUrgent,
        logo: inscri.logo,
       map: inscri.map,
      etat: inscri.etat === 'VALIDE' ?  'Validé' : inscri.etat === 'ANNULE' ? 'Annulé' : 'Confirmé'

      });
    });
    this.dataIncriptionsSource = new MatTableDataSource<InscriptionElement>(listElements);
    this.dataIncriptionsSource.sort = this.sort;
    this.dataIncriptionsSource.paginator = this.paginator;
    this.utilisateursService.getTotalIncriptions().subscribe((response) => {
    //  console.log('Total credits is ', response);
      this.dataIncriptionsSource.paginator.length = response.totalInscriptions;
    });
}
 specifyListColumnsToBeDisplayed() {
   this.columnsDefinitions = [{name: 'numeroInscription' , displayTitle: 'N°Inscription'},
   {name: 'dateInscription' , displayTitle: 'Date Inscription'},
   {name: 'raisonSociale' , displayTitle: 'R.Sociale'},
   {name: 'chiffreAffaires' , displayTitle: 'Ch.Affaires'},
   {name: 'budgetPrepare' , displayTitle: 'Budget'},
   {name: 'adresse' , displayTitle: 'Adresse'},
   {name: 'contact' , displayTitle: 'Contact'},
   {name: 'email' , displayTitle: 'Email'},
   {name: 'elementsSI' , displayTitle: 'Syst.Info'},
     ];
   this.columnsTitles = this.columnsDefinitions.map((colDef) => {
             return colDef.displayTitle;
   });
   this.columnsNames[0] = 'select';
   this.columnsDefinitions.map((colDef) => {
    this.columnsNames.push(colDef.name);
});
   this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
    return colDef.displayTitleAr;
});
}
masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataIncriptionsSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataIncriptionsSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataIncriptionsSource.filter = filterValue.trim().toLowerCase();
  if (this.dataIncriptionsSource.paginator) {
    this.dataIncriptionsSource.paginator.firstPage();
  }
}


loadViewInscriptionComponent() {
  this.router.navigate(['/pms/utilisateurs/editerInscription', this.selection.selected[ this.selection.selected.length - 1].code]) ;

}
supprimerInscriptions()
{
 
  if ( this.selection.selected.length !== 0) {
      this.selection.selected.forEach(function(inscri, index) {
          this.utilisateursService.deleteInscription(inscri.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.openSnackBar( this.selection.selected.length + ' Element(s) Supprimé(s)');
             this.loadData();
            }
      }, this);
    } else {
      this.openSnackBar('Aucune Inscription Séléctionnée !');
    }
}
refresh() {
  this.loadData();
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 
 /*attacherDocumentCredit()
 {
  if ( this.selection.selected.length !== 0) {
    
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
    top : '0'
  };
  dialogConfig.data = {
    codeCredit : this.selection.selected[0].code
  };

  const dialogRef = this.dialog.open(DialogAddDocumentCreditComponent,
    dialogConfig
  );
} else {
  this.openSnackBar('Aucun Crédit Séléctionné!');
}
}*/
 
 

changePage($event) {
  console.log('page event is ', $event);
 this.utilisateursService.getListInscriptions($event.pageIndex, $event.pageSize ).subscribe((inscriptions) => {
    this.listInscriptions = inscriptions;
   // this.transformDataToByConsumedByMatTable();
   const listElements = [];
     console.log('diffe of undefined');
     this.listInscriptions.forEach(inscri => {
      listElements.push( {code: inscri.code ,
        numeroInscription: inscri.numeroInscription,
        dateInscription: inscri.dateInscription,
        raisonSociale: inscri.raisonSociale,
        chiffreAffaires: inscri.chiffreAffaires,
        adresse : inscri.adresse,
        contact: inscri.contact,
        email: inscri.email,
        elementsSI: inscri.elementsSI,
        budgetPrepare: inscri.budgetPrepare,
         besoinUrgent: inscri.besoinUrgent,
        logo: inscri.logo,
       map: inscri.map,
      etat: inscri.etat === 'VALIDE' ?  'Validé' : inscri.etat === 'ANNULE' ? 'Annulé' : 'Confirmé'

 } );
 this.dataIncriptionsSource = new MatTableDataSource<InscriptionElement>(listElements);
 this.utilisateursService.getTotalIncriptions().subscribe((response) => {
   this.dataIncriptionsSource.paginator.length = response.totalInscriptions;
 });
});

});
}
}
