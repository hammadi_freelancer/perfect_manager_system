import { Component, OnInit , Output, EventEmitter } from '@angular/core';
// import { ArticleService } from './article.service';
import {Inscription} from './inscription.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { UtilisateurService } from './utilisateur.service';
import {MatSnackBar} from '@angular/material';
import { JwtHelperService } from '@auth0/angular-jwt';
import { JwtModule, JWT_OPTIONS  } from '@auth0/angular-jwt';



@Component({
  selector: 'app-inscription-edit',
  templateUrl: './inscription-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class InscriptionEditComponent implements OnInit {

private inscription: Inscription = new Inscription();



  constructor( private route: ActivatedRoute, private matSnackBar: MatSnackBar ,
    private router: Router , private utilisateurService: UtilisateurService,
  ) {
  }
  ngOnInit() {

  }

  confirmerInscription() {
        this.utilisateurService.confirmerInscription(this.inscription).subscribe((response) => {
             if (response.error_message ===  undefined) {
               // this.save.emit(response);
               this.inscription = new Inscription();
               this.openSnackBar(  'Inscription Confirmée');
             } else {
              // show error message
              this.openSnackBar(  'Echéc de Confirmation');
              
             }
         });
     }
     logoChanged($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.inscription.logo = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     mapChanged($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.inscription.map = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

}
