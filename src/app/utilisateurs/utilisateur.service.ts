import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';

import {Utilisateur} from './utilisateur.model' ;
import {AccessRight} from './access-right.model' ;

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';
import {Inscription} from './inscription.model' ;

// const  resourceUrlUtilisateurs = 'http://localhost:8082/listUtilisateurs';
// const resourceUrlAddUtilisateur = 'http://localhost:8082/addUtilisateur';

const  URL_GET_LIST_UTILISATEURS = 'http://localhost:8082/utilisateurs/getUtilisateurs';
const  URL_GET_PAGE_UTILISATEURS = 'http://localhost:8082/utilisateurs/getPageUtilisateurs';

const URL_ADD_UTILISATEUR = 'http://localhost:8082/utilisateurs/addUtilisateur';
const URL_GET_UTILISATEUR = 'http://localhost:8082/utilisateurs/getUtilisateur';

const URL_UPDATE_UTILISATEUR = 'http://localhost:8082/utilisateurs/updateUtilisateur';
const URL_DELETE_UTILISATEUR = 'http://localhost:8082/utilisateurs/deleteUtilisateur';
const URL_DELETE_INSCRITPION= 'http://localhost:8082/utilisateurs/deleteInscription';
const URL_GET_TOTAL_UTILISATEURS = 'http://localhost:8082/utilisateurs/getTotalUtilisateurs';

 const  URL_GET_LIST_ACCESS_RIGHTS = 'http://localhost:8082/accessRights/getAccessRights';
 const  URL_GET_PAGE_ACCESS_RIGHTS = 'http://localhost:8082/accessRights/getPageAccessRights';
 const URL_GET_TOTAL_ACCESS_RIGHTS = 'http://localhost:8082/accessRights/getTotalAccessRights';
 
const URL_ADD_ACCESS_RIGHT = 'http://localhost:8082/accessRights/addAccessRight';
const URL_GET_ACCESS_RIGHT = 'http://localhost:8082/accessRights/getAccessRight';

const URL_UPDATE_ACCESS_RIGHT = 'http://localhost:8082/accessRights/updateAccessRight';
const URL_DELETE_ACCESS_RIGHT = 'http://localhost:8082/accessRights/deleteAccessRight';
const URL_GET_TOTAL_GROUPES = 'http://localhost:8082/accessRights/getTotalGroupes';

const  URL_GET_LIST_GROUPES = 'http://localhost:8082/groupes/getGroupes';
const  URL_GET_PAGE_GROUPES = 'http://localhost:8082/groupes/getPageGroupes';

const URL_ADD_GROUPE = 'http://localhost:8082/groupes/addGroupe';
const URL_GET_GROUPE = 'http://localhost:8082/groupes/getGroupe';

const URL_UPDATE_GROUPE = 'http://localhost:8082/groupes/updateGroupe';
const URL_DELETE_GROUPE = 'http://localhost:8082/groupes/deleteGroupe';

const resourceUrlAuthenticate = 'http://localhost:8082/login';

const URL_ADD_INSCRIPTION = 'http://localhost:8082/utilisateurs/addInscription';
const URL_GET_LIST_INSCRIPTIONS = 'http://localhost:8082/utilisateurs/getInscriptions';
const URL_GET_TOTAL_INSCRIPTIONS = 'http://localhost:8082/utilisateurs/getTotalInscriptions';
const URL_GET_INSCRIPTION = 'http://localhost:8082/utilisateurs/getInscription';

const URL_CONFIRM_INSCRIPTION = 'http://localhost:8082/utilisateurs/confirmInscription';

@Injectable({
  providedIn: 'root'
})
export class UtilisateurService {
 
  constructor(private httpClient: HttpClient) {

   }

   authenticate(utilisateur: Utilisateur): Observable<any> {
    // return this.httpClient.post<Utilisateur>(resourceUrlAuthenticate, utilisateur);
    return this.httpClient.post<any>(resourceUrlAuthenticate, utilisateur);
  }
   getListUtilisateurs(): Observable<Utilisateur[]> {
    return this.httpClient
    .get<Utilisateur[]>(URL_GET_LIST_UTILISATEURS);
   }

   getPageUtilisateurs(pageNumber, nbElementsPerPage, filter): Observable<Utilisateur[]> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('pageNumber', pageNumber)
    .set('nbElementsPerPage', nbElementsPerPage)
    .set('sort', 'name')
    .set('filter', filterStr);
    return this.httpClient
    .get<Utilisateur[]>(URL_GET_PAGE_UTILISATEURS, {params});
   }
   getTotalUtilisateurs(filter): Observable<any> {
    
        const filterStr = JSON.stringify(filter);
        const params = new HttpParams()
        .set('filter', filterStr);
        return this.httpClient
        .get<any>(URL_GET_TOTAL_UTILISATEURS, {params});
    }

   addUtilisateur(utilisateur: Utilisateur): Observable<any> {
    return this.httpClient.post<Utilisateur>(URL_ADD_UTILISATEUR, utilisateur);
  }
  getUtilisateur(codeUtilisateur): Observable<Utilisateur> {
    return this.httpClient.get<Utilisateur>(URL_GET_UTILISATEUR + '/' + codeUtilisateur);
  }
   updateUtilisateur(utilisateur: Utilisateur): Observable<any> {
    return this.httpClient.put<Utilisateur>(URL_UPDATE_UTILISATEUR, utilisateur);
  }
  deleteUtilisateur(codeUtilisateur): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_UTILISATEUR + '/' + codeUtilisateur);
  }
  getListAccessRights(): Observable<AccessRight[]> {
    return this.httpClient
    .get<AccessRight[]>(URL_GET_LIST_ACCESS_RIGHTS);
   }
   addAccessRight(accessRight: AccessRight): Observable<any> {
    return this.httpClient.post<AccessRight>(URL_ADD_ACCESS_RIGHT, accessRight);
  }
  
  getAccessRight(codeAccessRight): Observable<AccessRight> {
    return this.httpClient.get<AccessRight>(URL_GET_ACCESS_RIGHT + '/' + codeAccessRight);
  }
 


   updateAccessRight(accessRight: AccessRight): Observable<any> {
    return this.httpClient.put<AccessRight>(URL_UPDATE_ACCESS_RIGHT, accessRight);
  }
  deleteAccessRight(codeAccessRight): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_ACCESS_RIGHT + '/' + codeAccessRight);
  }

  getPageAccessRights(pageNumber, nbElementsPerPage, filter): Observable<AccessRight[]> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('pageNumber', pageNumber)
    .set('nbElementsPerPage', nbElementsPerPage)
    .set('sort', 'name')
    .set('filter', filterStr);
    return this.httpClient
    .get<AccessRight[]>(URL_GET_PAGE_ACCESS_RIGHTS, {params});
   }
   getTotalAccessRights(filter): Observable<any> {
    
        const filterStr = JSON.stringify(filter);
        const params = new HttpParams()
        .set('filter', filterStr);
        return this.httpClient
        .get<any>(URL_GET_TOTAL_ACCESS_RIGHTS, {params});
    }

    /*getPageGroupes(pageNumber, nbElementsPerPage, filter): Observable<Groupe[]> {
      const filterStr = JSON.stringify(filter);
      const params = new HttpParams()
      .set('pageNumber', pageNumber)
      .set('nbElementsPerPage', nbElementsPerPage)
      .set('sort', 'name')
      .set('filter', filterStr);
      return this.httpClient
      .get<Groupe[]>(URL_GET_PAGE_GROUPES, {params});
     }*/
     getTotalGroupes(filter): Observable<any> {
      
          const filterStr = JSON.stringify(filter);
          const params = new HttpParams()
          .set('filter', filterStr);
          return this.httpClient
          .get<any>(URL_GET_TOTAL_GROUPES, {params});
      }







  addInscription(inscription: Inscription): Observable<any> {
    return this.httpClient.post<Inscription>(URL_ADD_INSCRIPTION, inscription);
  }
  getInscription(codeInscription): Observable<Inscription> {
    return this.httpClient.get<Inscription>(URL_GET_INSCRIPTION + '/' + codeInscription);
  }
  getListInscriptions(pageNumber, nbElementsPerPage): Observable<Inscription[]> {
    const params = new HttpParams()
    .set('pageNumber', pageNumber)
    .set('nbElementsPerPage', nbElementsPerPage);
    return this.httpClient
    .get<Inscription[]>(URL_GET_LIST_INSCRIPTIONS, { params} );
   }

   getTotalIncriptions(): Observable<any> {
    return this.httpClient
    .get<any>(URL_GET_TOTAL_INSCRIPTIONS);
   }

   confirmerInscription(inscription)
   {
    return this.httpClient.post<any>(URL_CONFIRM_INSCRIPTION, inscription);
    
   }

   deleteInscription(codeInscription): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_INSCRITPION + '/' + codeInscription);
  }
}
