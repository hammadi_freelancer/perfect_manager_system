
export class AccessRightDataFilter  {
    public code: string;
    public entityName: string;
    public rights?: string[] ;
}
