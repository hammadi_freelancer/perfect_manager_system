import { Component, OnInit, Input, EventEmitter, Output, AfterContentChecked,
AfterViewChecked
} from '@angular/core';
// import { ArticleService } from './article.service';
import {Utilisateur } from './utilisateur.model';
import {UtilisateurService} from './utilisateur.service';
import {AccessRight } from './access-right.model';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {MatSnackBar} from '@angular/material';
import { SelectValues } from './select-values.interface';
import {ProfileAchatsFilter} from './profiles-achats.filter';
import {ProfileVentesFilter} from './profiles-ventes.filter';

@Component({
  selector: 'app-utilisateur-edit',
  templateUrl: './utilisateur-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class UtilisateurEditComponent implements OnInit, AfterContentChecked {

// private client: Client = new Client();


private utilisateur:  Utilisateur = new  Utilisateur();
private sourceListAccessRights: AccessRight[];
private targetListAccessRights: AccessRight[] = [];

private file: any;
private profileAchatsFilter : ProfileAchatsFilter;
private profileVentesFilter : ProfileVentesFilter;
private listProfilesAchats: any;
private listProfilesVentes: any;

private rolesUtilisateurs: SelectValues[] = [
  { value: 'Super Administrateur', viewValue: 'Super Administrateur' },
  {value: 'Admin', viewValue: 'Administrateur'},
  {value: 'Uilisateur', viewValue: 'Uilisateur'},
  {value: 'Visiteur', viewValue: 'Visiteur'},

];
private etatsUtilisateurs: SelectValues[] = [
  {value: 'Active', viewValue: 'Activé'},
  {value: 'Desactive', viewValue: 'Désactivé'},
  {value: 'EnCours', viewValue: 'En Cours'}

];
  constructor( private route: ActivatedRoute,
    private router: Router , private utilisateurService: UtilisateurService, private matSnackBar: MatSnackBar,
   ) {
  }
  ngOnInit() {
    this.sourceListAccessRights = this.route.snapshot.data.accessRights;
    this.listProfilesAchats= this.route.snapshot.data.profilesAchats;
    this.listProfilesVentes= this.route.snapshot.data.profilesVentes;
    this.profileAchatsFilter = new ProfileAchatsFilter( this.listProfilesAchats);
    this.profileVentesFilter = new ProfileVentesFilter(this.listProfilesVentes);
    
    console.log('access rights recieved ...');
    console.log(this.sourceListAccessRights);
    console.log(this.utilisateur);
      const codeUtilisateur = this.route.snapshot.paramMap.get('codeUtilisateur');
      this.utilisateurService.getUtilisateur(codeUtilisateur).subscribe((utilisateur) => {
          this.utilisateur = utilisateur;
           const selectedAccessRights = [];
    if (this.utilisateur.accessRights !== undefined && this.utilisateur.accessRights.length !== 0) {
           this.utilisateur.accessRights.forEach(function(accessRight, index) {
                     const acR = this.sourceListAccessRights.filter((ac) => ac.code === accessRight.code)[0];
                     if ( acR !== undefined ) {
                     this.targetListAccessRights.push(acR);
                     this.sourceListAccessRights =  this.sourceListAccessRights.filter( (accessR) => accessR.code !== acR.code);
                     }
                 }, this);
                 }

      });
  }
  /*loadAddUtilisateurComponent() {
    this.router.navigate(['/pms/administration/ajouterUtilisateur']) ;
  
  }*/
  loadGridUtilisateurs() {
    this.router.navigate(['/pms/administration/utilisateurs']) ;
  }
 /* supprimerUtilisateur() {
     
      this.utilisateurService.deleteUtilisateur(this.utilisateur.code).subscribe((response) => {

        this.openSnackBar( ' Element Supprimé');
        this.loadGridUtilisateurs() ;
            });
  }*/
updateUtilisateur() {
  this.utilisateur.accessRights = [];
if (this.targetListAccessRights.length !== 0) {
     this.targetListAccessRights.forEach(function(accessRight, index) {
      this.utilisateur.accessRights.push(accessRight) ;
     }, this);
  }
  console.log(this.utilisateur);
  if (this.utilisateur.confirmPassword !== this.utilisateur.password) {
      this.openSnackBar(  'Le mot de passe ne correspond pas à la confirmation');
    } else {
  this.utilisateurService.updateUtilisateur(this.utilisateur).subscribe((response) => {
    if (response.error_message ===  undefined) {
      this.openSnackBar( ' Utilisateur mis à jour ');
    } else {
      this.openSnackBar( ' Erreurs.. ');
      
     // show error message
    }
});
    }
}
fileChanged($event) {
  this.file = $event.target.files[0];
  const fileReader = new FileReader();
  fileReader.onload = (e) => {
    console.log(fileReader.result);
     this.utilisateur.image = fileReader.result;
    // this.filesData[indexFile] = fileData;
  };
  fileReader.readAsDataURL(this.file);
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

ngAfterContentChecked() {

}


selectedTabChanged($event) {
  if ($event.index === 4) { // Composition tab was selected
   // this.loadListArticles();
   } else if ($event.index === 1) {
    // this.loadListSchemasConfiguration() ;
   }
  }


}


