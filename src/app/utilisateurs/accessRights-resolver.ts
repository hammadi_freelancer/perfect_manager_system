import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { UtilisateurService } from './utilisateur.service';
import { Utilisateur } from './utilisateur.model';

@Injectable()
export class AccessRightsResolver implements Resolve<Observable<Utilisateur[]>> {
  constructor(private utilisateursService: UtilisateurService) { }

  resolve() {
     return this.utilisateursService.getListAccessRights();
}
}
