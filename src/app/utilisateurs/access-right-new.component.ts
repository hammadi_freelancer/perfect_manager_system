import { Component, OnInit, Input, EventEmitter, Output,
    AfterViewChecked
    } from '@angular/core';
    // import { ArticleService } from './article.service';
    import {AccessRight} from './access-right.model';
    import {UtilisateurService } from './utilisateur.service';
    import {FormControl} from '@angular/forms';
    import {map, startWith} from 'rxjs/operators';
    import {Observable} from 'rxjs';
    import { Router, ActivatedRoute, ParamMap } from '@angular/router';
    import {MatSnackBar} from '@angular/material';
    import { SelectItem } from 'primeng/api';
    import { ColumnDefinition } from '../common/column-definition.interface';
    
    
    @Component({
      selector: 'app-access-right-new',
      templateUrl: './access-right-new.component.html',
      // styleUrls: ['./players.component.css']
    })
export class AccessRightNewComponent implements OnInit {
    private accessRight: AccessRight = new AccessRight();
  
    private columnsDefinitions: ColumnDefinition[] = [];
    private defaultColumnsDefinitions: ColumnDefinition[] = [];
    private columnsDefinitionsToDisplay :ColumnDefinition[] = [];
   
    private columnsToSelect : SelectItem[];
     private  selectedColumnsDefinitions: string[];
   
    private selectedColumnsNames: string[];
    private columnsTitles: string[] = [];
    private columnsTitlesAr: string[] = [];
    private columnsNames: string[] = [];
   
 
   
   
    private entityNames: any[] = [
      {value: 'Articles', viewValue: 'Articles'},
      {value: 'Magasins', viewValue: 'Magasins'},
      {value: 'UnMesures', viewValue: 'Unités Mesures'},
      {value: 'SchemasConfigurations', viewValue: 'Schemas Configurations'},
      {value: 'Lots', viewValue: 'Lots'},
      {value: 'Classes', viewValue: 'Classes'},
      {value: 'Entrees', viewValue: 'Entrees'},
      {value: 'Sorties', viewValue: 'Sorties'},
      {value: 'Utilisateurs', viewValue: 'Utilisateurs'},
      {value: 'DroitsAcces', viewValue: 'Droits Accés'},
      {value: 'Groupes', viewValue: 'Groupes'},
      {value: 'FacturesAchats', viewValue: 'Factures Achats'},
      {value: 'FacturesVentes', viewValue: 'Factures Ventes'},
      {value: 'CreditsAchats', viewValue: 'Credits Achats'},
      {value: 'CreditsVentes', viewValue: 'Credits Ventes'},
      {value: 'Fournisseurs', viewValue: 'Fournisseurs'},
      {value: 'Clients', viewValue: 'Clients'},
      {value: 'Journaux', viewValue: 'Journaux'},
      {value: 'OperationsCourantes', viewValue: 'Opérations Courantes'},
      {value: 'ComptesFournisseurs', viewValue: 'Comptes Fournisseurs'},
      {value: 'ComptesClients', viewValue: 'Comptes Clients'},
      {value: 'RelevesAchats', viewValue: 'Releves Achats'},
      {value: 'RelevesVentes', viewValue: 'Releves Ventes'},
      {value: 'DemandesVentes', viewValue: 'Demandes Ventes'},
      {value: 'DemandesAchats', viewValue: 'Demandes Achats'},
      {value: 'BonsLivraisons', viewValue: 'Bons Livraisons'},
      {value: 'BonsReceptions', viewValue: 'Bons Réceptions'},
      {value: 'Organisations', viewValue: 'Organisations'}
    ];
    private rightNames: any[] = [
      {value: 'Ajout', viewValue: 'Ajout'},
      {value: 'Modification', viewValue: 'Modification'},
      {value: 'Visualisation', viewValue: 'Visualisation'},
      {value: 'Suppression', viewValue: 'Suppression'},
      {value: 'SuppressionDefenitive', viewValue: 'Suppression Définitive'},
      {value: 'VisualisationFiltrée', viewValue: 'Visualisation Filtrée'},
      {value: 'VisualisationPersonalise', viewValue: 'Visualisation Personalisée'},
      {value: 'Historique', viewValue: 'Historique'}      
    ];
      constructor( private route: ActivatedRoute,
        private router: Router , private utilisateurService: UtilisateurService, private matSnackBar: MatSnackBar ) {
      }
      ngOnInit() {
        this.columnsToSelect = [
          {label: 'Ajout', value: 'Ajout', title: 'Ajout'},
          {label: 'Modification', value: 'Modification', title: 'Modification'},
          {label: 'Visualisation', value: 'Visualisation', title: 'Visualisation'},
          {label: 'Visualisation Filtrée', value: 'VisualisationFiltrée', title: 'Visualisation Filtrée'},
          {label: 'Suppression', value: 'Suppression', title: 'Suppression'},
          {label: 'Suppression Defenitive', value: 'SuppressionDefenitive', title: 'Suppression Defenitive'},
          {label: 'Visualisation Personalise', value: 'VisualisationPersonalise', title: 'Visualisation Personalise'},
          {label: 'Historique', value: 'Historique', title: 'Historique'}
          
      ];
      this.defaultColumnsDefinitions = [
        {name: 'Ajout' , displayTitle: 'Ajout'},
      {name: 'Modification' , displayTitle: 'Modification'},
      {name: 'Visualisation' , displayTitle: 'Visualisation'},
      {name: 'VisualisationFiltrée' , displayTitle: 'Visualisation Filtrée'},
      {name: 'VisualisationPersonalise' , displayTitle: 'Visualisation Personalisé'},
      {name: 'SuppressionDefenitive' , displayTitle: 'Suppression Defenitive'},
      {name: 'Suppression' , displayTitle: 'Suppression'},
      {name: 'Historique' , displayTitle: 'Historique'}
    ];
      this.selectedColumnsNames = [
        'Ajout',
       'Modification',
       'Visualisation'
        ];
       
      }
    saveAccessRight() {
      
      if (this.selectedColumnsDefinitions.length !== 0) {
        this.accessRight.rights = [];
      this.selectedColumnsDefinitions.forEach( (right, index) => {
        this.accessRight.rights[index] = right;
  } , this);
      } else {
        this.accessRight.rights = [];
      }
        this.utilisateurService.addAccessRight(this.accessRight).subscribe((response) => {
            if (response.error_message ===  undefined) {
              // this.save.emit(response);
              this.accessRight = new AccessRight();
              this.openSnackBar(  'Droit Enregistré');
            } else {
             // show error message
             this.openSnackBar(  'Erreurs!');
             
            }
        });
    }
    loadGridAccessRights() {
      this.router.navigate(['/pms/administration/accessRights']) ;
    }

       vider() {
         this.accessRight = new AccessRight();
       }
       openSnackBar(messageToDisplay) {
        this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
            verticalPosition: 'top'});
       }
       specifyListColumnsToBeDisplayed() {
        
        //  console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
         this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined?
          this.defaultColumnsDefinitions : [];
          this.selectedColumnsNames = [];
          
          if (this.selectedColumnsDefinitions !== undefined)
           {
             this.selectedColumnsDefinitions.forEach((colName) => { 
                  const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
                  const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
                  const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
                  this.columnsDefinitionsToDisplay.push(colDef);
                 });
           }
          this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
         this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
                    return colDef.displayTitle;
          });
          console.log('columns to disp:', this.columnsDefinitionsToDisplay);
          this.columnsNames = [];
          this.columnsNames[0] = 'select';
          this.columnsDefinitionsToDisplay .map((colDef) => {
           this.columnsNames.push(colDef.name);
          if (this.selectedColumnsDefinitions !== undefined) {
           this.selectedColumnsNames.push(colDef.displayTitle);
          }
       });
          this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
           return colDef.displayTitleAr;
       });
       }
      
      
    }
