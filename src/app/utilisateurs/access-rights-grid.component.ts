import { Component, OnInit, Output, ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {AccessRight} from './access-right.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {UtilisateurService} from './utilisateur.service';

import { AccessRightElement } from './access-right.interface';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
// import { DialogAddDocumentClientComponent} from './popups';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {AccessRightDataFilter } from './access-right-data.filter';
import { SelectItem } from 'primeng/api';
@Component({
  selector: 'app-access-rights-grid',
  templateUrl: './access-rights-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class AccessRightsGridComponent implements OnInit {

  private accessRight: AccessRight =  new AccessRight();
  private showFilter = false;
  private showSelectColumnsMultiSelect = false;
  private accessRightDataFilter = new AccessRightDataFilter();
  private selection = new SelectionModel<AccessRight>(true, []);
  


 selectedAccessRight: AccessRight ;

private deleteEnCours = false;

 @Input()
 private listAccessRights: any = [] ;

 // listClientsOrgin: any = [];
 private checkedAll: false;
 private showParticulier = false;
 private showEntreprise = false;
 private  dataAccessRightsSource: MatTableDataSource<AccessRightElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay :ColumnDefinition[] = [];

 private columnsToSelect : SelectItem[];
  private  selectedColumnsDefinitions: string[];

 private selectedColumnsNames: string[];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];

  constructor( private route: ActivatedRoute, private utilisateursService: UtilisateurService,
    private router: Router, private matSnackBar: MatSnackBar, private dialog: MatDialog ) {
  }
  ngOnInit() {
    this.defaultColumnsDefinitions = [
      {name: 'code' , displayTitle: 'Code'},
    {name: 'entityName' , displayTitle: 'Entité'}

  ];
    
    this.columnsToSelect = [
      {label: 'Code', value: 'code', title: 'Code'},
      {label: 'Entité', value: 'entityName', title: 'Entité'}
  ];
  this.selectedColumnsDefinitions = [
    'code',
   'entityName'
    ];


    this.utilisateursService.getPageAccessRights(0, 5, null).subscribe((rights) => {
      this.listAccessRights = rights;
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });
  

  }
  deleteAccessRight(accessRight) {
     // console.log('call delete !', client );
    this.utilisateursService.deleteAccessRight(accessRight.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData(null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.utilisateursService.getPageAccessRights(0, 5, filter).subscribe((rights) => {
    this.listAccessRights = rights;
    // this.listClients[this.listClients.length] = this.lastClientAdded;
    this.transformDataToByConsumedByMatTable();
   });
  // this.listClientsOrgin = this.listClients ;
}


checkOneActionInvoked() {
  const listSelectedRights: AccessRight[] = [];

if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((accessRightElement) => {
  return accessRightElement.code;
});
this.listAccessRights.forEach(r => {
  if (codes.findIndex(code => code === r.code) > -1) {
    listSelectedRights.push(r);
  }
  });
}
// this.select.emit(listSelectedClients);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listAccessRights !== undefined) {
    this.listAccessRights.forEach(accessR => {
      listElements.push( {code: accessR.code ,
        entityName: accessR.entityName,
        rights: accessR.rights,
      } );
    });
  }
    this.dataAccessRightsSource = new MatTableDataSource<AccessRightElement>(listElements);
    this.dataAccessRightsSource.sort = this.sort;
    this.dataAccessRightsSource.paginator = this.paginator;
    this.utilisateursService.getTotalAccessRights(this.accessRightDataFilter).subscribe((response) => {
     //  console.log('Total Clients   is ', response);
     if (this.dataAccessRightsSource.paginator !== undefined) {
       this.dataAccessRightsSource.paginator.length = response['totalAccessRights'];
     }
     });
}

masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
    //   this.dataAccessRightsSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataAccessRightsSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataAccessRightsSource.filter = filterValue.trim().toLowerCase();
  if (this.dataAccessRightsSource.paginator) {
    this.dataAccessRightsSource.paginator.firstPage();
  }
}
loadAddAccessRightComponent() {
  this.router.navigate(['/pms/administration/ajouterAccessRight']) ;

}
loadEditAccessRightComponent() {
  if (this.selection.selected.length === 0) {
    this.openSnackBar(' Aucun élément sélectionné' );
    // this.loadData();
    } else
   {
  this.router.navigate(['/pms/administration/editerAccessRight', this.selection.selected[ this.selection.selected.length - 1].code]) ;
    }
}
supprimerAccessRights()
{
  if ( this.selection.selected.length !== 0) {
    this.deleteEnCours = true;
      this.selection.selected.forEach(function(accessRight, index) {
          this.utilisateursService.deleteAccessRight(accessRight.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.deleteEnCours = false;
            this.openSnackBar( ' Element(s) Supprimé(s)');
             this.loadData(null);
            }
      }, this);
    } else {
      this.openSnackBar('Aucun Droit Séléctionné!');
    }
}
refresh()
{
  this.loadData(null);
  
}
  openSnackBar(messageToDisplay) {
 this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
     verticalPosition: 'top'});
}

changePage($event) {
  console.log('page event is ', $event);
 this.utilisateursService.getPageAccessRights($event.pageIndex, $event.pageSize,
  this.accessRightDataFilter)
 .subscribe((rights) => {
    this.listAccessRights = rights;
    const listElements = [];
 if (this.listAccessRights !== undefined) {
    this.listAccessRights.forEach(right => {
      listElements.push( {code: right.code ,
        entityName: right.entityName ,
        rights: right.rights} );
    });
  }
     this.dataAccessRightsSource = new MatTableDataSource<AccessRightElement>(listElements);
     this.utilisateursService.getTotalAccessRights(this.accessRightDataFilter).
     subscribe((response) => {
      console.log('Total clients is ', response);
      // this.dataClientsSource.paginator = this.paginator;
      if (this.dataAccessRightsSource.paginator) {
        
       this.dataAccessRightsSource.paginator.length = response.totalAccessRights;
      }
     });

    });
}
toggleFilterPanel() {
  this.showFilter = !this.showFilter;
}
toggleShowColumnsMultiSelect() {
 this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
}
loadFiltredData() {
 console.log('the filter is ', this.accessRightDataFilter);
 this.loadData(this.accessRightDataFilter);
}

specifyListColumnsToBeDisplayed() {
  
   console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
   this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
 ?
    this.defaultColumnsDefinitions : [];
    this.selectedColumnsNames = [];
    
    if (this.selectedColumnsDefinitions !== undefined)
     {
       this.selectedColumnsDefinitions.forEach((colName) => { 
            const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
            const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
            const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
            this.columnsDefinitionsToDisplay.push(colDef);
           });
     }
    this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
   this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
              return colDef.displayTitle;
    });
    console.log('columns to disp:', this.columnsDefinitionsToDisplay);
    this.columnsNames = [];
    this.columnsNames[0] = 'select';
    this.columnsDefinitionsToDisplay .map((colDef) => {
     this.columnsNames.push(colDef.name);
    if (this.selectedColumnsDefinitions !== undefined) {
     this.selectedColumnsNames.push(colDef.displayTitle);
    }
 });
    this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
     return colDef.displayTitleAr;
 });
 }






}
