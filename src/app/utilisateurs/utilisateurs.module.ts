import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
 import { AppComponent } from '../app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ReactiveFormsModule } from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatChipsModule} from '@angular/material/chips';
import {MatMenuModule} from '@angular/material/menu';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {TableModule} from 'primeng/table';
import {MatSortModule} from '@angular/material/sort';
import {MatSelectModule} from '@angular/material/select';
import {MatTabsModule} from '@angular/material/tabs';
import {PickListModule} from 'primeng/picklist';
import {MultiSelectModule} from 'primeng/multiselect';
import { ProfilesVentesResolver } from './profiles-ventes-resolver';
import { ProfilesAchatsResolver } from './profiles-achats-resolver';
// import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';
 import { UtilisateursGridComponent } from './utilisateurs-grid.component';
 import { LoginComponent } from './login.component';
 import { UtilisateurNewComponent } from './utilisateur-new.component';
 import { UtilisateurEditComponent } from './utilisateur-edit.component';
 import { HomeUtilisateursComponent } from './home-utilisateurs.component';
 import { AccessRightNewComponent } from './access-right-new.component';
 import { AccessRightEditComponent } from './access-right-edit.component';
 import { AccessRightHomeComponent } from './access-right-home.component';
 import { AccessRightsGridComponent } from './access-rights-grid.component';
 import { InscriptionEditComponent } from './inscription-edit.component';
 import { InscriptionsGridComponent } from './inscriptions-grid.component';
 import { SignupComponent } from './signup.component';
 import { JwtModule, JWT_OPTIONS  } from '@auth0/angular-jwt';
 import { JwtHelperService } from '@auth0/angular-jwt';
import {FormsModule } from '@angular/forms';
// import { commonRoute } from './common.route';
 import {APP_BASE_HREF} from '@angular/common';

   import { AccessRightsResolver } from './accessRights-resolver';
 // const BASE_URL = [{provide: APP_BASE_HREF, useValue: '/common'}];



 import {MatPaginatorModule} from '@angular/material/paginator';
 import { RouterModule, provideRoutes} from '@angular/router';
 // import { CommunicationService } from './magasins/communication.service';
 import {MatSidenavModule} from '@angular/material/sidenav';
 import {MatRadioModule} from '@angular/material/radio';
@NgModule({
  declarations: [
    LoginComponent,
    UtilisateursGridComponent,
    HomeUtilisateursComponent,
    UtilisateurEditComponent,
    UtilisateurNewComponent,
    AccessRightsGridComponent,
    AccessRightHomeComponent,
    AccessRightEditComponent,
    AccessRightNewComponent,
    InscriptionsGridComponent,
    InscriptionEditComponent,
    SignupComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, MatDatepickerModule, MatNativeDateModule,
     BrowserAnimationsModule, ReactiveFormsModule, MatAutocompleteModule,
    MatInputModule, MatCardModule, MatCheckboxModule, MatButtonModule, MatIconModule,
    MatMenuModule, MatProgressSpinnerModule, MatTableModule, RouterModule,
    FormsModule, TableModule , MatSortModule, MatSelectModule, MatTabsModule,
     PickListModule, MatPaginatorModule, MatSidenavModule, MatRadioModule, MatChipsModule,
     JwtModule, MultiSelectModule
     

],
  providers: [MatNativeDateModule, AccessRightsResolver, 
    JwtHelperService , ProfilesVentesResolver, ProfilesAchatsResolver
     // BASE_URL
  ],
  exports : [
    RouterModule,
    SignupComponent,
    LoginComponent,
  ],
  bootstrap: [AppComponent]
})
export class UtilisateursModule { }
