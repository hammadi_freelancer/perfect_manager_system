import { Component, OnInit , Output, EventEmitter } from '@angular/core';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatAutocompleteSelectedEvent, MatChipInputEvent, MatAutocomplete} from '@angular/material';
// import { ArticleService } from './article.service';
import { AfterContentChecked, Input, ElementRef ,
    ViewChild } from '@angular/core';
import {Inscription} from './inscription.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { UtilisateurService } from './utilisateur.service';
import {MatSnackBar} from '@angular/material';
import { JwtHelperService } from '@auth0/angular-jwt';
import { JwtModule, JWT_OPTIONS  } from '@auth0/angular-jwt';

type ELEMENT_SI =
'EXCEL_ACCESS' | 'LOGICELS_DESKTOP' | 'SERVICES_WEB' | 'AUTRES' | 'PAPIERS';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  // styleUrls: ['./players.component.css']
})
export class SignupComponent implements OnInit {

private inscription: Inscription = new Inscription();


visible = true;
selectable = true;
removable = true;
addOnBlur = true;
separatorKeysCodes: number[] = [ENTER, COMMA];
elementSICtrl = new FormControl();
filteredElementsSI: Observable<string[]>;
elementsSI: string[] = ['EXCEL_ACCESS'];
allElementsSI: string[] = ['EXCEL_ACCESS', 'LOGICELS_DESKTOP', 'SERVICES_WEB', 'PAPIERS', 'AUTRES'];


@ViewChild('elementSIInput') elementSIInput: ElementRef<HTMLInputElement>;
@ViewChild('auto') matAutocomplete: MatAutocomplete;

  constructor( private route: ActivatedRoute, private matSnackBar: MatSnackBar ,
    private router: Router , private utilisateurService: UtilisateurService,
  ) {
    this.filteredElementsSI = this.elementSICtrl.valueChanges.pipe(
      startWith(null),
      map((elementSI: string | null) => elementSI ? this._filter(elementSI) : this.allElementsSI.slice()));
  }
  ngOnInit() {

  }

  signup() {
        this.utilisateurService.addInscription(this.inscription).subscribe((response) => {
             if (response.error_message ===  undefined) {
               // this.save.emit(response);
               this.inscription = new Inscription();
               this.openSnackBar(  'Demande d\'inscription Enregistrée:\n' +
               'Vous aurez une réponse sur votre demande d\'inscription dans les meilleures délais' );
             } else {
              // show error message
              this.openSnackBar(  'Echéc d\'inscription');
              
             }
         });
     }
     logoChanged($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.inscription.logo = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     mapChanged($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.inscription.map = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }


 add(event: MatChipInputEvent): void {
  // Add fruit only when MatAutocomplete is not open
  // To make sure this does not conflict with OptionSelected Event
  if (!this.matAutocomplete.isOpen) {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.elementsSI.push(value.trim());
      this.inscription.elementsSI = this.elementsSI;
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }

    this.elementSICtrl.setValue(null);
  }
}

remove(elementSI: string): void {
  const index = this.elementsSI.indexOf(elementSI);

  if (index >= 0) {
    this.elementsSI.splice(index, 1);
  }
}

selected(event: MatAutocompleteSelectedEvent): void {

  console.log('selected is called ');
  console.log(event);
  this.elementsSI.push(event.option.value);
  this.inscription.elementsSI = this.elementsSI;
  this.elementSIInput.nativeElement.value = event.option.value;
  this.elementSICtrl.setValue(null);
}

private _filter(value: string): string[] {
  const filterValue = value.toLowerCase();

  return this.allElementsSI.filter(elementSI => elementSI.toLowerCase().indexOf(filterValue) === 0);
}

}
