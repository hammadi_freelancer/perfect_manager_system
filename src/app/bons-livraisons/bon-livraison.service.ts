import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import {BonLivraison} from './bon-livraison.model' ;
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_BONS_LIVRAISONS = 'http://localhost:8082/bonsLivraisons/getBonsLivraisons';
const  URL_GET_PAGE_BONS_LIVRAISONS= 'http://localhost:8082/bonsLivraisons/getPageBonsLivraisons';

const URL_GET_BON_LIVRAISON = 'http://localhost:8082/bonsLivraisons/getBonLivraison';
const  URL_ADD_BON_LIVRAISON   = 'http://localhost:8082/bonsLivraisons/addBonLivraison';
const  URL_UPDATE_BON_LIVRAISON   = 'http://localhost:8082/bonsLivraisons/updateBonLivraison';
const  URL_DELETE_BON_LIVRAISON  = 'http://localhost:8082/bonsLivraisons/deleteBonLivraison';
const  URL_GET_TOTAL_BONS_LIVRAISONS = 'http://localhost:8082/bonsLivraisons/getTotalBonsLivraisons';



@Injectable({
  providedIn: 'root'
})
export class BonLivraisonService {

  constructor(private httpClient: HttpClient) {

   }
   getTotalBonsLivraisons(): Observable<any> {
    return this.httpClient
    .get<any>(URL_GET_TOTAL_BONS_LIVRAISONS);
   }


   getPageBonsLivraisons(pageNumber, nbElementsPerPage): Observable<BonLivraison[]> {
    const params = new HttpParams()
    .set('pageNumber', pageNumber)
    .set('nbElementsPerPage', nbElementsPerPage);
    return this.httpClient
    .get<BonLivraison[]>(URL_GET_PAGE_BONS_LIVRAISONS, {params});
   }

   getBonsLivraisons(): Observable<BonLivraison[]> {
   
    return this.httpClient
    .get<BonLivraison[]>(URL_GET_LIST_BONS_LIVRAISONS);
   }
   addBonLivraison(bonLivraison: BonLivraison): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_BON_LIVRAISON, bonLivraison);
  }
  updateBonLivraison(bonLivraison: BonLivraison): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_BON_LIVRAISON, bonLivraison);
  }
  deleteBonLivraison(codeBonLivraison): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_BON_LIVRAISON + '/' + codeBonLivraison);
  }
  getBonLivraison(codeBonLivraison): Observable<BonLivraison> {
    return this.httpClient.get<BonLivraison>(URL_GET_BON_LIVRAISON + '/' + codeBonLivraison);
  }


}
