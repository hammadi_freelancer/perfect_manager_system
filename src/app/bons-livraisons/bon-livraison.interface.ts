export interface BonLivraisonElement {
   
         code: string;
         numeroBonLivraison: string;
         cinClient: string;
         nomClient: string;
         prenomClient: string;
         dateOperation: string;
         dateDebut: string;
         dateFin: string;
         montantTotal: string;
        etat: string;

        }
