import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,
  
    ViewChild } from '@angular/core';
  // import { ArticleService } from './article.service';
  import {BonLivraison} from './bon-livraison.model';
  // import { ParametresVentes } from '../parametres-ventes.model';
  
  import {BonLivraisonService} from './bon-livraison.service';
  
  // import {VentesService} from '../ventes.service';
  import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
  
  import {FormControl} from '@angular/forms';
  import {map, startWith} from 'rxjs/operators';
  import {Observable} from 'rxjs';
  import { Router, ActivatedRoute, ParamMap } from '@angular/router';
  // import {ActionData} from './action-data.interface';
  import { ClientService } from '../ventes/clients/client.service';
  import { FournisseurService } from '../achats/fournisseurs/fournisseur.service';
  
  import { ClientFilter } from '../ventes/clients/client.filter';
  import { FournisseurFilter } from '../achats/fournisseurs/fournisseur.filter';
  import { Fournisseur } from '../achats/fournisseurs/fournisseur.model';
  
  import { ArticleFilter } from '../stocks/articles/article.filter';
  
  import {Client} from '../ventes/clients/client.model';
  
  import {LigneBonLivraison} from './ligne-bon-livraison.model';
  
  import {LigneBonLivraisonElement} from './ligne-bon-livraison.interface';
  import { ColumnDefinition } from '../common/column-definition.interface';
  
  import {MatSnackBar} from '@angular/material';
  import { MatExpansionPanel } from '@angular/material';
  import {ArticleService} from '..//stocks/articles/article.service';
  import {Article} from '../stocks/articles/article.model';
  import {ModePayementLibelle} from './mode-payement-libelle.interface';
  import {SelectionModel} from '@angular/cdk/collections';
  
  type LibelleModePayement = 'Credit'| 'Comptant' | 'Cheque' ;
  
  @Component({
    selector: 'app-bon-livraison-edit',
    templateUrl: './bon-livraison-edit.component.html',
    // styleUrls: ['./players.component.css']
  })
export class BonLivraisonEditComponent implements OnInit  {

  private bonLivraison: BonLivraison =  new BonLivraison();
  
  
  private updateEnCours = false;
  private saveClientEnCours = false;
  
  @ViewChild('firstMatExpansionPanel')
   private firstMatExpansionPanel : MatExpansionPanel;
  
   @ViewChild('intervenantsMatExpansionPanel')
   private intervenantsMatExpansionPanel : MatExpansionPanel;
  
   @ViewChild('selectArticlesMatExpansionPanel')
   private selectArticlesMatExpansionPanel : MatExpansionPanel;
  
   @ViewChild('listLignesMatExpansionPanel')
   private listLignesMatExpansionPanel : MatExpansionPanel;
  
   @ViewChild('paiementDataMatExpansionPanel')
   private paiementDataMatExpansionPanel : MatExpansionPanel;
  
  private clientFilter: ClientFilter;
  private fournisseurFilter: FournisseurFilter;
  
  private articleFilter: ArticleFilter;
  private listClients: Client[];
  private listFournisseurs: Fournisseur[];
  
  private listArticles: Article[];
  
  private sourceListArticles: Article[] = [];
  private targetListArticles: Article[] = [];
  
  private payementModes: any[] = [
    {value: 'Comptant', viewValue: 'Comptant'},
    {value: 'Cheque', viewValue: 'Chèque'},
    {value: 'Credit', viewValue: 'Crédit'},
    {value: 'Avance', viewValue: 'Avance'},
    {value: 'Comptant_Cheque', viewValue: 'Comptant/Chèque'},
    {value: 'Comptant_Avance', viewValue: 'Comptant/Avance'},
    {value: 'Credit_Comptant', viewValue: 'Crédit/Comptant'},
    {value: 'Credit_Cheque', viewValue: 'Crédit/Chèque'},
    {value: 'Credit_Avance', viewValue: 'Crédit/Avance'},
    {value: 'Donnation', viewValue: 'Amicalité'}
  ];
  private etatsBonLivraison: any[] = [
    {value: 'Initial', viewValue: 'Initiale'},
    {value: 'Valide', viewValue: 'Validée'},
    {value: 'Annule', viewValue: 'Annulée'},
    {value: 'Regle', viewValue: 'Reglée'},
    {value: 'NonRegle', viewValue: 'Non Reglée'},
    {value: 'PartiellementRegle', viewValue: 'Partiellement Reglée'},
  
  ];
  private typesBonLivraison: any[] = [
    {value: 'Achats', viewValue: 'Achats'},
    {value: 'Ventes', viewValue: 'Ventes'},
    {value: '', viewValue: ''},
  ];
    constructor( private route: ActivatedRoute,
      private router: Router, private bonLivraisonService: BonLivraisonService,
       private clientService: ClientService,
       private fournisseurService: FournisseurService,
       
      private articleService: ArticleService,
     private matSnackBar: MatSnackBar, private elRef: ElementRef) {
    }
  ngOnInit() {
    this.fournisseurFilter = new FournisseurFilter([]);
    // this.selectCin = true;
    //  this.selectCinF = true;
     this.fournisseurService.getFournisseurs().subscribe((listFournisseurs) => {
          this.listFournisseurs = listFournisseurs;
          this.fournisseurFilter = new FournisseurFilter(this.listFournisseurs);
        });
    const codeBonLivraison = this.route.snapshot.paramMap.get('codeBonLivraison');
    
    this.bonLivraisonService.getBonLivraison(codeBonLivraison).subscribe(
      (bonLivraison) =>  {
  
             this.bonLivraison = bonLivraison;
         
        console.log('livraison courante sent from the server is :', bonLivraison);
             if (this.bonLivraison.etat === 'Valide' || this.bonLivraison.etat === 'Annule')
              {
                this.etatsBonLivraison.splice(0, 1);
              }
              if (this.bonLivraison.etat === 'Initial')
                {
                  this.etatsBonLivraison.splice(2, 4);
                }
               // this.transformDataToByConsumedByMatTable();
            });
            this.firstMatExpansionPanel.open();

            this.listClients = this.route.snapshot.data.clients;
        
             this.clientFilter = new ClientFilter(this.listClients);
             this.selectArticlesMatExpansionPanel.opened.subscribe(openedPanel => {
              this.articleService.getArticles().subscribe((listArticles) => {
                    this.sourceListArticles = listArticles;
                    this.targetListArticles = listArticles;
              });
            });
        
            this.intervenantsMatExpansionPanel.opened.subscribe(openedPanel => {
           
            });
  }
  loadGridBonsLivraisons() {
    this.router.navigate(['/pms/suivie/bonsLivraisons']) ;
  }
updateBonLivraison() {


  this.updateEnCours = true;
  this.bonLivraisonService.updateBonLivraison(this.bonLivraison).subscribe((response) => {
    this.updateEnCours = false;
    if (response.error_message ===  undefined) {
      console.log('the response of update is ', response );
      if (this.bonLivraison.etat === 'Valide') {
          this.etatsBonLivraison = [
            {value: 'Annule', viewValue: 'Annulé'},
            {value: 'Valide', viewValue: 'Validé'},
            {value: 'Reglee', viewValue: 'Réglée'},
            {value: 'NonReglee', viewValue: 'Non Réglée'},
            {value: 'PartiellementRegle', viewValue: 'Partiellement Réglée'},
            
  
          ];
        }
    

      this.openSnackBar( ' Opération mise à jour ');
    } else {
      this.openSnackBar( ' Erreurs!');
    }
}); 
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 
 fillDataNomPrenomClient()
 {
   const listFiltred  = this.listClients.filter((client) => (client.cin === this.bonLivraison.client.cin));
   this.bonLivraison.client.nom = listFiltred[0].nom;
   this.bonLivraison.client.prenom = listFiltred[0].prenom;
 }

 fillDataCINPrenomClient()
 {
   const listFiltred  = this.listClients.filter((client) => (client.nom === this.bonLivraison.client.nom));
   this.bonLivraison.client.cin = listFiltred[0].cin;
   this.bonLivraison.client.prenom = listFiltred[0].prenom;
 }
 fillDataCINNomClient()
 {
   const listFiltred  = this.listClients.filter((client) => (client.prenom === this.bonLivraison.client.prenom));
   const num =   Math.floor(Math.random() * Math.floor(listFiltred.length));
   this.bonLivraison.client.cin = listFiltred[num].cin;
   this.bonLivraison.client.nom= listFiltred[num].nom;
 }
 fillDataMatriculeRaisonClient() {
   const listFiltred  = this.listClients.filter((client) => (client.registreCommerce === this.bonLivraison.client.registreCommerce));
   this.bonLivraison.client.matriculeFiscale = listFiltred[0].matriculeFiscale;
   this.bonLivraison.client.raisonSociale = listFiltred[0].raisonSociale;
 }
 fillDataMatriculeRegistreCommercial()
 {
   const listFiltred  = this.listClients.filter((client) => (client.raisonSociale === this.bonLivraison.client.raisonSociale));
   this.bonLivraison.client.matriculeFiscale = listFiltred[0].matriculeFiscale;
   this.bonLivraison.client.registreCommerce = listFiltred[0].registreCommerce;
 }


saveClient() {
  
   // console.log(this.client);
   this.saveClientEnCours = true;
   this.clientService.addClient(this.bonLivraison.client).subscribe((response) => {
     this.saveClientEnCours = false;
       if (response.error_message ===  undefined) {
         // this.save.emit(response);
        // this.client = new Client();
         this.openSnackBar(  'Client Enregistré');
       } else {
        // show error message
        this.openSnackBar(  'Erreurs!:Essayer une autre fois');
        
       }
   });
}
















}
