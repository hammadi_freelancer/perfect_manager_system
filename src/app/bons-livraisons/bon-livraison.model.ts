
import { BaseEntity } from '../common/base-entity.model' ;
// import { Tranche } from './tranche.model' ;
import { Client } from '../ventes/clients/client.model' ;
//import { Fournisseur } from '../achats/fournisseurs/fournisseur.model' ;

// import { PayementData } from './payement-data.model';
// import { Article } from '../stocks/articles/article.model';
 import { LigneBonLivraison } from './ligne-bon-livraison.model';
// import { LigneReleveVente } from './ligne-releve-vente.model';
// import { Credit } from '../credits/credit.model';

type  EtatBonLivraison= 'Initial' |'Valide' | 'Annule' |'Regle'
|'NonRegle' | 'PartiellementRegle' | '' ;
type  TypeOperationCourante= 'Achats' |'Ventes' |'' ;
type  ModePaiement= 'Comptant' |'Credit' | 'Cheque' | 'Avance' |'Credit_Comptant' | 'Donnation' | '';

export class BonLivraison extends BaseEntity {
    constructor(
        public  code?: string,
        public numeroBonLivraison?: string,
        public dateOperationObject? : Date,
        public  dateDebutObject? : Date, 
        public dateFinObject? : Date, 
        public client?: Client,
        public montantTotal?: number,
        public marchandisesLivrees?: boolean,
        public montantTotalRegle?: boolean,
        public lignesBonLivraison?: LigneBonLivraison[],
        public etat?: EtatBonLivraison,
   
     ) {
         super();
         this.client = new Client();
         this.montantTotal = Number(0);
     
         
        //  this.listLignesRelVentes = [] ;
         // this.categoryClient = 'PERSONNE_PHYSIQUE';
    }
}
