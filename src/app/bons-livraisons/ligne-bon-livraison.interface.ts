export interface LigneBonLivraisonElement {
    
     codeArticle: string;
     libelleArticle: string;
     quantite: number;
     prixTotal: number;
        
    
    }
 