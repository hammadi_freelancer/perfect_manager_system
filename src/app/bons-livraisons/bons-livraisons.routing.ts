import { Injectable } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
// operationsCourantes components
import { BonLivraisonNewComponent } from './bon-livraison-new.component';
import { BonLivraisonEditComponent } from './bon-livraison-edit.component';
import { BonsLivraisonsGridComponent } from './bons-livraisons-grid.component';
import { BonLivraisonHomeComponent } from './bon-livraison-home.component';

import { ClientsResolver } from '../ventes/clients-resolver';

import {ModuleWithProviders  } from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';

 export const bonsLivraisonsRoute: Routes = [
    {
        path: 'bonsLivraisons',
        component: BonsLivraisonsGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterBonLivraison',
        component: BonLivraisonNewComponent,
        resolve: {
            clients: ClientsResolver,
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerBonLivraison/:codeBonLivraison',
        component: BonLivraisonEditComponent,
        resolve: {
            clients: ClientsResolver,
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
];
// export const ventesRoutingModule: ModuleWithProviders = RouterModule.forRoot(ventesRoute);
// commonRouting.providers = [{ provide: APP_BASE_HREF, useValue: '/common'} ] ;


