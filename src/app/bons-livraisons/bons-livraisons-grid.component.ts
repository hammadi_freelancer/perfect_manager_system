import { Component, OnInit, Output,ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {BonLivraison} from './bon-livraison.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {BonLivraisonService} from './bon-livraison.service';
// import { ActionData } from './action-data.interface';
// import { Client } from '../../ventes/clients/client.model';
import { BonLivraisonElement } from './bon-livraison.interface';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DialogAddClientComponent } from '../ventes/clients/dialog-add-client.component';

@Component({
  selector: 'app-bons-livraisons-grid',
  templateUrl: './bons-livraisons-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class BonsLivraisonsGridComponent implements OnInit {

  private bonLivraison: BonLivraison =  new BonLivraison();
  private selection = new SelectionModel<BonLivraisonElement>(true, []);
  
@Output()
select: EventEmitter<BonLivraison[]> = new EventEmitter<BonLivraison[]>();


 selectedBonLivraison: BonLivraison ;

 

 @Input()
 private listBonsLivraisons: any = [] ;
 private checkedAll: false;

 private  dataBonsLivraisonsSource: MatTableDataSource<BonLivraisonElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
  constructor( private route: ActivatedRoute, private bonLivraisonService: BonLivraisonService,
    private router: Router, private matSnackBar: MatSnackBar,
  private  dialog: MatDialog ) {
  }
  ngOnInit() {
    this.bonLivraisonService.getPageBonsLivraisons(0, 5).
    subscribe((operationsCourantes) => {
      this.listBonsLivraisons = operationsCourantes;
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });


  }
  deleteBonLivraison(bonLivraison) {
      console.log('call delete !', bonLivraison );
    this.bonLivraisonService.deleteBonLivraison(bonLivraison.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData();

      } else {
       // show error message
      }
     });

  }
loadData() {
  this.bonLivraisonService.getPageBonsLivraisons(0, 5).subscribe((operationsCourantes) => {
    this.listBonsLivraisons = operationsCourantes;
    this.transformDataToByConsumedByMatTable();
   });
}


checkOneActionInvoked() {
  const listSelectedBonsLivraisons: BonLivraison[] = [];

if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((OperCElement) => {
  return OperCElement.code;
});
this.listBonsLivraisons.forEach(OP => {
  if (codes.findIndex(code => code === OP.code) > -1) {
    listSelectedBonsLivraisons.push(OP);
  }
  });
}
this.select.emit(listSelectedBonsLivraisons);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listBonsLivraisons !== undefined) {
    this.listBonsLivraisons.forEach(opCourante => {
      listElements.push( {
        code: opCourante.code ,
        numeroBonLivraison: opCourante.numeroBonLivraison,
        cinClient: opCourante.cinClient,
        nomClient: opCourante.nomClient ,
        prenomClient: opCourante.prenomClient ,
        cinFournisseur: opCourante.cinFournisseur,
        nomFournisseur: opCourante.nomFournisseur ,
        prenomFournisseur: opCourante.prenomFournisseur ,
       valeurFinanciere: opCourante.valeurFinanciere ,
       valeurBenefice: opCourante.valeurBenefice ,
        dateOperation: opCourante.dateOperation,
        modePaiement: opCourante.modePaiement,
       etat: opCourante.etat,
       type: opCourante.type,
       livre: opCourante.livre ? 'Oui' : 'Non',
       recu: opCourante.recu ? 'Oui' : 'Non',
      } );
    });
  }
    this.dataBonsLivraisonsSource = new MatTableDataSource<BonLivraisonElement>(listElements);
    this.dataBonsLivraisonsSource.sort = this.sort;
    this.dataBonsLivraisonsSource.paginator = this.paginator;
    this.bonLivraisonService.getTotalBonsLivraisons().subscribe((response) => {
    //   console.log('Total Releves de Ventes  is ', response);
       this.dataBonsLivraisonsSource.paginator.length = response['totalBonsLivraisons'];
     });
}
 specifyListColumnsToBeDisplayed() {
   this.columnsDefinitions = [
   {name: 'numeroBonLivraison' , displayTitle: 'N°Opér'},
   {name: 'dateOperation' , displayTitle: 'Date Opération'},
    {name: 'nomClient' , displayTitle: 'Nom Client'},
   {name: 'prenomClient' , displayTitle: 'Prénom'},
    {name: 'valeurFinanciere' , displayTitle: 'Valeur'},
    {name: 'valeurBenefice' , displayTitle: 'Benefice'},
    {name: 'etat', displayTitle: 'Etat' }
   ];
   this.columnsTitles = this.columnsDefinitions.map((colDef) => {
             return colDef.displayTitle;
   });
   this.columnsNames[0] = 'select';
   this.columnsDefinitions.map((colDef) => {
    this.columnsNames.push(colDef.name);
});
   this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
    return colDef.displayTitleAr;
});
}
masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataBonsLivraisonsSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataBonsLivraisonsSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataBonsLivraisonsSource.filter = filterValue.trim().toLowerCase();
  if (this.dataBonsLivraisonsSource.paginator) {
    this.dataBonsLivraisonsSource.paginator.firstPage();
  }
}

loadAddBonLivraisonComponent() {
  this.router.navigate(['/pms/suivie/ajouterBonLivraison']) ;

}
loadEditBonLivraisonComponent() {
  this.router.navigate(['/pms/suivie/editerBonLivraison', this.selection.selected[ this.selection.selected.length - 1].code]) ;

}
supprimerBonsLivraisons()
{

  if ( this.selection.selected.length !== 0) {
      this.selection.selected.forEach(function(opCourante, index) {

   this.bonLivraisonService.deleteBonLivraison(opCourante.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.openSnackBar( this.selection.selected.length + ' Opération de Courante Supprimé(s)');
            this.selection.selected = [];
            this.loadData();
            }
      }, this);
    } else {
      this.openSnackBar('Aucune Opération Séléctionnée !');
    }
}
refresh() {
  this.loadData();
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

 history()
 {
   
 }
 addClient()
 {
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
  };

  const dialogRef = this.dialog.open(DialogAddClientComponent,
    dialogConfig
  );
 }
 attacherDocuments()
 {

 }
 changePage($event) {
  console.log('page event is ', $event);
 this.bonLivraisonService.getPageBonsLivraisons($event.pageIndex, $event.pageSize ).subscribe((opsCourantes) => {
    this.listBonsLivraisons = opsCourantes;
   const listElements = [];
   this.listBonsLivraisons.forEach(opCourante => {
    listElements.push( {
      code: opCourante.code ,
      numeroBonLivraison: opCourante.numeroBonLivraison,
      cinClient: opCourante.cinClient,
      nomClient: opCourante.nomClient ,
      prenomClient: opCourante.prenomClient ,
      cinFournisseur: opCourante.cinFournisseur,
      nomFournisseur: opCourante.nomFournisseur ,
      prenomFournisseur: opCourante.prenomFournisseur ,
     valeurFianciere: opCourante.valeurFianciere ,
     valeurBenefice: opCourante.valeurBenefice ,
      dateOperation: opCourante.dateOperation,
      modePaiement: opCourante.modePaiement,
     etat: opCourante.etat,
     type: opCourante.type,
     livre: opCourante.livre ? 'Oui' : 'Non',
     recu: opCourante.recu ? 'Oui' : 'Non',
      
  });
});
     this.dataBonsLivraisonsSource= new MatTableDataSource<BonLivraisonElement>(listElements);

     
     this.bonLivraisonService.getTotalBonsLivraisons().subscribe((response) => {
      console.log('Total OPer Courantes  is ', response);
      if (this.dataBonsLivraisonsSource.paginator) {
       this.dataBonsLivraisonsSource.paginator.length = response['totalBonsLivraisons'];
      }
     });

    });
}

}
