import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {BonLivraison} from './bon-livraison.model';
// import { ParametresVentes } from '../parametres-ventes.model';

import {BonLivraisonService} from './bon-livraison.service';

// import {VentesService} from '../ventes.service';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
// import {ActionData} from './action-data.interface';
import { ClientService } from '../ventes/clients/client.service';
import { FournisseurService } from '../achats/fournisseurs/fournisseur.service';

import { ClientFilter } from '../ventes/clients/client.filter';
import { FournisseurFilter } from '../achats/fournisseurs/fournisseur.filter';
import { Fournisseur } from '../achats/fournisseurs/fournisseur.model';

import { ArticleFilter } from '../stocks/articles/article.filter';

import {Client} from '../ventes/clients/client.model';

import {LigneBonLivraison} from './ligne-bon-livraison.model';

import {LigneBonLivraisonElement} from './ligne-bon-livraison.interface';
import { ColumnDefinition } from '../common/column-definition.interface';

import {MatSnackBar} from '@angular/material';
import { MatExpansionPanel } from '@angular/material';
import {ArticleService} from '..//stocks/articles/article.service';
import {Article} from '../stocks/articles/article.model';
import {ModePayementLibelle} from './mode-payement-libelle.interface';
import {SelectionModel} from '@angular/cdk/collections';

type LibelleModePayement = 'Credit'| 'Comptant' | 'Cheque' ;

@Component({
  selector: 'app-bon-livraison-new',
  templateUrl: './bon-livraison-new.component.html',
  // styleUrls: ['./players.component.css']
})
export class BonLivraisonNewComponent implements OnInit {

 private bonLivraison: BonLivraison =  new BonLivraison();


private saveEnCours = false;
private saveClientEnCours = false;
private saveFournisseurEnCours = false;

@ViewChild('firstMatExpansionPanel')
 private firstMatExpansionPanel : MatExpansionPanel;

 @ViewChild('intervenantsMatExpansionPanel')
 private intervenantsMatExpansionPanel : MatExpansionPanel;

 @ViewChild('selectArticlesMatExpansionPanel')
 private selectArticlesMatExpansionPanel : MatExpansionPanel;

 @ViewChild('listLignesMatExpansionPanel')
 private listLignesMatExpansionPanel : MatExpansionPanel;

 @ViewChild('paiementDataMatExpansionPanel')
 private paiementDataMatExpansionPanel : MatExpansionPanel;

private clientFilter: ClientFilter;
private fournisseurFilter: FournisseurFilter;

private articleFilter: ArticleFilter;
private listClients: Client[];
private listFournisseurs: Fournisseur[];

private listArticles: Article[];

private sourceListArticles: Article[] = [];
private targetListArticles: Article[] = [];


private etatsBonLivraison: any[] = [
  {value: 'Initial', viewValue: 'Initiale'},
  {value: 'Valide', viewValue: 'Validée'},
  {value: 'Annule', viewValue: 'Annulée'},
  // {value: 'Regle', viewValue: 'Reglée'},
  // {value: 'NonRegle', viewValue: 'Non Reglée'},
 //  {value: 'PartiellementRegle', viewValue: 'Partiellement Reglée'},

];





  constructor( private route: ActivatedRoute,
    private router: Router, private bonLivraisonService: BonLivraisonService,
     private clientService: ClientService,
     private fournisseurService: FournisseurService,
     
    private articleService: ArticleService,
   private matSnackBar: MatSnackBar, private elRef: ElementRef) {
  }


  ngOnInit() {


   this.listClients = this.route.snapshot.data.clients;
  
   this.clientFilter = new ClientFilter(this.listClients);

    this.fournisseurFilter = new FournisseurFilter([]);
    
   this.firstMatExpansionPanel.open();
   this.selectArticlesMatExpansionPanel.opened.subscribe(openedPanel => {
     if(this.sourceListArticles.length === 0  && 
      this.targetListArticles.length === 0 ) {
    this.articleService.getArticles().subscribe((listArticles) => {
          this.sourceListArticles = listArticles;
         // this.targetListArticles = listArticles;
    });
  }
  });
  this.listLignesMatExpansionPanel.opened.subscribe(openedPanel => {
  

  });

  this.intervenantsMatExpansionPanel.opened.subscribe(openedPanel => {
  
  this.bonLivraison.etat = 'Initial';
  });
}
existeArticleInListLignes(article)
{
 for (let i = 0 ; i < this.bonLivraison.lignesBonLivraison.length ; i++) {
        if(this.bonLivraison.lignesBonLivraison[i].codeArticle === article.code
        && this.bonLivraison.lignesBonLivraison[i].libelleArticle === article.libelle)
        {
             return true;
        }
 }
 return false;
}

existeArticleInTarget(article)
{
 for (let i = 0 ; i < this.targetListArticles.length ; i++) {
        if(this.targetListArticles[i].code === article.code
        && this.targetListArticles[i].libelle === article.libelle)
        {
             return true;
        }
 }
 return false;
}
  generateLignesBonLivraison()
  {
    if (this.bonLivraison.lignesBonLivraison === undefined) {
    this.bonLivraison.lignesBonLivraison = [];
    }
    this.targetListArticles.forEach((article, index) => {
      if (! this.existeArticleInListLignes(article))
      {
             const ligne = new LigneBonLivraison();
                ligne.codeArticle = article.code;
                ligne.libelleArticle = article.libelle;
                ligne.quantite  = 1;
               // ligne.prixVentesFixe = article.prixVentes;
                ligne.prixTotal = article.prixVentes;
                
         this.bonLivraison.lignesBonLivraison[index] = ligne;
      }
    }, this);
  }
  removeLigneBonLivraison() {
    if (this.bonLivraison.lignesBonLivraison === undefined) {
    this.bonLivraison.lignesBonLivraison = [];
    }
    const listLignes = this.bonLivraison.lignesBonLivraison;
    listLignes.forEach((ligneOP, index) => {
      if (! this.existeArticleInTarget({code: ligneOP.codeArticle, libelle: ligneOP.libelleArticle})) {
        this.bonLivraison.lignesBonLivraison =
        listLignes.filter((ligne) => (ligne.codeArticle !== ligneOP.codeArticle &&
          ligne.libelleArticle !== ligneOP.libelleArticle ));

      }
    }, this);
  }
  removeAllLignesBonsLivraisons()
  {
    this.bonLivraison.lignesBonLivraison = [];
  }
  calculPrixTotal(i) {
    this.bonLivraison.lignesBonLivraison[i].prixTotal =  this.bonLivraison.lignesBonLivraison[i].prixTotal *
    this.bonLivraison.lignesBonLivraison[i].quantite;
  }
  saveBonLivraison() {

 
      if (this.bonLivraison.etat === '' || this.bonLivraison.etat === undefined) {
        this.bonLivraison.etat = 'Initial';
      }

    this.saveEnCours = true;
    
    this.bonLivraisonService.addBonLivraison(this.bonLivraison).subscribe((response) => {
      this.saveEnCours = false;
      
        if (response.error_message ===  undefined) {
          console.log('undefined error message');
          // this.save.emit(response);
          this.bonLivraison = new  BonLivraison();
          this.bonLivraison.lignesBonLivraison = [];
          this.openSnackBar(  'Opération Enregistrée');
        } else {
         // show error message
         this.openSnackBar(  'Erreurs:Opération Echouée');
         
        }
    });
  }
  fillDataNomPrenomClient()
  {
    const listFiltred  = this.listClients.filter((client) => (client.cin === this.bonLivraison.client.cin));
    this.bonLivraison.client.nom = listFiltred[0].nom;
    this.bonLivraison.client.prenom = listFiltred[0].prenom;
  }

  fillDataCINPrenomClient()
  {
    const listFiltred  = this.listClients.filter((client) => (client.nom === this.bonLivraison.client.nom));
    this.bonLivraison.client.cin = listFiltred[0].cin;
    this.bonLivraison.client.prenom = listFiltred[0].prenom;
  }
  fillDataCINNomClient()
  {
    const listFiltred  = this.listClients.filter((client) => (client.prenom === this.bonLivraison.client.prenom));
    const num =   Math.floor(Math.random() * Math.floor(listFiltred.length));
    this.bonLivraison.client.cin = listFiltred[num].cin;
    this.bonLivraison.client.nom= listFiltred[num].nom;
  }
  fillDataMatriculeRaisonClient() {
    const listFiltred  = this.listClients.filter((client) => (client.registreCommerce === this.bonLivraison.client.registreCommerce));
    this.bonLivraison.client.matriculeFiscale = listFiltred[0].matriculeFiscale;
    this.bonLivraison.client.raisonSociale = listFiltred[0].raisonSociale;
  }
  fillDataMatriculeRegistreCommercial()
  {
    const listFiltred  = this.listClients.filter((client) => (client.raisonSociale === this.bonLivraison.client.raisonSociale));
    this.bonLivraison.client.matriculeFiscale = listFiltred[0].matriculeFiscale;
    this.bonLivraison.client.registreCommerce = listFiltred[0].registreCommerce;
  }
  fillDataRegistreCommercialRaison()
  {
    const listFiltred  = this.listClients.filter((client) => (client.matriculeFiscale === this.bonLivraison.client.matriculeFiscale));
    this.bonLivraison.client.raisonSociale = listFiltred[0].raisonSociale;
    this.bonLivraison.client.registreCommerce = listFiltred[0].registreCommerce;
  }



  vider() {
    this.bonLivraison =  new BonLivraison();
  }
  openSnackBar(messageToDisplay) {
   this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
       verticalPosition: 'top', panelClass: 'snackBarClass'});
  }
loadGridBonsLivraisons() {
  this.router.navigate(['/pms/suivie/bonsLivraisons']) ;
}

 
  saveClient() {
    
     // console.log(this.client);
     this.saveClientEnCours = true;
     this.clientService.addClient(this.bonLivraison.client).subscribe((response) => {
       this.saveClientEnCours = false;
         if (response.error_message ===  undefined) {
           // this.save.emit(response);
          // this.client = new Client();
           this.openSnackBar(  'Client Enregistré');
         } else {
          // show error message
          this.openSnackBar(  'Erreurs!:Essayer une autre fois');
          
         }
     });
 }

}
