
import { BaseEntity } from '../common/base-entity.model' ;
// import { Tranche } from './tranche.model' ;
import { Client } from '../ventes/clients/client.model' ;

import { PayementData } from './payement-data.model';
// import { LigneReleveVente } from './ligne-releve-vente.model';
// import { Credit } from '../credits/credit.model';

// type  TypeOperationCourante= 'Achats' |'Ventes' |'' ;

export class  HistoriqueClient extends BaseEntity {
    constructor(
        public code?: string,
        public numeroHistoriqueClient?: string,
        public dateAchatsObject?: Date,
        public dateReglementObject?: Date,
        public montantMisEnValeur?: number,
        public additionalInfos?: string,
        public etat?: string,
        public client?: Client,
        public description?: string
     ) {
         super();
         this.client = new Client();
         this.montantMisEnValeur = Number(0);
         
        //  this.listLignesRelVentes = [] ;
         // this.categoryClient = 'PERSONNE_PHYSIQUE';
    }
}
