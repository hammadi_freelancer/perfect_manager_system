export interface HistoriqueClientElement {
         code: string;
         numeroHistoriqueClient: string;
         dateAchats: string;
         dateReglement: string;
         montantMisEnValeur: number;
         cinClient: string;
         nomClient: string;
          prenomClient: string;
         raisonSocialeClient: string;
         additionalInfos : string;
         etat: string;
         description: string;
        }
