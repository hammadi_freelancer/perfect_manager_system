

export class HistoriqueClientFilter {
    code: string;
    numeroHistoriqueClient: string;
    dateAchats: string;
    dateReglement: string;
    montantMisEnValeur: number;
    cinClient: string;
    nomClient: string;
     prenomClient: string;
    raisonSocialeClient: string;
    additionInfos : string;
    etat: string;
    description: string;
   }
