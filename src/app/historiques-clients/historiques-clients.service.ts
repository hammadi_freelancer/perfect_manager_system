import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import {HistoriqueClient} from './historique-client.model' ;
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_HISTORIQUES_CLIENTS = 'http://localhost:8082/historiquesClients/getHistoriquesClients';
const  URL_GET_PAGE_HISTORIQUES_CLIENTS= 'http://localhost:8082/historiquesClients/getPageHistoriquesClients';

const URL_GET_HISTORIQUE_CLIENT = 'http://localhost:8082/historiquesClients/getHistoriqueClient';
const  URL_ADD_HISTORIQUE_CLIENT = 'http://localhost:8082/historiquesClients/addHistoriqueClient';
const  URL_UPDATE_HISTORIQUE_CLIENT  = 'http://localhost:8082/historiquesClients/updateHistoriqueClient';
const  URL_DELETE_HISTORIQUE_CLIENT= 'http://localhost:8082/historiquesClients/deleteHistoriqueClient';
const  URL_GET_TOTAL_HISTORIQUES_CLIENTS = 'http://localhost:8082/historiquesClients/getTotalHistoriquesClients';



@Injectable({
  providedIn: 'root'
})
export class HistoriquesClientsService {

  constructor(private httpClient: HttpClient) {

   }
   getTotalHistoriquesClients(filter): Observable<any> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('filter', filterStr);
        return this.httpClient
        .get<any>(URL_GET_TOTAL_HISTORIQUES_CLIENTS, {params});
   }
   getPageHistoriquesClients(pageNumber, nbElementsPerPage, filter): Observable<HistoriqueClient[]> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('pageNumber', pageNumber)
    .set('nbElementsPerPage', nbElementsPerPage)
    .set('filter', filterStr);
    return this.httpClient
    .get<HistoriqueClient[]>(URL_GET_PAGE_HISTORIQUES_CLIENTS, {params});
   }
   getHistoriquesClients(): Observable<HistoriqueClient[]> {
   
    return this.httpClient
    .get<HistoriqueClient[]>(URL_GET_LIST_HISTORIQUES_CLIENTS);
   }
   addHistoriqueClient(historiqueClient: HistoriqueClient): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_HISTORIQUE_CLIENT, historiqueClient);
  }
  updateHistoriqueClient(historiqueClient: HistoriqueClient): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_HISTORIQUE_CLIENT, historiqueClient);
  }
  deleteHistoriqueClient(codeHistoriqueClient): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_HISTORIQUE_CLIENT + '/' + codeHistoriqueClient);
  }
  getHistoriqueClient(codeHistoriqueClient): Observable<HistoriqueClient> {
    return this.httpClient.get<HistoriqueClient>(URL_GET_HISTORIQUE_CLIENT + '/' + codeHistoriqueClient);
  }
}
