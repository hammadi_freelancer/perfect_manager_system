import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,
  
    ViewChild } from '@angular/core';
  // import { ArticleService } from './article.service';
  import {HistoriqueClient} from './historique-client.model';
  // import { ParametresVentes } from '../parametres-ventes.model';
  
  import {HistoriquesClientsService} from './historiques-clients.service';
  
  // import {VentesService} from '../ventes.service';
  import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
  
  import {FormControl} from '@angular/forms';
  import {map, startWith} from 'rxjs/operators';
  import {Observable} from 'rxjs';
  import { Router, ActivatedRoute, ParamMap } from '@angular/router';
  // import {ActionData} from './action-data.interface';
  import { ClientService } from '../ventes/clients/client.service';
  
  import { ClientFilter } from '../ventes/clients/client.filter';
  import { Client } from '../ventes/clients/client.model';
  
  
  
  
  import { ColumnDefinition } from '../common/column-definition.interface';
  
  import {MatSnackBar} from '@angular/material';
  import { MatExpansionPanel } from '@angular/material';
  import {ModePayementLibelle} from './mode-payement-libelle.interface';
  import {SelectionModel} from '@angular/cdk/collections';
  
  type LibelleModePayement = 'Credit'| 'Comptant' | 'Cheque' ;
  
  @Component({
    selector: 'app-historique-client-edit',
    templateUrl: './historique-client-edit.component.html',
    // styleUrls: ['./players.component.css']
  })
export class HistoriqueClientEditComponent implements OnInit  {

  private historiqueClient: HistoriqueClient =  new HistoriqueClient();
  
  
  private updateEnCours = false;

  
  private clientFilter: ClientFilter;
  
  private listClients: Client[];
  

    constructor( private route: ActivatedRoute,
      private router: Router, private historiqueClientService: HistoriquesClientsService,
     //   private clientService: ClientService,
       private clientService: ClientService,
       
      // private articleService: ArticleService,
     private matSnackBar: MatSnackBar, private elRef: ElementRef) {
    }
  ngOnInit() {

    const codeHistoriqueClient = this.route.snapshot.paramMap.get('codeHistoriqueClient');
    
    this.historiqueClientService.getHistoriqueClient(codeHistoriqueClient).subscribe(
      (histClient) =>  {
             this.historiqueClient = histClient;
            this.listClients = this.route.snapshot.data.clients;
            this.clientFilter = new ClientFilter(this.listClients);
      });

     
  }
  loadGridHistoriquesClients() {
    this.router.navigate(['/pms/suivie/historiquesClients']) ;
  }
updateHistoriqueClient() {


  this.updateEnCours = true;
  this.historiqueClientService.updateHistoriqueClient(this.historiqueClient).subscribe((response) => {
    this.updateEnCours = false;
    if (response.error_message ===  undefined) {
      console.log('the response of update is ', response );
      this.openSnackBar( ' Historique mise à jour ');
    } else {
      this.openSnackBar( ' Erreurs!');
    }
}
);
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 

 fillDataMatriculeRaisonClient() {
   const listFiltred  = this.listClients.filter((client) =>
    (client.registreCommerce === this.historiqueClient.client.registreCommerce));
   this.historiqueClient.client.matriculeFiscale = listFiltred[0].matriculeFiscale;
   this.historiqueClient.client.raisonSociale = listFiltred[0].raisonSociale;
 }
 fillDataMatriculeRegistreCommercialF()
 {
   const listFiltred  = this.listClients.filter((client) => (client.raisonSociale === 
     this.historiqueClient.client.raisonSociale));
   this.historiqueClient.client.matriculeFiscale = listFiltred[0].matriculeFiscale;
   this.historiqueClient.client.registreCommerce = listFiltred[0].registreCommerce;
 }
 fillDataRegistreCommercialRaisonF()
 {
   const listFiltred  = this.listClients.filter((client) => 
   (client.matriculeFiscale === this.historiqueClient.client.matriculeFiscale));
   this.historiqueClient.client.raisonSociale = listFiltred[0].raisonSociale;
   this.historiqueClient.client.registreCommerce = listFiltred[0].registreCommerce;
 }
 fillDataNomPrenomClient() {
   const listFiltred  = this.listClients.filter((client) => (client.cin === this.historiqueClient.client.cin));
   this.historiqueClient.client.nom = listFiltred[0].nom;
   this.historiqueClient.client.prenom = listFiltred[0].prenom;
 }
 fillDataCINPrenomClient()
 {
   const listFiltred  = this.listClients.filter((client) => (client.nom === this.historiqueClient.client.nom));
   this.historiqueClient.client.cin = listFiltred[0].cin;
   this.historiqueClient.client.prenom = listFiltred[0].prenom;
 }
 fillDataCINNomClient()
 {
   const listFiltred  = this.listClients.filter((client) => (client.prenom === this.historiqueClient.client.prenom));
   const num =   Math.floor(Math.random() * Math.floor(listFiltred.length));
   this.historiqueClient.client.cin = listFiltred[num].cin;
   this.historiqueClient.client.nom= listFiltred[num].nom;
 }

 



}
