import { Injectable } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
// operationsCourantes components
import { HistoriqueClientNewComponent } from './historique-client-new.component';
import { HistoriqueClientEditComponent } from './historique-client-edit.component';
import { HistoriquesClientsGridComponent } from './historiques-clients-grid.component';
import { HistoriqueClientHomeComponent } from './historique-client-home.component';

import { ClientsResolver } from '../ventes/clients-resolver';

import {ModuleWithProviders  } from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';

 export const historiquesClientsRoute: Routes = [
    {
        path: 'historiquesClients',
        component: HistoriquesClientsGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterHistoriqueClient',
        component: HistoriqueClientNewComponent,
        resolve: {
            clients: ClientsResolver,
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerHistoriqueClient/:codeHistoriqueClient',
        component: HistoriqueClientEditComponent,
        resolve: {
            clients: ClientsResolver,
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
];
// export const ventesRoutingModule: ModuleWithProviders = RouterModule.forRoot(ventesRoute);
// commonRouting.providers = [{ provide: APP_BASE_HREF, useValue: '/common'} ] ;


