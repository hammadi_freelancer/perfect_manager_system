import { Component, OnInit, Output,ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {HistoriqueClient} from './historique-client.model';
import {HistoriqueClientFilter} from './historique-client.filter';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {HistoriquesClientsService} from './historiques-clients.service';
// import { ActionData } from './action-data.interface';
// import { Client } from '../../ventes/clients/client.model';
import { HistoriqueClientElement } from './historique-client.interface';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
// import { DialogAddClientComponent } from '../ventes/clients/dialog-add-client.component';
import {SelectItem} from 'primeng/api';

@Component({
  selector: 'app-historiques-clients-grid',
  templateUrl: './historiques-clients-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class HistoriquesClientsGridComponent implements OnInit {

  private historiqueClient: HistoriqueClient =  new HistoriqueClient();
  private selection = new SelectionModel<HistoriqueClientElement>(true, []);
  private historiqueClientFilter: HistoriqueClientFilter = new HistoriqueClientFilter();
  

  private columnsToSelect : SelectItem[];
   private  selectedColumnsDefinitions: string[];
  private selectedColumnsNames: string[];
  
@Output()
select: EventEmitter<HistoriqueClient[]> = new EventEmitter<HistoriqueClient[]>();


 selectedHistoriqueClient: HistoriqueClient ;
 private   showFilter = false;
 // private   showFilterClient = false;
  private   showFilterClient = false;
 
 private showSelectColumnsMultiSelect = false;
 




 @Input()
 private listHistoriquesClients: any = [] ;
 private checkedAll: false;

 private  dataHistoriquesClientsSource: MatTableDataSource<HistoriqueClientElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay :ColumnDefinition[] = [];
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
  constructor( private route: ActivatedRoute, private historiquesClientsService: HistoriquesClientsService,
    private router: Router, private matSnackBar: MatSnackBar,
  private  dialog: MatDialog ) {
  }
  ngOnInit() {
    this.columnsToSelect = [
      {label: 'N° Historique', value: 'numeroHistoriqueClient', title: 'N° Historique'},
      {label: 'Date Achats', value: 'dateAchats', title: 'Date Achats'},
      {label: 'Date Règlement', value: 'dateReglement', title: 'Date Règlement'},
      {label: 'Montant Mis En Valeur', value: 'montantMisEnValeur', title: 'Montant Mis En Valeur'},
      {label: 'Infos Complémentaires', value: 'additionalInfos', title: 'Infos Complémentaires'},
      {label: 'Rs.Soc.Client', value:  'raisonSocialeClient',  title: 'Rs.Soc.Client' },
      {label: 'CIN Client', value:  'cinClient', title: 'CIN Client' },
      {label: 'Nom Client', value:  'nomClient', title: 'Nom Client' },
      {label: 'Prénom Client', value:  'prenomClient', title: 'Prénom Client' },
      {label: 'Déscription', value:  'description', title: 'Déscription' }
  
  ];
  this.selectedColumnsDefinitions = [
    'numeroHistoriqueClient',
   'dateAchats' ,
   'montantMisEnValeur' ,
   'cinClient'
    ];
    this.historiquesClientsService.getPageHistoriquesClients(0, 5, null).
    subscribe((regCredits) => {
      this.listHistoriquesClients = regCredits;
  
      this.defaultColumnsDefinitions = [
        {name: 'numeroHistoriqueClient' , displayTitle: 'N° Historique'},
        {name: 'dateAchats' , displayTitle: 'Date Achats'},
         {name: 'dateReglement' , displayTitle: 'Date Règlement'},
         {name: 'montantMisEnValeur' , displayTitle: 'Montant Mis en Valeur'},
        {name: 'cinClient' , displayTitle: 'CIN Client'},
        {name: 'nomClient' , displayTitle: 'Nom Client'},
        {name: 'prenomClient' , displayTitle: 'Prénom Client'},
        {name: 'raisonSocialeClient' , displayTitle: 'Raison Soc.'},
         {name: 'additionalInfos' , displayTitle: 'Infos Complémentaires'},
         {name: 'description' , displayTitle: 'Déscription'}
         
        ];
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });
  }
  deleteHistoriqueClient(historiqueClient) {
     //  console.log('call delete !', operationCourante );
    this.historiquesClientsService.deleteHistoriqueClient(historiqueClient.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData(null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.historiquesClientsService.getPageHistoriquesClients(0, 5, filter).subscribe((historiquesClients) => {
    this.listHistoriquesClients= historiquesClients;
    this.transformDataToByConsumedByMatTable();
   });
}


checkOneActionInvoked() {
  const listSelectedHistoriquesClients: HistoriqueClient[] = [];

if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((regElement) => {
  return regElement.code;
});
this.listHistoriquesClients.forEach(RG => {
  if (codes.findIndex(code => code === RG.code) > -1) {
    listSelectedHistoriquesClients.push(RG);
  }
  });
}
this.select.emit(listSelectedHistoriquesClients);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listHistoriquesClients !== undefined) {
    this.listHistoriquesClients.forEach(histClient => {
      listElements.push( {
        code: histClient.code ,
        numeroHistoriqueClient: histClient.numeroHistoriqueClient,
        cinClient: histClient.client.cin,
        nomClient: histClient.client.nom ,
        prenomClient: histClient.client.prenom ,
        raisonSocialeClient: histClient.client.raisonSociale ,
       montantMisEnValeur: histClient.montantMisEnValeur ,
       dateAchats: histClient.dateAchats ,
       dateReglement: histClient.dateReglement ,
       additionalInfos: histClient.additionalInfos ,
    
       description: histClient.description
       
      } );
    });
  }
    this.dataHistoriquesClientsSource = new MatTableDataSource<HistoriqueClientElement>(listElements);
    this.dataHistoriquesClientsSource.sort = this.sort;
    this.dataHistoriquesClientsSource.paginator = this.paginator;
    this.historiquesClientsService.getTotalHistoriquesClients(this.historiqueClientFilter).subscribe((response) => {
    //   console.log('Total Releves de Ventes  is ', response);
    if (this.dataHistoriquesClientsSource.paginator) {
       this.dataHistoriquesClientsSource.paginator.length = response['totalHistoriquesClients'];
  
        }    });
}

specifyListColumnsToBeDisplayed() {
  
   console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
   this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
 ?
    this.defaultColumnsDefinitions : [];
    this.selectedColumnsNames = [];
    
    if (this.selectedColumnsDefinitions !== undefined)
     {
       this.selectedColumnsDefinitions.forEach((colName) => { 
            const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
            const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
            const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
            this.columnsDefinitionsToDisplay.push(colDef);
           });
     }
    this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
   this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
              return colDef.displayTitle;
    });
    console.log('columns to disp:', this.columnsDefinitionsToDisplay);
    this.columnsNames = [];
    this.columnsNames[0] = 'select';
    this.columnsDefinitionsToDisplay .map((colDef) => {
     this.columnsNames.push(colDef.name);
    if (this.selectedColumnsDefinitions !== undefined) {
     this.selectedColumnsNames.push(colDef.displayTitle);
    }
 });
    this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
     return colDef.displayTitleAr;
 });
 }

masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataHistoriquesClientsSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataHistoriquesClientsSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataHistoriquesClientsSource.filter = filterValue.trim().toLowerCase();
  if (this.dataHistoriquesClientsSource.paginator) {
    this.dataHistoriquesClientsSource.paginator.firstPage();
  }
}

loadAddHistoriqueClientComponent() {
  this.router.navigate(['/pms/suivie/ajouterHistoriqueClient']) ;

}
loadEditHistoriqueClientComponent() {
  this.router.navigate(['/pms/suivie/editerHistoriqueClient', this.selection.selected[ this.selection.selected.length - 1].code]) ;

}
supprimerHistoriquesClients()
{

  if ( this.selection.selected.length !== 0) {
      this.selection.selected.forEach(function(regCredit, index) {

   this.historiquesClientsService.deleteHistoriqueClient(regCredit.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.openSnackBar( this.selection.selected.length + ' Historique(s) Client(s) Supprimé(s)');
            this.selection.selected = [];
            this.loadData(null);
            }
      }, this);
    } else {
      this.openSnackBar('Aucun Historique Séléctionnée !');
    }
}
refresh() {
  this.loadData(null);
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

 history()
 {
   
 }
 /*addClient()
 {
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
  };

  const dialogRef = this.dialog.open(DialogAddClientComponent,
    dialogConfig
  );
 }*/

 toggleFilterPanel()
 {
    this.showFilter = !this.showFilter;
 }
 toggleShowColumnsMultiSelect()
 {
   this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
 }
 loadFiltredData()
 {
  //  console.log('the filter is ', this.operationCouranteFilter);
   this.loadData(this.historiqueClientFilter);
 }
 attacherDocuments()
 {

 }
 changePage($event) {
  console.log('page event is ', $event);
 this.historiquesClientsService.getPageHistoriquesClients($event.pageIndex, $event.pageSize,
  this.historiqueClientFilter ).subscribe((histsClients) => {
    this.listHistoriquesClients = histsClients;
   const listElements = [];
   this.listHistoriquesClients.forEach(histClient => {
    listElements.push( {
      code: histClient.code ,
      numeroHistoriqueClient: histClient.numeroHistoriqueClient,
      cinClient: histClient.client.cin,
      nomClient: histClient.client.nom ,
      prenomClient: histClient.client.prenom ,
      raisonSocialeClient: histClient.client.raisonSociale ,
     montantMisEnValeur: histClient.montantMisEnValeur ,
     dateAchats: histClient.dateAchats ,
     dateReglement: histClient.dateReglement ,
     additionalInfos: histClient.additionInfos ,
  
     description: histClient.description
      
  });
});
     this.dataHistoriquesClientsSource= new MatTableDataSource<HistoriqueClientElement>(listElements);

     
     this.historiquesClientsService.getTotalHistoriquesClients(this.historiqueClientFilter)
     .subscribe((response) => {
      console.log('Total OPer Courantes  is ', response);
      if (this.dataHistoriquesClientsSource.paginator) {
       this.dataHistoriquesClientsSource.paginator.length = response['totalHistoriquesClients'];
      }
     });

    });
}


activateFilterClient()
{
  this.showFilterClient = !this.showFilterClient ;
  
}

}
