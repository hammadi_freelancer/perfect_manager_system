import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,
  AfterViewInit , AfterContentInit,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {Journal} from './journal.model';

import {JournauxService} from './journaux.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { MatDatepickerInput } from '@angular/material/datepicker';
import { MatSnackBar } from '@angular/material';
import { MatDatepicker } from '@angular/material/datepicker';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-journal-edit',
  templateUrl: './journal-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class JournalEditComponent implements OnInit  {

  private journal: Journal =  new Journal();
  private tabsDisabled = true;
  private updateEnCours = false;
  private etatsJournal: any[] = [
    {value: 'Ouvert', viewValue: 'Ouvert'},
    {value: 'Initial', viewValue: 'Initiale'},
    {value: 'EnCoursValidation', viewValue: 'En Cours de Validation'},
    {value: 'Valide', viewValue: 'Validé'},
    {value: 'Cloture', viewValue: 'Cloturé'},
    {value: 'Annule', viewValue: 'Annulé'}
  ];
  constructor( private route: ActivatedRoute,
    private router: Router, private journauxService: JournauxService, private matSnackBar: MatSnackBar,
    public dialog: MatDialog
   ) {
  }
  ngOnInit() {
    const codeJournal = this.route.snapshot.paramMap.get('codeJournal');
    this.journauxService.getJournal(codeJournal).subscribe((journal) => {
             this.journal = journal;
             console.log('the date is ');
          
             if (this.journal.etat === 'Initial')
              {
                this.tabsDisabled = false;
                this.etatsJournal = [
                  {value: 'Initial', viewValue: 'Initial'},
                  {value: 'Valide', viewValue: 'Validé'},
                  {value: 'Annule', viewValue: 'Annulé'},
                  
                ];
              }
              if (this.journal.etat === 'Cloture')
                {
                  this.tabsDisabled = false;
                  this.etatsJournal = [
                    {value: 'Cloture', viewValue: 'Cloturé'},
                    {value: 'Annule', viewValue: 'Annulé'},
                    
                  ];
                }
            if (this.journal.etat === 'Valide')
              {
                this.tabsDisabled = false;
                this.etatsJournal = [
                  {value: 'Annule', viewValue: 'Annulé'},
                  {value: 'Valide', viewValue: 'Validé'},
                  {value: 'Cloture', viewValue: 'Cloturé'},
                  {value: 'Initial', viewValue: 'Initial'}
                  
                ];
              }
              if (this.journal.etat === 'Annule')
                {
                  this.etatsJournal = [
                    {value: 'Annule', viewValue: 'Annulé'},
                    {value: 'Valide', viewValue: 'Validé'},
                  ];
                  this.tabsDisabled = true;
               
                }
 
    });
  }
 
  loadAddJournalComponent() {
    this.router.navigate(['/pms/suivie/ajouterJournal']) ;
  }
  loadGridJournaux() {
    this.router.navigate(['/pms/suivie/journaux']) ;
  }
  supprimerJournal() {
      this.journauxService.deleteJournal(this.journal.code).subscribe((response) => {

        this.openSnackBar( ' Journal Supprimé');
        this.loadGridJournaux() ;
            });
  }
updateJournal() {
  this.updateEnCours = true;
  this.journauxService.updateJournal(this.journal).subscribe((response) => {
    this.updateEnCours = false;
    if (response.error_message ===  undefined) {

      if (this.journal.etat === 'Initial')
        {
         // this.tabsDisabled = true;
          this.etatsJournal = [
            {value: 'Initial', viewValue: 'Initial'},
            {value: 'Valide', viewValue: 'Validé'},
            {value: 'Annule', viewValue: 'Annulé'},
            
          ];
        }
        if (this.journal.etat === 'Cloture')
          {
            this.tabsDisabled = false;
            this.etatsJournal = [
              {value: 'Cloture', viewValue: 'Cloturé'},
              {value: 'Annule', viewValue: 'Annulé'},
              
            ];
          }
      if (this.journal.etat === 'Valide')
        {
          this.tabsDisabled = false;
          this.etatsJournal = [
            {value: 'Annule', viewValue: 'Annulé'},
            {value: 'Valide', viewValue: 'Validé'},
            {value: 'Initial', viewValue: 'Initiale'},
            {value: 'Cloture', viewValue: 'Cloturé'},
            
          ];
        }
        if (this.journal.etat === 'Annule')
          {
            this.etatsJournal = [
              {value: 'Annule', viewValue: 'Annulé'},
              {value: 'Valide', viewValue: 'Validé'},
            ];
            this.tabsDisabled = false;
         
          }
      this.openSnackBar( ' Journal mis à jour ');
    
    } else {
      this.openSnackBar( ' Erreurs:Opération Echouée ');
    }
});
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

 selectedTabChanged($event) {
  if ($event.index === 4) { // Composition tab was selected
   // this.loadListArticles();
   } else if ($event.index === 1) {
    //  this.loadListSchemasConfiguration() ;
   }
  }
}
