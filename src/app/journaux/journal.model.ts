
import { BaseEntity } from '../common/base-entity.model' ;

type Etat = 'Initial' | 'Valide' | 'Annule' | 'EnCoursValidation' | 'Ouvert' | 'Cloture'

export class Journal extends BaseEntity {
    constructor(
        public code?: string,
        public numeroJournal?: string,
        public dateJournalObject?: Date,
        public dateJournal?: string,
         public montantVentes?: number,
         public montantAchats?: number,
         public montantRH?: number,
         public montantCharges?: number,
         public etat?: Etat,
    // public listTranches?: Tranche[]
     ) {
         super();
      
    }
 
}
