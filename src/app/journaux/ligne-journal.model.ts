
import { BaseEntity } from '../common/base-entity.model' ;

type TypeOperation =   'AchatsMatieresPrimaires' | 'AchatsMarchandises' |
'AchatsFournituresBureau' |'AchatsMaterielles' | 'AutresAchats'

| 'Electricite' |'EauPotable' | 'ConsommationTelephonique'
| 'Transport' |  'Location' | 'Impots' | 'AutresCharges' |

'VentesMarchandises' | 'VentesProduitsFinis'
| 'VentesMatieresPrimaires' |  'VentesProduitsSemiFinis' | 'VentesMateriellesEpuisées'
|'VentesLocaux' | 'VentesProduitsNonConformes' | 'AutresVentes' |

'PaieSalaires' | 'PaieHoraires' | 'PaieCNSS' | 'PaiePrimes' |'PaieAvances' |
'PaieRecompenses' |'PaieRecrutement' | 'AutresPaies'
 ;
 type Etat = 'Initial' | 'Valide' | 'Annule'
 
 type ModePaiement = 'Espece' | 'Cheque' | 'Virement' | 'Credit'
 
export class LigneJournal extends BaseEntity {
    constructor(
        public code?: string,
        public codeJournal?: string,
         public numeroLigneJournal?: string,
         public typeOperation?: TypeOperation,
         public heureOperation?: number,
          public montant?: number,
          public modePaiement?: ModePaiement,
          public etat?: Etat,
     ) {
         super();
        
       }
  
}
