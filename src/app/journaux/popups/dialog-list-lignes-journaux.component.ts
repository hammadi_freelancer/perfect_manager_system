
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {JournauxService} from '../journaux.service';
import {LigneJournal} from '../ligne-journal.model';
import { LigneJournalElement } from '../ligne-journal.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-lignes-journaux',
    templateUrl: 'dialog-list-lignes-journaux.component.html',
  })
  export class DialogListLignesJournauxComponent implements OnInit {
     private listLignesJournaux: LigneJournal[] = [];
     private  dataLignesJournauxSource: MatTableDataSource<LigneJournalElement>;
     private selection = new SelectionModel<LigneJournalElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private journauxService: JournauxService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      this.journauxService.getLignesJournauxByCodeJournal(this.data.codeJournal).subscribe((listLignesJournaux) => {
        this.listLignesJournaux= listLignesJournaux;
// console.log('recieving data from backend', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedLignesJournaux: LigneJournal[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((ligneJlement) => {
    return ligneJlement.code;
  });
  this.listLignesJournaux.forEach(lJour => {
    if (codes.findIndex(code => code === lJour.code) > -1) {
      listSelectedLignesJournaux.push(lJour);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listLignesJournaux !== undefined) {

      this.listLignesJournaux.forEach(lj => {
             listElements.push( {code: lj.code ,
          heureOperation: lj.heureOperation,
          numeroLigneJournal: lj.numeroLigneJournal,
          typeOperation: lj.typeOperation,
          montant: lj.montant,
          description: lj.description,
          etat: lj.etat,
          
        } );
      });
    }
      this.dataLignesJournauxSource = new MatTableDataSource<LigneJournalElement>(listElements);
      this.dataLignesJournauxSource.sort = this.sort;
      this.dataLignesJournauxSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'numeroLigneJournal' , displayTitle: 'N°Ligne'},
     {name: 'montant' , displayTitle: 'Montant(en Dinars)'},
     {name: 'typeOperation' , displayTitle: 'Opération'},
     {name: 'heureOperation' , displayTitle: 'Heure'},
     { name : 'etat' , displayTitle : 'Etat'},
       { name : 'description' , displayTitle : 'Déscription'},
       
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataLignesJournauxSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataLignesJournauxSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataLignesJournauxSource.filter = filterValue.trim().toLowerCase();
    if (this.dataLignesJournauxSource.paginator) {
      this.dataLignesJournauxSource.paginator.firstPage();
    }
  }

}
