import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';
import { HttpParams } from '@angular/common/http';

import {Journal} from './journal.model' ;
// import { TrancheCredit } from './tranche-credit.model';
import { DocumentJournal } from './document-journal.model';
import { LigneJournal } from './ligne-journal.model';

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_JOURNAUX = 'http://localhost:8082/journaux/getJournaux';
const URL_GET_JOURNAL = 'http://localhost:8082/journaux/getJournal';
const  URL_ADD_JOURNAL = 'http://localhost:8082/journaux/addJournal';
const  URL_UPDATE_JOURNAL = 'http://localhost:8082/journaux/updateJournal';
const  URL_DELETE_JOURNAL = 'http://localhost:8082/journaux/deleteJournal';
const  URL_GET_TOTAL_JOURNAUX = 'http://localhost:8082/journaux/getTotalJournaux';



  const  URL_ADD_DOCUMENT_JOURNAL = 'http://localhost:8082/journaux/documents/addDocumentJournal';
  const  URL_GET_LIST_DOCUMENT_JOURNAL = 'http://localhost:8082/journaux/documents/getDocumentsJournaux';
  const  URL_DELETE_DOCUCMENT_JOURNAL = 'http://localhost:8082/journaux/documents/deleteDocumentJournal';
  const  URL_UPDATE_DOCUMENT_JOURNAL = 'http://localhost:8082/journaux/documents/updateDocumentJournal';
  const URL_GET_DOCUMENT_JOURNAL = 'http://localhost:8082/journaux/documents/getDocumentJournal';
  const URL_GET_LIST_DOCUMENT_JOURNAL_BY_CODE_JOURNAL =
   'http://localhost:8082/journaux/documents/getDocumentsJournauxByCodeJournal';


   const  URL_ADD_LIGNE_JOURNAL = 'http://localhost:8082/journaux/lignes/addLigneJournal';
   const  URL_GET_LIST_LIGNE_JOURNAL = 'http://localhost:8082/journaux/lignes/getLignesJournaux';
   const  URL_DELETE_LIGNE_JOURNAL = 'http://localhost:8082/journaux/lignes/deleteLigneJournal';
   const  URL_UPDATE_LIGNE_JOURNAL = 'http://localhost:8082/journaux/lignes/updateLigneJournal';
   const URL_GET_LIGNE_JOURNAL = 'http://localhost:8082/journaux/lignes/getLigneJournal';
   const URL_GET_LIST_LIGNE_JOURNAL_BY_CODE_JOURNAL =
    'http://localhost:8082/journaux/lignes/getLignesJournauxByCodeJournal';

@Injectable({
  providedIn: 'root'
})
export class JournauxService {

  constructor(private httpClient: HttpClient) {

   }

   getTotalJournaux(filter): Observable<any> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('filter', filterStr);
        return this.httpClient
        .get<any>(URL_GET_TOTAL_JOURNAUX, {params});
       }
   getJournaux(pageNumber, nbElementsPerPage, filter): Observable<Journal[]> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('pageNumber', pageNumber)
    .set('nbElementsPerPage', nbElementsPerPage)
    .set('sort', 'name')
    .set('filter', filterStr);
    return this.httpClient
    .get<Journal[]>(URL_GET_LIST_JOURNAUX, {params});
   }
   addJournal(journal: Journal): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_JOURNAL, journal);
  }
  updateJournal(journal: Journal): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_JOURNAL, journal);
  }
  deleteJournal(codeJournal): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_JOURNAL + '/' + codeJournal);
  }
  getJournal(codeJournal): Observable<Journal> {
    return this.httpClient.get<Journal>(URL_GET_JOURNAL + '/' + codeJournal);
  }


  addDocumentJournal(documentJournal: DocumentJournal): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_DOCUMENT_JOURNAL, documentJournal);
    
  }
  updateDocumentJournal(docJournal: DocumentJournal): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_DOCUMENT_JOURNAL, docJournal);
  }
  deleteDocumentJournal(codeDocumentJournal): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_DOCUCMENT_JOURNAL + '/' + codeDocumentJournal);
  }
  getDocumentJournal(codeDocumentJournal): Observable<DocumentJournal> {
    return this.httpClient.get<DocumentJournal>(URL_GET_DOCUMENT_JOURNAL + '/' + codeDocumentJournal);
  }
  getDocumentsJournaux(): Observable<DocumentJournal[]> {
    return this.httpClient
    .get<DocumentJournal[]>(URL_GET_LIST_DOCUMENT_JOURNAL);
   }
  getDocumentsJournauxByCodeJournal(codeDocumentJournal): Observable<DocumentJournal[]> {
    return this.httpClient.get<DocumentJournal[]>(URL_GET_LIST_DOCUMENT_JOURNAL_BY_CODE_JOURNAL + '/' + codeDocumentJournal);
  }

// Lignes journaux


addLigneJournal(ligneJournal: LigneJournal): Observable<any> {
  return this.httpClient.post<any>(URL_ADD_LIGNE_JOURNAL, ligneJournal);
  
}
updateLigneJournal(ligneJournal: LigneJournal): Observable<any> {
  return this.httpClient.put<any>(URL_UPDATE_LIGNE_JOURNAL, ligneJournal);
}
deleteLigneJournal(codeLigneJournal): Observable<any> {
  return this.httpClient.delete<any>(URL_DELETE_LIGNE_JOURNAL + '/' + codeLigneJournal);
}
getLigneJournal(codeLigneJournal): Observable<LigneJournal> {
  return this.httpClient.get<LigneJournal>(URL_GET_LIGNE_JOURNAL + '/' + codeLigneJournal);
}
getLignesJournaux(): Observable<LigneJournal[]> {
  return this.httpClient
  .get<LigneJournal[]>(URL_GET_LIST_LIGNE_JOURNAL_BY_CODE_JOURNAL);
 }
getLignesJournauxByCodeJournal(codeLigneJournal): Observable<LigneJournal[]> {
  return this.httpClient.get<LigneJournal[]>(URL_GET_LIST_LIGNE_JOURNAL_BY_CODE_JOURNAL+ '/' + codeLigneJournal);
}





}
