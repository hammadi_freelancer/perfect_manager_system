
import { Component, OnInit, Inject,Input, ViewChild, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {JournauxService} from './journaux.service';
import {Journal} from './journal.model';

import {DocumentJournal} from './document-journal.model';
import { DocumentJournalElement } from './document-journal.interface';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogAddDocumentJournalComponent } from './popups/dialog-add-document-journal.component';
import { DialogEditDocumentJournalComponent } from './popups/dialog-edit-document-journal.component';
@Component({
    selector: 'app-documents-journaux-grid',
    templateUrl: 'documents-journaux-grid.component.html',
  })
  export class DocumentsJournauxGridComponent implements OnInit, OnChanges {
     private listDocumentsJournaux: DocumentJournal[] = [];
     private  dataDocumentsJournauxSource: MatTableDataSource<DocumentJournalElement>;
     private selection = new SelectionModel<DocumentJournalElement>(true, []);
    
     @Input()
     private journal : Journal

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private journauxService: JournauxService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.journauxService.getDocumentsJournauxByCodeJournal(this.journal.code).subscribe((listDocumentsJournaux) => {
        this.listDocumentsJournaux = listDocumentsJournaux;
  // console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
  }
  ngOnChanges()
  {
    this.specifyListColumnsToBeDisplayed();
    
    this.journauxService.getDocumentsJournauxByCodeJournal(this.journal.code).subscribe((listDocumentsJournaux) => {
      this.listDocumentsJournaux = listDocumentsJournaux;
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }

  checkOneActionInvoked() {
    const listSelectedDocumentsJournaux: DocumentJournal[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentJelement) => {
    return documentJelement.code;
  });
  this.listDocumentsJournaux.forEach(documentJournal=> {
    if (codes.findIndex(code => code === documentJournal.code) > -1) {
      listSelectedDocumentsJournaux.push(documentJournal);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsJournaux !== undefined) {

      this.listDocumentsJournaux.forEach(docJ => {
             listElements.push( {code: docJ.code ,
          dateReception: docJ.dateReception,
          numeroDocument: docJ.numeroDocument,
          typeDocument: docJ.typeDocument,
          description: docJ.description
        } );
      });
    }
      this.dataDocumentsJournauxSource = new MatTableDataSource<DocumentJournalElement>(listElements);
      this.dataDocumentsJournauxSource.sort = this.sort;
      this.dataDocumentsJournauxSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    //  console.log('calling specifyListColumnsToBeDisplayed documents grid');
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
    
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
      console.log(this.columnsNames);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsJournauxSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsJournauxSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsJournauxSource.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsJournauxSource.paginator) {
      this.dataDocumentsJournauxSource.paginator.firstPage();
    }
  }
  showAddDocumentDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeJournal : this.journal.code
   };
 
   const dialogRef = this.dialog.open(DialogAddDocumentJournalComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(docAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditDocumentDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeDocumentJournal : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditDocumentJournalComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(docAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Document Séléctionnée!');
    }
    }
    supprimerDocuments()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(doc, index) {
              this.journauxService.deleteDocumentJournal(doc.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Document Séléctionnée!');
        }
    }
    refresh() {
      this.journauxService.getDocumentsJournauxByCodeJournal(this.journal.code).subscribe((listDocsJournaux) => {
        this.listDocumentsJournaux = listDocsJournaux;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
