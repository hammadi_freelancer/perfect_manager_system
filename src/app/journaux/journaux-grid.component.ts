import { Component, OnInit, Output,ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {Journal} from './journal.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {JournauxService} from './journaux.service';
import { JournalElement } from './journal.interface';
import { JournalFilter } from './journal.filter';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {SelectItem} from 'primeng/api';

// import { DialogAddDocumentCreditComponent } from './dialog-add-document-credit.component';
@Component({
  selector: 'app-journaux-grid',
  templateUrl: './journaux-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class JournauxGridComponent implements OnInit {

  private journal: Journal =  new Journal();
  private journalFilter: JournalFilter = new JournalFilter();
  
  private selection = new SelectionModel<JournalElement>(true, []);
  private columnsToSelect : SelectItem[];
   private  selectedColumnsDefinitions: string[];
  private selectedColumnsNames: string[];
  
@Output()
select: EventEmitter<Journal[]> = new EventEmitter<Journal[]>();

//private lastClientAdded: Client;

 private selectedJournal: Journal ;
private   showFilter = false;
private showSelectColumnsMultiSelect = false;

 @Input()
 private listJournaux: Journal[] = [] ;
 private deleteEnCours = false;
 private checkedAll: false;
 private  dataJournauxSource: MatTableDataSource<JournalElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay :ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
 private etatsJournal: any[] = [
  {value: 'Ouvert', viewValue: 'Ouvert'},
  {value: 'Initial', viewValue: 'Initiale'},
  {value: 'EnCoursValidation', viewValue: 'En Cours de Validation'},
  {value: 'Valide', viewValue: 'Validé'},
  {value: 'Cloture', viewValue: 'Cloturé'}
];
  constructor( private route: ActivatedRoute, private journauxService: JournauxService,
    private router: Router, private matSnackBar: MatSnackBar ) {
  }
  ngOnInit() {
    this.journauxService.getJournaux(0, 5, null).subscribe((journaux) => {
      this.listJournaux = journaux;
      // this.listClients[this.listClients.length] = this.lastClientAdded;
      // this.listClientsOrgin = this.listClients ;
      this.defaultColumnsDefinitions = [{name: 'numeroJournal' , displayTitle: 'N°Journal'},
      {name: 'dateJournal' , displayTitle: 'Date'},
       {name: 'montantVentes' , displayTitle: 'Valeur Ventes'},
       {name: 'montantAchats' , displayTitle: 'Valeur Achats'},
       {name: 'montantCharges' , displayTitle: 'Valeur Charges'},
       {name: 'montantRH' , displayTitle: 'Valeur R.Humaines'},
       { name : 'etat' , displayTitle : 'Etat'},
       { name : 'description' , displayTitle : 'Déscription'},
   
        ];
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  /*  this.columnsToSelect = [
      {label: 'N°Journal', value: {id: 1, name: 'numeroJournal', displayTitle: 'N°Journal'}},
      {label: 'Date Journal', value: {id: 2, name: 'dateJournal', displayTitle: 'Date Journal'}},
      {label: 'Valeur Ventes', value: {id: 3, name: 'montantVentes', displayTitle: 'Valeur Ventes'}},
      {label: 'Valeur Achats', value: {id: 4, name: 'montantAchats', displayTitle: 'Valeur Achats'}},
      {label: 'Valeur R.Humaines', value: {id: 5, name: 'montantRH', displayTitle: 'Valeur R.Humaines'}},
      {label: 'Valeur Charges', value: {id: 6, name: 'montantCharges', displayTitle: 'Valeur Charges'} },
      {label: 'Déscription', value: {id: 7, name: 'description', displayTitle: 'Déscription'} },
      {label: 'Etat', value: {id: 8, name: 'etat', displayTitle: 'Etat'} },
      

  ];*/

      this.columnsToSelect = [
      {label: 'N°Journal', value: 'numeroJournal', title: 'N°Journal'},
      {label: 'Date Journal', value: 'dateJournal', title: 'Date Journal'},
      {label: 'Valeur Ventes', value: 'montantVentes', title: 'Valeur Ventes'},
      {label: 'Valeur Achats', value: 'montantAchats', title: 'Valeur Achats'},
      {label: 'Valeur R.Humaines', value: 'montantRH', title: 'Valeur R.Humaines'},
      {label: 'Valeur Charges', value:  'montantCharges', title: 'Valeur Charges' },
      {label: 'Déscription', value: 'description', title: 'Déscription' },
      {label: 'Etat', value:  'etat', title: 'Etat' },
      

  ];
  });
   /* this.creditsService.getCredits().subscribe((credits) => {
      console.log('list credits ', credits);
      // this.listCredits = credits;
      this.listCredits =  this.listCredits.map((credit) => {
        if (credit.client === 'undefined' || credit.client === null ) {
           credit.client = new Client();
         }
         return credit;
   });      this.listCreditsOrgin = this.listCredits ;
     });*/
    // this.listClientsOrgin = this.listClients ;

  }
  deleteJournal(journal) {
     //  console.log('call delete !', credit );
    this.journauxService.deleteJournal(journal.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData( null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.journauxService.getJournaux(0, 5 , filter).subscribe((journaux) => {
    this.listJournaux = journaux;
    this.transformDataToByConsumedByMatTable();
   });
  // this.listClientsOrgin = this.listClients ;
}


checkOneActionInvoked() {
  const listSelectedJournaux: Journal[] = [];
console.log('selected clients are ');
console.log(this.selection.selected);
if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((journalElement) => {
  return journalElement.code;
});
this.listJournaux.forEach(journal => {
  if (codes.findIndex(code => code === journal.code) > -1) {
    listSelectedJournaux.push(journal);
  }
  });
}
this.select.emit(listSelectedJournaux);

}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  this.dataJournauxSource = null;
  if (this.listJournaux !== undefined) {
   //  console.log('diffe of undefined');
    this.listJournaux.forEach(journal => {
           listElements.push( {code: journal.code ,
            numeroJournal: journal.numeroJournal,
            dateJournal: journal.dateJournal ,
            montantVentes: journal.montantVentes,
            montantAchats: journal.montantAchats,
            montantRH: journal.montantRH,
            montantCharges: journal.montantCharges,
            description: journal.description,
   etat: journal.etat === 'Valide' ?  'Validé' : journal.etat === 'Annule' ? 'Annulé' :
   journal.etat === 'EnCoursValidation' ?  'En Cours De Validation' :   journal.etat === 'Cloture' ? 'Cloturé' :
   journal.etat === 'Initial' ? 'Initiale' : 'Ouvert'

      } );
    });
  }
    this.dataJournauxSource = new MatTableDataSource<JournalElement>(listElements);
    this.dataJournauxSource.sort = this.sort;
    this.dataJournauxSource.paginator = this.paginator;
    this.journauxService.getTotalJournaux(null).subscribe((response) => {
    //  console.log('Total credits is ', response);
      this.dataJournauxSource.paginator.length = response.totalJournaux;
    });
}
 specifyListColumnsToBeDisplayed() {
 
  console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
  this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
?
   this.defaultColumnsDefinitions : [];
   this.selectedColumnsNames = [];
   
   if (this.selectedColumnsDefinitions !== undefined)
    {
      this.selectedColumnsDefinitions.forEach((colName) => { 
           const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
           const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
           const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
           this.columnsDefinitionsToDisplay.push(colDef);
          });
    }
   this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
  this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
             return colDef.displayTitle;
   });
   this.columnsNames = [];
   this.columnsNames[0] = 'select';
   this.columnsDefinitionsToDisplay .map((colDef) => {
    this.columnsNames.push(colDef.name);
   if (this.selectedColumnsDefinitions !== undefined) {
    this.selectedColumnsNames.push(colDef.displayTitle);
   }
});
   this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
    return colDef.displayTitleAr;
});
}
masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataJournauxSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataJournauxSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataJournauxSource.filter = filterValue.trim().toLowerCase();
  if (this.dataJournauxSource.paginator) {
    this.dataJournauxSource.paginator.firstPage();
  }
}

loadAddJournalComponent() {
  this.router.navigate(['/pms/suivie/ajouterJournal']) ;

}
loadEditJournalComponent() {
  this.router.navigate(['/pms/suivie/editerJournal', this.selection.selected[ this.selection.selected.length - 1].code]) ;

}
supprimerJournaux()
{
 
  if ( this.selection.selected.length !== 0) {
    this.deleteEnCours = true;
      this.selection.selected.forEach(function(journal, index) {
          this.journauxService.deleteJournal(journal.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.deleteEnCours = false;
            
            this.openSnackBar( this.selection.selected.length + ' Element(s) Supprimé(s)');
             this.loadData(null);
            }
      }, this);
    } else {
      this.openSnackBar('Aucun Journal Séléctionné !');
    }
}
refresh() {
  this.loadData(null);
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 
 toggleFilterPanel()
 {
    this.showFilter = !this.showFilter;
 }
 toggleShowColumnsMultiSelect()
 {
   this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
 }
 loadFiltredData()
 {
   console.log('the filter is ',this.journalFilter);
   this.loadData(this.journalFilter);
 }

changePage($event) {
  console.log('page event is ', $event);
 this.journauxService.getJournaux($event.pageIndex, $event.pageSize, this.journalFilter).subscribe((journaux) => {
    this.listJournaux = journaux;
   // this.transformDataToByConsumedByMatTable();
   const listElements = [];
     console.log('diffe of undefined');
     this.listJournaux.forEach(journal => {
      listElements.push( {code: journal.code ,
       numeroJournal: journal.numeroJournal,
       dateJournal: journal.dateJournal ,
       montantVentes: journal.montantVentes,
       montantAchats: journal.montantAchats,
       montantRH: journal.montantRH,
       montantCharges: journal.montantCharges,
       description: journal.description,
etat: journal.etat === 'Valide' ?  'Validé' : journal.etat === 'Annule' ? 'Annulé' :
journal.etat === 'EnCoursValidation' ?  'En Cours De Validation' :   journal.etat === 'Cloture' ? 'Cloturé' :
journal.etat === 'Initial' ? 'Initiale' : 'Ouvert'

 } );

});
this.dataJournauxSource = new MatTableDataSource<JournalElement>(listElements);
this.journauxService.getTotalJournaux(null).subscribe((response) => {
 console.log('Total credits is ', response);
  this.dataJournauxSource.paginator.length = response.totalJournaux;
});
});
}
}
