import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {Journal} from './journal.model';

import {JournauxService} from './journaux.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {MatSnackBar} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';


@Component({
  selector: 'app-journal-new',
  templateUrl: './journal-new.component.html',
  // styleUrls: ['./players.component.css']
})
export class JournalNewComponent implements OnInit {

 private journal: Journal =  new Journal();
 private saveEnCours = false;
private etatsJournal: any[] = [
  {value: 'Ouvert', viewValue: 'Ouvert'},
  {value: 'Initial', viewValue: 'Initiale'},
  {value: 'EnCoursValidation', viewValue: 'En Cours de Validation'},
  {value: 'Valide', viewValue: 'Validé'},
  {value: 'Cloture', viewValue: 'Cloturé'}
];
  constructor( private route: ActivatedRoute,
    private router: Router, private journauxService: JournauxService,
   private matSnackBar: MatSnackBar) {
  }

  ngOnInit() {
       this.journal.etat = 'Ouvert';
  }
  saveJournal() {

   /* if( this.journal.etat === undefined || this.journal.etat === 'Ouvert' ) {
      this.journal.etat = 'Initial';
    }*/
    // this.saveEnCours = true;
    this.saveEnCours = true;
    this.journauxService.addJournal(this.journal).subscribe((response) => {
      this.saveEnCours = false;
      
        if (response.error_message ===  undefined) {
          console.log('undefined error message');
          // this.save.emit(response);
          this.journal =  new Journal();
          this.openSnackBar(  'Journal Enregistré');
        } else {
         // show error message
         this.openSnackBar(  'Erreurs: Opération Echouée');
         
        }
    });
  }


  vider() {
    this.journal = new Journal();
  }
  openSnackBar(messageToDisplay) {
   this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
       verticalPosition: 'top'});
  }
loadGridJournaux() {
  this.router.navigate(['/pms/suivie/journaux']) ;
}

}
