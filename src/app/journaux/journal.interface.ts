export interface JournalElement {
         code: string ;
         numeroJournal: string;
         dateJournal: string ;
         montantVentes: number;
         montantAchats: number;
         montantRH: number;
         montantCharges: number;
         etat: string;
         description: string;
        }
