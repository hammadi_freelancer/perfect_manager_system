
import { Component, OnInit, Inject,Input, ViewChild, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {JournauxService} from './journaux.service';
import {Journal} from './journal.model';

import {LigneJournal} from './ligne-journal.model';
import { LigneJournalElement } from './ligne-journal.interface';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogAddLigneJournalComponent } from './popups/dialog-add-ligne-journal.component';
import { DialogEditLigneJournalComponent } from './popups/dialog-edit-ligne-journal.component';
@Component({
    selector: 'app-lignes-journaux-grid',
    templateUrl: 'lignes-journaux-grid.component.html',
  })
  export class LignesJournauxGridComponent implements OnInit, OnChanges {
     private listLignesJournaux: LigneJournal[] = [];
     private  dataLignesJournauxSource: MatTableDataSource<LigneJournalElement>;
     private selection = new SelectionModel<LigneJournalElement>(true, []);
    
     @Input()
     private journal : Journal

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private journauxService: JournauxService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.journauxService.getLignesJournauxByCodeJournal(this.journal.code).subscribe((listLignesJournaux) => {
        this.listLignesJournaux = listLignesJournaux;
  // console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
  }
  ngOnChanges()
  {
    this.specifyListColumnsToBeDisplayed();
    
    this.journauxService.getLignesJournauxByCodeJournal(this.journal.code).subscribe((listLignesJournaux) => {
      this.listLignesJournaux = listLignesJournaux;
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }

  checkOneActionInvoked() {
    const listSelectedLignesJournaux: LigneJournal[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((ligneJElement) => {
    return ligneJElement.code;
  });
  this.listLignesJournaux.forEach(lj => {
    if (codes.findIndex(code => code === lj.code) > -1) {
      listSelectedLignesJournaux.push(lj);
    }
    });
  }
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listLignesJournaux !== undefined) {

      this.listLignesJournaux.forEach(lj => {
             listElements.push( {code: lj.code ,
          heureOperation: lj.heureOperation,
          numeroLigneJournal: lj.numeroLigneJournal,
          typeOperation: lj.typeOperation,
          montant: lj.montant,
          modePaiement: lj.modePaiement,
          description: lj.description,
          etat: lj.etat,
        } );
      });
    }
      this.dataLignesJournauxSource = new MatTableDataSource<LigneJournalElement>(listElements);
      this.dataLignesJournauxSource.sort = this.sort;
      this.dataLignesJournauxSource.paginator = this.paginator;
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'numeroLigneJournal' , displayTitle: 'N°Ligne'},
     {name: 'montant' , displayTitle: 'Montant(en Dinars)'},
     {name: 'typeOperation' , displayTitle: 'Opération'},
     {name: 'heureOperation' , displayTitle: 'Heure'},
     {name: 'modePaiement' , displayTitle: 'Mode Paiement'},
     { name : 'etat' , displayTitle : 'Etat'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames = [];
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataLignesJournauxSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataLignesJournauxSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataLignesJournauxSource.filter = filterValue.trim().toLowerCase();
    if (this.dataLignesJournauxSource.paginator) {
      this.dataLignesJournauxSource.paginator.firstPage();
    }
  }
  showAddLigneDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeJournal : this.journal.code
   };
 
   const dialogRef = this.dialog.open(DialogAddLigneJournalComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(lAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditLigneDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeLigneJournal : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditLigneJournalComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(lAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucune Ligne Séléctionnée!');
    }
    }
    supprimerLignes()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(lJour, index) {
              this.journauxService.deleteLigneJournal(lJour.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar(' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucune Ligne Séléctionnée!');
        }
    }
    refresh() {
      this.journauxService.getLignesJournauxByCodeJournal(this.journal.code).subscribe((listLJournaux) => {
        this.listLignesJournaux = listLJournaux;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
