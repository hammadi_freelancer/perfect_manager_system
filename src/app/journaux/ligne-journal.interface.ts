


export interface LigneJournalElement {
         code: string ;
         numeroLigneJournal: string ;
         heureOperation: number ;
         typeOperation: string;
         montant: number;
         modePaiement: string;
          description?: string;

}
