export class OperationCouranteFilter {
    numeroOperationCourante: string;
    dateOperationFromObject: Date ;
    dateOperationToObject: Date;
    valeurFinanciere: number;
    recu: boolean;
    livre: boolean;
    type: string;
    cinClient: string;
    nomClient: string;
    prenomClient: string;
    raisonClient : string;
    matriculeClient : string;
    registreClient : string;
    cinFournisseur: string;
    nomFournisseur: string;
    prenomFournisseur: string;
    raisonFournisseur : string;
    matriculeFournisseur : string;
    registreFournisseur : string;
    etat: string;
   }
