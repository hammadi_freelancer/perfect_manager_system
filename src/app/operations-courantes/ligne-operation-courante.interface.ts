export interface LigneOperationCouranteElement {
    
     codeArticle: string;
     libelleArticle: string;
     quantite: number;
     prixTotal: number;
         }
 