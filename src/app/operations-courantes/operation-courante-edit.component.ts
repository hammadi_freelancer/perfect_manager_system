import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,
  
    ViewChild } from '@angular/core';
  // import { ArticleService } from './article.service';
  import {OperationCourante} from './operation-courante.model';
  // import { ParametresVentes } from '../parametres-ventes.model';
  
  import {OperationCouranteService} from './operation-courante.service';
  
  // import {VentesService} from '../ventes.service';
  import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
  
  import {FormControl} from '@angular/forms';
  import {map, startWith} from 'rxjs/operators';
  import {Observable} from 'rxjs';
  import { Router, ActivatedRoute, ParamMap } from '@angular/router';
  // import {ActionData} from './action-data.interface';
  import { ClientService } from '../ventes/clients/client.service';
  import { FournisseurService } from '../achats/fournisseurs/fournisseur.service';
  
  import { ClientFilter } from '../ventes/clients/client.filter';
  import { FournisseurFilter } from '../achats/fournisseurs/fournisseur.filter';
  import { Fournisseur } from '../achats/fournisseurs/fournisseur.model';
  
  import { ArticleFilter } from '../stocks/articles/article.filter';
  
  import {Client} from '../ventes/clients/client.model';
  
  import {LigneOperationCourante} from './ligne-operation-courante.model';
  
  import {LigneOperationCouranteElement} from './ligne-operation-courante.interface';
  import { ColumnDefinition } from '../common/column-definition.interface';
  
  import {MatSnackBar} from '@angular/material';
  import { MatExpansionPanel } from '@angular/material';
  import {ArticleService} from '..//stocks/articles/article.service';
  import {Article} from '../stocks/articles/article.model';
  import {ModePayementLibelle} from './mode-payement-libelle.interface';
  import {SelectionModel} from '@angular/cdk/collections';
  
  type LibelleModePayement = 'Credit'| 'Comptant' | 'Cheque' ;
  
  @Component({
    selector: 'app-operation-courante-edit',
    templateUrl: './operation-courante-edit.component.html',
    // styleUrls: ['./players.component.css']
  })
export class OperationCouranteEditComponent implements OnInit  {

  private operationCourante: OperationCourante =  new OperationCourante();
  
  
  private updateEnCours = false;
  private saveClientEnCours = false;
  private saveFournisseurEnCours = false;


  
  private selectCin: boolean;
  private selectNom: boolean;
  private selectPrenom: boolean;
  private selectMatricule: boolean;
  private selectRegistre: boolean;
  private selectRaison: boolean;
  private addClient: boolean;
  private addEntreprise: boolean;
  
  
  private selectCinF: boolean;
  private selectNomF: boolean;
  private selectPrenomF: boolean;
  private selectMatriculeF: boolean;
  private selectRegistreF: boolean;
  private selectRaisonF: boolean;
  private addFournisseur: boolean;
  private addEntrepriseF: boolean;
  
  
  @ViewChild('firstMatExpansionPanel')
   private firstMatExpansionPanel : MatExpansionPanel;
  
   @ViewChild('intervenantsMatExpansionPanel')
   private intervenantsMatExpansionPanel : MatExpansionPanel;
  
   @ViewChild('selectArticlesMatExpansionPanel')
   private selectArticlesMatExpansionPanel : MatExpansionPanel;
  
   @ViewChild('listLignesMatExpansionPanel')
   private listLignesMatExpansionPanel : MatExpansionPanel;
  
   @ViewChild('paiementDataMatExpansionPanel')
   private paiementDataMatExpansionPanel : MatExpansionPanel;
  
  private clientFilter: ClientFilter;
  private fournisseurFilter: FournisseurFilter;
  
  private articleFilter: ArticleFilter;
  private listClients: Client[];
  private listFournisseurs: Fournisseur[];
  
  private listArticles: Article[];
  
  private sourceListArticles: Article[] = [];
  private targetListArticles: Article[] = [];
  
  private payementModes: any[] = [
    {value: 'Comptant', viewValue: 'Comptant'},
    {value: 'Cheque', viewValue: 'Chèque'},
    {value: 'Credit', viewValue: 'Crédit'},
    {value: 'Avance', viewValue: 'Avance'},
    {value: 'Comptant_Cheque', viewValue: 'Comptant/Chèque'},
    {value: 'Comptant_Avance', viewValue: 'Comptant/Avance'},
    {value: 'Credit_Comptant', viewValue: 'Crédit/Comptant'},
    {value: 'Credit_Cheque', viewValue: 'Crédit/Chèque'},
    {value: 'Credit_Avance', viewValue: 'Crédit/Avance'},
    {value: 'Donnation', viewValue: 'Amicalité'}
  ];
  private etatsOperationCourante: any[] = [
    {value: 'Initial', viewValue: 'Initiale'},
    {value: 'Valide', viewValue: 'Validée'},
    {value: 'Annule', viewValue: 'Annulée'},
    {value: 'Regle', viewValue: 'Reglée'},
    {value: 'NonRegle', viewValue: 'Non Reglée'},
    {value: 'PartiellementRegle', viewValue: 'Partiellement Reglée'},
  
  ];
  private typesOperationCourante: any[] = [
    {value: 'Achats', viewValue: 'Achats'},
    {value: 'Ventes', viewValue: 'Ventes'},
    {value: '', viewValue: ''},
  ];
    constructor( private route: ActivatedRoute,
      private router: Router, private operationCouranteService: OperationCouranteService,
       private clientService: ClientService,
       private fournisseurService: FournisseurService,
       
      private articleService: ArticleService,
     private matSnackBar: MatSnackBar, private elRef: ElementRef) {
    }
  ngOnInit() {
    this.fournisseurFilter = new FournisseurFilter([]);
    this.selectCin = true;
    this.selectCinF = true;
     this.fournisseurService.getFournisseurs().subscribe((listFournisseurs) => {
          this.listFournisseurs = listFournisseurs;
          this.fournisseurFilter = new FournisseurFilter(this.listFournisseurs);
        });
    const codeOperationCourante = this.route.snapshot.paramMap.get('codeOperationCourante');
    
    this.operationCouranteService.getOperationCourante(codeOperationCourante).subscribe(
      (operationCourante) =>  {
  
             this.operationCourante = operationCourante;
         
        console.log('operation courante sent from the server is :', operationCourante);

        if (this.operationCourante.lignesOperationCourante !== undefined ) {
            console.log('list operations courantes', this.operationCourante.lignesOperationCourante);
           //  console.log('list operations courantes');
            
           this.articleService.getArticles().subscribe((listArticles) => {
             this.sourceListArticles = listArticles;
             this.targetListArticles = listArticles;
            this.sourceListArticles =  this.sourceListArticles.filter((article) => {
            return  this.operationCourante.lignesOperationCourante.filter((ligne) =>
              ( ligne.codeArticle === article.code && ligne.libelleArticle === article.libelle ) ).length === 0
              ;
            });

            this.targetListArticles =  this.targetListArticles.filter((article) => {
             return  this.operationCourante.lignesOperationCourante.filter((ligne) =>
               ( ligne.codeArticle === article.code && ligne.libelleArticle === article.libelle ) ).length !== 0
               ;
             });
             console.log('list target ', this.targetListArticles);
             

              });
           }
             if (this.operationCourante.etat === 'Valide' || this.operationCourante.etat === 'Annule')
              {
                this.etatsOperationCourante.splice(0, 1);
              }
              if (this.operationCourante.etat === 'Initial')
                {
                  this.etatsOperationCourante.splice(2, 4);
                }
               // this.transformDataToByConsumedByMatTable();
            });
            this.firstMatExpansionPanel.open();

            this.listClients = this.route.snapshot.data.clients;
        

             this.clientFilter = new ClientFilter(this.listClients);
             this.selectArticlesMatExpansionPanel.opened.subscribe(openedPanel => {
              if(this.sourceListArticles.length === 0  && 
                this.targetListArticles.length === 0 ) {
              this.articleService.getArticles().subscribe((listArticles) => {
                    this.sourceListArticles = listArticles;
                   // this.targetListArticles = listArticles;
              });
            }
            });
        
            this.intervenantsMatExpansionPanel.opened.subscribe(openedPanel => {
           
            });
  }
  loadGridOperationsCourantes() {
    this.router.navigate(['/pms/suivie/operationsCourantes']) ;
  }
updateOperationCourante() {


  this.updateEnCours = true;
  this.operationCouranteService.updateOperationCourante(this.operationCourante).subscribe((response) => {
    this.updateEnCours = false;
    if (response.error_message ===  undefined) {
      console.log('the response of update is ', response );
      if (this.operationCourante.etat === 'Valide') {
          this.etatsOperationCourante = [
            {value: 'Annule', viewValue: 'Annulé'},
            {value: 'Valide', viewValue: 'Validé'},
            {value: 'Reglee', viewValue: 'Réglée'},
            {value: 'NonReglee', viewValue: 'Non Réglée'},
            {value: 'PartiellementRegle', viewValue: 'Partiellement Réglée'},
            
  
          ];
        }
    

      this.openSnackBar( ' Opération mise à jour ');
    } else {
      this.openSnackBar( ' Erreurs!');
    }
}); 
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 
 fillDataNomPrenomClient()
 {
   const listFiltred  = this.listClients.filter((client) => (client.cin === this.operationCourante.client.cin));
   this.operationCourante.client.nom = listFiltred[0].nom;
   this.operationCourante.client.prenom = listFiltred[0].prenom;
 }

 fillDataCINPrenomClient()
 {
   const listFiltred  = this.listClients.filter((client) => (client.nom === this.operationCourante.client.nom));
   this.operationCourante.client.cin = listFiltred[0].cin;
   this.operationCourante.client.prenom = listFiltred[0].prenom;
 }
 fillDataCINNomClient()
 {
   const listFiltred  = this.listClients.filter((client) => (client.prenom === this.operationCourante.client.prenom));
   const num =   Math.floor(Math.random() * Math.floor(listFiltred.length));
   this.operationCourante.client.cin = listFiltred[num].cin;
   this.operationCourante.client.nom= listFiltred[num].nom;
 }
 fillDataMatriculeRaisonClient() {
   const listFiltred  = this.listClients.filter((client) => (client.registreCommerce === this.operationCourante.client.registreCommerce));
   this.operationCourante.client.matriculeFiscale = listFiltred[0].matriculeFiscale;
   this.operationCourante.client.raisonSociale = listFiltred[0].raisonSociale;
 }
 fillDataMatriculeRegistreCommercial()
 {
   const listFiltred  = this.listClients.filter((client) => (client.raisonSociale === this.operationCourante.client.raisonSociale));
   this.operationCourante.client.matriculeFiscale = listFiltred[0].matriculeFiscale;
   this.operationCourante.client.registreCommerce = listFiltred[0].registreCommerce;
 }
 fillDataRegistreCommercialRaison()
 {
   const listFiltred  = this.listClients.filter((client) => (client.matriculeFiscale === this.operationCourante.client.matriculeFiscale));
   this.operationCourante.client.raisonSociale = listFiltred[0].raisonSociale;
   this.operationCourante.client.registreCommerce = listFiltred[0].registreCommerce;
 }

 fillDataMatriculeRaisonFournisseur() {
   const listFiltred  = this.listFournisseurs.filter((fournisseur) =>
    (fournisseur.registreCommerce === this.operationCourante.fournisseur.registreCommerce));
   this.operationCourante.fournisseur.matriculeFiscale = listFiltred[0].matriculeFiscale;
   this.operationCourante.fournisseur.raisonSociale = listFiltred[0].raisonSociale;
 }
 fillDataMatriculeRegistreCommercialF()
 {
   const listFiltred  = this.listFournisseurs.filter((fournisseur) => (fournisseur.raisonSociale === 
     this.operationCourante.fournisseur.raisonSociale));
   this.operationCourante.fournisseur.matriculeFiscale = listFiltred[0].matriculeFiscale;
   this.operationCourante.fournisseur.registreCommerce = listFiltred[0].registreCommerce;
 }
 fillDataRegistreCommercialRaisonF()
 {
   const listFiltred  = this.listFournisseurs.filter((fournisseur) => 
   (fournisseur.matriculeFiscale === this.operationCourante.fournisseur.matriculeFiscale));
   this.operationCourante.fournisseur.raisonSociale = listFiltred[0].raisonSociale;
   this.operationCourante.fournisseur.registreCommerce = listFiltred[0].registreCommerce;
 }
 fillDataNomPrenomFournisseur() {
   const listFiltred  = this.listFournisseurs.filter((fournisseur) => (fournisseur.cin === this.operationCourante.fournisseur.cin));
   this.operationCourante.fournisseur.nom = listFiltred[0].nom;
   this.operationCourante.fournisseur.prenom = listFiltred[0].prenom;
 }
 fillDataCINPrenomFournisseur()
 {
   const listFiltred  = this.listFournisseurs.filter((fournisseur) => (fournisseur.nom === this.operationCourante.fournisseur.nom));
   this.operationCourante.fournisseur.cin = listFiltred[0].cin;
   this.operationCourante.fournisseur.prenom = listFiltred[0].prenom;
 }
 fillDataCINNomFournisseur()
 {
   const listFiltred  = this.listFournisseurs.filter((fournisseur) => (fournisseur.prenom === this.operationCourante.fournisseur.prenom));
   const num =   Math.floor(Math.random() * Math.floor(listFiltred.length));
   this.operationCourante.fournisseur.cin = listFiltred[num].cin;
   this.operationCourante.fournisseur.nom= listFiltred[num].nom;
 }

 activateSelectNom() {
  if(this.operationCourante.type === 'Ventes') {
    
   this.selectCin = false;
   this.selectNom = true;
   this.selectPrenom = false;
   this.selectRegistre = false;
   this.selectMatricule = false;
   this.selectRaison = false;
   this.addClient = false;
   this.addEntreprise = false;
  }
   if(this.operationCourante.type === 'Achats') {
    this.selectCinF = false;
    this.selectNomF = true;
    this.selectPrenomF = false;
    this.selectRegistreF = false;
    this.selectMatriculeF = false;
    this.selectRaisonF = false;
    this.addFournisseur = false;
    this.addEntrepriseF = false;
   }
    
   
 }
 activateSelectPrenom() {
  if (this.operationCourante.type === 'Ventes') {
     this.selectCin = false;
  this.selectNom = false;
  this.selectPrenom = true;
  this.selectRegistre = false;
  this.selectMatricule = false;
  this.selectRaison = false;
  this.addClient = false;
  this.addEntreprise = false;
  }
  if (this.operationCourante.type === 'Achats') {

    this.selectCinF = false;
    this.selectNomF = false;
    this.selectPrenomF = true;
    this.selectRegistreF = false;
    this.selectMatriculeF = false;
    this.selectRaisonF = false;
    this.addFournisseur = false;
    this.addEntrepriseF = false;

  }      
}
activateSelectCIN() {
  if(this.operationCourante.type === 'Ventes') {
    
  this.selectCin = true;
  this.selectNom = false;
  this.selectPrenom = false;
  this.selectRegistre = false;
  this.selectMatricule = false;
  this.selectRaison = false;
  this.addClient = false;
  this.addEntreprise = false;
  }
  if (this.operationCourante.type === 'Achats') {
    this.selectCinF = true;
    this.selectNomF = false;
    this.selectPrenomF = false;
    this.selectRegistreF = false;
    this.selectMatriculeF = false;
    this.selectRaisonF = false;
    this.addFournisseur= false;
    this.addEntrepriseF = false;
  }
}
activateSelectRegistre() {
  if(this.operationCourante.type === 'Ventes') {
    
  this.selectCin = false;
  this.selectNom = false;
  this.selectPrenom = false;
  this.selectRegistre = true;
  this.selectMatricule = false;
  this.selectRaison = false;
  this.addClient = false;
  this.addEntreprise = false;
  }
  if(this.operationCourante.type === 'Achats') {
    this.selectCinF = false;
    this.selectNomF = false;
    this.selectPrenomF = false;
    this.selectRegistreF = true;
    this.selectMatriculeF = false;
    this.selectRaisonF = false;
    this.addFournisseur = false;
    this.addEntrepriseF = false;
  }      
}
activateSelectMatricule() {
  console.log('activate select matricule ');
  if(this.operationCourante.type === 'Ventes') {
    
  this.selectCin = false;
  this.selectNom = false;
  this.selectPrenom = false;
  this.selectRegistre = false;
  this.selectMatricule = true;
  this.selectRaison = false;
  this.addClient = false;
  this.addEntreprise = false;
  }
  if(this.operationCourante.type === 'Achats') {
    console.log('activate select matricule : is achats');
    
   this.selectCinF = false;
  this.selectNomF = false;
  this.selectPrenomF = false;
  this.selectRegistreF = false;
  this.selectMatriculeF = true;
  this.selectRaisonF = false;
  this.addFournisseur = false;
  this.addEntrepriseF = false; 
  }
}
activateSelectRaison() {
  if(this.operationCourante.type === 'Ventes') {
    
  this.selectCin = false;
  this.selectNom = false;
  this.selectPrenom = false;
  this.selectRegistre = false;
  this.selectMatricule = false;
  this.selectRaison = true;
  this.addClient = false;
  this.addEntreprise = false;
  }
  if(this.operationCourante.type === 'Achats')
   {
    this.selectCinF = false;
    this.selectNomF = false;
    this.selectPrenomF = false;
    this.selectRegistreF = false;
    this.selectMatriculeF = false;
    this.selectRaisonF = true;
    this.addFournisseur = false;
    this.addEntrepriseF = false;
  }
}
activateAddClient()
 {
  if(this.operationCourante.type === 'Ventes') {
    
  this.selectCin = false;
  this.selectNom = false;
  this.selectPrenom = false;
  this.selectRegistre = false;
  this.selectMatricule = false;
  this.selectRaison = false;
  this.addClient = true;
  this.addEntreprise = false;
  this.operationCourante.client = new Client();
  }
  if(this.operationCourante.type === 'Achats')
    {
      this.selectCinF = false;
      this.selectNomF = false;
      this.selectPrenomF = false;
      this.selectRegistreF = false;
      this.selectMatriculeF = false;
      this.selectRaisonF = false;
      this.addFournisseur = true;
      this.addEntrepriseF = false;
      this.operationCourante.fournisseur = new Fournisseur();
  }      
}

activateAddEntreprise() {
  if(this.operationCourante.type === 'Ventes') {
    
  this.selectCin = false;
  this.selectNom = false;
  this.selectPrenom = false;
  this.selectRegistre = false;
  this.selectMatricule = false;
  this.selectRaison = false;
  this.addClient = false;
  this.addEntreprise = true;
  this.operationCourante.client = new Client();
  }

  if(this.operationCourante.type === 'Achats')
        {
    this.selectCinF = false;
    this.selectNomF = false;
    this.selectPrenomF = false;
    this.selectRegistreF = false;
    this.selectMatriculeF = false;
    this.selectRaisonF = false;
    this.addFournisseur = false;
    this.addEntrepriseF = true;
    this.operationCourante.fournisseur = new Fournisseur();
  }
  
}
saveClient() {
  
   // console.log(this.client);
   this.saveClientEnCours = true;
   this.clientService.addClient(this.operationCourante.client).subscribe((response) => {
     this.saveClientEnCours = false;
       if (response.error_message ===  undefined) {
         // this.save.emit(response);
        // this.client = new Client();
         this.openSnackBar(  'Client Enregistré');
       } else {
        // show error message
        this.openSnackBar(  'Erreurs!:Essayer une autre fois');
        
       }
   });
}
saveFournisseur() {

 // console.log(this.client);
 this.saveFournisseurEnCours = true;
 this.fournisseurService.addFournisseur(this.operationCourante.fournisseur).subscribe((response) => {
   this.saveFournisseurEnCours = false;
     if (response.error_message ===  undefined) {
       // this.save.emit(response);
      // this.client = new Client();
       this.openSnackBar(  'Fournisseur Enregistré');
     } else {
      // show error message
      this.openSnackBar(  'Erreurs!:Essayer une autre fois');
      
     }
 });
}
existeArticleInListLignes(article)
{
 for (let i = 0 ; i < this.operationCourante.lignesOperationCourante.length ; i++) {
        if(this.operationCourante.lignesOperationCourante[i].codeArticle === article.code
        && this.operationCourante.lignesOperationCourante[i].libelleArticle === article.libelle)
        {
             return true;
        }
 }
 return false;
}

existeArticleInTarget(article)
{
 for (let i = 0 ; i < this.targetListArticles.length ; i++) {
        if(this.targetListArticles[i].code === article.code
        && this.targetListArticles[i].libelle === article.libelle)
        {
             return true;
        }
 }
 return false;
}
  generateLignesOperationCourante()
  {
    if (this.operationCourante.lignesOperationCourante === undefined) {
    this.operationCourante.lignesOperationCourante = [];
    }
    this.targetListArticles.forEach((article, index) => {
      if (! this.existeArticleInListLignes(article))
      {
             const ligne = new LigneOperationCourante();
                ligne.codeArticle = article.code;
                ligne.libelleArticle = article.libelle;
                ligne.quantite  = 1;
                ligne.prixVentesFixe = article.prixVentes;
                ligne.prixTotal = article.prixVentes;
                
         this.operationCourante.lignesOperationCourante[index] = ligne;
      }
    }, this);
  }
  removeLigneOperationCourante() {
    if (this.operationCourante.lignesOperationCourante === undefined) {
    this.operationCourante.lignesOperationCourante = [];
    }
    const listLignes = this.operationCourante.lignesOperationCourante;
    listLignes.forEach((ligneOP, index) => {
      if (! this.existeArticleInTarget({code: ligneOP.codeArticle, libelle: ligneOP.libelleArticle})) {
        this.operationCourante.lignesOperationCourante =
        listLignes.filter((ligne) => (ligne.codeArticle !== ligneOP.codeArticle &&
          ligne.libelleArticle !== ligneOP.libelleArticle ));

      }
    }, this);
  }
  removeAllLignesOperationsCourantes()
  {
    this.operationCourante.lignesOperationCourante = [];
  }
  calculPrixTotal(i) {
    this.operationCourante.lignesOperationCourante[i].prixTotal =  this.operationCourante.lignesOperationCourante[i].prixVentesFixe *
    this.operationCourante.lignesOperationCourante[i].quantite;
  }









}
