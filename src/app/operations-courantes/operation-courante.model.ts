
import { BaseEntity } from '../common/base-entity.model' ;
// import { Tranche } from './tranche.model' ;
import { Client } from '../ventes/clients/client.model' ;
import { Fournisseur } from '../achats/fournisseurs/fournisseur.model' ;

import { PayementData } from './payement-data.model';
import { Article } from '../stocks/articles/article.model';
import { LigneOperationCourante } from './ligne-operation-courante.model';
// import { LigneReleveVente } from './ligne-releve-vente.model';
// import { Credit } from '../credits/credit.model';

type  EtatOperationCourante= 'Initial' |'Valide' | 'Annule' |'Regle'
|'NonRegle' | 'PartiellementRegle' | '' ;
type  TypeOperationCourante= 'Achats' |'Ventes' |'' ;
type  ModePaiement= 'Comptant' |'Credit' | 'Cheque' | 'Avance' |'Credit_Comptant' | 'Donnation' | '';

export class OperationCourante extends BaseEntity {
    constructor(
        public code?: string,
        public numeroOperationCourante?: string,
        public client?: Client,
        public fournisseur?: Fournisseur,
         public valeurFinanciere?: number,
         public valeurBenefice?: number ,
        public payementData?: PayementData,
        public dateOperationObject?: Date,
        public dateOperation?: string,
        public modePaiement?: ModePaiement,
        public articlesImpliques?: Article[],
       public etat?: EtatOperationCourante,
       public type?: TypeOperationCourante,
       
       public livre?: boolean,
       public recu?: boolean,
       public lignesOperationCourante?: LigneOperationCourante[],
     ) {
         super();
         this.client = new Client();
         this.fournisseur = new Fournisseur();
         this.valeurFinanciere = Number(0);
         this.valeurBenefice = Number(0);
         this.payementData = new PayementData();
         
        //  this.listLignesRelVentes = [] ;
         // this.categoryClient = 'PERSONNE_PHYSIQUE';
    }
}
