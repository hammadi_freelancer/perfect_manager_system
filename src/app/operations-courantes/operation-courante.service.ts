import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import {OperationCourante} from './operation-courante.model' ;
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_OPERATIONS_COURANTES = 'http://localhost:8082/operationsCourantes/getOperationsCourantes';
const  URL_GET_PAGE_OPERATIONS_COURANTES= 'http://localhost:8082/operationsCourantes/getPageOperationsCourantes';

const URL_GET_OPERATION_COURANTE = 'http://localhost:8082/operationsCourantes/getOperationCourante';
const  URL_ADD_OPERATION_COURANTE  = 'http://localhost:8082/operationsCourantes/addOperationCourante';
const  URL_UPDATE_OPERATION_COURANTE  = 'http://localhost:8082/operationsCourantes/updateOperationCourante';
const  URL_DELETE_OPERATION_COURANTE = 'http://localhost:8082/operationsCourantes/deleteOperationCourante';
const  URL_GET_TOTAL_OPERATIONS_COURANTES = 'http://localhost:8082/operationsCourantes/getTotalOperationsCourantes';



@Injectable({
  providedIn: 'root'
})
export class OperationCouranteService {

  constructor(private httpClient: HttpClient) {

   }
   getTotalOperationsCourantes(filter): Observable<any> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('filter', filterStr);
        return this.httpClient
        .get<any>(URL_GET_TOTAL_OPERATIONS_COURANTES, {params});
   }
   getPageOperationsCourantes(pageNumber, nbElementsPerPage, filter): Observable<OperationCourante[]> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('pageNumber', pageNumber)
    .set('nbElementsPerPage', nbElementsPerPage)
    .set('filter', filterStr);
    return this.httpClient
    .get<OperationCourante[]>(URL_GET_PAGE_OPERATIONS_COURANTES, {params});
   }
   getOperationsCourantes(): Observable<OperationCourante[]> {
   
    return this.httpClient
    .get<OperationCourante[]>(URL_GET_LIST_OPERATIONS_COURANTES);
   }
   addOperationCourante(operationCourante: OperationCourante): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_OPERATION_COURANTE, operationCourante);
  }
  updateOperationCourante(operationCourante: OperationCourante): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_OPERATION_COURANTE, operationCourante);
  }
  deleteOperationCourante(codeOperationCourante): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_OPERATION_COURANTE + '/' + codeOperationCourante);
  }
  getOperationCourante(codeOperationCourante): Observable<OperationCourante> {
    return this.httpClient.get<OperationCourante>(URL_GET_OPERATION_COURANTE + '/' + codeOperationCourante);
  }

  /*addLigneFactureVentes(ligneFactureVente: LigneFactureVente): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_LIGNE_FACTURE_VENTES, ligneFactureVente);
    
  }
  updateLigneFactureVentes(ligneFactureVente: LigneFactureVente): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_LIGNE_FACTURE_VENTES, ligneFactureVente);
  }
  getLignesFacturesVentesByCodeFacture(codeLigneFactureVentes): Observable<LigneFactureVente[]> {
    return this.httpClient.get<LigneFactureVente[]>( URL_GET_LIST_LIGNES_FACTURE_VENTES_BY_CODE_FACTURE_VENTES +
       '/' + codeLigneFactureVentes);
  }
  getLignesFacturesVentes(): Observable<LigneFactureVente[]> {
    return this.httpClient
    .get<LigneFactureVente[]>(URL_GET_LIST_LIGNES_FACTURE_VENTES);
   }
   getLigneFactureVentes(codeLigneFactureVentes): Observable<LigneFactureVente> {
    return this.httpClient.get<LigneFactureVente>(URL_GET_LIST_LIGNES_FACTURE_VENTES + '/' + codeLigneFactureVentes);
  }
  deleteLigneFactureVentes(codeLigneFactureVentes): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_LIGNE_FACTURE_VENTES + '/' + codeLigneFactureVentes);
  }*/
}
