import { Injectable } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
// operationsCourantes components
import { OperationCouranteNewComponent } from './operation-courante-new.component';
import { OperationCouranteEditComponent } from './operation-courante-edit.component';
import { OperationsCourantesGridComponent } from './operations-courantes-grid.component';
import { OperationCouranteHomeComponent } from './operation-courante-home.component';

import { ClientsResolver } from '../ventes/clients-resolver';

import {ModuleWithProviders  } from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';

 export const operationsCourantesRoute: Routes = [
    {
        path: 'operationsCourantes',
        component: OperationsCourantesGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterOperationCourante',
        component: OperationCouranteNewComponent,
        resolve: {
            clients: ClientsResolver,
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerOperationCourante/:codeOperationCourante',
        component: OperationCouranteEditComponent,
        resolve: {
            clients: ClientsResolver,
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
];
// export const ventesRoutingModule: ModuleWithProviders = RouterModule.forRoot(ventesRoute);
// commonRouting.providers = [{ provide: APP_BASE_HREF, useValue: '/common'} ] ;


