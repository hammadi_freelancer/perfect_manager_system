import { Component, OnInit, Output,ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {OperationCourante} from './operation-courante.model';
import {OperationCouranteFilter} from './operation-courante.filter';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {OperationCouranteService} from './operation-courante.service';
// import { ActionData } from './action-data.interface';
// import { Client } from '../../ventes/clients/client.model';
import { OperationCouranteElement } from './operation-courante.interface';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DialogAddClientComponent } from '../ventes/clients/dialog-add-client.component';
import {SelectItem} from 'primeng/api';

@Component({
  selector: 'app-operations-courantes-grid',
  templateUrl: './operations-courantes-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class OperationsCourantesGridComponent implements OnInit {

  private operationCourante: OperationCourante =  new OperationCourante();
  private selection = new SelectionModel<OperationCouranteElement>(true, []);
  private operationCouranteFilter: OperationCouranteFilter = new OperationCouranteFilter();
  

  private columnsToSelect : SelectItem[];
   private  selectedColumnsDefinitions: string[];
  private selectedColumnsNames: string[];
  
@Output()
select: EventEmitter<OperationCourante[]> = new EventEmitter<OperationCourante[]>();


 selectedOperationCourante: OperationCourante ;
 private   showFilter = false;
 private   showFilterClient = false;
 private   showFilterFournisseur = false;
 
 private showSelectColumnsMultiSelect = false;
 
 private etatsOperationCourante: any[] = [
  {value: 'Initial', viewValue: 'Initiale'},
  {value: 'Valide', viewValue: 'Validée'},
  {value: 'Annule', viewValue: 'Annulée'},
  // {value: 'Regle', viewValue: 'Reglée'},
  // {value: 'NonRegle', viewValue: 'Non Reglée'},
 //  {value: 'PartiellementRegle', viewValue: 'Partiellement Reglée'},
];
private typesOperationCourante: any[] = [
  {value: 'Achats', viewValue: 'Achats'},
  {value: 'Ventes', viewValue: 'Ventes'},
  {value: '', viewValue: ''},
];

 @Input()
 private listOperationsCourantes: any = [] ;
 private checkedAll: false;

 private  dataOperationsCourantesSource: MatTableDataSource<OperationCouranteElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay :ColumnDefinition[] = [];
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
  constructor( private route: ActivatedRoute, private operationCouranteService: OperationCouranteService,
    private router: Router, private matSnackBar: MatSnackBar,
  private  dialog: MatDialog ) {
  }
  ngOnInit() {
    this.columnsToSelect = [
      {label: 'N° Opération', value: 'numeroOperationCourante', title: 'N°Opération'},
      {label: 'Date Opération', value: 'dateOperation', title: 'Date Opération'},
      {label: 'Nom Client', value: 'nomClient', title: 'Nom Client'},
      {label: 'Prénom Client', value: 'prenomClient', title: 'Prénom Client'},
      {label: 'Valeur(DT)', value: 'valeurFinanciere', title: 'Valeur(DT)'},
      {label: 'Mode Paiement', value: 'modePaiement', title: 'Mode Paiement'},      
      {label: 'Bénéfice', value: 'valeurBenefice', title: 'Bénéfice'},
      {label: 'Livré', value: 'livre', title: 'Livré'},
      {label: 'Reçu', value: 'recu', title: 'Reçu'},
     
      {label: 'Rs.Soc.Client', value:  'raisonClient', title: 'Rs.Soc.Client' },
      {label: 'Matr.Client', value:  'matriculeClient', title: 'Matr.Client' },
      {label: 'Reg.Client', value:  'registreClient', title: 'Reg.Client' },
      {label: 'CIN Four.', value:  'cinFournisseur', title: 'CIN Four.' },
      {label: 'Nom Four.', value:  'nomFournisseur', title: 'Nom Four.' },
      {label: 'Prénom Four.', value:  'prenomFournisseur', title: 'Prénom Four.' },
      {label: 'Raison Four.', value:  'raisonFournisseur', title: 'Raison Four.' },
      {label: 'Matr Four.', value:  'matriculeFournisseur', title: 'Matr Four.' },
      {label: 'Reg Four.', value:  'registreFournisseur', title: 'Reg Four.' },
      {label: 'Type', value: 'type', title: 'Type' },
      {label: 'Etat', value:  'etat', title: 'Etat' },
  
  ];
  this.selectedColumnsDefinitions = [
    'numeroOperationCourante',
   'dateOperation' , 
   'nomClient' , 
   'prenomClient', 
   'valeurFinanciere', 
   'modePaiement', 'etat',
    ];
    this.operationCouranteService.getPageOperationsCourantes(0, 5, null).
    subscribe((operationsCourantes) => {
      this.listOperationsCourantes = operationsCourantes;
  
      this.defaultColumnsDefinitions = [
        {name: 'numeroOperationCourante' , displayTitle: 'N°Opér'},
        {name: 'dateOperation' , displayTitle: 'Date Opération'},
         {name: 'nomClient' , displayTitle: 'Nom Cl.'},
        {name: 'prenomClient' , displayTitle: 'Prénom Cl.'},
        {name: 'cinClient' , displayTitle: 'CIN Cl.'},
        {name: 'valeurFinanciere' , displayTitle: 'Valeur'},
        {name: 'valeurBenefice' , displayTitle: 'Bénéfice'},
        
        {name: 'matriculeClient' , displayTitle: 'Matr. Cl.'},
        {name: 'raisonClient' , displayTitle: 'Raison Cl.'},
        {name: 'registreClient' , displayTitle: 'Registre Cl.'},
        {name: 'nomFournisseur' , displayTitle: 'Nom Four.'},
        {name: 'prenomFournisseur' , displayTitle: 'Prénom Four.'},
        {name: 'cinFournisseur' , displayTitle: 'CIN Founr.'},
        {name: 'matriculeFournisseur' , displayTitle: 'Matr. Four.'},
        {name: 'raisonFournisseur' , displayTitle: 'Raison Four.'},
        {name: 'registreFournisseur' , displayTitle: 'Registre Four.'},
         {name: 'modePaiement' , displayTitle: 'Mode Paiement'},
         {name: 'etat', displayTitle: 'Etat' },
         {name: 'type', displayTitle: 'Type' },
         {name: 'livre', displayTitle: 'Livré' },
         {name: 'recu', displayTitle: 'Reçu' }
         
        ];
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });
  }
  deleteOperationCourante(operationCourante) {
      console.log('call delete !', operationCourante );
    this.operationCouranteService.deleteOperationCourante(operationCourante.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData(null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.operationCouranteService.getPageOperationsCourantes(0, 5, filter).subscribe((operationsCourantes) => {
    this.listOperationsCourantes = operationsCourantes;
    this.transformDataToByConsumedByMatTable();
   });
}


checkOneActionInvoked() {
  const listSelectedOperationsCourantes: OperationCourante[] = [];

if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((OperCElement) => {
  return OperCElement.code;
});
this.listOperationsCourantes.forEach(OP => {
  if (codes.findIndex(code => code === OP.code) > -1) {
    listSelectedOperationsCourantes.push(OP);
  }
  });
}
this.select.emit(listSelectedOperationsCourantes);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listOperationsCourantes !== undefined) {
    this.listOperationsCourantes.forEach(opCourante => {
      listElements.push( {
        code: opCourante.code ,
        numeroOperationCourante: opCourante.numeroOperationCourante,
        cinClient: opCourante.client.cin,
        nomClient: opCourante.client.nom ,
        prenomClient: opCourante.client.prenom ,
        raisonClient: opCourante.client.raisonSociale ,
        matriculeClient: opCourante.client.matriculeFiscale,
        registreClient: opCourante.client.registreCommerce ,
        
        cinFournisseur: opCourante.fournisseur.cin,
        nomFournisseur: opCourante.fournisseur.nom ,
        prenomFournisseur: opCourante.fournisseur.prenom ,
        raisonFournisseur: opCourante.fournisseur.raisonSociale,
        matriculeFournisseur: opCourante.fournisseur.matriculeFiscale,
        registreFournisseur: opCourante.fournisseur.registreCommerce ,
       valeurFinanciere: opCourante.valeurFinanciere ,
       valeurBenefice: opCourante.valeurBenefice ,
        dateOperation: opCourante.dateOperation,
        modePaiement: opCourante.modePaiement,
       etat: opCourante.etat,
       type: opCourante.type,
       livre: opCourante.livre ? 'Oui' : 'Non',
       recu: opCourante.recu ? 'Oui' : 'Non',
      } );
    });
  }
    this.dataOperationsCourantesSource = new MatTableDataSource<OperationCouranteElement>(listElements);
    this.dataOperationsCourantesSource.sort = this.sort;
    this.dataOperationsCourantesSource.paginator = this.paginator;
    this.operationCouranteService.getTotalOperationsCourantes(this.operationCouranteFilter).subscribe((response) => {
    //   console.log('Total Releves de Ventes  is ', response);
    if (this.dataOperationsCourantesSource.paginator) {
       this.dataOperationsCourantesSource.paginator.length = response['totalOperationsCourantes'];
  
        }    });
}

specifyListColumnsToBeDisplayed() {
  
   console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
   this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
 ?
    this.defaultColumnsDefinitions : [];
    this.selectedColumnsNames = [];
    
    if (this.selectedColumnsDefinitions !== undefined)
     {
       this.selectedColumnsDefinitions.forEach((colName) => { 
            const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
            const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
            const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
            this.columnsDefinitionsToDisplay.push(colDef);
           });
     }
    this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
   this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
              return colDef.displayTitle;
    });
    console.log('columns to disp:', this.columnsDefinitionsToDisplay);
    this.columnsNames = [];
    this.columnsNames[0] = 'select';
    this.columnsDefinitionsToDisplay .map((colDef) => {
     this.columnsNames.push(colDef.name);
    if (this.selectedColumnsDefinitions !== undefined) {
     this.selectedColumnsNames.push(colDef.displayTitle);
    }
 });
    this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
     return colDef.displayTitleAr;
 });
 }

masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataOperationsCourantesSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataOperationsCourantesSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataOperationsCourantesSource.filter = filterValue.trim().toLowerCase();
  if (this.dataOperationsCourantesSource.paginator) {
    this.dataOperationsCourantesSource.paginator.firstPage();
  }
}

loadAddOperationCouranteComponent() {
  this.router.navigate(['/pms/suivie/ajouterOperationCourante']) ;

}
loadEditOperationCouranteComponent() {
  this.router.navigate(['/pms/suivie/editerOperationCourante', this.selection.selected[ this.selection.selected.length - 1].code]) ;

}
supprimerOperationsCourantes()
{

  if ( this.selection.selected.length !== 0) {
      this.selection.selected.forEach(function(opCourante, index) {

   this.operationCouranteService.deleteOperationCourante(opCourante.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.openSnackBar( this.selection.selected.length + ' Opération de Courante Supprimé(s)');
            this.selection.selected = [];
            this.loadData(null);
            }
      }, this);
    } else {
      this.openSnackBar('Aucune Opération Séléctionnée !');
    }
}
refresh() {
  this.loadData(null);
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

 history()
 {
   
 }
 addClient()
 {
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
  };

  const dialogRef = this.dialog.open(DialogAddClientComponent,
    dialogConfig
  );
 }

 toggleFilterPanel()
 {
    this.showFilter = !this.showFilter;
 }
 toggleShowColumnsMultiSelect()
 {
   this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
 }
 loadFiltredData()
 {
   console.log('the filter is ', this.operationCouranteFilter);
   this.loadData(this.operationCouranteFilter);
 }
 attacherDocuments()
 {

 }
 changePage($event) {
  console.log('page event is ', $event);
 this.operationCouranteService.getPageOperationsCourantes($event.pageIndex, $event.pageSize,
  this.operationCouranteFilter ).subscribe((opsCourantes) => {
    this.listOperationsCourantes = opsCourantes;
   const listElements = [];
   this.listOperationsCourantes.forEach(opCourante => {
    listElements.push( {
      code: opCourante.code ,
      numeroOperationCourante: opCourante.numeroOperationCourante,
      cinClient: opCourante.client.cin,
      nomClient: opCourante.client.nom,
      prenomClient: opCourante.client.prenom ,
      raisonClient: opCourante.client.raisonSociale ,
      matriculeClient: opCourante.client.matriculeFiscale,
      registreClient: opCourante.client.registreCommerce ,
      cinFournisseur: opCourante.fournisseur.cin,
      nomFournisseur: opCourante.fournisseur.nom ,
      prenomFournisseur: opCourante.fournisseur.prenom ,
      raisonFournisseur: opCourante.fournisseur.raisonSociale,
      matriculeFournisseur: opCourante.fournisseur.matriculeFiscale,
      registreFournisseur: opCourante.fournisseur.registreCommerce ,
     valeurFianciere: opCourante.valeurFianciere ,
     valeurBenefice: opCourante.valeurBenefice ,
      dateOperation: opCourante.dateOperation,
      modePaiement: opCourante.modePaiement,
     etat: opCourante.etat,
     type: opCourante.type,
     livre: opCourante.livre ? 'Oui' : 'Non',
     recu: opCourante.recu ? 'Oui' : 'Non',
      
  });
});
     this.dataOperationsCourantesSource= new MatTableDataSource<OperationCouranteElement>(listElements);

     
     this.operationCouranteService.getTotalOperationsCourantes(this.operationCouranteFilter)
     .subscribe((response) => {
      console.log('Total OPer Courantes  is ', response);
      if (this.dataOperationsCourantesSource.paginator) {
       this.dataOperationsCourantesSource.paginator.length = response['totalOperationsCourantes'];
      }
     });

    });
}

activateFilterClient()
{
  this.showFilterClient = !this.showFilterClient ;
  
}
activateFilterFournisseur()
{
  this.showFilterFournisseur = !this.showFilterFournisseur ;
  
}

}
