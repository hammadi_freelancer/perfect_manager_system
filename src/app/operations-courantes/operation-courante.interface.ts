export interface OperationCouranteElement {
   
         code: string;
         numeroOperationCourante: string;
         cinClient: string;
         nomClient: string;
         prenomClient: string;
         cinFournisseur: string;
         nomFournisseur: string;
         prenomFournisseur: string;
        valeurFinanciere: number;
        valeurBenefice: number;
         dateOperation: string;
         modePaiement: string;
        etat: string;
        type: string;
        livre: string;
        recu: string;
        }
