
import { BaseEntity } from '../common/base-entity.model' ;


export class LigneOperationCourante extends BaseEntity {
    constructor(
        public codeArticle?: string,
        public libelleArticle?: string,
        public quantite?: number,
        public prixVentesFixe?: number,
        public prixTotal?: number

     ) {
         super();
         this.quantite = Number(0);
         this.prixTotal = Number(0);
         this.prixVentesFixe = Number(0);
         
         // this.payementData = new PayementData();
        //  this.listLignesRelVentes = [] ;
         // this.categoryClient = 'PERSONNE_PHYSIQUE';
    }
}
