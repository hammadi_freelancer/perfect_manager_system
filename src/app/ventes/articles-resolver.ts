import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { ArticleService } from '../stocks/articles/article.service';
import { Article} from '../stocks/articles/article.model';

@Injectable()
export class ArticlesResolver implements Resolve<Observable<Article[]>> {
  constructor(private articlesService: ArticleService) { }

  resolve() {
     return this.articlesService.getArticles();
}
}
