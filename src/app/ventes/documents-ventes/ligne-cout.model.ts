import { Article } from '../../stocks/articles/article.model';
import { BaseEntity } from '../../common/base-entity.model' ;
// type LibelleModePayement = 'Credit'| 'Credit_Espece' | 'Cheque' | 'Espece';
export class LigneCout extends BaseEntity {
    constructor(
        public code?: number,
        public article?: Article,
        public quantite?: number,
         public coutTransport?: number,
         public coutMainOeuvre?: number,
         public diversCouts?: number,
         public coutTotal?: number,
          
     ) {
        super();
         this.article = new Article();
         this.quantite = Number(0);
         this.coutTransport = Number(0);
         this.coutMainOeuvre = Number(0);
         this.coutTotal =  Number(0);
         
    }
}
