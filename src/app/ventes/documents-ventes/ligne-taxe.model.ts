import { Article } from '../../stocks/articles/article.model';
import { BaseEntity } from '../../common/base-entity.model' ;
// type LibelleModePayement = 'Credit'| 'Credit_Espece' | 'Cheque' | 'Espece';
export class LigneTaxe extends BaseEntity {
    constructor(
        public code?: number,
        public article?: Article,
        public quantite?: number,
         public tva?: number,
         public montantTVA?: number,
 
     ) {
        super();
         this.article = new Article();
         this.quantite = Number(0);
         this.tva = Number(0);
        this.montantTVA = Number(0);
         

    }
}
