import { Component, OnInit, Output,ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {DocumentVente} from './document-vente.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {DocumentVenteService} from './document-vente.service';
// import { ActionData } from './action-data.interface';
import { Client } from '../../ventes/clients/client.model';
import { DocumentVenteElement } from './document-vente.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DialogAddClientComponent } from '../clients/dialog-add-client.component';
// import { DialogAddPayementFactureVentesComponent } from './dialog-add-payement-facture-ventes.component';

@Component({
  selector: 'app-documents-ventes-grid',
  templateUrl: './documents-ventes-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class DocumentsVentesGridComponent implements OnInit {

  private documentVente: DocumentVente =  new DocumentVente();
  private selection = new SelectionModel<DocumentVenteElement>(true, []);
  
@Output()
select: EventEmitter<DocumentVente[]> = new EventEmitter<DocumentVente[]>();

//private lastClientAdded: Client;

 selectedDocumentVente: DocumentVente ;

 

 @Input()
 private listDocumentsVentes: any = [] ;
 listDocumentsVentesOrgin: any = [];
 private checkedAll: false;

 private  dataDocumentsVentesSource: MatTableDataSource<DocumentVenteElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
  constructor( private route: ActivatedRoute, private documentVenteService: DocumentVenteService,
    private router: Router, private matSnackBar: MatSnackBar,
  private  dialog: MatDialog ) {
  }
  ngOnInit() {
    this.documentVenteService.getDocumentsVentes().subscribe((documentsVentes) => {
      this.listDocumentsVentes = documentsVentes;
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });


  }
  deleteDocumentVente(documentVente) {
      console.log('call delete !', documentVente );
    this.documentVenteService.deleteDocumentVente(documentVente.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData();

      } else {
       // show error message
      }
     });

  }
loadData() {
  this.documentVenteService.getDocumentsVentes().subscribe((documentsVentes) => {
    this.listDocumentsVentes = documentsVentes;
    this.transformDataToByConsumedByMatTable();
   });
}


checkOneActionInvoked() {
  const listSelectedDocumentsVentes: DocumentVente[] = [];
console.log('selected clients are ');
console.log(this.selection.selected);
if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((documentVenteElement) => {
  return documentVenteElement.code;
});
this.listDocumentsVentes.forEach(documentVente => {
  if (codes.findIndex(code => code === documentVente.code) > -1) {
    listSelectedDocumentsVentes.push(documentVente);
  }
  });
}
this.select.emit(listSelectedDocumentsVentes);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listDocumentsVentes !== undefined) {
    this.listDocumentsVentes.forEach(documentVente => {
      listElements.push( { code: documentVente.code,
        numeroDocumentVente: documentVente.numeroDocumentVente,
        dateVente: documentVente.dateVente, etat: documentVente.etat,
        cin: documentVente.client.cin, nom: documentVente.client.nom, prenom: documentVente.client.prenom,
         raisonSociale: documentVente.client.raisonSociale , montantAPayer: documentVente.montantAPayer,
          montantTVA: documentVente.montantTVA, montantRemise: documentVente.montantRemise,
          montantCout: documentVente.montantCout, nbJoursOccupationMainOeuvre: documentVente.nbJoursOccupationMainOeuvre,
          nbMagasins: documentVente.nbMagasins, gestionMarchandises: documentVente.gestionMarchandises,
          gestionTaxes: documentVente.gestionTaxes, gestionLogistique: documentVente.gestionLogistique,
          gestionCouts: documentVente.gestionCouts,  gestionMainOeuvres: documentVente.gestionMainOeuvres, 
          gestionRemises: documentVente.gestionRemises,
       // montantHT: factureVente.montantHT, montantTVA: factureVente.montantTVA,
       // etat: factureVente.etat
       
    });
  });
    this.dataDocumentsVentesSource = new MatTableDataSource<DocumentVenteElement>(listElements);
    this.dataDocumentsVentesSource.sort = this.sort;
    this.dataDocumentsVentesSource.paginator = this.paginator;
}
}
 specifyListColumnsToBeDisplayed() {
   this.columnsDefinitions = [
   {name: 'numeroDocumentVente' , displayTitle: 'N.Document'},
   {name: 'dateVente' , displayTitle: 'Date Vente'},
   {name: 'etat' , displayTitle: 'Etat'},
    {name: 'cin' , displayTitle: 'CIN'},
   {name: 'nom' , displayTitle: 'Nom'},
   {name: 'prenom' , displayTitle: 'Prenom'},
   {name: 'raisonSociale' , displayTitle: 'Raison Soc.'},
    {name: 'montantAPayer' , displayTitle: 'M.A Payer'},
    {name: 'montantTVA' , displayTitle: 'M.TVA'},
    {name: 'montantRemise' , displayTitle: 'M.Remise'},
    {name: 'montantCout' , displayTitle: 'M.Coûts'},
    {name: 'nbJoursOccupationMainOeuvre', displayTitle: 'Nbre Jours OCC.' },
    {name: 'nbMagasins', displayTitle: 'Nbre Mag.' }
   ];
   this.columnsTitles = this.columnsDefinitions.map((colDef) => {
             return colDef.displayTitle;
   });
   this.columnsNames[0] = 'select';
   this.columnsDefinitions.map((colDef) => {
    this.columnsNames.push(colDef.name);
});
   this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
    return colDef.displayTitleAr;
});
}
masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataDocumentsVentesSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataDocumentsVentesSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataDocumentsVentesSource.filter = filterValue.trim().toLowerCase();
  if (this.dataDocumentsVentesSource.paginator) {
    this.dataDocumentsVentesSource.paginator.firstPage();
  }
}

loadAddDocumentVenteComponent() {
  this.router.navigate(['/pms/ventes/ajouterDocumentVente']) ;

}
loadEditDocumentVenteComponent() {
  this.router.navigate(['/pms/ventes/editerDocumentVente', this.selection.selected[ this.selection.selected.length - 1].code]) ;

}
supprimerDocumentsVentes() {
  if ( this.selection.selected.length !== 0) {
      this.selection.selected.forEach(function(documentVente, index) {
          this.documentVenteService.deleteDocumentVente(documentVente.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.openSnackBar( this.selection.selected.length + ' Documents(s) Supprimé(s)');
             this.loadData();
            }
      }, this);
    } else {
      this.openSnackBar('Aucun Document Séléctionné !');
    }
}
refresh() {
  this.loadData();
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

 history()
 {
   
 }
 addClient()
 {
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
    top : '0'
  };
  dialogConfig.data = {
    isParticulier : 'none'
  };
  const dialogRef = this.dialog.open(DialogAddClientComponent,
    dialogConfig
  );

}

}
