
import { BaseEntity } from '../../common/base-entity.model' ;
// import { Tranche } from './tranche.model' ;
import { Client } from '../clients/client.model' ;
import { LigneMarchandise } from './ligne-marchandise.model';
import { LigneLogistique } from './ligne-logistique.model';
import { LigneCout } from './ligne-cout.model';
import { LigneTaxe } from './ligne-taxe.model';
import { LigneRemise } from './ligne-remise.model';

import { LigneOccupationMainOeuvre } from './ligne-occupation-main-oeuvre.model';
import { PayementData } from './payement-data.model';

import { Credit } from '../credits/credit.model';

type  EtatDocument= 'Reglee'| 'Non Reglee' | 'Partiellement Reglee' |
'Initial' | 'Valide' | '' | 'Annule'  ;

export class DocumentVente extends BaseEntity {
    constructor(public code?: string,
        public numeroDocumentVente?: string,
        public client?: Client,
        public montantAPayer?: number,
        public montantTVA?: number,
        public montantRemise?: number,
        public montantCout?: number,
        public nbJoursOccupationMainOeuvre?: number,
        public credit?: Credit,
        public dateVenteObject?: Date,
         public dateVente?: string,
          public etat?: EtatDocument,
         public payementData?: PayementData,
         public nbMagasins?: number,
         public gestionMarchandises?: boolean,
         public gestionTaxes?: boolean,
         public gestionLogistique?: boolean,
         public gestionCouts?: boolean,
         public gestionMainOeuvres?: boolean,
         public gestionRemises?: boolean,
         
         // public montantAPayerApresRemise?: number,
        //  public timbreFiscale?: number ,

     public listLignesMarchandises?: LigneMarchandise[],
     public listLignesCouts?: LigneCout[],
     public listLignesTaxes?: LigneTaxe[],
     public listLignesLogistiques?: LigneLogistique[],
     public listLignesOccupationMainOeuvres?: LigneOccupationMainOeuvre[],
     public listLignesRemises?: LigneRemise[]

     ) {
         super();
         this.client = new Client();
        //  this.credit = Credit.constructDefaultInstance();
         this.montantAPayer =  Number(0);
         this.nbMagasins = Number(0);
         this.nbJoursOccupationMainOeuvre = Number(0);
         this.montantCout =  Number(0);
         // this.montantTTC  = Number(0);
         this.montantTVA = Number(0);
         this.montantRemise = Number(0);
        // this.timbreFiscale = Number(0);
         this.payementData = new PayementData();
         // this.categoryClient = 'PERSONNE_PHYSIQUE';
    }
}
