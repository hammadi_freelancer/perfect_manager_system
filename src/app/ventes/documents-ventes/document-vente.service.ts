import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';

import {DocumentVente} from './document-vente.model' ;
// import {DocumentVentes} from './payement-facture-ventes.model' ;

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_DOCUMENTS_VENTES = 'http://localhost:8082/ventes/documentsVentes/getDocumentsVentes';
const URL_GET_DOCUMENT_VENTE = 'http://localhost:8082/ventes/documentsVentes/getDocumentVente';
const  URL_ADD_DOCUMENT_VENTE  = 'http://localhost:8082/ventes/documentsVentes/addDocumentVente';
const  URL_UPDATE_DOCUMENT_VENTE  = 'http://localhost:8082/ventes/documentsVentes/updateDocumentVente';
const  URL_DELETE_DOCUMENT_VENTE  = 'http://localhost:8082/ventes/documentsVentes/deleteDocumentVente';

// const  URL_ADD_PAYEMENT_FACTURE_VENTES = 'http://localhost:8082/finance/payementsFacturesVentes/addPayementFactureVentes';
// const  URL_GET_LIST_PAYEMENT_FACTURE_VENTES  = 'http://localhost:8082/finance/payementsFacturesVentes/getPayementsFacturesVentes';
// const  URL_DELETE_PAYEMENT_FACTURE_VENTES  = 'http://localhost:8082/finance/payementsFacturesVentes/deletePayementFactureVentes';
// const  URL_UPDATE_PAYEMENT_FACTURE_VENTES  = 'http://localhost:8082/finance/payementsFacturesVentes/updatePayementFactureVentes';
// const URL_GET_PAYEMENT_FACTURE_VENTES  = 'http://localhost:8082/finance/payementsFacturesVentes/getPayementFactureVentes';
// const URL_GET_LIST_PAYEMENT_FACTURE_VENTES_BY_CODE_FACTURE =
// 'http://localhost:8082/finance/payementsFacturesVentes/getPayementsFacturesVentesByCodeFactureVentes';


@Injectable({
  providedIn: 'root'
})
export class DocumentVenteService {

  constructor(private httpClient: HttpClient) {

   }

   getDocumentsVentes(): Observable<DocumentVente[]> {
    return this.httpClient
    .get<DocumentVente[]>(URL_GET_LIST_DOCUMENTS_VENTES);
   }
   addDocumentVente(docVente: DocumentVente): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_DOCUMENT_VENTE, docVente);
  }
  updateDocumentVente(docVente: DocumentVente): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_DOCUMENT_VENTE, docVente);
  }
  deleteDocumentVente(codeDocVente): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_DOCUMENT_VENTE + '/' + codeDocVente);
  }
  getDocumentVente(codeDocVente): Observable<DocumentVente> {
    return this.httpClient.get<DocumentVente>(URL_GET_DOCUMENT_VENTE + '/' + codeDocVente);
  }
}
