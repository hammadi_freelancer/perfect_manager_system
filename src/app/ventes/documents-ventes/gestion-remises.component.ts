import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,
    
      ViewChild } from '@angular/core';
    // import { ArticleService } from './article.service';
    import {DocumentVente} from './document-vente.model';
    
    import {DocumentVenteService} from './document-vente.service';
    import {FormControl} from '@angular/forms';
    import {map, startWith} from 'rxjs/operators';
    import {Observable} from 'rxjs';
    import { Router, ActivatedRoute, ParamMap } from '@angular/router';
    // import {ActionData} from './action-data.interface';
    import { ClientService } from '../clients/client.service';
    import {Client} from '../clients/client.model';
    import {LigneRemise} from './ligne-remise.model';
    
    import {MatSnackBar} from '@angular/material';
    import { MatExpansionPanel } from '@angular/material';
    import {ArticleService} from '../../stocks/articles/article.service';
    import {Article} from '../../stocks/articles/article.model';
    import {ModePayementLibelle} from './mode-payement-libelle.interface';
    
    type LibelleModePayement = 'Credit'| 'Credit_Espece' | 'Cheque' | 'Espece';
    
    @Component({
      selector: 'app-gestion-remises',
      templateUrl: './gestion-remises.component.html',
      // styleUrls: ['./players.component.css']
    })
    export class GestionRemisesComponent implements OnInit {
    
     @Input()
     private documentVente: DocumentVente =  new DocumentVente();
    
     @Input()
     private mode: boolean;

     @Input()
     private listArticles: Article[];
     

    @Output()
    save: EventEmitter<DocumentVente> = new EventEmitter<DocumentVente>();
    
    
    private listClients: Client[];
    private listCodesArticles: string[];
    private lignesRemises: LigneRemise[] = [ ];
    private  pagesLignesRemises: LigneRemise[][] = [ ];
   //  private  pagesReadOnlyLignesFacures: boolean[][] = [ ];
    // private  pagesEditModeLignesFacures: boolean[][] = [ ];
    
    private currentPage = 1;
    // private editLignesMode: boolean [] = [];
    // private readOnlyLignes: boolean [] = [];
    
  
    
      constructor( private route: ActivatedRoute,
        private router: Router, private documentVenteService: DocumentVenteService, private clientService: ClientService,
        private articleService: ArticleService,
       private matSnackBar: MatSnackBar, private elRef: ElementRef) {
      }
    
    
      // myControl = new FormControl();
      // options: string[] = ['One', 'Two', 'Three'];
      // filteredOptions: Observable<string[]>;
    
      ngOnInit() {
    
       // this.listClients = this.route.snapshot.data.clients;
       // this.listArticles = this.route.snapshot.data.articles;
       this.listCodesArticles  = this.listArticles.map((article) => article.code);
      //  this.documentVenteFilter = new DocumentVenteFilter(this.listClients, this.listArticles);
       // this.firstMatExpansionPanel.open();
      this.initializeListLignesRemises();
      this.lignesRemises.forEach(function(ligne, index) {
                     ligne.code = index + 1;
      });
      this.pagesLignesRemises[this.currentPage - 1] = this.lignesRemises;
      
      }
      private initializeListLignesRemises() {
        this.lignesRemises = [];
       //  this.editLignesMode = [];
        // this.readOnlyLignes = [];
        this.lignesRemises.push(new LigneRemise());
        this.lignesRemises.push(new LigneRemise());
        this.lignesRemises.push(new LigneRemise());
        this.lignesRemises.push(new LigneRemise());
        this.lignesRemises.push(new LigneRemise());
       //  this.editLignesMode.push(false);
       //  this.editLignesMode.push(false);
        // this.editLignesMode.push(false);
        // this.editLignesMode.push(false);
        // this.editLignesMode.push(false);
    
        // this.readOnlyLignes.push(false);
        // this.readOnlyLignes.push(false);
        // this.readOnlyLignes.push(false);
        // this.readOnlyLignes.push(false);
        // this.readOnlyLignes.push(false);
      }
     /* constructClassName(i)  {
        return  'designationDivs_' + i;
       //  this.elRef.nativeElement.querySelector('.designationDivs_' + i )  ;
      }*/
   
      deleteRow(i)
      {
        
    
         //  this.documentVente.montantAPayer = this.documentVente.montantTTC ;
          console.log('after delete');
          // console.log( this.factureVente.montantHT);
          // console.log( this.factureVente.montantTTC);
          this.lignesRemises.splice(i, 1 );
          // this.editLignesMode.splice(i, 1); 
          // this.readOnlyLignes.splice(i, 1); 
          
          this.lignesRemises.push(new LigneRemise());
         //  this.editLignesMode.push(false);
        //   this.readOnlyLignes.push(false);
          
             
      }
  
      validateRow(i) {
        // this.factureVente.montantHT = +this.factureVente.montantHT + this.lignesFactures[i].montantHT ;
         this.documentVente.montantRemise = +this.documentVente.montantRemise + this.lignesRemises[i].montantRemise ;
         // this.factureVente.montantTTC = +this.factureVente.montantTTC  + this.lignesFactures[i].montantTTC ;
         // this.factureVente.montantRemise = +this.factureVente.montantRemise  + this.lignesFactures[i].montantRemise ;
         // this.factureVente.montantAPayer = this.factureVente.montantTTC ;
        //  this.factureVente.montantAPayerApresRemise = +this.factureVente.montantAPayer - this.factureVente.montantRemise;
         // this.editLignesMode[i] = true;
         // this.readOnlyLignes[i] = true;
       
       
        }
      
   
      nextPage()
      {
        this.pagesLignesRemises[this.currentPage - 1] = this.lignesRemises;
    
        // this.pagesReadOnlyLignesFacures[this.currentPage - 1] = this.readOnlyLignes;
        // this.pagesEditModeLignesFacures[this.currentPage - 1] = this.editLignesMode;
        
        this.currentPage++;
        if ( this.pagesLignesRemises[this.currentPage - 1] === undefined) {
        this.initializeListLignesRemises();
        this.lignesRemises.forEach(function(ligne, index) {
          console.log('number page');
          console.log(index + ((this.currentPage * this.currentPage) + 1));
          ligne.code = index + ((this.currentPage * this.currentPage) + 1);
    }, this);
           console.log('Next Page lignes Factures ');
              // console.log(this.lignesFactures);
         this.pagesLignesRemises[this.currentPage - 1 ] = this.lignesRemises;
         // this.pagesReadOnlyLignesFacures[this.currentPage - 1] = this.readOnlyLignes;
         // this.pagesEditModeLignesFacures[this.currentPage - 1] = this.editLignesMode;
         
         
        } else {
          console.log('Next Page lignes Factures ');
           this.lignesRemises = this.pagesLignesRemises[this.currentPage - 1 ];
          //  this.readOnlyLignes = this.pagesReadOnlyLignesFacures[this.currentPage - 1 ];
         //   this.editLignesMode = this.pagesEditModeLignesFacures[this.currentPage - 1 ];
        }
        const  button: HTMLButtonElement = this.elRef.nativeElement.querySelector('.paginationLabel' )  ;
        button.innerText = 'Page ' + this.currentPage + ' / ' + this.pagesLignesRemises.length;
      }
    
      previousPage()
      {
        this.pagesLignesRemises[this.currentPage - 1] = this.lignesRemises ;
       //  this.pagesReadOnlyLignesFacures[this.currentPage - 1] = this.readOnlyLignes;
      //   this.pagesEditModeLignesFacures[this.currentPage - 1] = this.editLignesMode;
        
        this.currentPage--;
       this.lignesRemises =  this.pagesLignesRemises[this.currentPage - 1];
      //  this.readOnlyLignes = this.pagesReadOnlyLignesFacures[this.currentPage - 1];
       // this.editLignesMode = this.pagesEditModeLignesFacures[this.currentPage - 1];
       
       // this.factureVenteFilter = new FactureVenteFilter(this.listClients, this.listArticles);
       const  button: HTMLButtonElement = this.elRef.nativeElement.querySelector('.paginationLabel' )  ;
       button.innerText = 'Page ' + this.currentPage + ' / ' + this.pagesLignesRemises.length;
    
      }
      selectedArticleCode(i, $event) {
         console.log(i);
         console.log($event);
         this.lignesRemises[i].article.code = $event.option.value;
         this.lignesRemises[i].article.libelle  = this.listArticles.find((article) => (article.code === $event.option.value)).libelle;
        
         const  div  : HTMLDivElement = this.elRef.nativeElement.querySelector('.designationDivs_' + i )  ;
         div.innerText = this.lignesRemises[i].article.libelle;
        // console.log(div);
    
    
      }
      fillDataLignesRemises()
      {
        this.documentVente.listLignesRemises= [];
         this.pagesLignesRemises.forEach(function(page, index) {
           page.forEach(function(ligne, i) {
               if ( ligne.montantRemise !== 0 ) {
                this.documentVente.listLignesRemises.push(ligne);
               }
           }, this
         );
         }, this);
      }
      openSnackBar(messageToDisplay) {
       this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
           verticalPosition: 'top'});
    }
    
}