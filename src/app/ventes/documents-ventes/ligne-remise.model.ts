import { Article } from '../../stocks/articles/article.model';
import { BaseEntity } from '../../common/base-entity.model' ;
// type LibelleModePayement = 'Credit'| 'Credit_Espece' | 'Cheque' | 'Espece';
export class LigneRemise extends BaseEntity {
    constructor(
        public code?: number,
        public article?: Article,
        public quantite?: number,
        public remise?: number,
         public montantRemise?: number,
 
     ) {
        super();
         this.article = new Article();
         this.quantite = Number(0);
         this.remise = Number(0);
        this.montantRemise = Number(0);
    }
}
