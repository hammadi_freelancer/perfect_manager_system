import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {DocumentVente} from './document-vente.model';

import {DocumentVenteService} from './document-vente.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
// import { CommunicationService} from './communication.service';
import { MatDatepickerInput } from '@angular/material/datepicker';
import { MatSnackBar } from '@angular/material';
import { DocumentVenteFilter } from './document-vente.filter';
import {Client} from '../clients/client.model';
import {ArticleService} from '../../stocks/articles/article.service';
import {Article} from '../../stocks/articles/article.model';
import {Magasin} from '../../stocks/magasins/magasin.model';

import { MatExpansionPanel } from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
// import { DialogAddPayementFactureVentesComponent } from './dialog-add-payement-facture-ventes.component';
// import { DialogListPayementsFactureVentesComponent } from './dialog-list-payements-facture-ventes.component';
import {LigneMarchandise} from '../documents-ventes/ligne-marchandise.model';
import {LigneLogistique} from '../documents-ventes/ligne-logistique.model';
import {LigneCout} from '../documents-ventes/ligne-cout.model';

import {LigneOccupationMainOeuvre} from '../documents-ventes/ligne-occupation-main-oeuvre.model';
import {LigneRemise} from '../documents-ventes/ligne-remise.model';
import {LigneTaxe} from '../documents-ventes/ligne-taxe.model';


@Component({
  selector: 'app-document-vente-edit',
  templateUrl: './document-vente-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class DocumentVenteEditComponent implements OnInit  {

  private documentVente: DocumentVente =  new DocumentVente();
  
@Input()
private listDocumentsVentes: any = [] ;

@ViewChild('firstMatExpansionPanel')
private firstMatExpansionPanel : MatExpansionPanel;
private listCodesArticles: string[];
// private listTranches: Tranche[] = [] ;
private documentVenteFilter: DocumentVenteFilter;
private listClients: Client[];
private listArticles: Article[];
private listMagasins: Magasin[];


private etatsDocumentVentes: any[] = [
  {value: 'Initial', viewValue: 'Initial'},
  {value: 'Valide', viewValue: 'Validé'},
  {value: 'Regle', viewValue: 'Réglé'},
  {value: 'PartiellementRegle', viewValue: 'Partiellement Réglé'},
  {value: 'Annule', viewValue: 'Annulé'}
];
  constructor( private route: ActivatedRoute,
    private router: Router, private documentVenteService: DocumentVenteService,
    private articleService: ArticleService, private dialog: MatDialog,
    private matSnackBar: MatSnackBar, private elRef: ElementRef,
   ) {
  }
  ngOnInit() {
    this.listClients = this.route.snapshot.data.clients;
    this.listArticles = this.route.snapshot.data.articles;
    this.listMagasins = this.route.snapshot.data.magasins;
    this.listCodesArticles  = this.listArticles.map((article) => article.code);
    this.documentVenteFilter = new DocumentVenteFilter(this.listClients, this.listArticles);
   // this.factureVenteFilter = new FactureVenteFilter(this.listClients, undefined);
    const codeDocumentVente = this.route.snapshot.paramMap.get('codeDocumentVente');
    this.documentVenteService.getDocumentVente(codeDocumentVente).subscribe((documentVente) => {
             this.documentVente = documentVente;
             if (this.documentVente.etat === 'Valide' || this.documentVente.etat === 'Annule')
              {
                this.etatsDocumentVentes.splice(1, 1);
              }
              if (this.documentVente.etat === 'Initial')
                {
                  this.etatsDocumentVentes.splice(0, 1);
                }
           // this.generatePages();
            });
            this.firstMatExpansionPanel.open();
            
  }

  
 
  loadAddDocumentVenteComponent() {
    this.router.navigate(['/pms/ventes/ajouterDocumentVente']) ;
  }
  loadGridDocumentsVentes() {
    this.router.navigate(['/pms/ventes/documentsVentes']) ;
  }
  supprimerDocumentVente() {
      this.documentVenteService.deleteDocumentVente(this.documentVente.code).subscribe((response) => {

        this.openSnackBar( ' Document Supprimé');
        this.loadGridDocumentsVentes() ;
            });
  }
  
updateDocumentVente() {


  console.log(this.documentVente);
  this.documentVenteService.updateDocumentVente(this.documentVente).subscribe((response) => {
    if (response.error_message ===  undefined) {
      console.log('the response of update is ', response );
      this.openSnackBar( ' Document mis à jour ');
     
    } else {
      this.openSnackBar( ' Erreurs.. ');
     
    }
});
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

 selectedTabChanged($event) {
  if ($event.index === 4) { // Composition tab was selected
    // this.loadListArticles();
   } else if ($event.index === 1) {
   //  this.loadListSchemasConfiguration() ;
   }
  }

}
