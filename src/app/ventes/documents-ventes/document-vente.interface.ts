export interface DocumentVenteElement {
        code: string;
         numeroDocumentVente: string;
         dateVente: string;
         etat: string;
         cin: string;
         nom: string;
         prenom: string;
         raisonSociale: string;
         montantAPayer: number;
         montantTVA: number;
         montantRemise: number;
         montantCout: number;
         nbJoursOccupationMainOeuvre: number;
          nbMagasins: number;
          gestionMarchandises: boolean;
          gestionTaxes: boolean;
          gestionLogistique: boolean;
          gestionCouts: boolean;
          gestionMainOeuvres: boolean;
          gestionRemises: boolean;
        }
