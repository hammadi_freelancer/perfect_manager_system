import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,
    
      ViewChild } from '@angular/core';
    // import { ArticleService } from './article.service';
    import {DocumentVente} from './document-vente.model';
    
    import {DocumentVenteService} from './document-vente.service';
    import {FormControl} from '@angular/forms';
    import {map, startWith} from 'rxjs/operators';
    import {Observable} from 'rxjs';
    import { Router, ActivatedRoute, ParamMap } from '@angular/router';
    // import {ActionData} from './action-data.interface';
    import { ClientService } from '../clients/client.service';
    import {Client} from '../clients/client.model';
    import {LigneMarchandise} from './ligne-marchandise.model';
    
    import {MatSnackBar} from '@angular/material';
    import { MatExpansionPanel } from '@angular/material';
    import {ArticleService} from '../../stocks/articles/article.service';
    import {Article} from '../../stocks/articles/article.model';
    import {ModePayementLibelle} from './mode-payement-libelle.interface';
    
    type LibelleModePayement = 'Credit'| 'Credit_Espece' | 'Cheque' | 'Espece';
    
    @Component({
      selector: 'app-gestion-marchandises',
      templateUrl: './gestion-marchandises.component.html',
      // styleUrls: ['./players.component.css']
    })
    export class GestionMarchandisesComponent implements OnInit {
    
     @Input()
     private documentVente: DocumentVente =  new DocumentVente();
    
     @Input()
     private mode: boolean;

     @Input()
     private listArticles: Article[];
     

    @Output()
    save: EventEmitter<DocumentVente> = new EventEmitter<DocumentVente>();
    
    
    private listClients: Client[];
    private listCodesArticles: string[];
    private lignesMarchandises: LigneMarchandise[] = [ ];
    private  pagesLignesMarchandises: LigneMarchandise[][] = [ ];
   //  private  pagesReadOnlyLignesFacures: boolean[][] = [ ];
    // private  pagesEditModeLignesFacures: boolean[][] = [ ];
    
    private currentPage = 1;
    // private editLignesMode: boolean [] = [];
    // private readOnlyLignes: boolean [] = [];
    
  
    
      constructor( private route: ActivatedRoute,
        private router: Router, private documentVenteService: DocumentVenteService, private clientService: ClientService,
        private articleService: ArticleService,
       private matSnackBar: MatSnackBar, private elRef: ElementRef) {
      }
    
    
      // myControl = new FormControl();
      // options: string[] = ['One', 'Two', 'Three'];
      // filteredOptions: Observable<string[]>;
    
      ngOnInit() {
    
       // this.listClients = this.route.snapshot.data.clients;
       // this.listArticles = this.route.snapshot.data.articles;
       this.listCodesArticles  = this.listArticles.map((article) => article.code);
      //  this.documentVenteFilter = new DocumentVenteFilter(this.listClients, this.listArticles);
       // this.firstMatExpansionPanel.open();
      this.initializeListLignesMarchandises();
      this.lignesMarchandises.forEach(function(ligne, index) {
                     ligne.code = index + 1;
      });
      this.pagesLignesMarchandises[this.currentPage - 1] = this.lignesMarchandises;
      
      }
      private initializeListLignesMarchandises() {
        this.lignesMarchandises = [];
       //  this.editLignesMode = [];
        // this.readOnlyLignes = [];
        this.lignesMarchandises.push(new LigneMarchandise());
        this.lignesMarchandises.push(new LigneMarchandise());
        this.lignesMarchandises.push(new LigneMarchandise());
        this.lignesMarchandises.push(new LigneMarchandise());
        this.lignesMarchandises.push(new LigneMarchandise());
       //  this.editLignesMode.push(false);
       //  this.editLignesMode.push(false);
        // this.editLignesMode.push(false);
        // this.editLignesMode.push(false);
        // this.editLignesMode.push(false);
    
        // this.readOnlyLignes.push(false);
        // this.readOnlyLignes.push(false);
        // this.readOnlyLignes.push(false);
        // this.readOnlyLignes.push(false);
        // this.readOnlyLignes.push(false);
      }
     /* constructClassName(i)  {
        return  'designationDivs_' + i;
       //  this.elRef.nativeElement.querySelector('.designationDivs_' + i )  ;
      }*/
   
      deleteRow(i)
      {
        
    
         //  this.documentVente.montantAPayer = this.documentVente.montantTTC ;
          console.log('after delete');
          // console.log( this.factureVente.montantHT);
          // console.log( this.factureVente.montantTTC);
          this.lignesMarchandises.splice(i, 1 );
          // this.editLignesMode.splice(i, 1); 
          // this.readOnlyLignes.splice(i, 1); 
          
          this.lignesMarchandises.push(new LigneMarchandise());
         //  this.editLignesMode.push(false);
        //   this.readOnlyLignes.push(false);
          
             
      }
  
      validateRow(i) {
        // this.factureVente.montantHT = +this.factureVente.montantHT + this.lignesFactures[i].montantHT ;
         this.documentVente.montantAPayer = +this.documentVente.montantAPayer + this.lignesMarchandises[i].montantAPayer ;
         // this.factureVente.montantTTC = +this.factureVente.montantTTC  + this.lignesFactures[i].montantTTC ;
         // this.factureVente.montantRemise = +this.factureVente.montantRemise  + this.lignesFactures[i].montantRemise ;
         // this.factureVente.montantAPayer = this.factureVente.montantTTC ;
        //  this.factureVente.montantAPayerApresRemise = +this.factureVente.montantAPayer - this.factureVente.montantRemise;
         // this.editLignesMode[i] = true;
         // this.readOnlyLignes[i] = true;
       
       
        }
      
   
      nextPage()
      {
        this.pagesLignesMarchandises[this.currentPage - 1] = this.lignesMarchandises;
    
        // this.pagesReadOnlyLignesFacures[this.currentPage - 1] = this.readOnlyLignes;
        // this.pagesEditModeLignesFacures[this.currentPage - 1] = this.editLignesMode;
        
        this.currentPage++;
        if ( this.pagesLignesMarchandises[this.currentPage - 1] === undefined) {
        this.initializeListLignesMarchandises();
        this.lignesMarchandises.forEach(function(ligne, index) {
          console.log('number page');
          console.log(index + ((this.currentPage * this.currentPage) + 1));
          ligne.code = index + ((this.currentPage * this.currentPage) + 1);
    }, this);
           console.log('Next Page lignes Factures ');
              // console.log(this.lignesFactures);
         this.pagesLignesMarchandises[this.currentPage - 1 ] = this.lignesMarchandises;
         // this.pagesReadOnlyLignesFacures[this.currentPage - 1] = this.readOnlyLignes;
         // this.pagesEditModeLignesFacures[this.currentPage - 1] = this.editLignesMode;
         
         
        } else {
          console.log('Next Page lignes Factures ');
           this.lignesMarchandises = this.pagesLignesMarchandises[this.currentPage - 1 ];
          //  this.readOnlyLignes = this.pagesReadOnlyLignesFacures[this.currentPage - 1 ];
         //   this.editLignesMode = this.pagesEditModeLignesFacures[this.currentPage - 1 ];
        }
        const  button: HTMLButtonElement = this.elRef.nativeElement.querySelector('.paginationLabel' )  ;
        button.innerText = 'Page ' + this.currentPage + ' / ' + this.pagesLignesMarchandises.length;
      }
    
      previousPage()
      {
        this.pagesLignesMarchandises[this.currentPage - 1] = this.lignesMarchandises ;
       //  this.pagesReadOnlyLignesFacures[this.currentPage - 1] = this.readOnlyLignes;
      //   this.pagesEditModeLignesFacures[this.currentPage - 1] = this.editLignesMode;
        
        this.currentPage--;
       this.lignesMarchandises =  this.pagesLignesMarchandises[this.currentPage - 1];
      //  this.readOnlyLignes = this.pagesReadOnlyLignesFacures[this.currentPage - 1];
       // this.editLignesMode = this.pagesEditModeLignesFacures[this.currentPage - 1];
       
       // this.factureVenteFilter = new FactureVenteFilter(this.listClients, this.listArticles);
       const  button: HTMLButtonElement = this.elRef.nativeElement.querySelector('.paginationLabel' )  ;
       button.innerText = 'Page ' + this.currentPage + ' / ' + this.pagesLignesMarchandises.length;
    
      }
      selectedArticleCode(i, $event) {
         console.log(i);
         console.log($event);
         this.lignesMarchandises[i].article.code = $event.option.value;
         this.lignesMarchandises[i].article.libelle  = this.listArticles.find((article) => (article.code === $event.option.value)).libelle;
        
         const  div  : HTMLDivElement = this.elRef.nativeElement.querySelector('.designationDivs_' + i )  ;
         div.innerText = this.lignesMarchandises[i].article.libelle;
        // console.log(div);
    
    
      }
      fillDataLignesMarchandises()
      {
        this.documentVente.listLignesMarchandises = [];
         this.pagesLignesMarchandises.forEach(function(page, index) {
           page.forEach(function(ligne, i) {
               if ( ligne.montantAPayer !== 0 ) {
                this.documentVente.listLignesMarchandises.push(ligne);
               }
           }, this
         );
         }, this);
      }
      openSnackBar(messageToDisplay) {
       this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
           verticalPosition: 'top'});
    }
}
