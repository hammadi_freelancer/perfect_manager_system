
type LibelleModePayement = 'Credit'| 'Credit_Espece' | 'Cheque' | 'Espece';
export class PayementData {
    constructor(public libelle?: LibelleModePayement,
        public numeroCheque?: string, public dateCheque?: string, public aReporter?: number,
        public aCompte?: number,
         public chequeData?: string,
        public proprietaireCheque?: string,
         public montantCheque?: number,
        public montantEspece?: number,
        public responsablePayement?: string ,
        public dateOperation?: string
     ) {
         this.aReporter =  Number(0);
         this.aCompte = Number(0);
         this.montantEspece = Number(0);
         this.montantCheque = Number(0);
    }
}
