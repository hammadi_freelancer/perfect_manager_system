import { Article } from '../../stocks/articles/article.model';
import { Magasin } from '../../stocks/magasins/magasin.model';

import { BaseEntity } from '../../common/base-entity.model' ;
// type LibelleModePayement = 'Credit'| 'Credit_Espece' | 'Cheque' | 'Espece';
export class LigneLogistique extends BaseEntity {
    constructor(
        public code?: number,
        public article?: Article,
        public quantite?: number,
        public magasin?: Magasin,
        public lot?: string,
 
     ) {
        super();
         this.article = new Article();
         this.quantite = Number(0);
         this.magasin = new Magasin();
    }
}
