import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {DocumentVente} from './document-vente.model';

import {DocumentVenteService} from './document-vente.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
// import {ActionData} from './action-data.interface';
import { ClientService } from '../clients/client.service';
 import { DocumentVenteFilter } from './document-vente.filter';
import {Client} from '../clients/client.model';
import {LigneMarchandise} from '../documents-ventes/ligne-marchandise.model';
import {LigneLogistique} from '../documents-ventes/ligne-logistique.model';
import {LigneCout} from '../documents-ventes/ligne-cout.model';

import {LigneOccupationMainOeuvre} from '../documents-ventes/ligne-occupation-main-oeuvre.model';
import {LigneRemise} from '../documents-ventes/ligne-remise.model';
import {LigneTaxe} from '../documents-ventes/ligne-taxe.model';
import {MatSnackBar} from '@angular/material';
import { MatExpansionPanel } from '@angular/material';
import {ArticleService} from '../../stocks/articles/article.service';
import {Article} from '../../stocks/articles/article.model';
import {ModePayementLibelle} from './mode-payement-libelle.interface';

type LibelleModePayement = 'Credit'| 'Credit_Espece' | 'Cheque' | 'Espece';

@Component({
  selector: 'app-document-vente-new',
  templateUrl: './document-vente-new.component.html',
  // styleUrls: ['./players.component.css']
})
export class DocumentVentesNewComponent implements OnInit {

 private documentVente: DocumentVente =  new DocumentVente();

@Input()
private listDocumentsVentes: any = [] ;

// @Output()
// save: EventEmitter<FactureVente> = new EventEmitter<FactureVente>();

 @ViewChild('firstMatExpansionPanel')
 private firstMatExpansionPanel : MatExpansionPanel;

 private documentVenteFilter: DocumentVenteFilter;
private listClients: Client[];
private listArticles: Article[];
private listCodesArticles: string[];
private lignesMarchandises: LigneMarchandise[] = [ ];
private lignesLogistiques: LigneLogistique[] = [ ];
private lignesCouts: LigneCout[] = [ ];
private lignesRemises: LigneRemise[] = [ ];
private lignesTaxes: LigneTaxe[] = [ ];

// private  pagesLignesFacures: LigneFacture[][] = [ ];
// private  pagesReadOnlyLignesFacures: boolean[][] = [ ];
// private  pagesEditModeLignesFacures: boolean[][] = [ ];

// private currentPage = 1;
// private editLignesMode: boolean [] = [];
// private readOnlyLignes: boolean [] = [];

private payementModes: ModePayementLibelle[] = [
  {value: 'Espece', viewValue: 'Espèce'},
  {value: 'Cheque', viewValue: 'Chèque'},
  {value: 'Credit', viewValue: 'Crédit'},
  {value: 'Credit_Espece', viewValue: 'Crédit/Espèce'}
];
private etatsDocumentVentes: any[] = [
  {value: 'Initial', viewValue: 'Initial'},
  {value: 'Valide', viewValue: 'Validé'}
];

  constructor( private route: ActivatedRoute,
    private router: Router, private documentVenteService: DocumentVenteService, private clientService: ClientService,
    private articleService: ArticleService,
   private matSnackBar: MatSnackBar, private elRef: ElementRef) {
  }


  // myControl = new FormControl();
  // options: string[] = ['One', 'Two', 'Three'];
  // filteredOptions: Observable<string[]>;

  ngOnInit() {

   this.listClients = this.route.snapshot.data.clients;
   this.listArticles = this.route.snapshot.data.articles;
   this.listCodesArticles  = this.listArticles.map((article) => article.code);
   this.documentVenteFilter = new DocumentVenteFilter(this.listClients, this.listArticles);
   this.firstMatExpansionPanel.open();
   // this.firstMatExpansionPanel.open();
  // this.initializeListLignesFactures();
 // this.lignesFact.forEach(function(ligne, index) {
               //   ligne.code = index + 1;
 //  });
  // this.pagesLignesFacures[this.currentPage - 1] = this.lignesFactures;

  }
  saveDocumentVente() {

 
      if (this.documentVente.etat === '' || this.documentVente.etat === undefined) {
        this.documentVente.etat = 'Initial';
      }
     // this.fillDataLignesFactures();
    console.log(this.documentVente);
    // this.saveEnCours = true;
    this.documentVenteService.addDocumentVente(this.documentVente).subscribe((response) => {
        if (response.error_message ===  undefined) {
          console.log('undefined error message');
          // this.save.emit(response);
          this.documentVente = new  DocumentVente();
         //  this.documentVente.listLignesFactures = [];
          this.openSnackBar(  'Document Enregistré');
        } else {
         // show error message
         this.openSnackBar(  'Erreurs..');
         
        }
    });
  }
  fillDataNomPrenom()
  {
    const listFiltred  = this.listClients.filter((client) => (client.cin === this.documentVente.client.cin));
    this.documentVente.client.nom = listFiltred[0].nom;
    this.documentVente.client.prenom = listFiltred[0].prenom;
  }
  fillDataMatriculeRaison()
  {
    const listFiltred  = this.listClients.filter((client) => (client.registreCommerce === this.documentVente.client.registreCommerce));
    this.documentVente.client.matriculeFiscale = listFiltred[0].matriculeFiscale;
    this.documentVente.client.raisonSociale = listFiltred[0].raisonSociale;
  }
  vider() {
    this.documentVente =  new DocumentVente();
  }
  openSnackBar(messageToDisplay) {
   this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
       verticalPosition: 'top'});
  }
loadGridDocumentsVentes() {
  this.router.navigate(['/pms/ventes/documentsVentes']) ;
}


}
