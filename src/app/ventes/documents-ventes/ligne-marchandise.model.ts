import { Article } from '../../stocks/articles/article.model';
import { BaseEntity } from '../../common/base-entity.model' ;
// type LibelleModePayement = 'Credit'| 'Credit_Espece' | 'Cheque' | 'Espece';
export class LigneMarchandise extends BaseEntity {
    constructor(
        public code?: number,
        public article?: Article,
        public quantite?: number,
        public prixUnitaire?: number,
         public montantAPayer?: number
     ) {
        super();
         this.article = new Article();
         this.montantAPayer =  Number(0);

    }
}
