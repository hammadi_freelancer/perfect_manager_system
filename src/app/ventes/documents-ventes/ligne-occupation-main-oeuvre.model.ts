import { Article } from '../../stocks/articles/article.model';

import { BaseEntity } from '../../common/base-entity.model' ;
// type LibelleModePayement = 'Credit'| 'Credit_Espece' | 'Cheque' | 'Espece';
export class LigneOccupationMainOeuvre extends BaseEntity {
    constructor(
        public code?: number,
        public article?: Article,
        public quantite?: number,
        public employe?: string,
         public nbHeures?: number,
         public nbJours?: number,
     ) {
        super();
         this.article = new Article();
         this.nbHeures =  Number(0);

    }
}
