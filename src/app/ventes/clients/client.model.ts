

import { BaseEntity } from '../../common/base-entity.model' ;
import { Personne } from '../../common/personne.interface' ;
// import { Adresse } from '../../common/personne.interface' ;

type Etat = 'Initial' | 'Valide' | 'Annule' | ''

export class Client extends BaseEntity {
   constructor(public code?: string,
      public numeroClient?: string,
     public nom?: string, public prenom?: string, public adresse?: string,
    public mobile?: string, public email?: string, public siteWeb?: string,
public cin?: string, public ville?: string, public codePostale?: string , public gouvernorat?: string,
 public fidele?: boolean, public image?: any,
 public raisonSociale?: string, public matriculeFiscale?: string, public registreCommerce?: string,
 public etat?: Etat
) {
    super();
   //  this.categoryClient = 'PERSONNE_PHYSIQUE'; // other possible value : PERSONNE_MORALE
    // this.checked = false;
    this.image = undefined;
}
}
