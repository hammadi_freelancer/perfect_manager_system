export class ClientDataFilter
{
     numeroClient: string;
     nom: string;
     prenom: string;
     cin: string;
     raisonSociale : string;
     matriculeFiscale : string;
     registreCommerce : string;
     mobile : string;
     adresse : string;
     codePostale : string;
     ville : string;
     gouvernorat : string;
}




