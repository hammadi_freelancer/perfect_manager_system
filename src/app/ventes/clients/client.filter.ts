
import { BaseEntity } from '../../common/base-entity.model' ;
// import { Tranche } from './tranche.model' ;
import { Client } from '../../ventes/clients/client.model' ;
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
// import { Service } from '../../stocks/articles/article.service';

export const MODES_GENERATION_TRANCHES = ['AUTOMATIQUE', 'MANUELLE', ];

export class ClientFilter  {
    public selectedCin: string;
    public selectedCode: string;
    
    public selectedNom: string;
    public selectedPrenom: string;
    public selectedRaisonSociale: string;
    public selectedMatriculeFiscale: string;
    
    public selectedRegistreCommerce: string;
    public selectedAdresse: string;
    private controlNoms = new FormControl();
    private controlPrenoms = new FormControl();
    private controlCins = new FormControl();
    private controlRaisonSociale = new FormControl();
    private controlMatriculeFiscale = new FormControl();
    private controlRegistreCommerce = new FormControl();
    
   //  private controlAdresse = new FormControl();
    private listNoms: string[] = [];
    private listPrenoms: string[] = [];
    private listCins: string[] = [];
    private listRaisonsSociales: string[] = [];
    private listMatriculesFiscales: string[] = [];
    
   //  private listAdresses: string[] = [];
    private listRegistreCommerce: string [] = [];
    private filteredNoms: Observable<string[]>;
    private filteredPrenoms: Observable<string[]>;
    private filteredCins: Observable<string[]>;
 
    
   //  private filteredAdresses: Observable<string[]>;
    private filteredRaisonsSociales: Observable<string[]>;
    private filteredMatriculesFiscales: Observable<string[]>;
    private filteredRegistresCommerces: Observable<string[]>;
    
    constructor (clients: Client[]) {
      if (clients !== null) {
       //  const listClientsMorals = clients.filter((client) => (client.categoryClient === 'PERSONNE_MORALE'));
        // const listClientsPhysiques = clients.filter((client) => (client.categoryClient === 'PERSONNE_PHYSIQUE'));
        this.listCins = clients.filter((client) =>
        (client.cin !== undefined && client.cin != null )).map((client) => (client.cin ));
        this.listNoms = clients.filter((client) =>
        (client.nom !== undefined && client.nom != null )).map((client) => (client.nom ));

        // this.listNoms = clients.map((client) => (client.nom));
        this.listPrenoms = clients.filter((client) =>
        (client.prenom !== undefined && client.prenom != null )).map((client) => (client.prenom ));
       // this.listPrenoms = clients.map((client) => (client.prenom));
        // this.listRaisonsSociales = clients.map((client) => (client.raisonSociale));
        // this.listMatriculesFiscales = clients.map((client) => (client.matriculeFiscale));
        // this.listRegistreCommerce = clients.filter((client) =>
         this.listRaisonsSociales = clients.filter((client) =>
         (client.raisonSociale !== undefined && client.raisonSociale != null )).map((client) => (client.raisonSociale ));

         this.listMatriculesFiscales = clients.filter((client) =>
         (client.matriculeFiscale !== undefined && client.matriculeFiscale != null )).map((client) => (client.matriculeFiscale ));

         this.listRegistreCommerce = clients.filter((client) =>
         (client.registreCommerce !== undefined && client.registreCommerce != null )).map((client) => (client.registreCommerce ));
    

      this.filteredNoms = this.controlNoms.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterNoms(value))
        );
        this.filteredPrenoms = this.controlPrenoms.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterPrenoms(value))
        );
        this.filteredCins = this.controlCins.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterCins(value))
        );
        this.filteredMatriculesFiscales = this.controlMatriculeFiscale.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterMatriculesFiscales(value))
        );
        this.filteredRaisonsSociales = this.controlRaisonSociale.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterRaisonsSociales(value))
        );
        this.filteredRegistresCommerces = this.controlRegistreCommerce.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterRegistresCommerces(value))
        );
     

    }

  }
    private _filterNoms(value: string): string[] {
      let  filterValue = '';
      if (value !== undefined) {
        filterValue = value.toLowerCase();
      }
        return this.listNoms.filter(nom => nom.toLowerCase().includes(filterValue));
      }
      private _filterCins(value: string): string[] {
        let  filterValue = '';
        if (value !== undefined) {
          filterValue = value.toLowerCase();
        }
        return this.listCins.filter(cin => cin.toLowerCase().includes(filterValue));
      }
      private _filterPrenoms(value: string): string[] {
        let  filterValue = '';
        if (value !== undefined) {
          filterValue = value.toLowerCase();
        }
        return this.listPrenoms.filter(prenom => prenom.toLowerCase().includes(filterValue));
      }
      private _filterRaisonsSociales(value: string): string[] {
        let  filterValue = '';
        if (value !== undefined) {
          filterValue = value.toLowerCase();
        }
        return this.listRaisonsSociales.filter(raison => raison.toLowerCase().includes(filterValue));
      }
      private _filterMatriculesFiscales(value: string): string[] {
        let  filterValue = '';
        if (value !== undefined) {
          filterValue = value.toLowerCase();
        }
        return this.listMatriculesFiscales.filter(matricule => matricule.toLowerCase().includes(filterValue));
      }

   
      private _filterRegistresCommerces(value: string): string[] {
        let  filterValue = '';
        if (value !== undefined) {
          filterValue = value.toLowerCase();
        }
        return this.listRegistreCommerce.filter((registre) => {
          if (registre !== undefined ) {
            return  registre.toLowerCase().includes(filterValue) ;
        } else  {
          return true;
        }
      });
         //  }
      }
      

      public getControlPrenoms(): FormControl {
        return this.controlPrenoms;
      }
      public getControlNoms(): FormControl {
        return this.controlNoms;
      }
      public getControlCins(): FormControl {
        return this.controlCins;
      }
 
      public getControlRaisonSociale(): FormControl {
        return this.controlRaisonSociale;
      }
      public getControlMatriculeFiscale(): FormControl {
        return this.controlMatriculeFiscale;
      }
      public getControlRegistreCommerce(): FormControl {
        return this.controlRegistreCommerce;
      }
      public getFiltredPrenoms(): Observable<string[]> {
        return this.filteredPrenoms;
      }
      public getFiltredNoms(): Observable<string[]> {
        return this.filteredNoms;
      }
      public getFiltredCins(): Observable<string[]> {
        return this.filteredCins;
      }
      public getFiltredRaisonsSociales(): Observable<string[]> {
        return this.filteredRaisonsSociales;
      }
      public getFiltredMatriculesFiscales(): Observable<string[]> {
        return this.filteredMatriculesFiscales;
      }
     /* public getFiltredAdresses(): Observable<string[]> {
        return this.filteredAdresses;
      }*/
      public getFiltredRegistreCommerce(): Observable<string[]> {

        return this.filteredRegistresCommerces;
      }
}





