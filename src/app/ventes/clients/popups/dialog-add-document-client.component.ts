
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ClientService} from '../client.service';
import {DocumentClient} from '../document-client.model';

@Component({
    selector: 'app-dialog-add-document-client',
    templateUrl: 'dialog-add-document-client.component.html',
  })
  export class DialogAddDocumentClientComponent implements OnInit {
     private documentClient: DocumentClient;
     private saveEnCours = false;
     private typesDoc: any[] = [
      {value: 'CIN', viewValue: 'Carte Identité'},
      {value: 'Passport', viewValue: 'Passport'},
      {value: 'RegistreCommerce', viewValue: 'Registre de Commerce'},
      {value: 'RIB', viewValue: 'RIB'},
      {value: 'AttestationTravail', viewValue: 'Attestation de Travail'},
      {value: 'ExtraitNaissance', viewValue: 'Extrait de Naissance'},
      {value: 'ReleveCompte', viewValue: 'Relevée de Compte'},
      {value: 'CertificatRésidence', viewValue: 'Certificat de Résidence'},
      {value: 'Cheque', viewValue: 'Chèque'},
      {value: 'Traite', viewValue: 'Traite'},
      {value: 'Autre', viewValue: 'Autre'},

    ];
     constructor(  private clientService: ClientService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.documentClient = new DocumentClient();
    }
    saveDocumentClient() 
    {
      this.documentClient.codeClient = this.data.codeClient;
       console.log(this.documentClient);
        this.saveEnCours = true;
       this.clientService.addDocumentClient(this.documentClient).subscribe((response) => {
         this.saveEnCours = false;
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.documentClient = new DocumentClient();
             this.openSnackBar(  'Document Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
     imageFace1Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentClient.imageFace1 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imageFace2Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentClient.imageFace2 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
  }
