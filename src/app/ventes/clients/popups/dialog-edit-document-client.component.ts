
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ClientService} from '../client.service';
import {DocumentClient} from '../document-client.model';

@Component({
    selector: 'app-dialog-edit-document-client',
    templateUrl: 'dialog-edit-document-client.component.html',
  })
  export class DialogEditDocumentClientComponent implements OnInit {
     private documentClient: DocumentClient;
     private updateEnCours = false;
     private typesDoc: any[] = [
      {value: 'CIN', viewValue: 'Carte Identité'},
      {value: 'Passport', viewValue: 'Passport'},
      {value: 'RegistreCommerce', viewValue: 'Registre de Commerce'},
      {value: 'RIB', viewValue: 'RIB'},
      {value: 'AttestationTravail', viewValue: 'Attestation de Travail'},
      {value: 'ExtraitNaissance', viewValue: 'Extrait de Naissance'},
      {value: 'ReleveCompte', viewValue: 'Relevée de Compte'},
      {value: 'CertificatRésidence', viewValue: 'Certificat de Résidence'},
      {value: 'Cheque', viewValue: 'Chèque'},
      {value: 'Traite', viewValue: 'Traite'},
      {value: 'Autre', viewValue: 'Autre'},

    ];
     constructor(  private clientService: ClientService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {

        this.clientService.getDocumentClient(this.data.codeDocumentClient).subscribe((doc) => {
            this.documentClient = doc;
     });
    }
    
    updateDocumentClient() 
    {
      this.updateEnCours = true;
       this.clientService.updateDocumentClient(this.documentClient).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.openSnackBar(  'Document Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
     imageFace1Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentClient.imageFace1 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imageFace2Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentClient.imageFace2 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
  }
