
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ClientService} from '../client.service';
import {DocumentClient} from '../document-client.model';
import { DocumentClientElement } from '../document-client.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-documents-clients',
    templateUrl: 'dialog-list-documents-clients.component.html',
  })
  export class DialogListDocumentsClientsComponent implements OnInit {
     private listDocumentsClients: DocumentClient[] = [];
     private  dataDocumentsClientsSource: MatTableDataSource<DocumentClientElement>;
     private selection = new SelectionModel<DocumentClientElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private clientService: ClientService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      console.log('code credit in dialog component is :', this.data.codeClient);
      this.clientService.getDocumentsClientsByCodeClient(this.data.codeClient).subscribe((listDocumentsClients) => {
        this.listDocumentsClients = listDocumentsClients;
// console.log('recieving data from backend', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedDocumentsClients: DocumentClient[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentClElement) => {
    return documentClElement.code;
  });
  this.listDocumentsClients.forEach(documentClient => {
    if (codes.findIndex(code => code === documentClient.code) > -1) {
      listSelectedDocumentsClients.push(documentClient);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsClients !== undefined) {

      this.listDocumentsClients.forEach(docClient => {
             listElements.push( {code: docClient.code ,
          dateReception: docClient.dateReception,
          numeroDocument: docClient.numeroDocument,
          typeDocument: docClient.typeDocument,
          description: docClient.description
        } );
      });
    }
      this.dataDocumentsClientsSource = new MatTableDataSource<DocumentClientElement>(listElements);
      this.dataDocumentsClientsSource.sort = this.sort;
      this.dataDocumentsClientsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsClientsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsClientsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsClientsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsClientsSource.paginator) {
      this.dataDocumentsClientsSource.paginator.firstPage();
    }
  }

}
