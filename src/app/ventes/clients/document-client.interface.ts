


export interface DocumentClientElement {
         code: string ;
         dateReception: string ;
         numeroDocument: number;
         typeDocument?: string;
          description?: string;
}
