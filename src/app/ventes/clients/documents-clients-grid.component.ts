
import { Component, OnInit, Inject,Input, ViewChild, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {ClientService} from './client.service';
import {Client} from './client.model';

import {DocumentClient} from './document-client.model';
import { DocumentClientElement } from './document-client.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogAddDocumentClientComponent } from './popups/dialog-add-document-client.component';
import { DialogEditDocumentClientComponent } from './popups/dialog-edit-document-client.component';
@Component({
    selector: 'app-documents-clients-grid',
    templateUrl: 'documents-clients-grid.component.html',
  })
  export class DocumentsClientsGridComponent implements OnInit, OnChanges {
     private listDocumentsClients: DocumentClient[] = [];
     private  dataDocumentsClientsSource: MatTableDataSource<DocumentClientElement>;
     private selection = new SelectionModel<DocumentClientElement>(true, []);
    
     @Input()
     private client : Client

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private clientService: ClientService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.clientService.getDocumentsClientsByCodeClient(this.client.code).subscribe((listDocumentsClients) => {
        this.listDocumentsClients = listDocumentsClients;
  // console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
  }
  ngOnChanges()
  {
    this.specifyListColumnsToBeDisplayed();
    
    this.clientService.getDocumentsClientsByCodeClient(this.client.code).subscribe((listDocumentsClients) => {
      this.listDocumentsClients = listDocumentsClients;
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }

  checkOneActionInvoked() {
    const listSelectedDocumentsClients: DocumentClient[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentCLElement) => {
    return documentCLElement.code;
  });
  this.listDocumentsClients.forEach(documentClient => {
    if (codes.findIndex(code => code === documentClient.code) > -1) {
      listSelectedDocumentsClients.push(documentClient);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsClients !== undefined) {

      this.listDocumentsClients.forEach(docCL => {
             listElements.push( {code: docCL.code ,
          dateReception: docCL.dateReception,
          numeroDocument: docCL.numeroDocument,
          typeDocument: docCL.typeDocument,
          description: docCL.description
        } );
      });
    }
      this.dataDocumentsClientsSource = new MatTableDataSource<DocumentClientElement>(listElements);
      this.dataDocumentsClientsSource.sort = this.sort;
      this.dataDocumentsClientsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    //  console.log('calling specifyListColumnsToBeDisplayed documents grid');
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
    
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
      console.log(this.columnsNames);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsClientsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsClientsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsClientsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsClientsSource.paginator) {
      this.dataDocumentsClientsSource.paginator.firstPage();
    }
  }
  showAddDocumentDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeClient : this.client.code
   };
 
   const dialogRef = this.dialog.open(DialogAddDocumentClientComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(docAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditDocumentDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeDocumentClient : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditDocumentClientComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(docAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Document Séléctionnée!');
    }
    }
    supprimerDocuments()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(doc, index) {
              this.clientService.deleteDocumentClient(doc.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( this.selection.selected.length + ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Document Séléctionnée!');
        }
    }
    refresh() {
      this.clientService.getDocumentsClientsByCodeClient(this.client.code).subscribe((listDocsClients) => {
        this.listDocumentsClients = listDocsClients;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
