import { Component, OnInit, Output, ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {Client} from './client.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {ClientService} from './client.service';
import { ActionData } from './action-data.interface';
// import { Fournisseur } from '../../ventes/clients/client.model';
import { ClientElement } from './client.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import { AlertMessage } from '../../common/alert-message.interface';
import { OperationRequest } from '../../common/operation-request.interface';
import { OperationResponse } from '../../common/operation-response.interface';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
// import { DialogAddDocumentClientComponent} from './popups';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {ClientDataFilter } from './client-data.filter';
import { SelectItem } from 'primeng/api';
@Component({
  selector: 'app-clients-grid',
  templateUrl: './clients-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class ClientsGridComponent implements OnInit {

  private client: Client =  new Client();
  private showFilter = false;
  private showSelectColumnsMultiSelect = false;
  private clientDataFilter = new ClientDataFilter();
  private selection = new SelectionModel<Client>(true, []);
  
@Output()
select: EventEmitter<Client[]> = new EventEmitter<Client[]>();

//private lastClientAdded: Client;

 selectedClient: Client ;

private deleteEnCours = false;

 @Input()
 private actionData: ActionData;

 @Input()
 private listClients: any = [] ;
 listClientsOrgin: any = [];
 private checkedAll: false;
 private showParticulier = false;
 private showEntreprise = false;
 private  dataClientsSource: MatTableDataSource<ClientElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay :ColumnDefinition[] = [];

 private columnsToSelect : SelectItem[];
  private  selectedColumnsDefinitions: string[];

 private selectedColumnsNames: string[];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
  constructor( private route: ActivatedRoute, private clientsService: ClientService,
    private router: Router, private matSnackBar: MatSnackBar, private dialog: MatDialog ) {
  }
  ngOnInit() {
    this.defaultColumnsDefinitions = [
      {name: 'numeroClient' , displayTitle: 'N°Client'},
    {name: 'cin' , displayTitle: 'CIN'},
    {name: 'nom' , displayTitle: 'Nom'},
     {name: 'prenom' , displayTitle: 'Prénom'},
    {name: 'raisonSociale' , displayTitle: 'Raison Soc.'},
    {name: 'matriculeFiscale' , displayTitle: 'Mat.Fiscale'},
    {name: 'registreCommerce' , displayTitle: 'R.Commerce'},
    {name: 'adresse' , displayTitle: 'Adresse'},
    {name: 'mobile' , displayTitle: 'Mobile'},
    {name: 'description' , displayTitle: 'Déscr.'},
  ];
    
    this.columnsToSelect = [
      {label: 'N°Client', value: 'numeroClient', title: 'N°Client'},
      {label: 'CIN', value: 'cin', title: 'CIN'},
      {label: 'Nom', value: 'nom', title: 'Nom'},
      {label: 'Prénom', value: 'prenom', title: 'Prénom'},
      {label: 'R.Sociale', value: 'raisonSociale', title: 'R.Sociale'},
      {label: 'Matr.Fiscale', value: 'matriculeFiscale', title: 'Matr. Fiscale'},
      {label: 'R.Commerce', value: 'registreCommerce', title: 'R.Commerce'},
      {label: 'Adresse', value: 'adresse', title: 'Adresse'},
      {label: 'Mobile', value: 'mobile', title: 'Mobile'},
      {label: 'Déscription', value: 'description', title: 'Déscription'},
      
  ];
  this.selectedColumnsDefinitions = [
    'numeroClient',
   'cin',
   'nom' ,
   'prenom', 
   'mobile'
    ];


    this.clientsService.getPageClients(0, 5,null).subscribe((clients) => {
      this.listClients = clients;
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });
  

  }
  deleteClient(client) {
      console.log('call delete !', client );
    this.clientsService.deleteClient(client.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData(null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.clientsService.getPageClients(0, 5, filter).subscribe((clients) => {
    this.listClients = clients;
    // this.listClients[this.listClients.length] = this.lastClientAdded;
    this.listClientsOrgin = this.listClients;
    this.transformDataToByConsumedByMatTable();
   });
  // this.listClientsOrgin = this.listClients ;
}


checkOneActionInvoked() {
  const listSelectedClients: Client[] = [];
console.log('selected clients are ');
console.log(this.selection.selected);
if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((clientElement) => {
  return clientElement.code;
});
this.listClients.forEach(client => {
  if (codes.findIndex(code => code === client.code) > -1) {
    listSelectedClients.push(client);
  }
  });
}
this.select.emit(listSelectedClients);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listClients !== undefined) {
    this.listClients.forEach(client => {
      listElements.push( {code: client.code ,
        numeroClient: client.numeroClient,
        cin: client.cin, nom: client.nom, prenom: client.prenom,
        matriculeFiscale: client.matriculeFiscale, raisonSociale: client.raisonSociale ,
         
        adresse: client.adresse,   mobile: client.mobile} );
    });
  }
    this.dataClientsSource = new MatTableDataSource<ClientElement>(listElements);
    this.dataClientsSource.sort = this.sort;
    this.dataClientsSource.paginator = this.paginator;
    this.clientsService.getTotalClients(this.clientDataFilter).subscribe((response) => {
      console.log('Total Clients   is ', response);
       this.dataClientsSource.paginator.length = response['totalClients'];
     });
}

masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataClientsSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataClientsSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataClientsSource.filter = filterValue.trim().toLowerCase();
  if (this.dataClientsSource.paginator) {
    this.dataClientsSource.paginator.firstPage();
  }
}
loadAddClientComponent() {
  this.router.navigate(['/pms/ventes/ajouterClient']) ;

}
loadEditClientComponent() {
  if (this.selection.selected.length === 0) {
    this.openSnackBar(' Aucun élément sélectionné' );
    // this.loadData();
    } else
   {
  this.router.navigate(['/pms/ventes/editerClient', this.selection.selected[ this.selection.selected.length - 1].code]) ;
    }
}
supprimerClients()
{
  console.log('NBRE OF ELEMENTS SELECTED');
  console.log(this.selection.selected.length);
  console.log(' ELEMENTS SELECTED');
  console.log(this.selection.selected);
  if ( this.selection.selected.length !== 0) {
    this.deleteEnCours = true;
      this.selection.selected.forEach(function(client, index) {
          this.clientsService.deleteClient(client.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.deleteEnCours = false;
            this.openSnackBar( this.selection.selected.length + ' Element(s) Supprimé(s)');
             this.loadData();
            }
      }, this);
    } else {
      this.openSnackBar('Aucun Client Séléctionné!');
    }
}
refresh()
{
  this.loadData(null);
  
}


/*attacherDocumentClient()
{
  if ( this.selection.selected.length !== 0) {
    
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
    top : '0'
  };
  dialogConfig.data = {
    codeClient : this.selection.selected[0].code
  };

  const dialogRef = this.dialog.open(DialogAddDocumentClientComponent,
    dialogConfig
  );
} else {
  this.openSnackBar('Aucun Client Séléctionné!');
}
}*/
  openSnackBar(messageToDisplay) {
 this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
     verticalPosition: 'top'});
}

changePage($event) {
  console.log('page event is ', $event);
 this.clientsService.getPageClients($event.pageIndex, $event.pageSize,this.clientDataFilter ).subscribe((clients) => {
    this.listClients = clients;
    console.log('list clients recieved is :', this.listClients);
   // this.transformDataToByConsumedByMatTable();
   const listElements = [];
 if (this.listClients !== undefined) {
    this.listClients.forEach(client => {
      listElements.push( {code: client.code ,
        numeroClient: client.numeroClient ,
        cin: client.cin, nom: client.nom, prenom: client.prenom,
        matriculeFiscale: client.matriculeFiscale, raisonSociale: client.raisonSociale ,
         
        adresse: client.adresse,   mobile: client.mobile} );
    });
  }
     this.dataClientsSource = new MatTableDataSource<ClientElement>(listElements);
     this.clientsService.getTotalClients(this.clientDataFilter).subscribe((response) => {
      console.log('Total clients is ', response);
      // this.dataClientsSource.paginator = this.paginator;
      if (this.dataClientsSource.paginator) {
        
       this.dataClientsSource.paginator.length = response.totalClients;
      }
     });

    });
}
toggleFilterPanel() {
  this.showFilter = !this.showFilter;
}
toggleShowColumnsMultiSelect() {
 this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
}
loadFiltredData() {
 console.log('the filter is ', this.clientDataFilter);
 this.loadData(this.clientDataFilter);
}

specifyListColumnsToBeDisplayed() {
  
   console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
   this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
 ?
    this.defaultColumnsDefinitions : [];
    this.selectedColumnsNames = [];
    
    if (this.selectedColumnsDefinitions !== undefined)
     {
       this.selectedColumnsDefinitions.forEach((colName) => { 
            const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
            const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
            const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
            this.columnsDefinitionsToDisplay.push(colDef);
           });
     }
    this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
   this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
              return colDef.displayTitle;
    });
    console.log('columns to disp:', this.columnsDefinitionsToDisplay);
    this.columnsNames = [];
    this.columnsNames[0] = 'select';
    this.columnsDefinitionsToDisplay .map((colDef) => {
     this.columnsNames.push(colDef.name);
    if (this.selectedColumnsDefinitions !== undefined) {
     this.selectedColumnsNames.push(colDef.displayTitle);
    }
 });
    this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
     return colDef.displayTitleAr;
 });
 }






}
