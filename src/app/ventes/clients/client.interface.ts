import { Personne } from '../../common/personne.interface'

export interface ClientElement {
         code: string ;
         numeroClient : string;
         cin: string ;
         nom: string;
         prenom: string;
         matriculeFiscale: string;
         responsableCommerciale: Personne;
         raisonSociale: string;
         categoryClient: string;
         mobile1: string;
         mobile2: string;
         adresse1: string;
         adresse2: string;
         email: string;
         ville: string;
         gouvernorat: string;
         codePostal: string;

         // descriptionEchange: string;
        // montantAPayer: number;
        // avance: number;
         // montantReste?: number;
          // periodTranche?: string;
        //  dateDebut?: string;
        //  valeurTranche?: number;
        //  nbreTranches?: number ;
        // public modeGenerationTranches = MODES_GENERATION_TRANCHES[1],
         // public listTranches?: Tranche[]
        }
