import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import {Client} from './client.model';
import {DocumentClient} from './document-client.model';

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_CLIENTS = 'http://localhost:8082/ventes/clients/getClients';
const URL_ADD_CLIENT = 'http://localhost:8082/ventes/clients/addClient';
const URL_GET_CLIENT = 'http://localhost:8082/ventes/clients/getClient';
const URL_GET_PAGE_CLIENTS = 'http://localhost:8082/ventes/clients/getPageClients';
const URL_UPDATE_CLIENT = 'http://localhost:8082/ventes/clients/updateClient';
const URL_DELETE_CLIENT = 'http://localhost:8082/ventes/clients/deleteClient';
const URL_GET_FILTRED_LIST_CLIENTS  = 'http://localhost:8082/ventes/clients/getFiltredClients';
const URL_GET_TOTAL_CLIENTS = 'http://localhost:8082/ventes/clients/getTotalClients';

const  URL_ADD_DOCUMENT_CLIENT = 'http://localhost:8082/ventes/documentsClients/addDocumentClient';
const  URL_GET_LIST_DOCUMENT_CLIENT = 'http://localhost:8082/ventes/documentsClients/getDocumentsClients';
const  URL_DELETE_DOCUCMENT_CLIENT = 'http://localhost:8082/ventes/documentsClients/deleteDocumentClient';
const  URL_UPDATE_DOCUMENT_CLIENT = 'http://localhost:8082/ventes/documentsClients/updateDocumentClient';
const URL_GET_DOCUMENT_CLIENT = 'http://localhost:8082/ventes/documentsClients/getDocumentClient';
const URL_GET_LIST_DOCUMENT_CLIENT_BY_CODE_CLIENT =
 'http://localhost:8082/ventes/documentsClients/getDocumentsClientsByCodeClient';
@Injectable({
  providedIn: 'root'
})
export class ClientService {



  constructor(private httpClient: HttpClient) {

   }
   getTotalClients(filter): Observable<any> {

    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('filter', filterStr);
    return this.httpClient
    .get<any>(URL_GET_TOTAL_CLIENTS, {params});
   }
   getClients(): Observable<Client[]> {
    return this.httpClient
    .get<Client[]>(URL_GET_LIST_CLIENTS);
   }
 


   getPageClients(pageNumber, nbElementsPerPage, filter): Observable<Client[]> {
    const filterStr = JSON.stringify(filter);
    
    const params = new HttpParams()
    .set('pageNumber', pageNumber)
    .set('nbElementsPerPage', nbElementsPerPage)
    .set('sort', 'name')
    .set('filter', filterStr);
    return this.httpClient
    .get<Client[]>(URL_GET_PAGE_CLIENTS, {params});
   }
  /* getClients(): Observable<Client[]> {
    return this.httpClient
    .get<Client[]>(URL_GET_LIST_CLIENTS);
   }*/
   getFiltredClients(filterClient): Observable<Client[]> {
    return this.httpClient
    .post<Client[]>(URL_GET_FILTRED_LIST_CLIENTS, filterClient);
   }
   addClient(client: Client): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_CLIENT, client);
  }
  updateClient(client: Client): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_CLIENT, client);
  }
  deleteClient(codeClient): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_CLIENT + '/' + codeClient);
  }
  getClient(codeClient): Observable<Client> {
    return this.httpClient.get<Client>(URL_GET_CLIENT + '/' + codeClient);
  }



  addDocumentClient(documentClient: DocumentClient): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_DOCUMENT_CLIENT, documentClient);
    
  }

  getDocumentsClients(): Observable<DocumentClient[]> {
    return this.httpClient
    .get<DocumentClient[]>(URL_GET_LIST_DOCUMENT_CLIENT);
   }
   getDocumentClient(codeDocumentClient): Observable<DocumentClient> {
    return this.httpClient.get<DocumentClient>(URL_GET_DOCUMENT_CLIENT + '/' + codeDocumentClient);
  }
  deleteDocumentClient(codeDocumentClient): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_DOCUCMENT_CLIENT + '/' + codeDocumentClient);
  }
  getDocumentsClientsByCodeClient(codeDocumentClient): Observable<DocumentClient[]> {
    return this.httpClient.get<DocumentClient[]>(URL_GET_LIST_DOCUMENT_CLIENT_BY_CODE_CLIENT + '/' + codeDocumentClient);
  }
  updateDocumentClient(doc: DocumentClient): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_DOCUMENT_CLIENT, doc);
  }
}
