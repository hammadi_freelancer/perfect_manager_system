import { Component, OnInit, Input, EventEmitter, Output, AfterContentChecked,
AfterViewChecked
} from '@angular/core';
// import { ArticleService } from './article.service';
import {Client} from './client.model';
import {ClientService} from './client.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {ActionData} from './action-data.interface';
import {MatSnackBar} from '@angular/material';

import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DialogAddDocumentClientComponent} from './popups/dialog-add-document-client.component';
// import { DialogListDocumentsClientsComponent } from './dialog-list-documents-clients.component';
@Component({
  selector: 'app-client-edit',
  templateUrl: './client-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class ClientEditComponent implements OnInit, AfterContentChecked {

// private client: Client = new Client();


private client: Client = new Client();
private updateEnCours = false;
private tabsDisabled = false;
private etatsClients: any[] = [
  {value: 'Annule', viewValue: 'Annulé'},
  {value: 'Initial', viewValue: 'Initial'},
  {value: 'Valide', viewValue: 'Validé'}
];

  constructor( private route: ActivatedRoute,
    private router: Router , private clientService: ClientService, private matSnackBar: MatSnackBar,
    private dialog: MatDialog
   ) {
  }
  ngOnInit() {
 
      const codeClient = this.route.snapshot.paramMap.get('codeClient');
      this.clientService.getClient(codeClient).subscribe((client) => {
               this.client = client;
             /*  if (this.client.etat === 'Valide') {
                this.tabsDisabled = false;
               }
               if (this.client.etat === 'Valide' || this.client.etat === 'Annule')
                {
                  this.etatsClients.splice(1, 1);
                }
                if (this.client.etat === 'Initial')
                  {
                    this.etatsClients.splice(0, 1);
                  }*/
      });
      // this.client = this.actionData.selectedClients[0];
     
  }
  loadAddClientComponent() {
    this.router.navigate(['/pms/ventes/ajouterClient']) ;
  
  }
  loadGridClients() {
    this.router.navigate(['/pms/ventes/clients']) ;
  }
  supprimerClient() {
     
      this.clientService.deleteClient(this.client.code).subscribe((response) => {

        this.openSnackBar( ' Element Supprimé');
        this.loadGridClients() ;
            });
  }
updateClient() {
  console.log(this.client);
  this.updateEnCours = true;
  
  this.clientService.updateClient(this.client).subscribe((response) => {
    this.updateEnCours = false;
    
    if (response.error_message ===  undefined) {
    /*  if (this.client.etat === 'Valide')
        {
          this.tabsDisabled = false;
          this.etatsClients = [
            {value: 'Annule', viewValue: 'Annulé'},
            {value: 'Valide', viewValue: 'Validé'}
  
          ];
        }
        if (this.client.etat === 'Annule')
          {
            this.tabsDisabled = true;
         
          }*/
   
    } else {
      this.openSnackBar( ' Erreurs.. ');
      
     // show error message
    }
});
}


fileChanged($event) {
  const file = $event.target.files[0];
  const fileReader = new FileReader();
  fileReader.onload = (e) => {
    console.log(fileReader.result);
     this.client.image = fileReader.result;
    // this.filesData[indexFile] = fileData;
  };
  fileReader.readAsDataURL(file);
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

ngAfterContentChecked() {
 
}
selectedTabChanged($event) {
  if ($event.index === 4) { // Composition tab was selected
   // this.loadListArticles();
   } else if ($event.index === 1) {
    //  this.loadListSchemasConfiguration() ;
   }
  }
}


