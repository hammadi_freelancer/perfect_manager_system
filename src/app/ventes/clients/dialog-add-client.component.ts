
import { Component, OnInit, Inject} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {ClientService} from '../clients/client.service';
import {Client} from '../clients/client.model';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
    selector: 'app-dialog-add-client',
    templateUrl: 'dialog-add-client.component.html',
  })
  export class DialogAddClientComponent implements OnInit {
     private client: Client;
     private saveEnCours = false;
     constructor(  private clientService: ClientService,
     private matSnackBar: MatSnackBar,  @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
          
       this.client = new Client();
           
    }
    saveClient() 
    {
      
       console.log(this.client);
      this.saveEnCours = true;
       this.clientService.addClient(this.client).subscribe((response) => {
         this.saveEnCours = false;
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.client = new Client();
             this.openSnackBar(  'Client Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }