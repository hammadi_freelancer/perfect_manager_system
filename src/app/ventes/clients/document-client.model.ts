
import { BaseEntity } from '../../common/base-entity.model' ;

type TypeDocument =   'Cheque' | 'Kimbiela' |'CarteIdentite' | 'ExtraitNaissance'
 | 'AttestationTravail' | 'RegistreCommerce' | 'CompteBancaire' | 'ComptePostale';

export class DocumentClient extends BaseEntity {
    constructor(
        public code?: string,
         public codeClient?: string,
         public numeroDocument?: string,
        public dateReceptionObject?: Date,
        public dateReception?: string,
        public typeDocument?: TypeDocument,
        public fileExtension?: string,
        public imageFace1?: any,
        public imageFace2?: any,
     ) {
         super();
        
       }
  
}
