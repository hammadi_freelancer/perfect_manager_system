import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,
    
      ViewChild } from '@angular/core';
    // import { ArticleService } from './article.service';
    import {Client} from './client.model';
    // import { MODES_GENERATION_TRANCHES} from './fournisseur.model';
    import {ClientService} from './client.service';
    import {FormControl} from '@angular/forms';
    import {map, startWith} from 'rxjs/operators';
    import {Observable} from 'rxjs';
    import { Router, ActivatedRoute, ParamMap } from '@angular/router';
    import {ActionData} from './action-data.interface';
    @Component({
        selector: 'app-client-home',
        templateUrl: './client-home.component.html',
        // styleUrls: ['./players.component.css']
      })
 export class ClientHomeComponent implements OnInit {

    @Input()
    private actionData: ActionData;

    
    private homeMessage = 'Here I will display the list of : last clients updated' +
    'last client created ; last client ...';

        constructor( private route: ActivatedRoute,
            private router: Router, private clientService: ClientService) {
          }

          ngOnInit() {
        }
        
        }
