import { Component, OnInit, Input, EventEmitter, Output, AfterContentChecked,
    AfterViewChecked
    } from '@angular/core';
    // import { ArticleService } from './article.service';
    import {Client} from './client.model';
    import {ClientService} from './client.service';
    import {FormControl} from '@angular/forms';
    import {map, startWith} from 'rxjs/operators';
    import {Observable} from 'rxjs';
    import { Router, ActivatedRoute, ParamMap } from '@angular/router';
    import {ActionData} from './action-data.interface';
    import {MatSnackBar} from '@angular/material';
    
    @Component({
      selector: 'app-client-new',
      templateUrl: './client-new.component.html',
      // styleUrls: ['./players.component.css']
    })
export class ClientNewComponent implements OnInit, AfterContentChecked {
    private client: Client = new Client();
    @Input()
    private listClients: any = [] ;
    
private saveEnCours = false;
    // @Output()
    // save: EventEmitter<Client> = new EventEmitter<Client>();
    
 
      constructor( private route: ActivatedRoute,
        private router: Router , private clientService: ClientService, private matSnackBar: MatSnackBar ) {
      }
      ngOnInit() {
     
      }
    saveClient() {
       
        console.log(this.client);
        this.saveEnCours = true;
        this.clientService.addClient(this.client).subscribe((response) => {
          this.saveEnCours = false;
            if (response.error_message ===  undefined) {
              // this.save.emit(response);
              this.client = new Client();
              this.openSnackBar(  'Client Enregistré');
            } else {
             // show error message
             this.openSnackBar(  'Erreurs!:Essayer une autre fois');
             
            }
        });
    }
    loadGridClients() {
      this.router.navigate(['/pms/ventes/clients']) ;
    }
    fileChanged($event) {
        const file = $event.target.files[0];
        const fileReader = new FileReader();
        fileReader.onload = (e) => {
             console.log(e);
          console.log(fileReader.result);
          
           this.client.image = fileReader.result;
          // this.filesData[indexFile] = fileData;
        };
        fileReader.readAsDataURL(file);
       }
       vider() {
         this.client = new Client();
       }
       openSnackBar(messageToDisplay) {
        this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
            verticalPosition: 'top'});
       }
    ngAfterContentChecked() {
    
    }
    }
