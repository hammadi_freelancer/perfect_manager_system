import { Injectable } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { ClientsGridComponent } from './clients/clients-grid.component';
import { ClientNewComponent } from './clients/client-new.component';
import { ClientEditComponent } from './clients/client-edit.component';
// credit vente
import { CreditNewComponent } from './credits/credit-new.component';
import { CreditEditComponent } from './credits/credit-edit.component';
import { CreditsGridComponent } from './credits/credits-grid.component';
// Facture ventes
import { FactureVenteNewComponent } from './factures-ventes/facture-vente-new.component';
import { FactureVenteEditComponent } from './factures-ventes/facture-vente-edit.component';
import { FacturesVentesGridComponent } from './factures-ventes/factures-ventes-grid.component';
import { DocumentVentesNewComponent } from './documents-ventes/document-vente-new.component';
import { DocumentVenteEditComponent } from './documents-ventes/document-vente-edit.component';
import { DocumentsVentesGridComponent } from './documents-ventes/documents-ventes-grid.component';
import { ClientsResolver } from './clients-resolver';
import { ArticlesResolver } from './articles-resolver';
import { MagasinsResolver } from './magasins-resolver';
import { UnMesuresResolver } from './unMesures-resolver';

// releves ventes

import { ReleveVenteNewComponent } from './releves-ventes/releve-vente-new.component';
import { ReleveVenteEditComponent } from './releves-ventes/releve-vente-edit.component';
import { RelevesVentesGridComponent } from './releves-ventes/releves-ventes-grid.component';


import { DemandeVentesNewComponent } from './demandes-ventes/demande-ventes-new.component';
import { DemandeVentesEditComponent } from './demandes-ventes/demande-ventes-edit.component';
import { DemandesVentesGridComponent } from './demandes-ventes/demandes-ventes-grid.component';



import { ParametresVentesComponent } from './parametres-ventes.component';

import {ModuleWithProviders  } from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';

 export const ventesRoute: Routes = [
    {
        path: 'parametresVentes',
        component: ParametresVentesComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'clients',
        component: ClientsGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },

    {
        path: 'ajouterClient',
        component: ClientNewComponent,
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerClient/:codeClient',
        component: ClientEditComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'credits',
        component: CreditsGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterCredit',
        component: CreditNewComponent,
        resolve: { clients: ClientsResolver
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerCredit/:codeCredit',
        component: CreditEditComponent,
        resolve: { clients: ClientsResolver
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
    },
    // Releve de vente routing
    {
        path: 'relevesVentes',
        component: RelevesVentesGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterReleveVentes',
        component: ReleveVenteNewComponent,
        resolve: { 
            clients: ClientsResolver,
             articles: ArticlesResolver,
             unMesures : UnMesuresResolver,
            //  magasins : MagasinsResolver
             
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerReleveVentes/:codeReleveVentes',
        component: ReleveVenteEditComponent,
        resolve: { 
            clients: ClientsResolver, 
             articles: ArticlesResolver,
             unMesures : UnMesuresResolver,
            //  magasins : MagasinsResolver
             
             
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        }
    },
    // Releve de vente routing
    {
        path: 'facturesVentes',
        component: FacturesVentesGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterFactureVentes',
        component: FactureVenteNewComponent,
        resolve: { 
            clients: ClientsResolver,
             articles: ArticlesResolver,
             unMesures : UnMesuresResolver,
             magasins : MagasinsResolver
             
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerFactureVentes/:codeFactureVentes',
        component: FactureVenteEditComponent,
        resolve: { 
            clients: ClientsResolver, 
             articles: ArticlesResolver,
            unMesures : UnMesuresResolver,
             magasins : MagasinsResolver
             
             
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        }
    },

    {
        path: 'demandesVentes',
        component: DemandesVentesGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterDemandeVentes',
        component: DemandeVentesNewComponent,
        resolve: { 
            clients: ClientsResolver,
             articles: ArticlesResolver,
             unMesures : UnMesuresResolver,
           //  magasins : MagasinsResolver
             
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerDemandeVentes/:codeDemandeVentes',
        component: DemandeVentesEditComponent,
        resolve: { clients: ClientsResolver, 
             articles: ArticlesResolver,
             unMesures : UnMesuresResolver,
             
            // magasins : MagasinsResolver
             
             
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        }
    },



    {
        path: 'documentsVentes',
        component: DocumentsVentesGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterDocumentVente',
        component: DocumentVentesNewComponent,
        resolve: { clients: ClientsResolver, articles: ArticlesResolver
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerDocumentVente/:codeDocumentVente',
        component: DocumentVenteEditComponent,
        resolve: { clients: ClientsResolver,  articles: ArticlesResolver, magasins: MagasinsResolver
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        }
    },























    {
        path: 'parametresVentes',
        component: ParametresVentesComponent,
    
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        }
    }

];
// export const ventesRoutingModule: ModuleWithProviders = RouterModule.forRoot(ventesRoute);
// commonRouting.providers = [{ provide: APP_BASE_HREF, useValue: '/common'} ] ;


