import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ParametresVentes } from './parametres-ventes.model';

const  URL_SAVE_PARAMETRES_VENTES = 'http://localhost:8082/ventes/parametresVentes/saveParametresVentes';
const  URL_GET_PARAMETRES_VENTES = 'http://localhost:8082/ventes/parametresVentes/getParametresVentes';


@Injectable({
  providedIn: 'root'
})
export class VentesService {

  constructor(private httpClient: HttpClient) {

   }

   getParametresVentes(): Observable<ParametresVentes> {
    return this.httpClient
    .get<ParametresVentes>(URL_GET_PARAMETRES_VENTES);
   }
  saveParametresVentes(parametresVentes: ParametresVentes): Observable<any> {
    return this.httpClient.post<any>(URL_SAVE_PARAMETRES_VENTES, parametresVentes);
  }
}
