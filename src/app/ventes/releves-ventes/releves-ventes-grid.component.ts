import { Component, OnInit, Output,ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {ReleveVente} from './releve-vente.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {ReleveVenteService} from './releve-vente.service';
// import { ActionData } from './action-data.interface';
import { Client } from '../../ventes/clients/client.model';
import { ReleveVenteElement } from './releve-vente.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DialogAddClientComponent } from '../clients/dialog-add-client.component';
import { DialogAddPayementReleveVentesComponent } from './popups/dialog-add-payement-releve-ventes.component';
import { ReleveVentesDataFilter } from './releve-ventes-data.filter';
import { SelectItem } from 'primeng/api';

@Component({
  selector: 'app-releves-ventes-grid',
  templateUrl: './releves-ventes-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class RelevesVentesGridComponent implements OnInit {

  private releveVentes: ReleveVente =  new ReleveVente();
  private showSelectColumnsMultiSelect = false;
  private showFilter = false;
  private showFilterClient = false;
  private releveVentesDataFilter = new ReleveVentesDataFilter();
  private selection = new SelectionModel<ReleveVenteElement>(true, []);
  
@Output()
select: EventEmitter<ReleveVente[]> = new EventEmitter<ReleveVente[]>();

//private lastClientAdded: Client;

 selectedReleveVente: ReleveVente ;

 

 @Input()
 private listRelevesVentes: any = [] ;
 private checkedAll: false;

 private  dataRelevesVentesSource: MatTableDataSource<ReleveVenteElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
 
 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay :ColumnDefinition[] = [];

 private columnsToSelect : SelectItem[];
  private  selectedColumnsDefinitions: string[];

 private selectedColumnsNames: string[];
  constructor( private route: ActivatedRoute, private releveVenteService: ReleveVenteService,
    private router: Router, private matSnackBar: MatSnackBar,
  private  dialog: MatDialog ) {
  }
  ngOnInit() {
    this.columnsToSelect = [
      {label: 'N° Relevé', value: 'numeroReleveVente', title: 'N° Relevé'},
      {label: 'Date Ventes', value: 'dateVentes', title: 'Date Ventes'},
      {label: 'Montant à Payer', value: 'montantAPayer', title: 'Montant à Payer'},
      {label: 'Montant Bénéfice', value: 'montantBeneficeEstime', title: 'Montant Bénéfice'},
      {label: 'Réglée', value: 'regleToutes', title: 'Réglée'},
      {label: 'Livrée', value: 'livreToutes', title: 'Livrée'},
      {label: 'Rs.Soc.Client', value:  'raisonSociale', title: 'Rs.Soc.Client' },
      {label: 'Matr.Client', value:  'matriculeFiscale', title: 'Matr.Client' },
      {label: 'Reg.Client', value:  'registreCommerce', title: 'Reg.Client' },
      {label: 'CIN Client', value: 'cin', title: 'CIN Client'},  
      {label: 'Nom Client', value: 'nom', title: 'Nom Client'},
      {label: 'Prénom Client', value: 'prenom', title: 'Prénom Client'},
      {label: 'Etat', value:  'etat', title: 'Etat' },
  
  ];
  this.selectedColumnsDefinitions = [
    'numeroReleveVente',
    'dateVentes' , 
    'montantAPayer', 
    'nom' ,
    'prenom',
    'cin', 
    'etat'
    ];
    this.defaultColumnsDefinitions = [
      {name: 'numeroReleveVente' , displayTitle: 'N°Relevé'},
      {name: 'dateVentes' , displayTitle: 'Date Ventes'},
      {name: 'montantAPayer' , displayTitle: 'Montant à Payer'},
      {name: 'montantBeneficeEstime' , displayTitle: 'Montant Bénéfice'},
      {name: 'regleToutes' , displayTitle: 'Réglée'},
      {name: 'livreToutes' , displayTitle: 'Livrée'},
       {name: 'nom' , displayTitle: 'Nom Four.'},
      {name: 'prenom' , displayTitle: 'Prénom Four.'},
      {name: 'cin' , displayTitle: 'CIN Four.'},
      {name: 'matriculeFiscale' , displayTitle: 'Matr. Four.'},
      {name: 'raisonSociale' , displayTitle: 'Raison Four.'},
      {name: 'registreCommerce' , displayTitle: 'Registre Four.'},
       {name: 'etat', displayTitle: 'Etat' },
      ];

    this.releveVenteService.getPageRelevesVentes(0, 5, null).subscribe((releveVentes) => {
      this.listRelevesVentes = releveVentes;
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });


  }
  deleteReleveVente(releveVente) {
      console.log('call delete !', releveVente );
    this.releveVenteService.deleteReleveVente(releveVente.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData(null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.releveVenteService.getPageRelevesVentes(0, 5,filter).subscribe((relevesVentes) => {
    this.listRelevesVentes = relevesVentes;
    this.transformDataToByConsumedByMatTable();
   });
}


checkOneActionInvoked() {
  const listSelectedRelevesVentes: ReleveVente[] = [];
console.log('selected clients are ');
console.log(this.selection.selected);
if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((relVenteElement) => {
  return relVenteElement.code;
});
this.listRelevesVentes.forEach(rVente => {
  if (codes.findIndex(code => code === rVente.code) > -1) {
    listSelectedRelevesVentes.push(rVente);
  }
  });
}
this.select.emit(listSelectedRelevesVentes);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listRelevesVentes !== undefined) {
    this.listRelevesVentes.forEach(releveVente => {
      listElements.push( {
         code: releveVente.code ,
         numeroReleveVente: releveVente.numeroReleveVente,
        dateVentes: releveVente.dateVente,
        cin: releveVente.client.cin,
        nom: releveVente.client.nom,
         prenom: releveVente.client.prenom,
        matriculeFiscale: releveVente.client.matriculeFiscale,
        raisonSociale: releveVente.client.raisonSociale ,
        montantAPayer: releveVente.montantAPayer,
        montantBenefice: releveVente.montantBenefice,
        regleToutes: releveVente.regleToutes? 'Oui' : 'Non',
        livreToutes: releveVente.livreToutes? 'Oui': 'Non',
        etat: releveVente.etat,
        
    });
  });
    this.dataRelevesVentesSource = new MatTableDataSource<ReleveVenteElement>(listElements);
    this.dataRelevesVentesSource.sort = this.sort;
    this.dataRelevesVentesSource.paginator = this.paginator;
    this.releveVenteService.getTotalRelevesVentes(this.releveVentesDataFilter).subscribe((response) => {
      console.log('Total Releves de Ventes  is ', response);
       this.dataRelevesVentesSource.paginator.length = response['totalRelevesVentes'];
     });
}
}
specifyListColumnsToBeDisplayed() {
  
   console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
   this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
 ?
    this.defaultColumnsDefinitions : [];
    this.selectedColumnsNames = [];
    
    if (this.selectedColumnsDefinitions !== undefined)
     {
       this.selectedColumnsDefinitions.forEach((colName) => { 
            const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
            const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
            const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
            this.columnsDefinitionsToDisplay.push(colDef);
           });
     }
    this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
   this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
              return colDef.displayTitle;
    });
    console.log('columns to disp:', this.columnsDefinitionsToDisplay);
    this.columnsNames = [];
    this.columnsNames[0] = 'select';
    this.columnsDefinitionsToDisplay .map((colDef) => {
     this.columnsNames.push(colDef.name);
    if (this.selectedColumnsDefinitions !== undefined) {
     this.selectedColumnsNames.push(colDef.displayTitle);
    }
 });
    this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
     return colDef.displayTitleAr;
 });
 }
masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataRelevesVentesSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataRelevesVentesSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataRelevesVentesSource.filter = filterValue.trim().toLowerCase();
  if (this.dataRelevesVentesSource.paginator) {
    this.dataRelevesVentesSource.paginator.firstPage();
  }
}

loadAddReleveVentesComponent() {
  this.router.navigate(['/pms/ventes/ajouterReleveVentes']) ;

}
loadEditReleveVentesComponent() {
  this.router.navigate(['/pms/ventes/editerReleveVentes', this.selection.selected[ this.selection.selected.length - 1].code]) ;

}
supprimerRelevesVentes()
{

  if ( this.selection.selected.length !== 0) {
      this.selection.selected.forEach(function(releveVente, index) {

   this.releveVenteService.deleteReleveVente(releveVente.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.openSnackBar( this.selection.selected.length + ' Relevé(s) de Ventes Supprimé(s)');
            this.selection.selected = [];
            this.loadData();
            }
      }, this);
    } else {
      this.openSnackBar('Aucun Relevé Séléctionnée !');
    }
}
refresh() {
  this.loadData(null);
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 addPayement()
 {
  if ( this.selection.selected.length !== 0) {
    
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
    top: '0'
  };
  dialogConfig.data = {
    codeReleveVentes : this.selection.selected[0].code
  };

  const dialogRef = this.dialog.open(DialogAddPayementReleveVentesComponent,
    dialogConfig
  );
} else {
  this.openSnackBar('Aucun Relevé Séléctionnée !');
}
 }
 history()
 {
   
 }
 addClient()
 {
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
  };

  const dialogRef = this.dialog.open(DialogAddClientComponent,
    dialogConfig
  );
 }
 attacherDocuments()
 {

 }


 changePage($event) {
  console.log('page event is ', $event);
 this.releveVenteService.getPageRelevesVentes($event.pageIndex, $event.pageSize,this.releveVentesDataFilter ).subscribe((relvesVentes) => {
    this.listRelevesVentes= relvesVentes;
   //  console.log(this.listFacturesVentes);
   // this.transformDataToByConsumedByMatTable();
   const listElements = [];
   this.listRelevesVentes.forEach(relVente => {
    listElements.push( {code: relVente.code ,
       numeroReleveVente: relVente.numeroReleveVente,
      dateVentes: relVente.dateVente, 
      cin: relVente.client.cin,
      nom: relVente.client.nom, prenom: relVente.client.prenom,
      matriculeFiscale: relVente.client.matriculeFiscale, raisonSociale: relVente.client.raisonSociale ,
      montantAPayer: relVente.montantAPayer,
      montantBenefice: relVente.montantBenefice,
      regleToutes: relVente.regleToutes? 'Oui' : 'Non',
      livreToutes: relVente.regleLivre? 'Oui' : 'Non',
      etat: relVente.etat
  });
});
     this.dataRelevesVentesSource = new MatTableDataSource<ReleveVenteElement>(listElements);

     
     this.releveVenteService.getTotalRelevesVentes(this.releveVentesDataFilter).subscribe((response) => {
      console.log('Total Releves Ventes  is ', response);
      if (this.dataRelevesVentesSource.paginator) {
       this.dataRelevesVentesSource.paginator.length = response['totalRelevesVentes'];
      }
     });

    });
}
toggleFilterPanel() {
  this.showFilter = !this.showFilter;
}
toggleShowColumnsMultiSelect() {
 this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
}
loadFiltredData() {
 console.log('the filter is ', this.releveVentesDataFilter);
 this.loadData(this.releveVentesDataFilter);
}
activateFilterClient()
{
  this.showFilterClient = !this.showFilterClient;
}
}
