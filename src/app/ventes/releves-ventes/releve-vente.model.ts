
import { BaseEntity } from '../../common/base-entity.model' ;
// import { Tranche } from './tranche.model' ;
import { Client } from '../clients/client.model' ;
import { PayementData } from './payement-data.model';
import { LigneReleveVente } from './ligne-releve-vente.model';
import { Credit } from '../credits/credit.model';

type  EtatReleveVente= |'Initial' |'Valide' | 'Annule' | '';


export class ReleveVente extends BaseEntity {
    constructor(
        public code?: string,
        public numeroReleveVente?: string,
        public client?: Client,
         public montantBenefice?: number,
        public montantAPayer?: number,
        public payementData?: PayementData,
        public dateVenteObject?: Date,
        public dateVente?: string,
        public regleToutes?: boolean,
        public livreToutes?: boolean,
          public etat?: EtatReleveVente,
     public listLignesRelVentes?: LigneReleveVente[]
     ) {
         super();
         this.client = new Client();
         this.montantAPayer = Number(0);
         this.montantBenefice = Number(0);
         this.payementData = new PayementData();
         this.listLignesRelVentes = [] ;
         // this.categoryClient = 'PERSONNE_PHYSIQUE';
    }
}
