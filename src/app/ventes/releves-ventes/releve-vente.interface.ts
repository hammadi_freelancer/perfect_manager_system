export interface ReleveVenteElement {
            code: string;
         numeroReleveVente: string;
         cin: string;
         nom: string;
          prenom: string;
         matriculeFiscale: string ;
         raisonSociale: string;
          montantBenefice: number;
          montantAPayer: number;
         // payementData: PayementData;
         dateVentes?: string;
         regleToutes?: boolean;
         livreToutes?: boolean;
         etat: string;
        }
