import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {FactureVente} from '../factures-ventes/facture-vente.model';
import { ParametresVentes } from '../parametres-ventes.model';

import {FactureVenteService} from '../factures-ventes/facture-vente.service';
import {ReleveVenteService} from './releve-vente.service';

import {VentesService} from '../ventes.service';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
// import {ActionData} from './action-data.interface';
import { ClientService } from '../clients/client.service';
import { ClientFilter } from '../clients/client.filter';
import { ArticleFilter } from './article.filter';
import { UnMesureFilter } from '../../stocks/un-mesures/un-mesures.filter';

import {Client} from '../clients/client.model';
import {Magasin} from '../../stocks/magasins/magasin.model';
import {UnMesure} from '../../stocks/un-mesures/un-mesure.model';

import {ReleveVente} from '../releves-ventes/releve-vente.model';
import {LigneReleveVente} from '../releves-ventes/ligne-releve-vente.model';

import {LigneReleveVenteElement} from '../releves-ventes/ligne-releve-vente.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';

import {MatSnackBar} from '@angular/material';
import { MatExpansionPanel } from '@angular/material';
import {ArticleService} from '../../stocks/articles/article.service';
import {Article} from '../../stocks/articles/article.model';
import {ModePayementLibelle} from './mode-payement-libelle.interface';
import {SelectionModel} from '@angular/cdk/collections';

type LibelleModePayement = 'Credit'| 'Comptant' | 'Cheque' ;

@Component({
  selector: 'app-releve-vente-new',
  templateUrl: './releve-vente-new.component.html',
  // styleUrls: ['./players.component.css']
})
export class ReleveVenteNewComponent implements OnInit {

 private releveVentes: ReleveVente =  new ReleveVente();
 private parametresVentes = new ParametresVentes();
 private  dataLignesReleveVentesSource: MatTableDataSource<LigneReleveVenteElement>;
 
//@Input()
// private listVentes: any = [] ;

private saveEnCours = false;
private managedLigneReleveVentes = new LigneReleveVente();

private selection = new SelectionModel<LigneReleveVenteElement>(true, []);
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
 
/*@Output()
save: EventEmitter<FactureVente> = new EventEmitter<FactureVente>();*/

@ViewChild('firstMatExpansionPanel')
 private firstMatExpansionPanel : MatExpansionPanel;

 @ViewChild('manageLigneMatExpansionPanel')
 private manageLigneMatExpansionPanel : MatExpansionPanel;

 @ViewChild('listLignesMatExpansionPanel')
 private listLignesMatExpansionPanel : MatExpansionPanel;

private clientFilter: ClientFilter;
private articleFilter: ArticleFilter;
private unMesureFilter : UnMesureFilter;
private listClients: Client[];
private listArticles: Article[];


private payementModes: ModePayementLibelle[] = [
  {value: 'Comptant', viewValue: 'Comptant'},
  {value: 'Cheque', viewValue: 'Chèque'},
  {value: 'Credit', viewValue: 'Crédit'},
  {value: 'Credit_Comptant', viewValue: 'Crédit/Comptant'}
];
private etatsReleveVentes: any[] = [
  {value: 'Initial', viewValue: 'Initial'}
 // {value: 'Valide', viewValue: 'Validé'}
];

  constructor( private route: ActivatedRoute,
    private router: Router, private releveVenteService: ReleveVenteService, private clientService: ClientService,
    private articleService: ArticleService, private ventesService: VentesService,
   private matSnackBar: MatSnackBar, private elRef: ElementRef) {
  }


  // myControl = new FormControl();
  // options: string[] = ['One', 'Two', 'Three'];
  // filteredOptions: Observable<string[]>;

  ngOnInit() {

   this.listClients = this.route.snapshot.data.clients;
   this.listArticles = this.route.snapshot.data.articles;
  // this.listCodesArticles  = this.listArticles.map((article) => article.code);
   this.clientFilter = new ClientFilter(this.listClients);
   this.articleFilter = new ArticleFilter(this.route.snapshot.data.articles,
    [], [] );
     this.unMesureFilter = new UnMesureFilter(this.route.snapshot.data.unMesures);

   this.firstMatExpansionPanel.open();
   this.manageLigneMatExpansionPanel.opened.subscribe(openedPanel => {
   
     });
   this.specifyListColumnsToBeDisplayed();
   this.transformDataToByConsumedByMatTable();
  // this.initializeListLignesFactures();
  /*this.lignesFactures.forEach(function(ligne, index) {
                 ligne.code =  String(index + 1);
  });*/
  this.releveVentes.etat = 'Initial';
  this.ventesService.getParametresVentes().subscribe((response) => {
    if (response !==  undefined) {
      // this.save.emit(response);
      this.parametresVentes = response;
      
    } else {
     // show error message
    }
});
  }
 
  calculPrixTotal()
  {
    // this.managedLigneReleveVentes.prixTotal = this.
    this.managedLigneReleveVentes.prixTotal  = +  this.managedLigneReleveVentes.prixUnt *
    this.managedLigneReleveVentes.quantite  ;
    
  }



  constructClassName(i)  {
    return  'designationDivs_' + i;
   //  this.elRef.nativeElement.querySelector('.designationDivs_' + i )  ;
  }
 
  /*selectedArticleCode(i, $event) {
     console.log(i);
     console.log($event);
     this.listLignesFactureVentes[i].article.code = $event.option.value;
     this.listLignesFactureVentes[i].article.libelle  = this.listArticles.find((article) => (article.code === $event.option.value)).libelle;
    
     const  div  : HTMLDivElement = this.elRef.nativeElement.querySelector('.designationDivs_' + i )  ;
     div.innerText = this.listLignesFactureVentes[i].article.libelle;
     console.log(div);


  }*/

  saveReleveVente() {

    /*if (this.modeUpdate ) {
      this.updateCredit();
    } else {*/
      if (this.releveVentes.etat === '' || this.releveVentes.etat === undefined) {
        this.releveVentes.etat = 'Initial';
      }

    this.saveEnCours = true;
    
    this.releveVenteService.addReleveVente(this.releveVentes).subscribe((response) => {
      this.saveEnCours = false;
      
        if (response.error_message ===  undefined) {
          console.log('undefined error message');
          // this.save.emit(response);
          this.releveVentes = new  ReleveVente();
          this.releveVentes.listLignesRelVentes = [];
          this.openSnackBar(  'Relevée de Vente Enregistrée');
        } else {
         // show error message
         this.openSnackBar(  'Erreurs:Opération Echouée');
         
        }
    });
  }
  fillDataNomPrenom()
  {
    const listFiltred  = this.listClients.filter((client) => (client.cin === this.releveVentes.client.cin));
    this.releveVentes.client.nom = listFiltred[0].nom;
    this.releveVentes.client.prenom = listFiltred[0].prenom;
  }
  fillDataMatriculeRaison()
  {
    const listFiltred  = this.listClients.filter((client) => (client.registreCommerce === this.releveVentes.client.registreCommerce));
    this.releveVentes.client.matriculeFiscale = listFiltred[0].matriculeFiscale;
    this.releveVentes.client.raisonSociale = listFiltred[0].raisonSociale;
  }
  vider() {
    this.releveVentes =  new ReleveVente();
  }
  openSnackBar(messageToDisplay) {
   this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
       verticalPosition: 'top', panelClass: 'snackBarClass'});
  }
loadGridRelevesVentes() {
  this.router.navigate(['/pms/ventes/relevesVentes']) ;
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.releveVentes.listLignesRelVentes !== undefined) {
    this.releveVentes.listLignesRelVentes.forEach(ligneRel => {
           listElements.push( {code: ligneRel.code ,
            numeroLigneRelVentes: ligneRel.numeroLigneRelVentes,
            article: ligneRel.article.code,
             unMesure: ligneRel.unMesure.code,
             quantite: ligneRel.quantite,
             prixUnt: ligneRel.prixUnt,
             prixTotal: ligneRel.prixTotal,
             beneficeTotal: ligneRel.beneficeTotal,
             regle: ligneRel.regle? 'Oui' : 'Non',
             livre: ligneRel.livre? 'Oui' : 'Non'
      } );
    });
  }
    this.dataLignesReleveVentesSource= new MatTableDataSource<LigneReleveVenteElement>(listElements);
    this.dataLignesReleveVentesSource.sort = this.sort;
    this.dataLignesReleveVentesSource.paginator = this.paginator;
    
    
}
 specifyListColumnsToBeDisplayed() {
   // console.log('calling specifyListColumnsToBeDisplayed documents grid');
  this.columnsTitles = [];
  this.columnsNames = [];
  this.columnsTitlesAr  = [];
  
   this.columnsDefinitions = [{name: 'numeroLigneRelVentes' , displayTitle: 'N°'},
   {name: 'article' , displayTitle: 'Art.'},
   {name: 'unMesure' , displayTitle: 'Un.Mes.'},
   {name: 'quantite' , displayTitle: 'Qté'},
   {name: 'prixUnt' , displayTitle: 'P.Unt(TTC)'},
   {name: 'prixTotal' , displayTitle: 'P.Tot(TTC)'},
   {name: 'beneficeTotal' , displayTitle: 'Bénef.Tot.'},
   {name: 'regle' , displayTitle: 'Réglé'},
   {name: 'livre' , displayTitle: 'Livré'},
    ];

   this.columnsTitles = this.columnsDefinitions.map((colDef) => {
             return colDef.displayTitle;
   });
   
   this.columnsNames[0] = 'select';
   this.columnsDefinitions.map((colDef) => {
    this.columnsNames.push(colDef.name);
    console.log(this.columnsNames);
});
   this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
    return colDef.displayTitleAr;
});
}

applyFilter(filterValue: string) {
  this.dataLignesReleveVentesSource.filter = filterValue.trim().toLowerCase();
  if (this.dataLignesReleveVentesSource.paginator) {
    this.dataLignesReleveVentesSource.paginator.firstPage();
  }
}

checkLigneReleveVentes()
{

}
getIndexOfRow(row)
{

    for ( let i = 0; i < this.releveVentes.listLignesRelVentes.length ; i++) {
      if (this.releveVentes.listLignesRelVentes[i].numeroLigneRelVentes === row.numeroLigneRelVentes)
       {
             return i;
        }
    }
    return -1;
}
mapRowGridToObjectLigneReleveVente(rowGrid): LigneReleveVente
{
  const ligneRV  = new  LigneReleveVente() ;
  ligneRV.numeroReleveVentes = rowGrid.numeroReleveVentes;
  ligneRV.numeroLigneRelVentes = rowGrid.numeroLigneRelVentes;
  ligneRV.article.code =  rowGrid.article;
  ligneRV.unMesure.code =  rowGrid.unMesure;
  ligneRV.quantite =  rowGrid.quantite;
  ligneRV.prixUnt =  rowGrid.prixUnt;
  ligneRV.prixTotal =  rowGrid.prixTotal;
  ligneRV.beneficeTotal =  rowGrid.beneficeTotal;
  ligneRV.livre =  rowGrid.livre;
  ligneRV.regle =  rowGrid.regle;
  return ligneRV;
}
editLigneReleveVentes(row)
{
  this.managedLigneReleveVentes = this.mapRowGridToObjectLigneReleveVente(row);


    if(row.unMesure === undefined)
      {
        this.managedLigneReleveVentes.unMesure = new UnMesure();
      }
      if(row.article === undefined)
        {
          this.managedLigneReleveVentes.article = new Article();
        }
        console.log('row to edit is ', row);

  this.manageLigneMatExpansionPanel.open();
  
}
deleteLigneReleveVentes(row)
{
 // console.log('index is ');
  //console.log(index);
  const indexOfRow = this.getIndexOfRow(row);
  this.releveVentes.listLignesRelVentes.splice(indexOfRow , 1 );
  this.transformDataToByConsumedByMatTable();
  
}
addLigneReleveVentes()
{
  if (this.managedLigneReleveVentes.quantite === undefined
    || this.managedLigneReleveVentes.quantite === 0 ||
    this.managedLigneReleveVentes.quantite === null ||
    this.managedLigneReleveVentes.prixUnt === undefined
      || this.managedLigneReleveVentes.prixUnt === 0 ||
      this.managedLigneReleveVentes.prixUnt === null  ||
      this.managedLigneReleveVentes.article.code === undefined  ||
      this.managedLigneReleveVentes.article.code === '' ||
      this.managedLigneReleveVentes.article.code === null 
  ) {
        this.openSnackBar('Donnnées Manquées (QTE/ART/PUNIT!');
    } else if (this.managedLigneReleveVentes.numeroLigneRelVentes === undefined
  || this.managedLigneReleveVentes.numeroLigneRelVentes === '' ||
  this.managedLigneReleveVentes.numeroLigneRelVentes=== null ) {
      this.openSnackBar('Numéro de Ligne Non Spécifié !');
    } else {
  this.managedLigneReleveVentes.numeroReleveVentes = this.releveVentes.numeroReleveVente;
  if (this.releveVentes.listLignesRelVentes.length === 0) {
  this.releveVentes.listLignesRelVentes.push(this.managedLigneReleveVentes);
  } else  {
    console.log('look for index:');
     const indexOfRow = this.getIndexOfRow(this.managedLigneReleveVentes);
     console.log(indexOfRow);
     
     {
           if (indexOfRow === -1) {
    this.releveVentes.listLignesRelVentes.push(this.managedLigneReleveVentes);

         } else  {
     this.releveVentes.listLignesRelVentes[indexOfRow] = this.managedLigneReleveVentes;

       }
      }
  }

  this.transformDataToByConsumedByMatTable() ;
  this.managedLigneReleveVentes = new LigneReleveVente();
  this.manageLigneMatExpansionPanel.close();
  this.listLignesMatExpansionPanel.open();
}
}

/*masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataLignesFactureVentesSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataLignesFactureVentesSource.data.length;
  return numSelected === numRows;
}*/
 /* deleteRow(i)
  {
    
    this.factureVente.montantHT = +this.factureVente.montantHT - this.lignesFactures[i].montantHT;
    this.factureVente.montantTVA = +this.factureVente.montantTVA - this.lignesFactures[i].montantTVA ;
    this.factureVente.montantTTC = +this.factureVente.montantTTC  - this.lignesFactures[i].montantTTC ;

    this.factureVente.montantRemise = +this.factureVente.montantRemise  - this.lignesFactures[i].montantRemise ;
      this.factureVente.montantAPayer = this.factureVente.montantTTC ;
      this.factureVente.montantAPayerApresRemise = +this.factureVente.montantAPayer - this.factureVente.montantRemise;
      console.log('after delete');
      console.log( this.factureVente.montantHT);
      console.log( this.factureVente.montantTTC);
      this.lignesFactures.splice(i, 1 );
      /*this.lignesFactures.forEach(function(ligne, index) {
                  ligne.code = index + 1;         
      },
      this);*/
      /*this.editLignesMode.splice(i, 1); 
      this.readOnlyLignes.splice(i, 1); 
      
      this.lignesFactures.push(new LigneFactureVente());
      this.editLignesMode.push(false);
      this.readOnlyLignes.push(false);
      
         
}*/

/*

  validateRow(i)
  {
     this.factureVente.montantHT = +this.factureVente.montantHT + this.lignesFactures[i].montantHT ;
     this.factureVente.montantTVA = +this.factureVente.montantTVA + this.lignesFactures[i].montantTVA ;
     this.factureVente.montantTTC = +this.factureVente.montantTTC  + this.lignesFactures[i].montantTTC ;
     this.factureVente.montantRemise = +this.factureVente.montantRemise  + this.lignesFactures[i].montantRemise ;
     this.factureVente.montantAPayer = this.factureVente.montantTTC ;
     this.factureVente.montantAPayerApresRemise = +this.factureVente.montantAPayer - this.factureVente.montantRemise;
     this.editLignesMode[i] = true;
     this.readOnlyLignes[i] = true;
    }
    editRow(i)
    {
      if (this.readOnlyLignes[i])
      {
        this.readOnlyLignes[i] = false;
      }  else {
       // this.validateRow(i);

        this.recalculMontants();
        this.readOnlyLignes[i] = true;
        
      }
       
    }*/

/*
nextPage()
  {
    this.pagesLignesFacures[this.currentPage - 1] = this.lignesFactures;

    this.pagesReadOnlyLignesFacures[this.currentPage - 1] = this.readOnlyLignes;
    this.pagesEditModeLignesFacures[this.currentPage - 1] = this.editLignesMode;
    
    this.currentPage++;
    if ( this.pagesLignesFacures[this.currentPage - 1] === undefined) {
    this.initializeListLignesFactures();

console.log('Next Page lignes Factures ');
console.log(this.lignesFactures);
this.pagesLignesFacures[this.currentPage - 1 ] = this.lignesFactures;
this.pagesReadOnlyLignesFacures[this.currentPage - 1] = this.readOnlyLignes;
this.pagesEditModeLignesFacures[this.currentPage - 1] = this.editLignesMode;


} else {
console.log('Next Page lignes Factures ');
this.lignesFactures = this.pagesLignesFacures[this.currentPage - 1 ];
this.readOnlyLignes = this.pagesReadOnlyLignesFacures[this.currentPage - 1 ];
this.editLignesMode = this.pagesEditModeLignesFacures[this.currentPage - 1 ];

}
this.factureVenteFilter = new FactureVenteFilter(this.listClients, this.listArticles);
const  button: HTMLButtonElement = this.elRef.nativeElement.querySelector('.paginationLabel' )  ;
button.innerText = 'Page ' + this.currentPage + ' / ' + this.pagesLignesFacures.length;
}

previousPage()
{
this.pagesLignesFacures[this.currentPage - 1] = this.lignesFactures ;
this.pagesReadOnlyLignesFacures[this.currentPage - 1] = this.readOnlyLignes;
this.pagesEditModeLignesFacures[this.currentPage - 1] = this.editLignesMode;

this.currentPage--;
this.lignesFactures =  this.pagesLignesFacures[this.currentPage - 1];
this.readOnlyLignes = this.pagesReadOnlyLignesFacures[this.currentPage - 1];
this.editLignesMode = this.pagesEditModeLignesFacures[this.currentPage - 1];

this.factureVenteFilter = new FactureVenteFilter(this.listClients, this.listArticles);
const  button: HTMLButtonElement = this.elRef.nativeElement.querySelector('.paginationLabel' )  ;
button.innerText = 'Page ' + this.currentPage + ' / ' + this.pagesLignesFacures.length;

}*/

  /*fillDataLignesFactures()
  {
    this.factureVente.listLignesFactures = [];
     this.listLignesFactureVentes.forEach(function(ligne, index) {
           if ( ligne.montantHT !== 0 ) {
             if (ligne.code === undefined || ligne.code === '')
              {
                ligne.code = this.factureVente.listLignesFactures.length + 1;
              }
            this.factureVente.listLignesFactures.push(ligne);
           }
    
     }, this);
  }*/
  /*checkOneActionInvoked() {
  const listSelectedLignesFactureVentes: LigneFactureVente[] = [];
console.log(this.selection.selected);
if (this.selection.selected.length !== 0 ) {
 this.managedLigneFactureVentes.numeroLigneFactureVentes =  this.selection.selected[this.selection.selected.length - 1]
 .numeroLigneFactureVentes;
 this.managedLigneFactureVentes.article.code =  this.selection.selected[this.selection.selected.length - 1].article;
 this.managedLigneFactureVentes.magasin.code =  this.selection.selected[this.selection.selected.length - 1].magasin;
 this.managedLigneFactureVentes.unMesure.code =  this.selection.selected[this.selection.selected.length - 1].unMesure;
 this.managedLigneFactureVentes.quantite =  this.selection.selected[this.selection.selected.length - 1].quantite;
 this.managedLigneFactureVentes.prixUnitaire=  this.selection.selected[this.selection.selected.length - 1].prixUnitaire;
 this.managedLigneFactureVentes.montantHT =  this.selection.selected[this.selection.selected.length - 1].montantHT;
 this.managedLigneFactureVentes.montantTTC =  this.selection.selected[this.selection.selected.length - 1].montantTTC;
 this.managedLigneFactureVentes.tva =  this.selection.selected[this.selection.selected.length - 1].tva; 
 this.managedLigneFactureVentes.montantTVA =  this.selection.selected[this.selection.selected.length - 1].montantTVA;
 this.managedLigneFactureVentes.fodec =  this.selection.selected[this.selection.selected.length - 1].fodec;
 this.managedLigneFactureVentes.montantFodec =  this.selection.selected[this.selection.selected.length - 1].montantFodec;
 this.managedLigneFactureVentes.remise =  this.selection.selected[this.selection.selected.length - 1].remise;
 this.managedLigneFactureVentes.montantRemise=  this.selection.selected[this.selection.selected.length - 1].montantRemise;

 this.managedLigneFactureVentes.otherTaxe1 =  this.selection.selected[this.selection.selected.length - 1].otherTaxe1;
 this.managedLigneFactureVentes.montantOtherTaxe1 =  this.selection.selected[this.selection.selected.length - 1].montantOtherTaxe1;
 
 this.managedLigneFactureVentes.otherTaxe2 =  this.selection.selected[this.selection.selected.length - 1].otherTaxe2;
 this.managedLigneFactureVentes.montantOtherTaxe2=  this.selection.selected[this.selection.selected.length - 1].montantOtherTaxe2;

 this.managedLigneFactureVentes.montantBenefice=  this.selection.selected[this.selection.selected.length - 1].montantBenefice;
 this.managedLigneFactureVentes.benefice=  this.selection.selected[this.selection.selected.length - 1].benefice;
 
 
const codes: string[] = this.selection.selected.map((ligneFactElement) => {
  return ligneFactElement.code;
});
this.factureVente.listLignesFactures.forEach(LigneF => {
  if (codes.findIndex(code => code === LigneF.code) > -1) {
    listSelectedLignesFactureVentes.push(LigneF);
  }
  });
}
}*/
  fillDataLibelleUnMesure()
  {

  }
  fillDataLibelleMagasins()
  {
    
  }
  fillDataLibelleArticle()  {
    
      this.listArticles.forEach((article, index) => {
                 if (this.managedLigneReleveVentes.article.code === article.code) {
                  this.managedLigneReleveVentes.article.designation = article.designation;
                  this.managedLigneReleveVentes.article.libelle = article.libelle;
                  
                  }
    
            } , this);
   }

}
