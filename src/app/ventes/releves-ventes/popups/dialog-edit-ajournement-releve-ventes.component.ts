
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ReleveVenteService} from '../releve-vente.service';
import {AjournementReleveVentes} from '../ajournement-releve-ventes.model';

@Component({
    selector: 'app-dialog-edit-ajournement-releve-ventes',
    templateUrl: 'dialog-edit-ajournement-releve-ventes.component.html',
  })
  export class DialogEditAjournementReleveVentesComponent implements OnInit {
     private ajournementReleveVentes: AjournementReleveVentes;
     private updateEnCours = false;
     
     constructor(  private releveVenteService: ReleveVenteService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {

        this.releveVenteService.getAjournementReleveVentes(this.data.codeAjournementReleveVentes).subscribe((ajour) => {
            this.ajournementReleveVentes = ajour;
     });


    }
    updateAjournementReleveVentes() 
    {
     this.updateEnCours = true;
       this.releveVenteService.updateAjournementReleveVentes(this.ajournementReleveVentes).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.openSnackBar(  'Ajournement Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
