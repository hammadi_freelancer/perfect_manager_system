
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ReleveVenteService} from '../releve-vente.service';
import {DocumentReleveVentes} from '../document-releve-ventes.model';
import { DocumentReleveVentesElement } from '../document-releve-ventes.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-documents-releves-ventes',
    templateUrl: 'dialog-list-documents-releves-ventes.component.html',
  })
  export class DialogListDocumentsRelevesVentesComponent implements OnInit {
     private listDocumentsRelevesVentes: DocumentReleveVentes[] = [];
     private  dataDocumentsRelevesVentes: MatTableDataSource<DocumentReleveVentesElement>;
     private selection = new SelectionModel<DocumentReleveVentesElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private releveVenteService: ReleveVenteService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
     // console.log('code credit in dialog component is :', this.data.codeCredit);
      this.releveVenteService.getDocumentsReleveVentesByCodeReleve(this.data.codeReleveVentes).subscribe((listDocumentRelVentes) => {
        this.listDocumentsRelevesVentes = listDocumentRelVentes;
//console.log('recieving data from backend', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedDocumentsRelVentes: DocumentReleveVentes[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentRElement) => {
    return documentRElement.code;
  });
  this.listDocumentsRelevesVentes.forEach(documentRV => {
    if (codes.findIndex(code => code === documentRV.code) > -1) {
      listSelectedDocumentsRelVentes.push(documentRV);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsRelevesVentes!== undefined) {

      this.listDocumentsRelevesVentes.forEach(docRVente => {
             listElements.push( {code: docRVente.code ,
          dateReception: docRVente.dateReception,
          numeroDocument: docRVente.numeroDocument,
          typeDocument: docRVente.typeDocument,
          description: docRVente.description
        } );
      });
    }
      this.dataDocumentsRelevesVentes= new MatTableDataSource<DocumentReleveVentesElement>(listElements);
      this.dataDocumentsRelevesVentes.sort = this.sort;
      this.dataDocumentsRelevesVentes.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsRelevesVentes.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsRelevesVentes.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsRelevesVentes.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsRelevesVentes.paginator) {
      this.dataDocumentsRelevesVentes.paginator.firstPage();
    }
  }

}
