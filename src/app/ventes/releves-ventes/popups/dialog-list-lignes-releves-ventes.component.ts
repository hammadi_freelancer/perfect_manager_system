
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ReleveVenteService} from '../releve-vente.service';
import {LigneReleveVente} from '../ligne-releve-vente.model';
import { LigneReleveVenteElement } from '../ligne-releve-vente.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-lignes-releve-ventes',
    templateUrl: 'dialog-list-lignes-releves-ventes.component.html',
  })
  export class DialogListLignesRelevesVentesComponent implements OnInit {
     private listLignesRelevesVentes: LigneReleveVente[] = [];
     private  dataLignesReleveVentesSource: MatTableDataSource<LigneReleveVenteElement>;
     private selection = new SelectionModel<LigneReleveVenteElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private releveVentesService: ReleveVenteService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      // console.log('code credit in dialog component is :', this.data.codeFactureVentes);
    /*  this.releveVentesService.get(this.data.codeFactureVentes)
      .subscribe((listLignesFactVentes) => {
        this.listLignesFactureVentes = listLignesFactVentes;
// console.log('recieving data from backend', this.listPayementsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });*/

  }


  checkOneActionInvoked() {
    const listSelectedLignesReleveVentes: LigneReleveVente[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((ligneReleveVentesElement) => {
    return ligneReleveVentesElement.code;
  });
  this.listLignesRelevesVentes.forEach(ligneRVentes => {
    if (codes.findIndex(code => code === ligneRVentes.code) > -1) {
      listSelectedLignesReleveVentes.push(ligneRVentes);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listLignesRelevesVentes !== undefined) {
 
      this.listLignesRelevesVentes.forEach(ligneRelVentes => {
             listElements.push( 
               {code: ligneRelVentes.code ,
                numeroLigneRelVentes: ligneRelVentes.numeroLigneRelVentes,
          article: ligneRelVentes.article.code,
          quantite: ligneRelVentes.quantite,
          prixUnitaire: ligneRelVentes.prixUnt,
          prixTotal: ligneRelVentes.prixTotal,
          unMesure: ligneRelVentes.unMesure.code,
          beneficeTotal: ligneRelVentes.beneficeTotal,
          livre: ligneRelVentes.livre,
          regle: ligneRelVentes.regle,
          
          
        } );
      });
    }
      this.dataLignesReleveVentesSource= new MatTableDataSource<LigneReleveVenteElement>(listElements);
      this.dataLignesReleveVentesSource.sort = this.sort;
      this.dataLignesReleveVentesSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsDefinitions = [{name: 'numeroLigneRelVentes' , displayTitle: 'N°'},
    {name: 'article' , displayTitle: 'Article'},
    {name: 'unMesure' , displayTitle: 'Un Mesure'},
    {name: 'quantite' , displayTitle: 'Quantite'},
    {name: 'prixUnt' , displayTitle: 'P.Unt(TTC)'},
    {name: 'prixTotal' , displayTitle: 'P.TOT(TTC)'},
    {name: 'beneficeTotal' , displayTitle: 'Benefice TOT'},
    {name: 'regle' , displayTitle: 'Réglé'},
    {name: 'livre' , displayTitle: 'Livré'},
     ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataLignesReleveVentesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataLignesReleveVentesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataLignesReleveVentesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataLignesReleveVentesSource.paginator) {
      this.dataLignesReleveVentesSource.paginator.firstPage();
    }
  }

}
