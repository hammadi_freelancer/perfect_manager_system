
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ReleveVenteService} from '../releve-vente.service';
import {PayementReleveVentes} from '../payement-releve-ventes.model';
import { PayementReleveVentesElement } from '../payement-releve-ventes.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-payements-releves-ventes',
    templateUrl: 'dialog-list-payements-releves-ventes.component.html',
  })
  export class DialogListPayementsRelevesVentesComponent implements OnInit {
     private listPayementsReleveVentes: PayementReleveVentes[] = [];
     private  dataPayementsReleveVentesSource: MatTableDataSource<PayementReleveVentesElement>;
     private selection = new SelectionModel<PayementReleveVentesElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private releveVentesService: ReleveVenteService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      // console.log('code credit in dialog component is :', this.data.codeFactureVentes);
      this.releveVentesService.getPayementsRelevesVentesByCodeReleve(this.data.codeReleveVentes)
      .subscribe((listPayementsRelVentes) => {
        this.listPayementsReleveVentes = listPayementsRelVentes;
// console.log('recieving data from backend', this.listPayementsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedPayementsReleveVentes: PayementReleveVentes[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((payementRelVentesElement) => {
    return payementRelVentesElement.code;
  });
  this.listPayementsReleveVentes.forEach(payementRVentes => {
    if (codes.findIndex(code => code === payementRVentes.code) > -1) {
      listSelectedPayementsReleveVentes.push(payementRVentes);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listPayementsReleveVentes !== undefined) {

      this.listPayementsReleveVentes.forEach(payementRelVentes => {
             listElements.push( {code: payementRelVentes.code ,
          dateOperation: payementRelVentes.dateOperation,
          montantPaye: payementRelVentes.montantPaye,
          modePayement: payementRelVentes.modePayement,
          numeroCheque: payementRelVentes.numeroCheque,
          dateCheque: payementRelVentes.dateCheque,
          compte: payementRelVentes.compte,
          compteAdversaire: payementRelVentes.compteAdversaire,
          banque: payementRelVentes.banque,
          banqueAdversaire: payementRelVentes.banqueAdversaire,
          description: payementRelVentes.description
        } );
      });
    }
      this.dataPayementsReleveVentesSource = new MatTableDataSource<PayementReleveVentesElement>(listElements);
      this.dataPayementsReleveVentesSource.sort = this.sort;
      this.dataPayementsReleveVentesSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'}, 
     {name: 'montantPaye' , displayTitle: 'Montant Payé'},
     {name: 'modePayement' , displayTitle: 'Mode Payement'},
      {name: 'numeroCheque' , displayTitle: 'N°Chèque'},
     {name: 'dateCheque' , displayTitle: 'Date Chèque'}, 
     {name: 'compte' , displayTitle: 'Compte'},
     {name: 'compteAdversaire' , displayTitle: 'Compte Adv.'},
      {name: 'banque' , displayTitle: 'Banque'},
       { name : 'banqueAdversaire' , displayTitle : 'Banque Adv.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataPayementsReleveVentesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataPayementsReleveVentesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataPayementsReleveVentesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataPayementsReleveVentesSource.paginator) {
      this.dataPayementsReleveVentesSource.paginator.firstPage();
    }
  }

}
