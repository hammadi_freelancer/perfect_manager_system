
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ReleveVenteService} from '../releve-vente.service';
import {AjournementReleveVentes} from '../ajournement-releve-ventes.model';

@Component({
    selector: 'app-dialog-add-ajournement-releve-ventes',
    templateUrl: 'dialog-add-ajournement-releve-ventes.component.html',
  })
  export class DialogAddAjournementReleveVentesComponent implements OnInit {
     private ajournementReleveVentes: AjournementReleveVentes;
     private saveEnCours = false;
     constructor(  private releveVenteService: ReleveVenteService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.ajournementReleveVentes = new AjournementReleveVentes;
    }
    saveAjournementReleveVentes() 
    {
      this.ajournementReleveVentes.codeReleveVentes = this.data.codeReleveVentes;
      this.saveEnCours = true;
      // console.log(this.ajournementCredit);
      //  this.saveEnCours = true;
       this.releveVenteService.addAjournementReleveVentes(this.ajournementReleveVentes).subscribe((response) => {
        this.saveEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.ajournementReleveVentes = new AjournementReleveVentes;
             this.openSnackBar(  'Ajournement Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
