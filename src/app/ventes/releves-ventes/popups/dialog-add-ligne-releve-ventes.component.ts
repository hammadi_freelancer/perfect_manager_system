
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ReleveVenteService} from '../releve-vente.service';
import {PayementReleveVentes} from '../payement-releve-ventes.model';
import {LigneReleveVente} from '../ligne-releve-vente.model';

@Component({
    selector: 'app-dialog-add-ligne-releve-ventes',
    templateUrl: 'dialog-add-ligne-releve-ventes.component.html',
  })
  export class DialogAddLigneReleveVentesComponent implements OnInit {
     private ligneReleveVentes: LigneReleveVente;
     private listCodesArticles: string[];
     private listCodesMagasins: string[];
     private listCodesUnMesures: string[];
     
     private etatsLignes: any[] = [
      {value: 'Annule', viewValue: 'Annulé'},
      {value: 'Initial', viewValue: 'Initial'},
      {value: 'Valide', viewValue: 'Validé'}
    ];
     constructor(  private  releveVentesService: ReleveVenteService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.ligneReleveVentes = new LigneReleveVente();
           this.listCodesArticles = this.data.listCodesArticles;
           // this.listCodesMagasins = this.data.listCodesMagasins;
           
          // this.ligneReleveVentes.etat = 'Initial';
           
    }
    saveLigneReleveVentes() 
    {
      this.ligneReleveVentes.codeReleveVentes = this.data.codeReleveVentes;
      //  console.log(this.ligneFactureVentes);
      //  this.saveEnCours = true;
/*    this.releveVentesService.addLigne(this.ligneReleveVentes).subscribe((response) => {
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.ligneReleveVentes = new LigneReleveVente();
             this.openSnackBar(  'Ligne Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });*/
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
