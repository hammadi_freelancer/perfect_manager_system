
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ReleveVenteService} from '../releve-vente.service';
import {ReleveVente} from '../releve-vente.model';
import { PayementReleveVentes } from '../payement-releve-ventes.model';
@Component({
    selector: 'app-dialog-edit-payement-releve-ventes',
    templateUrl: 'dialog-edit-payement-releve-ventes.component.html',
  })
  export class DialogEditPayementReleveVentesComponent implements OnInit {

     private payementReleveVentes: PayementReleveVentes;
     private updateEnCours = false;
     constructor(  private releveVentesService: ReleveVenteService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit() {
        this.releveVentesService.getPayementReleveVentes(this.data.codePayementReleveVentes).subscribe((payement) => {
            this.payementReleveVentes = payement;
     });
   }
    updatePayementReleveVentes() {
      //  this.saveEnCours = true;
      this.updateEnCours = true;
       this.releveVentesService.updatePayementReleveVentes(this.payementReleveVentes).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             // this.payementCredit = new PayementCredit();
             this.openSnackBar(  'Payement Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
