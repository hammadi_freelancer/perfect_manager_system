
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ReleveVenteService} from '../releve-vente.service';
import {AjournementReleveVentes} from '../ajournement-releve-ventes.model';
import { AjournementReleveVenteElement } from '../ajournement-releve-ventes.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-ajournements-releves-ventes',
    templateUrl: 'dialog-list-ajournements-releves-ventes.component.html',
  })
  export class DialogListAjournementsRelevesVentesComponent implements OnInit {
     private listAjournementRelevesVentes: AjournementReleveVentes[] = [];
     private  dataAjournementsRelevesVentesSource: MatTableDataSource<AjournementReleveVenteElement>;
     private selection = new SelectionModel<AjournementReleveVenteElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private releveVenteService: ReleveVenteService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      // console.log('code credit in dialog component is :', this.data.codeCredit);
      this.releveVenteService.getAjournementsRelevesVentesByCodeReleve(this.data.codeFactureVentes)
      .subscribe((listAjournementsRelVentes) => {
        this.listAjournementRelevesVentes = listAjournementsRelVentes;
// console.log('recieving data from backend', this.listAjournementCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedAjournementsReleveVentes: AjournementReleveVentes[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((ajouRelElement) => {
    return ajouRelElement.code;
  });
  this.listAjournementRelevesVentes.forEach(ajournementRelVentes => {
    if (codes.findIndex(code => code === ajournementRelVentes.code) > -1) {
      listSelectedAjournementsReleveVentes.push(ajournementRelVentes);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listAjournementRelevesVentes !== undefined) {

      this.listAjournementRelevesVentes.forEach(ajournementRVentes=> {
             listElements.push( {code: ajournementRVentes.code ,
          dateOperation: ajournementRVentes.dateOperation,
          nouvelleDatePayement: ajournementRVentes.nouvelleDatePayement,
          motif: ajournementRVentes.motif,
          description: ajournementRVentes.description
        } );
      });
    }
      this.dataAjournementsRelevesVentesSource = new MatTableDataSource<AjournementReleveVenteElement>(listElements);
      this.dataAjournementsRelevesVentesSource.sort = this.sort;
      this.dataAjournementsRelevesVentesSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'},
     {name: 'nouvelleDatePayement' , displayTitle: 'N°elle Date Pay.'},
     {name: 'motif' , displayTitle: 'Motif'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataAjournementsRelevesVentesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataAjournementsRelevesVentesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataAjournementsRelevesVentesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataAjournementsRelevesVentesSource.paginator) {
      this.dataAjournementsRelevesVentesSource.paginator.firstPage();
    }
  }

}
