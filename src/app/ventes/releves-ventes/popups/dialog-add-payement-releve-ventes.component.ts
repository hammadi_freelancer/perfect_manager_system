
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ReleveVenteService} from '../releve-vente.service';
import {PayementReleveVentes} from '../payement-releve-ventes.model';

@Component({
    selector: 'app-dialog-add-payement-releve-ventes',
    templateUrl: 'dialog-add-payement-releve-ventes.component.html',
  })
  export class DialogAddPayementReleveVentesComponent implements OnInit {
     private payementReleveVentes: PayementReleveVentes;
     
     constructor(  private  releveVentesService: ReleveVenteService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.payementReleveVentes = new PayementReleveVentes();
    }
    savePayementReleveVentes() 
    {
      this.payementReleveVentes.codeReleveVentes= this.data.codeReleveVentes;
      // console.log(this.payementFactureVentes);
      //  this.saveEnCours = true;
       this.releveVentesService.addPayementReleveVentes(this.payementReleveVentes).subscribe((response) => {
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.payementReleveVentes = new PayementReleveVentes();
             this.openSnackBar(  'Payement Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
