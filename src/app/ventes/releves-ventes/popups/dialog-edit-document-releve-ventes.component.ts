
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ReleveVenteService} from '../releve-vente.service';
import {DocumentReleveVentes} from '../document-releve-ventes.model';

@Component({
    selector: 'app-dialog-edit-document-releve-ventes',
    templateUrl: 'dialog-edit-document-releve-ventes.component.html',
  })
  export class DialogEditDocumentReleveVentesComponent implements OnInit {
     private documentReleveVentes: DocumentReleveVentes;
     private updateEnCours = false;
     private typesDoc: any[] = [
      {value: 'Cheque', viewValue: 'Chèque'},
      {value: 'Traite', viewValue: 'Traite'},
      {value: 'FactureVentes', viewValue: 'Facture Ventes'},
      {value: 'BonLivraison', viewValue: 'Bon Livraison'},
      {value: 'ReçuPayement', viewValue: 'Reçu Payement'},
      {value: 'Autre', viewValue: 'Autre'},

    ];
     constructor(  private releveVentesService: ReleveVenteService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {

        this.releveVentesService.getDocumentReleveVentes(this.data.codeDocumentReleveVentes).subscribe((doc) => {
            this.documentReleveVentes = doc;
     });
    }
    
    updateDocumentReleveVentes() 
    {
      this.updateEnCours = true;
       this.releveVentesService.updateDocumentReleveVentes(this.documentReleveVentes).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.openSnackBar(  'Document Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
     imageFace1Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentReleveVentes.imageFace1 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imageFace2Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentReleveVentes.imageFace2 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
  }
