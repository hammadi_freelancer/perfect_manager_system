
import { BaseEntity } from '../../common/base-entity.model' ;
// import { Tranche } from './tranche.model' ;
import { Client } from '../../ventes/clients/client.model' ;
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { ArticleService } from '../../stocks/articles/article.service';
import { Article } from '../../stocks/articles/article.model';
import { UnMesure } from '../../stocks/un-mesures/un-mesure.model';
import { Magasin } from '../../stocks/magasins/magasin.model';

export const MODES_GENERATION_TRANCHES = ['AUTOMATIQUE', 'MANUELLE', ];

export class ArticleFilter  {
    public selectedCodeArticle: string;
    public selectedCodeMagasin: string;
    public selectedCodeUnM: string;
    

    private controlCodesArticles = new FormControl();
    private controlLibellesArticles = new FormControl();

    
    private controlCodesMagasins = new FormControl();
    private controlLibellesMagasins = new FormControl();

    
    private controlCodesUnMesures = new FormControl();
    private controlLibellesUnMesures = new FormControl();
    
   //  private controlAdresse = new FormControl();
    private listCodesArticles: string[] = [];
    private listCodesMagasins: string[] = [];
    private listCodesUnMesures: string[] = [];
    
    private listLibellesArticles: string[] = [];
    private listLibellesMagasins: string[] = [];
    private listLibellesUnMesures: string[] = [];

    private filteredCodesArticles: Observable<string[]>;
    private filteredCodesMagasins: Observable<string[]>;
    private filteredCodesUnMesures: Observable<string[]>;
    private filteredLibellesArticles: Observable<string[]>;
    private filteredLibellesMagasins: Observable<string[]>;
    private filteredLibellesUnMesures: Observable<string[]>;
    
   
    
    constructor ( articles: Article[], magasins: Magasin[], unMesures: UnMesure[]) {
      if (articles !== null && articles !== undefined ) {
       //  const listClientsMorals = clients.filter((client) => (client.categoryClient === 'PERSONNE_MORALE'));
        // const listClientsPhysiques = clients.filter((client) => (client.categoryClient === 'PERSONNE_PHYSIQUE'));
        this.listCodesArticles = articles.map((article) => (article.code));
        this.listLibellesArticles = articles.filter((article) =>
        (article.libelle !== undefined && article.libelle != null )).map((article) => (article.libelle ));
      }
      if (magasins !== null && magasins !== undefined) {
        //  const listClientsMorals = clients.filter((client) => (client.categoryClient === 'PERSONNE_MORALE'));
         // const listClientsPhysiques = clients.filter((client) => (client.categoryClient === 'PERSONNE_PHYSIQUE'));
         this.listCodesMagasins = magasins.map((magasin) => (magasin.code));
         this.listLibellesMagasins = magasins.filter((magasin) =>
         (magasin.libelle !== undefined && magasin.libelle != null )).map((magasin) => (magasin.libelle ));
       }
        

       if (unMesures !== null && unMesures !== undefined ) {
         this.listCodesUnMesures = unMesures.map((unMesure) => (unMesure.code));
         this.listLibellesUnMesures = unMesures.filter((unMesure) =>
         (unMesure.libelle !== undefined && unMesure.libelle != null )).map((unMesure) => (unMesure.libelle ));
       }


     
    

      this.filteredCodesArticles = this.controlCodesArticles.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterCodesArticles(value))
        );
        this.filteredLibellesArticles = this.controlLibellesArticles.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterLibellesArticles(value))
        );
        this.filteredCodesMagasins = this.controlCodesMagasins.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterCodesMagasins(value))
        );
        this.filteredLibellesMagasins = this.controlLibellesMagasins.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterLibellesMagasins(value))
        );
        this.filteredCodesUnMesures = this.controlCodesUnMesures.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterCodesUnMesures(value))
        );
        this.filteredLibellesUnMesures = this.controlLibellesUnMesures.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterLibellesUnMesures(value))
        );
     
 
    }

  
    private _filterCodesArticles(value: string): string[] {
        const filterValue = value.toLowerCase();
        return this.listCodesArticles.filter(codeArt => codeArt.toLowerCase().includes(filterValue));
      }
      private _filterLibellesArticles(value: string): string[] {
        const filterValue = value.toLowerCase();
        return this.listLibellesArticles.filter(libelleArt => libelleArt.toLowerCase().includes(filterValue));
      }
      private _filterCodesMagasins(value: string): string[] {
        const filterValue = value.toLowerCase();
        return this.listCodesMagasins.filter(codeMG => codeMG.toLowerCase().includes(filterValue));
      }
      private _filterLibellesMagasins(value: string): string[] {
        const filterValue = value.toLowerCase();
        return this.listLibellesMagasins.filter(libelleMG => libelleMG.toLowerCase().includes(filterValue));
      }
      private _filterCodesUnMesures(value: string): string[] {
        const filterValue = value.toLowerCase();
        return this.listCodesUnMesures.filter(codeUN => codeUN.toLowerCase().includes(filterValue));
      }
      private _filterLibellesUnMesures(value: string): string[] {
        const filterValue = value.toLowerCase();
        return this.listLibellesUnMesures.filter(codeUN => codeUN.toLowerCase().includes(filterValue));
      }
 
      public getControlCodesArticles(): FormControl {
        return this.controlCodesArticles;
      }
      public getControlCodesMagasins(): FormControl {
        return this.controlCodesMagasins;
      }
      public getControlCodesUnMesures(): FormControl {
        return this.controlCodesUnMesures;
      }

      public getControlLibelleArticles(): FormControl {
        return this.controlLibellesArticles;
      }
      public getControlLibelleMagasins(): FormControl {
        return this.controlLibellesMagasins;
      }
      public getControlLibelleUnMesures(): FormControl {
        return this.controlLibellesUnMesures;
      }


      public getFiltredCodesArticles(): Observable<string[]> {
        return this.filteredCodesArticles;
      }
      public getFiltredLibellesArticles(): Observable<string[]> {
        return this.filteredLibellesArticles;
      }
      public getFiltredCodesMagasins(): Observable<string[]> {
        return this.filteredCodesMagasins;
      }
      public getFiltredLibellesMagasins(): Observable<string[]> {
        return this.filteredLibellesMagasins;
      }
   
      public getFiltredCodesUnMesures(): Observable<string[]> {
        return this.filteredCodesUnMesures;
      }
      public getFiltredLibellesUnMesures(): Observable<string[]> {
        return this.filteredLibellesUnMesures;
      }

}





