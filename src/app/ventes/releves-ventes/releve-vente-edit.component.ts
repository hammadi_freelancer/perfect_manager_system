import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
// import { CommunicationService} from './communication.service';
import { MatDatepickerInput } from '@angular/material/datepicker';
import { MatSnackBar } from '@angular/material';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {ArticleService} from '../../stocks/articles/article.service';
import {Article} from '../../stocks/articles/article.model';
import { MatExpansionPanel } from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {LigneFactureVenteElement} from '../factures-ventes/ligne-facture-vente.interface';
import {SelectionModel} from '@angular/cdk/collections';
import {Magasin} from '../../stocks/magasins/magasin.model';
import {ModePayementLibelle} from './mode-payement-libelle.interface';
import {FactureVente} from '../factures-ventes/facture-vente.model';
import { ParametresVentes } from '../parametres-ventes.model';

import {FactureVenteService} from '../factures-ventes/facture-vente.service';
import {ReleveVenteService} from './releve-vente.service';

import {VentesService} from '../ventes.service';
import { ClientService } from '../clients/client.service';
import { ClientFilter } from '../clients/client.filter';
import { ArticleFilter } from './article.filter';
import { UnMesureFilter } from '../../stocks/un-mesures/un-mesures.filter';

import {Client} from '../clients/client.model';
import {UnMesure} from '../../stocks/un-mesures/un-mesure.model';

import {ReleveVente} from '../releves-ventes/releve-vente.model';
import {LigneReleveVente} from '../releves-ventes/ligne-releve-vente.model';

import {LigneReleveVenteElement} from '../releves-ventes/ligne-releve-vente.interface';

@Component({
  selector: 'app-releve-vente-edit',
  templateUrl: './releve-vente-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class ReleveVenteEditComponent implements OnInit  {

  private releveVentes: ReleveVente =  new ReleveVente();
  private updateEnCours = false;
  private tabsDisabled = true;
  private managedLigneReleveVentes = new LigneReleveVente();
  private selection = new SelectionModel<LigneReleveVenteElement>(true, []);
  @ViewChild(MatPaginator) paginator: MatPaginator; 
  @ViewChild(MatSort) sort: MatSort;
  private columnsDefinitions: ColumnDefinition[] = [];
  private columnsTitles: string[] = [];
  private columnsTitlesAr: string[] = [];
  private columnsNames: string[] = [];
  
@Input()
private listRelevesVentes: any = [] ;

@ViewChild('firstMatExpansionPanel')
private firstMatExpansionPanel : MatExpansionPanel;

@ViewChild('manageLigneMatExpansionPanel')
private manageLigneMatExpansionPanel : MatExpansionPanel;

@ViewChild('listLignesMatExpansionPanel')
private listLignesMatExpansionPanel : MatExpansionPanel;

// private listCodesArticles: string[];
// private lignesFactures: LigneFactureVente[] = [ ];
// private listTranches: Tranche[] = [] ;
// private factureVenteFilter: FactureVenteFilter;
private articleFilter: ArticleFilter;
private listClients: Client[];
private listArticles: Article[];

private clientFilter: ClientFilter;
private unMesureFilter : UnMesureFilter;
// private listArticles: Article[];
// private listLignesFactureVentes: LigneFactureVente[] = [ ];
private  dataLignesReleveVentesSource: MatTableDataSource<LigneReleveVenteElement>;

// private editLignesMode: boolean [] = [];

private etatsReleveVentes: any[] = [
  {value: 'Annule', viewValue: 'Annulé'},
  {value: 'Initial', viewValue: 'Initial'},
  {value: 'Valide', viewValue: 'Validé'}
];
/*private etatsLigneFactureVentes: any[] = [
  {value: 'Regle', viewValue: 'Réglée'},
  {value: 'NonRegle', viewValue: 'NonRéglée'},
  {value: 'PartiellementRegle', viewValue: 'Partiellement Réglée'}
];*/
private payementModes: ModePayementLibelle[] = [
  {value: 'Comptant', viewValue: 'Comptant'},
  {value: 'Cheque', viewValue: 'Chèque'},
  {value: 'Credit', viewValue: 'Crédit'},
  {value: 'Credit_Comptant', viewValue: 'Crédit/Comptant'}
];
  constructor( private route: ActivatedRoute,
    private router: Router, private releveVenteService: ReleveVenteService,
    private articleService: ArticleService, private dialog: MatDialog,
    private matSnackBar: MatSnackBar, private elRef: ElementRef,
   ) {
  }
  ngOnInit() {
    this.listClients = this.route.snapshot.data.clients;
    this.listArticles = this.route.snapshot.data.articles;
    
    this.clientFilter = new ClientFilter(this.listClients);
    this.articleFilter = new ArticleFilter(this.route.snapshot.data.articles,
    [], [] );
     this.unMesureFilter = new UnMesureFilter(this.route.snapshot.data.unMesures);
 
    this.articleFilter = new ArticleFilter(this.route.snapshot.data.articles,
      [], []);
  
    const codeReleveVentes = this.route.snapshot.paramMap.get('codeReleveVentes');

    this.specifyListColumnsToBeDisplayed();
    
    this.releveVenteService.getReleveVente(codeReleveVentes).subscribe((releveVente) => {
             this.releveVentes = releveVente;
             if (this.releveVentes.etat === 'Valide')
              {
                this.tabsDisabled = false;
              }
             if (this.releveVentes.etat === 'Valide' || this.releveVentes.etat === 'Annule')
              {
                this.etatsReleveVentes.splice(1, 1);
              }
              if (this.releveVentes.etat === 'Initial')
                {
                  this.etatsReleveVentes.splice(0, 1);
                }
                this.transformDataToByConsumedByMatTable();
            });
            this.firstMatExpansionPanel.open();

           
           // this.initializeListLignesFactures();
           /*this.lignesFactures.forEach(function(ligne, index) {
                          ligne.code =  String(index + 1);
           });*/
          // this.releveVentes.etat = 'Initial';
        /*   this.ventesService.getParametresVentes().subscribe((response) => {
             if (response !==  undefined) {
               // this.save.emit(response);
               this.parametresVentes = response;
               
             } else {
              // show error message
             }
         });*/




            
  }
  loadGridRelevesVentes() {
    this.router.navigate(['/pms/ventes/relevesVentes']) ;
  }
updateReleveVente() {

  //console.log(this.factureVente);
  let noClient = false;
  let noRow = false;
  if (this.releveVentes.etat === 'Valide' )  {
    if ( this.releveVentes.client.cin === null ||  this.releveVentes.client.cin === undefined)  {
        noClient = true;
        
      }
  if (this.releveVentes.listLignesRelVentes[0] === undefined
    || this.releveVentes.listLignesRelVentes[0].quantite === 0  ) {
      noRow = true;
    }
  }
        if (noRow)  {
          this.openSnackBar( 'La Relevée doit avoir au moins une ligne valide !');
          
        } else {
  this.updateEnCours = true;
  this.releveVenteService.updateReleveVente(this.releveVentes).subscribe((response) => {
    this.updateEnCours = false;
    if (response.error_message ===  undefined) {
      console.log('the response of update is ', response );
      if (this.releveVentes.etat === 'Valide')
        {
          this.tabsDisabled = false;
          this.etatsReleveVentes = [
            {value: 'Annule', viewValue: 'Annulé'},
            {value: 'Valide', viewValue: 'Validé'},
            {value: 'Reglee', viewValue: 'Réglée'},
            {value: 'Valide', viewValue: 'Partiellement Réglée'},
            
  
          ];
        }
        if (this.releveVentes.etat === 'Annule')
          {
            this.tabsDisabled = true;
         
          }

      this.openSnackBar( ' Relevée de Vente mise à jour ');
      //  this.save.emit(response);
      // this.client = new Client();
      // this.modeUpdate = false;
    } else {
      this.openSnackBar( ' Erreurs:Opération Echouée ');
     // show error message
    }
});
        }
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 
 fillDataNomPrenom()
 {
   const listFiltred  = this.listClients.filter((client) => (client.cin === this.releveVentes.client.cin));
   this.releveVentes.client.nom = listFiltred[0].nom;
   this.releveVentes.client.prenom = listFiltred[0].prenom;
 }
 fillDataMatriculeRaison()
 {
   const listFiltred  = this.listClients.filter((client) => (client.registreCommerce === this.releveVentes.client.registreCommerce));
   this.releveVentes.client.matriculeFiscale = listFiltred[0].matriculeFiscale;
   this.releveVentes.client.raisonSociale = listFiltred[0].raisonSociale;
 }


 transformDataToByConsumedByMatTable() {
   console.log('list lignes rel ventes recievdd from the server is :',
   this.releveVentes.listLignesRelVentes );
  const listElements = [];
  if (this.releveVentes.listLignesRelVentes !== undefined) {
    this.releveVentes.listLignesRelVentes.forEach(ligneRel => {
           listElements.push( {code: ligneRel.code ,
            numeroLigneRelVentes: ligneRel.numeroLigneRelVentes,
            article: ligneRel.article.code,
             unMesure: ligneRel.unMesure.code,
             quantite: ligneRel.quantite,
             prixUnt: ligneRel.prixUnt,
             prixTotal: ligneRel.prixTotal,
             beneficeTotal: ligneRel.beneficeTotal,
             regle: ligneRel.regle ? 'oui' : 'non',
             livre: ligneRel.livre ? 'oui' : 'non'
      } );
    });
  }
    this.dataLignesReleveVentesSource= new MatTableDataSource<LigneReleveVenteElement>(listElements);
    this.dataLignesReleveVentesSource.sort = this.sort;
    this.dataLignesReleveVentesSource.paginator = this.paginator;
    
    
}
 specifyListColumnsToBeDisplayed() {
   // console.log('calling specifyListColumnsToBeDisplayed documents grid');
  this.columnsTitles = [];
  this.columnsNames = [];
  this.columnsTitlesAr  = [];
  
   this.columnsDefinitions = [{name: 'numeroLigneRelVentes' , displayTitle: 'N°'},
   {name: 'article' , displayTitle: 'Art.'},
   {name: 'unMesure' , displayTitle: 'Un.Mes.'},
   {name: 'quantite' , displayTitle: 'Qté'},
   {name: 'prixUnt' , displayTitle: 'P.Unt(TTC)'},
   {name: 'prixTotal' , displayTitle: 'P.Tot(TTC)'},
   {name: 'beneficeTotal' , displayTitle: 'Bénef.Tot'},
   {name: 'regle' , displayTitle: 'Réglé'},
   {name: 'livre' , displayTitle: 'Livré'},
    ];

   this.columnsTitles = this.columnsDefinitions.map((colDef) => {
             return colDef.displayTitle;
   });
   
   this.columnsNames[0] = 'select';
   this.columnsDefinitions.map((colDef) => {
    this.columnsNames.push(colDef.name);
    console.log(this.columnsNames);
});
   this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
    return colDef.displayTitleAr;
});
}

applyFilter(filterValue: string) {
  this.dataLignesReleveVentesSource.filter = filterValue.trim().toLowerCase();
  if (this.dataLignesReleveVentesSource.paginator) {
    this.dataLignesReleveVentesSource.paginator.firstPage();
  }
}

checkLigneReleveVentes()
{

}
getIndexOfRow(row)
{

    for ( let i = 0; i < this.releveVentes.listLignesRelVentes.length ; i++) {
      if (this.releveVentes.listLignesRelVentes[i].numeroLigneRelVentes === row.numeroLigneRelVentes)
       {
             return i;
        }
    }
    return -1;
}
mapRowGridToObjectLigneReleveVente(rowGrid): LigneReleveVente
{
  const ligneRV  = new  LigneReleveVente() ;
  
  ligneRV.numeroReleveVentes = rowGrid.numeroReleveVentes;
  ligneRV.numeroLigneRelVentes = rowGrid.numeroLigneRelVentes;
  ligneRV.article.code =  rowGrid.article;
  ligneRV.unMesure.code =  rowGrid.unMesure;
  ligneRV.quantite =  rowGrid.quantite;
  ligneRV.prixUnt =  rowGrid.prixUnt;
  ligneRV.prixTotal =  rowGrid.prixTotal;
  ligneRV.beneficeTotal =  rowGrid.beneficeTotal;
  ligneRV.livre =  rowGrid.livre;
  ligneRV.regle =  rowGrid.regle;
  return ligneRV;
}
editLigneReleveVentes(row)
{
  this.managedLigneReleveVentes = this.mapRowGridToObjectLigneReleveVente(row);


    if(row.unMesure === undefined)
      {
        this.managedLigneReleveVentes.unMesure = new UnMesure();
      }
      if(row.article === undefined)
        {
          this.managedLigneReleveVentes.article = new Article();
        }
        console.log('row to edit is ', row);

  this.manageLigneMatExpansionPanel.open();
  
}
deleteLigneReleveVentes(row)
{

  const indexOfRow = this.getIndexOfRow(row);
  this.releveVentes.listLignesRelVentes.splice(indexOfRow , 1 );
  this.transformDataToByConsumedByMatTable();
  
}
addLigneReleveVentes()
{
  if (this.managedLigneReleveVentes.quantite === undefined
    || this.managedLigneReleveVentes.quantite === 0 ||
    this.managedLigneReleveVentes.quantite === null ||
    this.managedLigneReleveVentes.prixUnt === undefined
      || this.managedLigneReleveVentes.prixUnt === 0 ||
      this.managedLigneReleveVentes.prixUnt === null  ||
      this.managedLigneReleveVentes.article.code === undefined  ||
      this.managedLigneReleveVentes.article.code === '' ||
      this.managedLigneReleveVentes.article.code === null 
  ) {
        this.openSnackBar('Donnnées Manquées (QTE/ART/PUNIT!');
    } else if (this.managedLigneReleveVentes.numeroLigneRelVentes === undefined
  || this.managedLigneReleveVentes.numeroLigneRelVentes === '' ||
  this.managedLigneReleveVentes.numeroLigneRelVentes=== null ) {
      this.openSnackBar('Numéro de Ligne Non Spécifié !');
    } else {
  this.managedLigneReleveVentes.numeroReleveVentes = this.releveVentes.numeroReleveVente;
  if (this.releveVentes.listLignesRelVentes.length === 0) {
  this.releveVentes.listLignesRelVentes.push(this.managedLigneReleveVentes);
  } else  {
    console.log('look for index:');
     const indexOfRow = this.getIndexOfRow(this.managedLigneReleveVentes);
     console.log(indexOfRow);
     
     {
           if (indexOfRow === -1) {
    this.releveVentes.listLignesRelVentes.push(this.managedLigneReleveVentes);

         } else  {
     this.releveVentes.listLignesRelVentes[indexOfRow] = this.managedLigneReleveVentes;

       }
      }
  }

  this.transformDataToByConsumedByMatTable() ;
  this.managedLigneReleveVentes = new LigneReleveVente();
  this.manageLigneMatExpansionPanel.close();
  this.listLignesMatExpansionPanel.open();
}
}
calculPrixTotal()
{
  // this.managedLigneReleveVentes.prixTotal = this.
  this.managedLigneReleveVentes.prixTotal  = +  this.managedLigneReleveVentes.prixUnt *
  this.managedLigneReleveVentes.quantite  ;
  
}
selectedTabChanged($event) {
  if ($event.index === 4) { // Composition tab was selected
   // this.loadListArticles();
   } else if ($event.index === 1) {
    //  this.loadListSchemasConfiguration() ;
   }
  }
  fillDataLibelleArticle()  {
    
      this.listArticles.forEach((article, index) => {
                 if (this.managedLigneReleveVentes.article.code === article.code) {
                  this.managedLigneReleveVentes.article.designation = article.designation;
                  this.managedLigneReleveVentes.article.libelle = article.libelle;
                  
                  }
    
            } , this);
   }

}
