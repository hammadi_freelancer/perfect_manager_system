
import { Component, OnInit, Inject, ViewChild, Input, OnChanges} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ReleveVenteService} from './releve-vente.service';
import { ReleveVente} from './releve-vente.model';
import {AjournementReleveVentes} from './ajournement-releve-ventes.model';
import { AjournementReleveVenteElement } from './ajournement-releve-ventes.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import { DialogAddAjournementReleveVentesComponent } from './popups/dialog-add-ajournement-releve-ventes.component';
import { DialogEditAjournementReleveVentesComponent} from './popups/dialog-edit-ajournement-releve-ventes.component';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-ajournements-releves-ventes-grid',
    templateUrl: 'ajournements-releves-ventes-grid.component.html',
  })
  export class AjournementsRelevesVentesGridComponent implements OnInit, OnChanges {
     private listAjournementReleveVentes: AjournementReleveVentes[] = [];
     private  dataAjournementsRelevesVentesSource: MatTableDataSource<AjournementReleveVenteElement>;
     private selection = new SelectionModel<AjournementReleveVenteElement>(true, []);

     @Input()
     private releveVentes : ReleveVente;

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private releveVenteService: ReleveVenteService,
     private matSnackBar: MatSnackBar, public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      this.releveVenteService.getAjournementsRelevesVentesByCodeReleve(this.releveVentes.code).
      subscribe((listAjournementsRelVentes) => {
        this.listAjournementReleveVentes = listAjournementsRelVentes;
// console.log('recieving data from backend', this.listAjournementsFactureVentes  );
        this.transformDataToByConsumedByMatTable();
         });

  }

ngOnChanges()
{
  this.releveVenteService.getAjournementsRelevesVentesByCodeReleve
  (this.releveVentes.code).subscribe((listAjournementsRelVentes) => {
    this.listAjournementReleveVentes = listAjournementsRelVentes;
//console.log('recieving data from backend', this.listAjournementCredits  );
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
     });
}
  checkOneActionInvoked() {
    const listSelectedAjournementsReleveVentes: AjournementReleveVentes[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((ajournementRVentesElement) => {
    return ajournementRVentesElement.code;
  });
  this.listAjournementReleveVentes.forEach(ajournementRVen => {
    if (codes.findIndex(code => code === ajournementRVen.code) > -1) {
      listSelectedAjournementsReleveVentes.push(ajournementRVen);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listAjournementReleveVentes !== undefined) {

      this.listAjournementReleveVentes.forEach(ajournementRVentes => {
             listElements.push( {code: ajournementRVentes.code ,
          dateOperation: ajournementRVentes.dateOperation,
          nouvelleDatePayement: ajournementRVentes.nouvelleDatePayement,
          motif: ajournementRVentes.motif,
          description: ajournementRVentes.description
        } );
      });
    }
      this.dataAjournementsRelevesVentesSource = new MatTableDataSource<AjournementReleveVenteElement>(listElements);
      this.dataAjournementsRelevesVentesSource.sort = this.sort;
      this.dataAjournementsRelevesVentesSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'},
     {name: 'nouvelleDatePayement' , displayTitle: 'N°elle Date Pay.'},
     {name: 'motif' , displayTitle: 'Motif'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataAjournementsRelevesVentesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataAjournementsRelevesVentesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataAjournementsRelevesVentesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataAjournementsRelevesVentesSource.paginator) {
      this.dataAjournementsRelevesVentesSource.paginator.firstPage();
    }
  }

  showAddAjournementDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeReleveVentes: this.releveVentes.code
   };
 
   const dialogRef = this.dialog.open(DialogAddAjournementReleveVentesComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(ajAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditAjournementDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeAjournementFactureVentes : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditAjournementReleveVentesComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(ajAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Ajournement Séléctionnée!');
    }
    }
    supprimerAjournements()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(ajour, index) {
              this.releveVenteService.deleteAjournementReleveVentes(ajour.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar(  ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Ajournement Séléctionnée!');
        }
    }
    refresh() {
      this.releveVenteService.getAjournementsRelevesVentesByCodeReleve(this.releveVentes.code).subscribe((listAjourRel) => {
        this.listAjournementReleveVentes= listAjourRel;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }


}
