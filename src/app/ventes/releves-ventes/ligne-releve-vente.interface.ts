export interface LigneReleveVenteElement {
   code: string ;
   numeroLigneRelVentes: string ;
   article: string;
   unMesure: string;
   quantite: number;
   prixUnt: number;
    prixTotal: number;
    beneficeTotal: number;
    regle: boolean;
    livre: boolean;
    }
