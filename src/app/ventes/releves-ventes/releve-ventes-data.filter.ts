export class ReleveVentesDataFilter {
    numeroReleveVente : string;
    montantBenefice : number;
    montantAPayer : number;
    regleToutes : boolean;
    livreToutes: boolean;
    etat : string;
    dateVenteFromObject : Date;
    dateVenteToObject : Date;
}


