
import { Component, OnInit, Inject, ViewChild, Input, AfterViewChecked, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {ReleveVenteService} from './releve-vente.service';
import { ReleveVente } from './releve-vente.model';
import {PayementReleveVentes} from './payement-releve-ventes.model';
import { PayementReleveVentesElement } from './payement-releve-ventes.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
 import { DialogAddPayementReleveVentesComponent} from './popups/dialog-add-payement-releve-ventes.component';
 import { DialogEditPayementReleveVentesComponent } from './popups/dialog-edit-payement-releve-ventes.component';
@Component({
    selector: 'app-payements-releves-ventes-grid',
    templateUrl: 'payements-releves-ventes-grid.component.html',
  })
  export class PayementsRelevesVentesGridComponent implements OnInit, OnChanges {
     private listPayementsRelevesVentes: PayementReleveVentes[] = [];
     private  dataPayementsRelevesVentesSource: MatTableDataSource<PayementReleveVentesElement>;
     private selection = new SelectionModel<PayementReleveVentesElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];

     @Input()
     private releveVentes: ReleveVente;

     constructor(  private releveVenteService: ReleveVenteService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      // console.log('credit code after dipslay tab ', this.credit.code);
      this.releveVenteService.getPayementsRelevesVentesByCodeReleve(this.releveVentes.code).subscribe((listPayementsRVentes) => {
        this.listPayementsRelevesVentes= listPayementsRVentes;
// console.log('recieving data from backend', this.listPayementsCredits  );
        this.transformDataToByConsumedByMatTable();
         });

  }

  ngOnChanges() {
    this.releveVenteService.getPayementsRelevesVentesByCodeReleve(this.releveVentes.code).subscribe((listPayementsRVentes) => {
      this.listPayementsRelevesVentes = listPayementsRVentes;
// console.log('recieving data from backend', this.listPayementsCredits  );
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }
  checkOneActionInvoked() {
    const listSelectedPayementsRelVentes: PayementReleveVentes[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((payementRelElement) => {
    return payementRelElement.code;
  });
  this.listPayementsRelevesVentes.forEach(payementR => {
    if (codes.findIndex(code => code === payementR.code) > -1) {
      listSelectedPayementsRelVentes.push(payementR);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listPayementsRelevesVentes !== undefined) {

      this.listPayementsRelevesVentes.forEach(payementRelVentes => {
             listElements.push( {code: payementRelVentes.code ,
          dateOperation: payementRelVentes.dateOperation,
          montantPaye: payementRelVentes.montantPaye,
          modePayement: payementRelVentes.modePayement,
          numeroCheque: payementRelVentes.numeroCheque,
          dateCheque: payementRelVentes.dateCheque,
          compte: payementRelVentes.compte,
          compteAdversaire: payementRelVentes.compteAdversaire,
          banque: payementRelVentes.banque,
          banqueAdversaire: payementRelVentes.banqueAdversaire,
          description: payementRelVentes.description
        } );
      });
    }
      this.dataPayementsRelevesVentesSource = new MatTableDataSource<PayementReleveVentesElement>(listElements);
      this.dataPayementsRelevesVentesSource.sort = this.sort;
      this.dataPayementsRelevesVentesSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'}, 
     {name: 'montantPaye' , displayTitle: 'Montant Payé'},
     {name: 'modePayement' , displayTitle: 'Mode Payement'},
      {name: 'numeroCheque' , displayTitle: 'N°Chèque'},
     {name: 'dateCheque' , displayTitle: 'Date Chèque'}, 
     {name: 'compte' , displayTitle: 'Compte'},
     {name: 'compteAdversaire' , displayTitle: 'Compte Adv.'},
      {name: 'banque' , displayTitle: 'Banque'},
       { name : 'banqueAdversaire' , displayTitle : 'Banque Adv.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataPayementsRelevesVentesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataPayementsRelevesVentesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataPayementsRelevesVentesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataPayementsRelevesVentesSource.paginator) {
      this.dataPayementsRelevesVentesSource.paginator.firstPage();
    }
  }

  showAddPayementDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeReleveVentes : this.releveVentes.code
   };
 
   const dialogRef = this.dialog.open(DialogAddPayementReleveVentesComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(payAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditPayementDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codePayementReleveVentes: this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditPayementReleveVentesComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(payAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Paiement Séléctionnée!');
    }
    }
    supprimerPayements()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(pay, index) {
              this.releveVenteService.deletePayementReleveVentes(pay.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Paiement Séléctionnée!');
        }
    }
    refresh() {
      this.releveVenteService.getPayementsRelevesVentesByCodeReleve(this.releveVentes.code).subscribe((listPayementsRels) => {
        this.listPayementsRelevesVentes = listPayementsRels;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
















}
