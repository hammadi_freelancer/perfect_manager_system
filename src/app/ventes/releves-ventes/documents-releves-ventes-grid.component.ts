
import { Component, OnInit, Inject,Input, ViewChild, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {ReleveVenteService} from './releve-vente.service';
import {ReleveVente} from './releve-vente.model';

import {DocumentReleveVentes} from './document-releve-ventes.model';
import { DocumentReleveVentesElement } from './document-releve-ventes.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogAddDocumentReleveVentesComponent } from './popups/dialog-add-document-releve-ventes.component';
 import { DialogEditDocumentReleveVentesComponent } from './popups/dialog-edit-document-releve-ventes.component';
@Component({
    selector: 'app-documents-releves-ventes-grid',
    templateUrl: 'documents-releves-ventes-grid.component.html',
  })
  export class DocumentsReleveVentesGridComponent implements OnInit, OnChanges {
     private listDocumentsRelevesVentes: DocumentReleveVentes[] = [];
     private  dataDocumentsRelevesVentesSource: MatTableDataSource<DocumentReleveVentesElement>;
     private selection = new SelectionModel<DocumentReleveVentesElement>(true, []);
    
     @Input()
     private releveVentes : ReleveVente;

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private releveVenteService: ReleveVenteService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.releveVenteService.getDocumentsReleveVentesByCodeReleve(this.releveVentes.code).subscribe((listDocumentsRelVentes) => {
        this.listDocumentsRelevesVentes= listDocumentsRelVentes;
 //  console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsFacturesVentes  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
  }
  ngOnChanges()
  {
    this.specifyListColumnsToBeDisplayed();
    
    this.releveVenteService.getDocumentsReleveVentesByCodeReleve(this.releveVentes.code).subscribe((listDocumentsRelVentes) => {
      this.listDocumentsRelevesVentes = listDocumentsRelVentes;
// console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsCredits  );
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }

  checkOneActionInvoked() {
    const listSelectedDocumentsRelVentes: DocumentReleveVentes[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentRElement) => {
    return documentRElement.code;
  });
  this.listDocumentsRelevesVentes.forEach(documentR => {
    if (codes.findIndex(code => code === documentR.code) > -1) {
      listSelectedDocumentsRelVentes.push(documentR);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsRelevesVentes!== undefined) {

      this.listDocumentsRelevesVentes.forEach(docRel=> {
             listElements.push( {code: docRel.code ,
          dateReception: docRel.dateReception,
          numeroDocument: docRel.numeroDocument,
          typeDocument: docRel.typeDocument,
          description: docRel.description
        } );
      });
    }
      this.dataDocumentsRelevesVentesSource = new MatTableDataSource<DocumentReleveVentesElement>(listElements);
      this.dataDocumentsRelevesVentesSource.sort = this.sort;
      this.dataDocumentsRelevesVentesSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     console.log('calling specifyListColumnsToBeDisplayed documents grid');
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
    
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
      console.log(this.columnsNames);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsRelevesVentesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsRelevesVentesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsRelevesVentesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsRelevesVentesSource.paginator) {
      this.dataDocumentsRelevesVentesSource.paginator.firstPage();
    }
  }
  showAddDocumentDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeReleveVentes : this.releveVentes.code
   };
 
   const dialogRef = this.dialog.open(DialogAddDocumentReleveVentesComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(docAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditDocumentDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeDocumentReleveVentes : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditDocumentReleveVentesComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(docAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Document Séléctionnée!');
    }
    }
    supprimerDocuments()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(doc, index) {
              this.releveVenteService.deleteDocumentReleveVentes(doc.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Document Séléctionnée!');
        }
    }
    refresh() {
      this.releveVenteService.getDocumentsReleveVentesByCodeReleve(this.releveVentes.code).subscribe((listDocsRelVentes) => {
        this.listDocumentsRelevesVentes = listDocsRelVentes;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
