import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import {ReleveVente} from './releve-vente.model' ;
import {DocumentReleveVentes} from './document-releve-ventes.model' ;
import {PayementReleveVentes} from './payement-releve-ventes.model' ;
import {AjournementReleveVentes} from './ajournement-releve-ventes.model' ;

import {Credit} from '../credits/credit.model' ;
import {LigneReleveVente} from './ligne-releve-vente.model' ;

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_RELEVE_VENTES = 'http://localhost:8082/ventes/relevesVentes/getRelevesVentes';
const  URL_GET_PAGE_RELEVE_VENTES = 'http://localhost:8082/ventes/relevesVentes/getPageRelevesVentes';

const URL_GET_RELEVE_VENTE = 'http://localhost:8082/ventes/relevesVentes/getReleveVentes';
const  URL_ADD_RELEVE_VENTE  = 'http://localhost:8082/ventes/relevesVentes/addReleveVentes';
const  URL_UPDATE_RELEVE_VENTE  = 'http://localhost:8082/ventes/relevesVentes/updateReleveVentes';
const  URL_DELETE_RELEVE_VENTE  = 'http://localhost:8082/ventes/relevesVentes/deleteReleveVentes';
const  URL_GET_TOTAL_RELEVE_VENTES = 'http://localhost:8082/ventes/relevesVentes/getTotalRelevesVentes';

const  URL_ADD_PAYEMENT_RELEVE_VENTES = 'http://localhost:8082/finance/payementsRelevesVentes/addPayementReleveVentes';
const  URL_GET_LIST_PAYEMENT_RELEVE_VENTES  = 'http://localhost:8082/finance/payementsRelevesVentes/getPayementsReleveVentes';
const  URL_DELETE_PAYEMENT_RELEVE_VENTES  = 'http://localhost:8082/finance/payementsRelevesVentes/deletePayementReleveVentes';
const  URL_UPDATE_PAYEMENT_RELEVE_VENTES  = 'http://localhost:8082/finance/payementsRelevesVentes/updatePayementReleveVentes';
const URL_GET_PAYEMENT_RELEVE_VENTES  = 'http://localhost:8082/finance/payementsRelevesVentes/getPayementReleveVentes';
const URL_GET_LIST_PAYEMENT_RELEVE_VENTES_BY_CODE_RELEVE =
'http://localhost:8082/finance/payementsRelevesVentes/getPayementsReleveVentesByCodeReleve';
  

const  URL_ADD_DOCUMENT_RELEVE_VENTES = 'http://localhost:8082/ventes/documentsRelevesVentes/addDocumentReleveVentes';
const  URL_GET_LIST_DOCUMENTS_RELEVE_VENTES = 'http://localhost:8082/ventes/documentsRelevesVentes/getDocumentsReleveVentes';
const  URL_DELETE_DOCUMENT_RELEVE_VENTES = 'http://localhost:8082/ventes/documentsRelevesVentes/deleteDocumentReleveVentes';
const  URL_UPDATE_DOCUMENT_RELEVE_VENTES = 'http://localhost:8082/ventes/documentsRelevesVentes/updateDocumentReleveVentes';
const URL_GET_DOCUMENT_RELEVE_VENTES = 'http://localhost:8082/ventes/documentsRelevesVentes/getDocumentReleveVentes';
const URL_GET_LIST_DOCUMENTS_RELEVE_VENTES_BY_CODE_RELEVE =
 'http://localhost:8082/ventes/documentsRelevesVentes/getDocumentsReleveVentesByCodeReleve';

 const  URL_ADD_AJOURNEMENT_RELEVE_VENTES = 'http://localhost:8082/ventes/ajournementsRelevesVentes/addAjournementReleveVentes';
 const  URL_GET_LIST_AJOURNEMENT_RELEVE_VENTES = 'http://localhost:8082/ventes/ajournementsRelevesVentes/getAjournementsReleveVentes';
 const  URL_DELETE_AJOURNEMENT_RELEVE_VENTES = 'http://localhost:8082/ventes/ajournementsRelevesVentes/deleteAjournementReleveVentes';
 const  URL_UPDATE_AJOURNEMENT_RELEVE_VENTES = 'http://localhost:8082/ventes/ajournementsRelevesVentes/updateAjournementReleveVentes';
 const URL_GET_AJOURNEMENT_RELEVE_VENTES = 'http://localhost:8082/ventes/ajournementsRelevesVentes/getAjournementReleveVentes';
 const URL_GET_LIST_AJOURNEMENT_RELEVE_VENTES_BY_CODE_RELEVE =
  'http://localhost:8082/ventes/ajournementsRelevesVentes/getAjournementsReleveVentesByCodeReleve';



  /*const  URL_ADD_LIGNE_FACTURE_VENTES = 'http://localhost:8082/ventes/lignesFacturesVentes/addLigneFactureVentes';
  const  URL_GET_LIST_LIGNES_FACTURE_VENTES = 'http://localhost:8082/ventes/lignesFacturesVentes/getLignesFactureVentes';
  const  URL_DELETE_LIGNE_FACTURE_VENTES = 'http://localhost:8082/ventes/lignesFacturesVentes/deleteLigneFactureVentes';
  const  URL_UPDATE_LIGNE_FACTURE_VENTES = 'http://localhost:8082/ventes/lignesFacturesVentes/updateLigneFactureVentes';
  const URL_GET_LIGNE_FACTURE_VENTES = 'http://localhost:8082/ventes/lignesFacturesVentes/getLigneFactureVentes';
  const URL_GET_LIST_LIGNES_FACTURE_VENTES_BY_CODE_FACTURE_VENTES =
   'http://localhost:8082/ventes/lignesFacturesVentes/getLignesFactureVentesByCodeFactureVente';*/

@Injectable({
  providedIn: 'root'
})
export class ReleveVenteService {

  constructor(private httpClient: HttpClient) {

   }
   getTotalRelevesVentes(filter): Observable<any> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('filter', filterStr);
    return this.httpClient
    .get<any>(URL_GET_TOTAL_RELEVE_VENTES, {params});
   }


   getPageRelevesVentes(pageNumber, nbElementsPerPage, filter): Observable<ReleveVente[]> {

    const filterStr = JSON.stringify(filter);

    const params = new HttpParams()
    .set('pageNumber', pageNumber)
    .set('nbElementsPerPage', nbElementsPerPage)
    .set('filter', filterStr);
    return this.httpClient
    .get<ReleveVente[]>(URL_GET_PAGE_RELEVE_VENTES, {params});
   }

   getRelevesVentes(): Observable<ReleveVente[]> {
   
    return this.httpClient
    .get<ReleveVente[]>(URL_GET_LIST_RELEVE_VENTES);
   }
   addReleveVente(releveVente: ReleveVente): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_RELEVE_VENTE, releveVente);
  }
  updateReleveVente(releveVente: ReleveVente): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_RELEVE_VENTE, releveVente);
  }
  deleteReleveVente(codeReleveVente): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_RELEVE_VENTE + '/' + codeReleveVente);
  }
  getReleveVente(codeReleveVente): Observable<ReleveVente> {
    return this.httpClient.get<ReleveVente>(URL_GET_RELEVE_VENTE + '/' + codeReleveVente);
  }


  addPayementReleveVentes(payementReleveVentes: PayementReleveVentes): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_PAYEMENT_RELEVE_VENTES, payementReleveVentes);
    
  }
  getPayementsRelevesVentes(): Observable<PayementReleveVentes[]> {
    return this.httpClient
    .get<PayementReleveVentes[]>(URL_GET_LIST_RELEVE_VENTES);
   }
   getPayementReleveVentes(codePayementReleveVentes): Observable<PayementReleveVentes> 
   {
    return this.httpClient.get<PayementReleveVentes>(URL_GET_PAYEMENT_RELEVE_VENTES + '/' + codePayementReleveVentes);
  }
  deletePayementReleveVentes(codePayementReleveVentes): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_PAYEMENT_RELEVE_VENTES + '/' + codePayementReleveVentes);
  }
  getPayementsRelevesVentesByCodeReleve(codeReleveVentes): Observable<PayementReleveVentes[]> {
    return this.httpClient.get<PayementReleveVentes[]>(URL_GET_LIST_PAYEMENT_RELEVE_VENTES_BY_CODE_RELEVE
       + '/' + codeReleveVentes);
  }

  updatePayementReleveVentes(payRelVente: PayementReleveVentes): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_PAYEMENT_RELEVE_VENTES, payRelVente);
  }

  addDocumentReleveVentes(docReleveVentes: DocumentReleveVentes): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_DOCUMENT_RELEVE_VENTES, docReleveVentes);
    
  }
  getDocumentsRelevesVentes(): Observable<DocumentReleveVentes[]> {
    return this.httpClient
    .get<DocumentReleveVentes[]>(URL_GET_LIST_DOCUMENTS_RELEVE_VENTES);
   }
   getDocumentReleveVentes(codeDocumentReleveVentes): Observable<DocumentReleveVentes> {
    return this.httpClient.get<DocumentReleveVentes>(URL_GET_DOCUMENT_RELEVE_VENTES + '/' + codeDocumentReleveVentes);
  }
  deleteDocumentReleveVentes(codeDocumentReleveVentes): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_DOCUMENT_RELEVE_VENTES + '/' + codeDocumentReleveVentes);
  }
  updateDocumentReleveVentes(docRelVente: DocumentReleveVentes): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_DOCUMENT_RELEVE_VENTES, docRelVente);
  }


  getDocumentsReleveVentesByCodeReleve(codeReleveVentes): Observable<DocumentReleveVentes[]> {
    return this.httpClient.get<DocumentReleveVentes[]>( URL_GET_LIST_DOCUMENTS_RELEVE_VENTES_BY_CODE_RELEVE +
       '/' + codeReleveVentes);
  }

  addAjournementReleveVentes(ajournReleveVente: AjournementReleveVentes): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_AJOURNEMENT_RELEVE_VENTES, ajournReleveVente);
    
  }
  updateAjournementReleveVentes(ajournReleveVente: AjournementReleveVentes): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_AJOURNEMENT_RELEVE_VENTES, ajournReleveVente);
  }
  getAjournementsRelevesVentesByCodeReleve(codeRelVentes): Observable<AjournementReleveVentes[]> {
    return this.httpClient.get<AjournementReleveVentes[]>(
      URL_GET_LIST_AJOURNEMENT_RELEVE_VENTES_BY_CODE_RELEVE +
       '/' + codeRelVentes);
  }
  getAjournementsRelevesVentes(): Observable<AjournementReleveVentes[]> {
    return this.httpClient
    .get<AjournementReleveVentes[]>(URL_GET_LIST_AJOURNEMENT_RELEVE_VENTES);
   }
   getAjournementReleveVentes(codeAjournementReleveVentes): Observable<AjournementReleveVentes> {
    return this.httpClient.get<AjournementReleveVentes>(URL_GET_AJOURNEMENT_RELEVE_VENTES + '/' + codeAjournementReleveVentes);
  }
  deleteAjournementReleveVentes(codeAjournementReleveVentes): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_AJOURNEMENT_RELEVE_VENTES + '/' + codeAjournementReleveVentes);
  }


  /*addLigneFactureVentes(ligneFactureVente: LigneFactureVente): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_LIGNE_FACTURE_VENTES, ligneFactureVente);
    
  }
  updateLigneFactureVentes(ligneFactureVente: LigneFactureVente): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_LIGNE_FACTURE_VENTES, ligneFactureVente);
  }
  getLignesFacturesVentesByCodeFacture(codeLigneFactureVentes): Observable<LigneFactureVente[]> {
    return this.httpClient.get<LigneFactureVente[]>( URL_GET_LIST_LIGNES_FACTURE_VENTES_BY_CODE_FACTURE_VENTES +
       '/' + codeLigneFactureVentes);
  }
  getLignesFacturesVentes(): Observable<LigneFactureVente[]> {
    return this.httpClient
    .get<LigneFactureVente[]>(URL_GET_LIST_LIGNES_FACTURE_VENTES);
   }
   getLigneFactureVentes(codeLigneFactureVentes): Observable<LigneFactureVente> {
    return this.httpClient.get<LigneFactureVente>(URL_GET_LIST_LIGNES_FACTURE_VENTES + '/' + codeLigneFactureVentes);
  }
  deleteLigneFactureVentes(codeLigneFactureVentes): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_LIGNE_FACTURE_VENTES + '/' + codeLigneFactureVentes);
  }*/
}
