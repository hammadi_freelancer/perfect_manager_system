import { Component, OnInit, OnDestroy,
   AfterContentChecked, Input, Output, EventEmitter, ElementRef ,
    ViewChild } from '@angular/core';
  // import { ArticleService } from './article.service';
  import { ParametresVentes} from './parametres-ventes.model';
  import {VentesService} from './ventes.service';
  import {FormControl} from '@angular/forms';
  import {map, startWith} from 'rxjs/operators';
  import {Observable} from 'rxjs';
  import { Router, ActivatedRoute, ParamMap } from '@angular/router';
  import {MatSnackBar} from '@angular/material';
  
 
  @Component({
    selector: 'app-parametres-ventes',
    templateUrl: './parametres-ventes.component.html',
    // styleUrls: ['./players.component.css']
  })
  export class ParametresVentesComponent implements OnInit, OnDestroy {

    private parametresVentes: ParametresVentes =  new ParametresVentes();
    private  showSelectEntityClaculPrix =  false;
    private  showSelectEntityClaculTVA =  false;
    private  showSelectEntityClaculQuantite =  false;
    private  showSelectEntityClaculAttenteLivraison =  false;
    private saveEnCours = false;

    constructor( private route: ActivatedRoute,
      private router: Router, private ventesService: VentesService, private matSnackBar: MatSnackBar) {
    }
    ngOnInit() {
        this.ventesService.getParametresVentes().subscribe((response) => {
          console.log('Parametres Ventes Recieved are : ', response);
            if (response !==  undefined) {
              // this.save.emit(response);
              this.parametresVentes = response;
            } else {
             // show error message
            }
            if (this.parametresVentes === null || this.parametresVentes === undefined ) {
              this.parametresVentes = new ParametresVentes();
              }
        });
    }
    saveParametresVentes() {
     this.saveEnCours = true;
    this.ventesService.saveParametresVentes(this.parametresVentes).subscribe((response) => {
      console.log('response is', response );
      this.saveEnCours = false;
        if (response.ok ==  1) {
          // this.save.emit(response);
          //this.parametresVentes =  new ParametresVentes();
          this.openSnackBar(  'Paramètres de Vente Enregistrés');
        } else {
         // show error message
         this.openSnackBar(  'Erreurs..');
        }
    });
}
    openSnackBar(messageToDisplay) {
        this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
            verticalPosition: 'top'});
       }
     ngOnDestroy()  {

     //  this.router.navigate(['/pms']) ;

       }
 
  }

