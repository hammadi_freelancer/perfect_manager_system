

type EntityCalculTVA =  ['Documents', 'Invocies'];
type EntityCalculPrix =  ['Documents', 'Invocies'];
type EntityCalculQunatite =  ['Documents', 'Invocies', 'BonsReception'];
type EntityCalculAttenteReception =  ['Documents', 'Invoices', 'Ordres'  , 'Demandes'];
type MAGASIN_SEARCH_BY =  ['Libelle', 'Code'];
type UN_MESURE_SEARCH_BY =  ['Libelle', 'Code'];
type ARTICLE_SEARCH_BY =  ['Libelle', 'Code'];
type CONFIGURATION_SEARCH_BY =  ['Libelle', 'Code'];


 type StrategieCaculTVA  = ['FirstLine', 'LastLine', 'MiddleLines' ,
 ' MaxTVA', 'MinTVA' , 'MoyTVA' ];
 type StrategieCaculPrix = ['FirstLine', 'LastLine', 'MiddleLines' ,
 ' MaxPrix', 'MinPrix' , 'MoyPrix' ];
// import { BaseEntity } from '../../common/base-entity.model' ;
export class ParametresVentes {
   constructor(
     public tvaClaculated?: boolean, public prixClaculated?: boolean,
     public quantiteClaculated?: boolean,
     public attenteLivraisonCalculated?: boolean,
   public strategieCaclulTVA?: StrategieCaculTVA,
   public strategieCaclulPrix?: StrategieCaculPrix,
   public entityCalculPrix?: EntityCalculPrix,
   public entityCalculTVA?: EntityCalculTVA,
   public entityCalculQuantite?: EntityCalculQunatite,
   public entityCalculAttenteLivraison?: EntityCalculAttenteReception,

   public inclureBeneficeFacture?: boolean,
   public defaultBeneficePourcentage?: number,

   public inclureTVAFacture?: boolean,
   public defaultTVAPourcentage?: number,

   public inclureFodecFacture?: boolean,
   public defaultFodecPourcentage?: number,

   public inclure3rdTaxeFacture?: boolean,
   public default3rdTaxePourcentage?: number,
   public default3rdTaxeLibelle?: string,
   
   public inclure4rdTaxeFacture?: boolean,
   public default4rdTaxePourcentage?: number,
   public default4rdTaxeLibelle?: string,
   
   public inclurePrixVentePublicFacture?: boolean,
   public defaultPrixVentePourcentage?: number,
   
   public inclureRemise?: boolean,
   public defaultRemisePourcentage?: number,
   
   public inclureMagasins?: boolean,
   public magasinSearchBy?: MAGASIN_SEARCH_BY,
   public inclureUnMesure?: boolean,
   public inclureConfigurations?: boolean,
   public configurationSearchBy?: boolean,
   
   public unMesureSearchBy?: UN_MESURE_SEARCH_BY,
   public articleSearchBy?: UN_MESURE_SEARCH_BY,
   
   public inclureTimbreFiscal?: boolean,
   public valeurTimbreFiscal?: number,
   
) {
    // super();
      // this.image = undefined;
}
}
