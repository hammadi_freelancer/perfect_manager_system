import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
 import { AppComponent } from '../app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ReactiveFormsModule } from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatMenuModule} from '@angular/material/menu';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {TableModule} from 'primeng/table';
import {MatSelectModule} from '@angular/material/select';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTabsModule} from '@angular/material/tabs';
import {MatBadgeModule} from '@angular/material/badge';

import { MAT_DIALOG_DATA} from '@angular/material';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressBarModule} from '@angular/material/progress-bar';

import { RouterModule, provideRoutes} from '@angular/router';
import { ClientsGridComponent } from './clients/clients-grid.component';

 import { DocumentTypesGridComponent } from '../documents/documents-types-grid.component';
 import { DocumentTypeComponent } from '../documents/document-type.component';
import { GestionDocumentsTypesComponent } from '../documents/gestion-documents-types.component';
import {MultiSelectModule} from 'primeng/multiselect';

import { CommonModule } from '../common/common.module';
import { DocumentsModule } from '../documents/documents.module';
import { SharedComponentModule } from '../shared-components/shared-components.module';
import { CommunicationService } from './clients/communication.service';
import { CreditSearchComponent } from './credits/credit-search.component';
// clients components

import { ClientsResolver } from './clients-resolver';
import { ArticlesResolver } from './articles-resolver';
import { ClientNewComponent } from './clients/client-new.component';
import { ClientEditComponent } from './clients/client-edit.component';
import { ClientHomeComponent } from './clients/client-home.component';

// credits components
import { CreditNewComponent } from './credits/credit-new.component';
import { CreditEditComponent } from './credits/credit-edit.component';
import { CreditHomeComponent } from './credits/credit-home.component';
import { CreditsGridComponent } from './credits/credits-grid.component';

// releves ventes 


import { ReleveVenteNewComponent } from './releves-ventes/releve-vente-new.component';
import { ReleveVenteEditComponent } from './releves-ventes/releve-vente-edit.component';
import { RelevesVentesGridComponent } from './releves-ventes/releves-ventes-grid.component';
// demandes ventes

import { DemandeVentesNewComponent } from './demandes-ventes/demande-ventes-new.component';
import { DemandeVentesEditComponent } from './demandes-ventes/demande-ventes-edit.component';
import { DemandesVentesGridComponent } from './demandes-ventes/demandes-ventes-grid.component';

// Facture vente components
import { FactureVenteNewComponent } from './factures-ventes/facture-vente-new.component';
import { FactureVenteEditComponent } from './factures-ventes/facture-vente-edit.component';
import { FacturesVentesGridComponent } from './factures-ventes/factures-ventes-grid.component';
import { DocumentVentesNewComponent } from './documents-ventes/document-vente-new.component';
import { DocumentVenteEditComponent } from './documents-ventes/document-vente-edit.component';
import { DocumentsVentesGridComponent } from './documents-ventes/documents-ventes-grid.component';
import { GestionMarchandisesComponent } from './documents-ventes/gestion-marchandises.component';
import { GestionTaxesComponent } from './documents-ventes/gestion-taxes.component';
import { GestionCoutsComponent } from './documents-ventes/gestion-couts.component';
import { GestionOccupationsMainsOeuvresComponent } from './documents-ventes/gestion-occupations-mains-oeuvres.component';
import { GestionRemisesComponent } from './documents-ventes/gestion-remises.component';
import { GestionLogistiqueComponent } from './documents-ventes/gestion-logistique.component';

import { FactureVenteHomeComponent } from './factures-ventes/facture-vente-home.component';
import { ParametresVentesComponent } from './parametres-ventes.component';

import { DialogAddPayementCreditComponent } from './credits/popups/dialog-add-payement-credit.component';
import { DialogAddAjournementCreditComponent } from './credits/popups/dialog-add-ajournement-credit.component';
import { DialogAddDocumentCreditComponent } from './credits/popups/dialog-add-document-credit.component';
import { DialogAddTrancheCreditComponent } from './credits/popups/dialog-add-tranche-credit.component';
import { DialogAddLigneCreditComponent } from './credits/popups/dialog-add-ligne-credit.component';
import { DialogAddDocumentClientComponent } from './clients/popups/dialog-add-document-client.component';

import { DialogEditPayementCreditComponent } from './credits/popups/dialog-edit-payement-credit.component';
import { DialogEditAjournementCreditComponent } from './credits/popups/dialog-edit-ajournement-credit.component';
import { DialogEditDocumentCreditComponent } from './credits/popups/dialog-edit-document-credit.component';
import { DialogEditTrancheCreditComponent } from './credits/popups/dialog-edit-tranche-credit.component';
import { DialogEditLigneCreditComponent } from './credits/popups/dialog-edit-ligne-credit.component';
import { DialogEditDocumentClientComponent } from './clients/popups/dialog-edit-document-client.component';


import { PayementsCreditsGridComponent } from './credits/payements-credits-grid.component';
import { AjournementsCreditsGridComponent } from './credits/ajournements-credits-grid.component';
import { DocumentsCreditsGridComponent } from './credits/documents-credits-grid.component';
import {TranchesCreditsGridComponent } from './credits/tranches-grid.component';
import {LignesCreditsGridComponent } from './credits/lignes-credits-grid.component';
import { DocumentsClientsGridComponent} from './clients/documents-clients-grid.component';







import { DialogAddClientComponent } from './clients/dialog-add-client.component';
import { DialogListPayementsCreditsComponent } from './credits/popups/dialog-list-payements-credits.component ';
// import { DialogListPayementsFactureVentesComponent } from './factures-ventes/dialog-list-payements-facture-ventes.component';
import { DialogListAjournementsCreditsComponent } from './credits/popups/dialog-list-ajournements-credits.component';
import { DialogListLignesCreditsComponent } from './credits/popups/dialog-list-lignes-credits.component';

import { DialogListTranchesCreditsComponent } from './credits/popups/dialog-list-tranches-credits.component';
import { DialogListDocumentsCreditsComponent } from './credits/popups/dialog-list-documents-credits.component';
import { DialogListDocumentsClientsComponent } from './clients/popups/dialog-list-documents-clients.component';

import {MatSidenavModule} from '@angular/material/sidenav';
import {MatRadioModule} from '@angular/material/radio';

import { MagasinsResolver } from './magasins-resolver';
import { UnMesuresResolver } from './unMesures-resolver';

import {FormsModule } from '@angular/forms';
 import {APP_BASE_HREF} from '@angular/common';
 import { HomeVentesComponent } from './home-ventes.component' ;






 import { DialogAddPayementFactureVentesComponent } from './factures-ventes/popups/dialog-add-payement-facture-ventes.component';
 import { DialogAddAjournementFactureVentesComponent } from './factures-ventes/popups/dialog-add-ajournement-facture-ventes.component';
 import { DialogAddDocumentFactureVentesComponent } from './factures-ventes/popups/dialog-add-document-facture-ventes.component';
 
 import { DialogEditPayementFactureVentesComponent } from './factures-ventes/popups/dialog-edit-payement-facture-ventes.component';
 import { DialogEditAjournementFactureVentesComponent } from './factures-ventes/popups/dialog-edit-ajournement-facture-ventes.component';
 import { DialogEditDocumentFactureVentesComponent } from './factures-ventes/popups/dialog-edit-document-facture-ventes.component';
 import { DialogListPayementsFactureVentesComponent} from './factures-ventes/popups/dialog-list-payements-factures-ventes.component';
 import { DialogListDocumentsFacturesVentesComponent} from './factures-ventes/popups/dialog-list-documents-factures-ventes.component';
 import { DialogListAjournementsFacturesVentesComponent} from './factures-ventes/popups/dialog-list-ajournements-factures-ventes.component';
 import { DialogEditLigneFactureVentesComponent } from './factures-ventes/popups/dialog-edit-ligne-facture-ventes.component';
 import { DialogAddLigneFactureVentesComponent } from './factures-ventes/popups/dialog-add-ligne-facture-ventes.component';
 import { DialogListLignesFactureVentesComponent} from './factures-ventes/popups/dialog-list-lignes-factures-ventes.component';

 import { LignesFacturesVentesGridComponent} from './factures-ventes/lignes-factures-ventes-grid.component';
 
 import { PayementsFacturesVentesGridComponent } from './factures-ventes/payements-factures-ventes-grid.component';
 import { AjournementsFacturesVentesGridComponent} from './factures-ventes/ajournements-factures-ventes-grid.component';
 import { DocumentsFactureVentesGridComponent} from './factures-ventes/documents-factures-ventes-grid.component';



// popups releve ventes 





import { DialogAddPayementReleveVentesComponent } from './releves-ventes/popups/dialog-add-payement-releve-ventes.component';
import { DialogAddAjournementReleveVentesComponent } from './releves-ventes/popups/dialog-add-ajournement-releve-ventes.component';
import { DialogAddDocumentReleveVentesComponent } from './releves-ventes/popups/dialog-add-document-releve-ventes.component';

import { DialogEditPayementReleveVentesComponent } from './releves-ventes/popups/dialog-edit-payement-releve-ventes.component';
import { DialogEditAjournementReleveVentesComponent } from './releves-ventes/popups/dialog-edit-ajournement-releve-ventes.component';
import { DialogEditDocumentReleveVentesComponent } from './releves-ventes/popups/dialog-edit-document-releve-ventes.component';
import { DialogListPayementsRelevesVentesComponent} from './releves-ventes/popups/dialog-list-payements-releves-ventes.component';
import { DialogListDocumentsRelevesVentesComponent} from './releves-ventes/popups/dialog-list-documents-releves-ventes.component';
import { DialogListAjournementsRelevesVentesComponent} from './releves-ventes/popups/dialog-list-ajournements-releves-ventes.component';
import { DialogEditLigneReleveVentesComponent } from './releves-ventes/popups/dialog-edit-ligne-releve-ventes.component';
import { DialogAddLigneReleveVentesComponent } from './releves-ventes/popups/dialog-add-ligne-releve-ventes.component';
import { DialogListLignesRelevesVentesComponent} from './releves-ventes/popups/dialog-list-lignes-releves-ventes.component';
 
// import { LignesRelevesVentesGridComponent} from './releves-ventes/li';

import { PayementsRelevesVentesGridComponent } from './releves-ventes/payements-releves-ventes-grid.component';
import { AjournementsRelevesVentesGridComponent} from './releves-ventes/ajournements-releves-ventes-grid.component';
import { DocumentsReleveVentesGridComponent} from './releves-ventes/documents-releves-ventes-grid.component';




 import { DialogDisplayStatistiquesComponent } from './credits/popups/dialog-display-statistiques.component';
 







// popups releve ventes 





import { DialogAddPayementDemandeVentesComponent } from './demandes-ventes/popups/dialog-add-payement-demande-ventes.component';
import { DialogAddAjournementDemandeVentesComponent } from './demandes-ventes/popups/dialog-add-ajournement-demande-ventes.component';
import { DialogAddDocumentDemandeVentesComponent } from './demandes-ventes/popups/dialog-add-document-demande-ventes.component';

import { DialogEditPayementDemandeVentesComponent } from './demandes-ventes/popups/dialog-edit-payement-demande-ventes.component';
import { DialogEditAjournementDemandeVentesComponent } from './demandes-ventes/popups/dialog-edit-ajournement-demande-ventes.component';
import { DialogEditDocumentDemandeVentesComponent } from './demandes-ventes/popups/dialog-edit-document-demande-ventes.component';
import { DialogListPayementsDemandesVentesComponent} from './demandes-ventes/popups/dialog-list-payements-demandes-ventes.component';
import { DialogListDocumentsDemandesVentesComponent} from './demandes-ventes/popups/dialog-list-documents-demandes-ventes.component';
import { DialogListAjournementsDemandesVentesComponent} from './demandes-ventes/popups/dialog-list-ajournements-demandes-ventes.component';
import { DialogEditLigneDemandeVentesComponent } from './demandes-ventes/popups/dialog-edit-ligne-demande-ventes.component';
import { DialogAddLigneDemandeVentesComponent } from './demandes-ventes/popups/dialog-add-ligne-demande-ventes.component';
import { DialogListLignesDemandesVentesComponent} from './demandes-ventes/popups/dialog-list-lignes-demandes-ventes.component';

// import { LignesRelevesVentesGridComponent} from './releves-ventes/li';

import { PayementsDemandesVentesGridComponent } from './demandes-ventes/payements-demandes-ventes-grid.component';
import { AjournementsDemandesVentesGridComponent} from './demandes-ventes/ajournements-demandes-ventes-grid.component';
import { DocumentsDemandeVentesGridComponent} from './demandes-ventes/documents-demandes-ventes-grid.component';




/*const ENTITY_STATES = [
  ...commonRoute
];*/

 // const BASE_URL = [{provide: APP_BASE_HREF, useValue: '/common'}];
@NgModule({
  declarations: [
    ClientsGridComponent,
    ClientNewComponent,
    ClientEditComponent,
    ClientHomeComponent,
    // popups clients
    DialogAddClientComponent,
    DialogListDocumentsClientsComponent,
    DialogAddDocumentClientComponent,
    DialogEditDocumentClientComponent,
    DocumentsClientsGridComponent,
    // credits componenents
    CreditEditComponent,
    CreditNewComponent,
    CreditsGridComponent,
     CreditSearchComponent,
     CreditHomeComponent,
     ParametresVentesComponent,
     // releves ventes components 

     ReleveVenteNewComponent,
     ReleveVenteEditComponent,
     RelevesVentesGridComponent,

     // releves ventes popups
     DialogAddPayementReleveVentesComponent,
     DialogAddAjournementReleveVentesComponent,
     DialogAddDocumentReleveVentesComponent,
     DialogEditPayementReleveVentesComponent,
     DialogEditAjournementReleveVentesComponent,
     DialogEditDocumentReleveVentesComponent,
     DialogListDocumentsRelevesVentesComponent,
     DialogListAjournementsRelevesVentesComponent,
     DialogListPayementsRelevesVentesComponent,
     DialogDisplayStatistiquesComponent,
     PayementsRelevesVentesGridComponent,
     AjournementsRelevesVentesGridComponent,
     DocumentsReleveVentesGridComponent,
     DialogEditLigneReleveVentesComponent,
     DialogAddLigneReleveVentesComponent,
     DialogListLignesRelevesVentesComponent,
     // demandes ventes componenents
     DemandeVentesNewComponent,
     DemandesVentesGridComponent,
     DemandeVentesEditComponent,
     // facture ventes componenents
     FactureVenteNewComponent,
     FactureVenteNewComponent,
     FacturesVentesGridComponent,
     FactureVenteEditComponent,
     LignesFacturesVentesGridComponent,
     // popups facture ventes
    // DialogListPayementsFactureVentesComponent,
    // DialogAddPayementFactureVentesComponent,
     
     // document ventes componnents
     DocumentVentesNewComponent,
     DocumentVenteEditComponent,
     DocumentsVentesGridComponent,
     GestionMarchandisesComponent,
     GestionOccupationsMainsOeuvresComponent,
     GestionTaxesComponent,
     GestionCoutsComponent,
     GestionRemisesComponent,
     GestionLogistiqueComponent,

     // popups lignes credits 
     LignesCreditsGridComponent,
     DialogEditLigneCreditComponent,
     DialogAddLigneCreditComponent,
     DialogListLignesCreditsComponent,
     // popups credits ajournements

     AjournementsCreditsGridComponent,
     DialogEditAjournementCreditComponent,
     DialogAddAjournementCreditComponent,
     DialogListAjournementsCreditsComponent,
     
          // popups credits paiements

     DialogAddPayementCreditComponent,
     DialogListPayementsCreditsComponent,
     DialogEditPayementCreditComponent,
     PayementsCreditsGridComponent,

   // popups credits  tranches    
     DialogAddTrancheCreditComponent,
     DialogListTranchesCreditsComponent,
     DialogEditTrancheCreditComponent,
     TranchesCreditsGridComponent,

     // popups credits documents
     
     DialogAddDocumentCreditComponent,
     DialogListDocumentsCreditsComponent,
     DocumentsCreditsGridComponent,
     DialogEditDocumentCreditComponent,
     // popups facture ventes

     DialogAddPayementFactureVentesComponent,
     DialogAddAjournementFactureVentesComponent,
     DialogAddDocumentFactureVentesComponent,
     DialogEditPayementFactureVentesComponent,
     DialogEditAjournementFactureVentesComponent,
     DialogEditDocumentFactureVentesComponent,
     DialogListDocumentsFacturesVentesComponent,
     DialogListAjournementsFacturesVentesComponent,
     DialogListPayementsFactureVentesComponent,
     DialogDisplayStatistiquesComponent,
     PayementsFacturesVentesGridComponent,
     AjournementsFacturesVentesGridComponent,
     DocumentsFactureVentesGridComponent,
     DialogEditLigneFactureVentesComponent,
     DialogAddLigneFactureVentesComponent,
     DialogListLignesFactureVentesComponent,
        // demandes ventes popups
        DialogAddPayementDemandeVentesComponent,
        DialogAddAjournementDemandeVentesComponent,
        DialogAddDocumentDemandeVentesComponent,
        DialogEditPayementDemandeVentesComponent,
        DialogEditAjournementDemandeVentesComponent,
        DialogEditDocumentDemandeVentesComponent,
        DialogListDocumentsDemandesVentesComponent,
        DialogListAjournementsDemandesVentesComponent,
        DialogListPayementsDemandesVentesComponent,
        DialogDisplayStatistiquesComponent,
        PayementsDemandesVentesGridComponent,
        AjournementsDemandesVentesGridComponent,
        DocumentsDemandeVentesGridComponent,
        DialogEditLigneDemandeVentesComponent,
        DialogAddLigneDemandeVentesComponent,
        DialogListLignesDemandesVentesComponent,
    HomeVentesComponent

  ],
  imports: [
    BrowserModule, HttpClientModule, MatDatepickerModule, MatNativeDateModule,
     BrowserAnimationsModule, ReactiveFormsModule, MatAutocompleteModule,
    MatInputModule, MatCardModule, MatCheckboxModule, MatButtonModule, MatIconModule,
    MatMenuModule, MatProgressSpinnerModule, MatExpansionModule, MatSortModule, MatSelectModule, MatPaginatorModule, MatTableModule,
    FormsModule, SharedComponentModule, MatTabsModule, MatBadgeModule,
    MatProgressBarModule,
    MultiSelectModule,
    // ventesRoutingModule,
    CommonModule, TableModule , MatDialogModule,
    DocumentsModule, MatSidenavModule, MatRadioModule
],
  providers: [MatNativeDateModule , ClientsResolver,
    MatDialogModule,
    ArticlesResolver,
    UnMesuresResolver,
    MagasinsResolver // BASE_URL
  ],
  exports : [
    RouterModule,
     GestionDocumentsTypesComponent,
     DocumentTypesGridComponent,
    DocumentTypeComponent,
    HomeVentesComponent

  ],
  entryComponents : [
    
    DialogAddDocumentCreditComponent,
    DialogListDocumentsCreditsComponent,
    DocumentsCreditsGridComponent,
    DialogEditDocumentCreditComponent,
    DialogListDocumentsClientsComponent,
    DialogAddDocumentClientComponent,
    DialogEditDocumentClientComponent,
    DialogAddTrancheCreditComponent,
    DialogListTranchesCreditsComponent,
    DialogEditTrancheCreditComponent,
    TranchesCreditsGridComponent,

    DialogAddPayementCreditComponent,
    DialogListPayementsCreditsComponent,
    DialogEditPayementCreditComponent,
    PayementsCreditsGridComponent,

    AjournementsCreditsGridComponent,
    DialogEditAjournementCreditComponent,
    DialogAddAjournementCreditComponent,
    DialogListAjournementsCreditsComponent,
    LignesCreditsGridComponent,
    DialogEditLigneCreditComponent,
    DialogAddLigneCreditComponent,
    DialogListLignesCreditsComponent,
    DialogDisplayStatistiquesComponent,
    DialogAddClientComponent,
 
    
 // popups factures ventes
    DialogAddPayementFactureVentesComponent,
    DialogAddAjournementFactureVentesComponent,
    DialogAddDocumentFactureVentesComponent,
    DialogEditPayementFactureVentesComponent,
    DialogEditAjournementFactureVentesComponent,
    DialogEditDocumentFactureVentesComponent,
    DialogListDocumentsFacturesVentesComponent,
    DialogListAjournementsFacturesVentesComponent,
    DialogListPayementsFactureVentesComponent,
    PayementsFacturesVentesGridComponent,
    AjournementsFacturesVentesGridComponent,
    DocumentsFactureVentesGridComponent,
    DialogEditLigneFactureVentesComponent,
    DialogAddLigneFactureVentesComponent,
    DialogListLignesFactureVentesComponent,

    // popups rel vents 

    DialogAddPayementReleveVentesComponent,
    DialogAddAjournementReleveVentesComponent,
    DialogAddDocumentReleveVentesComponent,
    DialogEditPayementReleveVentesComponent,
    DialogEditAjournementReleveVentesComponent,
    DialogEditDocumentReleveVentesComponent,
    DialogListDocumentsRelevesVentesComponent,
    DialogListAjournementsRelevesVentesComponent,
    DialogListPayementsRelevesVentesComponent,
    DialogEditLigneReleveVentesComponent,
    DialogAddLigneReleveVentesComponent,
    DialogListLignesRelevesVentesComponent,

        // popups demandes ventes

        DialogAddPayementDemandeVentesComponent,
        DialogAddAjournementDemandeVentesComponent,
        DialogAddDocumentDemandeVentesComponent,
        DialogEditPayementDemandeVentesComponent,
        DialogEditAjournementDemandeVentesComponent,
        DialogEditDocumentDemandeVentesComponent,
        DialogListDocumentsDemandesVentesComponent,
        DialogListAjournementsDemandesVentesComponent,
        DialogListPayementsDemandesVentesComponent,
        DialogEditLigneDemandeVentesComponent,
        DialogAddLigneDemandeVentesComponent,
        DialogListLignesDemandesVentesComponent
  ],
  bootstrap: [AppComponent]
})
export class VentesModule { }
