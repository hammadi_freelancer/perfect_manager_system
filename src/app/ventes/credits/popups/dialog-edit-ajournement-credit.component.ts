
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {CreditService} from '../credit.service';
import {AjournementCredit} from '../ajournement-credit.model';

@Component({
    selector: 'app-dialog-edit-ajournement-credit',
    templateUrl: 'dialog-edit-ajournement-credit.component.html',
  })
  export class DialogEditAjournementCreditComponent implements OnInit {
     private ajournementCredit: AjournementCredit;
     private updateEnCours = false;
     
     constructor(  private creditService: CreditService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {

        this.creditService.getAjournementCredit(this.data.codeAjournementCredit).subscribe((ajour) => {
            this.ajournementCredit = ajour;
     });


    }
    updateAjournementCredit() 
    {
     this.updateEnCours = true;
       this.creditService.updateAjournementCredit(this.ajournementCredit).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.openSnackBar(  'Ajournement Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
