
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {CreditService} from '../credit.service';
import {PayementCredit} from '../payement-credit.model';

@Component({
    selector: 'app-dialog-edit-payement-credit',
    templateUrl: 'dialog-edit-payement-credit.component.html',
  })
  export class DialogEditPayementCreditComponent implements OnInit {
     private payementCredit: PayementCredit;
     private updateEnCours = false;
     constructor(  private creditService: CreditService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit() {
        this.creditService.getPayementCredit(this.data.codePayementCredit).subscribe((payement) => {
            this.payementCredit = payement;
     });
   }
    updatePayementCredit() {
      //  this.saveEnCours = true;
      this.updateEnCours = true;
       this.creditService.updatePayementCredit(this.payementCredit).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             // this.payementCredit = new PayementCredit();
             this.openSnackBar(  'Payement Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
