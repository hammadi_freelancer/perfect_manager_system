
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {CreditService} from '../credit.service';
import {AjournementCredit} from '../ajournement-credit.model';

@Component({
    selector: 'app-dialog-add-ajournement-credit',
    templateUrl: 'dialog-add-ajournement-credit.component.html',
  })
  export class DialogAddAjournementCreditComponent implements OnInit {
     private ajournementCredit: AjournementCredit;
     private saveEnCours = false;
     constructor(  private creditService: CreditService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.ajournementCredit = new AjournementCredit();
    }
    saveAjournementCredit() 
    {
      this.ajournementCredit.codeCredit = this.data.codeCredit;
      this.saveEnCours = true;
       console.log(this.ajournementCredit);
      //  this.saveEnCours = true;
       this.creditService.addAjournementCredit(this.ajournementCredit).subscribe((response) => {
        this.saveEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.ajournementCredit = new AjournementCredit();
             this.openSnackBar(  'Ajournement Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
