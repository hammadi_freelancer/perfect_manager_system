
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {CreditService} from '../credit.service';
import {AjournementCredit} from '../ajournement-credit.model';
import { AjournementCreditElement } from '../ajournement-credit.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-ajournements-credits',
    templateUrl: 'dialog-list-ajournements-credits.component.html',
  })
  export class DialogListAjournementsCreditsComponent implements OnInit {
     private listAjournementCredits: AjournementCredit[] = [];
     private  dataAjournementsCreditsSource: MatTableDataSource<AjournementCreditElement>;
     private selection = new SelectionModel<AjournementCreditElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private creditService: CreditService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      console.log('code credit in dialog component is :', this.data.codeCredit);
      this.creditService.getAjournementsCreditsByCodeCredit(this.data.codeCredit).subscribe((listAjournementsCredits) => {
        this.listAjournementCredits = listAjournementsCredits;
console.log('recieving data from backend', this.listAjournementCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedAjournementsCredits: AjournementCredit[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((payementCreditElement) => {
    return payementCreditElement.code;
  });
  this.listAjournementCredits.forEach(ajournementCredit => {
    if (codes.findIndex(code => code === ajournementCredit.code) > -1) {
      listSelectedAjournementsCredits.push(ajournementCredit);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listAjournementCredits !== undefined) {

      this.listAjournementCredits.forEach(ajournementCredit => {
             listElements.push( {code: ajournementCredit.code ,
          dateOperation: ajournementCredit.dateOperation,
          nouvelleDatePayement: ajournementCredit.nouvelleDatePayement,
          motif: ajournementCredit.motif,
          description: ajournementCredit.description
        } );
      });
    }
      this.dataAjournementsCreditsSource = new MatTableDataSource<AjournementCreditElement>(listElements);
      this.dataAjournementsCreditsSource.sort = this.sort;
      this.dataAjournementsCreditsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'},
     {name: 'nouvelleDatePayement' , displayTitle: 'N°elle Date Pay.'},
     {name: 'motif' , displayTitle: 'Motif'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataAjournementsCreditsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataAjournementsCreditsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataAjournementsCreditsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataAjournementsCreditsSource.paginator) {
      this.dataAjournementsCreditsSource.paginator.firstPage();
    }
  }

}
