
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {CreditService} from '../credit.service';
import {LigneCredit} from '../ligne-credit.model';

@Component({
    selector: 'app-dialog-display-statistiques',
    templateUrl: 'dialog-display-statistiques.component.html',
  })
  export class DialogDisplayStatistiquesComponent implements OnInit {
     private loadEnCours = false;
     private dateDepartObject ;
     private dateFinObject ;
     private montantRegleVentes;
     private montantNonRegleVentes;
     private montantRegleCredit;
     private montantNonRegleCredit;
     constructor(  private creditService: CreditService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
          // this.ligneCredit = new LigneCredit();
    }
    recuperateMontants() 
    {
     
       this.loadEnCours = true;
       
      //  this.saveEnCours = true;
       this.creditService.recuperateMontants(this.dateDepartObject, this.dateFinObject).subscribe((response) => {
        this.loadEnCours = false;
        if (response.error_message ===  undefined) {
             this.montantNonRegleCredit = response.montantNonRegleCredit;
             this.montantRegleCredit = response.montantRegleCredit;
             this.montantRegleVentes = response.montantRegleVentes;
             this.montantNonRegleVentes = response.montantNonRegleVentes;
             
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
