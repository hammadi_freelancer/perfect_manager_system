
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {CreditService} from '../credit.service';
import {TrancheCredit} from '../tranche-credit.model';

@Component({
    selector: 'app-dialog-edit-tranche-credit',
    templateUrl: 'dialog-edit-tranche-credit.component.html',
  })
  export class DialogEditTrancheCreditComponent implements OnInit {
     private trancheCredit: TrancheCredit;
     private updateEnCours = false;
     constructor(  private creditService: CreditService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.creditService.getTrancheCredit(this.data.codeTrancheCredit).subscribe((tranche) => {
            console.log('Tranche Credit Recuperated is :');
            console.log(tranche);
            
                  this.trancheCredit = tranche;
           });
    }
    updateTrancheCredit() 
    {
       console.log(this.trancheCredit);
        this.updateEnCours = true;
      //  this.saveEnCours = true;
       this.creditService.updateTrancheCredit(this.trancheCredit).subscribe((response) => {
           this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             //this.trancheCredit = new TrancheCredit();
             this.openSnackBar(  'Tranche Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
