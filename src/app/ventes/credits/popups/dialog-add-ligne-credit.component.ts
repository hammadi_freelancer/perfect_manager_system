
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {CreditService} from '../credit.service';
import {LigneCredit} from '../ligne-credit.model';

@Component({
    selector: 'app-dialog-add-ligne-credit',
    templateUrl: 'dialog-add-ligne-credit.component.html',
  })
  export class DialogAddLigneCreditComponent implements OnInit {
     private ligneCredit: LigneCredit;
     private saveEnCours = false;
     
     constructor(  private creditService: CreditService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.ligneCredit = new LigneCredit();
    }
    saveLigneCredit() 
    {
      this.ligneCredit.codeCredit = this.data.codeCredit;
       console.log(this.ligneCredit);
       this.saveEnCours = true;
       
      //  this.saveEnCours = true;
       this.creditService.addLigneCredit(this.ligneCredit).subscribe((response) => {
        this.saveEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.ligneCredit = new LigneCredit();
             this.openSnackBar(  'Ligne Enregistrée');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
