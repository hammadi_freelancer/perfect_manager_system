
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {CreditService} from '../credit.service';
import {TrancheCredit} from '../tranche-credit.model';
import { TrancheCreditElement } from '../tranche-credit.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-tranches-credits',
    templateUrl: 'dialog-list-tranches-credits.component.html',
  })
  export class DialogListTranchesCreditsComponent implements OnInit {
     private listTranchesCredits: TrancheCredit[] = [];
     private  dataTranchesCreditsSource: MatTableDataSource<TrancheCreditElement>;
     private selection = new SelectionModel<TrancheCreditElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private creditService: CreditService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      console.log('code credit in dialog component is :', this.data.codeCredit);
      this.creditService.getTranchesCreditsByCodeCredit(this.data.codeCredit).subscribe((listTranchesCredits) => {
        this.listTranchesCredits = listTranchesCredits;
console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedTranchesCredits: TrancheCredit[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((trancheCreditElement) => {
    return trancheCreditElement.code;
  });
  this.listTranchesCredits.forEach(trancheCreditElement => {
    if (codes.findIndex(code => code === trancheCreditElement.code) > -1) {
      listSelectedTranchesCredits.push(trancheCreditElement);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listTranchesCredits !== undefined) {

      this.listTranchesCredits.forEach(trancheCredit => {
             listElements.push( {code: trancheCredit.code ,
            datePayementProgramme: trancheCredit.datePayementProgramme,
          montantPaye: trancheCredit.montantPaye,
          etat: trancheCredit.etat,
          description: trancheCredit.description
        
        } );
      });
    }
      this.dataTranchesCreditsSource = new MatTableDataSource<TrancheCreditElement>(listElements);
      this.dataTranchesCreditsSource.sort = this.sort;
      this.dataTranchesCreditsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'datePayementProgramme' , displayTitle: 'Date de Payement Prévue'},
     {name: 'montantPaye' , displayTitle: 'Montant Prévu'},
     {name: 'etat' , displayTitle: 'Etat'},
      {name: 'description' , displayTitle: 'description'}
    
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataTranchesCreditsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataTranchesCreditsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataTranchesCreditsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataTranchesCreditsSource.paginator) {
      this.dataTranchesCreditsSource.paginator.firstPage();
    }
  }

}
