
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {CreditService} from '../credit.service';
import {PayementCredit} from '../payement-credit.model';
import { PayementCreditElement } from '../payement-credit.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-payements-credits',
    templateUrl: 'dialog-list-payements-credits.component.html',
  })
  export class DialogListPayementsCreditsComponent implements OnInit {
     private listPayementsCredits: PayementCredit[] = [];
     private  dataPayementsCreditsSource: MatTableDataSource<PayementCreditElement>;
     private selection = new SelectionModel<PayementCreditElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private creditService: CreditService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      console.log('code credit in dialog component is :', this.data.codeCredit);
      this.creditService.getPayementsCreditsByCodeCredit(this.data.codeCredit).subscribe((listPayementsCredits) => {
        this.listPayementsCredits = listPayementsCredits;
console.log('recieving data from backend', this.listPayementsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedPayementsCredits: PayementCredit[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((payementCreditElement) => {
    return payementCreditElement.code;
  });
  this.listPayementsCredits.forEach(payementCredit => {
    if (codes.findIndex(code => code === payementCredit.code) > -1) {
      listSelectedPayementsCredits.push(payementCredit);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listPayementsCredits !== undefined) {

      this.listPayementsCredits.forEach(payementCredit => {
             listElements.push( {code: payementCredit.code ,
          dateOperation: payementCredit.dateOperation,
          montantPaye: payementCredit.montantPaye,
          modePayement: payementCredit.modePayement,
          numeroCheque: payementCredit.numeroCheque,
          dateCheque: payementCredit.dateCheque,
          compte: payementCredit.compte,
          compteAdversaire: payementCredit.compteAdversaire,
          banque: payementCredit.banque,
          banqueAdversaire: payementCredit.banqueAdversaire,
          description: payementCredit.description
        } );
      });
    }
      this.dataPayementsCreditsSource = new MatTableDataSource<PayementCreditElement>(listElements);
      this.dataPayementsCreditsSource.sort = this.sort;
      this.dataPayementsCreditsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'}, 
     {name: 'montantPaye' , displayTitle: 'Montant Payé'},
     {name: 'modePayement' , displayTitle: 'Mode Payement'},
      {name: 'numeroCheque' , displayTitle: 'N°Chèque'},
     {name: 'dateCheque' , displayTitle: 'Date Chèque'}, 
     {name: 'compte' , displayTitle: 'Compte'},
     {name: 'compteAdversaire' , displayTitle: 'Compte Adv.'},
      {name: 'banque' , displayTitle: 'Banque'},
       { name : 'banqueAdversaire' , displayTitle : 'Banque Adv.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataPayementsCreditsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataPayementsCreditsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataPayementsCreditsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataPayementsCreditsSource.paginator) {
      this.dataPayementsCreditsSource.paginator.firstPage();
    }
  }

}
