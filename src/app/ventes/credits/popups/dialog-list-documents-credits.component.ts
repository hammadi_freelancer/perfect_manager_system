
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {CreditService} from '../credit.service';
import {DocumentCredit} from '../document-credit.model';
import { DocumentCreditElement } from '../document-credit.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-documents-credits',
    templateUrl: 'dialog-list-documents-credits.component.html',
  })
  export class DialogListDocumentsCreditsComponent implements OnInit {
     private listDocumentsCredits: DocumentCredit[] = [];
     private  dataDocumentsCreditsSource: MatTableDataSource<DocumentCreditElement>;
     private selection = new SelectionModel<DocumentCreditElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private creditService: CreditService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      console.log('code credit in dialog component is :', this.data.codeCredit);
      this.creditService.getDocumentsCreditsByCodeCredit(this.data.codeCredit).subscribe((listDocumentsCredits) => {
        this.listDocumentsCredits = listDocumentsCredits;
console.log('recieving data from backend', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedDocumentsCredits: DocumentCredit[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentCreditElement) => {
    return documentCreditElement.code;
  });
  this.listDocumentsCredits.forEach(documentCredit => {
    if (codes.findIndex(code => code === documentCredit.code) > -1) {
      listSelectedDocumentsCredits.push(documentCredit);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsCredits !== undefined) {

      this.listDocumentsCredits.forEach(docCredit => {
             listElements.push( {code: docCredit.code ,
          dateReception: docCredit.dateReception,
          numeroDocument: docCredit.numeroDocument,
          typeDocument: docCredit.typeDocument,
          description: docCredit.description
        } );
      });
    }
      this.dataDocumentsCreditsSource = new MatTableDataSource<DocumentCreditElement>(listElements);
      this.dataDocumentsCreditsSource.sort = this.sort;
      this.dataDocumentsCreditsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsCreditsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsCreditsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsCreditsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsCreditsSource.paginator) {
      this.dataDocumentsCreditsSource.paginator.firstPage();
    }
  }

}
