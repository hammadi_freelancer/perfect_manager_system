
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {CreditService} from '../credit.service';
import {TrancheCredit} from '../tranche-credit.model';

@Component({
    selector: 'app-dialog-add-tranche-credit',
    templateUrl: 'dialog-add-tranche-credit.component.html',
  })
  export class DialogAddTrancheCreditComponent implements OnInit {
     private trancheCredit: TrancheCredit;
     private saveEnCours = false;
     
     constructor(  private creditService: CreditService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.trancheCredit = new TrancheCredit();
    }
    saveTrancheCredit() 
    {
      this.trancheCredit.codeCredit = this.data.codeCredit;
       console.log(this.trancheCredit);
       this.saveEnCours = true;
       
      //  this.saveEnCours = true;
       this.creditService.addTrancheCredit(this.trancheCredit).subscribe((response) => {
        this.saveEnCours = false;
        
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.trancheCredit = new TrancheCredit();
             this.openSnackBar(  'Tranche Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
