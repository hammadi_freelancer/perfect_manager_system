
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {CreditService} from '../credit.service';
import {LigneCredit} from '../ligne-credit.model';
import { LigneCreditElement } from '../ligne-credit.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-lignes-credits',
    templateUrl: 'dialog-list-lignes-credits.component.html',
  })
  export class DialogListLignesCreditsComponent implements OnInit {
     private listLignesCredits: LigneCredit[] = [];
     private  dataLignesCreditsSource: MatTableDataSource<LigneCreditElement>;
     private selection = new SelectionModel<LigneCreditElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private creditService: CreditService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      console.log('code credit in dialog component is :', this.data.codeCredit);
      this.creditService.getLignesCreditsByCodeCredit(this.data.codeCredit).subscribe((listLignesCredits) => {
        this.listLignesCredits = listLignesCredits;
// console.log('recieving data from backend', this.listPayementsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedLignesCredits: LigneCredit[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((ligneCreditElement) => {
    return ligneCreditElement.code;
  });
  this.listLignesCredits.forEach(ligneCredit => {
    if (codes.findIndex(code => code === ligneCredit.code) > -1) {
      listSelectedLignesCredits.push(ligneCredit);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listLignesCredits !== undefined) {

      this.listLignesCredits.forEach(ligneCredit => {
             listElements.push( {code: ligneCredit.code ,
          dateOperation: ligneCredit.dateOperation,
          montantAPaye: ligneCredit.montantAPayer,
          description: ligneCredit.description
        } );
      });
    }
      this.dataLignesCreditsSource = new MatTableDataSource<LigneCreditElement>(listElements);
      this.dataLignesCreditsSource.sort = this.sort;
      this.dataLignesCreditsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'}, 
     {name: 'montantAPayer' , displayTitle: 'Montant A Payer'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataLignesCreditsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataLignesCreditsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataLignesCreditsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataLignesCreditsSource.paginator) {
      this.dataLignesCreditsSource.paginator.firstPage();
    }
  }

}
