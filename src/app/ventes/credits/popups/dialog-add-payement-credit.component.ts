
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {CreditService} from '../credit.service';
import {PayementCredit} from '../payement-credit.model';

@Component({
    selector: 'app-dialog-add-payement-credit',
    templateUrl: 'dialog-add-payement-credit.component.html',
  })
  export class DialogAddPayementCreditComponent implements OnInit {
     private payementCredit: PayementCredit;
     private saveEnCours = false;
     
     constructor(  private creditService: CreditService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.payementCredit = new PayementCredit();
    }
    savePayementCredit() 
    {
      this.payementCredit.codeCredit = this.data.codeCredit;
       console.log(this.payementCredit);
       this.saveEnCours = true;
       
      //  this.saveEnCours = true;
       this.creditService.addPayementCredit(this.payementCredit).subscribe((response) => {
        this.saveEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.payementCredit = new PayementCredit();
             this.openSnackBar(  'Payement Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
