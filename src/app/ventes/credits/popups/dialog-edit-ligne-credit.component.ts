
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {CreditService} from '../credit.service';
import {LigneCredit} from '../ligne-credit.model';

@Component({
    selector: 'app-dialog-edit-ligne-credit',
    templateUrl: 'dialog-edit-ligne-credit.component.html',
  })
  export class DialogEditLigneCreditComponent implements OnInit {
     private ligneCredit: LigneCredit;
     private etatsLigne: any[] = [
      {value: 'NonReglee', viewValue: 'Non Réglée'},
      {value: 'Regle', viewValue: 'Réglée'}
    ];
     private updateEnCours = false;
     constructor(  private creditService: CreditService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit() {
        this.creditService.getLigneCredit(this.data.codeLigneCredit).subscribe((ligne) => {
            this.ligneCredit = ligne;
     });
   }
    updateLigneCredit() {
      //  this.saveEnCours = true;
      this.updateEnCours = true;
       this.creditService.updateLigneCredit(this.ligneCredit).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             // this.payementCredit = new PayementCredit();
             this.openSnackBar(  'Ligne Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
