
import { Component, OnInit, Inject, ViewChild, Input, AfterViewChecked, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {CreditService} from './credit.service';
import { Credit } from './credit.model';
import {LigneCredit} from './ligne-credit.model';
import { LigneCreditElement } from './ligne-credit.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DialogAddLigneCreditComponent } from './popups/dialog-add-ligne-credit.component';
import { DialogEditLigneCreditComponent } from './popups/dialog-edit-ligne-credit.component';
@Component({
    selector: 'app-lignes-credits-grid',
    templateUrl: 'lignes-credits-grid.component.html',
  })
  export class LignesCreditsGridComponent implements OnInit, OnChanges {
     private listLignesCredits: LigneCredit[] = [];
     private  dataLignesCreditsSource: MatTableDataSource<LigneCreditElement>;
     private selection = new SelectionModel<LigneCreditElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];

     @Input()
     private credit: Credit;

     constructor(  private creditService: CreditService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      console.log('credit code after dipslay tab ', this.credit.code);
      this.creditService.getLignesCreditsByCodeCredit(this.credit.code).subscribe((listLignesCredits) => {
        this.listLignesCredits = listLignesCredits;
// console.log('recieving data from backend', this.listPayementsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }

  ngOnChanges() {
    this.creditService.getLignesCreditsByCodeCredit(this.credit.code).subscribe((listLignesCredits) => {
      this.listLignesCredits = listLignesCredits;
//console.log('recieving data from backend', this.listPayementsCredits  );
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }
  checkOneActionInvoked() {
    const listSelectedLignesCredits: LigneCredit[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((ligneCreditElement) => {
    return ligneCreditElement.code;
  });
  this.listLignesCredits.forEach(ligneCredit => {
    if (codes.findIndex(code => code === ligneCredit.code) > -1) {
      listSelectedLignesCredits.push(ligneCredit);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listLignesCredits !== undefined) {

      this.listLignesCredits.forEach(ligneCredit => {
             listElements.push( {code: ligneCredit.code ,
          dateOperation: ligneCredit.dateOperation,
          montantAPaye: ligneCredit.montantAPayer,
          description: ligneCredit.description,
          etat: ligneCredit.etat,
        } );
      });
    }
      this.dataLignesCreditsSource = new MatTableDataSource<LigneCreditElement>(listElements);
      this.dataLignesCreditsSource.sort = this.sort;
      this.dataLignesCreditsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'},
     {name: 'montantAPaye' , displayTitle: 'Montant A Payer'},
       { name : 'description' , displayTitle : 'Déscription'},
       { name: 'etat', displayTitle : 'Etat' },
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataLignesCreditsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataLignesCreditsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataLignesCreditsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataLignesCreditsSource.paginator) {
      this.dataLignesCreditsSource.paginator.firstPage();
    }
  }

  showAddLigneDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeCredit : this.credit.code
   };
  
   const dialogRef = this.dialog.open(DialogAddLigneCreditComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(ligneAdded => {
    console.log('The dialog was closed');
  
     // this.listLignesCredits .push(ligneAdded);
     this.refresh() ;

});
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditLigneDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeLigneCredit : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditLigneCreditComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(ligneAdded => {
        console.log('The dialog was closed');
      
         // this.listLignesCredits .push(ligneAdded);
         this.refresh() ;
    
    });
    } else {
      this.openSnackBar('Aucune Ligne Séléctionnée!');
    }
    }
    supprimerLignes()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(pay, index) {
              this.creditService.deleteLigneCredit(pay.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( this.selection.selected.length + ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucune Ligne Séléctionnée!');
        }
    }
    refresh() {
      this.creditService.getLignesCreditsByCodeCredit(this.credit.code).subscribe((listLignesCredits) => {
        this.listLignesCredits = listLignesCredits;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
















}
