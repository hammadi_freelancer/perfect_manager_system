


export interface DocumentCreditElement {
         code: string ;
         dateReception: string ;
         numeroDocument: number;
         typeDocument?: string;
          description?: string;
}
