import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';
import { HttpParams } from '@angular/common/http';

import {Credit} from './credit.model' ;
import { PayementCredit} from './payement-credit.model';
import { TrancheCredit } from './tranche-credit.model';
import { DocumentCredit } from './document-credit.model';
import { LigneCredit } from './ligne-credit.model';
import { AjournementCredit} from './ajournement-credit.model';

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_CREDITS = 'http://localhost:8082/finance/credits/getCredits';
const URL_GET_CREDIT = 'http://localhost:8082/finance/credits/getCredit';
const  URL_ADD_CREDIT = 'http://localhost:8082/finance/credits/addCredit';
const  URL_UPDATE_CREDITS = 'http://localhost:8082/finance/credits/updateCredit';
const  URL_DELETE_CREDIT = 'http://localhost:8082/finance/credits/deleteCredit';
const  URL_GET_TOTAL_CREDITS = 'http://localhost:8082/finance/credits/getTotalCredits';
const URL_GET_TOTAL_PAYEMENTS_CREDIT_BY_CODE_CREDIT = 'http://localhost:8082/finance/credits/getTotalPayementsCreditsByCodeCredit';

const  URL_RECUPERATE_MONTANTS_BY_PERIODE = 'http://localhost:8082/finance/credits/getMontantsByPeriode';


const  URL_ADD_PAYEMENT_CREDIT = 'http://localhost:8082/finance/payementsCredits/addPayementCredit';
const  URL_GET_LIST_PAYEMENT_CREDIT = 'http://localhost:8082/finance/payementsCredits/getPayementsCredits';
const  URL_DELETE_PAYEMENT_CREDIT = 'http://localhost:8082/finance/payementsCredits/deletePayementCredit';
const  URL_UPDATE_PAYEMENT_CREDIT = 'http://localhost:8082/finance/payementsCredits/updatePayementCredit';
const URL_GET_PAYEMENT_CREDIT = 'http://localhost:8082/finance/payementsCredits/getPayementCredit';
const URL_GET_LIST_PAYEMENT_CREDIT_BY_CODE_CREDIT = 'http://localhost:8082/finance/payementsCredits/getPayementsCreditsByCodeCredit';






const  URL_ADD_AJOURNEMENT_CREDIT = 'http://localhost:8082/finance/ajournementsCredits/addAjournementCredit';
const  URL_GET_LIST_AJOURNEMENT_CREDIT = 'http://localhost:8082/finance/ajournementsCredits/getAjournementsCredits';
const  URL_DELETE_AJOURNEMENT_CREDIT = 'http://localhost:8082/finance/ajournementsCredits/deleteAjournementCredit';
const  URL_UPDATE_AJOURNEMENT_CREDIT = 'http://localhost:8082/finance/ajournementsCredits/updateAjournementCredit';
const URL_GET_AJOURNEMENT_CREDIT = 'http://localhost:8082/finance/ajournementsCredits/getAjournementCredit';
const URL_GET_LIST_AJOURNEMENT_CREDIT_BY_CODE_CREDIT =
 'http://localhost:8082/finance/ajournementsCredits/getAjournementsCreditsByCodeCredit';

 const  URL_ADD_TRANCHE_CREDIT = 'http://localhost:8082/finance/tranchesCredits/addTrancheCredit';
 const  URL_GET_LIST_TRANCHE_CREDIT = 'http://localhost:8082/finance/tranchesCredits/getTranchesCredits';
 const  URL_DELETE_TRANCHE_CREDIT = 'http://localhost:8082/finance/tranchesCredits/deleteTrancheCredit';
 const  URL_UPDATE_TRANCHE_CREDIT = 'http://localhost:8082/finance/tranchesCredits/updateTrancheCredit';
 const URL_GET_TRANCHE_CREDIT = 'http://localhost:8082/finance/tranchesCredits/getTrancheCredit';
 const URL_GET_LIST_TRANCHE_CREDIT_BY_CODE_CREDIT =
  'http://localhost:8082/finance/tranchesCredits/getTranchesCreditsByCodeCredit';

  const  URL_ADD_DOCUMENT_CREDIT = 'http://localhost:8082/finance/documentsCredits/addDocumentCredit';
  const  URL_GET_LIST_DOCUMENT_CREDIT = 'http://localhost:8082/finance/documentsCredits/getDocumentsCredits';
  const  URL_DELETE_DOCUCMENT_CREDIT = 'http://localhost:8082/finance/documentsCredits/deleteDocumentCredit';
  const  URL_UPDATE_DOCUMENT_CREDIT = 'http://localhost:8082/finance/documentsCredits/updateDocumentCredit';
  const URL_GET_DOCUMENT_CREDIT = 'http://localhost:8082/finance/documentsCredits/getDocumentCredit';
  const URL_GET_LIST_DOCUMENT_CREDIT_BY_CODE_CREDIT =
   'http://localhost:8082/finance/documentsCredits/getDocumentsCreditsByCodeCredit';

   const  URL_ADD_LIGNE_CREDIT = 'http://localhost:8082/finance/lignesCredits/addLigneCredit';
   const  URL_GET_LIST_LIGNE_CREDIT = 'http://localhost:8082/finance/lignesCredits/getLignesCredits';
   const  URL_DELETE_LIGNE_CREDIT = 'http://localhost:8082/finance/lignesCredits/deleteLigneCredit';
   const  URL_UPDATE_LIGNE_CREDIT = 'http://localhost:8082/finance/lignesCredits/updateLigneCredit';
   const URL_GET_LIGNE_CREDIT = 'http://localhost:8082/finance/lignesCredits/getLigneCredit';
   const URL_GET_LIST_LIGNE_CREDIT_BY_CODE_CREDIT =
    'http://localhost:8082/finance/lignesCredits/getLignesCreditsByCodeCredit';

@Injectable({
  providedIn: 'root'
})
export class CreditService {

  constructor(private httpClient: HttpClient) {

   }

   getTotalPayementsCreditsByCodeCredit(codeCredit): Observable<any> {
    return this.httpClient
    .get<any>(URL_GET_TOTAL_PAYEMENTS_CREDIT_BY_CODE_CREDIT + '/' + codeCredit);
   }
   getTotalCredits(filter): Observable<any> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('filter', filterStr);
        return this.httpClient
        .get<any>(URL_GET_TOTAL_CREDITS, {params});
       }
   getCredits(pageNumber, nbElementsPerPage, filter): Observable<Credit[]> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('pageNumber', pageNumber)
    .set('nbElementsPerPage', nbElementsPerPage)
    .set('sort', 'name')
    .set('filter', filterStr);
    return this.httpClient
    .get<Credit[]>(URL_GET_LIST_CREDITS, {params});
   }




   addCredit(credit: Credit): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_CREDIT, credit);
  }
  updateCredit(credit: Credit): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_CREDITS, credit);
  }
  deleteCredit(codeCredit): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_CREDIT + '/' + codeCredit);
  }
  getCredit(codeCredit): Observable<Credit> {
    return this.httpClient.get<Credit>(URL_GET_CREDIT + '/' + codeCredit);
  }
  addPayementCredit(payementCredit: PayementCredit): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_PAYEMENT_CREDIT, payementCredit);
    
  }
  updatePayementCredit(pay: PayementCredit): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_PAYEMENT_CREDIT, pay);
  }
  getPayementsCredits(): Observable<PayementCredit[]> {
    return this.httpClient
    .get<PayementCredit[]>(URL_GET_LIST_PAYEMENT_CREDIT);
   }
   getPayementCredit(codePayementCredit): Observable<PayementCredit> {
    return this.httpClient.get<PayementCredit>(URL_GET_PAYEMENT_CREDIT + '/' + codePayementCredit);
  }
  deletePayementCredit(codePayementCredit): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_PAYEMENT_CREDIT + '/' + codePayementCredit);
  }
  getPayementsCreditsByCodeCredit(codeCredit): Observable<PayementCredit[]> {
    return this.httpClient.get<PayementCredit[]>(URL_GET_LIST_PAYEMENT_CREDIT_BY_CODE_CREDIT + '/' + codeCredit);
  }





  addAjournementCredit(ajournementCredit: AjournementCredit): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_AJOURNEMENT_CREDIT, ajournementCredit);
    
  }
  updateAjournementCredit(ajour: AjournementCredit): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_AJOURNEMENT_CREDIT, ajour);
  }
  getAjournementsCredits(): Observable<AjournementCredit[]> {
    return this.httpClient
    .get<AjournementCredit[]>(URL_GET_LIST_AJOURNEMENT_CREDIT);
   }
   getAjournementCredit(codeAjournementCredit): Observable<AjournementCredit> {
    return this.httpClient.get<AjournementCredit>(URL_GET_AJOURNEMENT_CREDIT + '/' + codeAjournementCredit);
  }
  deleteAjournementCredit(codeAjournementCredit): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_AJOURNEMENT_CREDIT + '/' + codeAjournementCredit);
  }
  getAjournementsCreditsByCodeCredit(codeCredit): Observable<AjournementCredit[]> {
    return this.httpClient.get<AjournementCredit[]>(URL_GET_LIST_AJOURNEMENT_CREDIT_BY_CODE_CREDIT + '/' + codeCredit);
  }




  addTrancheCredit(trancheCredit: TrancheCredit): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_TRANCHE_CREDIT, trancheCredit);
    
  }
  updateTrancheCredit(tr: TrancheCredit): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_TRANCHE_CREDIT, tr);
  }
  getTranchesCredits(): Observable<TrancheCredit[]> {
    return this.httpClient
    .get<TrancheCredit[]>(URL_GET_LIST_TRANCHE_CREDIT);
   }
   getTrancheCredit(codeTrancheCredit): Observable<TrancheCredit> {
    return this.httpClient.get<TrancheCredit>(URL_GET_TRANCHE_CREDIT + '/' + codeTrancheCredit);
  }
  deleteTrancheCredit(codeTrancheCredit): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_TRANCHE_CREDIT + '/' + codeTrancheCredit);
  }
  getTranchesCreditsByCodeCredit(codeCredit): Observable<TrancheCredit[]> {
    return this.httpClient.get<TrancheCredit[]>(URL_GET_LIST_TRANCHE_CREDIT_BY_CODE_CREDIT + '/' + codeCredit);
  }

  addDocumentCredit(documentCredit: DocumentCredit): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_DOCUMENT_CREDIT, documentCredit);
    
  }
  updateDocumentCredit(doc: DocumentCredit): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_DOCUMENT_CREDIT, doc);
  }
  getDocumentsCredits(): Observable<DocumentCredit[]> {
    return this.httpClient
    .get<DocumentCredit[]>(URL_GET_LIST_DOCUMENT_CREDIT);
   }
   getDocumentCredit(codeDocumentCredit): Observable<DocumentCredit> {
    return this.httpClient.get<DocumentCredit>(URL_GET_DOCUMENT_CREDIT + '/' + codeDocumentCredit);
  }
  deleteDocumentCredit(codeDocumentCredit): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_DOCUCMENT_CREDIT + '/' + codeDocumentCredit);
  }
  getDocumentsCreditsByCodeCredit(codeCredit): Observable<DocumentCredit[]> {
    return this.httpClient.get<DocumentCredit[]>(URL_GET_LIST_DOCUMENT_CREDIT_BY_CODE_CREDIT + '/' + codeCredit);
  }

  addLigneCredit(ligneCredit: LigneCredit): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_LIGNE_CREDIT, ligneCredit);
    
  }
  updateLigneCredit(ligneCredit: LigneCredit): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_LIGNE_CREDIT, ligneCredit);
  }
  getLignesCredits(): Observable<LigneCredit[]> {
    return this.httpClient
    .get<LigneCredit[]>(URL_GET_LIST_LIGNE_CREDIT);
   }
   getLigneCredit(codeLigneCredit): Observable<LigneCredit> {
    return this.httpClient.get<LigneCredit>(URL_GET_LIGNE_CREDIT + '/' + codeLigneCredit);
  }
  deleteLigneCredit(codeLigneCredit): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_LIGNE_CREDIT + '/' + codeLigneCredit);
  }
  getLignesCreditsByCodeCredit(codeLigneCredit): Observable<LigneCredit[]> {
    return this.httpClient.get<LigneCredit[]>(URL_GET_LIST_LIGNE_CREDIT_BY_CODE_CREDIT + '/' + codeLigneCredit);
  }

  recuperateMontants(dateDepart, dateFin): Observable<any>
  {
    return this.httpClient.post<any>(URL_RECUPERATE_MONTANTS_BY_PERIODE , {'startDate': dateDepart, 'findDate': dateFin});
    
  }



}
