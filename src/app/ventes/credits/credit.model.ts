
import { BaseEntity } from '../../common/base-entity.model' ;
import { Client } from '../../ventes/clients/client.model' ;

export const MODES_GENERATION_TRANCHES = ['AUTOMATIQUE', 'MANUELLE', ];
type Etat = 'Initial' | 'Valide' | 'Annule' | ''

export class Credit extends BaseEntity {
    constructor(
        public code?: string,
        public numeroCreditVente?: string,
         public client?: Client,
         public montantAPayer?: number,
         public montantTouche?: number,
         public montantReste?: number,
        public periodTranche?: string ,
        public  dateDebutPayement?: string,
        public descriptionEchange?: string,
        public avance?: number, public dateOperation?: string,
        public dateOperationObject?: Date,
        public dateDebutPayementObject?: Date,
        public dateNextPayementObject?: Date,
        public dateLastPayementObject?: Date,
        public dateNextPayement?: string,
        public dateLastPayement?: string,
     public valeurTranche?: number, public nbreTranches?: number ,
     public modeGenerationTranches = MODES_GENERATION_TRANCHES[1],
     public etat?: Etat,
    // public listTranches?: Tranche[]
     ) {
         super();
        //  this.listTranches = [];
         this.client = new Client();
         // this.categoryClient = 'PERSONNE_PHYSIQUE';
    }
    static constructDefaultInstance(): Credit {
         return new Credit( );
    }
}
