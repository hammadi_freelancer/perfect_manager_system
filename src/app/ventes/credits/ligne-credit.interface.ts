


export interface LigneCreditElement {
    code: string ;
    dateOperation: string ;
    montantAPaye: number;
     description?: string;
     etat?: string;
 
   }
