import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,
    
      ViewChild } from '@angular/core';
    // import { ArticleService } from './article.service';
    import {Credit} from './credit.model';
    import { MODES_GENERATION_TRANCHES} from './credit.model';
    import {CreditService} from './credit.service';
    import {FormControl} from '@angular/forms';
    import {map, startWith} from 'rxjs/operators';
    import {Observable} from 'rxjs';
    import { Router, ActivatedRoute, ParamMap } from '@angular/router';
    import {ActionData} from './action-data.interface';
    import { ClientService } from '../../ventes/clients/client.service';
    @Component({
        selector: 'app-credit-home',
        templateUrl: './credit-home.component.html',
        // styleUrls: ['./players.component.css']
      })
 export class CreditHomeComponent implements OnInit {

    @Input()
    private actionData: ActionData;

    
    private homeMessage = 'Here I will display the list of : last credits updated' +
    'last credits created ; last credits ...';

        constructor( private route: ActivatedRoute,
            private router: Router, private creditService: CreditService, private clientService: ClientService) {
          }

          ngOnInit() {
        }
        
        }
