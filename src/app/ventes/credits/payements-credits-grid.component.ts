
import { Component, OnInit, Inject, ViewChild, Input, AfterViewChecked, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {CreditService} from './credit.service';
import { Credit } from './credit.model';
import {PayementCredit} from './payement-credit.model';
import { PayementCreditElement } from './payement-credit.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DialogAddPayementCreditComponent } from './popups/dialog-add-payement-credit.component';
import { DialogEditPayementCreditComponent } from './popups/dialog-edit-payement-credit.component';
@Component({
    selector: 'app-payements-credits-grid',
    templateUrl: 'payements-credits-grid.component.html',
  })
  export class PayementsCreditsGridComponent implements OnInit, OnChanges {
     private listPayementsCredits: PayementCredit[] = [];
     private  dataPayementsCreditsSource: MatTableDataSource<PayementCreditElement>;
     private selection = new SelectionModel<PayementCreditElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];

     @Input()
     private credit: Credit;

     constructor(  private creditService: CreditService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      console.log('credit code after dipslay tab ', this.credit.code);
      this.creditService.getPayementsCreditsByCodeCredit(this.credit.code).subscribe((listPayementsCredits) => {
        this.listPayementsCredits = listPayementsCredits;
console.log('recieving data from backend', this.listPayementsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }

  ngOnChanges() {
    this.creditService.getPayementsCreditsByCodeCredit(this.credit.code).subscribe((listPayementsCredits) => {
      this.listPayementsCredits = listPayementsCredits;
console.log('recieving data from backend', this.listPayementsCredits  );
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }
  checkOneActionInvoked() {
    const listSelectedPayementsCredits: PayementCredit[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((payementCreditElement) => {
    return payementCreditElement.code;
  });
  this.listPayementsCredits.forEach(payementCredit => {
    if (codes.findIndex(code => code === payementCredit.code) > -1) {
      listSelectedPayementsCredits.push(payementCredit);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listPayementsCredits !== undefined) {

      this.listPayementsCredits.forEach(payementCredit => {
             listElements.push( {code: payementCredit.code ,
          dateOperation: payementCredit.dateOperation,
          montantPaye: payementCredit.montantPaye,
          modePayement: payementCredit.modePayement,
          numeroCheque: payementCredit.numeroCheque,
          dateCheque: payementCredit.dateCheque,
          compte: payementCredit.compte,
          compteAdversaire: payementCredit.compteAdversaire,
          banque: payementCredit.banque,
          banqueAdversaire: payementCredit.banqueAdversaire,
          description: payementCredit.description
        } );
      });
    }
      this.dataPayementsCreditsSource = new MatTableDataSource<PayementCreditElement>(listElements);
      this.dataPayementsCreditsSource.sort = this.sort;
      this.dataPayementsCreditsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'}, 
     {name: 'montantPaye' , displayTitle: 'Montant Payé'},
     {name: 'modePayement' , displayTitle: 'Mode Payement'},
      {name: 'numeroCheque' , displayTitle: 'N°Chèque'},
     {name: 'dateCheque' , displayTitle: 'Date Chèque'}, 
     {name: 'compte' , displayTitle: 'Compte'},
     {name: 'compteAdversaire' , displayTitle: 'Compte Adv.'},
      {name: 'banque' , displayTitle: 'Banque'},
       { name : 'banqueAdversaire' , displayTitle : 'Banque Adv.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataPayementsCreditsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataPayementsCreditsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataPayementsCreditsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataPayementsCreditsSource.paginator) {
      this.dataPayementsCreditsSource.paginator.firstPage();
    }
  }

  showAddPayementDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeCredit : this.credit.code
   };
 
   const dialogRef = this.dialog.open(DialogAddPayementCreditComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(payAdded => {
    console.log('The dialog was closed');
  
     // this.listLignesCredits .push(ligneAdded);
     this.refresh() ;

});
 
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditPayementDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codePayementCredit : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditPayementCreditComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(payAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
    });

    } else {
      this.openSnackBar('Aucun Paiement Séléctionnée!');
    }
    }
    supprimerPayements()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(pay, index) {
              this.creditService.deletePayementCredit(pay.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( this.selection.selected.length + ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Paiement Séléctionnée!');
        }
    }
    refresh() {
      this.creditService.getPayementsCreditsByCodeCredit(this.credit.code).subscribe((listPayementsCredits) => {
        this.listPayementsCredits = listPayementsCredits;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
















}
