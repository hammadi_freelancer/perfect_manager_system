export class CreditVentestDataFilter {

    numeroCreditVentes: string;
    montantAPayer: number;
    descriptionEchange: string;
    montantReste: number;
    montantTouche: number;
    
    dateDebutPayementFromObject: Date;
    dateDebutPayementToObject: Date;
    dateCreditVentesFromObject: Date;
    dateCreditVentesToObject: Date;
    dateNextPayementFromObject: Date;
    dateNextPayementToObject: Date;
    periodTranche: string;
    nbreTranches: number;
    etat: string;
    cinClient: string;
    nomClient: string;
    prenomClient: string;
    raisonClient: string;
    matriculeClient: string;
    registreClient: string;
}
