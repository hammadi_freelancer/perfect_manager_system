
import { Component, OnInit, Inject, ViewChild, Input, OnChanges} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {CreditService} from './credit.service';
import { Credit } from './credit.model';
import {AjournementCredit} from './ajournement-credit.model';
import { AjournementCreditElement } from './ajournement-credit.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import { DialogAddAjournementCreditComponent } from './popups/dialog-add-ajournement-credit.component';
import { DialogEditAjournementCreditComponent } from './popups/dialog-edit-ajournement-credit.component';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-ajournements-credits-grid',
    templateUrl: 'ajournements-credits-grid.component.html',
  })
  export class AjournementsCreditsGridComponent implements OnInit, OnChanges {
     private listAjournementCredits: AjournementCredit[] = [];
     private  dataAjournementsCreditsSource: MatTableDataSource<AjournementCreditElement>;
     private selection = new SelectionModel<AjournementCreditElement>(true, []);
     @Input()
     private credit : Credit;
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private creditService: CreditService,
     private matSnackBar: MatSnackBar, public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.creditService.getAjournementsCreditsByCodeCredit(this.credit.code).subscribe((listAjournementsCredits) => {
        this.listAjournementCredits = listAjournementsCredits;
console.log('recieving data from backend', this.listAjournementCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }

ngOnChanges()
{
  this.creditService.getAjournementsCreditsByCodeCredit(this.credit.code).subscribe((listAjournementsCredits) => {
    this.listAjournementCredits = listAjournementsCredits;
console.log('recieving data from backend', this.listAjournementCredits  );
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
     });
}
  checkOneActionInvoked() {
    const listSelectedAjournementsCredits: AjournementCredit[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((payementCreditElement) => {
    return payementCreditElement.code;
  });
  this.listAjournementCredits.forEach(ajournementCredit => {
    if (codes.findIndex(code => code === ajournementCredit.code) > -1) {
      listSelectedAjournementsCredits.push(ajournementCredit);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listAjournementCredits !== undefined) {

      this.listAjournementCredits.forEach(ajournementCredit => {
             listElements.push( {code: ajournementCredit.code ,
          dateOperation: ajournementCredit.dateOperation,
          nouvelleDatePayement: ajournementCredit.nouvelleDatePayement,
          motif: ajournementCredit.motif,
          description: ajournementCredit.description
        } );
      });
    }
      this.dataAjournementsCreditsSource = new MatTableDataSource<AjournementCreditElement>(listElements);
      this.dataAjournementsCreditsSource.sort = this.sort;
      this.dataAjournementsCreditsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'},
     {name: 'nouvelleDatePayement' , displayTitle: 'N°elle Date Pay.'},
     {name: 'motif' , displayTitle: 'Motif'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataAjournementsCreditsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataAjournementsCreditsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataAjournementsCreditsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataAjournementsCreditsSource.paginator) {
      this.dataAjournementsCreditsSource.paginator.firstPage();
    }
  }

  showAddAjournementDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeCredit : this.credit.code
   };
 
   const dialogRef = this.dialog.open(DialogAddAjournementCreditComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(ajAdd => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditAjournementDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeAjournementCredit : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditAjournementCreditComponent,
        dialogConfig
      );

      dialogRef.afterClosed().subscribe(ajAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Ajournement Séléctionnée!');
    }
    }
    supprimerAjournements()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(ajour, index) {
              this.creditService.deleteAjournementCredit(ajour.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( this.selection.selected.length + ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Ajournement Séléctionnée!');
        }
    }
    refresh() {
      this.creditService.getAjournementsCreditsByCodeCredit(this.credit.code).subscribe((listAjourCredits) => {
        this.listAjournementCredits = listAjourCredits;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }


}
