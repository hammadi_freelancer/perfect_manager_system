import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {Credit} from './credit.model';
import { MODES_GENERATION_TRANCHES} from './credit.model';

import {CreditService} from './credit.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {ActionData} from './action-data.interface';
import { ClientService } from '../../ventes/clients/client.service';
import { CreditFilter } from '../../ventes/credits/credit.filter';
import {Client} from '../../ventes/clients/client.model';
import { ClientFilter} from '../../ventes/clients/client.filter';
import {MatSnackBar} from '@angular/material';
import { DialogAddClientComponent } from '../clients/dialog-add-client.component';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';


@Component({
  selector: 'app-credit-new',
  templateUrl: './credit-new.component.html',
  // styleUrls: ['./players.component.css']
})
export class CreditNewComponent implements OnInit {

 private credit: Credit =  new Credit();

//@Input()
//private listCredits: any = [] ;

// @ViewChild('EDITIONBUTTONS') editionButtons: ElementRef;

//@ViewChild('SAVEBUTTON') saveButton: ElementRef;

//@ViewChild('PANELTRANCHES') panelTranches : ElementRef;
//@ViewChild('PANELHEADER') panelHeader : ElementRef;

//@Input()
//private actionData: ActionData;

private clientFilter: ClientFilter;
private listClients: Client[];
private saveEnCours = false;

private etatsCredits: any[] = [
  {value: 'Initial', viewValue: 'Initial'}
 // {value: 'Valide', viewValue: 'Validé'}
];
private periodesCreditsVentes: any[] = [
  {value: 'Semaine', viewValue: 'Semaine'},
  {value: 'Mois', viewValue: 'Mois'},
  {value: 'Trimestre', viewValue: 'Trimestre'},
  {value: 'Simestre', viewValue: 'Simestre'},
  {value: 'Annee', viewValue: 'Annéé'}
  
  
];
  constructor( private route: ActivatedRoute,
    private router: Router, private creditService: CreditService, private clientService: ClientService,
   private matSnackBar: MatSnackBar, private dialog: MatDialog) {
  }


  // myControl = new FormControl();
  // options: string[] = ['One', 'Two', 'Three'];
  // filteredOptions: Observable<string[]>;

  ngOnInit() {
  // this.clientService.getClients().subscribe((clients) => {
      //  this.creditFilter = new CreditFilter(clients);
   // });
              // this.creditFilter = new CreditFilter(this.listClients);
              this.listClients = this.route.snapshot.data.clients;
              this.clientFilter = new ClientFilter(this.listClients);
  }
  saveCredit() {
    /*if (this.modeUpdate ) {
      this.updateCredit();
    } else {*/
    console.log(this.credit);
    if(this.credit.etat === '' || this.credit.etat === undefined) {
      this.credit.etat = 'Initial';
    }
    if (this.credit.montantAPayer === 0 || this.credit.montantAPayer === undefined )
      {
        this.openSnackBar(  'Il\'est indisponsable de préciser le montant de crédit');
        
      } else  {
    // this.saveEnCours = true;
    this.saveEnCours = true;
    this.creditService.addCredit(this.credit).subscribe((response) => {
      this.saveEnCours = false;
        if (response.error_message ===  undefined) {
          console.log('undefined error message');
          // this.save.emit(response);
          this.credit =  new Credit();
          this.openSnackBar(  'Crédit Enregistré');
        } else {
         // show error message
         this.openSnackBar(  'Opération Echouée: Essayer Encore Une fois');
         
        }
    });
  }
  }
  fillDataNomPrenom() {
    const listFiltred  = this.listClients.filter((client) => (client.cin === this.credit.client.cin));
    this.credit.client.nom = listFiltred[0].nom;
    this.credit.client.prenom = listFiltred[0].prenom;
  }
  fillDataMatriculeRaison()
  {
    const listFiltred  = this.listClients.filter((client) => (client.registreCommerce === this.credit.client.registreCommerce));
    this.credit.client.matriculeFiscale = listFiltred[0].matriculeFiscale;
    this.credit.client.raisonSociale = listFiltred[0].raisonSociale;
  }
  vider() {
    this.credit =  Credit.constructDefaultInstance();
  }
  openSnackBar(messageToDisplay) {
   this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
       verticalPosition: 'top'});
  }

  loadClientFilter()
  {
   // this.clientFilter = new ClientFilter(this.listClients);
    this.clientService.getClients().subscribe((clients) => {
      this.listClients = clients;
       this.clientFilter = new ClientFilter(clients);
    });
  }


loadGridCredits() {
  this.router.navigate(['/pms/ventes/credits']) ;
}
previous() {
 // this.panelTranches.nativeElement.hidden = true;
  // this.panelHeader.nativeElement.hidden = false;
}
/*showTrancheParametersPanel() {
  this.credit.modeGenerationTranches = MODES_GENERATION_TRANCHES[0];

}*/
addClientParticulier()
{
 const dialogConfig = new MatDialogConfig();
 
 dialogConfig.disableClose = false;
 dialogConfig.autoFocus = true;
 dialogConfig.position = {
   top : '0'
 };
 dialogConfig.data = {
  isParticulier : true
};

 const dialogRef = this.dialog.open(DialogAddClientComponent,
   dialogConfig
 );
 dialogRef.afterClosed().subscribe(clientAdded => {
  console.log('The dialog was closed');

   this.loadClientFilter();

});
}


addClientEntreprise()
{
 const dialogConfig = new MatDialogConfig();
 
 dialogConfig.disableClose = false;
 dialogConfig.autoFocus = true;
 dialogConfig.position = {
   top : '0'
 };
 dialogConfig.data = {
  isParticulier : false
};

 const dialogRef = this.dialog.open(DialogAddClientComponent,
   dialogConfig
 );
 dialogRef.afterClosed().subscribe(clientAdded => {
 // console.log('The dialog was closed');
   this.loadClientFilter();
});
}
}
