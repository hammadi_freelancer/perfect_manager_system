export interface CreditElement {
         code: string ;
         numeroCreditVentes: string;
         dateCreditVentes: string ;
         cin: string ;
         nom: string;
         prenom: string;
         matriculeFiscale: string;
         raisonSociale: string;
         descriptionEchange: string;
         montantAPayer: number;
         avance: number;
          montantReste?: number;
          montantTouche?: number;
          periodTranche?: string;
         dateDebutPayement?: string;
         dateNextPayement?: string;
         
         valeurTranche?: number;
         etat?: string;
         nbreTranches?: number ;
        // public modeGenerationTranches = MODES_GENERATION_TRANCHES[1],
         // public listTranches?: Tranche[]
        }
