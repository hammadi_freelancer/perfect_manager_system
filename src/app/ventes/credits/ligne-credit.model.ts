
import { BaseEntity } from '../../common/base-entity.model' ;
type Etat = 'Initial' | 'Valide' | 'Annule' | 'Reglee' | 'PartiellementRegle' | 'NonReglee';

export class LigneCredit extends BaseEntity {
    constructor(public code ?: string,
        public codeCredit?: string,
         public dateOperationObject?: Date,
         public datePayementObject?: Date,
         public datePayement?: string,
         public dateOperation?: string,
     public montantAPayer?: number,
     public etat?: Etat,
    ) {
         super();
        this.etat =  'Initial';
    }
}
