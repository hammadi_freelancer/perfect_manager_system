import { Component, OnInit, Output,ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {Credit} from './credit.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {CreditService} from './credit.service';
import { ActionData } from './action-data.interface';
import { Client } from '../../ventes/clients/client.model';
import { CreditElement } from './credit.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {ClientService} from '../clients/client.service';
import { DialogAddClientComponent } from '../clients/dialog-add-client.component';
import { DialogAddPayementCreditComponent } from './popups/dialog-add-payement-credit.component';
import { DialogAddAjournementCreditComponent } from './popups/dialog-add-ajournement-credit.component';
import { DialogAddTrancheCreditComponent } from './popups/dialog-add-tranche-credit.component';
import { DialogAddDocumentCreditComponent } from './popups/dialog-add-document-credit.component';
import { DialogDisplayStatistiquesComponent } from './popups/dialog-display-statistiques.component';
import { CreditVentestDataFilter } from './credit-data.filter';
import { SelectItem } from 'primeng/api';

@Component({
  selector: 'app-credits-grid',
  templateUrl: './credits-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class CreditsGridComponent implements OnInit {

  private credit: Credit =  Credit.constructDefaultInstance();
  private showFilter = false;
  private showFilterClient = false;
  private showSelectColumnsMultiSelect = false;
  
  private creditVentesDataFilter = new CreditVentestDataFilter();
  private selection = new SelectionModel<CreditElement>(true, []);
  
@Output()
select: EventEmitter<Credit[]> = new EventEmitter<Credit[]>();

//private lastClientAdded: Client;

 selectedCredit: Credit ;

private deleteEnCours = false;

 @Input()
 private actionData: ActionData;

 @Input()
 private listCredits: Credit[] = [] ;
 listCreditsOrgin: any = [];
 private checkedAll: false;
 private showParticulier = false;
 private showEntreprise = false;
 private  dataCreditsSource: MatTableDataSource<CreditElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
 
 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay :ColumnDefinition[] = [];

 private columnsToSelect : SelectItem[];
  private  selectedColumnsDefinitions: string[];
  private periodesCreditsVentes: any[] = [
    {value: 'Semaine', viewValue: 'Semaine'},
    {value: 'Mois', viewValue: 'Mois'},
    {value: 'Trimestre', viewValue: 'Trimestre'},
    {value: 'Simestre', viewValue: 'Simestre'},
    {value: 'Annee', viewValue: 'Annéé'}
  ];
 private selectedColumnsNames: string[];
  constructor( private route: ActivatedRoute, private creditsService: CreditService,
    private router: Router, private matSnackBar: MatSnackBar,
    public dialog: MatDialog ) {
  }
  ngOnInit() {
    this.columnsToSelect = [
      {label: 'N° Crédit', value: 'numeroCreditVentes', title: 'N°Crédit'},
      {label: 'Date Crédit', value: 'dateCreditVentes', title: 'Date Crédit'},
      {label: 'Montant à Payer', value: 'montantAPayer', title: 'Montant à Payer'},
      {label: 'Montant Touché', value: 'montantTouche', title: 'Montant Touché'},
      {label: 'Montant Resté', value: 'montantReste', title: 'Montant Resté'},
      {label: 'Rs.Soc.Client', value:  'raisonSociale', title: 'Rs.Soc.Client' },
      {label: 'Matr.Client', value:  'matriculeFiscale', title: 'Matr.Client' },
      {label: 'Reg.Client', value:  'registreCommerce', title: 'Reg.Client' },
      {label: 'CIN Client', value: 'cin', title: 'CIN Client'},  
      {label: 'Nom Client', value: 'nom', title: 'Nom Client'},
      {label: 'Prénom Client', value: 'prenom', title: 'Prénom Client'},
      {label: 'Date Début Pai.', value: 'dateDebutPayement', title: 'Date Début Pai.' },
      {label: 'Nbre Tranches', value: 'nbreTranches', title: 'Nbre Tranches'},
      {label: 'Valeur Tranche', value: 'valeurTranche', title: 'Valeur Tranche'},
      {label: 'Période', value: 'periodTranche', title: 'Période'},
      {label: 'Déscription', value: 'descriptionEchange', title: 'Déscription' },
      {label: 'Etat', value:  'etat', title: 'Etat' },
  
  ];
  this.selectedColumnsDefinitions = [
    'numeroCreditVentes',
    'dateCreditVentes' , 
    'montantAPayer', 
    'nom' ,
    'prenom',
    'cin', 
    'etat'
    ];
    this.defaultColumnsDefinitions = [
      {name: 'numeroCreditVentes' , displayTitle: 'N°Crédit'},
      {name: 'dateCreditVentes' , displayTitle: 'Date Crédit'},
       {name: 'nom' , displayTitle: 'Nom Cl.'},
      {name: 'prenom' , displayTitle: 'Prénom Cl.'},
      {name: 'cin' , displayTitle: 'CIN Cl.'},
      {name: 'montantAPayer' , displayTitle: 'Valeur à Payer'},
      {name: 'montantReste' , displayTitle: 'Valeur Resté'},
      {name: 'montantTouche' , displayTitle: 'Valeur Touché'},
      {name: 'matricule' , displayTitle: 'Matr. Cl.'},
      {name: 'raison' , displayTitle: 'Raison Cl.'},
      {name: 'registre' , displayTitle: 'Registre Cl.'},
      {name: 'periodTranche' , displayTitle: 'Période '},
      {name: 'nbreTranches' , displayTitle: 'Nbre Tranches'},
      {name: 'valeurTranche' , displayTitle: 'Valeur Tranche'},
      {name: 'descriptionEchange' , displayTitle: 'Déscription'},
      {name: 'dateDebutPayement' , displayTitle: 'Date Début Pai.'},
      {name: 'dateNextPayement' , displayTitle: 'Date Prochaine Pai.'},
       {name: 'etat', displayTitle: 'Etat' },
 
       
      ];
    this.creditsService.getCredits(0, 5,null).subscribe((credits) => {
      this.listCredits = credits;
      // this.listClients[this.listClients.length] = this.lastClientAdded;
      // this.listClientsOrgin = this.listClients ;
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });
   /* this.creditsService.getCredits().subscribe((credits) => {
      console.log('list credits ', credits);
      // this.listCredits = credits;
      this.listCredits =  this.listCredits.map((credit) => {
        if (credit.client === 'undefined' || credit.client === null ) {
           credit.client = new Client();
         }
         return credit;
   });      this.listCreditsOrgin = this.listCredits ;
     });*/
    // this.listClientsOrgin = this.listClients ;

  }
  deleteCredit(credit) {
      console.log('call delete !', credit );
    this.creditsService.deleteCredit(credit.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData(null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.creditsService.getCredits(0, 5,filter ).subscribe((credits) => {
    this.listCredits = credits;
    this.transformDataToByConsumedByMatTable();
   });
  // this.listClientsOrgin = this.listClients ;
}


checkOneActionInvoked() {
  const listSelectedCredits: Credit[] = [];
console.log('selected clients are ');
console.log(this.selection.selected);
if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((creditElement) => {
  return creditElement.code;
});
this.listCredits.forEach(credit => {
  if (codes.findIndex(code => code === credit.code) > -1) {
    listSelectedCredits.push(credit);
  }
  });
}
this.select.emit(listSelectedCredits);

}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  this.dataCreditsSource = null;
  if (this.listCredits !== undefined) {
    console.log('diffe of undefined');
    this.listCredits.forEach(credit => {
           listElements.push( {
             code: credit.code ,
             numeroCreditVentes: credit.numeroCreditVente,
            dateCreditVentes: credit.dateOperation,
         cin: credit.client.cin, nom: credit.client.nom, prenom: credit.client.prenom,
        matriculeFiscale: credit.client.matriculeFiscale, raisonSociale: credit.client.raisonSociale,
         montantAPayer: credit.montantAPayer,
         descriptionEchange: credit.descriptionEchange,
        avance: credit.avance, montantReste: credit.montantReste,   periodTranche: credit.periodTranche,
        dateDebutPayement: credit.dateDebutPayement, valeurTranche: credit.valeurTranche, 
        nbreTranches: credit.nbreTranches,
        etat: credit.etat === 'Valide' ?   'Validé' : credit.etat === 'Annule' ? 'Annulé' : 'Initial'

      } );
    });
  }
    this.dataCreditsSource = new MatTableDataSource<CreditElement>(listElements);
    this.dataCreditsSource.sort = this.sort;
    this.dataCreditsSource.paginator = this.paginator;
    this.creditsService.getTotalCredits(this.creditVentesDataFilter).subscribe((response) => {
     console.log('Total credits is ', response);
      this.dataCreditsSource.paginator.length = response.totalCredits;
    });
}
specifyListColumnsToBeDisplayed() {
  
   console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
   this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
 ?
    this.defaultColumnsDefinitions : [];
    this.selectedColumnsNames = [];
    
    if (this.selectedColumnsDefinitions !== undefined)
     {
       this.selectedColumnsDefinitions.forEach((colName) => { 
            const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
            const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
            const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
            this.columnsDefinitionsToDisplay.push(colDef);
           });
     }
    this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
   this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
              return colDef.displayTitle;
    });
    console.log('columns to disp:', this.columnsDefinitionsToDisplay);
    this.columnsNames = [];
    this.columnsNames[0] = 'select';
    this.columnsDefinitionsToDisplay .map((colDef) => {
     this.columnsNames.push(colDef.name);
    if (this.selectedColumnsDefinitions !== undefined) {
     this.selectedColumnsNames.push(colDef.displayTitle);
    }
 });
    this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
     return colDef.displayTitleAr;
 });
 }
masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataCreditsSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataCreditsSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataCreditsSource.filter = filterValue.trim().toLowerCase();
  if (this.dataCreditsSource.paginator) {
    this.dataCreditsSource.paginator.firstPage();
  }
}

loadAddCreditComponent() {
  this.router.navigate(['/pms/ventes/ajouterCredit']) ;

}
loadEditCreditComponent() {
  this.router.navigate(['/pms/ventes/editerCredit', this.selection.selected[ this.selection.selected.length - 1].code]) ;

}
existCreditAnnuleValide(listCredits)
{
  listCredits.forEach(function(credit, index) {
    if (credit.etat === 'Valide' || credit.etat === 'Annule')
      {
           return true;
      }
  }, this);
    return false;
}
supprimerCredits()
{
  console.log('NBRE OF ELEMENTS SELECTED');
  console.log(this.selection.selected.length);
  console.log(' ELEMENTS SELECTED');
  console.log(this.selection.selected);
  if ( this.selection.selected.length !== 0) {
    
    
    if(this.existCreditAnnuleValide(this.selection.selected))
    {
       this.openSnackBar( 'Il n\'est pas possible de supprimer un crédit validé ou annulé' );
      
    }else{
      this.deleteEnCours = true;
      console.log(' deleteEnCours', this.deleteEnCours);
      this.selection.selected.forEach(function(credit, index) {
        
          this.creditsService.deleteCredit(credit.code).subscribe((response) => {
            
          });
          if (this.selection.selected.length - 1 === index) {
            this.deleteEnCours = false;
            this.openSnackBar( this.selection.selected.length + ' Element(s) Supprimé(s)');
             this.loadData();
            }
      }, this);
    }
    } else {
      this.openSnackBar('Aucun Crédit Séléctionné');
    }
}
refresh() {
  this.loadData(null);
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 addPayementCredit()
 {
  if ( this.selection.selected.length !== 0) {
    
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
    top : '0'
  };
  dialogConfig.data = {
    codeCredit : this.selection.selected[0].code
  };

  const dialogRef = this.dialog.open(DialogAddPayementCreditComponent,
    dialogConfig
  );
} else {
  this.openSnackBar('Aucun Crédit Séléctionné');
}
 }
 ajournerCredit()
 {
  if ( this.selection.selected.length !== 0) {
    
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
    top : '0'
  };
  dialogConfig.data = {
    codeCredit : this.selection.selected[0].code
  };

  const dialogRef = this.dialog.open(DialogAddAjournementCreditComponent,
    dialogConfig
  );
} else {
  this.openSnackBar('Aucun Crédit Séléctionné');
}
 }
 showStatistiquesDialog()
 {
 /* if ( this.selection.selected.length !== 0) {*/
    
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
    top : '0'
  };


  const dialogRef = this.dialog.open(DialogDisplayStatistiquesComponent,
    dialogConfig
  );

 }
 history() {

 }
 addClient() {
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
    top : '0'
  };
  dialogConfig.data = {
    isParticulier : 'none'
  };
  const dialogRef = this.dialog.open(DialogAddClientComponent,
    dialogConfig
  );
 }
 attacherDocumentCredit()
 {
  if ( this.selection.selected.length !== 0) {
    
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
    top : '0'
  };
  dialogConfig.data = {
    codeCredit : this.selection.selected[0].code
  };

  const dialogRef = this.dialog.open(DialogAddDocumentCreditComponent,
    dialogConfig
  );
} else {
  this.openSnackBar('Aucun Crédit Séléctionné!');
}
}
 
 addTrancheCredit()
 {
  if ( this.selection.selected.length !== 0) {
    
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
    top : '0'
  };
  dialogConfig.data = {
    codeCredit : this.selection.selected[0].code
  };

  const dialogRef = this.dialog.open(DialogAddTrancheCreditComponent,
    dialogConfig
  );
} else {
  this.openSnackBar('Aucun Crédit Séléctionné!');
}
}

changePage($event) {
  console.log('page event is ', $event);
 this.creditsService.getCredits($event.pageIndex, $event.pageSize, this.creditVentesDataFilter ).
 subscribe((credits) => {
    this.listCredits = credits;
    console.log(this.listCredits);
   // this.transformDataToByConsumedByMatTable();
   const listElements = [];
     console.log('diffe of undefined');
     this.listCredits.forEach(credit => {
            listElements.push( {code: credit.code ,
             numeroCreditVente: credit.numeroCreditVente,
             dateCreditVentes: credit.dateOperation,
          cin: credit.client.cin, nom: credit.client.nom, prenom: credit.client.prenom,
         matriculeFiscale: credit.client.matriculeFiscale, raisonSociale: credit.client.raisonSociale,
          montantAPayer: credit.montantAPayer,
          descriptionEchange: credit.descriptionEchange,
         avance: credit.avance, montantReste: credit.montantReste,   periodTranche: credit.periodTranche,
         dateDebutPayement: credit.dateDebutPayement, valeurTranche: credit.valeurTranche, 
         nbreTranches: credit.nbreTranches,
         etat: credit.etat === 'Valide' ?   'Validé' : credit.etat === 'Annule' ? 'Annulé' : 'Initial'
 
       } );
     });
    // const pagin = this.dataCreditsSource.paginator;
    // const sor = this.dataCreditsSource.sort;
     this.dataCreditsSource = new MatTableDataSource<CreditElement>(listElements);
     console.log('creditSource');
     console.log(this.dataCreditsSource );
     // this.dataCreditsSource.sort = sor;
     // this.dataCreditsSource.paginator = pagin;
     this.creditsService.getTotalCredits(this.creditVentesDataFilter).subscribe((response) => {
      console.log('Total credits is ', response);
      if( this.dataCreditsSource.paginator !== null &&  this.dataCreditsSource.paginator !== undefined ) {
       this.dataCreditsSource.paginator.length = response.totalCredits;
      }
     });

    });
}
toggleFilterPanel() {
  this.showFilter = !this.showFilter;
}
toggleShowColumnsMultiSelect() {
 this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
}
loadFiltredData() {
 console.log('the filter is ', this.creditVentesDataFilter);
 this.loadData(this.creditVentesDataFilter);
}

activateFilterClient()
{
  this.showFilterClient = !this.showFilterClient;
}




}



