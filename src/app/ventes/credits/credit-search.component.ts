import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,
    
      ViewChild } from '@angular/core';
    // import { ArticleService } from './article.service';
    import {Credit} from './credit.model';
    import { MODES_GENERATION_TRANCHES} from './credit.model';
    import {CreditService} from './credit.service';
    import {FormControl} from '@angular/forms';
    import { MatExpansionPanel } from '@angular/material/expansion';
    import {map, startWith} from 'rxjs/operators';
    import {Observable} from 'rxjs';
    import { Router, ActivatedRoute, ParamMap } from '@angular/router';
    import {ActionData} from './action-data.interface';
    import { ClientService } from '../../ventes/clients/client.service';
    import { Category} from '../../common/category.interface';
    import { CreditFilter } from '../../ventes/credits/credit.filter';
    import { Client } from '../../ventes/clients/client.model';
    
    @Component({
        selector: 'app-credit-search',
        templateUrl: './credit-search.component.html',
        // styleUrls: ['./players.component.css']
      })
 export class CreditSearchComponent implements OnInit {

    @Input()
    private actionData: ActionData;

    @Input()
    private listClients: Client[];
    private categorySelected = 'N/A';
    private homeMessage = 'Here I will display the list of : last credits updated' +
    'last credits created ; last credits ...';
    private categories: Category[] = [{value: 'UNKNOWN', viewValue : 'N/A'},
        {value: 'PERSONNE_MORALE', viewValue: 'Entreprise'}, {value: 'PERSONNE_PHYSIQUE', viewValue : 'Client'}
      
      ];
      // private panelPersonnePhysique: MatExpansionPanel ;
      // private panelPersonneMorale: MatExpansionPanel;
      panelOpenState = false;
      private creditFilter: CreditFilter;
      // private creditFilterValues: CreditFilter;
      
        constructor( private route: ActivatedRoute,
            private router: Router, private creditService: CreditService, private clientService: ClientService) {
         
              
         
            }
          ngOnInit() {
            this.creditFilter = new CreditFilter(this.listClients);
           // this.clientService.getClients().subscribe((clients) => {
            //  this.creditFilter = new CreditFilter(clients);
         // });
          }
          show() {
            console.log('the catge selected is :');
            console.log(this.categorySelected);
            
           /* if ( this.panelPersonnePhysique !== undefined) {
            this.panelPersonnePhysique.close();
            }
            if ( this.panelPersonneMorale !== undefined) {
           this.panelPersonneMorale.close();
            }*/
          }

        }
    

