
import { BaseEntity } from '../../common/base-entity.model' ;
type Etat = 'Initial' | 'Valide' | 'Annule' | 'Reglee' | 'PartiellementRegle' | 'NonReglee';

export class TrancheCredit extends BaseEntity {
    constructor(public code ?: string,
        public codeCredit?: string,
         public datePayementProgrammeObject?: Date,
         public datePayementObject?: Date,
         public datePayement?: string,
         public datePayementProgramme?: string,
     public montantProgramme?: number,
     public montantPaye?: number,
     public etat?: Etat,
    ) {
         super();
        this.etat =  'Initial';
    }
}
