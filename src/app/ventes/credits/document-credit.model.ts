
import { BaseEntity } from '../../common/base-entity.model' ;

type TypeDocument =   'Cheque' | 'Kimbiela' |'FactureVentes' | 'BonLivraison'
 | 'ReçuPayement' | 'Autre' ;

export class DocumentCredit extends BaseEntity {
    constructor(
        public code?: string,
         public codeCredit?: string,
         public numeroDocument?: string,
        public dateReceptionObject?: Date,
        public dateReception?: string,
        public typeDocument?: TypeDocument,
        public fileExtension?: string,
        public imageFace1?: any,
        public imageFace2?: any,
     ) {
         super();
        
       }
  
}
