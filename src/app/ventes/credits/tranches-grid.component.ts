
import { Component, OnInit, Inject, ViewChild, Input, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {CreditService} from './credit.service';
import {Credit } from './credit.model';
import {TrancheCredit} from './tranche-credit.model';
import { TrancheCreditElement } from './tranche-credit.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DialogAddTrancheCreditComponent } from './popups/dialog-add-tranche-credit.component';
import { DialogEditTrancheCreditComponent } from './popups/dialog-edit-tranche-credit.component';

@Component({
    selector: 'app-tranches-grid',
    templateUrl: 'tranches-grid.component.html',
  })
  export class TranchesCreditsGridComponent implements OnInit, OnChanges {

     @Input()
     private credit : Credit;

     private listTranchesCredits: TrancheCredit[] = [];
     private  dataTranchesCreditsSource: MatTableDataSource<TrancheCreditElement>;
     private selection = new SelectionModel<TrancheCreditElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private creditService: CreditService,
     private matSnackBar: MatSnackBar,
    public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.creditService.getTranchesCreditsByCodeCredit(this.credit.code).subscribe((listTranchesCredits) => {
        this.listTranchesCredits = listTranchesCredits;
console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }

  ngOnChanges()
  {
    this.creditService.getTranchesCreditsByCodeCredit(this.credit.code).subscribe((listTranchesCredits) => {
      this.listTranchesCredits = listTranchesCredits;
console.log('recieving data from backend', this.listTranchesCredits  );
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });

  }
  checkOneActionInvoked() {
    const listSelectedTranchesCredits: TrancheCredit[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((trancheCreditElement) => {
    return trancheCreditElement.code;
  });
  this.listTranchesCredits.forEach(trancheCreditElement => {
    if (codes.findIndex(code => code === trancheCreditElement.code) > -1) {
      listSelectedTranchesCredits.push(trancheCreditElement);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listTranchesCredits !== undefined) {

      this.listTranchesCredits.forEach(trancheCredit => {
             listElements.push( {code: trancheCredit.code ,
            datePayementProgramme: trancheCredit.datePayementProgramme,
          montantPaye: trancheCredit.montantPaye,
          etat: trancheCredit.etat,
          description: trancheCredit.description
        
        } );
      });
    }
      this.dataTranchesCreditsSource = new MatTableDataSource<TrancheCreditElement>(listElements);
      this.dataTranchesCreditsSource.sort = this.sort;
      this.dataTranchesCreditsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'datePayementProgramme' , displayTitle: 'Date de Payement Prévue'},
     {name: 'montantPaye' , displayTitle: 'Montant Prévu'},
     {name: 'etat' , displayTitle: 'Etat'},
      {name: 'description' , displayTitle: 'description'}
    
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataTranchesCreditsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataTranchesCreditsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataTranchesCreditsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataTranchesCreditsSource.paginator) {
      this.dataTranchesCreditsSource.paginator.firstPage();
    }
  }
  showAddTrancheDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeCredit : this.credit.code
   };
 
   const dialogRef = this.dialog.open(DialogAddTrancheCreditComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(trAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditTrancheDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeTrancheCredit : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditTrancheCreditComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(trAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });

    } else {
      this.openSnackBar('Aucune Tranche Séléctionnée!');
    }
    }
    supprimerTranches()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(tranche, index) {
            console.log('tranche to delete is ', tranche);
              this.creditService.deleteTrancheCredit(tranche.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( this.selection.selected.length + ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucune Tranche Séléctionnée!');
        }
    }
    refresh() {
      this.creditService.getTranchesCreditsByCodeCredit(this.credit.code).subscribe((listTranchesCredits) => {
        this.listTranchesCredits = listTranchesCredits;
console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
