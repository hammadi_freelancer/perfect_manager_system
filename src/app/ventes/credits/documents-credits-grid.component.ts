
import { Component, OnInit, Inject,Input, ViewChild, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {CreditService} from './credit.service';
import {Credit} from './credit.model';

import {DocumentCredit} from './document-credit.model';
import { DocumentCreditElement } from './document-credit.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogAddDocumentCreditComponent } from './popups/dialog-add-document-credit.component';
import { DialogEditDocumentCreditComponent } from './popups/dialog-edit-document-credit.component';
@Component({
    selector: 'app-documents-credits-grid',
    templateUrl: 'documents-credits-grid.component.html',
  })
  export class DocumentsCreditsGridComponent implements OnInit, OnChanges {
     private listDocumentsCredits: DocumentCredit[] = [];
     private  dataDocumentsCreditsSource: MatTableDataSource<DocumentCreditElement>;
     private selection = new SelectionModel<DocumentCreditElement>(true, []);
    
     @Input()
     private credit : Credit;

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private creditService: CreditService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.creditService.getDocumentsCreditsByCodeCredit(this.credit.code).subscribe((listDocumentsCredits) => {
        this.listDocumentsCredits = listDocumentsCredits;
  console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
  }
  ngOnChanges()
  {
    this.specifyListColumnsToBeDisplayed();
    
    this.creditService.getDocumentsCreditsByCodeCredit(this.credit.code).subscribe((listDocumentsCredits) => {
      this.listDocumentsCredits = listDocumentsCredits;
console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsCredits  );
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }

  checkOneActionInvoked() {
    const listSelectedDocumentsCredits: DocumentCredit[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentCreditElement) => {
    return documentCreditElement.code;
  });
  this.listDocumentsCredits.forEach(documentCredit => {
    if (codes.findIndex(code => code === documentCredit.code) > -1) {
      listSelectedDocumentsCredits.push(documentCredit);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsCredits !== undefined) {

      this.listDocumentsCredits.forEach(docCredit => {
             listElements.push( {code: docCredit.code ,
          dateReception: docCredit.dateReception,
          numeroDocument: docCredit.numeroDocument,
          typeDocument: docCredit.typeDocument,
          description: docCredit.description
        } );
      });
    }
      this.dataDocumentsCreditsSource = new MatTableDataSource<DocumentCreditElement>(listElements);
      this.dataDocumentsCreditsSource.sort = this.sort;
      this.dataDocumentsCreditsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     console.log('calling specifyListColumnsToBeDisplayed documents grid');
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
    
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
      console.log(this.columnsNames);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsCreditsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsCreditsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsCreditsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsCreditsSource.paginator) {
      this.dataDocumentsCreditsSource.paginator.firstPage();
    }
  }
  showAddDocumentDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeCredit : this.credit.code
   };
 
   const dialogRef = this.dialog.open(DialogAddDocumentCreditComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(docAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

});
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditDocumentDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeDocumentCredit : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditDocumentCreditComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(docAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
    });
    } else {
      this.openSnackBar('Aucun Document Séléctionnée!');
    }
    }
    supprimerDocuments()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(doc, index) {
              this.creditService.deleteDocumentCredit(doc.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( this.selection.selected.length + ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Document Séléctionnée!');
        }
    }
    refresh() {
      this.creditService.getDocumentsCreditsByCodeCredit(this.credit.code).subscribe((listDocsCredits) => {
        this.listDocumentsCredits = listDocsCredits;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
