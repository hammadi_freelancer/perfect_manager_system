
import { BaseEntity } from '../../common/base-entity.model' ;

type Etat = 'Initial' | 'Valide' | 'Annule' 
type ModePayement = 'Espece' | 'Cheque' | 'Virement'

export class PayementCredit extends BaseEntity {
    constructor(
        public code?: string,
         public codeCredit?: string,
         public montantPaye?: number,
        public dateOperationObject?: Date,
        public modePayement?: ModePayement,
        public numeroCheque?: string,
        public dateChequeObject?: Date,
        public compte?: string,
        public compteAdversaire?: string,
        public banque?: string,
        public banqueAdversaire?: string,
        public dateCheque?: string,
        public dateOperation?: string,
        
     ) {
         super();
        
       }
  
}
