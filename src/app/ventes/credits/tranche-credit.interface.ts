


export interface TrancheCreditElement {
         code: string ;
         montantProgramme: number;
         montantPaye: number;
          datePayementProgramme?: string;
          datePayement?: string;
          etat?: string;
          description?: string ;
        }
