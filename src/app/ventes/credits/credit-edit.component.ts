import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,
  AfterViewInit , AfterContentInit,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {Credit} from './credit.model';
import {TrancheCredit} from './tranche-credit.model';
import { MODES_GENERATION_TRANCHES} from './credit.model';

import {CreditService} from './credit.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import {ActionData} from './action-data.interface';
// import { CommunicationService} from './communication.service';
import { MatDatepickerInput } from '@angular/material/datepicker';
import { MatSnackBar } from '@angular/material';
import { CreditFilter } from './credit.filter';
import {Client} from '../clients/client.model';
import { MatDatepicker } from '@angular/material/datepicker';
// import { DialogAddPayementCreditComponent } from './dialog-add-payement-credit.component';
// import { DialogListPayementsCreditsComponent} from './dialog-list-payements-credits.component ';
// import { DialogListAjournementsCreditsComponent} from './dialog-list-ajournements-credits.component';
// import { DialogListTranchesCreditsComponent } from './dialog-list-tranches-credits.component';
// import { DialogListDocumentsCreditsComponent } from './dialog-list-documents-credits.component';
// import { DialogAddTrancheCreditComponent } from './dialog-add-tranche-credit.component';
// import { DialogAddAjournementCreditComponent } from './dialog-add-ajournement-credit.component';
// import { DialogAddDocumentCreditComponent } from './dialog-add-document-credit.component';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-credit-edit',
  templateUrl: './credit-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class CreditEditComponent implements OnInit  {

  private credit: Credit =  Credit.constructDefaultInstance();
  private totalPayements = 0;
  private totalReste  = 0;
@Input()
private listCredits: any = [] ;

@Output()
creditRecuperated: EventEmitter<Credit> = new EventEmitter<Credit>();

@Input()
private actionData: ActionData;
private creditFilter: CreditFilter;
private listClients: Client[];
private formControlDate = new FormControl();
@ViewChild('DateOper') dateOper: ElementRef;
private updateEnCours = false;
private tabsDisabled = true;
private etatsCredits: any[] = [
  {value: 'Annule', viewValue: 'Annulé'},
  {value: 'Initial', viewValue: 'Initial'},
  {value: 'Valide', viewValue: 'Validé'}
];
private periodesCreditsVentes: any[] = [
  {value: 'Semaine', viewValue: 'Semaine'},
  {value: 'Mois', viewValue: 'Mois'},
  {value: 'Trimestre', viewValue: 'Trimestre'},
  {value: 'Simestre', viewValue: 'Simestre'},
  {value: 'Annee', viewValue: 'Annéé'}
  
  
];
  constructor( private route: ActivatedRoute,
    private router: Router, private creditService: CreditService, private matSnackBar: MatSnackBar,
    public dialog: MatDialog
   ) {
  }
  ngOnInit() {
    this.listClients = this.route.snapshot.data.clients;
    this.creditFilter = new CreditFilter(this.listClients);
    const codeCredit = this.route.snapshot.paramMap.get('codeCredit');
    this.creditService.getCredit(codeCredit).subscribe((credit) => {
             this.credit = credit;
             if (this.credit.etat === 'Valide') {
                this.tabsDisabled = false;
                this.creditService.getTotalPayementsCreditsByCodeCredit(codeCredit).
                subscribe((totalPayements) => {
                this.totalPayements = totalPayements;
                console.log('Total Paye credit is ', this.totalPayements);
                this.totalReste = +this.credit.montantAPayer - this.totalPayements ;
                });
              }
             this.creditRecuperated.emit(this.credit);
             console.log('the date is ');
            //  console.log(this.credit.dateOperation);
            // this.dateOper = new FormControl(new Date());
             // console.log(this.formControlDate);
             // this.picker1._datepickerInput.value = new Date(this.credit.dateOperation);
             console.log(this.credit.dateOperationObject);
             if (this.credit.etat === 'Valide' || this.credit.etat === 'Annule')
              {
                this.etatsCredits.splice(1, 1);
              }
              if (this.credit.etat === 'Initial')
                {
                  this.etatsCredits.splice(0, 1);
                }

            // this.dateOper.nativeElement.value = this.credit.dateOperationObject; 
 
    });
  }
 
  showDate($event)
  {
    console.log($event);
  }
  loadAddCreditComponent() {
    this.router.navigate(['/pms/ventes/ajouterCredit']) ;
  }
  loadGridCredits() {
    this.router.navigate(['/pms/ventes/credits']) ;
  }
  supprimerCredit() {
      this.creditService.deleteCredit(this.credit.code).subscribe((response) => {

        this.openSnackBar( ' Crédit Supprimé');
        this.loadGridCredits() ;
            });
  }
updateCredit() {
  console.log('CLIENT IS ');
  console.log(this.credit.client.cin);
  let noClient = false;
  let noMontant = false;
  if (this.credit.etat === 'Valide' ) {
    console.log('valide');
    if ( this.credit.client.cin === null ||  this.credit.client.cin === undefined)  {
        noClient = true;
        
      }

  }
  if (this.credit.montantAPayer === 0 || this.credit.montantAPayer === undefined ) {
      noMontant = true;
    }
    if (noClient)
      {
        this.openSnackBar( ' Vous Venez de valider le crédit sans préciser les informations de client! ');
        this.credit.etat = 'Initial';
      } else if (noMontant)  {
          this.openSnackBar( 'Un Crédit Crée sans un Montant bien Précis !');
          
        } else {
     this.updateEnCours = true;          
  this.creditService.updateCredit(this.credit).subscribe((response) => {
    this.updateEnCours = false; 
    if (response.error_message ===  undefined) {
      if (this.credit.etat === 'Valide')
        {
          this.tabsDisabled = false;
          this.etatsCredits = [
            {value: 'Annule', viewValue: 'Annulé'},
            {value: 'Valide', viewValue: 'Validé'}
  
          ];
        }
        if (this.credit.etat === 'Annule')
          {
            this.tabsDisabled = true;
         
          }
      console.log('the response of update is ', response );
      this.openSnackBar( ' Crédit mis à jour ');

      //  this.save.emit(response);
      // this.client = new Client();
      // this.modeUpdate = false;
    } else {
      this.openSnackBar( ' Erreurs:Opération Echouée ');
     // show error message
    }
});
        }
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

 
history()
{

}
fillDataNomPrenom() {
  const listFiltred  = this.listClients.filter((client) => (client.cin === this.credit.client.cin));
  this.credit.client.nom = listFiltred[0].nom;
  this.credit.client.prenom = listFiltred[0].prenom;
}
fillDataMatriculeRaison()
{
  const listFiltred  = this.listClients.filter((client) => (client.registreCommerce === this.credit.client.registreCommerce));
  this.credit.client.matriculeFiscale = listFiltred[0].matriculeFiscale;
  this.credit.client.raisonSociale = listFiltred[0].raisonSociale;
}
selectedTabChanged($event) {
  if ($event.index === 4) { // Composition tab was selected
   // this.loadListArticles();
   } else if ($event.index === 1) {
    //  this.loadListSchemasConfiguration() ;
   }
  }
}
