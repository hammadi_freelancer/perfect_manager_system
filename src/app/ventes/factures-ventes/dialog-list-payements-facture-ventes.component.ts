
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {FactureVenteService} from './facture-vente.service';
import {PayementFactureVentes} from './payement-facture-ventes.model';
import { PayementFactureVentesElement } from './payement-facture-ventes.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-payements-factures-ventes',
    templateUrl: 'dialog-list-payements-factures-ventes.component.html',
  })
  export class DialogListPayementsFactureVentesComponent implements OnInit {
     private listPayementFactureVentes: PayementFactureVentes[] = [];
     private  dataPayementFactureVentesSource: MatTableDataSource<PayementFactureVentesElement>;
     private selection = new SelectionModel<PayementFactureVentesElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private factureVenteService: FactureVenteService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      console.log('code fct  in dialog component is :', this.data.codeFactureVentes );
      this.factureVenteService.getPayementsFacturesVentesByCodeFacture(this.data.codeFactureVentes).
      subscribe(listPayementsFacturesVentes => {
        this.listPayementFactureVentes = listPayementsFacturesVentes;
console.log('recieving data from backend', this.listPayementFactureVentes  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedPayementsFacturesVentes : PayementFactureVentes[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((payementFactureVentes) => {
    return payementFactureVentes.code;
  });
  this.listPayementFactureVentes.forEach(payementFactureVente => {
    if (codes.findIndex(code => code === payementFactureVente.code) > -1) {
      listSelectedPayementsFacturesVentes.push(payementFactureVente);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listPayementFactureVentes !== undefined) {

      this.listPayementFactureVentes.forEach(payementFactureVentes => {
             listElements.push( {code: payementFactureVentes.code ,
          dateOperation: payementFactureVentes.dateOperation,
          montantPaye: payementFactureVentes.montantPaye,
          modePayement: payementFactureVentes.modePayement,
          numeroCheque: payementFactureVentes.numeroCheque,
          dateCheque: payementFactureVentes.dateCheque,
          compte: payementFactureVentes.compte,
          compteAdversaire: payementFactureVentes.compteAdversaire,
          banque: payementFactureVentes.banque,
          banqueAdversaire: payementFactureVentes.banqueAdversaire,
          description: payementFactureVentes.description
        } );
      });
    }
      this.dataPayementFactureVentesSource = new MatTableDataSource<PayementFactureVentesElement>(listElements);
      this.dataPayementFactureVentesSource.sort = this.sort;
      this.dataPayementFactureVentesSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'}, 
     {name: 'montantPaye' , displayTitle: 'Montant Payé'},
     {name: 'modePayement' , displayTitle: 'Mode Payement'},
      {name: 'numeroCheque' , displayTitle: 'N°Chèque'},
     {name: 'dateCheque' , displayTitle: 'Date Chèque'}, 
     {name: 'compte' , displayTitle: 'Compte'},
     {name: 'compteAdversaire' , displayTitle: 'Compte Adv.'},
      {name: 'banque' , displayTitle: 'Banque'},
       { name : 'banqueAdversaire' , displayTitle : 'Banque Adv.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataPayementFactureVentesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataPayementFactureVentesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataPayementFactureVentesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataPayementFactureVentesSource.paginator) {
      this.dataPayementFactureVentesSource.paginator.firstPage();
    }
  }

}
