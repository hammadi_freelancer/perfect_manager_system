import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import {FactureVente} from './facture-vente.model' ;
import { DocumentFactureVentes } from './document-facture-ventes.model';
import {PayementFactureVentes} from './payement-facture-ventes.model' ;
import {AjournementFactureVente} from './ajournement-facture-ventes.model' ;
import {Credit} from '../credits/credit.model' ;
import {LigneFactureVente} from './ligne-facture-vente.model' ;

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_FACTURES_VENTES = 'http://localhost:8082/ventes/facturesVentes/getFacturesVentes';
const URL_GET_FACTURE_VENTE = 'http://localhost:8082/ventes/facturesVentes/getFactureVente';
const  URL_ADD_FACTURE_VENTE  = 'http://localhost:8082/ventes/facturesVentes/addFactureVente';
const  URL_UPDATE_FACTURE_VENTE  = 'http://localhost:8082/ventes/facturesVentes/updateFactureVente';
const  URL_DELETE_FACTURE_VENTE  = 'http://localhost:8082/ventes/facturesVentes/deleteFactureVente';
const  URL_GET_TOTAL_FACTURE_VENTES = 'http://localhost:8082/ventes/facturesVentes/getTotalFacturesVentes';

const  URL_ADD_PAYEMENT_FACTURE_VENTES = 'http://localhost:8082/finance/payementsFacturesVentes/addPayementFactureVentes';
const  URL_GET_LIST_PAYEMENT_FACTURE_VENTES  = 'http://localhost:8082/finance/payementsFacturesVentes/getPayementsFacturesVentes';
const  URL_DELETE_PAYEMENT_FACTURE_VENTES  = 'http://localhost:8082/finance/payementsFacturesVentes/deletePayementFactureVentes';
const  URL_UPDATE_PAYEMENT_FACTURE_VENTES  = 'http://localhost:8082/finance/payementsFacturesVentes/updatePayementFactureVentes';
const URL_GET_PAYEMENT_FACTURE_VENTES  = 'http://localhost:8082/finance/payementsFacturesVentes/getPayementFactureVentes';
const URL_GET_LIST_PAYEMENT_FACTURE_VENTES_BY_CODE_FACTURE =
'http://localhost:8082/finance/payementsFacturesVentes/getPayementsFacturesVentesByCodeFactureVentes';


const  URL_ADD_DOCUMENT_FACTURE_VENTES = 'http://localhost:8082/ventes/documentsFacturesVentes/addDocumentFactureVentes';
const  URL_GET_LIST_DOCUMENTS_FACTURE_VENTES = 'http://localhost:8082/ventes/documentsFacturesVentes/getDocumentsFactureVentes';
const  URL_DELETE_DOCUMENT_FACTURE_VENTES = 'http://localhost:8082/ventes/documentsFacturesVentes/deleteDocumentFactureVentes';
const  URL_UPDATE_DOCUMENT_FACTURE_VENTES = 'http://localhost:8082/ventes/documentsFacturesVentes/updateDocumentFactureVentes';
const URL_GET_DOCUMENT_FACTURE_VENTES = 'http://localhost:8082/ventes/documentsFacturesVentes/getDocumentFactureVentes';
const URL_GET_LIST_DOCUMENTS_FACTURE_VENTES_BY_CODE_FACTURE_VENTES =
 'http://localhost:8082/ventes/documentsFacturesVentes/getDocumentsFactureVentesByCodeFactureVentes';

 const  URL_ADD_AJOURNEMENT_FACTURE_VENTES = 'http://localhost:8082/ventes/ajournementsFacturesVentes/addAjournementFactureVentes';
 const  URL_GET_LIST_AJOURNEMENT_FACTURE_VENTES = 'http://localhost:8082/ventes/ajournementsFacturesVentes/getAjournementsFactureVentes';
 const  URL_DELETE_AJOURNEMENT_FACTURE_VENTES = 'http://localhost:8082/ventes/ajournementsFacturesVentes/deleteAjournementFactureVentes';
 const  URL_UPDATE_AJOURNEMENT_FACTURE_VENTES = 'http://localhost:8082/ventes/ajournementsFacturesVentes/updateAjournementFactureVentes';
 const URL_GET_AJOURNEMENT_FACTURE_VENTES = 'http://localhost:8082/ventes/ajournementsFacturesVentes/getAjournementFactureVentes';
 const URL_GET_LIST_AJOURNEMENT_FACTURE_VENTES_BY_CODE_FACTURE_VENTES =
  'http://localhost:8082/ventes/ajournementsFacturesVentes/getAjournementsFacturesVentesByCodeFactureVentes';



  const  URL_ADD_LIGNE_FACTURE_VENTES = 'http://localhost:8082/ventes/lignesFacturesVentes/addLigneFactureVentes';
  const  URL_GET_LIST_LIGNES_FACTURE_VENTES = 'http://localhost:8082/ventes/lignesFacturesVentes/getLignesFactureVentes';
  const  URL_DELETE_LIGNE_FACTURE_VENTES = 'http://localhost:8082/ventes/lignesFacturesVentes/deleteLigneFactureVentes';
  const  URL_UPDATE_LIGNE_FACTURE_VENTES = 'http://localhost:8082/ventes/lignesFacturesVentes/updateLigneFactureVentes';
  const URL_GET_LIGNE_FACTURE_VENTES = 'http://localhost:8082/ventes/lignesFacturesVentes/getLigneFactureVentes';
  const URL_GET_LIST_LIGNES_FACTURE_VENTES_BY_CODE_FACTURE_VENTES =
   'http://localhost:8082/ventes/lignesFacturesVentes/getLignesFactureVentesByCodeFactureVente';

@Injectable({
  providedIn: 'root'
})
export class FactureVenteService {

  constructor(private httpClient: HttpClient) {

   }
   getTotalFacturesVentes(filter): Observable<any> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('filter', filterStr);
    return this.httpClient
    .get<any>(URL_GET_TOTAL_FACTURE_VENTES, {params});
   }


   getFacturesVentes(pageNumber, nbElementsPerPage, filter): Observable<FactureVente[]> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('pageNumber', pageNumber)
    .set('nbElementsPerPage', nbElementsPerPage)
    .set('filter', filterStr);
    return this.httpClient
    .get<FactureVente[]>(URL_GET_LIST_FACTURES_VENTES, {params});


   }
   addFactureVente(factureVente: FactureVente): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_FACTURE_VENTE, factureVente);
  }
  updateFactureVente(factureVente: FactureVente): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_FACTURE_VENTE, factureVente);
  }
  deleteFactureVente(codeFactureVente): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_FACTURE_VENTE + '/' + codeFactureVente);
  }
  getFactureVente(codeFactureVente): Observable<FactureVente> {
    return this.httpClient.get<FactureVente>(URL_GET_FACTURE_VENTE + '/' + codeFactureVente);
  }


  addPayementFactureVentes(payementFactureVentes: PayementFactureVentes): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_PAYEMENT_FACTURE_VENTES, payementFactureVentes);
    
  }
  getPayementsFacturesVentes(): Observable<PayementFactureVentes[]> {
    return this.httpClient
    .get<PayementFactureVentes[]>(URL_GET_LIST_PAYEMENT_FACTURE_VENTES);
   }
   getPayementFactureVentes(codePayementFactureVentes): Observable<PayementFactureVentes> {
    return this.httpClient.get<PayementFactureVentes>(URL_GET_PAYEMENT_FACTURE_VENTES + '/' + codePayementFactureVentes);
  }
  deletePayementFactureVentes(codePayementFactureVentes): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_PAYEMENT_FACTURE_VENTES + '/' + codePayementFactureVentes);
  }
  getPayementsFacturesVentesByCodeFacture(codeFactureVentes): Observable<PayementFactureVentes[]> {
    return this.httpClient.get<PayementFactureVentes[]>(URL_GET_LIST_PAYEMENT_FACTURE_VENTES_BY_CODE_FACTURE + '/' + codeFactureVentes);
  }

  updatePayementFactureVentes(payFactureVente: PayementFactureVentes): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_PAYEMENT_FACTURE_VENTES, payFactureVente);
  }

  addDocumentFactureVentes(docFactureVentes: DocumentFactureVentes): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_DOCUMENT_FACTURE_VENTES, docFactureVentes);
    
  }
  getDocumentsFacturesVentes(): Observable<DocumentFactureVentes[]> {
    return this.httpClient
    .get<DocumentFactureVentes[]>(URL_GET_LIST_DOCUMENTS_FACTURE_VENTES);
   }
   getDocumentFactureVentes(codeDocumentFactureVentes): Observable<DocumentFactureVentes> {
    return this.httpClient.get<DocumentFactureVentes>(URL_GET_DOCUMENT_FACTURE_VENTES + '/' + codeDocumentFactureVentes);
  }
  deleteDocumentFactureVentes(codeDocumentFactureVentes): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_DOCUMENT_FACTURE_VENTES + '/' + codeDocumentFactureVentes);
  }
  updateDocumentFactureVentes(docFactureVente: DocumentFactureVentes): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_DOCUMENT_FACTURE_VENTES, docFactureVente);
  }


  getDocumentsFacturesVentesByCodeFacture(codeFactureVentes): Observable<DocumentFactureVentes[]> {
    return this.httpClient.get<DocumentFactureVentes[]>( URL_GET_LIST_DOCUMENTS_FACTURE_VENTES_BY_CODE_FACTURE_VENTES +
       '/' + codeFactureVentes);
  }

  addAjournementFactureVentes(ajournFactureVente: AjournementFactureVente): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_AJOURNEMENT_FACTURE_VENTES, ajournFactureVente);
    
  }
  updateAjournementFactureVentes(ajournFactureVente: AjournementFactureVente): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_AJOURNEMENT_FACTURE_VENTES, ajournFactureVente);
  }
  getAjournementsFacturesVentesByCodeFacture(codeFactureVentes): Observable<AjournementFactureVente[]> {
    return this.httpClient.get<AjournementFactureVente[]>( URL_GET_LIST_AJOURNEMENT_FACTURE_VENTES_BY_CODE_FACTURE_VENTES +
       '/' + codeFactureVentes);
  }
  getAjournementsFacturesVentes(): Observable<AjournementFactureVente[]> {
    return this.httpClient
    .get<AjournementFactureVente[]>(URL_GET_LIST_AJOURNEMENT_FACTURE_VENTES);
   }
   getAjournementFactureVentes(codeAjournementFactureVentes): Observable<AjournementFactureVente> {
    return this.httpClient.get<AjournementFactureVente>(URL_GET_AJOURNEMENT_FACTURE_VENTES + '/' + codeAjournementFactureVentes);
  }
  deleteAjournementFactureVentes(codeAjournementFactureVentes): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_AJOURNEMENT_FACTURE_VENTES + '/' + codeAjournementFactureVentes);
  }


  addLigneFactureVentes(ligneFactureVente: LigneFactureVente): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_LIGNE_FACTURE_VENTES, ligneFactureVente);
    
  }
  updateLigneFactureVentes(ligneFactureVente: LigneFactureVente): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_LIGNE_FACTURE_VENTES, ligneFactureVente);
  }
  getLignesFacturesVentesByCodeFacture(codeLigneFactureVentes): Observable<LigneFactureVente[]> {
    return this.httpClient.get<LigneFactureVente[]>( URL_GET_LIST_LIGNES_FACTURE_VENTES_BY_CODE_FACTURE_VENTES +
       '/' + codeLigneFactureVentes);
  }
  getLignesFacturesVentes(): Observable<LigneFactureVente[]> {
    return this.httpClient
    .get<LigneFactureVente[]>(URL_GET_LIST_LIGNES_FACTURE_VENTES);
   }
   getLigneFactureVentes(codeLigneFactureVentes): Observable<LigneFactureVente> {
    return this.httpClient.get<LigneFactureVente>(URL_GET_LIST_LIGNES_FACTURE_VENTES + '/' + codeLigneFactureVentes);
  }
  deleteLigneFactureVentes(codeLigneFactureVentes): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_LIGNE_FACTURE_VENTES + '/' + codeLigneFactureVentes);
  }
}
