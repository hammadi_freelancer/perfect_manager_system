import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {FactureVente} from './facture-vente.model';
import { ParametresVentes } from '../parametres-ventes.model';

import {FactureVenteService} from './facture-vente.service';
import {VentesService} from '../ventes.service';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
// import {ActionData} from './action-data.interface';
import { ClientService } from '../clients/client.service';
import { FactureVenteFilter } from './facture-vente.filter';
import { ArticleFilter } from './article.filter';

import {Client} from '../clients/client.model';
import {Magasin} from '../../stocks/magasins/magasin.model';
import {UnMesure} from '../../stocks/un-mesures/un-mesure.model';
import {MagasinFilter} from '../../stocks/magasins/magasin.filter';
import {UnMesureFilter} from '../../stocks/un-mesures/un-mesures.filter';
import { ClientFilter } from '../clients/client.filter';
import {LigneFactureVente} from '../factures-ventes/ligne-facture-vente.model';
import {LigneFactureVenteElement} from '../factures-ventes/ligne-facture-vente.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';

import {MatSnackBar} from '@angular/material';
import { MatExpansionPanel } from '@angular/material';
import {ArticleService} from '../../stocks/articles/article.service';
import {Article} from '../../stocks/articles/article.model';
import {ModePayementLibelle} from './mode-payement-libelle.interface';
import {SelectionModel} from '@angular/cdk/collections';

type LibelleModePayement = 'Credit'| 'Credit_Espece' | 'Cheque' | 'Espece';

@Component({
  selector: 'app-facture-vente-new',
  templateUrl: './facture-vente-new.component.html',
  // styleUrls: ['./players.component.css']
})
export class FactureVenteNewComponent implements OnInit {

 private factureVente: FactureVente =  new FactureVente();
 private parametresVentes = new ParametresVentes();
 private  dataLignesFactureVentesSource: MatTableDataSource<LigneFactureVenteElement>;
 
@Input()
private listFacturesVentes: any = [] ;

private saveEnCours = false;
private managedLigneFactureVentes = new LigneFactureVente();

private selection = new SelectionModel<LigneFactureVenteElement>(true, []);
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
 private magasinFilter: MagasinFilter;
 private unMesureFilter: UnMesureFilter;
 private clientFilter: ClientFilter;
 
/*@Output()
save: EventEmitter<FactureVente> = new EventEmitter<FactureVente>();*/

@ViewChild('firstMatExpansionPanel')
 private firstMatExpansionPanel : MatExpansionPanel;

 @ViewChild('manageLigneMatExpansionPanel')
 private manageLigneMatExpansionPanel : MatExpansionPanel;

 @ViewChild('listLignesMatExpansionPanel')
 private listLignesMatExpansionPanel : MatExpansionPanel;

private factureVenteFilter: FactureVenteFilter;
private articleFilter: ArticleFilter;
private listClients: Client[];
private listArticles: Article[];
private listMagasins: Magasin[];


private payementModes: any[] = [
  {value: 'Comptant', viewValue: 'Comptant'},
  {value: 'Cheque', viewValue: 'Chèque'},
  {value: 'Credit', viewValue: 'Crédit'},
  {value: 'Avance', viewValue: 'Avance'},
  {value: 'Comptant_Cheque', viewValue: 'Comptant/Chèque'},
  {value: 'Comptant_Avance', viewValue: 'Comptant/Avance'},
  {value: 'Credit_Comptant', viewValue: 'Crédit/Comptant'},
  {value: 'Credit_Cheque', viewValue: 'Crédit/Chèque'},
  {value: 'Credit_Avance', viewValue: 'Crédit/Avance'},
  {value: 'Donnation', viewValue: 'Amicalité'}
];
private etatsFactureVentes: any[] = [
  {value: 'Initial', viewValue: 'Initial'}
 // {value: 'Valide', viewValue: 'Validé'}
];

  constructor( private route: ActivatedRoute,
    private router: Router, private factureVenteService: FactureVenteService, private clientService: ClientService,
    private articleService: ArticleService, private ventesService: VentesService,
   private matSnackBar: MatSnackBar, private elRef: ElementRef) {
  }


  // myControl = new FormControl();
  // options: string[] = ['One', 'Two', 'Three'];
  // filteredOptions: Observable<string[]>;

  ngOnInit() {

   this.listClients = this.route.snapshot.data.clients;
   this.listArticles = this.route.snapshot.data.articles;
   this.listMagasins = this.route.snapshot.data.magasins;
   
  // this.listCodesArticles  = this.listArticles.map((article) => article.code);
   this.articleFilter = new ArticleFilter(this.route.snapshot.data.articles,
  [  ], []);

  this.clientFilter  = new ClientFilter(this.listClients );
   this.magasinFilter  = new MagasinFilter( this.route.snapshot.data.magasins);
   this.unMesureFilter  = new UnMesureFilter( this.route.snapshot.data.unMesures);

   this.firstMatExpansionPanel.open();
   this.manageLigneMatExpansionPanel.opened.subscribe(openedPanel => {
    this.articleFilter = new ArticleFilter(this.route.snapshot.data.articles,
      this.route.snapshot.data.magasins, this.route.snapshot.data.unMesures);
     });
   this.specifyListColumnsToBeDisplayed();
   //this.
   //();
  // this.initializeListLignesFactures();
  /*this.lignesFactures.forEach(function(ligne, index) {
                 ligne.code =  String(index + 1);
  });*/
  this.factureVente.etat = 'Initial';
  this.ventesService.getParametresVentes().subscribe((response) => {
    if (response !==  undefined) {
      // this.save.emit(response);
      this.parametresVentes = response;
      
    } else {
     // show error message
    }
});
  }
 
  constructClassName(i)  {
    return  'designationDivs_' + i;
   //  this.elRef.nativeElement.querySelector('.designationDivs_' + i )  ;
  }
  calculMontants()
  {
    
  }

  recalculMontants()
  {
   /* if ( this.pagesLignesFacures[this.currentPage - 1] === undefined) {
      this.pagesLignesFacures[this.currentPage - 1]  =  this.lignesFactures;
    }*/
      
    this.factureVente.montantHT = Number(0);
    this.factureVente.montantTVA = Number(0);
    this.factureVente.montantTTC = Number(0);
    this.factureVente.montantRemise = Number(0);
    this.factureVente.montantAPayer = Number(0);
    this.factureVente.montantAPayerApresRemise = Number(0);
    this.factureVente.listLignesFactures.forEach(function(ligne , index) {
        this.factureVente.montantHT = +this.factureVente.montantHT + ligne.montantHT ;
        this.factureVente.montantTVA = +this.factureVente.montantTVA + ligne.montantTVA ;
        this.factureVente.montantTTC = +this.factureVente.montantTTC  + ligne.montantTTC ;
        this.factureVente.montantRemise = +this.factureVente.montantRemise  + ligne.montantRemise ;
        this.factureVente.montantAPayer = this.factureVente.montantTTC ;
        this.factureVente.montantAPayerApresRemise = +this.factureVente.montantAPayer - this.factureVente.montantRemise;
        // this.editLignesMode[i] = true;
       // this.readOnlyLignes[i] = true;
  }, this);

  }

    calculMontantAPayer()
    {
    this.factureVente.montantAPayer = +this.factureVente.timbreFiscale + this.factureVente.montantTTC;
    
    return this.factureVente.montantAPayer ;
    }
    calculMontantApayerApresRemise()
    {
      
    this.factureVente.montantAPayerApresRemise = +this.factureVente.montantAPayer - this.factureVente.montantRemise;
      return this.factureVente.montantAPayerApresRemise;
    }
  claculMontantRemise() {
    const prixVente   =  +(+this.managedLigneFactureVentes.montantTTC / +this.managedLigneFactureVentes.quantite)
    ;
    if (!isNaN(prixVente)) {
    console.log(prixVente);
  this.managedLigneFactureVentes.montantRemise = +this.managedLigneFactureVentes.remise  * ( +prixVente / 100) *
   this.managedLigneFactureVentes.quantite;
  // console.log(this.lignesFactures[i].montantRemise);
    }
  return this.managedLigneFactureVentes.montantRemise;
}
calculMontantHT()
{
  this.managedLigneFactureVentes.montantHT = +this.managedLigneFactureVentes.prixUnitaire * this.managedLigneFactureVentes.quantite;
  return this.managedLigneFactureVentes.montantHT;
}
  calculMontantTTC()
  {
    this.managedLigneFactureVentes.montantTTC = +(+this.managedLigneFactureVentes.prixUnitaire * this.managedLigneFactureVentes.quantite)
    + this.managedLigneFactureVentes.montantTVA;
    
    return  this.managedLigneFactureVentes.montantTTC;
  }
  calculMontantTVA() {

    this.managedLigneFactureVentes.montantTVA =  (this.managedLigneFactureVentes.prixUnitaire / 100) *
    this.managedLigneFactureVentes.tva * this.managedLigneFactureVentes.quantite;
    this.managedLigneFactureVentes.montantTTC = +(+this.managedLigneFactureVentes.prixUnitaire * this.managedLigneFactureVentes.quantite)
    + this.managedLigneFactureVentes.montantTVA;
    // console.log('montant tva');
   // console.log(this.factureVente.montantTVA + this.listLignesFactureVentes[i].montantTVA);
   if(this.managedLigneFactureVentes.remise !== 0) {
    this.claculMontantRemise() ;
  }  
    return  this.managedLigneFactureVentes.montantTVA;
  }

  calculMontantBenefice() {
    
        this.managedLigneFactureVentes.montantBenefice =  (this.managedLigneFactureVentes.prixUnitaire / 100) *
        this.managedLigneFactureVentes.benefice * this.managedLigneFactureVentes.quantite;
        return  this.managedLigneFactureVentes.montantBenefice;
      }

 
 calculMontant4thTaxe() {
        
            this.managedLigneFactureVentes.montantOtherTaxe2 =  (this.managedLigneFactureVentes.prixUnitaire / 100) *
            this.managedLigneFactureVentes.otherTaxe2 * this.managedLigneFactureVentes.quantite;
            if(this.managedLigneFactureVentes.montantTTC === 0 ) {
              
       this.managedLigneFactureVentes.montantTTC = +(+this.managedLigneFactureVentes.prixUnitaire * this.managedLigneFactureVentes.quantite)
            + this.managedLigneFactureVentes.montantOtherTaxe2;
            } else {
              this.managedLigneFactureVentes.montantTTC = + this.managedLigneFactureVentes.montantTTC
              + this.managedLigneFactureVentes.montantOtherTaxe2;
            }

            if(this.managedLigneFactureVentes.remise !== 0) {
              this.claculMontantRemise() ;
            }  
            return  this.managedLigneFactureVentes.montantOtherTaxe2;
          }

 calculMontant3rdTaxe() {
            
                this.managedLigneFactureVentes.montantOtherTaxe1 =  (this.managedLigneFactureVentes.prixUnitaire / 100) *
                this.managedLigneFactureVentes.otherTaxe1 * this.managedLigneFactureVentes.quantite;
               if(this.managedLigneFactureVentes.montantTTC === 0 ) {
        this.managedLigneFactureVentes.montantTTC = +(+this.managedLigneFactureVentes.prixUnitaire
          * this.managedLigneFactureVentes.quantite)
                + this.managedLigneFactureVentes.montantOtherTaxe1;
                 } else {
                  this.managedLigneFactureVentes.montantTTC = + this.managedLigneFactureVentes.montantTTC
                          + this.managedLigneFactureVentes.montantOtherTaxe1;
                 }
                 if(this.managedLigneFactureVentes.remise !== 0) {
                  this.claculMontantRemise() ;
                }  
                return  this.managedLigneFactureVentes.montantOtherTaxe1;
              }

      calculMontantFodec() {
                
             this.managedLigneFactureVentes.montantFodec =  (this.managedLigneFactureVentes.prixUnitaire / 100) *
                    this.managedLigneFactureVentes.fodec * this.managedLigneFactureVentes.quantite;
                    if(this.managedLigneFactureVentes.montantTTC === 0 ) {
                      
                    this.managedLigneFactureVentes.montantTTC = +(+this.managedLigneFactureVentes.prixUnitaire *
                       this.managedLigneFactureVentes.quantite)
                    + this.managedLigneFactureVentes.montantFodec;
                    } else  {
                      this.managedLigneFactureVentes.montantTTC = + this.managedLigneFactureVentes.montantTTC
                      + this.managedLigneFactureVentes.montantFodec;
                    }
                    if(this.managedLigneFactureVentes.remise !== 0) {
                      this.claculMontantRemise() ;
                    }  
                    return  this.managedLigneFactureVentes.montantFodec;
                  }
  /*selectedArticleCode(i, $event) {
     console.log(i);
     console.log($event);
     this.listLignesFactureVentes[i].article.code = $event.option.value;
     this.listLignesFactureVentes[i].article.libelle  = this.listArticles.find((article) => (article.code === $event.option.value)).libelle;
    
     const  div  : HTMLDivElement = this.elRef.nativeElement.querySelector('.designationDivs_' + i )  ;
     div.innerText = this.listLignesFactureVentes[i].article.libelle;
     console.log(div);


  }*/

  saveFactureVente() {

    /*if (this.modeUpdate ) {
      this.updateCredit();
    } else {*/
      if (this.factureVente.etat === '' || this.factureVente.etat === undefined) {
        this.factureVente.etat = 'Initial';
      }
      //this.fillDataLignesFactures();
    console.log(this.factureVente);
    // this.saveEnCours = true;
    this.saveEnCours = true;
    
    this.factureVenteService.addFactureVente(this.factureVente).subscribe((response) => {
      this.saveEnCours = false;
      
        if (response.error_message ===  undefined) {
          console.log('undefined error message');
          // this.save.emit(response);
          this.factureVente = new  FactureVente();
          this.factureVente.listLignesFactures = [];
          this.openSnackBar(  'Facture de Vente Enregistrée');
        } else {
         // show error message
         this.openSnackBar(  'Erreurs:Opération Echouée');
         
        }
    });
  }
  fillDataNomPrenom()
  {
    const listFiltred  = this.listClients.filter((client) => (client.cin === this.factureVente.client.cin));
    this.factureVente.client.nom = listFiltred[0].nom;
    this.factureVente.client.prenom = listFiltred[0].prenom;
  }
  fillDataMatriculeRaison()
  {
    const listFiltred  = this.listClients.filter((client) => (client.registreCommerce === this.factureVente.client.registreCommerce));
    this.factureVente.client.matriculeFiscale = listFiltred[0].matriculeFiscale;
    this.factureVente.client.raisonSociale = listFiltred[0].raisonSociale;
  }
  vider() {
    this.factureVente =  new FactureVente();
  }
  openSnackBar(messageToDisplay) {
   this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
       verticalPosition: 'top', panelClass: 'snackBarClass'});
  }
loadGridFacturesVentes() {
  this.router.navigate(['/pms/ventes/facturesVentes']) ;
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.factureVente.listLignesFactures !== undefined) {
    this.factureVente.listLignesFactures.forEach(ligneFact => {
           listElements.push( {code: ligneFact.code ,
            numeroLigneFactureVentes: ligneFact.numeroLigneFactureVentes,
            article: ligneFact.article.code,
             magasin: ligneFact.magasin.code,
             unMesure: ligneFact.unMesure.code,
             quantite: ligneFact.quantite,
             prixUnitaire: ligneFact.prixUnitaire,
             tva: ligneFact.tva,
             montantTVA: ligneFact.montantTVA,
             remise: ligneFact.remise,
             montantRemise: ligneFact.montantRemise,
             montantHT: ligneFact.montantHT,
             montantTTC: ligneFact.montantTTC,
             etat: ligneFact.etat
      } );
    });
  }
    this.dataLignesFactureVentesSource= new MatTableDataSource<LigneFactureVenteElement>(listElements);
    this.dataLignesFactureVentesSource.sort = this.sort;
    this.dataLignesFactureVentesSource.paginator = this.paginator;
    
    
}
 specifyListColumnsToBeDisplayed() {
   // console.log('calling specifyListColumnsToBeDisplayed documents grid');
  this.columnsTitles = [];
  this.columnsNames = [];
  this.columnsTitlesAr  = [];
  
   this.columnsDefinitions = [{name: 'numeroLigneFactureVentes' , displayTitle: 'N°'},
   {name: 'article' , displayTitle: 'Article'},
   {name: 'magasin' , displayTitle: 'Magasin'},
   {name: 'unMesure' , displayTitle: 'Un Mesure'},
   {name: 'quantite' , displayTitle: 'Quantite'},
   {name: 'prixUnitaire' , displayTitle: 'Prix Unitaire'},
   {name: 'tva' , displayTitle: 'TVA'},
   {name: 'montantTVA' , displayTitle: 'Montant TVA'},
   {name: 'montantHT' , displayTitle: 'Montant HT'},
   { name : 'montantTTC' , displayTitle : 'Montant TTC'},
  
    ];

   this.columnsTitles = this.columnsDefinitions.map((colDef) => {
             return colDef.displayTitle;
   });
   
   this.columnsNames[0] = 'select';
   this.columnsDefinitions.map((colDef) => {
    this.columnsNames.push(colDef.name);
    console.log(this.columnsNames);
});
   this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
    return colDef.displayTitleAr;
});
}

applyFilter(filterValue: string) {
  this.dataLignesFactureVentesSource.filter = filterValue.trim().toLowerCase();
  if (this.dataLignesFactureVentesSource.paginator) {
    this.dataLignesFactureVentesSource.paginator.firstPage();
  }
}

checkLigneFactureVentes()
{

}
getIndexOfRow(row)
{

    for ( let i = 0; i < this.factureVente.listLignesFactures.length ; i++) {
      if (this.factureVente.listLignesFactures[i].numeroLigneFactureVentes === row.numeroLigneFactureVentes)
       {
             return i;
        }
    }
    return -1;
}
editLigneFactureVentes(row)
{
  this.managedLigneFactureVentes.numeroLigneFactureVentes = row.numeroLigneFactureVentes;
  this.managedLigneFactureVentes.article.code =  row.article;
  this.managedLigneFactureVentes.magasin.code =  row.magasin;
  this.managedLigneFactureVentes.unMesure.code =  row.unMesure;
  this.managedLigneFactureVentes.quantite =  row.quantite;
  this.managedLigneFactureVentes.prixUnitaire =  row.prixUnitaire;
  this.managedLigneFactureVentes.montantHT =  row.montantHT;
  this.managedLigneFactureVentes.montantTTC =  row.montantTTC;
  this.managedLigneFactureVentes.tva =  row.tva; 
  this.managedLigneFactureVentes.montantTVA =  row.montantTVA;
  this.managedLigneFactureVentes.fodec =  row.fodec;
  this.managedLigneFactureVentes.montantFodec =  row.montantFodec;
  this.managedLigneFactureVentes.remise =  row.remise;
  this.managedLigneFactureVentes.montantRemise=  row.montantRemise;
 
  this.managedLigneFactureVentes.otherTaxe1 =  row.otherTaxe1;
  this.managedLigneFactureVentes.montantOtherTaxe1 =  row.montantOtherTaxe1;
  this.managedLigneFactureVentes.otherTaxe2 =  row.otherTaxe2;
  this.managedLigneFactureVentes.montantOtherTaxe2=  row.montantOtherTaxe2;
  this.managedLigneFactureVentes.montantBenefice=  row.montantBenefice;
  this.managedLigneFactureVentes.benefice=  row.benefice;

  if(row.magasin === undefined)
    {
      this.managedLigneFactureVentes.magasin = new Magasin();
    } else
    {
      this.fillDataLibelleMagasin();
    }
    if(row.unMesure === undefined)
      {
        this.managedLigneFactureVentes.unMesure = new UnMesure();
      }
      if(row.article === undefined)
        {
          this.managedLigneFactureVentes.article = new Article();
        } else
        {
          this.fillDataLibelleArticle();
        }
        console.log('row to edit is ', row);

  this.manageLigneMatExpansionPanel.open();
  
}
deleteLigneFactureVentes(row)
{
 // console.log('index is ');
  //console.log(index);
  const indexOfRow = this.getIndexOfRow(row);
  this.factureVente.listLignesFactures.splice(indexOfRow , 1 );
  this.transformDataToByConsumedByMatTable();
  
}
addLigneFactureVentes()
{
  if (this.managedLigneFactureVentes.quantite === undefined
    || this.managedLigneFactureVentes.quantite === 0 ||
    this.managedLigneFactureVentes.quantite === null ||
    this.managedLigneFactureVentes.prixUnitaire === undefined
      || this.managedLigneFactureVentes.prixUnitaire === 0 ||
      this.managedLigneFactureVentes.prixUnitaire === null  ||
      this.managedLigneFactureVentes.article.code === undefined  ||
      this.managedLigneFactureVentes.article.code === '' ||
      this.managedLigneFactureVentes.article.code === null 
  ) {
        this.openSnackBar('Donnnées Manquées (QTE/ART/PUNIT!');
    } else if (this.managedLigneFactureVentes.numeroLigneFactureVentes === undefined
  || this.managedLigneFactureVentes.numeroLigneFactureVentes === '' ||
  this.managedLigneFactureVentes.numeroLigneFactureVentes === null ) {
      this.openSnackBar('Numéro de Ligne Non Spécifié !');
    } else {
  this.managedLigneFactureVentes.numeroFactureVentes = this.factureVente.numeroFactureVente;
  if (this.factureVente.listLignesFactures.length === 0) {
  this.factureVente.listLignesFactures.push(this.managedLigneFactureVentes);
  } else  {
    console.log('look for index:');
     const indexOfRow = this.getIndexOfRow(this.managedLigneFactureVentes);
     console.log(indexOfRow);
     
     {
           if (indexOfRow === -1) {
    this.factureVente.listLignesFactures.push(this.managedLigneFactureVentes);

         } else  {
     this.factureVente.listLignesFactures[indexOfRow] = this.managedLigneFactureVentes;

       }
      }
  }

  this.transformDataToByConsumedByMatTable() ;
  this.managedLigneFactureVentes = new LigneFactureVente();
  this.manageLigneMatExpansionPanel.close();
  this.listLignesMatExpansionPanel.open();
}
}

/*masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataLignesFactureVentesSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataLignesFactureVentesSource.data.length;
  return numSelected === numRows;
}*/
 /* deleteRow(i)
  {
    
    this.factureVente.montantHT = +this.factureVente.montantHT - this.lignesFactures[i].montantHT;
    this.factureVente.montantTVA = +this.factureVente.montantTVA - this.lignesFactures[i].montantTVA ;
    this.factureVente.montantTTC = +this.factureVente.montantTTC  - this.lignesFactures[i].montantTTC ;

    this.factureVente.montantRemise = +this.factureVente.montantRemise  - this.lignesFactures[i].montantRemise ;
      this.factureVente.montantAPayer = this.factureVente.montantTTC ;
      this.factureVente.montantAPayerApresRemise = +this.factureVente.montantAPayer - this.factureVente.montantRemise;
      console.log('after delete');
      console.log( this.factureVente.montantHT);
      console.log( this.factureVente.montantTTC);
      this.lignesFactures.splice(i, 1 );
      /*this.lignesFactures.forEach(function(ligne, index) {
                  ligne.code = index + 1;         
      },
      this);*/
      /*this.editLignesMode.splice(i, 1); 
      this.readOnlyLignes.splice(i, 1); 
      
      this.lignesFactures.push(new LigneFactureVente());
      this.editLignesMode.push(false);
      this.readOnlyLignes.push(false);
      
         
}*/

/*

  validateRow(i)
  {
     this.factureVente.montantHT = +this.factureVente.montantHT + this.lignesFactures[i].montantHT ;
     this.factureVente.montantTVA = +this.factureVente.montantTVA + this.lignesFactures[i].montantTVA ;
     this.factureVente.montantTTC = +this.factureVente.montantTTC  + this.lignesFactures[i].montantTTC ;
     this.factureVente.montantRemise = +this.factureVente.montantRemise  + this.lignesFactures[i].montantRemise ;
     this.factureVente.montantAPayer = this.factureVente.montantTTC ;
     this.factureVente.montantAPayerApresRemise = +this.factureVente.montantAPayer - this.factureVente.montantRemise;
     this.editLignesMode[i] = true;
     this.readOnlyLignes[i] = true;
    }
    editRow(i)
    {
      if (this.readOnlyLignes[i])
      {
        this.readOnlyLignes[i] = false;
      }  else {
       // this.validateRow(i);

        this.recalculMontants();
        this.readOnlyLignes[i] = true;
        
      }
       
    }*/

/*
nextPage()
  {
    this.pagesLignesFacures[this.currentPage - 1] = this.lignesFactures;

    this.pagesReadOnlyLignesFacures[this.currentPage - 1] = this.readOnlyLignes;
    this.pagesEditModeLignesFacures[this.currentPage - 1] = this.editLignesMode;
    
    this.currentPage++;
    if ( this.pagesLignesFacures[this.currentPage - 1] === undefined) {
    this.initializeListLignesFactures();

console.log('Next Page lignes Factures ');
console.log(this.lignesFactures);
this.pagesLignesFacures[this.currentPage - 1 ] = this.lignesFactures;
this.pagesReadOnlyLignesFacures[this.currentPage - 1] = this.readOnlyLignes;
this.pagesEditModeLignesFacures[this.currentPage - 1] = this.editLignesMode;


} else {
console.log('Next Page lignes Factures ');
this.lignesFactures = this.pagesLignesFacures[this.currentPage - 1 ];
this.readOnlyLignes = this.pagesReadOnlyLignesFacures[this.currentPage - 1 ];
this.editLignesMode = this.pagesEditModeLignesFacures[this.currentPage - 1 ];

}
this.factureVenteFilter = new FactureVenteFilter(this.listClients, this.listArticles);
const  button: HTMLButtonElement = this.elRef.nativeElement.querySelector('.paginationLabel' )  ;
button.innerText = 'Page ' + this.currentPage + ' / ' + this.pagesLignesFacures.length;
}

previousPage()
{
this.pagesLignesFacures[this.currentPage - 1] = this.lignesFactures ;
this.pagesReadOnlyLignesFacures[this.currentPage - 1] = this.readOnlyLignes;
this.pagesEditModeLignesFacures[this.currentPage - 1] = this.editLignesMode;

this.currentPage--;
this.lignesFactures =  this.pagesLignesFacures[this.currentPage - 1];
this.readOnlyLignes = this.pagesReadOnlyLignesFacures[this.currentPage - 1];
this.editLignesMode = this.pagesEditModeLignesFacures[this.currentPage - 1];

this.factureVenteFilter = new FactureVenteFilter(this.listClients, this.listArticles);
const  button: HTMLButtonElement = this.elRef.nativeElement.querySelector('.paginationLabel' )  ;
button.innerText = 'Page ' + this.currentPage + ' / ' + this.pagesLignesFacures.length;

}*/

  /*fillDataLignesFactures()
  {
    this.factureVente.listLignesFactures = [];
     this.listLignesFactureVentes.forEach(function(ligne, index) {
           if ( ligne.montantHT !== 0 ) {
             if (ligne.code === undefined || ligne.code === '')
              {
                ligne.code = this.factureVente.listLignesFactures.length + 1;
              }
            this.factureVente.listLignesFactures.push(ligne);
           }
    
     }, this);
  }*/
  /*checkOneActionInvoked() {
  const listSelectedLignesFactureVentes: LigneFactureVente[] = [];
console.log(this.selection.selected);
if (this.selection.selected.length !== 0 ) {
 this.managedLigneFactureVentes.numeroLigneFactureVentes =  this.selection.selected[this.selection.selected.length - 1]
 .numeroLigneFactureVentes;
 this.managedLigneFactureVentes.article.code =  this.selection.selected[this.selection.selected.length - 1].article;
 this.managedLigneFactureVentes.magasin.code =  this.selection.selected[this.selection.selected.length - 1].magasin;
 this.managedLigneFactureVentes.unMesure.code =  this.selection.selected[this.selection.selected.length - 1].unMesure;
 this.managedLigneFactureVentes.quantite =  this.selection.selected[this.selection.selected.length - 1].quantite;
 this.managedLigneFactureVentes.prixUnitaire=  this.selection.selected[this.selection.selected.length - 1].prixUnitaire;
 this.managedLigneFactureVentes.montantHT =  this.selection.selected[this.selection.selected.length - 1].montantHT;
 this.managedLigneFactureVentes.montantTTC =  this.selection.selected[this.selection.selected.length - 1].montantTTC;
 this.managedLigneFactureVentes.tva =  this.selection.selected[this.selection.selected.length - 1].tva; 
 this.managedLigneFactureVentes.montantTVA =  this.selection.selected[this.selection.selected.length - 1].montantTVA;
 this.managedLigneFactureVentes.fodec =  this.selection.selected[this.selection.selected.length - 1].fodec;
 this.managedLigneFactureVentes.montantFodec =  this.selection.selected[this.selection.selected.length - 1].montantFodec;
 this.managedLigneFactureVentes.remise =  this.selection.selected[this.selection.selected.length - 1].remise;
 this.managedLigneFactureVentes.montantRemise=  this.selection.selected[this.selection.selected.length - 1].montantRemise;

 this.managedLigneFactureVentes.otherTaxe1 =  this.selection.selected[this.selection.selected.length - 1].otherTaxe1;
 this.managedLigneFactureVentes.montantOtherTaxe1 =  this.selection.selected[this.selection.selected.length - 1].montantOtherTaxe1;
 
 this.managedLigneFactureVentes.otherTaxe2 =  this.selection.selected[this.selection.selected.length - 1].otherTaxe2;
 this.managedLigneFactureVentes.montantOtherTaxe2=  this.selection.selected[this.selection.selected.length - 1].montantOtherTaxe2;

 this.managedLigneFactureVentes.montantBenefice=  this.selection.selected[this.selection.selected.length - 1].montantBenefice;
 this.managedLigneFactureVentes.benefice=  this.selection.selected[this.selection.selected.length - 1].benefice;
 
 
const codes: string[] = this.selection.selected.map((ligneFactElement) => {
  return ligneFactElement.code;
});
this.factureVente.listLignesFactures.forEach(LigneF => {
  if (codes.findIndex(code => code === LigneF.code) > -1) {
    listSelectedLignesFactureVentes.push(LigneF);
  }
  });
}
}*/
  fillDataLibelleUnMesure()
  {

  }
  fillDataLibelleMagasin()
  {
    this.listMagasins.forEach((magasin, index) => {
      if (this.managedLigneFactureVentes.magasin.code === magasin.code) {
       this.managedLigneFactureVentes.magasin.description = magasin.description;
       this.managedLigneFactureVentes.magasin.adresse = magasin.adresse;
       
       }

 } , this);
  }
  fillDataLibelleArticle()  {
    
            this.listArticles.forEach((article, index) => {
                 if (this.managedLigneFactureVentes.article.code === article.code) {
                  this.managedLigneFactureVentes.article.designation = article.designation;
                  this.managedLigneFactureVentes.article.libelle = article.libelle;
                  
                  }
    
            } , this);
   }
}
