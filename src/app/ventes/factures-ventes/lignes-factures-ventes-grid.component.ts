
import { Component, OnInit, Inject, ViewChild, Input, AfterViewChecked, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {FactureVenteService} from './facture-vente.service';
import { FactureVente } from './facture-vente.model';
import {LigneFactureVente} from './ligne-facture-vente.model';
import { LigneFactureVenteElement } from './ligne-facture-vente.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
 import { DialogAddLigneFactureVentesComponent} from './popups/dialog-add-ligne-facture-ventes.component';
 import { DialogEditLigneFactureVentesComponent } from './popups/dialog-edit-ligne-facture-ventes.component';
@Component({
    selector: 'app-lignes-factures-ventes-grid',
    templateUrl: 'lignes-factures-ventes-grid.component.html',
  })
  export class LignesFacturesVentesGridComponent implements OnInit, OnChanges {
     private listLignesFacturesVentes: LigneFactureVente[] = [];
     private  dataLignesFacturesVentesSource: MatTableDataSource<LigneFactureVenteElement>;
     private selection = new SelectionModel<LigneFactureVenteElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];

     @Input()
     private factureVentes: FactureVente;

     constructor(  private factureVenteService: FactureVenteService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      // console.log('credit code after dipslay tab ', this.credit.code);
      this.factureVenteService.getLignesFacturesVentesByCodeFacture(this.factureVentes.code).subscribe((listLignesFactVentes) => {
        this.listLignesFacturesVentes = listLignesFactVentes;
// console.log('recieving data from backend', this.listPayementsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }

  ngOnChanges() {
    this.factureVenteService.getLignesFacturesVentesByCodeFacture(this.factureVentes.code).subscribe((listLignesFactVentes) => {
      this.listLignesFacturesVentes = listLignesFactVentes;
// console.log('recieving data from backend', this.listPayementsCredits  );
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }
  checkOneActionInvoked() {
    const listSelectedLignesFactVentes: LigneFactureVente[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((lFactElement) => {
    return lFactElement.code;
  });
  this.listLignesFacturesVentes.forEach(ligneF => {
    if (codes.findIndex(code => code === ligneF.code) > -1) {
      listSelectedLignesFactVentes.push(ligneF);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listLignesFacturesVentes !== undefined) {
 
      this.listLignesFacturesVentes.forEach(ligneFactureVentes => {
             listElements.push( {code: ligneFactureVentes.code ,
              numeroLigneFactureVentes: ligneFactureVentes.numeroLigneFactureVentes,
          article: ligneFactureVentes.article.code,
          magasin: ligneFactureVentes.magasin.code,
          quantite: ligneFactureVentes.quantite,
          prixUnitaire: ligneFactureVentes.prixUnitaire,
          unMesure: ligneFactureVentes.unMesure,
          
          montantHT: ligneFactureVentes.montantHT,
          fodec: ligneFactureVentes.fodec,
          montantFodec: ligneFactureVentes.montantFodec,
          otherTaxe1: ligneFactureVentes.otherTaxe1,
          montantOtherTaxe1: ligneFactureVentes.montantOtherTaxe1,
          otherTaxe2: ligneFactureVentes.otherTaxe2,
          montantOtherTaxe2: ligneFactureVentes.montantOtherTaxe2,
          tva: ligneFactureVentes.tva,
          montantTVA: ligneFactureVentes.montantTVA,
          montantTTC: ligneFactureVentes.montantTTC,
          remise: ligneFactureVentes.remise,
          montantRemise: ligneFactureVentes.montantRemise,
          montantAPayer: ligneFactureVentes.montantAPayer,
          benefice: ligneFactureVentes.benefice,
          montantBenefice: ligneFactureVentes.montantBenefice,
          etat: ligneFactureVentes.etat,
          prixVente: ligneFactureVentes.prixVente,
          
          
        } );
      });
    }
      this.dataLignesFacturesVentesSource = new MatTableDataSource<LigneFactureVenteElement>(listElements);
      this.dataLignesFacturesVentesSource.sort = this.sort;
      this.dataLignesFacturesVentesSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'numeroLigneFactureVentes' , displayTitle: 'N°'},
     {name: 'article' , displayTitle: 'Art'}, 
     {name: 'magasin' , displayTitle: 'Mag'},
     {name: 'quantite' , displayTitle: 'Qte'},
     {name: 'unMesure' , displayTitle: 'UM'},
      {name: 'prixUnitaire' , displayTitle: 'P.UN'},
     {name: 'montantHT' , displayTitle: 'M.HT'}, 
     {name: 'montantTVA' , displayTitle: 'M.TVA'},
     {name: 'montantTTC' , displayTitle: 'M.TTC'},
     {name: 'montantRemise' , displayTitle: 'M.Remise'},
      {name: 'montantBenefice' , displayTitle: 'M.Benefice'},
      {name: 'montantAPayer' , displayTitle: 'M.APayer'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataLignesFacturesVentesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataLignesFacturesVentesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataLignesFacturesVentesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataLignesFacturesVentesSource.paginator) {
      this.dataLignesFacturesVentesSource.paginator.firstPage();
    }
  }
  showAddLigneDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeFactureVentes : this.factureVentes.code
   };
 
   const dialogRef = this.dialog.open(DialogAddLigneFactureVentesComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(lAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditLigneDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeLigneFactureVentes: this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditLigneFactureVentesComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(lAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucune Ligne Séléctionnée!');
    }
    }
    supprimerLignes()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(pay, index) {
              this.factureVenteService.deleteLigneFactureVentes(pay.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar(  ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucune Ligne Séléctionnée!');
        }
    }
    refresh() {
      this.factureVenteService.getLignesFacturesVentesByCodeFacture(this.factureVentes.code).subscribe((listLignesFacs) => {
        this.listLignesFacturesVentes = listLignesFacs;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
















}
