import { Component, OnInit, Output,ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {FactureVente} from './facture-vente.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {FactureVenteService} from './facture-vente.service';
// import { ActionData } from './action-data.interface';
import { Client } from '../../ventes/clients/client.model';
import { FactureVenteElement } from './facture-vente.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DialogAddClientComponent } from '../clients/dialog-add-client.component';
import { DialogAddPayementFactureVentesComponent } from './popups/dialog-add-payement-facture-ventes.component';
import { FactureVentesDataFilter } from './facture-ventes-data.filter';
import { SelectItem } from 'primeng/api';

@Component({
  selector: 'app-factures-ventes-grid',
  templateUrl: './factures-ventes-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class FacturesVentesGridComponent implements OnInit {

  private factureVente: FactureVente =  new FactureVente();
  private selection = new SelectionModel<FactureVenteElement>(true, []);
  private factureVentesDataFilter = new FactureVentesDataFilter();
  private showSelectColumnsMultiSelect = false;
  private showFilter = false;
  private showFilterClient = false;
  
@Output()
select: EventEmitter<FactureVente[]> = new EventEmitter<FactureVente[]>();

//private lastClientAdded: Client;

 selectedFactureVente: FactureVente ;

 

 @Input()
 private listFacturesVentes: any = [] ;
 listFacturesVentesOrgin: any = [];
 private checkedAll: false;

 private  dataFacturesVentesSource: MatTableDataSource<FactureVenteElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
 
 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay :ColumnDefinition[] = [];

 private columnsToSelect : SelectItem[];
  private  selectedColumnsDefinitions: string[];

 private selectedColumnsNames: string[];

 private etatsFactureVentes: any[] = [
  {value: 'Annule', viewValue: 'Annulé'},
  {value: 'Initial', viewValue: 'Initial'},
  {value: 'Valide', viewValue: 'Validé'}
];
private payementModes: any[] = [
  {value: 'Comptant', viewValue: 'Comptant'},
  {value: 'Cheque', viewValue: 'Chèque'},
  {value: 'Credit', viewValue: 'Crédit'},
  {value: 'Avance', viewValue: 'Avance'},
  {value: 'Comptant_Cheque', viewValue: 'Comptant/Chèque'},
  {value: 'Comptant_Avance', viewValue: 'Comptant/Avance'},
  {value: 'Credit_Comptant', viewValue: 'Crédit/Comptant'},
  {value: 'Credit_Cheque', viewValue: 'Crédit/Chèque'},
  {value: 'Credit_Avance', viewValue: 'Crédit/Avance'},
  {value: 'Donnation', viewValue: 'Amicalité'}
];
  constructor( private route: ActivatedRoute, private factureVenteService: FactureVenteService,
    private router: Router, private matSnackBar: MatSnackBar,
  private  dialog: MatDialog ) {
  }
  ngOnInit() {

    this.columnsToSelect = [
      {label: 'N° Facture ', value: 'numeroFactureVente', title: 'N°Facture'},
      {label: 'Date Ventes', value: 'dateVente', title: 'Date Ventes'},
      {label: 'Date Facturation', value: 'dateFacturation', title: 'Date Facturation'},
      {label: 'Montant à Payer', value: 'montantAPayer', title: 'Montant à Payer'},
      {label: 'Montant TTC', value: 'montantTTC', title: 'Montant TTC'},
      {label: 'Montant HT', value: 'montantHT', title: 'Montant HT'},
      {label: 'Montant TVA', value: 'montantTVA', title: 'Montant TVA'},
      {label: 'Mode Paiement', value: 'modePayement', title: 'Mode Paiement'},
      {label: 'Rs.Soc.Client', value:  'raisonSociale', title: 'Rs.Soc.Client' },
      {label: 'Matr.Client', value:  'matriculeFiscale', title: 'Matr.Client' },
      {label: 'Reg.Client', value:  'registreCommerce', title: 'Reg.Client' },
      {label: 'CIN Client', value: 'cin', title: 'CIN Client'},  
      {label: 'Nom Client', value: 'nom', title: 'Nom Client'},
      {label: 'Prénom Client', value: 'prenom', title: 'Prénom Client'},
      {label: 'Etat', value:  'etat', title: 'Etat' },
  
  ];
  this.selectedColumnsDefinitions = [
    'numeroFactureVente',
    'dateVente' , 
    'montantAPayer', 
    'nom' ,
    'prenom',
    'cin', 
    'etat'
    ];
    this.defaultColumnsDefinitions = [
      {name: 'numeroFactureVente' , displayTitle: 'N°Facture'},
      {name: 'dateVente' , displayTitle: 'Date Ventes'},
      {name: 'dateFacturation' , displayTitle: 'Date Facturation'},
      {name: 'montantAPayer' , displayTitle: 'Montant à Payer'},
      {name: 'montantTTC' , displayTitle: 'Montant TTC'},
      {name: 'montantHT' , displayTitle: 'Montant HT'},
      {name: 'montantTVA' , displayTitle: 'Montant TVA'},
      {name: 'modePayement' , displayTitle: 'Mode Paiement'},
       {name: 'nom' , displayTitle: 'Nom Cl.'},
      {name: 'prenom' , displayTitle: 'Prénom Cl.'},
      {name: 'cin' , displayTitle: 'CIN Cl.'},
      {name: 'matriculeFiscale' , displayTitle: 'Matr. Cl.'},
      {name: 'raisonSociale' , displayTitle: 'Raison Cl.'},
      {name: 'registreCommerce' , displayTitle: 'Registre Cl.'},
       {name: 'etat', displayTitle: 'Etat' },
      ];

    this.factureVenteService.getFacturesVentes(0, 5,null).subscribe((facturesVentes) => {
      this.listFacturesVentes = facturesVentes;
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });


  }
  deleteFactureVente(factureVente) {
      console.log('call delete !', factureVente );
    this.factureVenteService.deleteFactureVente(factureVente.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData(null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.factureVenteService.getFacturesVentes(0, 5, filter).subscribe((facturesVentes) => {
    this.listFacturesVentes = facturesVentes;
    this.transformDataToByConsumedByMatTable();
   });
}


checkOneActionInvoked() {
  const listSelectedFacturesVentes: FactureVente[] = [];
console.log('selected clients are ');
console.log(this.selection.selected);
if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((factureVenteElement) => {
  return factureVenteElement.code;
});
this.listFacturesVentes.forEach(factureVente => {
  if (codes.findIndex(code => code === factureVente.code) > -1) {
    listSelectedFacturesVentes.push(factureVente);
  }
  });
}
this.select.emit(listSelectedFacturesVentes);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listFacturesVentes !== undefined) {
    this.listFacturesVentes.forEach(factureVente => {
      listElements.push( {code: factureVente.code ,
         numeroFactureVente: factureVente.numeroFactureVente,
        dateVente: factureVente.dateVente, cin: factureVente.client.cin,
        nom: factureVente.client.nom, prenom: factureVente.client.prenom,
        matriculeFiscale: factureVente.client.matriculeFiscale, raisonSociale: factureVente.client.raisonSociale ,
        montantAPayer: factureVente.montantAPayer, montantTTC: factureVente.montantTTC,
        montantHT: factureVente.montantHT, montantTVA: factureVente.montantTVA,
        etat: factureVente.etat
       
    });
  });
    this.dataFacturesVentesSource = new MatTableDataSource<FactureVenteElement>(listElements);
    this.dataFacturesVentesSource.sort = this.sort;
    this.dataFacturesVentesSource.paginator = this.paginator;
    this.factureVenteService.getTotalFacturesVentes(this.factureVentesDataFilter).
    subscribe((response) => {
      console.log('Total credits is ', response);
       this.dataFacturesVentesSource.paginator.length = response.totalFacturesVentes;
     });
}
}
specifyListColumnsToBeDisplayed() {
  
   console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
   this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
 ?
    this.defaultColumnsDefinitions : [];
    this.selectedColumnsNames = [];
    
    if (this.selectedColumnsDefinitions !== undefined)
     {
       this.selectedColumnsDefinitions.forEach((colName) => { 
            const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
            const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
            const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
            this.columnsDefinitionsToDisplay.push(colDef);
           });
     }
    this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
   this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
              return colDef.displayTitle;
    });
    console.log('columns to disp:', this.columnsDefinitionsToDisplay);
    this.columnsNames = [];
    this.columnsNames[0] = 'select';
    this.columnsDefinitionsToDisplay .map((colDef) => {
     this.columnsNames.push(colDef.name);
    if (this.selectedColumnsDefinitions !== undefined) {
     this.selectedColumnsNames.push(colDef.displayTitle);
    }
 });
    this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
     return colDef.displayTitleAr;
 });
 }
masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataFacturesVentesSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataFacturesVentesSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataFacturesVentesSource.filter = filterValue.trim().toLowerCase();
  if (this.dataFacturesVentesSource.paginator) {
    this.dataFacturesVentesSource.paginator.firstPage();
  }
}

loadAddFactureVenteComponent() {
  this.router.navigate(['/pms/ventes/ajouterFactureVentes']) ;

}
loadEditFactureVenteComponent() {
  this.router.navigate(['/pms/ventes/editerFactureVentes', this.selection.selected[ this.selection.selected.length - 1].code]) ;

}
supprimerFacturesVentes()
{
  console.log('NBRE OF ELEMENTS SELECTED');
  console.log(this.selection.selected.length);
  console.log(' ELEMENTS SELECTED');
  console.log(this.selection.selected);
  if ( this.selection.selected.length !== 0) {
      this.selection.selected.forEach(function(factureVente, index) {

          this.factureVenteService.deleteFactureVente(factureVente.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.openSnackBar( this.selection.selected.length + ' Facture(s) de Ventes Supprimé(s)');
            this.selection.selected = [];
            this.loadData();
            }
      }, this);
    } else {
      this.openSnackBar('Aucune Facture Séléctionnée !');
    }
}
refresh() {
  this.loadData(null);
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 addPayement()
 {
  if ( this.selection.selected.length !== 0) {
    
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
    top: '0'
  };
  dialogConfig.data = {
    codeFactureVentes : this.selection.selected[0].code
  };

  const dialogRef = this.dialog.open(DialogAddPayementFactureVentesComponent,
    dialogConfig
  );
} else {
  this.openSnackBar('Aucune Facture Séléctionnée !');
}
 }
 history()
 {
   
 }
 addClient()
 {
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
  };

  const dialogRef = this.dialog.open(DialogAddClientComponent,
    dialogConfig
  );
 }
 attacherDocuments()
 {

 }


 changePage($event) {
  console.log('page event is ', $event);
 this.factureVenteService.getFacturesVentes($event.pageIndex, $event.pageSize,this.factureVentesDataFilter ).
 subscribe((facturesVentes) => {
    this.listFacturesVentes = facturesVentes;
    console.log(this.listFacturesVentes);
   // this.transformDataToByConsumedByMatTable();
   const listElements = [];
   this.listFacturesVentes.forEach(factureVente => {
    listElements.push( {code: factureVente.code ,
       numeroFactureVente: factureVente.numeroFactureVente,
      dateVente: factureVente.dateVente, cin: factureVente.client.cin,
      nom: factureVente.client.nom, prenom: factureVente.client.prenom,
      matriculeFiscale: factureVente.client.matriculeFiscale, raisonSociale: factureVente.client.raisonSociale ,
      montantAPayer: factureVente.montantAPayer, montantTTC: factureVente.montantTTC,
      montantHT: factureVente.montantHT, montantTVA: factureVente.montantTVA,
      etat: factureVente.etat
     
  });
});
     this.dataFacturesVentesSource = new MatTableDataSource<FactureVenteElement>(listElements);
 
     this.factureVenteService.getTotalFacturesVentes(this.factureVentesDataFilter)
     .subscribe((response) => {
      console.log('Total credits is ', response);
       this.dataFacturesVentesSource.paginator.length = response.totalFacturesVentes;
     });

    });
}
toggleFilterPanel() {
  this.showFilter = !this.showFilter;
}
toggleShowColumnsMultiSelect() {
 this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
}
loadFiltredData() {
 console.log('the filter is ', this.factureVentesDataFilter);
 this.loadData(this.factureVentesDataFilter);
}
activateFilterClient()
{
  this.showFilterClient = !this.showFilterClient;
}

}
