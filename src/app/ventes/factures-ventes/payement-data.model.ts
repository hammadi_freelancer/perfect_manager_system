
type LibelleModePayement = 'Credit'| 'Comptant' | 'Cheque' ;
export class PayementData {
    constructor(public libelle?: LibelleModePayement,
        public numeroCheque?: string,
         public dateVirementCheque?: string, 
         public dateVirementChequeObject?: Date, 
         public aReporter?: number,
        public aCompte?: number,
         public chequeData?: string,
        public proprietaireCheque?: string,
         public montantComptant?: number,
        public montantCredit?: number,
        public responsablePayement?: string ,
        public dateOperation?: string
     ) {
         this.aReporter =  Number(0);
         this.aCompte = Number(0);
         this.montantComptant = Number(0);
         this.montantCredit = Number(0);
    }
}
