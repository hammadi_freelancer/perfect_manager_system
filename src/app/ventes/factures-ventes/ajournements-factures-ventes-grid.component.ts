
import { Component, OnInit, Inject, ViewChild, Input, OnChanges} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {FactureVenteService} from './facture-vente.service';
import { FactureVente} from './facture-vente.model';
import {AjournementFactureVente} from './ajournement-facture-ventes.model';
import { AjournementFactureVenteElement } from './ajournement-facture-ventes.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import { DialogAddAjournementFactureVentesComponent } from './popups/dialog-add-ajournement-facture-ventes.component';
import { DialogEditAjournementFactureVentesComponent} from './popups/dialog-edit-ajournement-facture-ventes.component';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-ajournements-factures-ventes-grid',
    templateUrl: 'ajournements-factures-ventes-grid.component.html',
  })
  export class AjournementsFacturesVentesGridComponent implements OnInit, OnChanges {
     private listAjournementFacturesVentes: AjournementFactureVente[] = [];
     private  dataAjournementsFacturesVentesSource: MatTableDataSource<AjournementFactureVenteElement>;
     private selection = new SelectionModel<AjournementFactureVenteElement>(true, []);

     @Input()
     private factureVentes : FactureVente;

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private factureVenteService: FactureVenteService,
     private matSnackBar: MatSnackBar, public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.factureVenteService.getAjournementsFacturesVentesByCodeFacture(this.factureVentes.code).
      subscribe((listAjournementsFactureVentes) => {
        this.listAjournementFacturesVentes = listAjournementsFactureVentes;
// console.log('recieving data from backend', this.listAjournementsFactureVentes  );
        this.transformDataToByConsumedByMatTable();
         });

  }

ngOnChanges()
{
  this.factureVenteService.getAjournementsFacturesVentesByCodeFacture
  (this.factureVentes.code).subscribe((listAjournementsFactureVentes) => {
    this.listAjournementFacturesVentes = listAjournementsFactureVentes;
//console.log('recieving data from backend', this.listAjournementCredits  );
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
     });
}
  checkOneActionInvoked() {
    const listSelectedAjournementsFacturesVentes: AjournementFactureVente[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((ajournementFactureVentesElement) => {
    return ajournementFactureVentesElement.code;
  });
  this.listAjournementFacturesVentes.forEach(ajournementFactVen => {
    if (codes.findIndex(code => code === ajournementFactVen.code) > -1) {
      listSelectedAjournementsFacturesVentes.push(ajournementFactVen);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listAjournementFacturesVentes !== undefined) {

      this.listAjournementFacturesVentes.forEach(ajournementFVentes => {
             listElements.push( {code: ajournementFVentes.code ,
          dateOperation: ajournementFVentes.dateOperation,
          nouvelleDatePayement: ajournementFVentes.nouvelleDatePayement,
          motif: ajournementFVentes.motif,
          description: ajournementFVentes.description
        } );
      });
    }
      this.dataAjournementsFacturesVentesSource = new MatTableDataSource<AjournementFactureVenteElement>(listElements);
      this.dataAjournementsFacturesVentesSource.sort = this.sort;
      this.dataAjournementsFacturesVentesSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'},
     {name: 'nouvelleDatePayement' , displayTitle: 'N°elle Date Pay.'},
     {name: 'motif' , displayTitle: 'Motif'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataAjournementsFacturesVentesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataAjournementsFacturesVentesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataAjournementsFacturesVentesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataAjournementsFacturesVentesSource.paginator) {
      this.dataAjournementsFacturesVentesSource.paginator.firstPage();
    }
  }

  showAddAjournementDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeFactureVentes: this.factureVentes.code
   };
 
   const dialogRef = this.dialog.open(DialogAddAjournementFactureVentesComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(ajAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditAjournementDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeAjournementFactureVentes : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditAjournementFactureVentesComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(ajAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Ajournement Séléctionnée!');
    }
    }
    supprimerAjournements()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(ajour, index) {
              this.factureVenteService.deleteAjournementFactureVentes(ajour.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( this.selection.selected.length + ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Ajournement Séléctionnée!');
        }
    }
    refresh() {
      this.factureVenteService.getAjournementsFacturesVentesByCodeFacture(this.factureVentes.code).subscribe((listAjourFact) => {
        this.listAjournementFacturesVentes= listAjourFact;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }


}
