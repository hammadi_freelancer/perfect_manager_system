
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {FactureVenteService} from '../facture-vente.service';
import {PayementFactureVentes} from '../payement-facture-ventes.model';
import {LigneFactureVente} from '../ligne-facture-vente.model';

@Component({
    selector: 'app-dialog-add-ligne-facture-ventes',
    templateUrl: 'dialog-add-ligne-facture-ventes.component.html',
  })
  export class DialogAddLigneFactureVentesComponent implements OnInit {
     private ligneFactureVentes: LigneFactureVente;
     private listCodesArticles: string[];
     private listCodesMagasins: string[];
     private listCodesUnMesures: string[];
     
     private etatsLignes: any[] = [
      {value: 'Annule', viewValue: 'Annulé'},
      {value: 'Initial', viewValue: 'Initial'},
      {value: 'Valide', viewValue: 'Validé'}
    ];
     constructor(  private  factureVentesService: FactureVenteService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.ligneFactureVentes = new LigneFactureVente();
           this.listCodesArticles = this.data.listCodesArticles;
           // this.listCodesMagasins = this.data.listCodesMagasins;
           
           this.ligneFactureVentes.etat = 'Initial';
           
    }
    saveLigneFactureVentes() 
    {
      this.ligneFactureVentes.codeFactureVentes = this.data.codeFactureVentes;
       console.log(this.ligneFactureVentes);
      //  this.saveEnCours = true;
       this.factureVentesService.addLigneFactureVentes(this.ligneFactureVentes).subscribe((response) => {
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.ligneFactureVentes = new LigneFactureVente();
             this.openSnackBar(  'Ligne Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
