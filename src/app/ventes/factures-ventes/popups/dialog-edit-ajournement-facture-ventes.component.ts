
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {FactureVenteService} from '../facture-vente.service';
import {AjournementFactureVente} from '../ajournement-facture-ventes.model';

@Component({
    selector: 'app-dialog-edit-ajournement-facture-ventes',
    templateUrl: 'dialog-edit-ajournement-facture-ventes.component.html',
  })
  export class DialogEditAjournementFactureVentesComponent implements OnInit {
     private ajournementFactureVente: AjournementFactureVente;
     private updateEnCours = false;
     
     constructor(  private factureVenteService: FactureVenteService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {

        this.factureVenteService.getAjournementFactureVentes(this.data.codeAjournementFactureVentes).subscribe((ajour) => {
            this.ajournementFactureVente = ajour;
     });


    }
    updateAjournementFactureVentes() 
    {
     this.updateEnCours = true;
       this.factureVenteService.updateAjournementFactureVentes(this.ajournementFactureVente).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.openSnackBar(  'Ajournement Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
