
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {FactureVenteService} from '../facture-vente.service';
import {AjournementFactureVente} from '../ajournement-facture-ventes.model';

@Component({
    selector: 'app-dialog-add-ajournement-facture-ventes',
    templateUrl: 'dialog-add-ajournement-facture-ventes.component.html',
  })
  export class DialogAddAjournementFactureVentesComponent implements OnInit {
     private ajournementFactureVentes: AjournementFactureVente;
     private saveEnCours = false;
     constructor(  private factureVenteService: FactureVenteService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.ajournementFactureVentes = new AjournementFactureVente();
    }
    saveAjournementFactureVentes() 
    {
      this.ajournementFactureVentes.codeFactureVente = this.data.codeFactureVentes;
      this.saveEnCours = true;
      // console.log(this.ajournementCredit);
      //  this.saveEnCours = true;
       this.factureVenteService.addAjournementFactureVentes(this.ajournementFactureVentes).subscribe((response) => {
        this.saveEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.ajournementFactureVentes = new AjournementFactureVente();
             this.openSnackBar(  'Ajournement Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
