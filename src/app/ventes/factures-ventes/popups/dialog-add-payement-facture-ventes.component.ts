
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {FactureVenteService} from '../facture-vente.service';
import {PayementFactureVentes} from '../payement-facture-ventes.model';

@Component({
    selector: 'app-dialog-add-payement-facture-ventes',
    templateUrl: 'dialog-add-payement-facture-ventes.component.html',
  })
  export class DialogAddPayementFactureVentesComponent implements OnInit {
     private payementFactureVentes: PayementFactureVentes;
     
     constructor(  private  factureVentesService: FactureVenteService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.payementFactureVentes = new PayementFactureVentes();
    }
    savePayementFactureVentes() 
    {
      this.payementFactureVentes.codeFactureVentes = this.data.codeFactureVentes;
       console.log(this.payementFactureVentes);
      //  this.saveEnCours = true;
       this.factureVentesService.addPayementFactureVentes(this.payementFactureVentes).subscribe((response) => {
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.payementFactureVentes = new PayementFactureVentes();
             this.openSnackBar(  'Payement Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
