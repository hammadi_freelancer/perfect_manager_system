
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {FactureVenteService} from '../facture-vente.service';
import {DocumentFactureVentes} from '../document-facture-ventes.model';
import { DocumentFactureVentesElement } from '../document-facture-ventes.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-documents-factures-ventes',
    templateUrl: 'dialog-list-documents-factures-ventes.component.html',
  })
  export class DialogListDocumentsFacturesVentesComponent implements OnInit {
     private listDocumentsFacturesVentes: DocumentFactureVentes[] = [];
     private  dataDocumentsFacturesVentes: MatTableDataSource<DocumentFactureVentesElement>;
     private selection = new SelectionModel<DocumentFactureVentesElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private factureVenteService: FactureVenteService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
     // console.log('code credit in dialog component is :', this.data.codeCredit);
      this.factureVenteService.getDocumentsFacturesVentesByCodeFacture(this.data.codeFactureVentes).subscribe((listDocumentFacVentes) => {
        this.listDocumentsFacturesVentes = listDocumentFacVentes;
//console.log('recieving data from backend', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedDocumentsFactVentes: DocumentFactureVentes[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentFactElement) => {
    return documentFactElement.code;
  });
  this.listDocumentsFacturesVentes.forEach(documentF => {
    if (codes.findIndex(code => code === documentF.code) > -1) {
      listSelectedDocumentsFactVentes.push(documentF);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsFacturesVentes !== undefined) {

      this.listDocumentsFacturesVentes.forEach(docFVente => {
             listElements.push( {code: docFVente.code ,
          dateReception: docFVente.dateReception,
          numeroDocument: docFVente.numeroDocument,
          typeDocument: docFVente.typeDocument,
          description: docFVente.description
        } );
      });
    }
      this.dataDocumentsFacturesVentes= new MatTableDataSource<DocumentFactureVentesElement>(listElements);
      this.dataDocumentsFacturesVentes.sort = this.sort;
      this.dataDocumentsFacturesVentes.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsFacturesVentes.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsFacturesVentes.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsFacturesVentes.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsFacturesVentes.paginator) {
      this.dataDocumentsFacturesVentes.paginator.firstPage();
    }
  }

}
