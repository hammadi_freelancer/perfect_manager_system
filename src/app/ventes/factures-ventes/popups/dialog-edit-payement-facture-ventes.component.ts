
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {FactureVenteService} from '../facture-vente.service';
import {FactureVente} from '../facture-vente.model';
import { PayementFactureVentes } from '../payement-facture-ventes.model';
@Component({
    selector: 'app-dialog-edit-payement-facture-ventes',
    templateUrl: 'dialog-edit-payement-facture-ventes.component.html',
  })
  export class DialogEditPayementFactureVentesComponent implements OnInit {

     private payementFactureVentes: PayementFactureVentes;
     private updateEnCours = false;
     constructor(  private factureVentesService: FactureVenteService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit() {
        this.factureVentesService.getPayementFactureVentes(this.data.codePayementFactureVentes).subscribe((payement) => {
            this.payementFactureVentes = payement;
     });
   }
    updatePayementFactureVentes() {
      //  this.saveEnCours = true;
      this.updateEnCours = true;
       this.factureVentesService.updatePayementFactureVentes(this.payementFactureVentes).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             // this.payementCredit = new PayementCredit();
             this.openSnackBar(  'Payement Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
