
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {FactureVenteService} from '../facture-vente.service';
import {FactureVente} from '../facture-vente.model';

import { LigneFactureVente } from '../ligne-facture-vente.model';
@Component({
    selector: 'app-dialog-edit-ligne-facture-ventes',
    templateUrl: 'dialog-edit-ligne-facture-ventes.component.html',
  })
  export class DialogEditLigneFactureVentesComponent implements OnInit {

     private ligneFactureVentes: LigneFactureVente;
     private updateEnCours = false;
     private listCodesArticles: string[];
     private listCodesMagasins: string[];
     private listCodesUnMesures: string[];
     
     private etatsLignes: any[] = [
      {value: 'Annule', viewValue: 'Annulé'},
      {value: 'Initial', viewValue: 'Initial'},
      {value: 'Valide', viewValue: 'Validé'}
    ];
     constructor(  private factureVentesService: FactureVenteService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit() {
        this.factureVentesService.getLigneFactureVentes(this.data.codeLigneFactureVentes).subscribe((ligne) => {
            this.ligneFactureVentes = ligne;
            this.listCodesArticles = this.data.listCodesArticles;
            // this.listCodesMagasins = this.data.listCodesMagasins;
            
     });
   }
    updateLigneFactureVentes() {
      //  this.saveEnCours = true;
      this.updateEnCours = true;
       this.factureVentesService.updateLigneFactureVentes(this.ligneFactureVentes).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             // this.payementCredit = new PayementCredit();
             this.openSnackBar(  'Ligne Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
