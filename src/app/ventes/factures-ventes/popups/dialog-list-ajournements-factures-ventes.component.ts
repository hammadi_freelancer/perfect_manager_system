
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {FactureVenteService} from '../facture-vente.service';
import {AjournementFactureVente} from '../ajournement-facture-ventes.model';
import { AjournementFactureVenteElement } from '../ajournement-facture-ventes.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-ajournements-factures-ventes',
    templateUrl: 'dialog-list-ajournements-factures-ventes.component.html',
  })
  export class DialogListAjournementsFacturesVentesComponent implements OnInit {
     private listAjournementFacturesVentes: AjournementFactureVente[] = [];
     private  dataAjournementsFacturesVentesSource: MatTableDataSource<AjournementFactureVenteElement>;
     private selection = new SelectionModel<AjournementFactureVenteElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private factureVenteService: FactureVenteService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      // console.log('code credit in dialog component is :', this.data.codeCredit);
      this.factureVenteService.getAjournementsFacturesVentesByCodeFacture(this.data.codeFactureVentes)
      .subscribe((listAjournementsFacturesVentes) => {
        this.listAjournementFacturesVentes = listAjournementsFacturesVentes;
// console.log('recieving data from backend', this.listAjournementCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedAjournementsFactureVentes: AjournementFactureVente[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((ajouFactVElement) => {
    return ajouFactVElement.code;
  });
  this.listAjournementFacturesVentes.forEach(ajournementFactureVentes => {
    if (codes.findIndex(code => code === ajournementFactureVentes.code) > -1) {
      listSelectedAjournementsFactureVentes.push(ajournementFactureVentes);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listAjournementFacturesVentes !== undefined) {

      this.listAjournementFacturesVentes.forEach(ajournementFactureVentes=> {
             listElements.push( {code: ajournementFactureVentes.code ,
          dateOperation: ajournementFactureVentes.dateOperation,
          nouvelleDatePayement: ajournementFactureVentes.nouvelleDatePayement,
          motif: ajournementFactureVentes.motif,
          description: ajournementFactureVentes.description
        } );
      });
    }
      this.dataAjournementsFacturesVentesSource = new MatTableDataSource<AjournementFactureVenteElement>(listElements);
      this.dataAjournementsFacturesVentesSource.sort = this.sort;
      this.dataAjournementsFacturesVentesSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'},
     {name: 'nouvelleDatePayement' , displayTitle: 'N°elle Date Pay.'},
     {name: 'motif' , displayTitle: 'Motif'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataAjournementsFacturesVentesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataAjournementsFacturesVentesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataAjournementsFacturesVentesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataAjournementsFacturesVentesSource.paginator) {
      this.dataAjournementsFacturesVentesSource.paginator.firstPage();
    }
  }

}
