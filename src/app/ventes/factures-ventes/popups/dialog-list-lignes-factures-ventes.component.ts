
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {FactureVenteService} from '../facture-vente.service';
import {LigneFactureVente} from '../ligne-facture-vente.model';
import { LigneFactureVenteElement } from '../ligne-facture-vente.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-lignes-factures-ventes',
    templateUrl: 'dialog-list-lignes-factures-ventes.component.html',
  })
  export class DialogListLignesFactureVentesComponent implements OnInit {
     private listLignesFactureVentes: LigneFactureVente[] = [];
     private  dataLignesFactureVentesSource: MatTableDataSource<LigneFactureVenteElement>;
     private selection = new SelectionModel<LigneFactureVenteElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private factureVentesService: FactureVenteService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      // console.log('code credit in dialog component is :', this.data.codeFactureVentes);
      this.factureVentesService.getLignesFacturesVentesByCodeFacture(this.data.codeFactureVentes)
      .subscribe((listLignesFactVentes) => {
        this.listLignesFactureVentes = listLignesFactVentes;
// console.log('recieving data from backend', this.listPayementsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedLignesFactureVentes: LigneFactureVente[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((ligneFactureVentesElement) => {
    return ligneFactureVentesElement.code;
  });
  this.listLignesFactureVentes.forEach(ligneFactureVentes => {
    if (codes.findIndex(code => code === ligneFactureVentes.code) > -1) {
      listSelectedLignesFactureVentes.push(ligneFactureVentes);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listLignesFactureVentes !== undefined) {
 
      this.listLignesFactureVentes.forEach(ligneFactureVentes => {
             listElements.push( {code: ligneFactureVentes.code ,
              numeroLigneFactureVentes: ligneFactureVentes.numeroLigneFactureVentes,
          article: ligneFactureVentes.article.code,
          magasin: ligneFactureVentes.magasin.code,
          quantite: ligneFactureVentes.quantite,
          prixUnitaire: ligneFactureVentes.prixUnitaire,
          unMesure: ligneFactureVentes.unMesure,
          
          montantHT: ligneFactureVentes.montantHT,
          fodec: ligneFactureVentes.fodec,
          montantFodec: ligneFactureVentes.montantFodec,
          otherTaxe1: ligneFactureVentes.otherTaxe1,
          montantOtherTaxe1: ligneFactureVentes.montantOtherTaxe1,
          otherTaxe2: ligneFactureVentes.otherTaxe2,
          montantOtherTaxe2: ligneFactureVentes.montantOtherTaxe2,
          tva: ligneFactureVentes.tva,
          montantTVA: ligneFactureVentes.montantTVA,
          montantTTC: ligneFactureVentes.montantTTC,
          remise: ligneFactureVentes.remise,
          montantRemise: ligneFactureVentes.montantRemise,
          montantAPayer: ligneFactureVentes.montantAPayer,
          benefice: ligneFactureVentes.benefice,
          montantBenefice: ligneFactureVentes.montantBenefice,
          etat: ligneFactureVentes.etat,
          prixVente: ligneFactureVentes.prixVente,
          
          
        } );
      });
    }
      this.dataLignesFactureVentesSource = new MatTableDataSource<LigneFactureVenteElement>(listElements);
      this.dataLignesFactureVentesSource.sort = this.sort;
      this.dataLignesFactureVentesSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'numeroLigneFactureVentes' , displayTitle: 'N°'},
     {name: 'article' , displayTitle: 'Art'}, 
     {name: 'magasin' , displayTitle: 'Mag'},
     {name: 'quantite' , displayTitle: 'Qte'},
     {name: 'unMesure' , displayTitle: 'UM'},
      {name: 'prixUnitaire' , displayTitle: 'P.UN'},
     {name: 'montantHT' , displayTitle: 'M.HT'}, 
     {name: 'montantTVA' , displayTitle: 'M.TVA'},
     {name: 'montantTTC' , displayTitle: 'M.TTC'},
     {name: 'montantRemise' , displayTitle: 'M.Remise'},
      {name: 'montantBenefice' , displayTitle: 'M.Benefice'},
      {name: 'montantAPayer' , displayTitle: 'M.APayer'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataLignesFactureVentesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataLignesFactureVentesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataLignesFactureVentesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataLignesFactureVentesSource.paginator) {
      this.dataLignesFactureVentesSource.paginator.firstPage();
    }
  }

}
