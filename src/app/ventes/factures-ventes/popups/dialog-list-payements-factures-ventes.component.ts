
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {FactureVenteService} from '../facture-vente.service';
import {PayementFactureVentes} from '../payement-facture-ventes.model';
import { PayementFactureVentesElement } from '../payement-facture-ventes.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-payements-factures-ventes',
    templateUrl: 'dialog-list-payements-factures-ventes.component.html',
  })
  export class DialogListPayementsFactureVentesComponent implements OnInit {
     private listPayementsFactureVentes: PayementFactureVentes[] = [];
     private  dataPayementsFactureVentesSource: MatTableDataSource<PayementFactureVentesElement>;
     private selection = new SelectionModel<PayementFactureVentesElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private factureVentesService: FactureVenteService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      // console.log('code credit in dialog component is :', this.data.codeFactureVentes);
      this.factureVentesService.getPayementsFacturesVentesByCodeFacture(this.data.codeFactureVentes)
      .subscribe((listPayementsFactVentes) => {
        this.listPayementsFactureVentes = listPayementsFactVentes;
// console.log('recieving data from backend', this.listPayementsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedPayementsFactureVentes: PayementFactureVentes[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((payementFactureVentesElement) => {
    return payementFactureVentesElement.code;
  });
  this.listPayementsFactureVentes.forEach(payementFactureVentes => {
    if (codes.findIndex(code => code === payementFactureVentes.code) > -1) {
      listSelectedPayementsFactureVentes.push(payementFactureVentes);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listPayementsFactureVentes !== undefined) {

      this.listPayementsFactureVentes.forEach(payementFactureVentes => {
             listElements.push( {code: payementFactureVentes.code ,
          dateOperation: payementFactureVentes.dateOperation,
          montantPaye: payementFactureVentes.montantPaye,
          modePayement: payementFactureVentes.modePayement,
          numeroCheque: payementFactureVentes.numeroCheque,
          dateCheque: payementFactureVentes.dateCheque,
          compte: payementFactureVentes.compte,
          compteAdversaire: payementFactureVentes.compteAdversaire,
          banque: payementFactureVentes.banque,
          banqueAdversaire: payementFactureVentes.banqueAdversaire,
          description: payementFactureVentes.description
        } );
      });
    }
      this.dataPayementsFactureVentesSource = new MatTableDataSource<PayementFactureVentesElement>(listElements);
      this.dataPayementsFactureVentesSource.sort = this.sort;
      this.dataPayementsFactureVentesSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'}, 
     {name: 'montantPaye' , displayTitle: 'Montant Payé'},
     {name: 'modePayement' , displayTitle: 'Mode Payement'},
      {name: 'numeroCheque' , displayTitle: 'N°Chèque'},
     {name: 'dateCheque' , displayTitle: 'Date Chèque'}, 
     {name: 'compte' , displayTitle: 'Compte'},
     {name: 'compteAdversaire' , displayTitle: 'Compte Adv.'},
      {name: 'banque' , displayTitle: 'Banque'},
       { name : 'banqueAdversaire' , displayTitle : 'Banque Adv.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataPayementsFactureVentesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataPayementsFactureVentesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataPayementsFactureVentesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataPayementsFactureVentesSource.paginator) {
      this.dataPayementsFactureVentesSource.paginator.firstPage();
    }
  }

}
