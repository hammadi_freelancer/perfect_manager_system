
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {FactureVenteService} from '../facture-vente.service';
import {DocumentFactureVentes} from '../document-facture-ventes.model';

@Component({
    selector: 'app-dialog-edit-document-facture-ventes',
    templateUrl: 'dialog-edit-document-facture-ventes.component.html',
  })
  export class DialogEditDocumentFactureVentesComponent implements OnInit {
     private documentFactureVentes: DocumentFactureVentes;
     private updateEnCours = false;
     private typesDoc: any[] = [
      {value: 'Cheque', viewValue: 'Chèque'},
      {value: 'Traite', viewValue: 'Traite'},
      {value: 'FactureVentes', viewValue: 'Facture Ventes'},
      {value: 'BonLivraison', viewValue: 'Bon Livraison'},
      {value: 'ReçuPayement', viewValue: 'Reçu Payement'},
      {value: 'Autre', viewValue: 'Autre'},

    ];
     constructor(  private factureVentesService: FactureVenteService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {

        this.factureVentesService.getDocumentFactureVentes(this.data.codeDocumentFactureVentes).subscribe((doc) => {
            this.documentFactureVentes = doc;
     });
    }
    
    updateDocumentFactureVentes() 
    {
      this.updateEnCours = true;
       this.factureVentesService.updateDocumentFactureVentes(this.documentFactureVentes).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.openSnackBar(  'Document Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
     imageFace1Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentFactureVentes.imageFace1 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imageFace2Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentFactureVentes.imageFace2 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
  }
