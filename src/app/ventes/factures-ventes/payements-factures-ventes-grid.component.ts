
import { Component, OnInit, Inject, ViewChild, Input, AfterViewChecked, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {FactureVenteService} from './facture-vente.service';
import { FactureVente } from './facture-vente.model';
import {PayementFactureVentes} from './payement-facture-ventes.model';
import { PayementFactureVentesElement } from './payement-facture-ventes.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
 import { DialogAddPayementFactureVentesComponent} from './popups/dialog-add-payement-facture-ventes.component';
 import { DialogEditPayementFactureVentesComponent } from './popups/dialog-edit-payement-facture-ventes.component';
@Component({
    selector: 'app-payements-factures-ventes-grid',
    templateUrl: 'payements-factures-ventes-grid.component.html',
  })
  export class PayementsFacturesVentesGridComponent implements OnInit, OnChanges {
     private listPayementsFacturesVentes: PayementFactureVentes[] = [];
     private  dataPayementsFacturesVentesSource: MatTableDataSource<PayementFactureVentesElement>;
     private selection = new SelectionModel<PayementFactureVentesElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];

     @Input()
     private factureVentes: FactureVente;

     constructor(  private factureVenteService: FactureVenteService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      // console.log('credit code after dipslay tab ', this.credit.code);
      this.specifyListColumnsToBeDisplayed();
      
      this.factureVenteService.getPayementsFacturesVentesByCodeFacture(this.factureVentes.code).subscribe((listPayementsFactVentes) => {
        this.listPayementsFacturesVentes = listPayementsFactVentes;
// console.log('recieving data from backend', this.listPayementsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }

  ngOnChanges() {
    this.factureVenteService.getPayementsFacturesVentesByCodeFacture(this.factureVentes.code).subscribe((listPayementsFactVentes) => {
      this.listPayementsFacturesVentes = listPayementsFactVentes;
// console.log('recieving data from backend', this.listPayementsCredits  );
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }
  checkOneActionInvoked() {
    const listSelectedPayementsFactVentes: PayementFactureVentes[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((payementFactElement) => {
    return payementFactElement.code;
  });
  this.listPayementsFacturesVentes.forEach(payementF => {
    if (codes.findIndex(code => code === payementF.code) > -1) {
      listSelectedPayementsFactVentes.push(payementF);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listPayementsFacturesVentes !== undefined) {

      this.listPayementsFacturesVentes.forEach(payementFactVentes => {
             listElements.push( {code: payementFactVentes.code ,
          dateOperation: payementFactVentes.dateOperation,
          montantPaye: payementFactVentes.montantPaye,
          modePayement: payementFactVentes.modePayement,
          numeroCheque: payementFactVentes.numeroCheque,
          dateCheque: payementFactVentes.dateCheque,
          compte: payementFactVentes.compte,
          compteAdversaire: payementFactVentes.compteAdversaire,
          banque: payementFactVentes.banque,
          banqueAdversaire: payementFactVentes.banqueAdversaire,
          description: payementFactVentes.description
        } );
      });
    }
      this.dataPayementsFacturesVentesSource = new MatTableDataSource<PayementFactureVentesElement>(listElements);
      this.dataPayementsFacturesVentesSource.sort = this.sort;
      this.dataPayementsFacturesVentesSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'}, 
     {name: 'montantPaye' , displayTitle: 'Montant Payé'},
     {name: 'modePayement' , displayTitle: 'Mode Payement'},
      {name: 'numeroCheque' , displayTitle: 'N°Chèque'},
     {name: 'dateCheque' , displayTitle: 'Date Chèque'}, 
     {name: 'compte' , displayTitle: 'Compte'},
     {name: 'compteAdversaire' , displayTitle: 'Compte Adv.'},
      {name: 'banque' , displayTitle: 'Banque'},
       { name : 'banqueAdversaire' , displayTitle : 'Banque Adv.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataPayementsFacturesVentesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataPayementsFacturesVentesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataPayementsFacturesVentesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataPayementsFacturesVentesSource.paginator) {
      this.dataPayementsFacturesVentesSource.paginator.firstPage();
    }
  }

  showAddPayementDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeFactureVentes : this.factureVentes.code
   };
 
   const dialogRef = this.dialog.open(DialogAddPayementFactureVentesComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(payAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditPayementDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codePayementFactureVentes: this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditPayementFactureVentesComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(payAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Paiement Séléctionnée!');
    }
    }
    supprimerPayements()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(pay, index) {
              this.factureVenteService.deletePayementFactureVentes(pay.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( this.selection.selected.length + ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Paiement Séléctionnée!');
        }
    }
    refresh() {
      this.factureVenteService.getPayementsFacturesVentesByCodeFacture(this.factureVentes.code).subscribe((listPayementsFacs) => {
        this.listPayementsFacturesVentes = listPayementsFacs;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
















}
