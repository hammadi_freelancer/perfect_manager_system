
export class FactureVentesDataFilter {
    numeroFactureVente: string ;
    nomClient: string ;
    prenomClient: string;
    cinClient: string;
    raisonClient: string;
    matriculeClient : string;
    registreClient : string;
    dateFacturationFromObject : Date;
    dateFacturationToObject : Date;
    dateVenteFromObject : Date;
    dateVenteToObject : Date;
    montantAPayer : number;
    modePaiement : string;
    
    etat : string;
        }



        