import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {FactureVente} from './facture-vente.model';

import {FactureVenteService} from './facture-vente.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
// import { CommunicationService} from './communication.service';
import { MatDatepickerInput } from '@angular/material/datepicker';
import { MatSnackBar } from '@angular/material';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import { ColumnDefinition } from '../../common/column-definition.interface';
import { FactureVenteFilter } from './facture-vente.filter';
import {Client} from '../clients/client.model';
import {ArticleService} from '../../stocks/articles/article.service';
import {Article} from '../../stocks/articles/article.model';
import { MatExpansionPanel } from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { LigneFactureVente } from './ligne-facture-vente.model';
import {LigneFactureVenteElement} from '../factures-ventes/ligne-facture-vente.interface';
import {SelectionModel} from '@angular/cdk/collections';
import { ArticleFilter } from './article.filter';
import {Magasin} from '../../stocks/magasins/magasin.model';
import {UnMesure} from '../../stocks/un-mesures/un-mesure.model';
import {ModePayementLibelle} from './mode-payement-libelle.interface';
import {MagasinFilter} from '../../stocks/magasins/magasin.filter';
import {UnMesureFilter} from '../../stocks/un-mesures/un-mesures.filter';
import { ClientFilter } from '../clients/client.filter';

@Component({
  selector: 'app-facture-vente-edit',
  templateUrl: './facture-vente-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class FactureVenteEditComponent implements OnInit  {

  private factureVente: FactureVente =  new FactureVente();
  private updateEnCours = false;
  private tabsDisabled = true;
  private managedLigneFactureVentes = new LigneFactureVente();
  private selection = new SelectionModel<LigneFactureVenteElement>(true, []);
  @ViewChild(MatPaginator) paginator: MatPaginator; 
  @ViewChild(MatSort) sort: MatSort;
  private columnsDefinitions: ColumnDefinition[] = [];
  private columnsTitles: string[] = [];
  private columnsTitlesAr: string[] = [];
  private columnsNames: string[] = [];
  
@Input()
private listFacturesVentes: any = [] ;

@ViewChild('firstMatExpansionPanel')
private firstMatExpansionPanel : MatExpansionPanel;

@ViewChild('manageLigneMatExpansionPanel')
private manageLigneMatExpansionPanel : MatExpansionPanel;

@ViewChild('listLignesMatExpansionPanel')
private listLignesMatExpansionPanel : MatExpansionPanel;

private lignesFactures: LigneFactureVente[] = [ ];
private articleFilter: ArticleFilter;
private magasinFilter: MagasinFilter;
private unMesureFilter: UnMesureFilter;
private clientFilter: ClientFilter;

private listClients: Client[];
 private listArticles: Article[];
 private listMagasins: Magasin[];
private listLignesFactureVentes: LigneFactureVente[] = [ ];
private  dataLignesFactureVentesSource: MatTableDataSource<LigneFactureVenteElement>;

// private editLignesMode: boolean [] = [];

private etatsFactureVentes: any[] = [
  {value: 'Annule', viewValue: 'Annulé'},
  {value: 'Initial', viewValue: 'Initial'},
  {value: 'Valide', viewValue: 'Validé'}
];
private etatsLigneFactureVentes: any[] = [
  {value: 'Regle', viewValue: 'Réglée'},
  {value: 'NonRegle', viewValue: 'NonRéglée'},
  {value: 'PartiellementRegle', viewValue: 'Partiellement Réglée'}
];
private payementModes: any[] = [
  {value: 'Comptant', viewValue: 'Comptant'},
  {value: 'Cheque', viewValue: 'Chèque'},
  {value: 'Credit', viewValue: 'Crédit'},
  {value: 'Avance', viewValue: 'Avance'},
  {value: 'Comptant_Cheque', viewValue: 'Comptant/Chèque'},
  {value: 'Comptant_Avance', viewValue: 'Comptant/Avance'},
  {value: 'Credit_Comptant', viewValue: 'Crédit/Comptant'},
  {value: 'Credit_Cheque', viewValue: 'Crédit/Chèque'},
  {value: 'Credit_Avance', viewValue: 'Crédit/Avance'},
  {value: 'Donnation', viewValue: 'Amicalité'}
];
  constructor( private route: ActivatedRoute,
    private router: Router, private factureVenteService: FactureVenteService,
    private articleService: ArticleService, private dialog: MatDialog,
    private matSnackBar: MatSnackBar, private elRef: ElementRef,
   ) {
  }
  ngOnInit() {
    this.listClients = this.route.snapshot.data.clients;
   this.listArticles = this.route.snapshot.data.articles;
   this.listMagasins = this.route.snapshot.data.magasins;
    this.clientFilter  = new ClientFilter(this.listClients );

    this.articleFilter = new ArticleFilter(this.route.snapshot.data.articles,
      [] , []
     );

     this.magasinFilter  = new MagasinFilter( this.route.snapshot.data.magasins);
     this.unMesureFilter  = new UnMesureFilter( this.route.snapshot.data.unMesures);
    const codeFactureVentes = this.route.snapshot.paramMap.get('codeFactureVentes');
    console.log('code facture sent to the server is :', codeFactureVentes);
    this.specifyListColumnsToBeDisplayed();
    this.factureVenteService.getFactureVente(codeFactureVentes).subscribe((factureVente) => {
             this.factureVente = factureVente;
             console.log('facture ventes is ');
             console.log(this.factureVente.listLignesFactures);
             
             if (this.factureVente.etat === 'Valide')
              {
                this.tabsDisabled = false;
              }
             if (this.factureVente.etat === 'Valide' || this.factureVente.etat === 'Annule')
              {
                this.etatsFactureVentes.splice(1, 1);
              }
              if (this.factureVente.etat === 'Initial')
                {
                  this.etatsFactureVentes.splice(0, 1);
                }
                this.transformDataToByConsumedByMatTable();
                
            });
            this.firstMatExpansionPanel.open();
           
            
  }
  refreshFactureVente()
  {
    
    this.transformDataToByConsumedByMatTable();
    
  }
  loadGridFacturesVentes() {
    this.router.navigate(['/pms/ventes/facturesVentes']) ;
  }
updateFactureVente() {

  console.log(this.factureVente);
  let noClient = false;
  let noRow = false;
  if (this.factureVente.etat === 'Valide' )  {
    if ( this.factureVente.client.cin === null ||  this.factureVente.client.cin === undefined)  {
        noClient = true;
        
      }
  if (this.factureVente.listLignesFactures[0] === undefined
    || this.factureVente.listLignesFactures[0].quantite === 0  ) {
      noRow = true;
    }
  }
    if (noClient)
      {
        this.openSnackBar( 'Vous Venez de valider la facture sans préciser les informations de client! ');
        this.factureVente.etat = 'Initial';
      } else if (noRow)  {
          this.openSnackBar( 'La Facture doit avoir au moins une ligne valide !');
          
        } else {
  this.updateEnCours = true;
  this.factureVenteService.updateFactureVente(this.factureVente).subscribe((response) => {
    this.updateEnCours = false;
    if (response.error_message ===  undefined) {
      console.log('the response of update is ', response );
      if (this.factureVente.etat === 'Valide')
        {
          this.tabsDisabled = false;
          this.etatsFactureVentes = [
            {value: 'Annule', viewValue: 'Annulé'},
            {value: 'Valide', viewValue: 'Validé'},
            {value: 'Reglee', viewValue: 'Réglée'},
            {value: 'Valide', viewValue: 'Partiellement Réglée'},
            
  
          ];
        }
        if (this.factureVente.etat === 'Annule')
          {
            this.tabsDisabled = true;
         
          }

      this.openSnackBar( ' Facture de Vente mise à jour ');
    
    } else {
      this.openSnackBar( ' Erreurs:Opération Echouée ');
     // show error message
    }
});
        }
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 


  calculMontantAPayer()
  {
  this.factureVente.montantAPayer = +this.factureVente.timbreFiscale + this.factureVente.montantTTC;
  
  return this.factureVente.montantAPayer ;
  }
  calculMontantApayerApresRemise()
  {
    
  this.factureVente.montantAPayerApresRemise = +this.factureVente.montantAPayer - this.factureVente.montantRemise;
    return this.factureVente.montantAPayerApresRemise;
  }
claculMontantRemise() {
  const prixVente   =  +(+this.managedLigneFactureVentes.montantTTC / +this.managedLigneFactureVentes.quantite)
  ;
  if (!isNaN(prixVente)) {
  console.log(prixVente);
this.managedLigneFactureVentes.montantRemise = +this.managedLigneFactureVentes.remise  * ( +prixVente / 100) *
 this.managedLigneFactureVentes.quantite;
// console.log(this.lignesFactures[i].montantRemise);
  }
return this.managedLigneFactureVentes.montantRemise;
}
calculMontantHT()
{
this.managedLigneFactureVentes.montantHT = +this.managedLigneFactureVentes.prixUnitaire * this.managedLigneFactureVentes.quantite;
return this.managedLigneFactureVentes.montantHT;
}
calculMontantTTC()
{
  this.managedLigneFactureVentes.montantTTC = +(+this.managedLigneFactureVentes.prixUnitaire * this.managedLigneFactureVentes.quantite)
  + this.managedLigneFactureVentes.montantTVA;
  
  return  this.managedLigneFactureVentes.montantTTC;
}
calculMontantTVA() {

  this.managedLigneFactureVentes.montantTVA =  (this.managedLigneFactureVentes.prixUnitaire / 100) *
  this.managedLigneFactureVentes.tva * this.managedLigneFactureVentes.quantite;
  this.managedLigneFactureVentes.montantTTC = +(+this.managedLigneFactureVentes.prixUnitaire * this.managedLigneFactureVentes.quantite)
  + this.managedLigneFactureVentes.montantTVA;
  // console.log('montant tva');
 // console.log(this.factureVente.montantTVA + this.listLignesFactureVentes[i].montantTVA);
 if(this.managedLigneFactureVentes.remise !== 0) {
  this.claculMontantRemise() ;
}  
  return  this.managedLigneFactureVentes.montantTVA;
}

calculMontantBenefice() {
  
      this.managedLigneFactureVentes.montantBenefice =  (this.managedLigneFactureVentes.prixUnitaire / 100) *
      this.managedLigneFactureVentes.benefice * this.managedLigneFactureVentes.quantite;
      return  this.managedLigneFactureVentes.montantBenefice;
    }


calculMontant4thTaxe() {
      
          this.managedLigneFactureVentes.montantOtherTaxe2 =  (this.managedLigneFactureVentes.prixUnitaire / 100) *
          this.managedLigneFactureVentes.otherTaxe2 * this.managedLigneFactureVentes.quantite;
          if(this.managedLigneFactureVentes.montantTTC === 0 ) {
            
     this.managedLigneFactureVentes.montantTTC = +(+this.managedLigneFactureVentes.prixUnitaire * this.managedLigneFactureVentes.quantite)
          + this.managedLigneFactureVentes.montantOtherTaxe2;
          } else {
            this.managedLigneFactureVentes.montantTTC = + this.managedLigneFactureVentes.montantTTC
            + this.managedLigneFactureVentes.montantOtherTaxe2;
          }

          if(this.managedLigneFactureVentes.remise !== 0) {
            this.claculMontantRemise() ;
          }  
          return  this.managedLigneFactureVentes.montantOtherTaxe2;
        }

calculMontant3rdTaxe() {
          
              this.managedLigneFactureVentes.montantOtherTaxe1 =  (this.managedLigneFactureVentes.prixUnitaire / 100) *
              this.managedLigneFactureVentes.otherTaxe1 * this.managedLigneFactureVentes.quantite;
             if(this.managedLigneFactureVentes.montantTTC === 0 ) {
      this.managedLigneFactureVentes.montantTTC = +(+this.managedLigneFactureVentes.prixUnitaire
        * this.managedLigneFactureVentes.quantite)
              + this.managedLigneFactureVentes.montantOtherTaxe1;
               } else {
                this.managedLigneFactureVentes.montantTTC = + this.managedLigneFactureVentes.montantTTC
                        + this.managedLigneFactureVentes.montantOtherTaxe1;
               }
               if(this.managedLigneFactureVentes.remise !== 0) {
                this.claculMontantRemise() ;
              }  
              return  this.managedLigneFactureVentes.montantOtherTaxe1;
            }

    calculMontantFodec() {
              
           this.managedLigneFactureVentes.montantFodec =  (this.managedLigneFactureVentes.prixUnitaire / 100) *
                  this.managedLigneFactureVentes.fodec * this.managedLigneFactureVentes.quantite;
                  if(this.managedLigneFactureVentes.montantTTC === 0 ) {
                    
                  this.managedLigneFactureVentes.montantTTC = +(+this.managedLigneFactureVentes.prixUnitaire *
                     this.managedLigneFactureVentes.quantite)
                  + this.managedLigneFactureVentes.montantFodec;
                  } else  {
                    this.managedLigneFactureVentes.montantTTC = + this.managedLigneFactureVentes.montantTTC
                    + this.managedLigneFactureVentes.montantFodec;
                  }
                  if(this.managedLigneFactureVentes.remise !== 0) {
                    this.claculMontantRemise() ;
                  }  
                  return  this.managedLigneFactureVentes.montantFodec;
       }
     transformDataToByConsumedByMatTable() {
                  const listElements = [];
                  if (this.factureVente.listLignesFactures !== undefined) {
                    this.factureVente.listLignesFactures.forEach(ligneFact => {
                           listElements.push( {code: ligneFact.code ,
                            numeroLigneFactureVentes: ligneFact.numeroLigneFactureVentes,
                            article: ligneFact.article.code,
                             magasin: ligneFact.magasin.code,
                             unMesure: ligneFact.unMesure.code,
                             quantite: ligneFact.quantite,
                             prixUnitaire: ligneFact.prixUnitaire,
                             tva: ligneFact.tva,
                             montantTVA: ligneFact.montantTVA,
                             remise: ligneFact.remise,
                             montantRemise: ligneFact.montantRemise,
                             montantHT: ligneFact.montantHT,
                             montantTTC: ligneFact.montantTTC,
                             description: ligneFact.description,
                             
                             etat: ligneFact.etat
                      } );
                    });
                  }
                    this.dataLignesFactureVentesSource= new MatTableDataSource<LigneFactureVenteElement>(listElements);
                    this.dataLignesFactureVentesSource.sort = this.sort;
                    this.dataLignesFactureVentesSource.paginator = this.paginator;
                    
                    
                }
        specifyListColumnsToBeDisplayed() {
                   // console.log('calling specifyListColumnsToBeDisplayed documents grid');
                  this.columnsTitles = [];
                  this.columnsNames = [];
                  this.columnsTitlesAr  = [];
                  
                   this.columnsDefinitions = [{name: 'numeroLigneFactureVentes' , displayTitle: 'N°'},
                   {name: 'article' , displayTitle: 'Article'},
                   {name: 'magasin' , displayTitle: 'Magasin'},
                   {name: 'unMesure' , displayTitle: 'Un Mesure'},
                   {name: 'quantite' , displayTitle: 'Quantite'},
                   {name: 'prixUnitaire' , displayTitle: 'Prix Unitaire'},
                   {name: 'tva' , displayTitle: 'TVA'},
                   {name: 'montantTVA' , displayTitle: 'Montant TVA'},
                   {name: 'montantHT' , displayTitle: 'Montant HT'},
                   { name : 'montantTTC' , displayTitle : 'Montant TTC'},
                  
                    ];
                
                   this.columnsTitles = this.columnsDefinitions.map((colDef) => {
                             return colDef.displayTitle;
                   });
                   
                   this.columnsNames[0] = 'select';
                   this.columnsDefinitions.map((colDef) => {
                    this.columnsNames.push(colDef.name);
                    console.log(this.columnsNames);
                });
                   this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
                    return colDef.displayTitleAr;
                });
                }

 
      getIndexOfRow(row): number
      {
      
          for ( let i = 0; i < this.factureVente.listLignesFactures.length ; i++) {
            if (this.factureVente.listLignesFactures[i].numeroLigneFactureVentes === row.numeroLigneFactureVentes)
             {
                   return i;
              }
          }
          return -1;
      }
      reglerLigneFactureVentes(row)
      {
        this.managedLigneFactureVentes= this.mapRowGridToObjectLigneFacture(row);
        this.managedLigneFactureVentes.etat = 'Regle';
        const index = this.getIndexOfRow(row);
        this.factureVente.listLignesFactures[index] = this.managedLigneFactureVentes;
      }

      mapRowGridToObjectLigneFacture(rowGrid): LigneFactureVente
      {
        const ligneFV  = new  LigneFactureVente() ;
        ligneFV.numeroLigneFactureVentes = rowGrid.numeroLigneFactureVentes;
        ligneFV.article.code =  rowGrid.article;
        ligneFV.magasin.code =  rowGrid.magasin;
        ligneFV.unMesure.code =  rowGrid.unMesure;
        ligneFV.quantite =  rowGrid.quantite;
        ligneFV.prixUnitaire =  rowGrid.prixUnitaire;
        ligneFV.montantHT =  rowGrid.montantHT;
        ligneFV.montantTTC =  rowGrid.montantTTC;
        ligneFV.tva =  rowGrid.tva;
        ligneFV.montantTVA =  rowGrid.montantTVA;
        ligneFV.fodec =  rowGrid.fodec;
        ligneFV.montantFodec =  rowGrid.montantFodec;
        ligneFV.remise =  rowGrid.remise;
        ligneFV.montantRemise =  rowGrid.montantRemise;
        ligneFV.etat = rowGrid.etat;
        ligneFV.otherTaxe1 =  rowGrid.otherTaxe1;
        ligneFV.montantOtherTaxe1 =  rowGrid.montantOtherTaxe1;
        ligneFV.otherTaxe2 =  rowGrid.otherTaxe2;
        ligneFV.montantOtherTaxe2 =  rowGrid.montantOtherTaxe2;
        ligneFV.montantBenefice =  rowGrid.montantBenefice;
        ligneFV.benefice =  rowGrid.benefice;
        ligneFV.description =  rowGrid.description;
        
        return ligneFV;
      }
      editLigneFactureVentes(row)
      {
        this.managedLigneFactureVentes= this.mapRowGridToObjectLigneFacture(row);
      
        if(row.magasin === undefined)
          {
            this.managedLigneFactureVentes.magasin = new Magasin();
          } else {
            this.fillDataLibelleMagasin();
          }
          if(row.unMesure === undefined)
            {
              this.managedLigneFactureVentes.unMesure = new UnMesure();
            }
            if(row.article === undefined)
              {
                this.managedLigneFactureVentes.article = new Article();
              } else {
                this.fillDataLibelleArticle();
              }
              console.log('row to edit is ', row);
      
        this.manageLigneMatExpansionPanel.open();
        
      }
      deleteLigneFactureVentes(row)
      {
      
        const indexOfRow = this.getIndexOfRow(row);
        this.factureVente.listLignesFactures.splice(indexOfRow , 1 );
        this.transformDataToByConsumedByMatTable();
        
      }
      addLigneFactureVentes()
      {
        if (this.managedLigneFactureVentes.quantite === undefined
          || this.managedLigneFactureVentes.quantite === 0 ||
          this.managedLigneFactureVentes.quantite === null ||
          this.managedLigneFactureVentes.prixUnitaire === undefined
            || this.managedLigneFactureVentes.prixUnitaire === 0 ||
            this.managedLigneFactureVentes.prixUnitaire === null  ||
            this.managedLigneFactureVentes.article.code === undefined  ||
            this.managedLigneFactureVentes.article.code === '' ||
            this.managedLigneFactureVentes.article.code === null 
        ) {
              this.openSnackBar('Donnnées Manquées (QTE/ART/PUNIT!');
          } else if (this.managedLigneFactureVentes.numeroLigneFactureVentes === undefined
        || this.managedLigneFactureVentes.numeroLigneFactureVentes === '' ||
        this.managedLigneFactureVentes.numeroLigneFactureVentes === null ) {
            this.openSnackBar('Numéro de Ligne Non Spécifié !');
          } else {
        this.managedLigneFactureVentes.numeroFactureVentes = this.factureVente.numeroFactureVente;
        if (this.factureVente.listLignesFactures.length === 0) {
        this.factureVente.listLignesFactures.push(this.managedLigneFactureVentes);
        } else  {
          console.log('look for index:');
           const indexOfRow = this.getIndexOfRow(this.managedLigneFactureVentes);
           console.log(indexOfRow);
           
           {
                 if (indexOfRow === -1) {
          this.factureVente.listLignesFactures.push(this.managedLigneFactureVentes);
      
               } else  {
           this.factureVente.listLignesFactures[indexOfRow] = this.managedLigneFactureVentes;
      
             }
            }
        }
      
        this.transformDataToByConsumedByMatTable() ;
        this.managedLigneFactureVentes = new LigneFactureVente();
        this.manageLigneMatExpansionPanel.close();
        this.listLignesMatExpansionPanel.open();
      }
      }

      fillDataLibelleUnMesure()
      {
    
      }
      fillDataLibelleMagasin()
      {
        this.listMagasins.forEach((magasin, index) => {
          if (this.managedLigneFactureVentes.magasin.code === magasin.code) {
           this.managedLigneFactureVentes.magasin.description = magasin.description;
           this.managedLigneFactureVentes.magasin.adresse = magasin.adresse;
           
           }
    
     } , this);
      }
      fillDataLibelleArticle()  {

        this.listArticles.forEach((article, index) => {
             if (this.managedLigneFactureVentes.article.code === article.code) {
              this.managedLigneFactureVentes.article.designation = article.designation;
              this.managedLigneFactureVentes.article.libelle = article.libelle;
              
              }

        } , this);
      }

      applyFilter(filterValue: string) {
        this.dataLignesFactureVentesSource.filter = filterValue.trim().toLowerCase();
        if (this.dataLignesFactureVentesSource.paginator) {
          this.dataLignesFactureVentesSource.paginator.firstPage();
        }
      }

      selectedTabChanged($event) {
        if ($event.index === 4) { // Composition tab was selected
         // this.loadListArticles();
         } else if ($event.index === 1) {
          //  this.loadListSchemasConfiguration() ;
         }
        }
/*addPayement()
{
 if ( this.factureVente.etat === 'Valide') {
 const dialogConfig = new MatDialogConfig();
 
 dialogConfig.disableClose = false;
 dialogConfig.autoFocus = true;
 dialogConfig.position = {
   top : '0'
 };
 dialogConfig.data = {
   codeFactureVentes : this.factureVente.code
 };
 const dialogRef = this.dialog.open(DialogAddPayementFactureVentesComponent,
   dialogConfig
 );
} else {
 this.openSnackBar('La Facture  n\'est pas encore validée ');
}
}
listerPayements()
{
  if ( this.factureVente.etat === 'Valide') {
    const dialogConfig = new MatDialogConfig();
    
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.position = {
      top : '0'
    };
    dialogConfig.data = {
      codeFactureVentes : this.factureVente.code
    };
   
    const dialogRef = this.dialog.open(DialogListPayementsFactureVentesComponent,
      dialogConfig
    );
   } else {
    this.openSnackBar('La Facture n\'est pas encore validée ');
   }
}*/




/*deleteRow(i)
{
  
  this.factureVente.montantHT = +this.factureVente.montantHT - this.lignesFactures[i].montantHT;
  this.factureVente.montantTVA = +this.factureVente.montantTVA - this.lignesFactures[i].montantTVA ;
  this.factureVente.montantTTC = +this.factureVente.montantTTC  - this.lignesFactures[i].montantTTC ;

  this.factureVente.montantRemise = +this.factureVente.montantRemise  - this.lignesFactures[i].montantRemise ;
    this.factureVente.montantAPayer = this.factureVente.montantTTC ;
    this.factureVente.montantAPayerApresRemise = +this.factureVente.montantAPayer - this.factureVente.montantRemise;
    console.log('after delete');
    console.log( this.factureVente.montantHT);
    console.log( this.factureVente.montantTTC);
    this.lignesFactures.splice(i, 1 );
    this.editLignesMode.splice(i, 1); 
    this.readOnlyLignes.splice(i, 1); 
    
    this.lignesFactures.push(new LigneFactureVente());
    this.editLignesMode.push(false);
    this.readOnlyLignes.push(false);
    
       
}
recalculMontants()
{
  if ( this.pagesLignesFacures[this.currentPage - 1] === undefined) {
    this.pagesLignesFacures[this.currentPage - 1]  =  this.lignesFactures;
  }
    
  this.factureVente.montantHT = Number(0);
  this.factureVente.montantTVA = Number(0);
  this.factureVente.montantTTC = Number(0);
  this.factureVente.montantRemise = Number(0);
  this.factureVente.montantAPayer = Number(0);
  this.factureVente.montantAPayerApresRemise = Number(0);
  this.pagesLignesFacures.forEach(function(page , index) {
    page.forEach(function(ligne, i) {
      this.factureVente.montantHT = +this.factureVente.montantHT + ligne.montantHT ;
      this.factureVente.montantTVA = +this.factureVente.montantTVA + ligne.montantTVA ;
      this.factureVente.montantTTC = +this.factureVente.montantTTC  + ligne.montantTTC ;
      this.factureVente.montantRemise = +this.factureVente.montantRemise  + ligne.montantRemise ;
      this.factureVente.montantAPayer = this.factureVente.montantTTC ;
      this.factureVente.montantAPayerApresRemise = +this.factureVente.montantAPayer - this.factureVente.montantRemise;
      // this.editLignesMode[i] = true;
     // this.readOnlyLignes[i] = true;
    }, this);
}, this);

}
validateRow(i)
{
   this.factureVente.montantHT = +this.factureVente.montantHT + this.lignesFactures[i].montantHT ;
   this.factureVente.montantTVA = +this.factureVente.montantTVA + this.lignesFactures[i].montantTVA ;
   this.factureVente.montantTTC = +this.factureVente.montantTTC  + this.lignesFactures[i].montantTTC ;
   this.factureVente.montantRemise = +this.factureVente.montantRemise  + this.lignesFactures[i].montantRemise ;
   this.factureVente.montantAPayer = this.factureVente.montantTTC ;
   this.factureVente.montantAPayerApresRemise = +this.factureVente.montantAPayer - this.factureVente.montantRemise;
   this.editLignesMode[i] = true;
   this.readOnlyLignes[i] = true;
  }
  editRow(i)
  {
    if (this.readOnlyLignes[i])
    {
      this.readOnlyLignes[i] = false;
    }  else {
     // this.validateRow(i);

      this.recalculMontants();
      this.readOnlyLignes[i] = true;
      
    }
     
  }*/
 /* supprimerFactureVente() {
      this.factureVenteService.deleteFactureVente(this.factureVente.code).subscribe((response) => {

        this.openSnackBar( ' Element Supprimé');
        this.loadGridFacturesVentes() ;
            });
  }*/
   
  /*loadAddFactureVenteComponent() {
    this.router.navigate(['/pms/ventes/ajouterFactureVente']) ;
  }*/
}
