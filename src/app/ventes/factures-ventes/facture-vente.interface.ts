export interface FactureVenteElement {
        code: string;
         numeroFactureVente: string ;
         modePayement: string ;
         dateFacturation: string ;
         dateVente: string;
         cin: string ;
         nom: string;
         prenom: string;
         matriculeFiscale: string;
         raisonSociale: string;
         montantAPayer: number;
         montantTTC: number;
         montantHT: number;
         montantTVA: number;
         etat: string;
        }
