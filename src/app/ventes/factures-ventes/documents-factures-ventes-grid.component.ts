
import { Component, OnInit, Inject,Input, ViewChild, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {FactureVenteService} from './facture-vente.service';
import {FactureVente} from './facture-vente.model';

import {DocumentFactureVentes} from './document-facture-ventes.model';
import { DocumentFactureVentesElement } from './document-facture-ventes.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogAddDocumentFactureVentesComponent } from './popups/dialog-add-document-facture-ventes.component';
 import { DialogEditDocumentFactureVentesComponent } from './popups/dialog-edit-document-facture-ventes.component';
@Component({
    selector: 'app-documents-factures-ventes-grid',
    templateUrl: 'documents-factures-ventes-grid.component.html',
  })
  export class DocumentsFactureVentesGridComponent implements OnInit, OnChanges {
     private listDocumentsFacturesVentes: DocumentFactureVentes[] = [];
     private  dataDocumentsFacturesVentesSource: MatTableDataSource<DocumentFactureVentesElement>;
     private selection = new SelectionModel<DocumentFactureVentesElement>(true, []);
    
     @Input()
     private factureVentes : FactureVente;

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private factureVenteService: FactureVenteService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.factureVenteService.getDocumentsFacturesVentesByCodeFacture(this.factureVentes.code).subscribe((listDocumentsFactVentes) => {
        this.listDocumentsFacturesVentes= listDocumentsFactVentes;
 //  console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsFacturesVentes  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
  }
  ngOnChanges()
  {
    this.specifyListColumnsToBeDisplayed();
    
    this.factureVenteService.getDocumentsFacturesVentesByCodeFacture(this.factureVentes.code).subscribe((listDocumentsFactVentes) => {
      this.listDocumentsFacturesVentes = listDocumentsFactVentes;
// console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsCredits  );
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }

  checkOneActionInvoked() {
    const listSelectedDocumentsFactVentes: DocumentFactureVentes[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentFactElement) => {
    return documentFactElement.code;
  });
  this.listDocumentsFacturesVentes.forEach(documentF => {
    if (codes.findIndex(code => code === documentF.code) > -1) {
      listSelectedDocumentsFactVentes.push(documentF);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsFacturesVentes !== undefined) {

      this.listDocumentsFacturesVentes.forEach(docFact => {
             listElements.push( {code: docFact.code ,
          dateReception: docFact.dateReception,
          numeroDocument: docFact.numeroDocument,
          typeDocument: docFact.typeDocument,
          description: docFact.description
        } );
      });
    }
      this.dataDocumentsFacturesVentesSource = new MatTableDataSource<DocumentFactureVentesElement>(listElements);
      this.dataDocumentsFacturesVentesSource.sort = this.sort;
      this.dataDocumentsFacturesVentesSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     console.log('calling specifyListColumnsToBeDisplayed documents grid');
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
    
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
      console.log(this.columnsNames);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsFacturesVentesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsFacturesVentesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsFacturesVentesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsFacturesVentesSource.paginator) {
      this.dataDocumentsFacturesVentesSource.paginator.firstPage();
    }
  }
  showAddDocumentDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeFactureVentes : this.factureVentes.code
   };
 
   const dialogRef = this.dialog.open(DialogAddDocumentFactureVentesComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(docAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditDocumentDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeDocumentFactureVentes : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditDocumentFactureVentesComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(docAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Document Séléctionnée!');
    }
    }
    supprimerDocuments()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(doc, index) {
              this.factureVenteService.deleteDocumentFactureVentes(doc.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( this.selection.selected.length + ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Document Séléctionnée!');
        }
    }
    refresh() {
      this.factureVenteService.getDocumentsFacturesVentesByCodeFacture(this.factureVentes.code).subscribe((listDocsFactVentes) => {
        this.listDocumentsFacturesVentes = listDocsFactVentes;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
