
import { BaseEntity } from '../../common/base-entity.model' ;
// import { Tranche } from './tranche.model' ;
import { Client } from '../clients/client.model' ;
import { PayementData } from './payement-data.model';
import { LigneFactureVente } from './ligne-facture-vente.model';
import { Credit } from '../credits/credit.model';

type  EtatFacture= 'Reglee'| 'Non Reglee' | 'Partiellement Reglee' |'Initial' |'Valide' | 'Annule' | '';


export class FactureVente extends BaseEntity {
    constructor(public code?: string, public client?: Client,
         public montantAPayer?: number,
        public montantTTC?: number, public montantHT?: number, 
        public  montantRemise?: number,
        public montantTVA?: number,
        public numeroFactureVente?: string,
        public payementData?: PayementData,
        public credit?: Credit,
        public dateFacturationObject?: Date,
         public dateFacturation?: string,
         public dateVenteObject?: Date,
         public dateVente?: string,
          public etat?: EtatFacture,
         public montantRecu?: number,
         public montantAPayerApresRemise?: number,
         public timbreFiscale?: number ,

     public listLignesFactures?: LigneFactureVente[]
     ) {
         super();
         this.client = new Client();
         this.credit = Credit.constructDefaultInstance();
         this.montantAPayer =  Number(0);
         this.montantAPayerApresRemise = Number(0);
         this.montantHT = Number( 0);
         this.montantTTC  = Number(0);
         this.montantTVA = Number(0);
         this.montantRemise = Number(0);
         this.timbreFiscale = Number(0);
         this.payementData = new PayementData();
         this.listLignesFactures = [] ;
         // this.categoryClient = 'PERSONNE_PHYSIQUE';
    }
}
