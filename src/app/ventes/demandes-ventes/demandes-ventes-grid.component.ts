import { Component, OnInit, Output,ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {DemandeVentes} from './demande-ventes.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {DemandeVentesService} from './demande-ventes.service';
// import { ActionData } from './action-data.interface';
import { Client } from '../../ventes/clients/client.model';
import { DemandeVentesElement } from './demande-ventes.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DialogAddClientComponent } from '../clients/dialog-add-client.component';
import { DialogAddPayementDemandeVentesComponent} from './popups/dialog-add-payement-demande-ventes.component';
import { SelectItem } from 'primeng/api';
import { DemandeVentesDataFilter} from './demande-ventes-data.filter';

@Component({
  selector: 'app-demandes-ventes-grid',
  templateUrl: './demandes-ventes-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class DemandesVentesGridComponent implements OnInit {

  private demandeVentes: DemandeVentes =  new DemandeVentes();
  private selection = new SelectionModel<DemandeVentesElement>(true, []);
  private demandeVentesDataFilter = new DemandeVentesDataFilter();
  private showSelectColumnsMultiSelect = false;
  private showFilter = false;
  private showFilterClient = false;
@Output()
select: EventEmitter<DemandeVentes[]> = new EventEmitter<DemandeVentes[]>();

//private lastClientAdded: Client;

 selectedDemandeVentes: DemandeVentes ;

 

 @Input()
 private listDemandesVentes: any = [] ;
 private checkedAll: false;

 private  dataDemandesVentesSource: MatTableDataSource<DemandeVentesElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay :ColumnDefinition[] = [];

 private columnsToSelect : SelectItem[];
  private  selectedColumnsDefinitions: string[];

 private selectedColumnsNames: string[];

 private modesPaiementsDemandeVentes: any[] = [
  {value: 'Comptant', viewValue: 'Comptant'},
  {value: 'Cheque', viewValue: 'Chèque'},
  {value: 'Credit', viewValue: 'Crédit'},
  {value: 'Avance', viewValue: 'Avance'},
  {value: 'Comptant_Cheque', viewValue: 'Comptant/Chèque'},
  {value: 'Comptant_Avance', viewValue: 'Comptant/Avance'},
  {value: 'Credit_Comptant', viewValue: 'Crédit/Comptant'},
  {value: 'Credit_Cheque', viewValue: 'Crédit/Chèque'},
  {value: 'Credit_Avance', viewValue: 'Crédit/Avance'},
  {value: 'Donnation', viewValue: 'Amicalité'}
];
  constructor( private route: ActivatedRoute, private demandeVentesService: DemandeVentesService,
    private router: Router, private matSnackBar: MatSnackBar,
  private  dialog: MatDialog ) {
  }
  ngOnInit() {
    this.columnsToSelect = [
      {label: 'N° Demande', value: 'numeroDemandeVentes', title: 'N° Demande'},
      {label: 'Date Demande', value: 'dateDemandeVentes', title: 'Date Demande'},
      {label: 'Déscription', value: 'description', title: 'Déscription'},
      {label: 'Budget Préparé', value: 'budgetPrepare', title: 'Budget Préparé'},
      {label: 'Besoin Urgent', value: 'besoinUrgentToutes', title: 'Besoin Urgent'},
      {label: 'Mode Paiement', value: 'modePayementDesire', title: 'Mode Paiement'},
      {label: 'Disponibilité', value: 'dateDisponibilite', title: 'Disponibilité'},
      {label: 'Traité', value: 'traiteToutes', title: 'Traité'},
      {label: 'Rs.Soc.Client', value:  'raisonSociale', title: 'Rs.Soc.Client' },
      {label: 'Matr.Client', value:  'matriculeFiscale', title: 'Matr.Client' },
      {label: 'Reg.Client', value:  'registreCommerce', title: 'Reg.Client' },
      {label: 'CIN Client', value: 'cin', title: 'CIN Client'},
      {label: 'Nom Client', value: 'nom', title: 'Nom Client'},
      {label: 'Prénom Client', value: 'prenom', title: 'Prénom Client'},
      {label: 'Etat', value:  'etat', title: 'Etat' },
  
  ];
  this.selectedColumnsDefinitions = [
    'numeroDemandeVentes',
    'dateDemandeVentes' , 
    'budgetPrepare', 
    'nom' ,
    'prenom',
    'cin', 
    'etat'
    ];
    this.defaultColumnsDefinitions = [
      {name: 'numeroDemandeVentes' , displayTitle: 'N°Demande'},
      {name: 'dateDemandeVentes' , displayTitle: 'Date Demande'},
      {name: 'budgetPrepare' , displayTitle: 'Budget Préparé'},
      {name: 'besoinUrgentToutes' , displayTitle: 'Besoin Urgent'},
      {name: 'traiteToutes' , displayTitle: 'Traitée'},
      {name: 'modePayementDesire' , displayTitle: 'Mode Paiement'},
      {name: 'dateDisponibilite' , displayTitle: 'Disponibilité'},
       {name: 'nom' , displayTitle: 'Nom Four.'},
      {name: 'prenom' , displayTitle: 'Prénom Four.'},
      {name: 'cin' , displayTitle: 'CIN Four.'},
      {name: 'matriculeFiscale' , displayTitle: 'Matr. Four.'},
      {name: 'raisonSociale' , displayTitle: 'Raison Four.'},
      {name: 'registreCommerce' , displayTitle: 'Registre Four.'},
       {name: 'etat', displayTitle: 'Etat' },
      ];

    this.specifyListColumnsToBeDisplayed();
    this.demandeVentesService.getPageDemandesVentes(0, 5, null).subscribe((demandeVentes) => {
      this.listDemandesVentes = demandeVentes;
    this.transformDataToByConsumedByMatTable();
  });


  }
  deleteDemandeVentes(demandeVentes) {
     //  console.log('call delete !', demandeVente );
    this.demandeVentesService.deleteDemandeVentes(demandeVentes.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData(null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.demandeVentesService.getPageDemandesVentes(0, 5, filter).subscribe((demandeVentes) => {
    this.listDemandesVentes = demandeVentes;
    this.transformDataToByConsumedByMatTable();
   });
}


checkOneActionInvoked() {
  const listSelectedDemandesVentes: DemandeVentes[] = [];
console.log('selected clients are ');
console.log(this.selection.selected);
if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((demandeVentesElement) => {
  return demandeVentesElement.code;
});
this.listDemandesVentes.forEach(dVentes => {
  if (codes.findIndex(code => code === dVentes.code) > -1) {
    listSelectedDemandesVentes.push(dVentes);
  }
  });
}
this.select.emit(listSelectedDemandesVentes);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listDemandesVentes !== undefined) {
    this.listDemandesVentes.forEach(demandeVentes => {
      listElements.push( {
         code: demandeVentes.code ,
         numeroDemandeVentes: demandeVentes.numeroDemandeVentes,
         dateDemandeVentes: demandeVentes.dateDemandeVentes,
        cin: demandeVentes.client.cin,
        nom: demandeVentes.client.nom,
         prenom: demandeVentes.client.prenom,
        matriculeFiscale: demandeVentes.client.matriculeFiscale,
        raisonSociale: demandeVentes.client.raisonSociale ,
        budgetPrepare: demandeVentes.budgetPrepare,
       //  montantBeneficeEstime: demandeVentes.montantBeneficeEstime,
        besoinUrgentToutes: demandeVentes.besoinUrgentToutes,
        modePayementDesire: demandeVentes.modePayementDesire,
        dateDisponibilite: demandeVentes.dateDisponibilite,
        traiteToutes: demandeVentes.traiteToutes,
        etat: demandeVentes.etat,
        
    });
  });
    this.dataDemandesVentesSource = new MatTableDataSource<DemandeVentesElement>(listElements);
    this.dataDemandesVentesSource.sort = this.sort;
    this.dataDemandesVentesSource.paginator = this.paginator;
    this.demandeVentesService.getTotalDemandesVentes(this.demandeVentesDataFilter).subscribe((response) => {
      console.log('Total credits is ', response);
      if(this.dataDemandesVentesSource.paginator !== undefined) {
       this.dataDemandesVentesSource.paginator.length = response.totalDemandesVentes;
      }
     });
}
}
specifyListColumnsToBeDisplayed() {
  
   console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
   this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
 ?
    this.defaultColumnsDefinitions : [];
    this.selectedColumnsNames = [];
    
    if (this.selectedColumnsDefinitions !== undefined)
     {
       this.selectedColumnsDefinitions.forEach((colName) => { 
            const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
            const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
            const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
            this.columnsDefinitionsToDisplay.push(colDef);
           });
     }
    this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
   this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
              return colDef.displayTitle;
    });
    console.log('columns to disp:', this.columnsDefinitionsToDisplay);
    this.columnsNames = [];
    this.columnsNames[0] = 'select';
    this.columnsDefinitionsToDisplay .map((colDef) => {
     this.columnsNames.push(colDef.name);
    if (this.selectedColumnsDefinitions !== undefined) {
     this.selectedColumnsNames.push(colDef.displayTitle);
    }
 });
    this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
     return colDef.displayTitleAr;
 });
 }
masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataDemandesVentesSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataDemandesVentesSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataDemandesVentesSource.filter = filterValue.trim().toLowerCase();
  if (this.dataDemandesVentesSource.paginator) {
    this.dataDemandesVentesSource.paginator.firstPage();
  }
}

loadAddDemandeVentesComponent() {
  this.router.navigate(['/pms/ventes/ajouterDemandeVentes']) ;

}
loadEditDemandeVentesComponent() {
  this.router.navigate(['/pms/ventes/editerDemandeVentes', this.selection.selected[ this.selection.selected.length - 1].code]) ;

}
supprimerDemandesVentes()
{

  if ( this.selection.selected.length !== 0) {
      this.selection.selected.forEach(function(demandeVentes, index) {

   this.demandeVentesService.deleteDemandeVentes(demandeVentes.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.openSnackBar(  ' Demande(s) Ventes Supprimé(s)');
            this.selection.selected = [];
            this.loadData();
            }
      }, this);
    } else {
      this.openSnackBar('Aucune Demande Séléctionnée !');
    }
}
refresh() {
  this.loadData(null);
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 addPayement()
 {
  if ( this.selection.selected.length !== 0) {
    
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
    top: '0'
  };
  dialogConfig.data = {
    codeDemandeVentes : this.selection.selected[0].code
  };

  const dialogRef = this.dialog.open(DialogAddPayementDemandeVentesComponent,
    dialogConfig
  );
} else {
  this.openSnackBar('Aucune Demande Séléctionnée !');
}
 }
 history()
 {
   
 }
 addClient()
 {
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
  };

  const dialogRef = this.dialog.open(DialogAddClientComponent,
    dialogConfig
  );
 }
 attacherDocuments()
 {

 }


 changePage($event) {
  console.log('page event is ', $event);
 this.demandeVentesService.getPageDemandesVentes($event.pageIndex, $event.pageSize, this.demandeVentesDataFilter ).
 subscribe((demandesVentes) => {
    this.listDemandesVentes= demandesVentes;
   //  console.log(this.listFacturesVentes);
   // this.transformDataToByConsumedByMatTable();
   const listElements = [];
   this.listDemandesVentes.forEach(demandeVentes => {
    listElements.push( {
      code: demandeVentes.code ,
      numeroDemandeVentes: demandeVentes.numeroDemandeVentes,
      dateDemandeVentes: demandeVentes.dateDemandeVentes,
     cin: demandeVentes.client.cin,
     nom: demandeVentes.client.nom,
      prenom: demandeVentes.client.prenom,
     matriculeFiscale: demandeVentes.client.matriculeFiscale,
     raisonSociale: demandeVentes.client.raisonSociale ,
     budgetPrepare: demandeVentes.budgetPrepare,
    //  montantBeneficeEstime: demandeVentes.montantBeneficeEstime,
     besoinUrgentToutes: demandeVentes.besoinUrgentToutes,
     modePayementDesire: demandeVentes.modePayementDesire,
     dateDisponibilite: demandeVentes.dateDisponibilite,
     traiteToutes: demandeVentes.traiteToutes,
     etat: demandeVentes.etat,
  });
});
     this.dataDemandesVentesSource = new MatTableDataSource<DemandeVentesElement>(listElements);
  
     this.demandeVentesService.getTotalDemandesVentes(this.demandeVentesDataFilter).subscribe((response) => {
      console.log('Total credits is ', response);
       this.dataDemandesVentesSource.paginator.length = response.totalDemandesVentes;
     });

    });
}
toggleFilterPanel() {
  this.showFilter = !this.showFilter;
}
toggleShowColumnsMultiSelect() {
 this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
}
loadFiltredData() {
 console.log('the filter is ', this.demandeVentesDataFilter);
 this.loadData(this.demandeVentesDataFilter);
}
activateFilterClient()
{
  this.showFilterClient = !this.showFilterClient;
}

}
