export interface DemandeVentesElement {
            code: string;
            numeroDemandeVentes: string;
            description: string;
         cin: string;
         nom: string;
          prenom: string;
         matriculeFiscale: string ;
         raisonSociale: string;
         budgetPrepare: number;
         besoinUrgentToutes : boolean;
         modePayementDesire: string;
          dateDemandeVentes: string;
         
         dateDisponibilite?: string;
         traiteToutes: boolean;
         etat: string;
        }
     