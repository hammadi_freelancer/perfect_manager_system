export interface LigneDemandeVentesElement {
   code: string ;
   numLigneDemandeVentes: string ;
   article: string;
   unMesure: string;
   quantite: number;
   prixDesire: number;
   traite: boolean;
   besoinUrgent: boolean;
    dateDisponibilite: string;
    }
