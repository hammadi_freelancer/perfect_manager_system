
import { Component, OnInit, Inject, ViewChild, Input, AfterViewChecked, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {DemandeVentesService} from './demande-ventes.service';
import { DemandeVentes } from './demande-ventes.model';
import {PayementDemandeVentes} from './payement-demande-ventes.model';
import { PayementDemandeVentesElement } from './payement-demande-ventes.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
 import { DialogAddPayementDemandeVentesComponent} from './popups/dialog-add-payement-demande-ventes.component';
 import { DialogEditPayementDemandeVentesComponent } from './popups/dialog-edit-payement-demande-ventes.component';
@Component({
    selector: 'app-payements-demandes-ventes-grid',
    templateUrl: 'payements-demandes-ventes-grid.component.html',
  })
  export class PayementsDemandesVentesGridComponent implements OnInit, OnChanges {
     private listPayementsDemandesVentes: PayementDemandeVentes[] = [];
     private  dataPayementsDemandesVentesSource: MatTableDataSource<PayementDemandeVentesElement>;
     private selection = new SelectionModel<PayementDemandeVentesElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];

     @Input()
     private demandeVentes: DemandeVentes;

     constructor(  private demandeVentesService: DemandeVentesService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      // console.log('credit code after dipslay tab ', this.credit.code);
      this.specifyListColumnsToBeDisplayed();
      this.demandeVentesService.getPayementsDemandesVentesByCodeDemande(this.demandeVentes.code).subscribe((listPayementsRVentes) => {
        this.listPayementsDemandesVentes= listPayementsRVentes;
// console.log('recieving data from backend', this.listPayementsCredits  );
        this.transformDataToByConsumedByMatTable();
         });

  }

  ngOnChanges() {
    this.demandeVentesService.getPayementsDemandesVentesByCodeDemande(this.demandeVentes.code).subscribe((listPayementsRVentes) => {
      this.listPayementsDemandesVentes = listPayementsRVentes;
// console.log('recieving data from backend', this.listPayementsCredits  );
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }
  checkOneActionInvoked() {
    const listSelectedPayementsRelVentes: PayementDemandeVentes[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((payementRelElement) => {
    return payementRelElement.code;
  });
  this.listPayementsDemandesVentes.forEach(payementR => {
    if (codes.findIndex(code => code === payementR.code) > -1) {
      listSelectedPayementsRelVentes.push(payementR);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listPayementsDemandesVentes !== undefined) {

      this.listPayementsDemandesVentes.forEach(payementRelVentes => {
             listElements.push( {code: payementRelVentes.code ,
          dateOperation: payementRelVentes.dateOperation,
          montantPaye: payementRelVentes.montantPaye,
          modePayement: payementRelVentes.modePayement,
          numeroCheque: payementRelVentes.numeroCheque,
          dateCheque: payementRelVentes.dateCheque,
          compte: payementRelVentes.compte,
          compteAdversaire: payementRelVentes.compteAdversaire,
          banque: payementRelVentes.banque,
          banqueAdversaire: payementRelVentes.banqueAdversaire,
          description: payementRelVentes.description
        } );
      });
    }
      this.dataPayementsDemandesVentesSource = new MatTableDataSource<PayementDemandeVentesElement>(listElements);
      this.dataPayementsDemandesVentesSource.sort = this.sort;
      this.dataPayementsDemandesVentesSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'}, 
     {name: 'montantPaye' , displayTitle: 'Montant Payé'},
     {name: 'modePayement' , displayTitle: 'Mode Payement'},
      {name: 'numeroCheque' , displayTitle: 'N°Chèque'},
     {name: 'dateCheque' , displayTitle: 'Date Chèque'}, 
     {name: 'compte' , displayTitle: 'Compte'},
     {name: 'compteAdversaire' , displayTitle: 'Compte Adv.'},
      {name: 'banque' , displayTitle: 'Banque'},
       { name : 'banqueAdversaire' , displayTitle : 'Banque Adv.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataPayementsDemandesVentesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataPayementsDemandesVentesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataPayementsDemandesVentesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataPayementsDemandesVentesSource.paginator) {
      this.dataPayementsDemandesVentesSource.paginator.firstPage();
    }
  }

  showAddPayementDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeDemandeVentes : this.demandeVentes.code
   };
 
   const dialogRef = this.dialog.open(DialogAddPayementDemandeVentesComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(payAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditPayementDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codePayementDemandeVentes: this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditPayementDemandeVentesComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(payAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Paiement Séléctionnée!');
    }
    }
    supprimerPayements()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(pay, index) {
              this.demandeVentesService.deletePayementDemandeVentes(pay.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Paiement Séléctionnée!');
        }
    }
    refresh() {
      this.demandeVentesService.getPayementsDemandesVentesByCodeDemande(this.demandeVentes.code).subscribe((listPayementsRels) => {
        this.listPayementsDemandesVentes = listPayementsRels;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
















}
