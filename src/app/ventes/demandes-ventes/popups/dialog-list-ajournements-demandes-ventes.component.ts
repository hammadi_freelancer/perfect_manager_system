
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {DemandeVentesService} from '../demande-ventes.service';
import {AjournementDemandeVentes} from '../ajournement-demande-ventes.model';
import { AjournementDemandeVentesElement } from '../ajournement-demande-ventes.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-ajournements-demandes-ventes',
    templateUrl: 'dialog-list-ajournements-demandes-ventes.component.html',
  })
  export class DialogListAjournementsDemandesVentesComponent implements OnInit {
     private listAjournementDemandesVentes: AjournementDemandeVentes[] = [];
     private  dataAjournementsDemandesVentesSource: MatTableDataSource<AjournementDemandeVentesElement>;
     private selection = new SelectionModel<AjournementDemandeVentesElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private demandeVentesService: DemandeVentesService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      // console.log('code credit in dialog component is :', this.data.codeCredit);
      this.demandeVentesService.getAjournementsDemandesVentesByCodeDemande(this.data.codeDemandeVentes)
      .subscribe((listAjournementsRelVentes) => {
        this.listAjournementDemandesVentes = listAjournementsRelVentes;
// console.log('recieving data from backend', this.listAjournementCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedAjournementsDemandeVentes: AjournementDemandeVentes[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((ajouRelElement) => {
    return ajouRelElement.code;
  });
  this.listAjournementDemandesVentes.forEach(ajournementRelVentes => {
    if (codes.findIndex(code => code === ajournementRelVentes.code) > -1) {
      listSelectedAjournementsDemandeVentes.push(ajournementRelVentes);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listAjournementDemandesVentes !== undefined) {

      this.listAjournementDemandesVentes.forEach(ajournementRVentes=> {
             listElements.push( {code: ajournementRVentes.code ,
          dateOperation: ajournementRVentes.dateOperation,
          nouvelleDatePayement: ajournementRVentes.nouvelleDatePayement,
          motif: ajournementRVentes.motif,
          description: ajournementRVentes.description
        } );
      });
    }
      this.dataAjournementsDemandesVentesSource = new MatTableDataSource<AjournementDemandeVentesElement>(listElements);
      this.dataAjournementsDemandesVentesSource.sort = this.sort;
      this.dataAjournementsDemandesVentesSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'},
     {name: 'nouvelleDatePayement' , displayTitle: 'N°elle Date Pay.'},
     {name: 'motif' , displayTitle: 'Motif'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataAjournementsDemandesVentesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataAjournementsDemandesVentesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataAjournementsDemandesVentesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataAjournementsDemandesVentesSource.paginator) {
      this.dataAjournementsDemandesVentesSource.paginator.firstPage();
    }
  }

}
