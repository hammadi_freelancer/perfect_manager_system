
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {DemandeVentesService} from '../demande-ventes.service';
import {DemandeVentes} from '../demande-ventes.model';

import { LigneDemandeVentes } from '../ligne-demande-ventes.model';
@Component({
    selector: 'app-dialog-edit-ligne-demande-ventes',
    templateUrl: 'dialog-edit-ligne-demande-ventes.component.html',
  })
  export class DialogEditLigneDemandeVentesComponent implements OnInit {

     private ligneDemandeVentes: LigneDemandeVentes;
     private updateEnCours = false;
     private listCodesArticles: string[];
     private listCodesMagasins: string[];
     private listCodesUnMesures: string[];
     
     private etatsLignes: any[] = [
      {value: 'Annule', viewValue: 'Annulé'},
      {value: 'Initial', viewValue: 'Initial'},
      {value: 'Valide', viewValue: 'Validé'}
    ];
     constructor(  private demandeVentesService: DemandeVentesService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit() {
       /* this.demandeVentesService.get(this.data.codeLigneFactureVentes).subscribe((ligne) => {
            this.ligneFactureVentes = ligne;
            this.listCodesArticles = this.data.listCodesArticles;
            // this.listCodesMagasins = this.data.listCodesMagasins;
            
     });*/
   }
    updateLigneDemandeVentes() {
      //  this.saveEnCours = true;
    /*  this.updateEnCours = true;
       this.factureVentesService.updateLigneFactureVentes(this.ligneFactureVentes).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             // this.payementCredit = new PayementCredit();
             this.openSnackBar(  'Ligne Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });*/
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
