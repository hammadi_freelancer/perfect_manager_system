
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {DemandeVentesService} from '../demande-ventes.service';
import {DocumentDemandeVentes} from '../document-demande-ventes.model';
import { DocumentDemandeVentesElement } from '../document-demande-ventes.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-documents-demandes-ventes',
    templateUrl: 'dialog-list-documents-demandes-ventes.component.html',
  })
  export class DialogListDocumentsDemandesVentesComponent implements OnInit {
     private listDocumentsDemandesVentes: DocumentDemandeVentes[] = [];
     private  dataDocumentsDemandesVentes: MatTableDataSource<DocumentDemandeVentesElement>;
     private selection = new SelectionModel<DocumentDemandeVentesElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private demandeVentesService: DemandeVentesService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
     // console.log('code credit in dialog component is :', this.data.codeCredit);
      this.demandeVentesService.getDocumentsDemandeVentesByCodeDemande(this.data.codeDemandeVentes).subscribe((listDocumentRelVentes) => {
        this.listDocumentsDemandesVentes = listDocumentRelVentes;
//console.log('recieving data from backend', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedDocumentsRelVentes: DocumentDemandeVentes[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentRElement) => {
    return documentRElement.code;
  });
  this.listDocumentsDemandesVentes.forEach(documentRA => {
    if (codes.findIndex(code => code === documentRA.code) > -1) {
      listSelectedDocumentsRelVentes.push(documentRA);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsDemandesVentes!== undefined) {

      this.listDocumentsDemandesVentes.forEach(docRVentes => {
             listElements.push( {code: docRVentes.code ,
          dateReception: docRVentes.dateReception,
          numeroDocument: docRVentes.numeroDocument,
          typeDocument: docRVentes.typeDocument,
          description: docRVentes.description
        } );
      });
    }
      this.dataDocumentsDemandesVentes= new MatTableDataSource<DocumentDemandeVentesElement>(listElements);
      this.dataDocumentsDemandesVentes.sort = this.sort;
      this.dataDocumentsDemandesVentes.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsDemandesVentes.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsDemandesVentes.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsDemandesVentes.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsDemandesVentes.paginator) {
      this.dataDocumentsDemandesVentes.paginator.firstPage();
    }
  }

}
