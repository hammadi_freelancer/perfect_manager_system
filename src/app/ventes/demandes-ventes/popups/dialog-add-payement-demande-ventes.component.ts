
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {DemandeVentesService} from '../demande-ventes.service';
import {PayementDemandeVentes} from '../payement-demande-ventes.model';

@Component({
    selector: 'app-dialog-add-payement-demande-ventes',
    templateUrl: 'dialog-add-payement-demande-ventes.component.html',
  })
  export class DialogAddPayementDemandeVentesComponent implements OnInit {
     private payementDemandeVentes: PayementDemandeVentes;
     
     constructor(  private  demandeVentesService: DemandeVentesService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.payementDemandeVentes = new PayementDemandeVentes();
    }
    savePayementDemandeVentes() 
    {
      this.payementDemandeVentes.codeDemandeVentes= this.data.codeDemandeVentes;
      // console.log(this.payementFactureVentes);
      //  this.saveEnCours = true;
       this.demandeVentesService.addPayementDemandeVentes(this.payementDemandeVentes).subscribe((response) => {
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.payementDemandeVentes = new PayementDemandeVentes();
             this.openSnackBar(  'Payement Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
