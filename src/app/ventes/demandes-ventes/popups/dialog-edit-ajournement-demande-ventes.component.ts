
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {DemandeVentesService} from '../demande-ventes.service';
import {AjournementDemandeVentes} from '../ajournement-demande-ventes.model';

@Component({
    selector: 'app-dialog-edit-ajournement-demande-ventes',
    templateUrl: 'dialog-edit-ajournement-demande-ventes.component.html',
  })
  export class DialogEditAjournementDemandeVentesComponent implements OnInit {
     private ajournementDemandeVentes: AjournementDemandeVentes;
     private updateEnCours = false;
     
     constructor(  private demandeVentesService: DemandeVentesService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {

        this.demandeVentesService.getAjournementDemandeVentes(this.data.codeAjournementDemandeVentes).subscribe((ajour) => {
            this.ajournementDemandeVentes = ajour;
     });


    }
    updateAjournementDemandeVentes() 
    {
     this.updateEnCours = true;
       this.demandeVentesService.updateAjournementDemandeVentes(this.ajournementDemandeVentes).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.openSnackBar(  'Ajournement Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
