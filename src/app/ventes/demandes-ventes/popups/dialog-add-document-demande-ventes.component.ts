
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {DemandeVentesService} from '../demande-ventes.service';
import {DemandeVentes} from '../demande-ventes.model';
import { DocumentDemandeVentes } from '../document-demande-ventes.model';
@Component({
    selector: 'app-dialog-add-document-demande-ventes',
    templateUrl: 'dialog-add-document-demande-ventes.component.html',
  })
  export class DialogAddDocumentDemandeVentesComponent implements OnInit {
     private documentDemandeVentes: DocumentDemandeVentes;
     private saveEnCours = false;
     private typesDoc: any[] = [
      {value: 'Cheque', viewValue: 'Chèque'},
      {value: 'Traite', viewValue: 'Traite'},
      {value: 'FactureVentes', viewValue: 'Facture Ventes'},
      {value: 'BonReception', viewValue: 'Bon Réception'},
      {value: 'ReçuPayement', viewValue: 'Reçu Payement'},
      {value: 'Autre', viewValue: 'Autre'},

    ];
     constructor(  private demandeVentesService: DemandeVentesService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.documentDemandeVentes = new DocumentDemandeVentes();
    }
    saveDocumentDemandeVentes() 
    {
      this.documentDemandeVentes.codeDemandeVentes = this.data.codeDemandeVentes;
       // console.log(this.documentCredit);
        this.saveEnCours = true;
       this.demandeVentesService.addDocumentDemandeVentes(this.documentDemandeVentes).
       subscribe((response) => {
         this.saveEnCours = false;
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.documentDemandeVentes = new DocumentDemandeVentes();
             this.openSnackBar(  'Document Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
     imageFace1Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentDemandeVentes.imageFace1 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imageFace2Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentDemandeVentes.imageFace2 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
  }
