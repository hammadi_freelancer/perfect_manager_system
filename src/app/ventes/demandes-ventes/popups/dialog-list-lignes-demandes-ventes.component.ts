
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {DemandeVentesService} from '../demande-ventes.service';
import {LigneDemandeVentes} from '../ligne-demande-ventes.model';
import { LigneDemandeVentesElement } from '../ligne-demande-ventes.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-lignes-demande-ventes',
    templateUrl: 'dialog-list-lignes-demandes-ventes.component.html',
  })
  export class DialogListLignesDemandesVentesComponent implements OnInit {
     private listLignesDemandesVentes: LigneDemandeVentes[] = [];
     private  dataLignesDemandeVentesSource: MatTableDataSource<LigneDemandeVentesElement>;
     private selection = new SelectionModel<LigneDemandeVentesElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private demandeVentesService: DemandeVentesService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      // console.log('code credit in dialog component is :', this.data.codeFactureVentes);
    /*  this.demandeVentesService.get(this.data.codeFactureVentes)
      .subscribe((listLignesFactVentes) => {
        this.listLignesFactureVentes = listLignesFactVentes;
// console.log('recieving data from backend', this.listPayementsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });*/

  }


  checkOneActionInvoked() {
    const listSelectedLignesDemandeVentes: LigneDemandeVentes[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((ligneDemandeVentesElement) => {
    return ligneDemandeVentesElement.code;
  });
  this.listLignesDemandesVentes.forEach(ligneRVentes => {
    if (codes.findIndex(code => code === ligneRVentes.code) > -1) {
      listSelectedLignesDemandeVentes.push(ligneRVentes);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listLignesDemandesVentes!== undefined) {
      this.listLignesDemandesVentes.forEach(ligneDem => {
             listElements.push( {code: ligneDem.code ,
              numLigneDemandeVentes: ligneDem.numLigneDemandeVentes,
              article: ligneDem.article.code,
               unMesure: ligneDem.unMesure.code,
               quantite: ligneDem.quantite,
               prixDesire: ligneDem.prixDesire,
               besoinUrgent: ligneDem.besoinUrgent,
               traite: ligneDem.traite,
               dateDisponibilite: ligneDem.dateDisponibilite,
        } );
      });
    }
      this.dataLignesDemandeVentesSource= new MatTableDataSource<LigneDemandeVentesElement>(listElements);
      this.dataLignesDemandeVentesSource.sort = this.sort;
      this.dataLignesDemandeVentesSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsDefinitions = [{name: 'numLigneDemandeVentes' , displayTitle: 'N°'},
    {name: 'article' , displayTitle: 'Art.'},
    {name: 'unMesure' , displayTitle: 'Un.Mes.'},
    {name: 'quantite' , displayTitle: 'Qté'},
    {name: 'prixDesire' , displayTitle: 'P.Désiré'},
    {name: 'besoinUrgent' , displayTitle: 'Prioritaire'},
    {name: 'traite' , displayTitle: 'Traite'},
    {name: 'dateDisponibilite' , displayTitle: 'Date Disp.'},
     ];
 
     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataLignesDemandeVentesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataLignesDemandeVentesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataLignesDemandeVentesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataLignesDemandeVentesSource.paginator) {
      this.dataLignesDemandeVentesSource.paginator.firstPage();
    }
  }

}
