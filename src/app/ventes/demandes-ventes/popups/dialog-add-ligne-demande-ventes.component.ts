
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {DemandeVentesService} from '../demande-ventes.service';
import {PayementDemandeVentes} from '../payement-demande-ventes.model';
import {LigneDemandeVentes} from '../ligne-demande-ventes.model';

@Component({
    selector: 'app-dialog-add-ligne-demande-ventes',
    templateUrl: 'dialog-add-ligne-demande-ventes.component.html',
  })
  export class DialogAddLigneDemandeVentesComponent implements OnInit {
     private ligneDemandeVentes: LigneDemandeVentes;
     private listCodesArticles: string[];
     private listCodesMagasins: string[];
     private listCodesUnMesures: string[];
     
     private etatsLignes: any[] = [
      {value: 'Annule', viewValue: 'Annulé'},
      {value: 'Initial', viewValue: 'Initial'},
      {value: 'Valide', viewValue: 'Validé'}
    ];
     constructor(  private  demandeVentesService: DemandeVentesService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.ligneDemandeVentes = new LigneDemandeVentes();
           this.listCodesArticles = this.data.listCodesArticles;
           // this.listCodesMagasins = this.data.listCodesMagasins;
           
          // this.ligneDemandeVentes.etat = 'Initial';
           
    }
    saveLigneDemandeVentes() 
    {
      this.ligneDemandeVentes.codeDemandeVentes = this.data.codeDemandeVentes;
      //  console.log(this.ligneFactureVentes);
      //  this.saveEnCours = true;
/*    this.demandeVentesService.addLigne(this.ligneDemandeVentes).subscribe((response) => {
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.ligneDemandeVentes = new LigneDemandeVente();
             this.openSnackBar(  'Ligne Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });*/
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
