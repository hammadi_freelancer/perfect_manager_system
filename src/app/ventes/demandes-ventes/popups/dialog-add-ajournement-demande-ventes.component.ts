
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {DemandeVentesService} from '../demande-ventes.service';
import {AjournementDemandeVentes} from '../ajournement-demande-ventes.model';

@Component({
    selector: 'app-dialog-add-ajournement-demande-ventes',
    templateUrl: 'dialog-add-ajournement-demande-ventes.component.html',
  })
  export class DialogAddAjournementDemandeVentesComponent implements OnInit {
     private ajournementDemandeVentes: AjournementDemandeVentes;
     private saveEnCours = false;
     constructor(  private demandeVentesService: DemandeVentesService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.ajournementDemandeVentes = new AjournementDemandeVentes;
    }
    saveAjournementDemandeVentes() 
    {
      this.ajournementDemandeVentes.codeDemandeVentes = this.data.codeDemandeVentes;
      this.saveEnCours = true;
      // console.log(this.ajournementCredit);
      //  this.saveEnCours = true;
       this.demandeVentesService.addAjournementDemandeVentes(this.ajournementDemandeVentes).subscribe((response) => {
        this.saveEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.ajournementDemandeVentes = new AjournementDemandeVentes();
             this.openSnackBar(  'Ajournement Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
