
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {DemandeVentesService} from '../demande-ventes.service';
import {PayementDemandeVentes} from '../payement-demande-ventes.model';
import { PayementDemandeVentesElement } from '../payement-demande-ventes.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-payements-demandes-ventes',
    templateUrl: 'dialog-list-payements-demandes-ventes.component.html',
  })
  export class DialogListPayementsDemandesVentesComponent implements OnInit {
     private listPayementsDemandeVentes: PayementDemandeVentes[] = [];
     private  dataPayementsDemandeVentesSource: MatTableDataSource<PayementDemandeVentesElement>;
     private selection = new SelectionModel<PayementDemandeVentesElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private demandeVentesService: DemandeVentesService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      // console.log('code credit in dialog component is :', this.data.codeFactureVentes);
      this.demandeVentesService.getPayementsDemandesVentesByCodeDemande(this.data.codeDemandeVentes)
      .subscribe((listPayementsRelVentes) => {
        this.listPayementsDemandeVentes= listPayementsRelVentes;
// console.log('recieving data from backend', this.listPayementsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedPayementsDemandeVentes: PayementDemandeVentes[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((payementRelVentesElement) => {
    return payementRelVentesElement.code;
  });
  this.listPayementsDemandeVentes.forEach(payementRVentes => {
    if (codes.findIndex(code => code === payementRVentes.code) > -1) {
      listSelectedPayementsDemandeVentes.push(payementRVentes);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listPayementsDemandeVentes !== undefined) {

      this.listPayementsDemandeVentes.forEach(payementRelVentes => {
             listElements.push( {code: payementRelVentes.code ,
          dateOperation: payementRelVentes.dateOperation,
          montantPaye: payementRelVentes.montantPaye,
          modePayement: payementRelVentes.modePayement,
          numeroCheque: payementRelVentes.numeroCheque,
          dateCheque: payementRelVentes.dateCheque,
          compte: payementRelVentes.compte,
          compteAdversaire: payementRelVentes.compteAdversaire,
          banque: payementRelVentes.banque,
          banqueAdversaire: payementRelVentes.banqueAdversaire,
          description: payementRelVentes.description
        } );
      });
    }
      this.dataPayementsDemandeVentesSource = new MatTableDataSource<PayementDemandeVentesElement>(listElements);
      this.dataPayementsDemandeVentesSource.sort = this.sort;
      this.dataPayementsDemandeVentesSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'}, 
     {name: 'montantPaye' , displayTitle: 'Montant Payé'},
     {name: 'modePayement' , displayTitle: 'Mode Payement'},
      {name: 'numeroCheque' , displayTitle: 'N°Chèque'},
     {name: 'dateCheque' , displayTitle: 'Date Chèque'}, 
     {name: 'compte' , displayTitle: 'Compte'},
     {name: 'compteAdversaire' , displayTitle: 'Compte Adv.'},
      {name: 'banque' , displayTitle: 'Banque'},
       { name : 'banqueAdversaire' , displayTitle : 'Banque Adv.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataPayementsDemandeVentesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataPayementsDemandeVentesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataPayementsDemandeVentesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataPayementsDemandeVentesSource.paginator) {
      this.dataPayementsDemandeVentesSource.paginator.firstPage();
    }
  }

}
