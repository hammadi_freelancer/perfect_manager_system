
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {DemandeVentesService} from '../demande-ventes.service';
import {DemandeVentes} from '../demande-ventes.model';
import { PayementDemandeVentes } from '../payement-demande-ventes.model';
@Component({
    selector: 'app-dialog-edit-payement-demande-ventes',
    templateUrl: 'dialog-edit-payement-demande-ventes.component.html',
  })
  export class DialogEditPayementDemandeVentesComponent implements OnInit {

     private payementDemandeVentes: PayementDemandeVentes;
     private updateEnCours = false;
     constructor(  private demandeVentesService: DemandeVentesService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit() {
        this.demandeVentesService.getPayementDemandeVentes(this.data.codePayementDemandeVentes).subscribe((payement) => {
            this.payementDemandeVentes = payement;
     });
   }
    updatePayementDemandeVentes() {
      //  this.saveEnCours = true;
      this.updateEnCours = true;
       this.demandeVentesService.updatePayementDemandeVentes(this.payementDemandeVentes).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             // this.payementCredit = new PayementCredit();
             this.openSnackBar(  'Payement Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
