
import { Component, OnInit, Inject, ViewChild, Input, AfterViewChecked, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {DemandeVentesService} from './demande-ventes.service';
import { DemandeVentes } from './demande-ventes.model';
import {LigneDemandeVentes} from './ligne-demande-ventes.model';
import { LigneDemandeVentesElement } from './ligne-demande-ventes.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
 import { DialogAddLigneDemandeVentesComponent} from './popups/dialog-add-ligne-demande-ventes.component';
 import { DialogEditLigneDemandeVentesComponent } from './popups/dialog-edit-ligne-demande-ventes.component';
@Component({
    selector: 'app-lignes-demandes-ventes-grid',
    templateUrl: 'lignes-demandes-ventes-grid.component.html',
  })
  export class LignesDemandesVentesGridComponent implements OnInit, OnChanges {
     private listLignesDemandesVentes: LigneDemandeVentes[] = [];
     private  dataLignesDemandesVentesSource: MatTableDataSource<LigneDemandeVentesElement>;
     private selection = new SelectionModel<LigneDemandeVentesElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];

     @Input()
     private demandeVentes: DemandeVentes;

     constructor(  private demandeVentesService: DemandeVentesService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      // console.log('credit code after dipslay tab ', this.credit.code);
      /*this.demandeVentesService.getLignesR(this.factureVentes.code).subscribe((listLignesFactVentes) => {
        this.listLignesFacturesVentes= listLignesFactVentes;
// console.log('recieving data from backend', this.listPayementsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });*/

  }

  ngOnChanges() {
   /* this.factureVentesService.getLignesFacturesVentesByCodeFacture(this.factureVentes.code).subscribe((listLignesFactVentes) => {
      this.listLignesFacturesVentes= listLignesFactVentes;
// console.log('recieving data from backend', this.listPayementsCredits  );
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });*/
  }
  checkOneActionInvoked() {
    const listSelectedLignesRelVentes: LigneDemandeVentes[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((lFactElement) => {
    return lFactElement.code;
  });
  this.listLignesDemandesVentes.forEach(ligneF => {
    if (codes.findIndex(code => code === ligneF.code) > -1) {
      listSelectedLignesRelVentes.push(ligneF);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listLignesDemandesVentes !== undefined) {
 
      if (this.demandeVentes.listLignesDemandeVentes!== undefined) {
        this.demandeVentes.listLignesDemandeVentes.forEach(ligneDem => {
               listElements.push( {code: ligneDem.code ,
                numLigneDemandeVentes: ligneDem.numLigneDemandeVentes,
                article: ligneDem.article.code,
                 unMesure: ligneDem.unMesure.code,
                 quantite: ligneDem.quantite,
                 prixDesire: ligneDem.prixDesire,
                 besoinUrgent: ligneDem.besoinUrgent,
                 traite: ligneDem.traite,
                 dateDisponibilite: ligneDem.dateDisponibilite,
          } );
        });
      }
    }
      this.dataLignesDemandesVentesSource = new MatTableDataSource<LigneDemandeVentesElement>(listElements);
      this.dataLignesDemandesVentesSource.sort = this.sort;
      this.dataLignesDemandesVentesSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsDefinitions = [{name: 'numLigneDemandeVentes' , displayTitle: 'N°'},
    {name: 'article' , displayTitle: 'Art.'},
    {name: 'unMesure' , displayTitle: 'Un.Mes.'},
    {name: 'quantite' , displayTitle: 'Qté'},
    {name: 'prixDesire' , displayTitle: 'P.Désiré'},
    {name: 'besoinUrgent' , displayTitle: 'Prioritaire'},
    {name: 'traite' , displayTitle: 'Traite'},
    {name: 'dateDisponibilite' , displayTitle: 'Date Disp.'},
     ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataLignesDemandesVentesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataLignesDemandesVentesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataLignesDemandesVentesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataLignesDemandesVentesSource.paginator) {
      this.dataLignesDemandesVentesSource.paginator.firstPage();
    }
  }
  showAddLigneDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeDemandeVentes : this.demandeVentes.code
   };
 
   const dialogRef = this.dialog.open(DialogAddLigneDemandeVentesComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(lAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditLigneDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeLigneDemandeVentes: this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditLigneDemandeVentesComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(lAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucune Ligne Séléctionnée!');
    }
    }
    supprimerLignes()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(pay, index) {
              this.demandeVentesService.deleteLigneDemandeVentes(pay.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar(  ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucune Ligne Séléctionnée!');
        }
    }
    refresh() {
     /* this.demandeVentesService.getLignes(this.factureVentes.code).subscribe((listLignesFacs) => {
        this.listLignesFacturesVentes = listLignesFacs;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });*/
    }
















}
