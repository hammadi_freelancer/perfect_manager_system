
import { Component, OnInit, Inject, ViewChild, Input, OnChanges} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {DemandeVentesService} from './demande-ventes.service';
import { DemandeVentes} from './demande-ventes.model';
import {AjournementDemandeVentes} from './ajournement-demande-ventes.model';
import { AjournementDemandeVentesElement } from './ajournement-demande-ventes.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import { DialogAddAjournementDemandeVentesComponent } from './popups/dialog-add-ajournement-demande-ventes.component';
import { DialogEditAjournementDemandeVentesComponent} from './popups/dialog-edit-ajournement-demande-ventes.component';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-ajournements-demandes-ventes-grid',
    templateUrl: 'ajournements-demandes-ventes-grid.component.html',
  })
  export class AjournementsDemandesVentesGridComponent implements OnInit, OnChanges {
     private listAjournementDemandeVentes: AjournementDemandeVentes[] = [];
     private  dataAjournementsDemandesVentesSource: MatTableDataSource<AjournementDemandeVentesElement>;
     private selection = new SelectionModel<AjournementDemandeVentesElement>(true, []);

     @Input()
     private demandeVentes : DemandeVentes;

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private demandeVentesService: DemandeVentesService,
     private matSnackBar: MatSnackBar, public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.demandeVentesService.getAjournementsDemandesVentesByCodeDemande(this.demandeVentes.code).
      subscribe((listAjournementsRelVentes) => {
        this.listAjournementDemandeVentes = listAjournementsRelVentes;
// console.log('recieving data from backend', this.listAjournementsFactureVentes  );
        this.transformDataToByConsumedByMatTable();
         });

  }

ngOnChanges()
{
  this.demandeVentesService.getAjournementsDemandesVentesByCodeDemande
  (this.demandeVentes.code).subscribe((listAjournementsRelVentes) => {
    this.listAjournementDemandeVentes = listAjournementsRelVentes;
//console.log('recieving data from backend', this.listAjournementCredits  );
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
     });
}
  checkOneActionInvoked() {
    const listSelectedAjournementsDemandeVentes: AjournementDemandeVentes[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((ajournementRVentesElement) => {
    return ajournementRVentesElement.code;
  });
  this.listAjournementDemandeVentes.forEach(ajournementRAch => {
    if (codes.findIndex(code => code === ajournementRAch.code) > -1) {
      listSelectedAjournementsDemandeVentes.push(ajournementRAch);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listAjournementDemandeVentes !== undefined) {

      this.listAjournementDemandeVentes.forEach(ajournementRVentes => {
             listElements.push( {code: ajournementRVentes.code ,
          dateOperation: ajournementRVentes.dateOperation,
          nouvelleDatePayement: ajournementRVentes.nouvelleDatePayement,
          motif: ajournementRVentes.motif,
          description: ajournementRVentes.description
        } );
      });
    }
      this.dataAjournementsDemandesVentesSource = new MatTableDataSource<AjournementDemandeVentesElement>(listElements);
      this.dataAjournementsDemandesVentesSource.sort = this.sort;
      this.dataAjournementsDemandesVentesSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'},
     {name: 'nouvelleDatePayement' , displayTitle: 'N°elle Date Pay.'},
     {name: 'motif' , displayTitle: 'Motif'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataAjournementsDemandesVentesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataAjournementsDemandesVentesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataAjournementsDemandesVentesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataAjournementsDemandesVentesSource.paginator) {
      this.dataAjournementsDemandesVentesSource.paginator.firstPage();
    }
  }

  showAddAjournementDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeDemandeVentes: this.demandeVentes.code
   };
 
   const dialogRef = this.dialog.open(DialogAddAjournementDemandeVentesComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(ajAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditAjournementDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeAjournementDemandeVentes : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditAjournementDemandeVentesComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(ajAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Ajournement Séléctionnée!');
    }
    }
    supprimerAjournements()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(ajour, index) {
              this.demandeVentesService.deleteAjournementDemandeVentes(ajour.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar(  ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Ajournement Séléctionnée!');
        }
    }
    refresh() {
      this.demandeVentesService.getAjournementsDemandesVentesByCodeDemande(this.demandeVentes.code).subscribe((listAjourRel) => {
        this.listAjournementDemandeVentes= listAjourRel;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }


}
