import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
// import { CommunicationService} from './communication.service';
import { MatDatepickerInput } from '@angular/material/datepicker';
import { MatSnackBar } from '@angular/material';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {ArticleService} from '../../stocks/articles/article.service';
import {Article} from '../../stocks/articles/article.model';
import { MatExpansionPanel } from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {LigneFactureVenteElement} from '../factures-ventes/ligne-facture-vente.interface';
import {SelectionModel} from '@angular/cdk/collections';
import {Magasin} from '../../stocks/magasins/magasin.model';
import {ModePayementLibelle} from './mode-payement-libelle.interface';
import {FactureVente} from '../factures-ventes/facture-vente.model';
import { ParametresVentes } from '../parametres-ventes.model';

import {FactureVenteService} from '../factures-ventes/facture-vente.service';
import {DemandeVentesService} from './demande-ventes.service';

import {VentesService} from '../ventes.service';
import { ClientService } from '../clients/client.service';
import { ClientFilter } from '../clients/client.filter';
import { ArticleFilter } from './article.filter';
import { UnMesureFilter } from '../../stocks/un-mesures/un-mesures.filter';

import {Client} from '../clients/client.model';
import {UnMesure} from '../../stocks/un-mesures/un-mesure.model';

import {DemandeVentes} from '../demandes-ventes/demande-ventes.model';
import {LigneDemandeVentes} from '../demandes-ventes/ligne-demande-ventes.model';

import {LigneDemandeVentesElement} from '../demandes-ventes/ligne-demande-ventes.interface';

@Component({
  selector: 'app-demande-ventes-edit',
  templateUrl: './demande-ventes-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class DemandeVentesEditComponent implements OnInit  {

  private demandeVentes: DemandeVentes =  new DemandeVentes();
  private updateEnCours = false;
  private tabsDisabled = true;
  private managedLigneDemandeVentes = new LigneDemandeVentes();
  private selection = new SelectionModel<LigneDemandeVentesElement>(true, []);
  @ViewChild(MatPaginator) paginator: MatPaginator; 
  @ViewChild(MatSort) sort: MatSort;
  private columnsDefinitions: ColumnDefinition[] = [];
  private columnsTitles: string[] = [];
  private columnsTitlesAr: string[] = [];
  private columnsNames: string[] = [];
  
@Input()
private listDemandesVentes: any = [] ;

@ViewChild('firstMatExpansionPanel')
private firstMatExpansionPanel : MatExpansionPanel;

@ViewChild('manageLigneMatExpansionPanel')
private manageLigneMatExpansionPanel : MatExpansionPanel;

@ViewChild('listLignesMatExpansionPanel')
private listLignesMatExpansionPanel : MatExpansionPanel;

// private listCodesArticles: string[];
// private lignesFactures: LigneFactureVente[] = [ ];
// private listTranches: Tranche[] = [] ;
// private factureVenteFilter: FactureVenteFilter;
private articleFilter: ArticleFilter;
private listClients: Client[];
private clientFilter: ClientFilter;
private unMesureFilter : UnMesureFilter;
// private listArticles: Article[];
// private listLignesFactureVentes: LigneFactureVente[] = [ ];
private  dataLignesDemandeVentesSource: MatTableDataSource<LigneDemandeVentesElement>;

// private editLignesMode: boolean [] = [];

private etatsDemandeVentes: any[] = [
  {value: 'Annule', viewValue: 'Annulé'},
  {value: 'Initial', viewValue: 'Initial'},
  {value: 'Valide', viewValue: 'Validé'}
];

private payementModes: ModePayementLibelle[] = [
  {value: 'Comptant', viewValue: 'Comptant'},
  {value: 'Cheque', viewValue: 'Chèque'},
  {value: 'Credit', viewValue: 'Crédit'},
  {value: 'Credit_Comptant', viewValue: 'Crédit/Comptant'}
];
  constructor( private route: ActivatedRoute,
    private router: Router, private demandeVentesService: DemandeVentesService,
    private articleService: ArticleService, private dialog: MatDialog,
    private matSnackBar: MatSnackBar, private elRef: ElementRef,
   ) {
  }
  ngOnInit() {
    this.listClients = this.route.snapshot.data.clients;
   
    // this.clients = new ClientFilter(this.listClients);
    this.articleFilter = new ArticleFilter(this.route.snapshot.data.articles,
    [], [] );
     this.unMesureFilter = new UnMesureFilter(this.route.snapshot.data.unMesures);
     this.clientFilter = new ClientFilter(this.route.snapshot.data.clients);
     
    this.articleFilter = new ArticleFilter(this.route.snapshot.data.articles,
      [], []);
  
    const codeDemandeVentes = this.route.snapshot.paramMap.get('codeDemandeVentes');


    this.demandeVentesService.getDemandeVentes(codeDemandeVentes).subscribe((demandeVentes) => {
             this.demandeVentes = demandeVentes;
             if (this.demandeVentes.etat === 'Valide')
              {
                this.tabsDisabled = false;
              }
             if (this.demandeVentes.etat === 'Valide' || this.demandeVentes.etat === 'Annule')
              {
                this.etatsDemandeVentes.splice(1, 1);
              }
              if (this.demandeVentes.etat === 'Initial')
                {
                  this.etatsDemandeVentes.splice(0, 1);
                }
            });
            this.firstMatExpansionPanel.open();

            this.specifyListColumnsToBeDisplayed();
            this.transformDataToByConsumedByMatTable();
        
           this.demandeVentes.etat = 'Initial';
  




            
  }
  loadGridDemandesVentes() {
    this.router.navigate(['/pms/ventes/demandesVentes']) ;
  }
updateDemandeVentes() {

  //console.log(this.factureVente);
  let noClient = false;
  let noRow = false;
  if (this.demandeVentes.etat === 'Valide' )  {
    if ( this.demandeVentes.client.cin === null ||  this.demandeVentes.client.cin === undefined)  {
      noClient = true;
        
      }
  if (this.demandeVentes.listLignesDemandeVentes[0] === undefined
    || this.demandeVentes.listLignesDemandeVentes[0].quantite === 0  ) {
      noRow = true;
    }
  }
        if (noRow)  {
          this.openSnackBar( 'Le Client doit avoir au moins une ligne valide !');
          
        } else {
  this.updateEnCours = true;
  this.demandeVentesService.updateDemandeVentes(this.demandeVentes).subscribe((response) => {
    this.updateEnCours = false;
    if (response.error_message ===  undefined) {
      console.log('the response of update is ', response );
      if (this.demandeVentes.etat === 'Valide')
        {
          this.tabsDisabled = false;
          this.etatsDemandeVentes= [
            {value: 'Annule', viewValue: 'Annulé'},
            {value: 'Valide', viewValue: 'Validé'},
            {value: 'Reglee', viewValue: 'Réglée'},
            {value: 'Valide', viewValue: 'Partiellement Réglée'},
            
  
          ];
        }
        if (this.demandeVentes.etat === 'Annule')
          {
            this.tabsDisabled = true;
         
          }

      this.openSnackBar( ' Demande Ventes mise à jour ');
      //  this.save.emit(response);
      // this.client = new Client();
      // this.modeUpdate = false;
    } else {
      this.openSnackBar( ' Erreurs:Opération Echouée ');
     // show error message
    }
});
        }
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 
 fillDataNomPrenom()
 {
   const listFiltred  = this.listClients.filter((client) => (client.cin === this.demandeVentes.client.cin));
   this.demandeVentes.client.nom = listFiltred[0].nom;
   this.demandeVentes.client.prenom = listFiltred[0].prenom;
 }
 fillDataMatriculeRaison()
 {
   const listFiltred  = this.listClients.filter((client) =>
    (client.registreCommerce === this.demandeVentes.client.registreCommerce));
   this.demandeVentes.client.matriculeFiscale = listFiltred[0].matriculeFiscale;
   this.demandeVentes.client.raisonSociale = listFiltred[0].raisonSociale;
 }


 transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.demandeVentes.listLignesDemandeVentes!== undefined) {
    this.demandeVentes.listLignesDemandeVentes.forEach(ligneDem => {
           listElements.push( {code: ligneDem.code ,
            numLigneDemandeVentes: ligneDem.numLigneDemandeVentes,
            article: ligneDem.article.code,
             unMesure: ligneDem.unMesure.code,
             quantite: ligneDem.quantite,
             prixDesire: ligneDem.prixDesire,
             besoinUrgent: ligneDem.besoinUrgent,
             traite: ligneDem.traite,
             dateDisponibilite: ligneDem.dateDisponibilite,
      } );
    });
  }
    this.dataLignesDemandeVentesSource= new MatTableDataSource<LigneDemandeVentesElement>(listElements);
    this.dataLignesDemandeVentesSource.sort = this.sort;
    this.dataLignesDemandeVentesSource.paginator = this.paginator;
    
    
}
 specifyListColumnsToBeDisplayed() {
   // console.log('calling specifyListColumnsToBeDisplayed documents grid');
  this.columnsTitles = [];
  this.columnsNames = [];
  this.columnsTitlesAr  = [];
  
  this.columnsDefinitions = [{name: 'numLigneDemandeVentes' , displayTitle: 'N°'},
  {name: 'article' , displayTitle: 'Art.'},
  {name: 'unMesure' , displayTitle: 'Un.Mes.'},
  {name: 'quantite' , displayTitle: 'Qté'},
  {name: 'prixDesire' , displayTitle: 'P.Désiré'},
  {name: 'besoinUrgent' , displayTitle: 'Prioritaire'},
  {name: 'traite' , displayTitle: 'Traite'},
  {name: 'dateDisponibilite' , displayTitle: 'Date Disp.'},
   ];

   this.columnsTitles = this.columnsDefinitions.map((colDef) => {
             return colDef.displayTitle;
   });
   
   this.columnsNames[0] = 'select';
   this.columnsDefinitions.map((colDef) => {
    this.columnsNames.push(colDef.name);
    console.log(this.columnsNames);
});
   this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
    return colDef.displayTitleAr;
});
}

applyFilter(filterValue: string) {
  this.dataLignesDemandeVentesSource.filter = filterValue.trim().toLowerCase();
  if (this.dataLignesDemandeVentesSource.paginator) {
    this.dataLignesDemandeVentesSource.paginator.firstPage();
  }
}

checkLigneDemandeVentes()
{

}
getIndexOfRow(row)
{

    for ( let i = 0; i < this.demandeVentes.listLignesDemandeVentes.length ; i++) {
      if (this.demandeVentes.listLignesDemandeVentes[i].numLigneDemandeVentes === row.numLigneDemandeVentes)
       {
             return i;
        }
    }
    return -1;
}
mapRowGridToObjectLigneDemandeVentes(rowGrid): LigneDemandeVentes
{
  
  const ligneDA = new  LigneDemandeVentes() ;
  ligneDA.numeroDemandeVentes = rowGrid.numeroDemandeVentes;
  ligneDA.numLigneDemandeVentes = rowGrid.numLigneDemandeVentes;
  ligneDA.article.code =  rowGrid.article;
  ligneDA.unMesure.code =  rowGrid.unMesure;
  ligneDA.quantite =  rowGrid.quantite;
  ligneDA.prixDesire =  rowGrid.prixDesire;
  ligneDA.besoinUrgent =  rowGrid.besoinUrgent;
  ligneDA.dateDisponibilite =  rowGrid.dateDisponibilite;
  ligneDA.traite =  rowGrid.traite;
  return ligneDA;
}
editLigneDemandeVentes(row)
{
  this.managedLigneDemandeVentes = this.mapRowGridToObjectLigneDemandeVentes(row);


    if(row.unMesure === undefined)
      {
        this.managedLigneDemandeVentes.unMesure = new UnMesure();
      }
      if(row.article === undefined)
        {
          this.managedLigneDemandeVentes.article = new Article();
        }
        console.log('row to edit is ', row);

  this.manageLigneMatExpansionPanel.open();
  
}
deleteLigneDemandeVentes(row)
{

  const indexOfRow = this.getIndexOfRow(row);
  this.demandeVentes.listLignesDemandeVentes.splice(indexOfRow , 1 );
  this.transformDataToByConsumedByMatTable();
  
}
addLigneDemandeVentes()
{
  if (this.managedLigneDemandeVentes.quantite === undefined
    || this.managedLigneDemandeVentes.quantite === 0 ||
    this.managedLigneDemandeVentes.quantite === null ||
      this.managedLigneDemandeVentes.article.code === undefined  ||
      this.managedLigneDemandeVentes.article.code === '' ||
      this.managedLigneDemandeVentes.article.code === null 
  ) {
        this.openSnackBar('Donnnées Manquées (QTE/ART!');
    } else if (this.managedLigneDemandeVentes.numLigneDemandeVentes === undefined
  || this.managedLigneDemandeVentes.numLigneDemandeVentes === '' ||
  this.managedLigneDemandeVentes.numLigneDemandeVentes=== null ) {
      this.openSnackBar('Numéro de Ligne Non Spécifié !');
    } else {
  this.managedLigneDemandeVentes.numeroDemandeVentes = this.demandeVentes.numeroDemandeVentes;
  if (this.demandeVentes.listLignesDemandeVentes.length === 0) {
  this.demandeVentes.listLignesDemandeVentes.push(this.managedLigneDemandeVentes);
  } else  {
    console.log('look for index:');
     const indexOfRow = this.getIndexOfRow(this.managedLigneDemandeVentes);
     console.log(indexOfRow);
     
     {
           if (indexOfRow === -1) {
    this.demandeVentes.listLignesDemandeVentes.push(this.managedLigneDemandeVentes);

         } else  {
     this.demandeVentes.listLignesDemandeVentes[indexOfRow] = this.managedLigneDemandeVentes;

       }
      }
  }

  this.transformDataToByConsumedByMatTable() ;
  this.managedLigneDemandeVentes = new LigneDemandeVentes();
  this.manageLigneMatExpansionPanel.close();
  this.listLignesMatExpansionPanel.open();
}
}
/*calculPrixTotal()
{
  // this.managedLigneDemandeVentes.prixTotal = this.
  this.managedLigneDemandeVentes.prixTotal  = +  this.managedLigneDemandeVentes.prixUnt *
  this.managedLigneDemandeVentes.quantite  ;
  
}*/
selectedTabChanged($event) {
  if ($event.index === 4) { // Composition tab was selected
   // this.loadListArticles();
   } else if ($event.index === 1) {
    //  this.loadListSchemasConfiguration() ;
   }
  }
}
