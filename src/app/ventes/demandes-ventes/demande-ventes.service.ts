import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import {DemandeVentes} from './demande-ventes.model' ;
import {DocumentDemandeVentes} from './document-demande-ventes.model' ;
import {PayementDemandeVentes} from './payement-demande-ventes.model' ;
import {AjournementDemandeVentes} from './ajournement-demande-ventes.model' ;

// import {CreditVentes} from '../credits-ventes/credit-ventes.model' ;
import {LigneDemandeVentes} from './ligne-demande-ventes.model' ;

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_DEMANDE_VENTES = 'http://localhost:8082/ventes/demandesVentes/getDemandesVentes';
const  URL_GET_PAGE_DEMANDE_VENTES = 'http://localhost:8082/ventes/demandesVentes/getPageDemandesVentes';

const URL_GET_DEMANDE_VENTES = 'http://localhost:8082/ventes/demandesVentes/getDemandeVentes';
const  URL_ADD_DEMANDE_VENTES  = 'http://localhost:8082/ventes/demandesVentes/addDemandeVentes';
const  URL_UPDATE_DEMANDE_VENTES  = 'http://localhost:8082/ventes/demandesVentes/updateDemandeVentes';
const  URL_DELETE_DEMANDE_VENTES  = 'http://localhost:8082/ventes/demandesVentes/deleteDemandeVentes';
const  URL_GET_TOTAL_DEMANDE_VENTES = 'http://localhost:8082/ventes/demandesVentes/getTotalDemandesVentes';

const  URL_ADD_PAYEMENT_DEMANDE_VENTES = 'http://localhost:8082/finance/payementsDemandesVentes/addPayementDemandeVentes';
const  URL_GET_LIST_PAYEMENT_DEMANDE_VENTES  = 'http://localhost:8082/finance/payementsDemandesVentes/getPayementsDemandeVentes';
const  URL_DELETE_PAYEMENT_DEMANDE_VENTES  = 'http://localhost:8082/finance/payementsDemandesVentes/deletePayementDemandeVentes';
const  URL_UPDATE_PAYEMENT_DEMANDE_VENTES  = 'http://localhost:8082/finance/payementsDemandesVentes/updatePayementDemandeVentes';
const URL_GET_PAYEMENT_DEMANDE_VENTES  = 'http://localhost:8082/finance/payementsDemandesVentes/getPayementDemandeVentes';
const URL_GET_LIST_PAYEMENT_DEMANDE_VENTES_BY_CODE_DEMANDE =
'http://localhost:8082/finance/payementsDemandesVentes/getPayementsDemandeVentesByCodeDemande';


const  URL_ADD_DOCUMENT_DEMANDE_VENTES = 'http://localhost:8082/ventes/documentsDemandesVentes/addDocumentDemandeVentes';
const  URL_GET_LIST_DOCUMENTS_DEMANDE_VENTES = 'http://localhost:8082/ventes/documentsDemandesVentes/getDocumentsDemandeVentes';
const  URL_DELETE_DOCUMENT_DEMANDE_VENTES= 'http://localhost:8082/ventes/documentsDemandesVentes/deleteDocumentDemandeVentes';
const  URL_UPDATE_DOCUMENT_DEMANDE_VENTES = 'http://localhost:8082/ventes/documentsDemandesVentes/updateDocumentDemandeVentes';
const URL_GET_DOCUMENT_DEMANDE_VENTES = 'http://localhost:8082/ventes/documentsDemandesVentes/getDocumentDemandeVentes';
const URL_GET_LIST_DOCUMENTS_DEMANDE_VENTES_BY_CODE_DEMANDE =
 'http://localhost:8082/ventes/documentsDemandesVentes/getDocumentsDemandeVentesByCodeDemande';

 const  URL_ADD_AJOURNEMENT_DEMANDE_VENTES = 'http://localhost:8082/ventes/ajournementsDemandesVentes/addAjournementDemandeVentes';
 const  URL_GET_LIST_AJOURNEMENT_DEMANDE_VENTES = 'http://localhost:8082/ventes/ajournementsDemandesVentes/getAjournementsDemandeVentes';
 const  URL_DELETE_AJOURNEMENT_DEMANDE_VENTES= 'http://localhost:8082/ventes/ajournementsDemandesVentes/deleteAjournementDemandeVentes';
 const  URL_UPDATE_AJOURNEMENT_DEMANDE_VENTES = 'http://localhost:8082/ventes/ajournementsDemandesVentes/updateAjournementDemandeVentes';
 const URL_GET_AJOURNEMENT_DEMANDE_VENTES = 'http://localhost:8082/ventes/ajournementsDemandesVentes/getAjournementDemandeVentes';
 const URL_GET_LIST_AJOURNEMENT_DEMANDE_VENTES_BY_CODE_DEMANDE =
  'http://localhost:8082/ventes/ajournementsDemandesVentes/getAjournementsDemandeVentesByCodeDemande';



  /*const  URL_ADD_LIGNE_FACTURE_VENTES = 'http://localhost:8082/ventes/lignesFacturesVentes/addLigneFactureVentes';
  const  URL_GET_LIST_LIGNES_FACTURE_VENTES = 'http://localhost:8082/ventes/lignesFacturesVentes/getLignesFactureVentes';
  const  URL_DELETE_LIGNE_FACTURE_VENTES = 'http://localhost:8082/ventes/lignesFacturesVentes/deleteLigneFactureVentes';
  const  URL_UPDATE_LIGNE_FACTURE_VENTES = 'http://localhost:8082/ventes/lignesFacturesVentes/updateLigneFactureVentes';
  const URL_GET_LIGNE_FACTURE_VENTES = 'http://localhost:8082/ventes/lignesFacturesVentes/getLigneFactureVentes';
  const URL_GET_LIST_LIGNES_FACTURE_VENTES_BY_CODE_FACTURE_VENTES =
   'http://localhost:8082/ventes/lignesFacturesVentes/getLignesFactureVentesByCodeFactureVente';*/

@Injectable({
  providedIn: 'root'
})
export class DemandeVentesService {

  constructor(private httpClient: HttpClient) {

   }
   getTotalDemandesVentes(filter): Observable<any> {
    const filterStr = JSON.stringify(filter);
    
    const params = new HttpParams()
   .set('filter', filterStr);
    return this.httpClient
    .get<any>(URL_GET_TOTAL_DEMANDE_VENTES, {params});
   }


   getPageDemandesVentes(pageNumber, nbElementsPerPage, filter): Observable<DemandeVentes[]> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('pageNumber', pageNumber)
    .set('nbElementsPerPage', nbElementsPerPage)
   .set('filter', filterStr);
    return this.httpClient
    .get<DemandeVentes[]>(URL_GET_PAGE_DEMANDE_VENTES, {params});
   }

   getDemandesVentes(): Observable<DemandeVentes[]> {
   
    return this.httpClient
    .get<DemandeVentes[]>(URL_GET_LIST_DEMANDE_VENTES);
   }
   addDemandeVentes(demandeVentes: DemandeVentes): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_DEMANDE_VENTES, demandeVentes);
  }
  updateDemandeVentes(demandeVentes: DemandeVentes): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_DEMANDE_VENTES, demandeVentes);
  }
  deleteDemandeVentes(codeDemandeVentes): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_DEMANDE_VENTES+ '/' + codeDemandeVentes);
  }
  getDemandeVentes(codeDemandeVentes): Observable<DemandeVentes> {
    return this.httpClient.get<DemandeVentes>(URL_GET_DEMANDE_VENTES+ '/' + codeDemandeVentes);
  }


  addPayementDemandeVentes(payementDemandeVentes: PayementDemandeVentes): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_PAYEMENT_DEMANDE_VENTES, payementDemandeVentes);
    
  }
  getPayementsDemandesVentes(): Observable<PayementDemandeVentes[]> {
    return this.httpClient
    .get<PayementDemandeVentes[]>(URL_GET_LIST_DEMANDE_VENTES);
   }
   getPayementDemandeVentes(codePayementDemandeVentes): Observable<PayementDemandeVentes> 
   {
    return this.httpClient.get<PayementDemandeVentes>(URL_GET_PAYEMENT_DEMANDE_VENTES+ '/' + codePayementDemandeVentes);
  }
  deletePayementDemandeVentes(codePayementDemandeVentes): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_PAYEMENT_DEMANDE_VENTES + '/' + codePayementDemandeVentes);
  }
  getPayementsDemandesVentesByCodeDemande(codeDemandeVentes): Observable<PayementDemandeVentes[]> {
    return this.httpClient.get<PayementDemandeVentes[]>(URL_GET_LIST_PAYEMENT_DEMANDE_VENTES_BY_CODE_DEMANDE
       + '/' + codeDemandeVentes);
  }

  updatePayementDemandeVentes(payDemVentes: PayementDemandeVentes): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_PAYEMENT_DEMANDE_VENTES, payDemVentes);
  }

  addDocumentDemandeVentes(docDemandeVentes: DocumentDemandeVentes): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_DOCUMENT_DEMANDE_VENTES, docDemandeVentes);
    
  }
  getDocumentsDemandesVentes(): Observable<DocumentDemandeVentes[]> {
    return this.httpClient
    .get<DocumentDemandeVentes[]>(URL_GET_LIST_DOCUMENTS_DEMANDE_VENTES);
   }
   getDocumentDemandeVentes(codeDocumentDemandeVentes): Observable<DocumentDemandeVentes> {
    return this.httpClient.get<DocumentDemandeVentes>(URL_GET_DOCUMENT_DEMANDE_VENTES + '/' + codeDocumentDemandeVentes);
  }
  deleteDocumentDemandeVentes(codeDocumentDemandeVentes): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_DOCUMENT_DEMANDE_VENTES + '/' + codeDocumentDemandeVentes);
  }
  updateDocumentDemandeVentes(docDemVentes: DocumentDemandeVentes): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_DOCUMENT_DEMANDE_VENTES, docDemVentes);
  }


  getDocumentsDemandeVentesByCodeDemande(codeDemandeVentes): Observable<DocumentDemandeVentes[]> {
    return this.httpClient.get<DocumentDemandeVentes[]>( URL_GET_LIST_DOCUMENTS_DEMANDE_VENTES_BY_CODE_DEMANDE +
       '/' + codeDemandeVentes);
  }

  addAjournementDemandeVentes(ajournDemandeVentes: AjournementDemandeVentes): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_AJOURNEMENT_DEMANDE_VENTES, ajournDemandeVentes);
    
  }
  updateAjournementDemandeVentes(ajournDemandeVentes: AjournementDemandeVentes): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_AJOURNEMENT_DEMANDE_VENTES, ajournDemandeVentes);
  }
  getAjournementsDemandesVentesByCodeDemande(codeDemVentes): Observable<AjournementDemandeVentes[]> {
    return this.httpClient.get<AjournementDemandeVentes[]>(
      URL_GET_LIST_AJOURNEMENT_DEMANDE_VENTES_BY_CODE_DEMANDE +
       '/' + codeDemVentes);
  }
  getAjournementsDemandesVentes(): Observable<AjournementDemandeVentes[]> {
    return this.httpClient
    .get<AjournementDemandeVentes[]>(URL_GET_LIST_AJOURNEMENT_DEMANDE_VENTES);
   }
   getAjournementDemandeVentes(codeAjournementDemandeVentes): Observable<AjournementDemandeVentes> {
    return this.httpClient.get<AjournementDemandeVentes>(URL_GET_AJOURNEMENT_DEMANDE_VENTES + '/' + codeAjournementDemandeVentes);
  }
  deleteAjournementDemandeVentes(codeAjournementDemandeVentes): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_AJOURNEMENT_DEMANDE_VENTES + '/' + codeAjournementDemandeVentes);
  }


  /*addLigneFactureVentes(ligneFactureVente: LigneFactureVente): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_LIGNE_FACTURE_VENTES, ligneFactureVente);
    
  }
  updateLigneFactureVentes(ligneFactureVente: LigneFactureVente): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_LIGNE_FACTURE_VENTES, ligneFactureVente);
  }
  getLignesFacturesVentesByCodeFacture(codeLigneFactureVentes): Observable<LigneFactureVente[]> {
    return this.httpClient.get<LigneFactureVente[]>( URL_GET_LIST_LIGNES_FACTURE_VENTES_BY_CODE_FACTURE_VENTES +
       '/' + codeLigneFactureVentes);
  }
  getLignesFacturesVentes(): Observable<LigneFactureVente[]> {
    return this.httpClient
    .get<LigneFactureVente[]>(URL_GET_LIST_LIGNES_FACTURE_VENTES);
   }
   getLigneFactureVentes(codeLigneFactureVentes): Observable<LigneFactureVente> {
    return this.httpClient.get<LigneFactureVente>(URL_GET_LIST_LIGNES_FACTURE_VENTES + '/' + codeLigneFactureVentes);
  }
  deleteLigneFactureVentes(codeLigneFactureVentes): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_LIGNE_FACTURE_VENTES + '/' + codeLigneFactureVentes);
  }*/
}
