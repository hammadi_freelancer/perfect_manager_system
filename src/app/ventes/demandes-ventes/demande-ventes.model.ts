
import { BaseEntity } from '../../common/base-entity.model' ;
// import { Tranche } from './tranche.model' ;
import { Client } from '../clients/client.model' ;
import { PayementData } from './payement-data.model';
import { LigneDemandeVentes } from './ligne-demande-ventes.model';
//import { Credit } from '../credits/credit.model';
type TypeModePayement = 'Credit'| 'Comptant' | 'Cheque' | '';

type  EtatDemandeVentes= |'Initial' |'Valide' | 'Annule' | '';


export class DemandeVentes extends BaseEntity {
    constructor(
        public code?: string,
        public numeroDemandeVentes?: string,
        public dateDemandeVentes?: string,
        public dateDemandeVentesObject?: string,
        
        public client?: Client,
         public budgetPrepare?: number,
        public dateDisponibiliteObject?: Date,
        public dateDisponibilite?: string,
        public besoinUrgentToutes?: boolean,        
        public modePayementDesire?: TypeModePayement,
         public traiteToutes?: boolean,
          public etat?: EtatDemandeVentes,
     public listLignesDemandeVentes?: LigneDemandeVentes[]
     ) {
         super();
         this.client = new Client();
         this.budgetPrepare = Number(0);
       //  this.montantBeneficeEstime = Number(0);
        // this.payementData = new PayementData();
         this.listLignesDemandeVentes = [] ;
         // this.categoryClient = 'PERSONNE_PHYSIQUE';
    }
}
