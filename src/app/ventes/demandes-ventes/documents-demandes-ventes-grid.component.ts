
import { Component, OnInit, Inject,Input, ViewChild, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {DemandeVentesService} from './demande-ventes.service';
import {DemandeVentes} from './demande-ventes.model';

import {DocumentDemandeVentes} from './document-demande-ventes.model';
import { DocumentDemandeVentesElement } from './document-demande-ventes.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogAddDocumentDemandeVentesComponent } from './popups/dialog-add-document-demande-ventes.component';
 import { DialogEditDocumentDemandeVentesComponent } from './popups/dialog-edit-document-demande-ventes.component';
@Component({
    selector: 'app-documents-demandes-ventes-grid',
    templateUrl: 'documents-demandes-ventes-grid.component.html',
  })
  export class DocumentsDemandeVentesGridComponent implements OnInit, OnChanges {
     private listDocumentsDemandesVentes: DocumentDemandeVentes[] = [];
     private  dataDocumentsDemandesVentesSource: MatTableDataSource<DocumentDemandeVentesElement>;
     private selection = new SelectionModel<DocumentDemandeVentesElement>(true, []);
    
     @Input()
     private demandeVentes : DemandeVentes;

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private demandeVentesService: DemandeVentesService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.demandeVentesService.getDocumentsDemandeVentesByCodeDemande(this.demandeVentes.code).subscribe((listDocumentsRelVentes) => {
        this.listDocumentsDemandesVentes= listDocumentsRelVentes;
 //  console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsFacturesVentes  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
  }
  ngOnChanges()
  {
    this.specifyListColumnsToBeDisplayed();
    
    this.demandeVentesService.getDocumentsDemandeVentesByCodeDemande(this.demandeVentes.code).subscribe((listDocumentsRelVentes) => {
      this.listDocumentsDemandesVentes = listDocumentsRelVentes;
// console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsCredits  );
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }

  checkOneActionInvoked() {
    const listSelectedDocumentsRelVentes: DocumentDemandeVentes[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentRElement) => {
    return documentRElement.code;
  });
  this.listDocumentsDemandesVentes.forEach(documentR => {
    if (codes.findIndex(code => code === documentR.code) > -1) {
      listSelectedDocumentsRelVentes.push(documentR);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsDemandesVentes!== undefined) {

      this.listDocumentsDemandesVentes.forEach(docRel=> {
             listElements.push( {code: docRel.code ,
          dateReception: docRel.dateReception,
          numeroDocument: docRel.numeroDocument,
          typeDocument: docRel.typeDocument,
          description: docRel.description
        } );
      });
    }
      this.dataDocumentsDemandesVentesSource = new MatTableDataSource<DocumentDemandeVentesElement>(listElements);
      this.dataDocumentsDemandesVentesSource.sort = this.sort;
      this.dataDocumentsDemandesVentesSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     console.log('calling specifyListColumnsToBeDisplayed documents grid');
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
    
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
      console.log(this.columnsNames);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsDemandesVentesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsDemandesVentesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsDemandesVentesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsDemandesVentesSource.paginator) {
      this.dataDocumentsDemandesVentesSource.paginator.firstPage();
    }
  }
  showAddDocumentDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeDemandeVentes : this.demandeVentes.code
   };
 
   const dialogRef = this.dialog.open(DialogAddDocumentDemandeVentesComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(docAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditDocumentDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeDocumentDemandeVentes : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditDocumentDemandeVentesComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(docAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Document Séléctionnée!');
    }
    }
    supprimerDocuments()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(doc, index) {
              this.demandeVentesService.deleteDocumentDemandeVentes(doc.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Document Séléctionnée!');
        }
    }
    refresh() {
      this.demandeVentesService.getDocumentsDemandeVentesByCodeDemande(this.demandeVentes.code).subscribe((listDocsRelVentes) => {
        this.listDocumentsDemandesVentes = listDocsRelVentes;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
