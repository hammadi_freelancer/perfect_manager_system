import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { ClientService } from './clients/client.service';
import { Client } from './clients/client.model';

@Injectable()
export class ClientsResolver implements Resolve<Observable<Client[]>> {
  constructor(private clientsService: ClientService) { }

  resolve() {
     return this.clientsService.getClients();
}
}
