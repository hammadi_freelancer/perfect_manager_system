export class Taxe {
    constructor(public code?: string, public libelle?: string, public description?: string,
        public pourcentage?: number, public etat?: string // annule valide
    ) {}
}
