
type VALIDATY_STATE = 'Valide' | 'Annule' | 'En Cours';
export class BaseEntity {
    constructor(
        public  dateCreation?: string, 
        public dateLastUpdate?: string, 
        public userCreatorCode?: string,
        public userLastUpdatorCode?:
         string, public description?: string, 
         public validatiyState?: VALIDATY_STATE // annule valide
    ) {}
}
