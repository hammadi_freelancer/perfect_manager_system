
import { Adresse } from './adresse.interface';
type EtatCivile = 'celibataire' | 'marie' | 'divorce';
export interface  Personne {
     nom?: string ;
    prenom?: string ;
    cin?: string;
    dateNaissance?: string;
    dateIntervention?: string;
    niveauAcademique?: string;
    nbreEnfantsMajeurs?: number;
    nbreEnfantsMineurs?: number;
    nbreEnfantsMasculins?: number;
    nbreEnfantsFemminins?: number;
    nbreEnfantsHandicapes?: number;
    matriculeCNSS?: string;
    etatCivile?: EtatCivile;
    job?: string;
    image?: any;
    pere?: Personne;
     mere?: Personne;
     adresse: Adresse;
     }

