
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import { Setting } from './setting.model';
import { ColumnDefinition } from './column-definition.interface';
import {MatSnackBar} from '@angular/material';
// import {CreditAchatsService} from '../credit-achats.service';
// import {TrancheCreditAchats} from '../tranche-credit-achats.model';
import {CommonService } from './common.service';
@Component({
    selector: 'app-dialog-settings',
    templateUrl: 'dialog-settings.component.html',
  })
  export class DialogSettingsComponent implements OnInit {
    // private trancheCreditAchats: TrancheCreditAchats;
     private saveEnCours = false;
     private settings: Setting[];
     private settingManaged: Setting;
     private sourceListCols: ColumnDefinition[];
     private targetListCols: ColumnDefinition[] = [];
     constructor(  private commonService: CommonService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      this.commonService.getSettingsByEntityName(this.data.entityName).subscribe((settings) => {
        this.settings = settings;
 });
 }
    saveSetting()   {
      //  console.log(this.trancheCredit);
       this.saveEnCours = true;
      //  this.saveEnCours = true;
       this.commonService.addSetting(this.settingManaged).subscribe((response) => {
        this.saveEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
            // this.trancheCreditAchats = new TrancheCreditAchats();
             this.openSnackBar(  'Setting Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
   updateSetting()   {
    //  console.log(this.trancheCredit);
     this.saveEnCours = true;
    //  this.saveEnCours = true;
     this.commonService.updateSetting(this.settingManaged).subscribe((response) => {
      this.saveEnCours = false;
      if (response.error_message ===  undefined) {
           // this.save.emit(response);
          // this.trancheCreditAchats = new TrancheCreditAchats();
           this.openSnackBar(  'Setting Enregistré');
         } else {
          // show error message
          this.openSnackBar(  'Erreurs!');
          
         }
     });
 }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
