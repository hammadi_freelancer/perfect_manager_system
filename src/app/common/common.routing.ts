import { Injectable } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { TaxeComponent } from './taxe.component';
import { TaxesGridComponent } from './taxes-grid.component';
import {ModuleWithProviders  } from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';

 const commonRoute: Routes = [
    {
        path: 'common/ajouterTaxe',
        component: TaxeComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'common/listerTaxes',
        component: TaxesGridComponent,
        data: {
           // authorities: ['ROLE_ASSET_ASSIGNMENT'],
            // pageTitle: 'safeleasegtwApp.asset.home.titleAssign'
        },
        // canActivate: [UserRouteAccessService]
    }
];
export const commonRoutingModule: ModuleWithProviders = RouterModule.forRoot(commonRoute);
// commonRouting.providers = [{ provide: APP_BASE_HREF, useValue: '/common'} ] ;


