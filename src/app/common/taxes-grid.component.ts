import { Component, OnInit } from '@angular/core';
// import { ArticleService } from './article.service';
import {Taxe} from './taxe.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {TaxeService} from './taxe.service';



@Component({
  selector: 'app-taxes-grid',
  templateUrl: './taxes-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class TaxesGridComponent implements OnInit {

private taxe: Taxe = new Taxe();
 listTaxes: any;
 listTaxesOrgin: any;
 filterCIN: any;
  constructor( private route: ActivatedRoute, private taxeService: TaxeService,
    private router: Router ) {
  }
  ngOnInit() {
    this.taxeService.getTaxes().subscribe((taxes) => {
      this.listTaxes = taxes;
      this.listTaxesOrgin = this.listTaxes ;
     });
  }
reloadData() {
    if (this.filterCIN !== undefined && this.filterCIN !== '' ) {
    const th = this;
    this.listTaxes = this.listTaxesOrgin.filter(function(value, index) {
        console.log('the value is :', value.cinClient);
        console.log('the filter is ', th.filterCIN);
        return value.cinClient === th.filterCIN ;
    });
}
}
/*saveP() {
    console.log(this.taxe);
}*/
}
