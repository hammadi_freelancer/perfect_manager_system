

export interface Contact {
    emailPrincipal?: string ;
    emailSecondaire?: string;
    telPrincipal?: string;
    telSecondaire?: string;
    mobilePrincipal?: string;
    mobileSecondaire?: string;
    siteWeb?: string;
        // annule valid
}
