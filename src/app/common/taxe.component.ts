import { Component, OnInit } from '@angular/core';
import {Taxe} from './taxe.model';
import {TaxeService} from './taxe.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';




@Component({
  selector: 'app-taxe',
  templateUrl: './taxe.component.html',
  // styleUrls: ['./players.component.css']
})
export class TaxeComponent implements OnInit {

private taxe: Taxe = new Taxe();
  constructor( private route: ActivatedRoute,
    private router: Router, private taxeService: TaxeService) {
  }
  ngOnInit() {
  }
saveTaxe() {
    console.log(this.taxe);
    this.taxeService.addTaxe(this.taxe).subscribe();
}
}
