

export interface Adresse {
     rue?: string ;
     avenue?: string;
     codePostale?: string;
     ville?: string;
     gouvernorat?: string;
    pays?: string;
    imageMap?: any;
     temporaire?: boolean;
     datePriseEnConsideration?: string;
         // annule valid
}
