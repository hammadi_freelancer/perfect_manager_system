export interface OperationResponse {
      operation_name?: string ;
    operation_options?: string[];
    objects_concerned?: Object[];
}
