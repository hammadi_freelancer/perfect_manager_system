import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';

import {Taxe} from './taxe.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_TAXES = 'http://localhost:8082/common/taxes/getTaxes';
const URL_ADD_TAXE = 'http://localhost:8082/common/taxes/addTaxe';
@Injectable({
  providedIn: 'root'
})
export class TaxeService {

  constructor(private httpClient: HttpClient) {

   }

   getTaxes(): Observable<Taxe[]> {
    return this.httpClient
    .get<Taxe[]>(URL_GET_LIST_TAXES);
   }

   addTaxe(taxe: Taxe): Observable<Taxe> {
    return this.httpClient.post<Taxe>(URL_ADD_TAXE, taxe);
  }
  /* getArticles(): Array<Article> {
    const listArticles = new Array();
    listArticles.push(new Article('WMP', 'PORTABLE TT SAMSUNG', '' , '', 700, 'PORTABLES'  ));
    listArticles.push(new Article('WXP', 'PORTABLE GM SAMSUNG', '' , '', 444, 'PORTABLES'  ));
    listArticles.push(new Article('WRRP', 'PORTABLE Prime SAMSUNG', '' , '', 1000, 'PORTABLES'  ));
    return listArticles;
}*/
}
