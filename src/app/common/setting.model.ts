
type ENTITY_NAME  = 'CLIENTS' | 'FOURNISSEURS' | 'CREDITS_VENTES' | 'CREDITS_ACHATS' |
'FACTURES_VENTES' | 'FACTURES_ACHATS' |
'JOURNAUX' | 'EMPLOYES'| 'FICHES_PAIES' | 'DOCUMENTS_ACHATS' | 'DOCUMENTS_VENTES' | 'ORDRES_ACHATS' | 'ORDRES_VENTES' 
| 'COMMANDES_VENTES' | 'COMMANDES_ACHATS' | 'UTILISATEURS' | 'ORGANISATIONS';
export class Setting {
    constructor(public code?: string,
         public description?: string,
         public entityName?: ENTITY_NAME,
         public isDefault?: boolean,
         public columnsToDisplay?: string[],
         
    ) {}
}
