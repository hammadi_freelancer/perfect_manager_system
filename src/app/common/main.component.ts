import { Component, OnInit, AfterViewInit , ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
// import { DocumentType } from './document-type.model';
// import { DocumentTypeService } from './document-type.service';
// import { DocumentTypesGridComponent} from './documents-types-grid.component';



@Component({
  selector: 'app-main-common',
  templateUrl: './main.component.html',
  // styleUrls: ['./players.component.css']
})
export class MainComponent implements OnInit, AfterViewInit  {

 //  @ViewChild(ClientsGridComponent) clientsGrid;



  constructor( private route: ActivatedRoute,
    private router: Router,
    // private documentTypeService: DocumentTypeService
  ) {
  }
  ngOnInit() {
  }
  ngAfterViewInit() {
    // this.selectedClient = this.clientsGrid.selectedClient;
   //  console.log(this.clientsGrid.selectedClient);
  }
}
