import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';
import { HttpParams } from '@angular/common/http';

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Setting } from './setting.model';

const  URL_ADD_SETTING = 'http://localhost:8082/common/settings/addSetting';
const  URL_GET_SETTING = 'http://localhost:8082/common/settings/getSetting';
const  URL_GET_LIST_SETTINGS_BY_ENTITY_NAME = 'http://localhost:8082/common/settings/getSettingsByEntityName';
const  URL_GET_LIST_SETTINGS = 'http://localhost:8082/common/settings/getSettings';

const  URL_UPDATE_SETTING = 'http://localhost:8082/common/settings/updateSetting';
const  URL_DELETE_SETTING = 'http://localhost:8082/common/settings/deleteSetting';
const  URL_GET_TOTAL_SETTINGS = 'http://localhost:8082/common/settings/getTotalSettings';



@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private httpClient: HttpClient) {

   }
   getTotalSettings(): Observable<any> {
    return this.httpClient
    .get<any>(URL_GET_TOTAL_SETTINGS);
   }
   getSettingsByEntityName(entityName): Observable<Setting[]> {
    return this.httpClient
    .get<Setting[]>(URL_GET_LIST_SETTINGS_BY_ENTITY_NAME + '/' + entityName );
   }
   getSettings(pageNumber, nbElementsPerPage): Observable<Setting[]> {
    const params = new HttpParams()
    .set('pageNumber', pageNumber)
    .set('nbElementsPerPage', nbElementsPerPage)
    .set('sort', 'name');
    return this.httpClient
    .get<Setting[]>(URL_GET_LIST_SETTINGS, {params});
   }
  addSetting(setting: Setting): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_SETTING, setting);
  }
  updateSetting(setting: Setting): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_SETTING, setting);
  }
  getSetting(entityName): Observable<Setting> {
    return this.httpClient.get<Setting>(URL_GET_SETTING + '/' + entityName);
  }
  // getListColumns(entityName)

}
