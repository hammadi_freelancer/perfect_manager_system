import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
// import { PayementComponent } from './payements/payement.component';
// import { CreditComponent } from './credits/credit.component';

// import { CreditsGridComponent } from './credits/credits-grid.component';
// import { PayementsGridComponent } from './payements/payements-grid.component';
// import { GestionCreditsComponent } from './credits/gestion-credits.component';
// import { GestionPayementsComponent } from './payements/gestion-payements.component';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HomeVentesComponent } from './ventes/home-ventes.component';
import { HomeStocksComponent } from './stocks/home-stocks.component';

import { HomeAchatsComponent } from './achats/home-achats.component';
import { HomeUtilisateursComponent } from './utilisateurs/home-utilisateurs.component';
import { HomeOrganisationComponent } from './organisations/home-organisations.component'
import { HomeGroupesComponent } from './groupes/home-groupes.component';
;
import { HomeJournauxComponent } from './journaux/home-journaux.component';
import { HomeComptesClientsComponent } from './comptes-clients/home-comptes-clients.component';
import { HomeComptesFournisseursComponent } from './comptes-fournisseurs/home-comptes-fournisseurs.component';

import { HomeCaisseComponent } from './caisse/home-caisse.component';
import { HomeRHComponent } from './resources-humaines/home-resources-humaines.component';
import { HomePromotionComponent} from './marketing/promotions/home-promotion.component';

import { FournisseursGridComponent } from './achats/fournisseurs/fournisseurs-grid.component';

import {APP_BASE_HREF} from '@angular/common';
import { ventesRoute } from './ventes/ventes.routing';
import { achatsRoute } from './achats/achats.routing';
import { profilesRoute } from './profiles/profiles.routing';

import { stocksRoute} from './stocks/stocks.routing';
import { organisationsRoute} from './organisations/organisations.routing';
import { utilisateursRoute} from './utilisateurs/utilisateurs.routing';
import { groupesRoute} from './groupes/groupes.routing';

import { journauxRoute} from './journaux/journaux.routing';
import { operationsCourantesRoute} from './operations-courantes/operations-courantes.routing';
import { comptesClientsRoute} from './comptes-clients/comptes-clients.routing';
import { comptesFournisseursRoute} from './comptes-fournisseurs/comptes-fournisseurs.routing';
import { livresOresRoute} from './livres-ores/livres-ores.routing';
import { inventairesRoute} from './inventaires/inventaires.routing';

import { caisseRoute} from './caisse/caisse.routing';
import { RHRoute} from './resources-humaines/resources-humaines.routing';
import { reglementsCreditsRoute} from './reglements-credits/reglements-credits.routing';
import { reglementsDettesRoute} from './reglements-dettes/reglements-dettes.routing';

import { historiquesClientsRoute} from './historiques-clients/historiques-clients.routing';
import { historiquesFournisseursRoute} from './historiques-fournisseurs/historiques-fournisseurs.routing';

import { marketingRoute} from './marketing/marketing.routing';

import { AuthGuard} from './auth.guard';
 const appRoute: Routes = [

    {
        path: 'pms',
        component : AppComponent,
        children : [{
             path : '',
             component : HomeComponent,
             children :
            [{
                path : 'ventes',
                component : HomeVentesComponent,
                canActivate : [AuthGuard],
                children: ventesRoute

            },
             {
                path : 'achats',
                component : HomeAchatsComponent,
                canActivate : [AuthGuard],
                children: achatsRoute
             }  ,
             {
                path : 'stocks',
                component : HomeStocksComponent,
                canActivate : [AuthGuard],
                children: stocksRoute,
                
                
             },
             {
                path : 'administration',
                component : HomeUtilisateursComponent,
                canActivate : [AuthGuard],
                children: utilisateursRoute
             },
             {
                path : 'administration',
                component : HomeOrganisationComponent,
                canActivate : [AuthGuard],
                children: organisationsRoute
             },
             {
                path : 'administration',
                component : HomeGroupesComponent,
                canActivate : [AuthGuard],
                children: groupesRoute
             },
             {
                path : 'administration',
                component : HomeAchatsComponent,
                canActivate : [AuthGuard],
                children: profilesRoute
             },
             {
                path : 'suivie',
                component : HomeJournauxComponent,
                canActivate : [AuthGuard],
                children: journauxRoute
             },
             {
                path : 'suivie',
                component : HomeJournauxComponent,
                canActivate : [AuthGuard],
                children:  operationsCourantesRoute
             },
             {
                path : 'suivie',
                component : HomeJournauxComponent,
                canActivate : [AuthGuard],
                children:  livresOresRoute
             },
             {
                path : 'suivie',
                component : HomeJournauxComponent,
                canActivate : [AuthGuard],
                children:  inventairesRoute
             },
             {
                path : 'suivie',
                component : HomeJournauxComponent,
                canActivate : [AuthGuard],
                children:  reglementsCreditsRoute
             },
             {
                path : 'suivie',
                component : HomeJournauxComponent,
                canActivate : [AuthGuard],
                children:  reglementsDettesRoute
             },
             {
                path : 'suivie',
                component : HomeJournauxComponent,
                canActivate : [AuthGuard],
                children:  historiquesClientsRoute
             },
             {
                path : 'suivie',
                component : HomeJournauxComponent,
                canActivate : [AuthGuard],
                children:  historiquesFournisseursRoute
             },
             {
                path : 'finance',
                component : HomeComptesFournisseursComponent,
                canActivate : [AuthGuard],
                children: comptesFournisseursRoute
             },
             {
                path : 'finance',
                component : HomeComptesClientsComponent,
                canActivate : [AuthGuard],
                children:  comptesClientsRoute
             },
             {
                path : 'caisse',
                component : HomeCaisseComponent,
                canActivate : [AuthGuard],
                children: caisseRoute
             },
             {
                path : 'rh',
                component : HomeRHComponent,
                canActivate : [AuthGuard],
                children: RHRoute
             },
             {
                path : 'marketing',
                component : HomePromotionComponent,
                canActivate : [AuthGuard],
                children: marketingRoute
             },
            ]}
        ]

        }
];
export const appRoutingModule: ModuleWithProviders = RouterModule.forRoot(appRoute);
// payementsRouting.providers = [{ provide: APP_BASE_HREF, useValue: '/payements'} ] ;




