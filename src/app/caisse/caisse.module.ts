import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
 import { AppComponent } from '../app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ReactiveFormsModule } from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';

import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatMenuModule} from '@angular/material/menu';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {TableModule} from 'primeng/table';
import {MatSortModule} from '@angular/material/sort';
import {MatSelectModule} from '@angular/material/select';
import {MatTabsModule} from '@angular/material/tabs';
import {PickListModule} from 'primeng/picklist';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatPaginatorModule} from '@angular/material/paginator';
import { RouterModule, provideRoutes} from '@angular/router';
// import { CommunicationService } from './magasins/communication.service';
import { CommonModule } from '../common/common.module';
import { SharedComponentModule } from '../shared-components/shared-components.module';

import {MatSidenavModule} from '@angular/material/sidenav';
import {MatRadioModule} from '@angular/material/radio';





// import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

//  import { ArticleComponent } from './articles/article.component';
// import { ClientComponent } from './clients/client.component';
// import { PayementComponent } from './payements/payement.component';
// import { PayementsGridComponent} from './payements/payements-grid.component';
// import { ClientsGridComponent} from './clients/clients-grid.component';
// import { ArticlesGridComponent} from './articles/articles-grid.component';
// import { GestionClientsComponent} from './clients/gestion-clients.component';
 
// import { PayementService } from './payements/payement.service';
// import { UtilisateurComponent } from './utilisateurs/utilisateur.component';
import {FormsModule } from '@angular/forms';
// import { commonRoute } from './common.route';
 import {APP_BASE_HREF} from '@angular/common';
 import { caisseRoute} from './caisse.routing' ;
 import { HomeCaisseComponent } from './home-caisse.component' ;
 import { EntreesGridComponent } from './entrees/entrees-grid.component';
 import { EntreeNewComponent } from './entrees/entree-new.component';
 import { EntreeEditComponent } from './entrees/entree-edit.component';

 import { SortiesGridComponent } from './sorties/sorties-grid.component';
 import { SortieNewComponent } from './sorties/sortie-new.component';
 import { SortieEditComponent } from './sorties/sortie-edit.component';

 import { DialogAddDocumentSortieComponent} from './sorties/popups/dialog-add-document-sortie.component';
 import { DialogEditDocumentSortieComponent} from './sorties/popups/dialog-edit-document-sortie.component';
 import { DialogListDocumentsSortiesComponent} from './sorties/popups/dialog-list-documents-sorties.component';
 import { DocumentsSortiesGridComponent} from './sorties/documents-sorties-grid.component';
 
 import { DialogAddDocumentEntreeComponent} from './entrees/popups/dialog-add-document-entree.component';
 import { DialogEditDocumentEntreeComponent} from './entrees/popups/dialog-edit-document-entree.component';
 import { DialogListDocumentsEntreesComponent} from './entrees/popups/dialog-list-documents-entrees.component';
 import { DocumentsEntreesGridComponent} from './entrees/documents-entrees-grid.component';
 
 /*const ENTITY_STATES = [
  ...commonRoute
];*/

 // const BASE_URL = [{provide: APP_BASE_HREF, useValue: '/common'}];
@NgModule({
  declarations: [
     EntreeEditComponent,
     EntreeNewComponent,
     EntreesGridComponent,
     SortieEditComponent,
     SortieNewComponent,
     SortiesGridComponent,
     DocumentsSortiesGridComponent,
     DocumentsEntreesGridComponent,
     DialogAddDocumentSortieComponent,
     DialogEditDocumentSortieComponent,
     DialogListDocumentsSortiesComponent,
     DialogAddDocumentEntreeComponent,
     DialogEditDocumentEntreeComponent,
     DialogListDocumentsEntreesComponent,
    HomeCaisseComponent
    
  ],
  imports: [
    BrowserModule, HttpClientModule, MatDatepickerModule, MatNativeDateModule,
     BrowserAnimationsModule, ReactiveFormsModule, MatAutocompleteModule,
    MatInputModule, MatCardModule, MatCheckboxModule, MatButtonModule, MatIconModule,
    MatMenuModule, MatProgressSpinnerModule, MatSortModule, MatPaginatorModule, MatTableModule,
    FormsModule, SharedComponentModule, MatSelectModule, MatDatepickerModule, MatTabsModule,
    MatProgressBarModule, MatDialogModule,
   //  stocksRoutingModule,
    CommonModule, TableModule, MatSidenavModule, MatRadioModule
],
  providers: [MatNativeDateModule  // BASE_URL
  ],
  exports : [
    RouterModule,
    HomeCaisseComponent
    // GestionClientsComponent,
    //  GestionDocumentsTypesComponent,
    //  DocumentTypesGridComponent,
   //  DocumentTypeComponent,

  ],
  entryComponents : [
    DialogAddDocumentSortieComponent,
    DialogEditDocumentSortieComponent,
    DialogListDocumentsSortiesComponent,
    DialogAddDocumentEntreeComponent,
    DialogEditDocumentEntreeComponent,
    DialogListDocumentsEntreesComponent,
  ],
  bootstrap: [AppComponent]
})
export class CaisseModule { }
