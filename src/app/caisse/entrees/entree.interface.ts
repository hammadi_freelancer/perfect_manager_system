type Motif = 'VenteEspece'| 'VenteVirement' | 'PayementCredit' | 'VirementCredit' | 'FinancementInterne' | 'FinancementExterne';

export interface EntreeElement {
         code: string ;
         numeroEntree: string;
         motif: string ;
         description: string;
         montant: number;
         dateOperation: string;
        }
