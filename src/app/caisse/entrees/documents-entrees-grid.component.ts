
import { Component, OnInit, Inject,Input, ViewChild, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {EntreeService} from './entree.service';
import {Entree} from './entree.model';

import {DocumentEntree} from './document-entree.model';
import { DocumentEntreeElement } from './document-entree.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogAddDocumentEntreeComponent } from './popups/dialog-add-document-entree.component';
import { DialogEditDocumentEntreeComponent } from './popups/dialog-edit-document-entree.component';
@Component({
    selector: 'app-documents-entrees-grid',
    templateUrl: 'documents-entrees-grid.component.html',
  })
  export class DocumentsEntreesGridComponent implements OnInit, OnChanges {
     private listDocumentsEntrees: DocumentEntree[] = [];
     private  dataDocumentsEntreesSource: MatTableDataSource<DocumentEntreeElement>;
     private selection = new SelectionModel<DocumentEntreeElement>(true, []);
    
     @Input()
     private entree : Entree

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private entreeService: EntreeService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.entreeService.getDocumentsEntreesByCodeEntree(this.entree.code).subscribe((listDocumentsEntrees) => {
        this.listDocumentsEntrees= listDocumentsEntrees;
  // console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
  }
  ngOnChanges()
  {
    this.specifyListColumnsToBeDisplayed();
    
    this.entreeService.getDocumentsEntreesByCodeEntree(this.entree.code).subscribe((listDocumentsEntrees) => {
      this.listDocumentsEntrees = listDocumentsEntrees;
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }

  checkOneActionInvoked() {
    const listSelectedDocumentsEntrees: DocumentEntree[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentEntElement) => {
    return documentEntElement.code;
  });
  this.listDocumentsEntrees.forEach(documentE => {
    if (codes.findIndex(code => code === documentE.code) > -1) {
      listSelectedDocumentsEntrees.push(documentE);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsEntrees !== undefined) {

      this.listDocumentsEntrees.forEach(docE => {
             listElements.push( {code: docE.code ,
          dateReception: docE.dateReception,
          numeroDocumentEntree: docE.numeroDocumentEntree,
          typeDocument: docE.typeDocument,
          description: docE.description
        } );
      });
    }
      this.dataDocumentsEntreesSource= new MatTableDataSource<DocumentEntreeElement>(listElements);
      this.dataDocumentsEntreesSource.sort = this.sort;
      this.dataDocumentsEntreesSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    //  console.log('calling specifyListColumnsToBeDisplayed documents grid');
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
    
     this.columnsDefinitions = [
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocumentEntree' , displayTitle: 'N°DOC'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
      console.log(this.columnsNames);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsEntreesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsEntreesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsEntreesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsEntreesSource.paginator) {
      this.dataDocumentsEntreesSource.paginator.firstPage();
    }
  }
  showAddDocumentDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeEntree : this.entree.code
   };
 
   const dialogRef = this.dialog.open(DialogAddDocumentEntreeComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(docAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditDocumentDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeDocumentEntree : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditDocumentEntreeComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(docAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Document Séléctionnée!');
    }
    }
    supprimerDocuments()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(doc, index) {
              this.entreeService.deleteDocumentEntree(doc.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar(  ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Document Séléctionnée!');
        }
    }
    refresh() {
      this.entreeService.getDocumentsEntreesByCodeEntree(this.entree.code).subscribe((listDocsEnts) => {
        this.listDocumentsEntrees = listDocsEnts;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
