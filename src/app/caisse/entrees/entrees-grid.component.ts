import { Component, OnInit, Output, ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {Entree} from './entree.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {EntreeService} from './entree.service';
import { EntreeElement } from './entree.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';

import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {EntreeDataFilter} from './entree-data.filter';

@Component({
  selector: 'app-entrees-grid',
  templateUrl: './entrees-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class EntreesGridComponent implements OnInit {

  private entree: Entree =  new Entree();

  private entreeDataFilter = new EntreeDataFilter();
  private showSelectColumnsMultiSelect = false;
  private showFilter = false;

  private selection = new SelectionModel<EntreeElement>(true, []);
  
@Output()
select: EventEmitter<Entree[]> = new EventEmitter<Entree[]>();

//private lastClientAdded: Client;

 selectedEntree: Entree ;

 @Input()
 private listEntrees: any = [] ;
 listEntreesOrgin: any = [];
 private checkedAll: false;
 private  dataEntreesSource: MatTableDataSource<EntreeElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
  constructor( private route: ActivatedRoute, private entreesService: EntreeService,
    private router: Router, private matSnackBar: MatSnackBar ) {
  }
  ngOnInit() {
    this.entreesService.getEntrees().subscribe((entrees) => {
      this.listEntrees = entrees;
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });
   /* this.creditsService.getCredits().subscribe((credits) => {
      console.log('list credits ', credits);
      // this.listCredits = credits;
      this.listCredits =  this.listCredits.map((credit) => {
        if (credit.client === 'undefined' || credit.client === null ) {
           credit.client = new Client();
         }
         return credit;
   });      this.listCreditsOrgin = this.listCredits ;
     });*/
    // this.listClientsOrgin = this.listClients ;

  }
  deleteEntree(entree) {
      console.log('call delete !', entree );
    this.entreesService.deleteEntree(entree.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData();

      } else {
       // show error message
      }
     });

  }
loadData() {
  this.entreesService.getEntrees().subscribe((entrees) => {
    this.listEntrees = entrees;
    // this.listClients[this.listClients.length] = this.lastClientAdded;
    this.listEntreesOrgin = this.listEntrees;
    this.transformDataToByConsumedByMatTable();
   });
  // this.listClientsOrgin = this.listClients ;
}


checkOneActionInvoked() {
  const listSelectedEntrees: Entree[] = [];
console.log('selected articles are ');
console.log(this.selection.selected);
if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((entreeElement) => {
  return entreeElement.code;
});
this.listEntrees.forEach(entree => {
  if (codes.findIndex(code => code === entree.code) > -1) {
    listSelectedEntrees.push(entree);
  }
  });
}
this.select.emit(listSelectedEntrees);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listEntrees !== undefined) {
    this.listEntrees.forEach(entree => {
      listElements.push( {code: entree.code ,
        numeroEntree: entree.numeroEntree ,
        motif: entree.motif, description: entree.description,
         dateOperation: entree.dateOperation,
        montant: entree.montant} );
    });
  }
    this.dataEntreesSource = new MatTableDataSource<EntreeElement>(listElements);
    this.dataEntreesSource.sort = this.sort;
    this.dataEntreesSource.paginator = this.paginator;
}
 specifyListColumnsToBeDisplayed() {
   this.columnsDefinitions = [
     {name: 'numeroEntree' , displayTitle: 'N°Entree'},
   {name: 'motif' , displayTitle: 'Motif'},
   {name: 'description' , displayTitle: 'Description'},
   {name: 'dateOperation' , displayTitle: 'Date Opération'},
   {name: 'montant' , displayTitle: 'Montant'},
   ];
   this.columnsTitles = this.columnsDefinitions.map((colDef) => {
             return colDef.displayTitle;
   });
   this.columnsNames[0] = 'select';
   this.columnsDefinitions.map((colDef) => {
    this.columnsNames.push(colDef.name);
});
   this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
    return colDef.displayTitleAr;
});
}
masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataEntreesSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataEntreesSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataEntreesSource.filter = filterValue.trim().toLowerCase();
  if (this.dataEntreesSource.paginator) {
    this.dataEntreesSource.paginator.firstPage();
  }
}
loadAddEntreeComponent() {
  this.router.navigate(['/pms/caisse/ajouterEntree']) ;

}
loadEditEntreeComponent() {
  if (this.selection.selected.length === 0) {
    this.openSnackBar(' Aucun élément sélectionné' );
    // this.loadData();
    } else
   {
  this.router.navigate(['/pms/caisse/editerEntree', this.selection.selected[ this.selection.selected.length - 1].code]) ;
    }
}
supprimerEntrees()
{
  console.log('NBRE OF ELEMENTS SELECTED');
  console.log(this.selection.selected.length);
  console.log(' ELEMENTS SELECTED');
  console.log(this.selection.selected);
  if ( this.selection.selected.length !== 0) {
      this.selection.selected.forEach(function(entree, index) {
          this.entreesService.deleteEntree(entree.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.openSnackBar( ' Element(s) Supprimé(s)');
             this.loadData();
            }
      }, this);
    } else {
      this.openSnackBar('Aucune Entrée Séléctionnée');
    }
}
refresh()
{
  this.loadData();

}
  openSnackBar(messageToDisplay) {
 this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
     verticalPosition: 'top'});
}
}
