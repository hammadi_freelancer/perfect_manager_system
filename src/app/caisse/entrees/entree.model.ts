

import { BaseEntity } from '../../common/base-entity.model' ;
type Etat = 'Initial' | 'Valide' | 'Annule' | ''

type Motif = 'Ventes'
 | 'PayementsCreditsClients' |
 'VirementCheque' |
  'Emprunts' ;

export class Entree extends BaseEntity {
   constructor(
       public code?: string,
    public numeroEntree?: string,
    public motif?: Motif, public dateOperation?: string,
    public montant?: number,
    public etat?: Etat,
    
     public codeResponsable?: string,

) {
    super();
}
}
