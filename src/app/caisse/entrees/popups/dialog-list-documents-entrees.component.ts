
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {EntreeService} from '../entree.service';
import {DocumentEntree} from '../document-entree.model';
import { DocumentEntreeElement } from '../document-entree.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-documents-entrees',
    templateUrl: 'dialog-list-documents-entrees.component.html',
  })
  export class DialogListDocumentsEntreesComponent implements OnInit {
     private listDocumentsEntrees: DocumentEntree[] = [];
     private  dataDocumentsEntreesSource: MatTableDataSource<DocumentEntreeElement>;
     private selection = new SelectionModel<DocumentEntreeElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private entreeService: EntreeService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
     //  console.log('code credit in dialog component is :', this.data.codeClient);
      this.entreeService.getDocumentsEntreesByCodeEntree(this.data.codeEntree).subscribe((listDocumentsEntrees) => {
        this.listDocumentsEntrees = listDocumentsEntrees;
// console.log('recieving data from backend', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedDocumentsEntrees: DocumentEntree[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentEElement) => {
    return documentEElement.code;
  });
  this.listDocumentsEntrees.forEach(documentEn => {
    if (codes.findIndex(code => code === documentEn.code) > -1) {
      listSelectedDocumentsEntrees.push(documentEn);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsEntrees !== undefined) {

      this.listDocumentsEntrees.forEach(docEn => {
             listElements.push( {code: docEn.code ,
          dateReception: docEn.dateReception,
          numeroDocumentEntree: docEn.numeroDocumentEntree,
          typeDocument: docEn.typeDocument,
          description: docEn.description
        } );
      });
    }
      this.dataDocumentsEntreesSource = new MatTableDataSource<DocumentEntreeElement>(listElements);
      this.dataDocumentsEntreesSource.sort = this.sort;
      this.dataDocumentsEntreesSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocumentEntree' , displayTitle: 'N°Doc'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsEntreesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsEntreesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsEntreesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsEntreesSource.paginator) {
      this.dataDocumentsEntreesSource.paginator.firstPage();
    }
  }

}
