
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {EntreeService} from '../entree.service';
import {DocumentEntree} from '../document-entree.model';

@Component({
    selector: 'app-dialog-edit-document-entree',
    templateUrl: 'dialog-edit-document-entree.component.html',
  })
  export class DialogEditDocumentEntreeComponent implements OnInit {
     private documentEntree: DocumentEntree;
     private updateEnCours = false;
  
     constructor(  private entreeService: EntreeService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {

        this.entreeService.getDocumentEntree(this.data.codeDocumentEntree).subscribe((doc) => {
            this.documentEntree = doc;
     });
    }
    
    updateDocumentEntree() 
    {
      this.updateEnCours = true;
       this.entreeService.updateDocumentEntree(this.documentEntree).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.openSnackBar(  'Document Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
     imageFace1Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentEntree.imageFace1 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imageFace2Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentEntree.imageFace2 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
  }
