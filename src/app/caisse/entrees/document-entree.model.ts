
import { BaseEntity } from '../../common/base-entity.model' ;

type TypeDocument =   'Cheque' |'FactureVentes' |'FactureAchats'
| 'FactureElectricite' |'FactureEauPotable' | 'FactureTelephonique' | 'FichePaie' |
'BonLivraison' | 'BonReception' | 'RecuPayement' |  'RecuLivraison' | 'Autre';

export class DocumentEntree extends BaseEntity {
    constructor(
        public code?: string,
         public codeEntree?: string,
         public numeroDocumentEntree?: string,
        public dateReceptionObject?: Date,
        public dateReception?: string,
        public typeDocument?: TypeDocument,
        public fileExtension?: string,
        public imageFace1?: any,
        public imageFace2?: any,
     ) {
         super();
        
       }
  
}
