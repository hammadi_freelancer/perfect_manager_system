import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';

import {Entree} from './entree.model';
import { DocumentEntree } from './document-entree.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_ENTREES = 'http://localhost:8082/caisse/entrees/getEntrees';
const URL_ADD_ENTREE = 'http://localhost:8082/caisse/entrees/addEntree';
const URL_GET_ENTREE = 'http://localhost:8082/caisse/entrees/getEntree';

const URL_UPDATE_ENTREE = 'http://localhost:8082/caisse/entrees/updateEntree';
const URL_DELETE_ENTREE = 'http://localhost:8082/caisse/entrees/deleteEntree';



const  URL_ADD_DOCUMENT_ENTREE = 'http://localhost:8082/caisse/documentsEntrees/addDocumentEntree';
const  URL_GET_LIST_DOCUMENT_ENTREE = 'http://localhost:8082/caisse/documentsEntrees/getDocumentsEntrees';
const  URL_DELETE_DOCUCMENT_ENTREE = 'http://localhost:8082/caisse/documentsEntrees/deleteDocumentEntree';
const  URL_UPDATE_DOCUMENT_ENTREE = 'http://localhost:8082/caisse/documentsEntrees/updateDocumentEntree';
const URL_GET_DOCUMENT_ENTREE = 'http://localhost:8082/caisse/documentsEntrees/getDocumentEntree';
const URL_GET_LIST_DOCUMENT_ENTREE_BY_CODE_ENTREE =
 'http://localhost:8082/caisse/documentsEntrees/getDocumentsEntreesByCodeEntree';



@Injectable({
  providedIn: 'root'
})
export class EntreeService {



  constructor(private httpClient: HttpClient) {

   }

   getEntrees(): Observable<Entree[]> {
    return this.httpClient
    .get<Entree[]>(URL_GET_LIST_ENTREES);
   }
 
   addEntree(entree: Entree): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_ENTREE, entree);
  }
  updateEntree(entree: Entree): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_ENTREE, entree);
  }
  deleteEntree(codeEntree): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_ENTREE + '/' + codeEntree);
  }
  getEntree(codeEntree): Observable<Entree> {
    return this.httpClient.get<Entree>(URL_GET_ENTREE + '/' + codeEntree);
  }



  
  addDocumentEntree(documentEntree: DocumentEntree): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_DOCUMENT_ENTREE, documentEntree);
    
  }

  getDocumentsEntrees(): Observable<DocumentEntree[]> {
    return this.httpClient
    .get<DocumentEntree[]>(URL_GET_LIST_DOCUMENT_ENTREE);
   }
   getDocumentEntree(codeDocumentEntree): Observable<DocumentEntree> {
    return this.httpClient.get<DocumentEntree>(URL_GET_DOCUMENT_ENTREE + '/' + codeDocumentEntree);
  }
  deleteDocumentEntree(codeDocumentClient): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_DOCUCMENT_ENTREE + '/' + codeDocumentClient);
  }
  getDocumentsEntreesByCodeEntree(codeDocumentEntree): Observable<DocumentEntree[]> {
    return this.httpClient.get<DocumentEntree[]>(URL_GET_LIST_DOCUMENT_ENTREE_BY_CODE_ENTREE + '/' + codeDocumentEntree);
  }
  updateDocumentEntree(doc: DocumentEntree): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_DOCUMENT_ENTREE, doc);
  }
}
