import { Component, OnInit, Input, EventEmitter, Output,
    AfterViewChecked
    } from '@angular/core';
    // import { ArticleService } from './article.service';
    import {Entree} from './entree.model';
    import {EntreeService} from './entree.service';
    import {FormControl} from '@angular/forms';
    import {map, startWith} from 'rxjs/operators';
    import {Observable} from 'rxjs';
    import { Router, ActivatedRoute, ParamMap } from '@angular/router';
    import {MatSnackBar} from '@angular/material';
    import { MotifView } from './motif-view.interface';
    
    @Component({
      selector: 'app-entree-new',
      templateUrl: './entree-new.component.html',
      // styleUrls: ['./players.component.css']
    })
export class EntreeNewComponent implements OnInit {
    private entree: Entree = new Entree();
    @Input()
    private listEntrees: any = [] ;
 
    private file: any;

    private motifs: MotifView[] = [
      {value: 'VenteEspece', viewValue: 'Vente Espèce'},
      {value: 'VenteVirement', viewValue: 'Vente Virement'},
      {value: 'VirementCredit', viewValue: 'Virement Credit'},
      {value: 'PayementCredit', viewValue: 'Payement Credit'},
      {value: 'FinancementInterne', viewValue: 'Financement Interne'},
      {value: 'FinancementExterne', viewValue: 'Financement Externe'},
      
    ];
      constructor( private route: ActivatedRoute,
        private router: Router , private entreeService: EntreeService, private matSnackBar: MatSnackBar ) {
      }
      ngOnInit() {
     
      }
    saveEntree() {
       
        console.log(this.entree);
        this.entreeService.addEntree(this.entree).subscribe((response) => {
            if (response.error_message ===  undefined) {
              // this.save.emit(response);
              this.entree = new Entree();
              this.openSnackBar(  'Element Enregistré');
            } else {
             // show error message
             this.openSnackBar(  'Erreurs!');
             
            }
        });
    }
    loadGridEntrees() {
      this.router.navigate(['/pms/caisse/entrees']) ;
    }
    fileChanged($event) {
        this.file = $event.target.files[0];
        const fileReader = new FileReader();
        fileReader.onload = (e) => {
          console.log(fileReader.result);
          // this.article.image = fileReader.result;
          // this.filesData[indexFile] = fileData;
        };
        fileReader.readAsDataURL(this.file);
       }
       vider() {
         this.entree = new Entree();
       }
       openSnackBar(messageToDisplay) {
        this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
            verticalPosition: 'top'});
       }
 
    }
