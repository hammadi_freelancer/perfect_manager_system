


export interface DocumentEntreeElement {
         code: string ;
         dateReception: string ;
         numeroDocumentEntree: number;
         typeDocument?: string;
          description?: string;
}
