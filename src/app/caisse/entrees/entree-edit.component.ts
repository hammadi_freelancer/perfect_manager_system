import { Component, OnInit, Input, EventEmitter, Output, AfterContentChecked,
AfterViewChecked
} from '@angular/core';
// import { ArticleService } from './article.service';
import {Entree} from './entree.model';
import {EntreeService} from './entree.service';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {MatSnackBar} from '@angular/material';
import { MotifView } from './motif-view.interface';
@Component({
  selector: 'app-entree-edit',
  templateUrl: './entree-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class EntreeEditComponent implements OnInit, AfterContentChecked {

// private client: Client = new Client();


private entree: Entree = new Entree();
private motifs: MotifView[] = [
  {value: 'Ventes', viewValue: 'Ventes'},
  {value: 'VirementCheque', viewValue: 'Virement'},
  {value: 'VirementCredit', viewValue: 'Virement Credit'},
  {value: 'PayementCredit', viewValue: 'Payement Credit'},
  {value: 'FinancementInterne', viewValue: 'Financement Interne'},
  {value: 'FinancementExterne', viewValue: 'Financement Externe'},
];

private tabsDisabled = true;
private etatsEntrees: any[] = [
  {value: 'Annule', viewValue: 'Annulé'},
  {value: 'Initial', viewValue: 'Initial'},
  {value: 'Valide', viewValue: 'Validé'}
];

  constructor( private route: ActivatedRoute,
    private router: Router , private entreeService: EntreeService, private matSnackBar: MatSnackBar,
    
   ) {
  }
  ngOnInit() {
      const codeEntree = this.route.snapshot.paramMap.get('codeEntree');
      this.entreeService.getEntree(codeEntree).subscribe((entree) => {
               this.entree = entree;
               if (this.entree.etat === 'Valide') {
                this.tabsDisabled = false;
               }
               if (this.entree.etat === 'Valide' || this.entree.etat === 'Annule')
                {
                  this.etatsEntrees.splice(1, 1);
                }
                if (this.entree.etat === 'Initial')
                  {
                    this.etatsEntrees.splice(0, 1);
                  }
      });
     
  }
  loadAddEntreeComponent() {
    this.router.navigate(['/pms/caisse/ajouterEntree']) ;
  
  }
  loadGridEntrees() {
    this.router.navigate(['/pms/caisse/entrees']) ;
  }
  supprimerEntree() {
     
      this.entreeService.deleteEntree(this.entree.code).subscribe((response) => {

        this.openSnackBar( ' Element Supprimé');
        this.loadGridEntrees() ;
            });
  }
updateEntree() {

  console.log(this.entree);
  this.entreeService.updateEntree(this.entree).subscribe((response) => {
    if (response.error_message ===  undefined) {
      this.openSnackBar( ' Entree mis à jour ');
      if (this.entree.etat === 'Valide')
        {
          this.tabsDisabled = false;
          this.etatsEntrees = [
            {value: 'Annule', viewValue: 'Annulé'},
            {value: 'Valide', viewValue: 'Validé'}
  
          ];
        }
        if (this.entree.etat === 'Annule')
          {
            this.tabsDisabled = true;
         
          }
      
      //  this.save.emit(response);
      // this.client = new Client();
      // this.modeUpdate = false;
    } else {
      this.openSnackBar( ' Erreurs.. ');
      
     // show error message
    }
});
}

 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

ngAfterContentChecked() {

}
selectedTabChanged($event) {
  if ($event.index === 4) { // Composition tab was selected
   // this.loadListArticles();
   } else if ($event.index === 1) {
    //  this.loadListSchemasConfiguration() ;
   }
  }
}


