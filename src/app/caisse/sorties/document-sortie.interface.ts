


export interface DocumentSortieElement {
         code: string ;
         dateReception: string ;
         numeroDocumentSortie: string;
         typeDocument?: string;
          description?: string;
}
