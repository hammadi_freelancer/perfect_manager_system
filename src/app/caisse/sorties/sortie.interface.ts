type Motif = 'VenteEspece'| 'VenteVirement' | 'PayementCredit' | 'VirementCredit' | 'FinancementInterne' | 'FinancementExterne';

export interface SortieElement {
         code: string ;
         motif: string ;
         numeroSortie: string;
         description: string;
         montant: number;
         dateOperation: string;
        }
