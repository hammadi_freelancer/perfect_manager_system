import { Component, OnInit, Input, EventEmitter, Output,
    AfterViewChecked
    } from '@angular/core';
    // import { ArticleService } from './article.service';
    import {Sortie} from './sortie.model';
    import {SortieService} from './sortie.service';
    import {FormControl} from '@angular/forms';
    import {map, startWith} from 'rxjs/operators';
    import {Observable} from 'rxjs';
    import { Router, ActivatedRoute, ParamMap } from '@angular/router';
    import {MatSnackBar} from '@angular/material';
    import { MotifView } from './motif-view.interface';
    
    @Component({
      selector: 'app-sortie-new',
      templateUrl: './sortie-new.component.html',
      // styleUrls: ['./players.component.css']
    })
export class SortieNewComponent implements OnInit {
    private sortie: Sortie = new Sortie();
    @Input()
    private listSorties: any = [] ;
 
    private file: any;

    private motifs: MotifView[] = [
      {value: 'AchatsEspece', viewValue: 'Achats Espèce'},
      {value: 'AchatsVirement', viewValue: 'Achats Virement'},
      {value: 'VirementDettes', viewValue: 'Virement Dettes'},
      {value: 'PayementDettes', viewValue: 'Payement Dettes'},
      {value: 'PayementEmployees', viewValue: 'Payement Employés'}
      
    ];
      constructor( private route: ActivatedRoute,
        private router: Router , private sortieService: SortieService, private matSnackBar: MatSnackBar ) {
      }
      ngOnInit() {
     
      }
    saveSortie() {
       
        console.log(this.sortie);
        this.sortieService.addSortie(this.sortie).subscribe((response) => {
            if (response.error_message ===  undefined) {
              // this.save.emit(response);
              this.sortie = new Sortie();
              this.openSnackBar(  'Sortie Enregistré');
            } else {
             // show error message
             this.openSnackBar(  'Erreurs!');
             
            }
        });
    }
    loadGridSorties() {
      this.router.navigate(['/pms/caisse/sorties']) ;
    }
    fileChanged($event) {
        this.file = $event.target.files[0];
        const fileReader = new FileReader();
        fileReader.onload = (e) => {
          console.log(fileReader.result);
          // this.article.image = fileReader.result;
          // this.filesData[indexFile] = fileData;
        };
        fileReader.readAsDataURL(this.file);
       }
       vider() {
         this.sortie = new Sortie();
       }
       openSnackBar(messageToDisplay) {
        this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
            verticalPosition: 'top'});
       }
 
    }
