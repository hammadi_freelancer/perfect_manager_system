import { Component, OnInit, Input, EventEmitter, Output, AfterContentChecked,
AfterViewChecked
} from '@angular/core';
// import { ArticleService } from './article.service';
import {Sortie} from './sortie.model';
import {SortieService} from './sortie.service';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {MatSnackBar} from '@angular/material';
import { MotifView } from './motif-view.interface';
@Component({
  selector: 'app-sortie-edit',
  templateUrl: './sortie-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class SortieEditComponent implements OnInit, AfterContentChecked {

// private client: Client = new Client();


private sortie: Sortie = new Sortie();
private motifs: MotifView[] = [
  {value: 'AchatsEspece', viewValue: 'Achats Espèce'},
  {value: 'AchatsVirement', viewValue: 'Achats Virement'},
  {value: 'PayementDettes', viewValue: 'Payement Dettes'},
  {value: 'VirementDettes', viewValue: 'Virement Dettes'},
  {value: 'PayementEmployees', viewValue: 'Payement Employés'}
];
private tabsDisabled = true;
private etatsSorties: any[] = [
  {value: 'Annule', viewValue: 'Annulé'},
  {value: 'Initial', viewValue: 'Initial'},
  {value: 'Valide', viewValue: 'Validé'}
];

  constructor( private route: ActivatedRoute,
    private router: Router , private sortieService: SortieService, private matSnackBar: MatSnackBar,
    
   ) {
  }
  ngOnInit() {
      const codeSortie = this.route.snapshot.paramMap.get('codeSortie');
      this.sortieService.getSortie(codeSortie).subscribe((sortie) => {
               this.sortie = sortie;
               if (this.sortie.etat === 'Valide') {
                this.tabsDisabled = false;
               }
               if (this.sortie.etat === 'Valide' || this.sortie.etat === 'Annule')
                {
                  this.etatsSorties.splice(1, 1);
                }
                if (this.sortie.etat === 'Initial')
                  {
                    this.etatsSorties.splice(0, 1);
                  }
      });
     
  }
  loadAddSortieComponent() {
    this.router.navigate(['/pms/caisse/ajouterSortie']) ;
  
  }
  loadGridSorties() {
    this.router.navigate(['/pms/caisse/sorties']) ;
  }
  supprimerSortie() {
     
      this.sortieService.deleteSortie(this.sortie.code).subscribe((response) => {

        this.openSnackBar( ' Element Supprimé');
        this.loadGridSorties() ;
            });
  }
updateSortie() {

  console.log(this.sortie);
  this.sortieService.updateSortie(this.sortie).subscribe((response) => {
    if (response.error_message ===  undefined) {
      console.log('the response of update is ', response );
      this.openSnackBar( ' Sortie mise à jour ');
      if (this.sortie.etat === 'Valide')
        {
          this.tabsDisabled = false;
          this.etatsSorties= [
            {value: 'Annule', viewValue: 'Annulé'},
            {value: 'Valide', viewValue: 'Validé'}
  
          ];
        }
        if (this.sortie.etat === 'Annule')
          {
            this.tabsDisabled = true;
         
          }
      //  this.save.emit(response);
      // this.client = new Client();
      // this.modeUpdate = false;
    } else {
      this.openSnackBar( ' Erreurs.. ');
      
     // show error message
    }
});
}

 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

ngAfterContentChecked() {

}
selectedTabChanged($event) {
  if ($event.index === 4) { // Composition tab was selected
   // this.loadListArticles();
   } else if ($event.index === 1) {
    //  this.loadListSchemasConfiguration() ;
   }
  }
}


