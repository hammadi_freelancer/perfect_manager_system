

import { BaseEntity } from '../../common/base-entity.model' ;
type Etat = 'Initial' | 'Valide' | 'Annule' | ''

type Motif = 
'Achats'| 'VirementCheque' | 'PayementCreditFournisseur' | 'Emprunts';

export class Sortie extends BaseEntity {
   constructor(public code?: string, public numeroSortie?: string,
    public motif?: Motif, public dateOperation?: string,
    public montant?: number,
     public codeResponsable?: string,
     public etat?: Etat,
     
    

) {
    super();
}
}
