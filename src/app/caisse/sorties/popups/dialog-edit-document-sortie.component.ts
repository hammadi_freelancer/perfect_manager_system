
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {SortieService} from '../sortie.service';
import {DocumentSortie} from '../document-sortie.model';

@Component({
    selector: 'app-dialog-edit-document-sortie',
    templateUrl: 'dialog-edit-document-sortie.component.html',
  })
  export class DialogEditDocumentSortieComponent implements OnInit {
     private documentSortie: DocumentSortie;
     private updateEnCours = false;
     private typesDoc: any[] = [
      {value: 'FactureVentes', viewValue: 'Facture Ventes'},
      {value: 'FactureAchats', viewValue: 'Facture Achats'},
      {value: 'FactureTelephonique', viewValue: 'Facture Téléphonique'},
      {value: 'FactureElectricite', viewValue: 'Facture Eléctricité'},
      {value: 'FactureEauPotable', viewValue: 'Facture Eau Potable'},
      {value: 'FichePaie', viewValue: 'Fiche Paie'},
      {value: 'BonLivraison', viewValue: 'Bon Livraison'},
      {value: 'BonReception', viewValue: 'Bon Récéption'},
      {value: 'RecuPayement', viewValue: 'Reçu Payement'},
      {value: 'Cheque', viewValue: 'Chèque'},
      {value: 'Autre', viewValue: 'Autre'},
    ]
     constructor(  private sortieService: SortieService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {

        this.sortieService.getDocumentSortie(this.data.codeDocumentSortie).subscribe((doc) => {
            this.documentSortie = doc;
     });
    }
    
    updateDocumentSortie() 
    {
      this.updateEnCours = true;
       this.sortieService.updateDocumentSortie(this.documentSortie).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.openSnackBar(  'Document Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
     imageFace1Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentSortie.imageFace1 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imageFace2Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentSortie.imageFace2 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
  }
