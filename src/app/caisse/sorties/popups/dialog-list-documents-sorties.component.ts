
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {SortieService} from '../sortie.service';
import {DocumentSortie} from '../document-sortie.model';
import { DocumentSortieElement } from '../document-sortie.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-documents-sorties',
    templateUrl: 'dialog-list-documents-sorties.component.html',
  })
  export class DialogListDocumentsSortiesComponent implements OnInit {
     private listDocumentsSorties: DocumentSortie[] = [];
     private  dataDocumentsSortiesSource: MatTableDataSource<DocumentSortieElement>;
     private selection = new SelectionModel<DocumentSortieElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private sortieService: SortieService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      // console.log('code credit in dialog component is :', this.data.codeClient);
      this.sortieService.getDocumentsSortiesByCodeSortie(this.data.codeSortie).subscribe((listDocumentsSorties) => {
        this.listDocumentsSorties = listDocumentsSorties;
// console.log('recieving data from backend', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedDocumentsSorties: DocumentSortie[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentSElement) => {
    return documentSElement.code;
  });
  this.listDocumentsSorties.forEach(documentS => {
    if (codes.findIndex(code => code === documentS.code) > -1) {
      listSelectedDocumentsSorties.push(documentS);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsSorties !== undefined) {

      this.listDocumentsSorties.forEach(docS => {
             listElements.push( {code: docS.code ,
          dateReception: docS.dateReception,
          numeroDocumentSortie: docS.numeroDocumentSortie,
          typeDocument: docS.typeDocument,
          description: docS.description
        } );
      });
    }
      this.dataDocumentsSortiesSource= new MatTableDataSource<DocumentSortieElement>(listElements);
      this.dataDocumentsSortiesSource.sort = this.sort;
      this.dataDocumentsSortiesSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [
      
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocumentSortie' , displayTitle: 'N°DOC'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsSortiesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsSortiesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsSortiesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsSortiesSource.paginator) {
      this.dataDocumentsSortiesSource.paginator.firstPage();
    }
  }

}
