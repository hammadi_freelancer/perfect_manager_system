import { Component, OnInit, Output, ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {Sortie} from './sortie.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {SortieService} from './sortie.service';
import { SortieElement } from './sortie.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';

import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-sorties-grid',
  templateUrl: './sorties-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class SortiesGridComponent implements OnInit {

  private sortie: Sortie =  new Sortie();
  private selection = new SelectionModel<SortieElement>(true, []);
  
@Output()
select: EventEmitter<Sortie[]> = new EventEmitter<Sortie[]>();

//private lastClientAdded: Client;

 selectedSortie: Sortie ;

 @Input()
 private listSorties: any = [] ;
 listSortiesOrgin: any = [];
 private checkedAll: false;
 private  dataSortiesSource: MatTableDataSource<SortieElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
  constructor( private route: ActivatedRoute, private sortiesService: SortieService,
    private router: Router, private matSnackBar: MatSnackBar ) {
  }
  ngOnInit() {
    this.sortiesService.getSorties().subscribe((sorties) => {
      this.listSorties = sorties;
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });


  }
  deleteSortie(sortie) {
      console.log('call delete !', sortie );
    this.sortiesService.deleteSortie(sortie.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData();

      } else {
       // show error message
      }
     });

  }
loadData() {
  this.sortiesService.getSorties().subscribe((sorties) => {
    this.listSorties = sorties;
    // this.listClients[this.listClients.length] = this.lastClientAdded;
    this.listSortiesOrgin = this.listSorties;
    this.transformDataToByConsumedByMatTable();
   });
  // this.listClientsOrgin = this.listClients ;
}


checkOneActionInvoked() {
  const listSelectedSorties: Sortie[] = [];
console.log('selected sorties are ');
console.log(this.selection.selected);
if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((sortieElement) => {
  return sortieElement.code;
});
this.listSorties.forEach(sortie => {
  if (codes.findIndex(code => code === sortie.code) > -1) {
    listSelectedSorties.push(sortie);
  }
  });
}
this.select.emit(listSelectedSorties);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listSorties !== undefined) {
    this.listSorties.forEach(sortie => {
      listElements.push( {code: sortie.code ,
        numeroSortie: sortie.numeroSortie,
        motif: sortie.motif, description: sortie.description,
         dateOperation: sortie.dateOperation,
        montant: sortie.montant} );
    });
  }
    this.dataSortiesSource = new MatTableDataSource<SortieElement>(listElements);
    this.dataSortiesSource.sort = this.sort;
    this.dataSortiesSource.paginator = this.paginator;
}
 specifyListColumnsToBeDisplayed() {
   this.columnsDefinitions = [
   {name: 'motif' , displayTitle: 'Motif'},
   {name: 'numeroSortie' , displayTitle: 'N°Sortie'},
   {name: 'description' , displayTitle: 'Description'},
   {name: 'dateOperation' , displayTitle: 'Date Opération'},
   {name: 'montant' , displayTitle: 'Montant'},
   ];
   this.columnsTitles = this.columnsDefinitions.map((colDef) => {
             return colDef.displayTitle;
   });
   this.columnsNames[0] = 'select';
   this.columnsDefinitions.map((colDef) => {
    this.columnsNames.push(colDef.name);
});
   this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
    return colDef.displayTitleAr;
});
}
masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataSortiesSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataSortiesSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataSortiesSource.filter = filterValue.trim().toLowerCase();
  if (this.dataSortiesSource.paginator) {
    this.dataSortiesSource.paginator.firstPage();
  }
}
loadAddSortieComponent() {
  this.router.navigate(['/pms/caisse/ajouterSortie']) ;

}
loadEditSortieComponent() {
  if (this.selection.selected.length === 0) {
    this.openSnackBar(' Aucun élément sélectionné' );
    // this.loadData();
    } else
   {
  this.router.navigate(['/pms/caisse/editerSortie', this.selection.selected[ this.selection.selected.length - 1].code]) ;
    }
}
supprimerSorties()
{
  console.log('NBRE OF ELEMENTS SELECTED');
  console.log(this.selection.selected.length);
  console.log(' ELEMENTS SELECTED');
  console.log(this.selection.selected);
  if ( this.selection.selected.length !== 0) {
      this.selection.selected.forEach(function(sortie, index) {
          this.sortieService.deleteSortie(sortie.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.openSnackBar(' Element(s) Supprimé(s)');
             this.loadData();
            }
      }, this);
    } else {
      this.openSnackBar('Aucune Sortie Séléctionnée');
    }
}
refresh()
{
  this.loadData();

}
  openSnackBar(messageToDisplay) {
 this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
     verticalPosition: 'top'});
}
}
