import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';

import {Sortie} from './sortie.model';
import {DocumentSortie } from './document-sortie.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_SORTIES = 'http://localhost:8082/caisse/sorties/getSorties';
const URL_ADD_SORTIE = 'http://localhost:8082/caisse/sorties/addSortie';
const URL_GET_SORTIE = 'http://localhost:8082/caisse/sorties/getSortie';

const URL_UPDATE_SORTIE = 'http://localhost:8082/caisse/sorties/updateSortie';
const URL_DELETE_SORTIE = 'http://localhost:8082/caisse/sorties/deleteSortie';


const  URL_ADD_DOCUMENT_SORTIE = 'http://localhost:8082/caisse/documentsSorties/addDocumentSortie';
const  URL_GET_LIST_DOCUMENT_SORTIE = 'http://localhost:8082/caisse/documentsSorties/getDocumentsSorties';
const  URL_DELETE_DOCUCMENT_SORTIE = 'http://localhost:8082/caisse/documentsSorties/deleteDocumentSortie';
const  URL_UPDATE_DOCUMENT_SORTIE = 'http://localhost:8082/caisse/documentsSorties/updateDocumentSortie';
const URL_GET_DOCUMENT_SORTIE = 'http://localhost:8082/caisse/documentsSorties/getDocumentSortie';
const URL_GET_LIST_DOCUMENT_SORTIE_BY_CODE_SORTIE =
 'http://localhost:8082/caisse/documentsSorties/getDocumentsSortiesByCodeSortie';



@Injectable({
  providedIn: 'root'
})
export class SortieService {



  constructor(private httpClient: HttpClient) {

   }

   getSorties(): Observable<Sortie[]> {
    return this.httpClient
    .get<Sortie[]>(URL_GET_LIST_SORTIES);
   }
 
   addSortie(sortie: Sortie): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_SORTIE, sortie);
  }
  updateSortie(sortie: Sortie): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_SORTIE, sortie);
  }
  deleteSortie(codeSortie): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_SORTIE + '/' + codeSortie);
  }
  getSortie(codeSortie): Observable<Sortie> {
    return this.httpClient.get<Sortie>(URL_GET_SORTIE + '/' + codeSortie);
  }


  addDocumentSortie(documentSortie: DocumentSortie): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_DOCUMENT_SORTIE, documentSortie);
    
  }

  getDocumentsSorties(): Observable<DocumentSortie[]> {
    return this.httpClient
    .get<DocumentSortie[]>(URL_GET_LIST_DOCUMENT_SORTIE);
   }
   getDocumentSortie(codeDocumentSortie): Observable<DocumentSortie> {
    return this.httpClient.get<DocumentSortie>(URL_GET_DOCUMENT_SORTIE + '/' + codeDocumentSortie);
  }
  deleteDocumentSortie(codeDocumentClient): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_DOCUCMENT_SORTIE + '/' + codeDocumentClient);
  }
  getDocumentsSortiesByCodeSortie(codeDocumentSortie): Observable<DocumentSortie[]> {
    return this.httpClient.get<DocumentSortie[]>(URL_GET_LIST_DOCUMENT_SORTIE_BY_CODE_SORTIE + '/' + codeDocumentSortie);
  }
  updateDocumentSortie(doc: DocumentSortie): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_DOCUMENT_SORTIE, doc);
  }
}
