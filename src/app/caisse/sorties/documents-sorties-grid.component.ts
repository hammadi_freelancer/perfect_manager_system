
import { Component, OnInit, Inject,Input, ViewChild, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {SortieService} from './sortie.service';
import {Sortie} from './sortie.model';

import {DocumentSortie} from './document-sortie.model';
import { DocumentSortieElement } from './document-sortie.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogAddDocumentSortieComponent } from './popups/dialog-add-document-sortie.component';
import { DialogEditDocumentSortieComponent } from './popups/dialog-edit-document-sortie.component';
@Component({
    selector: 'app-documents-sorties-grid',
    templateUrl: 'documents-sorties-grid.component.html',
  })
  export class DocumentsSortiesGridComponent implements OnInit, OnChanges {
     private listDocumentsSorties: DocumentSortie[] = [];
     private  dataDocumentsSortiesSource: MatTableDataSource<DocumentSortieElement>;
     private selection = new SelectionModel<DocumentSortieElement>(true, []);
    
     @Input()
     private sortie : Sortie

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private sortieService: SortieService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.sortieService.getDocumentsSortiesByCodeSortie(this.sortie.code).subscribe((listDocumentsSorties) => {
        this.listDocumentsSorties = listDocumentsSorties;
  // console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
  }
  ngOnChanges()
  {
    this.specifyListColumnsToBeDisplayed();
    
    this.sortieService.getDocumentsSortiesByCodeSortie(this.sortie.code).subscribe((listDocumentsSorties) => {
      this.listDocumentsSorties = listDocumentsSorties;
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }

  checkOneActionInvoked() {
    const listSelectedDocumentsSorties: DocumentSortie[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentSorElement) => {
    return documentSorElement.code;
  });
  this.listDocumentsSorties.forEach(documentS => {
    if (codes.findIndex(code => code === documentS.code) > -1) {
      listSelectedDocumentsSorties.push(documentS);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsSorties !== undefined) {

      this.listDocumentsSorties.forEach(docSor => {
             listElements.push( {code: docSor.code ,
          dateReception: docSor.dateReception,
          numeroDocumentSortie: docSor.numeroDocumentSortie,
          typeDocument: docSor.typeDocument,
          description: docSor.description
        } );
      });
    }
      this.dataDocumentsSortiesSource = new MatTableDataSource<DocumentSortieElement>(listElements);
      this.dataDocumentsSortiesSource.sort = this.sort;
      this.dataDocumentsSortiesSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    //  console.log('calling specifyListColumnsToBeDisplayed documents grid');
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
    
     this.columnsDefinitions = [
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocumentSortie' , displayTitle: 'N°DOC'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
      console.log(this.columnsNames);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsSortiesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsSortiesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsSortiesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsSortiesSource.paginator) {
      this.dataDocumentsSortiesSource.paginator.firstPage();
    }
  }
  showAddDocumentDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeSortie : this.sortie.code
   };
 
   const dialogRef = this.dialog.open(DialogAddDocumentSortieComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(docAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditDocumentDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeDocumentSortie : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditDocumentSortieComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(docAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Document Séléctionnée!');
    }
    }
    supprimerDocuments()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(doc, index) {
              this.sortieService.deleteDocumentSortie(doc.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar(  ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Document Séléctionnée!');
        }
    }
    refresh() {
      this.sortieService.getDocumentsSortiesByCodeSortie(this.sortie.code).subscribe((listDocsSorties) => {
        this.listDocumentsSorties = listDocsSorties;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
