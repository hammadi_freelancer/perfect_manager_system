import { Injectable } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { EntreesGridComponent } from './entrees/entrees-grid.component';
import { EntreeNewComponent } from './entrees/entree-new.component';
import { EntreeEditComponent } from './entrees/entree-edit.component';
import { SortiesGridComponent } from './sorties/sorties-grid.component';
import { SortieNewComponent } from './sorties/sortie-new.component';
import { SortieEditComponent } from './sorties/sortie-edit.component';
import { HomeCaisseComponent } from './home-caisse.component';


 
import {ModuleWithProviders  } from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';


export const caisseRoute: Routes = [
    {
        path: 'entrees',
        component: EntreesGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterEntree',
        component: EntreeNewComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerEntree/:codeEntree',
        component: EntreeEditComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'sorties',
        component: SortiesGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterSortie',
        component: SortieNewComponent,
        resolve: { 
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerSortie/:codeSortie',
        component: SortieEditComponent,
        resolve: { 
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
];

