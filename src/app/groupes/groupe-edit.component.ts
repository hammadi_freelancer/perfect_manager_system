import { Component, OnInit, Input, EventEmitter, Output, AfterContentChecked,
AfterViewChecked
} from '@angular/core';
// import { ArticleService } from './article.service';
import {Utilisateur } from '../utilisateurs/utilisateur.model';
import {UtilisateurService} from '../utilisateurs/utilisateur.service';
import {AccessRight } from '../utilisateurs/access-right.model';
import {Groupe } from './groupe.model';
import {GroupeService } from './groupe.service';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {MatSnackBar} from '@angular/material';
@Component({
  selector: 'app-groupe-edit',
  templateUrl: './groupe-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class GroupeEditComponent implements OnInit, AfterContentChecked {

// private client: Client = new Client();


private groupe:  Groupe = new  Groupe();
private sourceListAccessRights: AccessRight[];
private targetListAccessRights: AccessRight[] = [];
private sourceListUtilisateurs: Utilisateur[];
private targetListUtilisateurs: Utilisateur[] = [];
private file: any;

private etatsGroupes: any[] = [
  {value: 'Active', viewValue: 'Activé'},
  {value: 'Desactive', viewValue: 'Désactivé'},
  {value: 'EnCours', viewValue: 'En Cours'}

];
  constructor( private route: ActivatedRoute,
    private router: Router , 
    private groupesService: GroupeService, private matSnackBar: MatSnackBar,
   ) {
  }
  ngOnInit() {
    this.sourceListAccessRights = this.route.snapshot.data.accessRights;
    this.sourceListUtilisateurs = this.route.snapshot.data.utilisateurs;
    
      const codeGroupe = this.route.snapshot.paramMap.get('codeGroupe');
      this.groupesService.getGroupe(codeGroupe).subscribe((grp) => {
          this.groupe  = grp;
           const selectedAccessRights = [];
           const selectedUtilisateurs = [];
    if (this.groupe.accessRights !== undefined && this.groupe.accessRights.length !== 0) {
           this.groupe.accessRights.forEach(function(accessRight, index) {
                     const acR = this.sourceListAccessRights.filter((ac) => ac.code === accessRight.code)[0];
                     if ( acR !== undefined ) {
                     this.targetListAccessRights.push(acR);
                     this.sourceListAccessRights =  this.sourceListAccessRights.filter( (accessR) => accessR.code !== acR.code);
                     }
                 }, this);
                 }
          if (this.groupe.utilisateurs !== undefined && this.groupe.utilisateurs.length !== 0) {
              this.groupe.utilisateurs.forEach(function(utilisateur, index) {
               const uts = this.sourceListUtilisateurs.filter((us) => us.code === utilisateur.code)[0];
                    if ( uts !== undefined ) {
                 this.targetListUtilisateurs.push(uts);
                   this.sourceListUtilisateurs =  this.sourceListUtilisateurs.filter( (usr) => usr.code !== uts.code);
                            }
                        }, this);
                        }

      });
  }
  /*loadAddUtilisateurComponent() {
    this.router.navigate(['/pms/administration/ajouterUtilisateur']) ;
  
  }*/
  loadGridGroupes() {
    this.router.navigate(['/pms/administration/groupes']) ;
  }
 /* supprimerUtilisateur() {
     
      this.utilisateurService.deleteUtilisateur(this.utilisateur.code).subscribe((response) => {

        this.openSnackBar( ' Element Supprimé');
        this.loadGridUtilisateurs() ;
            });
  }*/
updateGroupe() {
  this.groupe.accessRights = [];
  this.groupe.utilisateurs = [];
  
if (this.targetListAccessRights.length !== 0) {
     this.targetListAccessRights.forEach(function(accessRight, index) {
      this.groupe.accessRights.push(accessRight) ;
     }, this);
  }
  if (this.targetListUtilisateurs.length !== 0) {
    this.targetListUtilisateurs.forEach(function(uts, index) {
     this.groupe.utilisateurs.push(uts) ;
    }, this);
 }
  this.groupesService.updateGroupe(this.groupe).subscribe((response) => {
    if (response.error_message ===  undefined) {
      this.openSnackBar( ' Groupe mis à jour ');
    } else {
      this.openSnackBar( ' Erreurs.. ');
      
     // show error message
    }
});
    
}
fileChanged($event) {
  this.file = $event.target.files[0];
  const fileReader = new FileReader();
  fileReader.onload = (e) => {
    console.log(fileReader.result);
     this.groupe.logo = fileReader.result;
    // this.filesData[indexFile] = fileData;
  };
  fileReader.readAsDataURL(this.file);
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

ngAfterContentChecked() {

}


selectedTabChanged($event) {
  if ($event.index === 4) { // Composition tab was selected
   // this.loadListArticles();
   } else if ($event.index === 1) {
    // this.loadListSchemasConfiguration() ;
   }
  }


}


