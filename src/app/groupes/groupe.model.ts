
import { BaseEntity } from '../common/base-entity.model' ;
import {AccessRight } from '../utilisateurs/access-right.model';
import {Utilisateur } from '../utilisateurs/utilisateur.model';

type TypeEtat = 'Active' | 'En Cours' | 'Desactive';

// type USER_ROLE = 'ADMIN' | 'INVITE' | 'UTILISATEUR';
export class Groupe extends BaseEntity {
  constructor( 
     public code?: string,
    public nom?: string,
    public accessRights?: AccessRight[],
    public utilisateurs?: Utilisateur[],
    public etat?: TypeEtat,
     public logo?: any
  ) {
    super();
    this.accessRights = [];
    // this.identite = {adresse: {}, { } }
  }
}
