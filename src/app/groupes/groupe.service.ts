import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';

import {Groupe} from './groupe.model' ;
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';

const URL_GET_TOTAL_GROUPES = 'http://localhost:8082/groupes/getTotalGroupes';

const  URL_GET_LIST_GROUPES = 'http://localhost:8082/groupes/getGroupes';
const  URL_GET_PAGE_GROUPES = 'http://localhost:8082/groupes/getPageGroupes';

const URL_ADD_GROUPE = 'http://localhost:8082/groupes/addGroupe';
const URL_GET_GROUPE = 'http://localhost:8082/groupes/getGroupe';

const URL_UPDATE_GROUPE = 'http://localhost:8082/groupes/updateGroupe';
const URL_DELETE_GROUPE = 'http://localhost:8082/groupes/deleteGroupe';




@Injectable({
  providedIn: 'root'
})
export class GroupeService {
 
  constructor(private httpClient: HttpClient) {

   }

   addGroupe(groupe: Groupe): Observable<any> {
    return this.httpClient.post<Groupe>(URL_ADD_GROUPE, groupe);
  }
  
  getGroupe(codeGroupe): Observable<Groupe> {
    return this.httpClient.get<Groupe>(URL_GET_GROUPE + '/' + codeGroupe);
  }
   updateGroupe(groupe: Groupe): Observable<any> {
    return this.httpClient.put<Groupe>(URL_UPDATE_GROUPE, groupe);
  }
  deleteGroupe(codeGroupe): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_GROUPE + '/' + codeGroupe);
  }
    getPageGroupes(pageNumber, nbElementsPerPage, filter): Observable<Groupe[]> {
      const filterStr = JSON.stringify(filter);
      const params = new HttpParams()
      .set('pageNumber', pageNumber)
      .set('nbElementsPerPage', nbElementsPerPage)
      .set('sort', 'name')
      .set('filter', filterStr);
      return this.httpClient
      .get<Groupe[]>(URL_GET_PAGE_GROUPES, {params});
     }
     getTotalGroupes(filter): Observable<any> {
      
          const filterStr = JSON.stringify(filter);
          const params = new HttpParams()
          .set('filter', filterStr);
          return this.httpClient
          .get<any>(URL_GET_TOTAL_GROUPES, {params});
      }

 


}
