import { Injectable } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { GroupesGridComponent } from './groupes-grid.component';
import { GroupeNewComponent } from './groupe-new.component';
import { GroupeEditComponent } from './groupe-edit.component';
import { AccessRightsResolver } from '../utilisateurs/accessRights-resolver';

import {ModuleWithProviders  } from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';

 export const  groupesRoute: Routes = [
    {
        path: 'groupes',
        component: GroupesGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterGroupe',
        component: GroupeNewComponent,
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerGroupe/:codeGroupe',
        component: GroupeEditComponent,
        resolve: { accessRights: AccessRightsResolver,
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    }
];



