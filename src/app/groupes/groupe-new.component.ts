import { Component, OnInit, Input, EventEmitter, Output,
    AfterViewChecked
    } from '@angular/core';
    // import { ArticleService } from './article.service';
    import {Groupe} from './groupe.model';
    import {GroupeService} from './groupe.service';
    
    import {Utilisateur} from '../utilisateurs/utilisateur.model';
    import {UtilisateurService} from '../utilisateurs/utilisateur.service';
    

    import {FormControl} from '@angular/forms';
    import {map, startWith} from 'rxjs/operators';
    import {Observable} from 'rxjs';
    import { Router, ActivatedRoute, ParamMap } from '@angular/router';
    import {MatSnackBar} from '@angular/material';
   //  import { SelectValues } from './select-values.interface';
    @Component({
      selector: 'app-groupe-new',
      templateUrl: './groupe-new.component.html',
      // styleUrls: ['./players.component.css']
    })
export class GroupeNewComponent implements OnInit {
    private groupe: Groupe = new Groupe();

    @Input()
    private listGroupes: any = [] ;
    private file: any;
    private etatsGroupes: any[] = [
      {value: 'Active', viewValue: 'Activé'},
      {value: 'Desactive', viewValue: 'Désactivé'},
      {value: 'EnCours', viewValue: 'En Cours'}
    
    ];
      constructor( private route: ActivatedRoute,
        private router: Router ,
         private groupesService: GroupeService,
         private matSnackBar: MatSnackBar ) {
      }
      ngOnInit() {
     
          console.log('the action data is ');
      }
    saveGroupe() {
      
        this.groupesService.addGroupe(this.groupe).subscribe((response) => {
            if (response.error_message ===  undefined) {
              // this.save.emit(response);
              this.groupe = new Groupe();
              this.openSnackBar('Groupe Enregistré');
            } else {
             // show error message
             this.openSnackBar(  'Erreurs!');
             
            }
        });
      }
    
    loadGridGroupes() {
      this.router.navigate(['/pms/administration/groupes']) ;
    }
    fileChanged($event) {
        this.file = $event.target.files[0];
        const fileReader = new FileReader();
        fileReader.onload = (e) => {
          console.log(fileReader.result);
          // this.article.image = fileReader.result;
          // this.filesData[indexFile] = fileData;
        };
        fileReader.readAsDataURL(this.file);
       }
      
    
       openSnackBar(messageToDisplay) {
        this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
            verticalPosition: 'top'});
       }
 
    }
