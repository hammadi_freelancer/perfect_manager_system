import { Component, OnInit, Output, ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
// import {AccessRight} from './access-right.model';
import {Groupe} from './groupe.model';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {GroupeService} from './groupe.service';
import { GroupeElement } from './groupe.interface';

import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
// import { DialogAddDocumentClientComponent} from './popups';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {GroupeDataFilter } from './groupe-data.filter';
import { SelectItem } from 'primeng/api';
@Component({
  selector: 'app-groupes-grid',
  templateUrl: './groupes-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class GroupesGridComponent implements OnInit {

  private groupe: Groupe =  new Groupe();
  private showFilter = false;
  private showSelectColumnsMultiSelect = false;
  private groupeDataFilter = new GroupeDataFilter();
  private selection = new SelectionModel<Groupe>(true, []);

  @Input()
  private listGroupes: any = [] ;


 selectedGroupe: Groupe ;

private deleteEnCours = false;



 // listClientsOrgin: any = [];
 private checkedAll: false;
 private showParticulier = false;
 private showEntreprise = false;
 private  dataGroupesSource: MatTableDataSource<GroupeElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay :ColumnDefinition[] = [];

 private columnsToSelect : SelectItem[];
  private  selectedColumnsDefinitions: string[];

 private selectedColumnsNames: string[];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];

private etatsGroupes: any[] = [
  {value: 'Active', viewValue: 'Activé'},
  {value: 'Desactive', viewValue: 'Désactivé'},
  {value: 'EnCours', viewValue: 'En Cours'}

]
  constructor( private route: ActivatedRoute, private groupesService: GroupeService,
    private router: Router, private matSnackBar: MatSnackBar, private dialog: MatDialog ) {
  }
  ngOnInit() {
    this.defaultColumnsDefinitions = [
      {name: 'code' , displayTitle: 'Code'},
    {name: 'nom' , displayTitle: 'Nom'},
     {name: 'etat' , displayTitle: 'Etat'},
     {name: 'description' , displayTitle: 'Déscription'}
  ];
    
    this.columnsToSelect = [
      {label: 'Code', value: 'code', title: 'Code'},
      {label: 'Nom', value: 'nom', title: 'Nom'},
      {label: 'Déscription', value: 'description', title: 'Déscription'},
      {label: 'Etat', value: 'etat', title: 'Etat'},
      
      
  ];
  this.selectedColumnsDefinitions = [
    'code',
   'nom',
   'etat',
   'description'
    ];
    this.groupesService.getPageGroupes(0, 5, null).subscribe((groupes) => {
      this.listGroupes= groupes;
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });
  

  }
  deleteGroupe(groupe) {
     // console.log('call delete !', client );
    this.groupesService.deleteGroupe(groupe.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData(null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.groupesService.getPageGroupes(0, 5, filter).subscribe((groupes) => {
    this.listGroupes = groupes;
    // this.listClients[this.listClients.length] = this.lastClientAdded;
    this.transformDataToByConsumedByMatTable();
   });
  // this.listClientsOrgin = this.listClients ;
}


checkOneActionInvoked() {
  const listSelectedGroupes: Groupe[] = [];

if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((gElement) => {
  return gElement.code;
});
this.listGroupes.forEach(gr => {
  if (codes.findIndex(code => code === gr.code) > -1) {
    listSelectedGroupes.push(gr);
  }
  });
}
// this.select.emit(listSelectedClients);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listGroupes !== undefined) {
    this.listGroupes.forEach(groupe => {
      listElements.push(
        {
          code: groupe.code ,
          nom: groupe.nom ,
          description: groupe.description ,
          etat: groupe.etat ,
          role: groupe.role
      } );
    });
  }
    this.dataGroupesSource = new MatTableDataSource<GroupeElement>(listElements);
    this.dataGroupesSource.sort = this.sort;
    this.dataGroupesSource.paginator = this.paginator;
    this.groupesService.getTotalGroupes(this.groupeDataFilter).subscribe((response) => {
     //  console.log('Total Clients   is ', response);
     if (this.dataGroupesSource.paginator !== undefined) {
       this.dataGroupesSource.paginator.length = response['totalGroupes'];
     }
     });
}

masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
    //   this.dataAccessRightsSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataGroupesSource.data.length;
  return numSelected === numRows;
}
/*applyFilter(filterValue: string) {
  this.dataAccessRightsSource.filter = filterValue.trim().toLowerCase();
  if (this.dataAccessRightsSource.paginator) {
    this.dataAccessRightsSource.paginator.firstPage();
  }
}*/
loadAddGroupeComponent() {
  this.router.navigate(['/pms/administration/ajouterGroupe']) ;

}
loadEditGroupeComponent() {
  if (this.selection.selected.length === 0) {
    this.openSnackBar(' Aucun Groupe sélectionné' );
    // this.loadData();
    } else
   {
  this.router.navigate(['/pms/administration/editerGroupe', this.selection.selected[ this.selection.selected.length - 1].code]) ;
    }
}
supprimerGroupes()
{
  if ( this.selection.selected.length !== 0) {
    this.deleteEnCours = true;
      this.selection.selected.forEach(function(groupe, index) {
          this.groupesService.deleteGroupe(groupe.code).subscribe((response) => {
            if (this.selection.selected.length - 1 === index) {
              this.deleteEnCours = false;
              this.openSnackBar( ' Groupe(s) Supprimé(s)');
               this.loadData(null);
              }
          });
    
      }, this);
    } else {
      this.openSnackBar('Aucun Groupe Séléctionné!');
    }
}
refresh()
{
  this.loadData(null);
  
}
  openSnackBar(messageToDisplay) {
 this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
     verticalPosition: 'top'});
}

changePage($event) {
  console.log('page event is ', $event);
 this.groupesService.getPageGroupes($event.pageIndex, $event.pageSize,
  this.groupeDataFilter).subscribe((groupes) => {
    this.listGroupes = groupes;
    const listElements = [];
 if (this.listGroupes !== undefined) {
    this.listGroupes.forEach(groupe => {
      listElements.push( {
        code: groupe.code ,
        nom: groupe.nom ,
        etat: groupe.etat,
        description: groupe.description
      }
      );
    });
  }
     this.dataGroupesSource= new MatTableDataSource<GroupeElement>(listElements);
     this.groupesService.getTotalGroupes(this.groupeDataFilter).
     subscribe((response) => {
      console.log('Total clients is ', response);
      // this.dataClientsSource.paginator = this.paginator;
      if (this.dataGroupesSource.paginator) {
        
       this.dataGroupesSource.paginator.length = response.totalGroupes;
      }
     });

    });
}
toggleFilterPanel() {
  this.showFilter = !this.showFilter;
}
toggleShowColumnsMultiSelect() {
 this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
}
loadFiltredData() {
 // console.log('the filter is ', this.utilisateurDataFilter);
 this.loadData(this.groupeDataFilter);
}

specifyListColumnsToBeDisplayed() {
  
   console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
   this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
 ?
    this.defaultColumnsDefinitions : [];
    this.selectedColumnsNames = [];
    
    if (this.selectedColumnsDefinitions !== undefined)
     {
       this.selectedColumnsDefinitions.forEach((colName) => { 
            const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
            const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
            const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
            this.columnsDefinitionsToDisplay.push(colDef);
           });
     }
    this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
   this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
              return colDef.displayTitle;
    });
    console.log('columns to disp:', this.columnsDefinitionsToDisplay);
    this.columnsNames = [];
    this.columnsNames[0] = 'select';
    this.columnsDefinitionsToDisplay .map((colDef) => {
     this.columnsNames.push(colDef.name);
    if (this.selectedColumnsDefinitions !== undefined) {
     this.selectedColumnsNames.push(colDef.displayTitle);
    }
 });
    this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
     return colDef.displayTitleAr;
 });
 }






}
