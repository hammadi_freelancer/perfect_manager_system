import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
 import { AppComponent } from '../app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ReactiveFormsModule } from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatChipsModule} from '@angular/material/chips';
import {MatMenuModule} from '@angular/material/menu';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {TableModule} from 'primeng/table';
import {MatSortModule} from '@angular/material/sort';
import {MatSelectModule} from '@angular/material/select';
import {MatTabsModule} from '@angular/material/tabs';
import {PickListModule} from 'primeng/picklist';
import {MultiSelectModule} from 'primeng/multiselect';

// import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';
 import { GroupesGridComponent } from './groupes-grid.component';
 import { GroupeNewComponent } from './groupe-new.component';
 import { GroupeEditComponent } from './groupe-edit.component';
 import { HomeGroupesComponent } from './home-groupes.component';
 import { JwtModule, JWT_OPTIONS  } from '@auth0/angular-jwt';
 import { JwtHelperService } from '@auth0/angular-jwt';
import {FormsModule } from '@angular/forms';
// import { commonRoute } from './common.route';
 import {APP_BASE_HREF} from '@angular/common';

   import { AccessRightsResolver } from '../utilisateurs/accessRights-resolver';
  //  import { Resolver } from '../utilisateurs/ut';
   
 // const BASE_URL = [{provide: APP_BASE_HREF, useValue: '/common'}];



 import {MatPaginatorModule} from '@angular/material/paginator';
 import { RouterModule, provideRoutes} from '@angular/router';
 // import { CommunicationService } from './magasins/communication.service';
 import {MatSidenavModule} from '@angular/material/sidenav';
 import {MatRadioModule} from '@angular/material/radio';
@NgModule({
  declarations: [
  
    GroupesGridComponent,
    HomeGroupesComponent,
    GroupeEditComponent,
    GroupeNewComponent,
 
  ],
  imports: [
    BrowserModule, HttpClientModule, MatDatepickerModule, MatNativeDateModule,
     BrowserAnimationsModule, ReactiveFormsModule, MatAutocompleteModule,
    MatInputModule, MatCardModule, MatCheckboxModule, MatButtonModule, MatIconModule,
    MatMenuModule, MatProgressSpinnerModule, MatTableModule, RouterModule,
    FormsModule, TableModule , MatSortModule, MatSelectModule, MatTabsModule,
     PickListModule, MatPaginatorModule, MatSidenavModule, MatRadioModule, MatChipsModule,
     JwtModule, MultiSelectModule
     

],
  providers: [MatNativeDateModule, AccessRightsResolver, JwtHelperService ,  // BASE_URL
  ],
  exports : [
    RouterModule,

  ],
  bootstrap: [AppComponent]
})
export class GroupesModule { }
