import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
 import { AppComponent } from '../app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ReactiveFormsModule } from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatTableModule} from '@angular/material/table';

import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatRadioModule} from '@angular/material/radio';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';

import { RouterModule} from '@angular/router';
import { PayementComponent } from './payements/payement.component';
import { PayementsGridComponent } from './payements/payements-grid.component';

import { GestionPayementsComponent } from './payements/gestion-payements.component';
import { DocumentsModule } from '../documents/documents.module';
import {TableModule} from 'primeng/table';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatExpansionPanel} from '@angular/material/expansion';
import {MatMenuModule} from '@angular/material/menu';
import {MatSnackBarModule} from '@angular/material/snack-bar';

import { SharedComponentModule } from '../shared-components/shared-components.module';

// import { payementsRoute } from './payements.route';
import {FormsModule } from '@angular/forms';
import {APP_BASE_HREF} from '@angular/common';
import { financeRoutingModule } from './finance.routing';
import { MainComponent } from './main.component';

/*const ENTITY_STATES = [
  ...payementsRoute
];*/
@NgModule({
  declarations: [
    PayementComponent,
    PayementsGridComponent,
    GestionPayementsComponent,
    MainComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, MatDatepickerModule, MatNativeDateModule,
     BrowserAnimationsModule, ReactiveFormsModule, MatAutocompleteModule,
    MatInputModule, MatCardModule, MatSelectModule, MatIconModule , MatCheckboxModule ,
    FormsModule, MatRadioModule, MatMenuModule, MatSnackBarModule,
    DocumentsModule , TableModule , MatExpansionModule, SharedComponentModule, MatTableModule,
    MatSortModule, MatPaginatorModule,
    financeRoutingModule
   // RouterModule.forRoot(ENTITY_STATES, {useHash: false})
  ],
  providers: [MatNativeDateModule, MatExpansionPanel
],
  exports: [
    RouterModule,
    // GestionCreditsComponent,
    GestionPayementsComponent,
    MainComponent, //CreditHomeComponent
  ],
  bootstrap: [AppComponent]
})
export class FinanceModule { }
