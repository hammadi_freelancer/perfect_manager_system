export class Payement {
    constructor(public code?: string, public datePayement?: string, public cinClient?: string,
    public sommePaye?: number, public sommeReste?: number ,  public tranche?: number, public dateTranche?: string ) {}
}
