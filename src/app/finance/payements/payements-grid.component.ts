import { Component, OnInit } from '@angular/core';
// import { ArticleService } from './article.service';
import {Payement} from './payement.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {PayementService} from './payement.service';



@Component({
  selector: 'app-payements-grid',
  templateUrl: './payements-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class PayementsGridComponent implements OnInit {

private payement: Payement = new Payement();
 listPayements: any;
 listPayementsOrgin: any;
 filterCIN: any;
  constructor( private route: ActivatedRoute, private payementsService: PayementService,
    private router: Router ) {
  }
  ngOnInit() {
    this.payementsService.getPayements().subscribe((ventes) => {
      this.listPayements = ventes;
      this.listPayementsOrgin = this.listPayements ;
     });
  }
reloadData() {
    if (this.filterCIN !== undefined && this.filterCIN !== '' ) {
    const th = this;
    this.listPayements = this.listPayementsOrgin.filter(function(value, index) {
        console.log('the value is :', value.cinClient);
        console.log('the filter is ', th.filterCIN);
        return value.cinClient === th.filterCIN ;
    });
}
}
savePayement() {
    console.log(this.payement);
}
}
