import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';

import {Payement} from './payement.model' ;
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_PAYEMENTS = 'http://localhost:8082/finance/payements/getPayements';
const URL_ADD_PAYEMENT = 'http://localhost:8082/finance/payements/addPayement';

@Injectable({
  providedIn: 'root'
})
export class PayementService {

  constructor(private httpClient: HttpClient) {

   }

   getPayements(): Observable<Payement[]> {
    return this.httpClient
    .get<Payement[]>(URL_GET_PAYEMENTS);
   }
   addPayement(payement: Payement): Observable<Payement> {
    return this.httpClient.post<Payement>(URL_ADD_PAYEMENT, payement);
  }
  /* getVentes(): Array<Vente> {
       const listVentes = new Array();
       listVentes.push(new Vente('azr', '23-03-1985', '077777', 899888 , 8777, 5666, '6/9/2009' ));
       listVentes.push(new Vente('qksqkskq', '23-03-1985', '06666', 899888 , 8777 ));
       listVentes.push(new Vente('jjjjj', '23-03-1985', '077777', 899888 , 8777 ));
       return listVentes;
   }*/
}
