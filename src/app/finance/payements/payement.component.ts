import { Component, OnInit } from '@angular/core';
// import { ArticleService } from './article.service';
import {Payement} from './payement.model';
import {PayementService} from './payement.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';



@Component({
  selector: 'app-payement',
  templateUrl: './payement.component.html',
  // styleUrls: ['./players.component.css']
})
export class PayementComponent implements OnInit {

private payement: Payement = new Payement();
  constructor( private route: ActivatedRoute,
    private router: Router, private payementService: PayementService) {
  }
  ngOnInit() {
  }
savePayement() {
    console.log(this.payement);
  this.payementService.addPayement(this.payement).subscribe();
}
}
