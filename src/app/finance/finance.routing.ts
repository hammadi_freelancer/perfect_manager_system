import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { PayementComponent } from './payements/payement.component';

import { PayementsGridComponent } from './payements/payements-grid.component';
import { GestionPayementsComponent } from './payements/gestion-payements.component';

import {APP_BASE_HREF} from '@angular/common';

 const financeRoute: Routes = [
    {
        path: 'finance/payements/ajouterPayement',
        component: PayementComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'finance/payements/listerPayements',
        component: PayementsGridComponent,
        data: {
           // authorities: ['ROLE_ASSET_ASSIGNMENT'],
            // pageTitle: 'safeleasegtwApp.asset.home.titleAssign'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'finance/payements/gestionPayements',
        component: GestionPayementsComponent,
        data: {
           // authorities: ['ROLE_ASSET_ASSIGNMENT'],
            // pageTitle: 'safeleasegtwApp.asset.home.titleAssign'
        },
        // canActivate: [UserRouteAccessService]
    }

];
export const financeRoutingModule: ModuleWithProviders = RouterModule.forRoot(financeRoute);
// payementsRouting.providers = [{ provide: APP_BASE_HREF, useValue: '/payements'} ] ;




