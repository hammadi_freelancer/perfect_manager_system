export interface CompteClientElement {
         code: string ;
         numeroCompteClient: string;
         cinClient: string ;
         nomClient: string ;
         prenomClient: string ;
         raisonSocialClient: string ;
         profession: string ;
         dateOuverture: string ;
         dateCloture: string ;
         chiffreAffaires: number;
         valeurVentes: number;
         valeurCredits: number;
         valeurPaiements: number;
         etat: string ;
         description: string ;
        }
