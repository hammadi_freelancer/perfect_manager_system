import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {CompteClient} from './compte-client.model';

import {ComptesClientsService} from './comptes-clients.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {MatSnackBar} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { ClientFilter } from '../ventes/clients/client.filter';
import { ClientService } from '../ventes/clients/client.service';


import {Client} from '../ventes/clients/client.model';

@Component({
  selector: 'app-compte-client-new',
  templateUrl: './compte-client-new.component.html',
  // styleUrls: ['./players.component.css']
})
export class CompteClientNewComponent implements OnInit {

 private compteClient: CompteClient =  new CompteClient();
 private saveEnCours = false;
 private clientFilter: ClientFilter;
 private saveClientEnCours = false;
 
 private listClients: Client[];
private etatsCompteClient: any[] = [
  {value: 'Ouvert', viewValue: 'Ouvert'},
  {value: 'Initial', viewValue: 'Initiale'},
  {value: 'EnCoursValidation', viewValue: 'En Cours de Validation'},
  {value: 'Valide', viewValue: 'Validé'},
  {value: 'Cloture', viewValue: 'Cloturé'}
];
private selectCin: boolean;
private selectNom: boolean;
private selectPrenom: boolean;
private selectMatricule: boolean;
private selectRegistre: boolean;
private selectRaison: boolean;
private addClient: boolean;
private addEntreprise: boolean;



  constructor( private route: ActivatedRoute, private clientService: ClientService,
    private router: Router, private comptesClientsService: ComptesClientsService,
   private matSnackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.listClients = this.route.snapshot.data.clients;
    
     this.clientFilter = new ClientFilter(this.listClients);
     this.selectCin = true;
     
  }
  saveCompteClient() {

    if( this.compteClient.etat === undefined || this.compteClient.etat === 'Ouvert' ) {
      this.compteClient.etat = 'Initial';
    }
    // this.saveEnCours = true;
    this.saveEnCours = true;
    this.comptesClientsService.addCompteClient(this.compteClient).subscribe((response) => {
      this.saveEnCours = false;
      
        if (response.error_message ===  undefined) {
          console.log('undefined error message');
          // this.save.emit(response);
          this.compteClient =  new CompteClient();
          this.openSnackBar(  'Compte Client Enregistré');
        } else {
         // show error message
         this.openSnackBar(  'Erreurs: Opération Echouée');
         
        }
    });
  }


  vider() {
    this.compteClient = new CompteClient();
  }
  openSnackBar(messageToDisplay) {
   this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
       verticalPosition: 'top'});
  }
loadGridComptesClients() {
  this.router.navigate(['/pms/finance/comptesClients']) ;
}
fillDataNomPrenomClient()
{
  const listFiltred  = this.listClients.filter((client) => (client.cin === this.compteClient.client.cin));
  this.compteClient.client.nom = listFiltred[0].nom;
  this.compteClient.client.prenom = listFiltred[0].prenom;
}

fillDataCINPrenomClient()
{
  const listFiltred  = this.listClients.filter((client) => (client.nom === this.compteClient.client.nom));
  this.compteClient.client.cin = listFiltred[0].cin;
  this.compteClient.client.prenom = listFiltred[0].prenom;
}
fillDataCINNomClient()
{
  const listFiltred  = this.listClients.filter((client) => (client.prenom === this.compteClient.client.prenom));
  const num =   Math.floor(Math.random() * Math.floor(listFiltred.length));
  this.compteClient.client.cin = listFiltred[num].cin;
  this.compteClient.client.nom= listFiltred[num].nom;
}
fillDataMatriculeRaisonClient() {
  const listFiltred  = this.listClients.filter((client) => (client.registreCommerce === this.compteClient.client.registreCommerce));
  this.compteClient.client.matriculeFiscale = listFiltred[0].matriculeFiscale;
  this.compteClient.client.raisonSociale = listFiltred[0].raisonSociale;
}
fillDataMatriculeRegistreCommercial()
{
  const listFiltred  = this.listClients.filter((client) => (client.raisonSociale === this.compteClient.client.raisonSociale));
  this.compteClient.client.matriculeFiscale = listFiltred[0].matriculeFiscale;
  this.compteClient.client.registreCommerce = listFiltred[0].registreCommerce;
}
fillDataRegistreCommercialRaison()
{
  const listFiltred  = this.listClients.filter((client) => (client.matriculeFiscale === this.compteClient.client.matriculeFiscale));
  this.compteClient.client.raisonSociale = listFiltred[0].raisonSociale;
  this.compteClient.client.registreCommerce = listFiltred[0].registreCommerce;
}


activateSelectNom() {
    
   this.selectCin = false;
   this.selectNom = true;
   this.selectPrenom = false;
   this.selectRegistre = false;
   this.selectMatricule = false;
   this.selectRaison = false;
   this.addClient = false;
   this.addEntreprise = false;


    
   
 }
 activateSelectPrenom() {
     this.selectCin = false;
  this.selectNom = false;
  this.selectPrenom = true;
  this.selectRegistre = false;
  this.selectMatricule = false;
  this.selectRaison = false;
  this.addClient = false;
  this.addEntreprise = false;
    
}
activateSelectCIN() {
  
    
  this.selectCin = true;
  this.selectNom = false;
  this.selectPrenom = false;
  this.selectRegistre = false;
  this.selectMatricule = false;
  this.selectRaison = false;
  this.addClient = false;
  this.addEntreprise = false;
  
  
}
activateSelectRegistre() {
 
    
  this.selectCin = false;
  this.selectNom = false;
  this.selectPrenom = false;
  this.selectRegistre = true;
  this.selectMatricule = false;
  this.selectRaison = false;
  this.addClient = false;
  this.addEntreprise = false;
    
}
activateSelectMatricule() {
 
  this.selectCin = false;
  this.selectNom = false;
  this.selectPrenom = false;
  this.selectRegistre = false;
  this.selectMatricule = true;
  this.selectRaison = false;
  this.addClient = false;
  this.addEntreprise = false;
 
}
activateSelectRaison() {

  this.selectCin = false;
  this.selectNom = false;
  this.selectPrenom = false;
  this.selectRegistre = false;
  this.selectMatricule = false;
  this.selectRaison = true;
  this.addClient = false;
  this.addEntreprise = false;
 
}
activateAddClient()
 {
  this.selectCin = false;
  this.selectNom = false;
  this.selectPrenom = false;
  this.selectRegistre = false;
  this.selectMatricule = false;
  this.selectRaison = false;
  this.addClient = true;
  this.addEntreprise = false;
  this.compteClient.client = new Client();
}

activateAddEntreprise() {
    
  this.selectCin = false;
  this.selectNom = false;
  this.selectPrenom = false;
  this.selectRegistre = false;
  this.selectMatricule = false;
  this.selectRaison = false;
  this.addClient = false;
  this.addEntreprise = true;
  this.compteClient.client = new Client();
}
saveClient() {
  
   // console.log(this.client);
   this.saveClientEnCours = true;
   this.clientService.addClient(this.compteClient.client).subscribe((response) => {
     this.saveClientEnCours = false;
       if (response.error_message ===  undefined) {
         // this.save.emit(response);
        // this.client = new Client();
         this.openSnackBar(  'Client Enregistré');
       } else {
        // show error message
        this.openSnackBar(  'Erreurs!:Essayer une autre fois');
        
       }
   });
}

























}
