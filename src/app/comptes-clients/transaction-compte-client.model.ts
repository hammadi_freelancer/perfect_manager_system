
import { BaseEntity } from '../common/base-entity.model' ;

type TypeTransaction =   'PaiementVente' | 'Credit' |
'Vente' | 'PaiementCredit' | 'PaiementACompte' |'';


 type Etat = 'Initial' | 'Valide' | 'Annule'

export class TransactionCompteClient extends BaseEntity {
    constructor(
        public code?: string,
        public codeCompteClient?: string,
         public dateTransactionObject?: Date,
         public dateTransaction?: string,
         public valeurTransaction?: number,
         public type?: TypeTransaction,
     ) {
         super();
       }
}
