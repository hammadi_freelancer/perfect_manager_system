
export class CompteClientDataFilter
{
    numeroCompteClient: string;
    profession: string;
    chiffreAffaires: string;
    dateOuvertureFromObject: Date;
    dateOuvertureToObject: Date;
    dateClotureFromObject: Date;
    dateClotureToObject: Date;
    etat: string;
    cinClient: string;
    nomClient: string;
    prenomClient: string;
    raisonClient: string;
    matriculeClient: string;
    registreClient: string; 
}

