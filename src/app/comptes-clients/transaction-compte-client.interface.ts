
export interface TransactionCompteClientElement {
         code: string ;
         codeCompteClient: string ;
         dateTransaction: string;
         valeurTransaction: number;
         type: string;
          description?: string;

}
