import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,
  AfterViewInit , AfterContentInit,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {CompteClient} from './compte-client.model';

import {ComptesClientsService} from './comptes-clients.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { MatDatepickerInput } from '@angular/material/datepicker';
import { MatSnackBar } from '@angular/material';
import { MatDatepicker } from '@angular/material/datepicker';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { ClientFilter } from '../ventes/clients/client.filter';
import {Client} from '../ventes/clients/client.model';
@Component({
  selector: 'app-compte-client-edit',
  templateUrl: './compte-client-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class CompteClientEditComponent implements OnInit  {

  private compteClient: CompteClient =  new CompteClient();
  private listClients: Client[];
  private clientFilter: ClientFilter;
  
  private tabsDisabled = true;
  private updateEnCours = false;
  private etatsCompteClient: any[] = [
    {value: 'Initial', viewValue: 'Initiale'},
    {value: 'EnCoursValidation', viewValue: 'En Cours de Validation'},
    {value: 'Valide', viewValue: 'Validé'},
    {value: 'Cloture', viewValue: 'Cloturé'},
    {value: 'Annule', viewValue: 'Annulé'}
  ];
  constructor( private route: ActivatedRoute,
    private router: Router, private comptesClientsService: ComptesClientsService, private matSnackBar: MatSnackBar,
    public dialog: MatDialog
   ) {
  }
  ngOnInit() {
    this.listClients = this.route.snapshot.data.clients;
    
     this.clientFilter = new ClientFilter(this.listClients);

    const codeCompteClient = this.route.snapshot.paramMap.get('codeCompteClient');
    this.comptesClientsService.getCompteClient(codeCompteClient).subscribe((compteClient) => {
             this.compteClient = compteClient;
          
             if (this.compteClient.etat === 'Initial')
              {
                this.tabsDisabled = true;
                this.etatsCompteClient = [
                  {value: 'Initial', viewValue: 'Initial'},
                  {value: 'Valide', viewValue: 'Validé'},
                  {value: 'Annule', viewValue: 'Annulé'},
                  
                ];
              }
              if (this.compteClient.etat === 'Cloture')
                {
                  this.tabsDisabled = false;
                  this.etatsCompteClient= [
                    {value: 'Cloture', viewValue: 'Cloturé'},
                    {value: 'Annule', viewValue: 'Annulé'},
                    
                  ];
                }
            if (this.compteClient.etat === 'Valide')
              {
                this.tabsDisabled = false;
                this.etatsCompteClient = [
                  {value: 'Annule', viewValue: 'Annulé'},
                  {value: 'Valide', viewValue: 'Validé'},
                  {value: 'Cloture', viewValue: 'Cloturé'},
                  
                ];
              }
              if (this.compteClient.etat === 'Annule')
                {
                  this.etatsCompteClient = [
                    {value: 'Annule', viewValue: 'Annulé'},
                    {value: 'Valide', viewValue: 'Validé'},
                  ];
                  this.tabsDisabled = true;
               
                }
 
    });
  }
 
  loadAddCompteClientComponent() {
    this.router.navigate(['/pms/finance/ajouterCompteClient']) ;
  }
  loadGridComptesClients() {
    this.router.navigate(['/pms/finance/comptesClients']) ;
  }
  supprimerCompteClient() {
      this.comptesClientsService.deleteCompteClient(this.compteClient.code).subscribe((response) => {

        this.openSnackBar( ' Compte Client Supprimé');
        this.loadGridComptesClients() ;
            });
  }
updateCompteClient() {
  this.updateEnCours = true;
  this.comptesClientsService.updateCompteClient(this.compteClient).subscribe((response) => {
    this.updateEnCours = false;
    if (response.error_message ===  undefined) {

      if (this.compteClient.etat === 'Initial')
        {
          this.tabsDisabled = true;
          this.etatsCompteClient = [
            {value: 'Initial', viewValue: 'Initial'},
            {value: 'Valide', viewValue: 'Validé'},
            {value: 'Annule', viewValue: 'Annulé'},
            
          ];
        }
        if (this.compteClient.etat === 'Cloture')
          {
            this.tabsDisabled = false;
            this.etatsCompteClient = [
              {value: 'Cloture', viewValue: 'Cloturé'},
              {value: 'Annule', viewValue: 'Annulé'},
              
            ];
          }
      if (this.compteClient.etat === 'Valide')
        {
          this.tabsDisabled = false;
          this.etatsCompteClient = [
            {value: 'Annule', viewValue: 'Annulé'},
            {value: 'Valide', viewValue: 'Validé'},
            {value: 'Cloture', viewValue: 'Cloturé'},
            
          ];
        }
        if (this.compteClient.etat === 'Annule')
          {
            this.etatsCompteClient = [
              {value: 'Annule', viewValue: 'Annulé'},
              {value: 'Valide', viewValue: 'Validé'},
            ];
            this.tabsDisabled = true;
         
          }
      this.openSnackBar( ' Compte Client mis à jour ');
    
    } else {
      this.openSnackBar( ' Erreurs:Opération Echouée ');
    }
});
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

 selectedTabChanged($event) {
  if ($event.index === 4) { // Composition tab was selected
   // this.loadListArticles();
   } else if ($event.index === 1) {
    //  this.loadListSchemasConfiguration() ;
   }
  }
}
