import { Injectable } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { CompteClientNewComponent } from './compte-client-new.component';
import { CompteClientEditComponent } from './compte-client-edit.component';
import { ComptesClientsGridComponent } from './comptes-clients-grid.component';
import { HomeComptesClientsComponent } from './home-comptes-clients.component' ;
import { ClientsResolver } from '../ventes/clients-resolver';


import {ModuleWithProviders  } from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';

 export const comptesClientsRoute: Routes = [
    {
        path: 'comptesClients',
        component: ComptesClientsGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterCompteClient',
        component: CompteClientNewComponent,
        resolve: {
            clients: ClientsResolver,
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerCompteClient/:codeCompteClient',
        component: CompteClientEditComponent,
        resolve: {
            clients: ClientsResolver,
         },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
















];
// export const ventesRoutingModule: ModuleWithProviders = RouterModule.forRoot(ventesRoute);
// commonRouting.providers = [{ provide: APP_BASE_HREF, useValue: '/common'} ] ;


