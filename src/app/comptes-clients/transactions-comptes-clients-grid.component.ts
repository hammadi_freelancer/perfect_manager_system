
import { Component, OnInit, Inject,Input, ViewChild, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {ComptesClientsService} from './comptes-clients.service';
import {CompteClient} from './compte-client.model';

import {TransactionCompteClient} from './transaction-compte-client.model';
import { TransactionCompteClientElement } from './transaction-compte-client.interface';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogAddTransactionCompteClientComponent } from './popups/dialog-add-transaction-compte-client.component';
import { DialogEditTransactionCompteClientComponent } from './popups/dialog-edit-transaction-compte-client.component';
@Component({
    selector: 'app-transactions-comptes-clients-grid',
    templateUrl: 'transactions-comptes-clients-grid.component.html',
  })
  export class TransactionsComptesClientsGridComponent implements OnInit, OnChanges {
     private listTransactionsComptesClients: TransactionCompteClient[] = [];
     private  dataTransactionsComptesClientsSource: MatTableDataSource<TransactionCompteClientElement>;
     private selection = new SelectionModel<TransactionCompteClientElement>(true, []);
    
     @Input()
     private compteClient : CompteClient

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private comptesClientsService: ComptesClientsService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.comptesClientsService.getTransactionsCompteClientByCodeCompteClient(this.compteClient.code).subscribe((listTrans) => {
        this.listTransactionsComptesClients= listTrans;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
  }
  ngOnChanges()
  {
    this.specifyListColumnsToBeDisplayed();
    
    this.comptesClientsService.getTransactionsCompteClientByCodeCompteClient(this.compteClient.code).subscribe((listTrans) => {
      this.listTransactionsComptesClients = listTrans;
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }

  checkOneActionInvoked() {
    const listSelectedTransactions: TransactionCompteClient[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((transCC) => {
    return transCC.code;
  });
  this.listTransactionsComptesClients.forEach(tr => {
    if (codes.findIndex(code => code === tr.code) > -1) {
      listSelectedTransactions.push(tr);
    }
    });
  }
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listTransactionsComptesClients !== undefined) {

      this.listTransactionsComptesClients.forEach(tr => {
             listElements.push( {code: tr.code ,
              dateTransaction: tr.dateTransaction,
              valeurTransaction: tr.valeurTransaction,
               type: tr.type,
        } );
      });
    }
      this.dataTransactionsComptesClientsSource = new MatTableDataSource<TransactionCompteClientElement>(listElements);
      this.dataTransactionsComptesClientsSource.sort = this.sort;
      this.dataTransactionsComptesClientsSource.paginator = this.paginator;
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [
       {name: 'code' , displayTitle: 'Code'},
     {name: 'dateTransaction' , displayTitle: 'Date'},
     {name: 'valeurTransaction' , displayTitle: 'Valeur'},
     {name: 'type' , displayTitle: 'Type'},
   
      ];

      this.columnsNames = [];
     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataTransactionsComptesClientsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataTransactionsComptesClientsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataTransactionsComptesClientsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataTransactionsComptesClientsSource.paginator) {
      this.dataTransactionsComptesClientsSource.paginator.firstPage();
    }
  }
  showAddTransactionDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeCompteClient : this.compteClient.code
   };
 
   const dialogRef = this.dialog.open(DialogAddTransactionCompteClientComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(trAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditTransactionDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeTransactionCompteClient : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditTransactionCompteClientComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(trAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucune Transaction Séléctionnée!');
    }
    }
    supprimerTransactions()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(trC, index) {
              this.comptesClientsService.deleteTransactionCompteClient(trC.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar(' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucune Transaction Séléctionnée!');
        }
    }
    refresh() {
      this.comptesClientsService.getTransactionsCompteClientByCodeCompteClient(this.compteClient.code).subscribe((listTr) => {
        this.listTransactionsComptesClients = listTr;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
