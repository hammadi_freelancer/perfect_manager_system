
import { BaseEntity } from '../common/base-entity.model' ;
import { Client } from '../ventes/clients/client.model';
type Etat = 'Initial' | 'Valide' | 'Annule' | 'EnCoursValidation' | 'Ouvert' | 'Cloture'

export class CompteClient extends BaseEntity {
    constructor(
        public code?: string,
       
        public numeroCompteClient?: string,
        public client ?: Client,
        public profession ?: string,
        public chiffreAffaires ?: number,
        public valeurVentes ?: number,
        public valeurCredits ?: number,
        public valeurPaiements ?: number,
        public dateOuvertureObject ?: Date,
        public dateOuverture ?: string,
        
        public dateClotureObject ?: Date ,
        public dateCloture ?: string ,
        
         public etat?: Etat,
    // public listTranches?: Tranche[]
     ) {
         super();
         this.chiffreAffaires =  Number(0);
         this.valeurVentes =  Number(0);
         this.valeurPaiements =  Number(0);
         this.valeurCredits =  Number(0);
         this.client = new Client();
    }
 
}
