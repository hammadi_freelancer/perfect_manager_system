
import { BaseEntity } from '../common/base-entity.model' ;

type TypeDocument =   'Cheque' | 'Traite' |'FactureVentes' |
'BonLivraison' | 'BonReception' | 'RecuPayement' |  'RecuLivraison' | 'Autre';

export class DocumentCompteClient extends BaseEntity {
    constructor(
        public code?: string,
         public codeCompteClient?: string,
         public numeroDocument?: string,
        public dateReceptionObject?: Date,
        public dateReception?: string,
        public typeDocument?: TypeDocument,
        public fileExtension?: string,
        public imageFace1?: any,
        public imageFace2?: any,
     ) {
         super();
        
       }
  
}
