import { Component, OnInit, Output,ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {CompteClient} from './compte-client.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {ComptesClientsService} from './comptes-clients.service';
import { CompteClientElement } from './compte-client.interface';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {CompteClientDataFilter} from './compte-client-data.filter';
import { SelectItem } from 'primeng/api';

@Component({
  selector: 'app-comptes-clients-grid',
  templateUrl: './comptes-clients-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class ComptesClientsGridComponent implements OnInit {

  private compteClient: CompteClient =  new CompteClient();
  private compteClientDataFilter = new CompteClientDataFilter();
  private showSelectColumnsMultiSelect = false;
  private showFilter = false;
  private showFilterClient = false;
  private selection = new SelectionModel<CompteClientElement>(true, []);
  
@Output()
select: EventEmitter<CompteClient[]> = new EventEmitter<CompteClient[]>();

//private lastClientAdded: Client;

 selectedCompteClient: CompteClient ;


 @Input()
 private listComptesClients: CompteClient[] = [] ;
 private deleteEnCours = false;
 private checkedAll: false;
 private  dataComptesClientsSource: MatTableDataSource<CompteClientElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
 
 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay :ColumnDefinition[] = [];

 private columnsToSelect : SelectItem[];
  private  selectedColumnsDefinitions: string[];

 private selectedColumnsNames: string[];
  constructor( private route: ActivatedRoute, private comptesClientsService: ComptesClientsService,
    private router: Router, private matSnackBar: MatSnackBar ) {
  }
  ngOnInit() {
    this.columnsToSelect = [
      {label: 'N° Compte', value: 'numeroCompteClient', title: 'N°Compte'},
      {label: 'Date Ouverture', value: 'dateOuverture', title: 'Date Ouverture'},
      {label: 'Nom Client', value: 'nomClient', title: 'Nom Client'},
      {label: 'Prénom Client', value: 'prenomClient', title: 'Prénom Client'},
      {label: 'CIN Client', value: 'cinClient', title: 'CIN Client'},  
      {label: 'Valeur Ventes', value: 'valeurVentes', title: 'Valeur Ventes'},
      {label: 'Valeur Paiements', value: 'valeurPaiements', title: 'Valeur Paiements'},
      {label: 'Valeur Credits', value: 'valeurCredits', title: 'Valeur Credits'},
      {label: 'Rs.Soc.Client', value:  'raisonClient', title: 'Rs.Soc.Client' },
      {label: 'Matr.Client', value:  'matriculeClient', title: 'Matr.Client' },
      {label: 'Reg.Client', value:  'registreClient', title: 'Reg.Client' },
      {label: 'Date Clôture', value: 'dateCloture', title: 'Date Clôture' },
      {label: 'Déscription', value: 'description', title: 'Déscription' },
      {label: 'Etat', value:  'etat', title: 'Etat' },
  
  ];
  this.defaultColumnsDefinitions = [
    {name: 'numeroCompteClient' , displayTitle: 'N°Compte'},
    {name: 'dateOuverture' , displayTitle: 'Date Ouverture'},
     {name: 'nomClient' , displayTitle: 'Nom Cl.'},
    {name: 'prenomClient' , displayTitle: 'Prénom Cl.'},
    {name: 'cinClient' , displayTitle: 'CIN Cl.'},
    {name: 'valeurVentes' , displayTitle: 'Valeur Ventes'},
    {name: 'valeurCredits' , displayTitle: 'Valeur Credits'},
    {name: 'valeurPaiements' , displayTitle: 'Valeur Paiements'},
    
    {name: 'matriculeClient' , displayTitle: 'Matr. Cl.'},
    {name: 'raisonClient' , displayTitle: 'Raison Cl.'},
    {name: 'registreClient' , displayTitle: 'Registre Cl.'},
  
     {name: 'modePaiement' , displayTitle: 'Mode Paiement'},
     {name: 'etat', displayTitle: 'Etat' },
     {name: 'type', displayTitle: 'Type' },
     {name: 'livre', displayTitle: 'Livré' },
     {name: 'recu', displayTitle: 'Reçu' }
     
    ];
  this.selectedColumnsDefinitions = [
    'numeroCompteClient',
    'nomClient' , 
    'prenomClient', 
    'cinClient' , 
   'valeurVentes' , 
   'valeurPaiements',
    'etat'
    ];
    this.comptesClientsService.getPageComptesClients(0, 5,null).
    subscribe((comptesClients) => {
      this.listComptesClients = comptesClients;
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });
  }
  deleteCompteClient(compteClient) {
     //  console.log('call delete !', credit );
    this.comptesClientsService.deleteCompteClient(compteClient.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData(null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.comptesClientsService.getPageComptesClients(0, 5,filter )
  .subscribe((comptesClients) => {
    this.listComptesClients = comptesClients;
    this.transformDataToByConsumedByMatTable();
   });
  // this.listClientsOrgin = this.listClients ;
}


checkOneActionInvoked() {
  const listSelectedComptesClients: CompteClient[] = [];

if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((compteElement) => {
  return compteElement.code;
});
this.listComptesClients.forEach(compteCl => {
  if (codes.findIndex(code => code === compteCl.code) > -1) {
    listSelectedComptesClients.push(compteCl);
  }
  });
}
this.select.emit(listSelectedComptesClients);

}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  this.dataComptesClientsSource = null;
  if (this.listComptesClients !== undefined) {
   
    this.listComptesClients.forEach(compteClient => {
           listElements.push( {code: compteClient.code ,
            numeroCompteClient: compteClient.numeroCompteClient,
            dateOuverture: compteClient.dateOuverture ,
            dateCloture: compteClient.dateCloture ,
            cinClient: compteClient.client.cin,
            nomClient: compteClient.client.nom,
            prenomClient: compteClient.client.prenom,
            raisonSocialClient: compteClient.client.raisonSociale,
            chiffreAffaires: compteClient.chiffreAffaires ,
            profession: compteClient.profession ,
            valeurVentes: compteClient.valeurVentes ,
            valeurCredits: compteClient.valeurCredits ,
            valeurPaiements: compteClient.valeurPaiements ,
            etat: compteClient.etat === 'Valide' ?  'Validé' :
            compteClient.etat === 'Annule' ? 'Annulé' :
            compteClient.etat === 'EnCoursValidation' ?
             'En Cours De Validation' :   compteClient.etat === 'Cloture' ? 'Cloturé' :
            compteClient.etat === 'Initial' ? 'Initiale' : 'Ouvert',
            description: compteClient.description,

      } );
    });
  }
    this.dataComptesClientsSource = new MatTableDataSource<CompteClientElement>(listElements);
    this.dataComptesClientsSource.sort = this.sort;
    this.dataComptesClientsSource.paginator = this.paginator;
    this.comptesClientsService.getTotalComptesClients(this.compteClientDataFilter).
    subscribe((response) => {
    //  console.log('Total credits is ', response);
      this.dataComptesClientsSource.paginator.length = response.totalComptesClients;
    });
}
specifyListColumnsToBeDisplayed() {
  
   console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
   this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
 ?
    this.defaultColumnsDefinitions : [];
    this.selectedColumnsNames = [];
    
    if (this.selectedColumnsDefinitions !== undefined)
     {
       this.selectedColumnsDefinitions.forEach((colName) => { 
            const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
            const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
            const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
            this.columnsDefinitionsToDisplay.push(colDef);
           });
     }
    this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
   this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
              return colDef.displayTitle;
    });
    console.log('columns to disp:', this.columnsDefinitionsToDisplay);
    this.columnsNames = [];
    this.columnsNames[0] = 'select';
    this.columnsDefinitionsToDisplay .map((colDef) => {
     this.columnsNames.push(colDef.name);
    if (this.selectedColumnsDefinitions !== undefined) {
     this.selectedColumnsNames.push(colDef.displayTitle);
    }
 });
    this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
     return colDef.displayTitleAr;
 });
 }

masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataComptesClientsSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataComptesClientsSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataComptesClientsSource.filter = filterValue.trim().toLowerCase();
  if (this.dataComptesClientsSource.paginator) {
    this.dataComptesClientsSource.paginator.firstPage();
  }
}

loadAddCompteClientComponent() {
  this.router.navigate(['/pms/finance/ajouterCompteClient']) ;

}
loadEditCompteClientComponent() {
  this.router.navigate(['/pms/finance/editerCompteClient', this.selection.selected[ this.selection.selected.length - 1].code]) ;

}
supprimerComptesClients()
{
 
  if ( this.selection.selected.length !== 0) {
    this.deleteEnCours = true;
      this.selection.selected.forEach(function(compteClient, index) {
          this.comptesClientsService.deleteCompteClient(compteClient.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.deleteEnCours = false;
            
            this.openSnackBar( ' Element(s) Supprimé(s)');
             this.loadData();
            }
      }, this);
    } else {
      this.openSnackBar('Aucun Compte Séléctionné !');
    }
}
refresh() {
  this.loadData(null);
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

changePage($event) {
  console.log('page event is ', $event);
 this.comptesClientsService.getPageComptesClients($event.pageIndex, $event.pageSize,
this.compteClientDataFilter ).subscribe((comptesClients) => {
    this.listComptesClients = comptesClients;
   const listElements = [];
     this.listComptesClients.forEach(compteClient => {
      listElements.push( {code: compteClient.code ,
       numeroCompteClient: compteClient.numeroCompteClient,
       dateOuverture: compteClient.dateOuverture ,
       dateCloture: compteClient.dateCloture ,
       cinClient: compteClient.client.cin,
       nomClient: compteClient.client.nom,
       prenomClient: compteClient.client.prenom,
       raisonSocialClient: compteClient.client.raisonSociale,
       chiffreAffaires: compteClient.chiffreAffaires ,
       profession: compteClient.profession ,
       valeurVentes: compteClient.valeurVentes ,
       valeurCredits: compteClient.valeurCredits ,
       valeurPaiements: compteClient.valeurPaiements ,
       etat: compteClient.etat === 'Valide' ?  'Validé' :
       compteClient.etat === 'Annule' ? 'Annulé' :
       compteClient.etat === 'EnCoursValidation' ?
        'En Cours De Validation' :   compteClient.etat === 'Cloture' ? 'Cloturé' :
       compteClient.etat === 'Initial' ? 'Initiale' : 'Ouvert',
       description: compteClient.description,

 } );

});
this.dataComptesClientsSource = new MatTableDataSource<CompteClientElement>(listElements);
this.comptesClientsService.getTotalComptesClients(this.compteClientDataFilter).subscribe((response) => {
 console.log('Total credits is ', response);
  this.dataComptesClientsSource.paginator.length = response.totalComptesClients;
});
});
}
toggleFilterPanel() {
  this.showFilter = !this.showFilter;
}
toggleShowColumnsMultiSelect() {
 this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
}
loadFiltredData() {
 console.log('the filter is ', this.compteClientDataFilter);
 this.loadData(this.compteClientDataFilter);
}
activateFilterClient()
{
  this.showFilterClient = !this.showFilterClient ;
  
}
}
