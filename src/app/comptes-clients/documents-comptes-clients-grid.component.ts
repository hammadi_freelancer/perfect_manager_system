
import { Component, OnInit, Inject,Input, ViewChild, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {ComptesClientsService} from './comptes-clients.service';
import {CompteClient} from './compte-client.model';

import {DocumentCompteClient} from './document-compte-client.model';
import { DocumentCompteClientElement } from './document-compte-client.interface';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogAddDocumentCompteClientComponent } from './popups/dialog-add-document-compte-client.component';
import { DialogEditDocumentCompteClientComponent } from './popups/dialog-edit-document-compte-client.component';
@Component({
    selector: 'app-documents-comptes-clients-grid',
    templateUrl: 'documents-comptes-clients-grid.component.html',
  })
  export class DocumentsComptesClientsGridComponent implements OnInit, OnChanges {
     private listDocumentsComptesClients: DocumentCompteClient[] = [];
     private  dataDocumentsComptesClientsSource: MatTableDataSource<DocumentCompteClientElement>;
     private selection = new SelectionModel<DocumentCompteClientElement>(true, []);
    
     @Input()
     private compteClient : CompteClient

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private comptesClientsService: ComptesClientsService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.comptesClientsService.getDocumentsComptesClientsByCodeCompteClient(this.compteClient.code).
      subscribe((listDocumentsCC) => {
        this.listDocumentsComptesClients = listDocumentsCC;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
  }
  ngOnChanges()
  {
    this.specifyListColumnsToBeDisplayed();
    
    this.comptesClientsService.getDocumentsComptesClientsByCodeCompteClient(this.compteClient.code).subscribe((listDocumentsCC) => {
      this.listDocumentsComptesClients= listDocumentsCC;
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }

  checkOneActionInvoked() {
    const listSelectedDocumentsComptesClients: DocumentCompteClient[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentCC) => {
    return documentCC.code;
  });
  this.listDocumentsComptesClients.forEach(document=> {
    if (codes.findIndex(code => code === document.code) > -1) {
      listSelectedDocumentsComptesClients.push(document);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsComptesClients !== undefined) {

      this.listDocumentsComptesClients.forEach(docC => {
             listElements.push( {code: docC.code ,
          dateReception: docC.dateReception,
          numeroDocument: docC.numeroDocument,
          typeDocument: docC.typeDocument,
          description: docC.description
        } );
      });
    }
      this.dataDocumentsComptesClientsSource = new MatTableDataSource<DocumentCompteClientElement>(listElements);
      this.dataDocumentsComptesClientsSource.sort = this.sort;
      this.dataDocumentsComptesClientsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    //  console.log('calling specifyListColumnsToBeDisplayed documents grid');
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
    
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
      console.log(this.columnsNames);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsComptesClientsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsComptesClientsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsComptesClientsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsComptesClientsSource.paginator) {
      this.dataDocumentsComptesClientsSource.paginator.firstPage();
    }
  }
  showAddDocumentDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeCompteClient : this.compteClient.code
   };
 
   const dialogRef = this.dialog.open(DialogAddDocumentCompteClientComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(docAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditDocumentDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeDocumentCompteClient : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditDocumentCompteClientComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(docAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Document Séléctionnée!');
    }
    }
    supprimerDocuments()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(doc, index) {
              this.comptesClientsService.deleteDocumentCompteClient(doc.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Document Séléctionnée!');
        }
    }
    refresh() {
      this.comptesClientsService.getDocumentsComptesClientsByCodeCompteClient(this.compteClient.code).
      subscribe((listDocsComptesClients) => {
        this.listDocumentsComptesClients = listDocsComptesClients;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
