
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ComptesClientsService} from '../comptes-clients.service';
import {CompteClient} from '../compte-client.model';
import {TransactionCompteClient} from '../transaction-compte-client.model';

@Component({
    selector: 'app-dialog-add-transaction-compte-client',
    templateUrl: 'dialog-add-transaction-compte-client.component.html',
  })
  export class DialogAddTransactionCompteClientComponent implements OnInit {
     private transactionCompteClient: TransactionCompteClient;
     private saveEnCours = false;
     private typesTransactions: any[] = [
      {value: 'PaiementVente', viewValue: 'Paiement Vente'},
      {value: 'PaiementCredit', viewValue: 'Paiement Crédit'},
      {value: 'Credit', viewValue: 'Crédit'},
      {value: 'Vente', viewValue: 'Vente'},
      {value: 'PaiementACompte', viewValue: 'Paiement à Compte'},
      
     ]
  
     constructor(  private comptesClientsService: ComptesClientsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.transactionCompteClient = new TransactionCompteClient();
    }
    saveTransactionCompteClient() 
    {
      this.transactionCompteClient.codeCompteClient = this.data.codeCompteClient;
       this.saveEnCours = true;
       this.comptesClientsService.addTransactionCompteClient(this.transactionCompteClient).subscribe((response) => {
         this.saveEnCours = false;
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.transactionCompteClient = new TransactionCompteClient();
             this.openSnackBar(  'Transaction Enregistrée');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
 
  }
