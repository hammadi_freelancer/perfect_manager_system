
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ComptesClientsService} from '../comptes-clients.service';
import {DocumentCompteClient} from '../document-compte-client.model';
import { DocumentCompteClientElement } from '../document-compte-client.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-documents-comptes-clients',
    templateUrl: 'dialog-list-documents-comptes-clients.component.html',
  })
  export class DialogListDocumentsComptesClientsComponent implements OnInit {
     private listDocumentsComptesClients: DocumentCompteClient[] = [];
     private  dataDocumentsComptesClientsSource: MatTableDataSource<DocumentCompteClientElement>;
     private selection = new SelectionModel<DocumentCompteClientElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private comptesClientsService: ComptesClientsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      this.comptesClientsService.getDocumentsComptesClientsByCodeCompteClient(this.data.codeCompteClient).subscribe((listDocumentsCC) => {
        this.listDocumentsComptesClients = listDocumentsCC;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }

  checkOneActionInvoked() {
    const listSelectedDocumentsClients: DocumentCompteClient[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentCelement) => {
    return documentCelement.code;
  });
  this.listDocumentsComptesClients.forEach(documentCC=> {
    if (codes.findIndex(code => code === documentCC.code) > -1) {
      listSelectedDocumentsClients.push(documentCC);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsComptesClients !== undefined) {

      this.listDocumentsComptesClients.forEach(docCC => {
             listElements.push( {code: docCC.code ,
          dateReception: docCC.dateReception,
          numeroDocument: docCC.numeroDocument,
          typeDocument: docCC.typeDocument,
          description: docCC.description
        } );
      });
    }
      this.dataDocumentsComptesClientsSource = new MatTableDataSource<DocumentCompteClientElement>(listElements);
      this.dataDocumentsComptesClientsSource.sort = this.sort;
      this.dataDocumentsComptesClientsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    //  console.log('calling specifyListColumnsToBeDisplayed documents grid');
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
    
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
      console.log(this.columnsNames);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsComptesClientsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsComptesClientsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsComptesClientsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsComptesClientsSource.paginator) {
      this.dataDocumentsComptesClientsSource.paginator.firstPage();
    }
  }
 




}
