
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ComptesClientsService} from '../comptes-clients.service';
import {DocumentCompteClient} from '../document-compte-client.model';

@Component({
    selector: 'app-dialog-edit-document-compte-client',
    templateUrl: 'dialog-edit-document-compte-client.component.html',
  })
  export class DialogEditDocumentCompteClientComponent implements OnInit {
     private documentCompteClient: DocumentCompteClient;
     private updateEnCours = false;
     private typesDoc: any[] = [
      {value: 'FactureVentes', viewValue: 'Facture Ventes'},
      {value: 'BonLivraison', viewValue: 'Bon Livraison'},
      {value: 'RecuPayement', viewValue: 'Reçu Payement'},
      {value: 'Cheque', viewValue: 'Chèque'},
      {value: 'Traite', viewValue: 'Traite'},
      {value: 'Autre', viewValue: 'Autre'},
    ];
     constructor(  private comptesClientsService: ComptesClientsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit() {

 this.comptesClientsService.getDocumentCompteClient(this.data.codeDocumentCompteClient).subscribe((doc) => {
            this.documentCompteClient = doc;
     });
    }
    
    updateDocumentCompteClient() 
    {
      this.updateEnCours = true;
       this.comptesClientsService.updateDocumentCompteClient(this.documentCompteClient).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.openSnackBar(  'Document Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
     imageFace1Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentCompteClient.imageFace1 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imageFace2Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentCompteClient.imageFace2 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
  }
