
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ComptesClientsService} from '../comptes-clients.service';
import {TransactionCompteClient} from '../transaction-compte-client.model';

@Component({
    selector: 'app-dialog-edit-transaction-compte-client',
    templateUrl: 'dialog-edit-transaction-compte-client.component.html',
  })
  export class DialogEditTransactionCompteClientComponent implements OnInit {
     private transactionCompteClient: TransactionCompteClient;
     private updateEnCours = false;
 
     private typesTransactions: any[] = [
      {value: 'PaiementVente', viewValue: 'Paiement Vente'},
      {value: 'PaiementCredit', viewValue: 'Paiement Crédit'},
      {value: 'Credit', viewValue: 'Crédit'},
      {value: 'Vente', viewValue: 'Vente'},
      {value: 'PaiementACompte', viewValue: 'Paiement à Compte'},
      
     ]
  
   
     constructor(  private comptesClientsService: ComptesClientsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {

        this.comptesClientsService.getTransactionCompteClient
        (this.data.codeTransactionCompteClient).subscribe((transaction) => {
            this.transactionCompteClient = transaction;
     });
    }
    
    updateTransactionCompteClient() 
    {
      this.updateEnCours = true;
       this.comptesClientsService.updateTransactionCompteClient(this.transactionCompteClient).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {

             this.openSnackBar(  'Transaction Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }


  }
