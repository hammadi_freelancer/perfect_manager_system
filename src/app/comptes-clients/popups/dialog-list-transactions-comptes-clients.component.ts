
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ComptesClientsService} from '../comptes-clients.service';
import {TransactionCompteClient} from '../transaction-compte-client.model';
import { TransactionCompteClientElement } from '../transaction-compte-client.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-transactions-comptes-clients',
    templateUrl: 'dialog-list-transactions-comptes-clients.component.html',
  })
  export class DialogListTransactionsComptesClientsComponent implements OnInit {
     private listTransactionsComptesClients: TransactionCompteClient[] = [];
     private  dataTransactionsComptesClientsSource: MatTableDataSource<TransactionCompteClientElement>;
     private selection = new SelectionModel<TransactionCompteClientElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private comptesClientsService: ComptesClientsService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      this.comptesClientsService.getTransactionsCompteClientByCodeCompteClient(this.data.codeCompteClient).
      subscribe((listTr) => {
        this.listTransactionsComptesClients = listTr;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedTransactionsComptesClients: TransactionCompteClient[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((trCC) => {
    return trCC.code;
  });
  this.listTransactionsComptesClients.forEach(trC => {
    if (codes.findIndex(code => code === trC.code) > -1) {
      listSelectedTransactionsComptesClients.push(trC);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listTransactionsComptesClients !== undefined) {

      this.listTransactionsComptesClients.forEach(tr => {
             listElements.push( {code: tr.code ,
          dateTransaction: tr.dateTransaction,
          type: tr.type,
          valeurTransaction: tr.valeurTransaction,
       
          
        } );
      });
    }
      this.dataTransactionsComptesClientsSource = new MatTableDataSource<TransactionCompteClientElement>(listElements);
      this.dataTransactionsComptesClientsSource.sort = this.sort;
      this.dataTransactionsComptesClientsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'valeurTransaction' , displayTitle: 'Valeur'},
     {name: 'type' , displayTitle: 'Type'},
     {name: 'dateTransaction' , displayTitle: 'Date'},
       
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataTransactionsComptesClientsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataTransactionsComptesClientsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataTransactionsComptesClientsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataTransactionsComptesClientsSource.paginator) {
      this.dataTransactionsComptesClientsSource.paginator.firstPage();
    }
  }

}
