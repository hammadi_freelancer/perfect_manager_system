import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';
import { HttpParams } from '@angular/common/http';

import {CompteClient} from './compte-client.model' ;
// import { TrancheCredit } from './tranche-credit.model';
import { DocumentCompteClient } from './document-compte-client.model';
import { TransactionCompteClient } from './transaction-compte-client.model';

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_PAGE_COMPTES_CLIENTS= 'http://localhost:8082/finance/comptesClients/getPageComptesClients';
const URL_GET_COMPTE_CLIENT = 'http://localhost:8082/finance/comptesClients/getCompteClient';
const  URL_ADD_COMPTE_CLIENT  = 'http://localhost:8082/finance/comptesClients/addCompteClient';
const  URL_UPDATE_COMPTE_CLIENT  = 'http://localhost:8082/finance/comptesClients/updateCompteClient';
const  URL_DELETE_COMPTE_CLIENT  = 'http://localhost:8082/finance/comptesClients/deleteCompteClient';
const  URL_GET_TOTAL_COMPTES_CLIENTS = 'http://localhost:8082/finance/comptesClients/getTotalComptesClients';



  const  URL_ADD_DOCUMENT_COMPTE_CLIENT  = 'http://localhost:8082/finance/documentsComptesClients/addDocumentCompteClient';
  const  URL_GET_LIST_DOCUMENT_COMPTE_CLIENT  = 'http://localhost:8082/finance/documentsComptesClients/getDocumentsComptesClients';
  const  URL_DELETE_DOCUCMENT_COMPTE_CLIENT  = 'http://localhost:8082/finance/documentsComptesClients/deleteDocumentCompteClient';
  const  URL_UPDATE_DOCUMENT_COMPTE_CLIENT  = 'http://localhost:8082/finance/documentsComptesClients/updateDocumentCompteClient';
  const URL_GET_DOCUMENT_COMPTE_CLIENT  = 'http://localhost:8082/finance/documentsComptesClients/getDocumentCompteClient';
  const URL_GET_LIST_DOCUMENT_COMPTE_CLIENT_BY_CODE_COMPTE_CLIENT =
   'http://localhost:8082/finance/documentsComptesClients/getDocumentsComptesClientsByCodeCompteClient';


   const  URL_ADD_TRANSACTION_COMPTE_CLIENT  = 'http://localhost:8082/finance/transactionsComptesClients/addTransactionCompteClient';
   const  URL_GET_LIST_TRANSACTION_COMPTE_CLIENT  = 
   'http://localhost:8082/finance/transactionsComptesClients/getTransactionsComptesClients';
   const  URL_DELETE_TRANSACTION_COMPTE_CLIENT  = 'http://localhost:8082/finance/transactionsComptesClients/deleteTransactionCompteClient';
   const  URL_UPDATE_TRANSACTION_COMPTE_CLIENT  = 'http://localhost:8082/finance/transactionsComptesClients/updateTransactionCompteClient';
   const URL_GET_TRANSACTION_COMPTE_CLIENT  = 'http://localhost:8082/finance/transactionsComptesClients/getTransactionCompteClient';
   const URL_GET_LIST_TRANSACTIONS_COMPTE_CLIENT_BY_CODE_COMPTE_CLIENT  =
    'http://localhost:8082/finance/transactionsComptesClients/getTransactionsComptesClientsByCodeCompteClient';

@Injectable({
  providedIn: 'root'
})
export class ComptesClientsService {

  constructor(private httpClient: HttpClient) {

   }

   getTotalComptesClients(filter): Observable<any> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('filter', filterStr);
        return this.httpClient
        .get<any>(URL_GET_TOTAL_COMPTES_CLIENTS, {params});
       }
   getPageComptesClients(pageNumber, nbElementsPerPage, filter): Observable<CompteClient[]> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('pageNumber', pageNumber)
    .set('nbElementsPerPage', nbElementsPerPage)
    .set('filter', filterStr);
    return this.httpClient
    .get<CompteClient[]>(URL_GET_PAGE_COMPTES_CLIENTS, {params});
   }
   addCompteClient(compteClient: CompteClient): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_COMPTE_CLIENT, compteClient);
  }
  updateCompteClient(compteClient: CompteClient): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_COMPTE_CLIENT, compteClient);
  }
  deleteCompteClient(codeCompteClient): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_COMPTE_CLIENT + '/' + codeCompteClient);
  }
  getCompteClient(codeCompteClient): Observable<CompteClient> {
    return this.httpClient.get<CompteClient>(URL_GET_COMPTE_CLIENT + '/' + codeCompteClient);
  }

// documents comptes clients

  addDocumentCompteClient(documentCompteClient: DocumentCompteClient): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_DOCUMENT_COMPTE_CLIENT, documentCompteClient);
    
  }
  updateDocumentCompteClient(docCompteClient: DocumentCompteClient): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_DOCUMENT_COMPTE_CLIENT, docCompteClient);
  }
  deleteDocumentCompteClient(codeDocumentCompteClient): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_DOCUCMENT_COMPTE_CLIENT + '/' + codeDocumentCompteClient);
  }
  getDocumentCompteClient(codeDocumentCompteClient): Observable<DocumentCompteClient> {
    return this.httpClient.get<DocumentCompteClient>
    (URL_GET_DOCUMENT_COMPTE_CLIENT + '/' + codeDocumentCompteClient);
  }
  getDocumentsComptesClients(): Observable<DocumentCompteClient[]> {
    return this.httpClient
    .get<DocumentCompteClient[]>(URL_GET_LIST_DOCUMENT_COMPTE_CLIENT);
   }
  getDocumentsComptesClientsByCodeCompteClient(codeCompteClient): Observable<DocumentCompteClient[]> {
    return this.httpClient.get<DocumentCompteClient[]>(URL_GET_LIST_DOCUMENT_COMPTE_CLIENT_BY_CODE_COMPTE_CLIENT + '/' + codeCompteClient);
  }

// transactions comptes clients 


addTransactionCompteClient(transactionCompteClient: TransactionCompteClient): Observable<any> {
  return this.httpClient.post<any>(URL_ADD_TRANSACTION_COMPTE_CLIENT, transactionCompteClient);
  
}
updateTransactionCompteClient(transactionCompteClient: TransactionCompteClient): Observable<any> {
  return this.httpClient.put<any>(URL_UPDATE_TRANSACTION_COMPTE_CLIENT, transactionCompteClient);
}
deleteTransactionCompteClient(codeTransactionCompteClient): Observable<any> {
  return this.httpClient.delete<any>(URL_DELETE_TRANSACTION_COMPTE_CLIENT + '/' + codeTransactionCompteClient);
}
getTransactionCompteClient(codeTransactionCompteClient): Observable<TransactionCompteClient> {
  return this.httpClient.get<TransactionCompteClient>(URL_GET_TRANSACTION_COMPTE_CLIENT + '/' + codeTransactionCompteClient);
}
getTransactionsComptesClients(): Observable<TransactionCompteClient[]> {
  return this.httpClient
  .get<TransactionCompteClient[]>(URL_GET_LIST_TRANSACTION_COMPTE_CLIENT);
 }
getTransactionsCompteClientByCodeCompteClient(codeCompteClient): Observable<TransactionCompteClient[]> {
  return this.httpClient.get<TransactionCompteClient[]>(URL_GET_LIST_TRANSACTIONS_COMPTE_CLIENT_BY_CODE_COMPTE_CLIENT
    + '/' + codeCompteClient);
}





}
