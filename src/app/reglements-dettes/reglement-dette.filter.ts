

export class ReglementDetteFilter {
    numeroReglementDette: string ;
    dateReglementDetteFromObject: Date;
    dateReglementDetteToObject: Date;
    periodeReglementDetteFromObject: Date;
    periodeReglementDetteToObject: Date;
    valeurRegele: number;
    nomFournisseur: string;
    prenomFournisseur: string;
    raisonFournisseur: string;
    description: string;
    mois: string;
    modePaiement: string;
    etat: string;
   }
