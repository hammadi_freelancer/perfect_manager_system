import { Component, OnInit, Output,ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {ReglementDette} from './reglement-dette.model';
import {ReglementDetteFilter} from './reglement-dette.filter';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {ReglementsDettesService} from './reglements-dettes.service';
// import { ActionData } from './action-data.interface';
// import { Client } from '../../ventes/clients/client.model';
import { ReglementDetteElement } from './reglement-dette.interface';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
// import { DialogAddClientComponent } from '../ventes/clients/dialog-add-client.component';
import {SelectItem} from 'primeng/api';

@Component({
  selector: 'app-reglements-dettes-grid',
  templateUrl: './reglements-dettes-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class ReglementsDettesGridComponent implements OnInit {

  private reglementDette: ReglementDette =  new ReglementDette();
  private selection = new SelectionModel<ReglementDetteElement>(true, []);
  private reglementDetteFilter: ReglementDetteFilter = new ReglementDetteFilter();
  

  private columnsToSelect : SelectItem[];
   private  selectedColumnsDefinitions: string[];
  private selectedColumnsNames: string[];
  
@Output()
select: EventEmitter<ReglementDette[]> = new EventEmitter<ReglementDette[]>();


 selectedReglementDette: ReglementDette ;
 private   showFilter = false;
 // private   showFilterClient = false;
  private   showFilterFournisseur = false;
 
 private showSelectColumnsMultiSelect = false;
 
 private etatsReglementsDettes: any[] = [
  {value: 'Initial', viewValue: 'Initiale'},
  {value: 'Valide', viewValue: 'Validée'},
  {value: 'Annule', viewValue: 'Annulée'},
  // {value: 'Regle', viewValue: 'Reglée'},
  // {value: 'NonRegle', viewValue: 'Non Reglée'},
 //  {value: 'PartiellementRegle', viewValue: 'Partiellement Reglée'},
];

private modesPaiements: any[] = [
  {value: 'Comptant', viewValue: 'Comptant'},
  {value: 'Cheque', viewValue: 'Chèque'},
  {value: 'Avance', viewValue: 'Avance'},
  {value: 'Virement', viewValue: 'Virement'}
];

 @Input()
 private listReglementsDettes: any = [] ;
 private checkedAll: false;

 private  dataReglementsDettesSource: MatTableDataSource<ReglementDetteElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay :ColumnDefinition[] = [];
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
  constructor( private route: ActivatedRoute, private reglementsDettesService: ReglementsDettesService,
    private router: Router, private matSnackBar: MatSnackBar,
  private  dialog: MatDialog ) {
  }
  ngOnInit() {
    this.columnsToSelect = [
      {label: 'N° Règlement', value: 'numeroReglementDette', title: 'N° Règlement'},
      {label: 'Date Règlement', value: 'dateReglementDette', title: 'Date Règlement'},
      {label: 'Période De:', value: 'periodeFrom', title: 'Période De:'},
      {label: 'à', value: 'periodeTo', title: 'à'},
      {label: 'Valeur Règlé(DT)', value: 'valeurRegle', title: 'Valeur Règlé(DT)'},
      {label: 'Mode Paiement', value: 'modePaiement', title: 'Mode Paiement'},
      {label: 'Mois', value: 'mois', title: 'Mois'},
      {label: 'Rs.Soc.Fournisseur', value:  'raisonSocialeFournisseur',
       title: 'Rs.Soc.Fournisseur' },
      {label: 'CIN Four.', value:  'cinFournisseur', title: 'CIN Four.' },
      {label: 'Nom Four.', value:  'nomFournisseur', title: 'Nom Four.' },
      {label: 'Prénom Four.', value:  'prenomFournisseur', title: 'Prénom Four.' },
      {label: 'Déscription', value:  'description', title: 'Déscription' },
      {label: 'Etat', value:  'etat', title: 'Etat' },
  
  ];
  this.selectedColumnsDefinitions = [
    'numeroReglementDette',
   'dateReglementDette' ,
   'valeurRegle' ,
   'raisonSocialeFournisseur'
    ];
    this.reglementsDettesService.getPageReglementsDettes(0, 5, null).
    subscribe((regDettes) => {
      this.listReglementsDettes = regDettes;
  
      this.defaultColumnsDefinitions = [
        {name: 'numeroReglementDette' , displayTitle: 'N° Règlement'},
        {name: 'dateReglementDette' , displayTitle: 'Date Règlement'},
         {name: 'valeurRegle' , displayTitle: 'Valeur Règlé'},
        {name: 'cinFournisseur' , displayTitle: 'CIN Fournisseur'},
        {name: 'nomFournisseur' , displayTitle: 'Nom Fournisseur'},
        {name: 'prenomFournisseur' , displayTitle: 'Prénom Fournisseur'},
        {name: 'raisonSocialeFournisseur' , displayTitle: 'Raison Soc.'},
         {name: 'modePaiement' , displayTitle: 'Mode Paiement'},
         {name: 'description' , displayTitle: 'Déscription'},
         {name: 'etat', displayTitle: 'Etat' },
         
        ];
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });
  }
  deleteReglementDette(reglementDette) {
     //  console.log('call delete !', operationCourante );
    this.reglementsDettesService.deleteReglementDette(reglementDette.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData(null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.reglementsDettesService.getPageReglementsDettes(0, 5, filter).subscribe((reglementsDettes) => {
    this.listReglementsDettes= reglementsDettes;
    this.transformDataToByConsumedByMatTable();
   });
}


checkOneActionInvoked() {
  const listSelectedReglementsDettes: ReglementDette[] = [];

if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((regElement) => {
  return regElement.code;
});
this.listReglementsDettes.forEach(RG => {
  if (codes.findIndex(code => code === RG.code) > -1) {
    listSelectedReglementsDettes.push(RG);
  }
  });
}
this.select.emit(listSelectedReglementsDettes);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listReglementsDettes !== undefined) {
    this.listReglementsDettes.forEach(regDette => {
      listElements.push( {
        code: regDette.code ,
        numeroReglementDette: regDette.numeroReglementDette,
        cinFournisseur: regDette.fournisseur.cin,
        nomFournisseur: regDette.fournisseur.nom ,
        prenomFournisseur: regDette.fournisseur.prenom ,
        raisonSocialeFournisseur: regDette.fournisseur.raisonSociale ,
       valeurRegle: regDette.valeurRegle ,
       mois: regDette.mois ,
       dateReglementDette: regDette.dateReglementDette,
       periodeFrom: regDette.periodeFrom,
       periodeTo: regDette.periodeTo,
        modePaiement: regDette.modePaiement,
       etat: regDette.etat,
       description: regDette.description,
       
      } );
    });
  }
    this.dataReglementsDettesSource = new MatTableDataSource<ReglementDetteElement>(listElements);
    this.dataReglementsDettesSource.sort = this.sort;
    this.dataReglementsDettesSource.paginator = this.paginator;
    this.reglementsDettesService.getTotalReglementsDettes(this.reglementDetteFilter).subscribe((response) => {
    //   console.log('Total Releves de Ventes  is ', response);
    if (this.dataReglementsDettesSource.paginator) {
       this.dataReglementsDettesSource.paginator.length = response['totalReglementsDettes'];
  
        }    });
}

specifyListColumnsToBeDisplayed() {
  
   console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
   this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
 ?
    this.defaultColumnsDefinitions : [];
    this.selectedColumnsNames = [];
    
    if (this.selectedColumnsDefinitions !== undefined)
     {
       this.selectedColumnsDefinitions.forEach((colName) => { 
            const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
            const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
            const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
            this.columnsDefinitionsToDisplay.push(colDef);
           });
     }
    this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
   this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
              return colDef.displayTitle;
    });
    console.log('columns to disp:', this.columnsDefinitionsToDisplay);
    this.columnsNames = [];
    this.columnsNames[0] = 'select';
    this.columnsDefinitionsToDisplay .map((colDef) => {
     this.columnsNames.push(colDef.name);
    if (this.selectedColumnsDefinitions !== undefined) {
     this.selectedColumnsNames.push(colDef.displayTitle);
    }
 });
    this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
     return colDef.displayTitleAr;
 });
 }

masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataReglementsDettesSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataReglementsDettesSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataReglementsDettesSource.filter = filterValue.trim().toLowerCase();
  if (this.dataReglementsDettesSource.paginator) {
    this.dataReglementsDettesSource.paginator.firstPage();
  }
}

loadAddReglementDetteComponent() {
  this.router.navigate(['/pms/suivie/ajouterReglementDette']) ;

}
loadEditReglementDetteComponent() {
  this.router.navigate(['/pms/suivie/editerReglementDette', this.selection.selected[ this.selection.selected.length - 1].code]) ;

}
supprimerReglementsDettes()
{

  if ( this.selection.selected.length !== 0) {
      this.selection.selected.forEach(function(regDette, index) {

   this.reglementsDettesService.deleteReglementDette(regDette.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.openSnackBar( this.selection.selected.length + ' Reglement(s) Dette(s) Supprimé(s)');
            this.selection.selected = [];
            this.loadData(null);
            }
      }, this);
    } else {
      this.openSnackBar('Aucun Règlement Séléctionnée !');
    }
}
refresh() {
  this.loadData(null);
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

 history()
 {
   
 }
 /*addClient()
 {
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
  };

  const dialogRef = this.dialog.open(DialogAddClientComponent,
    dialogConfig
  );
 }*/

 toggleFilterPanel()
 {
    this.showFilter = !this.showFilter;
 }
 toggleShowColumnsMultiSelect()
 {
   this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
 }
 loadFiltredData()
 {
  //  console.log('the filter is ', this.operationCouranteFilter);
   this.loadData(this.reglementDetteFilter);
 }
 attacherDocuments()
 {

 }
 changePage($event) {
  console.log('page event is ', $event);
 this.reglementsDettesService.getPageReglementsDettes($event.pageIndex, $event.pageSize,
  this.reglementDetteFilter ).subscribe((regDettes) => {
    this.listReglementsDettes = regDettes;
   const listElements = [];
   this.listReglementsDettes.forEach(regDette => {
    listElements.push( {
      code: regDette.code ,
      numeroReglementDette: regDette.numeroReglementDette,
      cinFournisseur: regDette.fournisseur.cin,
      nomFournisseur: regDette.fournisseur.nom ,
      prenomFournisseur: regDette.fournisseur.prenom ,
      raisonSocialeFournisseur: regDette.fournisseur.raisonSociale ,
     valeurRegle: regDette.valeurRegle ,
     mois: regDette.mois ,
     dateReglementDette: regDette.dateReglementDette,
     periodeFrom: regDette.periodeFrom,
     periodeTo: regDette.periodeTo,
      modePaiement: regDette.modePaiement,
     etat: regDette.etat,
     description: regDette.description
      
  });
});
     this.dataReglementsDettesSource= new MatTableDataSource<ReglementDetteElement>(listElements);

     
     this.reglementsDettesService.getTotalReglementsDettes(this.reglementDetteFilter)
     .subscribe((response) => {
      console.log('Total OPer Courantes  is ', response);
      if (this.dataReglementsDettesSource.paginator) {
       this.dataReglementsDettesSource.paginator.length = response['totalReglementsDettes'];
      }
     });

    });
}


activateFilterFournisseur()
{
  this.showFilterFournisseur = !this.showFilterFournisseur ;
  
}

}
