
export interface ReglementDetteElement {
   
         code: string;
         numeroReglementDette: string;
         dateReglementDette: string;
         valeurRegle: number;
         periodeFrom: string;
         periodeTo: string;
         mois: string;
         modePaiement: string;
         etat: string;
         description: string;
        raisonSocialeFournisseur: string;
        nomFournisseur: string;
        prenomFournisseur: string;
        }
