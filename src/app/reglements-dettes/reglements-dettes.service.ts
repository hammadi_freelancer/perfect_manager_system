import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import {ReglementDette} from './reglement-dette.model' ;
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_REGLEMENTS_DETTES = 'http://localhost:8082/reglementsDettes/getReglementsDettes';
const  URL_GET_PAGE_REGLEMENTS_DETTES= 'http://localhost:8082/reglementsDettes/getPageReglementsDettes';

const URL_GET_REGLEMENT_DETTE = 'http://localhost:8082/reglementsDettes/getReglementDette';
const  URL_ADD_REGLEMENT_DETTE = 'http://localhost:8082/reglementsDettes/addReglementDette';
const  URL_UPDATE_REGLEMENT_DETTE  = 'http://localhost:8082/reglementsDettes/updateReglementDette';
const  URL_DELETE_REGLEMENT_DETTE= 'http://localhost:8082/reglementsDettes/deleteReglementDette';
const  URL_GET_TOTAL_REGLEMENTS_DETTES = 'http://localhost:8082/reglementsDettes/getTotalReglementsDettes';



@Injectable({
  providedIn: 'root'
})
export class ReglementsDettesService {

  constructor(private httpClient: HttpClient) {

   }
   getTotalReglementsDettes(filter): Observable<any> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('filter', filterStr);
        return this.httpClient
        .get<any>(URL_GET_TOTAL_REGLEMENTS_DETTES, {params});
   }
   getPageReglementsDettes(pageNumber, nbElementsPerPage, filter): Observable<ReglementDette[]> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('pageNumber', pageNumber)
    .set('nbElementsPerPage', nbElementsPerPage)
    .set('filter', filterStr);
    return this.httpClient
    .get<ReglementDette[]>(URL_GET_PAGE_REGLEMENTS_DETTES, {params});
   }
   getReglementsDettes(): Observable<ReglementDette[]> {
   
    return this.httpClient
    .get<ReglementDette[]>(URL_GET_LIST_REGLEMENTS_DETTES);
   }
   addReglementDette(reglementDette: ReglementDette): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_REGLEMENT_DETTE, reglementDette);
  }
  updateReglementDette(reglementDette: ReglementDette): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_REGLEMENT_DETTE, reglementDette);
  }
  deleteReglementDette(codeReglementDette): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_REGLEMENT_DETTE + '/' + codeReglementDette);
  }
  getReglementDette(codeReglementDette): Observable<ReglementDette> {
    return this.httpClient.get<ReglementDette>(URL_GET_REGLEMENT_DETTE + '/' + codeReglementDette);
  }
}
