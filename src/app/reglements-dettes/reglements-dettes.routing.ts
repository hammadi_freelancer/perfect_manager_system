import { Injectable } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
// operationsCourantes components
import { ReglementDetteNewComponent } from './reglement-dette-new.component';
import { ReglementDetteEditComponent } from './reglement-dette-edit.component';
import { ReglementsDettesGridComponent } from './reglements-dettes-grid.component';
import { ReglementDetteHomeComponent } from './reglement-dette-home.component';

import { FournisseursResolver } from '../achats/fournisseurs-resolver';

import {ModuleWithProviders  } from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';

 export const reglementsDettesRoute: Routes = [
    {
        path: 'reglementsDettes',
        component: ReglementsDettesGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterReglementDette',
        component: ReglementDetteNewComponent,
        resolve: {
            fournisseurs: FournisseursResolver,
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerReglementDette/:codeReglementDette',
        component: ReglementDetteEditComponent,
        resolve: {
            fournisseurs: FournisseursResolver,
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
];
// export const ventesRoutingModule: ModuleWithProviders = RouterModule.forRoot(ventesRoute);
// commonRouting.providers = [{ provide: APP_BASE_HREF, useValue: '/common'} ] ;


