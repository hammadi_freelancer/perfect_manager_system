import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {ReglementDette} from './reglement-dette.model';
// import { ParametresVentes } from '../parametres-ventes.model';

import {ReglementsDettesService} from './reglements-dettes.service';

// import {VentesService} from '../ventes.service';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
// import {ActionData} from './action-data.interface';
// import { ClientService } from '../ventes/clients/client.service';
import { FournisseurService } from '../achats/fournisseurs/fournisseur.service';

// import { ClientFilter } from '../ventes/clients/client.filter';
import { FournisseurFilter } from '../achats/fournisseurs/fournisseur.filter';
import { Fournisseur } from '../achats/fournisseurs/fournisseur.model';

// import { ArticleFilter } from '../stocks/articles/article.filter';

// import {Client} from '../ventes/clients/client.model';


import { ColumnDefinition } from '../common/column-definition.interface';

import {MatSnackBar} from '@angular/material';
import { MatExpansionPanel } from '@angular/material';
import {ModePayementLibelle} from './mode-payement-libelle.interface';
import {SelectionModel} from '@angular/cdk/collections';

type LibelleModePayement = 'Credit'| 'Comptant' | 'Cheque' ;

@Component({
  selector: 'app-reglement-dette-new',
  templateUrl: './reglement-dette-new.component.html',
  // styleUrls: ['./players.component.css']
})
export class ReglementDetteNewComponent implements OnInit {

 private reglementDette: ReglementDette =  new ReglementDette();


private saveEnCours = false;



private fournisseurFilter: FournisseurFilter;

private listFournisseurs: Fournisseur[];



private etatsReglementsDettes: any[] = [
  {value: 'Initial', viewValue: 'Initiale'},
  {value: 'Valide', viewValue: 'Validée'},
  {value: 'Annule', viewValue: 'Annulée'},
  // {value: 'Regle', viewValue: 'Reglée'},
  // {value: 'NonRegle', viewValue: 'Non Reglée'},
 //  {value: 'PartiellementRegle', viewValue: 'Partiellement Reglée'},
];

private modesPaiements: any[] = [
  {value: 'Comptant', viewValue: 'Comptant'},
  {value: 'Cheque', viewValue: 'Chèque'},
  {value: 'Avance', viewValue: 'Avance'},
  {value: 'Virement', viewValue: 'Virement'}
];

private listMois: any[] = [
  {value: 'Janvier', viewValue: 'Janvier'},
  {value: 'Février', viewValue: 'Février'},
  {value: 'Mars', viewValue: 'Mars'},
  {value: 'Avril', viewValue: 'Avril'},
  {value: 'Mai', viewValue: 'Mai'},
  {value: 'Juin', viewValue: 'Juin'},
  {value: 'Juillet', viewValue: 'Juillet'},
  {value: 'Août', viewValue: 'Août'},
  {value: 'Septembre', viewValue: 'Septembre'},
  {value: 'Octobre', viewValue: 'Octobre'},
  {value: 'Novembre', viewValue: 'Novembre'},
  {value: 'Décembre', viewValue: 'Décembre'}
];


  constructor( private route: ActivatedRoute,
    private router: Router, private reglementsDettesService: ReglementsDettesService,
     private fournisseurService: FournisseurService,
     
   private matSnackBar: MatSnackBar, private elRef: ElementRef) {
  }


  ngOnInit() {
   this.listFournisseurs = this.route.snapshot.data.fournisseurs;
    this.fournisseurFilter = new FournisseurFilter(this.listFournisseurs);
    

  }
  saveReglementDette() {

    this.saveEnCours = true;
    this.reglementsDettesService.addReglementDette(this.reglementDette).subscribe((response) => {
      this.saveEnCours = false;
      
        if (response.error_message ===  undefined) {
          console.log('undefined error message');
          // this.save.emit(response);
          this.reglementDette = new  ReglementDette();
          this.openSnackBar(  'Règlement Dette');
        } else {
         // show error message
         this.openSnackBar(  'Erreurs:Opération Echouée');
         
        }
    });
  }
  fillDataMatriculeRaisonFournisseur() {
    const listFiltred  = this.listFournisseurs.filter((fournisseur) =>
     (fournisseur.registreCommerce === this.reglementDette.fournisseur.registreCommerce));
    this.reglementDette.fournisseur.matriculeFiscale = listFiltred[0].matriculeFiscale;
    this.reglementDette.fournisseur.raisonSociale = listFiltred[0].raisonSociale;
  }
  fillDataMatriculeRegistreCommercialF()
  {
    const listFiltred  = this.listFournisseurs.filter((fournisseur) => (fournisseur.raisonSociale === 
      this.reglementDette.fournisseur.raisonSociale));
    this.reglementDette.fournisseur.matriculeFiscale = listFiltred[0].matriculeFiscale;
    this.reglementDette.fournisseur.registreCommerce = listFiltred[0].registreCommerce;
  }
  fillDataRegistreCommercialRaisonF()
  {
    const listFiltred  = this.listFournisseurs.filter((fournisseur) => 
    (fournisseur.matriculeFiscale === this.reglementDette.fournisseur.matriculeFiscale));
    this.reglementDette.fournisseur.raisonSociale = listFiltred[0].raisonSociale;
    this.reglementDette.fournisseur.registreCommerce = listFiltred[0].registreCommerce;
  }
  fillDataNomPrenomFournisseur() {
    const listFiltred  = this.listFournisseurs.filter((fournisseur) => (fournisseur.cin === this.reglementDette.fournisseur.cin));
    this.reglementDette.fournisseur.nom = listFiltred[0].nom;
    this.reglementDette.fournisseur.prenom = listFiltred[0].prenom;
  }
  fillDataCINPrenomFournisseur()
  {
    const listFiltred  = this.listFournisseurs.filter((fournisseur) => (fournisseur.nom === this.reglementDette.fournisseur.nom));
    this.reglementDette.fournisseur.cin = listFiltred[0].cin;
    this.reglementDette.fournisseur.prenom = listFiltred[0].prenom;
  }
  fillDataCINNomFournisseur()
  {
    const listFiltred  = this.listFournisseurs.filter((fournisseur) => (fournisseur.prenom === this.reglementDette.fournisseur.prenom));
    const num =   Math.floor(Math.random() * Math.floor(listFiltred.length));
    this.reglementDette.fournisseur.cin = listFiltred[num].cin;
    this.reglementDette.fournisseur.nom= listFiltred[num].nom;
  }
  vider() {
    this.reglementDette =  new ReglementDette();
  }
  openSnackBar(messageToDisplay) {
   this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
       verticalPosition: 'top', panelClass: 'snackBarClass'});
  }
loadGridReglementsDettes() {
  this.router.navigate(['/pms/suivie/reglementsDettes']) ;
}
}
