import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,
  
    ViewChild } from '@angular/core';
  // import { ArticleService } from './article.service';
  import {ReglementDette} from './reglement-dette.model';
  // import { ParametresVentes } from '../parametres-ventes.model';
  
  import {ReglementsDettesService} from './reglements-dettes.service';
  
  // import {VentesService} from '../ventes.service';
  import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
  
  import {FormControl} from '@angular/forms';
  import {map, startWith} from 'rxjs/operators';
  import {Observable} from 'rxjs';
  import { Router, ActivatedRoute, ParamMap } from '@angular/router';
  // import {ActionData} from './action-data.interface';
  import { FournisseurService } from '../achats/fournisseurs/fournisseur.service';
  
  import { FournisseurFilter } from '../achats/fournisseurs/fournisseur.filter';
  import { Fournisseur } from '../achats/fournisseurs/fournisseur.model';
  
  
  
  
  import { ColumnDefinition } from '../common/column-definition.interface';
  
  import {MatSnackBar} from '@angular/material';
  import { MatExpansionPanel } from '@angular/material';
  import {ModePayementLibelle} from './mode-payement-libelle.interface';
  import {SelectionModel} from '@angular/cdk/collections';
  
  type LibelleModePayement = 'Credit'| 'Comptant' | 'Cheque' ;
  
  @Component({
    selector: 'app-reglement-dette-edit',
    templateUrl: './reglement-dette-edit.component.html',
    // styleUrls: ['./players.component.css']
  })
export class ReglementDetteEditComponent implements OnInit  {

  private reglementDette: ReglementDette =  new ReglementDette();
  
  
  private updateEnCours = false;

  
  private fournisseurFilter: FournisseurFilter;
  
  private listFournisseurs: Fournisseur[];
  

    constructor( private route: ActivatedRoute,
      private router: Router, private regelementDetteService: ReglementsDettesService,
     //   private clientService: ClientService,
       private fournisseurService: FournisseurService,
       
      // private articleService: ArticleService,
     private matSnackBar: MatSnackBar, private elRef: ElementRef) {
    }
  ngOnInit() {

    const codeReglementDette = this.route.snapshot.paramMap.get('codeReglementDette');
    
    this.regelementDetteService.getReglementDette(codeReglementDette).subscribe(
      (regDette) =>  {
             this.reglementDette = regDette;
            this.listFournisseurs = this.route.snapshot.data.fournisseurs;
            this.fournisseurFilter = new FournisseurFilter(this.listFournisseurs);
      });

     
  }
  loadGridReglementsDettes() {
    this.router.navigate(['/pms/suivie/reglementsDettes']) ;
  }
updateReglementDette() {


  this.updateEnCours = true;
  this.regelementDetteService.updateReglementDette(this.reglementDette).subscribe((response) => {
    this.updateEnCours = false;
    if (response.error_message ===  undefined) {
      console.log('the response of update is ', response );
      if (this.reglementDette.etat === 'Valide') {
      this.openSnackBar( ' Règlement mise à jour ');
    } else {
      this.openSnackBar( ' Erreurs!');
    }
}}
);
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 

 fillDataMatriculeRaisonFournisseur() {
   const listFiltred  = this.listFournisseurs.filter((fournisseur) =>
    (fournisseur.registreCommerce === this.reglementDette.fournisseur.registreCommerce));
   this.reglementDette.fournisseur.matriculeFiscale = listFiltred[0].matriculeFiscale;
   this.reglementDette.fournisseur.raisonSociale = listFiltred[0].raisonSociale;
 }
 fillDataMatriculeRegistreCommercialF()
 {
   const listFiltred  = this.listFournisseurs.filter((fournisseur) => (fournisseur.raisonSociale === 
     this.reglementDette.fournisseur.raisonSociale));
   this.reglementDette.fournisseur.matriculeFiscale = listFiltred[0].matriculeFiscale;
   this.reglementDette.fournisseur.registreCommerce = listFiltred[0].registreCommerce;
 }
 fillDataRegistreCommercialRaisonF()
 {
   const listFiltred  = this.listFournisseurs.filter((fournisseur) => 
   (fournisseur.matriculeFiscale === this.reglementDette.fournisseur.matriculeFiscale));
   this.reglementDette.fournisseur.raisonSociale = listFiltred[0].raisonSociale;
   this.reglementDette.fournisseur.registreCommerce = listFiltred[0].registreCommerce;
 }
 fillDataNomPrenomFournisseur() {
   const listFiltred  = this.listFournisseurs.filter((fournisseur) => (fournisseur.cin === this.reglementDette.fournisseur.cin));
   this.reglementDette.fournisseur.nom = listFiltred[0].nom;
   this.reglementDette.fournisseur.prenom = listFiltred[0].prenom;
 }
 fillDataCINPrenomFournisseur()
 {
   const listFiltred  = this.listFournisseurs.filter((fournisseur) => (fournisseur.nom === this.reglementDette.fournisseur.nom));
   this.reglementDette.fournisseur.cin = listFiltred[0].cin;
   this.reglementDette.fournisseur.prenom = listFiltred[0].prenom;
 }
 fillDataCINNomFournisseur()
 {
   const listFiltred  = this.listFournisseurs.filter((fournisseur) => (fournisseur.prenom === this.reglementDette.fournisseur.prenom));
   const num =   Math.floor(Math.random() * Math.floor(listFiltred.length));
   this.reglementDette.fournisseur.cin = listFiltred[num].cin;
   this.reglementDette.fournisseur.nom= listFiltred[num].nom;
 }

 



}
