
import { Component, OnInit, Inject,Input, ViewChild, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {RelevesProductionService} from './releves-production.service';
import {ReleveProduction} from './releve-production.model';

import {DocumentReleveProduction} from './document-releve-production.model';
import { DocumentReleveProductionElement } from './document-releve-production.interface';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogAddDocumentReleveProductionComponent } from './popups/dialog-add-document-releve-production.component';
import { DialogEditDocumentReleveProductionComponent } from './popups/dialog-edit-document-releve-production.component';
@Component({
    selector: 'app-documents-releves-production-grid',
    templateUrl: 'documents-releves-production-grid.component.html',
  })
  export class DocumentsRelevesProductionGridComponent implements OnInit, OnChanges {
     private listDocumentsRelevesProduction: DocumentReleveProduction[] = [];
     private  dataDocumentsRelevesProductionSource: MatTableDataSource<DocumentReleveProductionElement>;
     private selection = new SelectionModel<DocumentReleveProductionElement>(true, []);
    
     @Input()
     private releveProduction : ReleveProduction

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private relevesProductionService: RelevesProductionService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.relevesProductionService.getDocumentsRelevesProductionByCodeReleve
      (this.releveProduction.code).subscribe((listDocumentsRelevesProduction) => {
        this.listDocumentsRelevesProduction = listDocumentsRelevesProduction;
  // console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
  }
  ngOnChanges()
  {
    this.specifyListColumnsToBeDisplayed();
    
    this.relevesProductionService.getDocumentsRelevesProductionByCodeReleve
    (this.releveProduction.code).subscribe((listDocumentsRelevesProduction) => {
      this.listDocumentsRelevesProduction = listDocumentsRelevesProduction;
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }

  checkOneActionInvoked() {
    const listSelectedDocumentsRelevesProduction: DocumentReleveProduction[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentJelement) => {
    return documentJelement.code;
  });
  this.listDocumentsRelevesProduction.forEach(documentReleveProduction=> {
    if (codes.findIndex(code => code === documentReleveProduction.code) > -1) {
      listSelectedDocumentsRelevesProduction.push(documentReleveProduction);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsRelevesProduction !== undefined) {

      this.listDocumentsRelevesProduction.forEach(docJ => {
             listElements.push( {code: docJ.code ,
          dateReception: docJ.dateReception,
          numeroDocument: docJ.numeroDocument,
          typeDocument: docJ.typeDocument,
          description: docJ.description
        } );
      });
    }
      this.dataDocumentsRelevesProductionSource = new MatTableDataSource<DocumentReleveProductionElement>(listElements);
      this.dataDocumentsRelevesProductionSource.sort = this.sort;
      this.dataDocumentsRelevesProductionSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    //  console.log('calling specifyListColumnsToBeDisplayed documents grid');
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
    
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
      console.log(this.columnsNames);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsRelevesProductionSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsRelevesProductionSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsRelevesProductionSource.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsRelevesProductionSource.paginator) {
      this.dataDocumentsRelevesProductionSource.paginator.firstPage();
    }
  }
  showAddDocumentDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeReleveProduction : this.releveProduction.code
   };
 
   const dialogRef = this.dialog.open(DialogAddDocumentReleveProductionComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(docAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditDocumentDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeDocumentReleveProduction : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditDocumentReleveProductionComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(docAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Document Séléctionnée!');
    }
    }
    supprimerDocuments()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(doc, index) {
              this.relevesProductionService.deleteDocumentReleveProduction(doc.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Document Séléctionnée!');
        }
    }
    refresh() {
      this.relevesProductionService.getDocumentsRelevesProductionByCodeReleve
      (this.releveProduction.code).subscribe((listDocsRelevesProduction) => {
        this.listDocumentsRelevesProduction = listDocsRelevesProduction;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
