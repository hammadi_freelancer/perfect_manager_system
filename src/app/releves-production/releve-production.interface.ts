export interface ReleveProductionElement {
         code: string ;
         numeroReleveProduction: string;
         dateDebutProduction: string ;
         dateFinProduction: string ;
         qteProduitIntacte: number;
         qteProduitDefaille: number;
         qteEnCoursProduction: number;
         cout: number;
         dureeHours: number;
         codeArticle: string ;
         libelleArticle: string ;
         designationArticle: string ;
         depotStockage: string ;
         conditionStockage: string ;
         responsable: string ;
         etat: string;
         description: string;
        }
