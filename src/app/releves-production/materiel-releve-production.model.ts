
import { BaseEntity } from '../common/base-entity.model' ;


 
export class MaterielReleveProduction extends BaseEntity {
    constructor(
        public code?: string,
        public codeReleveProduction?: string,
        public numeroReleveProduction?: string,
         public numeroMaterielReleveProduction?: string,

         public matricule?: string,
         public marque?: string,
         public reference?: string,
         public dateAchatsObject?: Date,
         public dateAchats?: string,
         public dateFabrication?: string,
         
         public dateFabricationObject?: Date,
         public dureeExploitationHours?: number,
          public etat?: string,
     ) {
         super();
        
       }
  
}
