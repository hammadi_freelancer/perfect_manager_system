import { Component, OnInit, Output,ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {ReleveProduction} from './releve-production.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {RelevesProductionService} from './releves-production.service';
import { ReleveProductionElement } from './releve-production.interface';
import { ReleveProductionFilter } from './releve-production.filter';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {SelectItem} from 'primeng/api';

// import { DialogAddDocumentCreditComponent } from './dialog-add-document-credit.component';
@Component({
  selector: 'app-releves-production-grid',
  templateUrl: './releves-production-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class RelevesProductionGridComponent implements OnInit {

  private releveProduction: ReleveProduction =  new ReleveProduction();
  private releveProductionFilter: ReleveProductionFilter = new ReleveProductionFilter();
  
  private selection = new SelectionModel<ReleveProductionElement>(true, []);
  private columnsToSelect : SelectItem[];
   private  selectedColumnsDefinitions: string[];
  private selectedColumnsNames: string[];
  
@Output()
select: EventEmitter<ReleveProduction[]> = new EventEmitter<ReleveProduction[]>();

//private lastClientAdded: Client;

 private selectedReleveProduction: ReleveProduction ;
private   showFilter = false;
private showSelectColumnsMultiSelect = false;

 @Input()
 private listRelevesProduction: ReleveProduction[] = [] ;
 private deleteEnCours = false;
 private checkedAll: false;

 private  dataRelevesProductionSource: MatTableDataSource<ReleveProductionElement>;

 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay :ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
 private etatsReleveProduction: any[] = [
  {value: 'Ouvert', viewValue: 'Ouvert'},
  {value: 'Initial', viewValue: 'Initiale'},
  {value: 'EnCoursValidation', viewValue: 'En Cours de Validation'},
  {value: 'Valide', viewValue: 'Validé'},
  {value: 'Cloture', viewValue: 'Cloturé'}
];
  constructor( private route: ActivatedRoute, private relevesProductionService: 
    RelevesProductionService,
    private router: Router, private matSnackBar: MatSnackBar ) {
  }
  ngOnInit() {
    this.relevesProductionService.getPageRelevesProduction(0, 5, null).subscribe((releves) => {
      this.listRelevesProduction = releves;
      // this.listClients[this.listClients.length] = this.lastClientAdded;
      // this.listClientsOrgin = this.listClients ;
      this.defaultColumnsDefinitions = [
        {name: 'numeroReleveProduction' , displayTitle: 'N°Relevé'},
      {name: 'dateDebutProduction' , displayTitle: 'Date Début Production'},
       {name: 'dateFinProduction' , displayTitle: 'Date Fin Production'},
       {name: 'dureeHours' , displayTitle: 'Durée(Hours)'},
       {name: 'qteProduitIntacte' , displayTitle: 'Qte Produit Intacte'},
       {name: 'qteProduitDefaille' , displayTitle: 'Qte Produit Domagée'},
       {name: 'qteEnCoursProduction' , displayTitle: 'Qte En Production'},
       {name: 'cout' , displayTitle: 'Coût'},
       {name: 'codeArticle' , displayTitle: 'Code Article'},
       {name: 'libelleArticle' , displayTitle: 'Libéllé Article'},
       {name: 'designationArticle' , displayTitle: 'Désignation Article'},
       {name: 'depotStockage' , displayTitle: 'Dépôt Stockage'},
       {name: 'conditionStockage' , displayTitle: 'Conditions Stockage'},
       {name: 'responsable' , displayTitle: 'Responsable'},
       { name : 'etat' , displayTitle : 'Etat'},
       { name : 'description' , displayTitle : 'Déscription'}
        ];

    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  /*  this.columnsToSelect = [
      {label: 'N°Journal', value: {id: 1, name: 'numeroJournal', displayTitle: 'N°Journal'}},
      {label: 'Date Journal', value: {id: 2, name: 'dateJournal', displayTitle: 'Date Journal'}},
      {label: 'Valeur Ventes', value: {id: 3, name: 'montantVentes', displayTitle: 'Valeur Ventes'}},
      {label: 'Valeur Achats', value: {id: 4, name: 'montantAchats', displayTitle: 'Valeur Achats'}},
      {label: 'Valeur R.Humaines', value: {id: 5, name: 'montantRH', displayTitle: 'Valeur R.Humaines'}},
      {label: 'Valeur Charges', value: {id: 6, name: 'montantCharges', displayTitle: 'Valeur Charges'} },
      {label: 'Déscription', value: {id: 7, name: 'description', displayTitle: 'Déscription'} },
      {label: 'Etat', value: {id: 8, name: 'etat', displayTitle: 'Etat'} },
      

  ];*/

      this.columnsToSelect = [
      {label: 'N° Relevé', value: 'numeroReleveProduction', title: 'N°Journal'},
      {label: 'Date Début Production', value: 'dateDebutProduction', title: 'Date Début Production'},
       {label: 'Date Fin Production', value: 'dateFinProduction', title: 'Date Fin Production'},
      {label: 'Durée(Hours)', value: 'dureeHours', title: 'Durée(Hours)'},
      {label: 'Qte Produit Intacte', value: 'qteProduitIntacte', title: 'Qte Produit Intacte'},
      {label: 'qteProduitDefaille', value: 'qteProduitDefaille', title: 'Qte Produit Domagée'},

      {label: 'Qte En Production', value:  'qteEnCoursProduction', title: 'Qte En Production' },

      {label: 'Côut', value:  'cout', title: 'Coût' },
      
      {label: 'Code Article', value:  'codeArticle', title: 'Code Article' },
      {label: 'Libéllé Article', value:  'libelleArticle', title: 'Libéllé Article' },
      {label: 'Désignation Article', value:  'designationArticle', title: 'Désignation Article' },
      {label: 'Dépôt Stockage', value:  'depotStockage', title: 'Dépôt Stockage' },
      {label: 'Conditions Stockage', value:  'conditionStockage', title: 'Conditions Stockage' },
      {label: 'Responsable', value:  'responsable', title: 'Responsable' },
      {label: 'Déscription', value: 'description', title: 'Déscription' },
      {label: 'Etat', value:  'etat', title: 'Etat' },
      

  ];
  });
   /* this.creditsService.getCredits().subscribe((credits) => {
      console.log('list credits ', credits);
      // this.listCredits = credits;
      this.listCredits =  this.listCredits.map((credit) => {
        if (credit.client === 'undefined' || credit.client === null ) {
           credit.client = new Client();
         }
         return credit;
   });      this.listCreditsOrgin = this.listCredits ;
     });*/
    // this.listClientsOrgin = this.listClients ;

  }
  deleteReleveProduction(releveProduction) {
     //  console.log('call delete !', credit );
    this.relevesProductionService.deleteReleveProduction(releveProduction.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData( null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.relevesProductionService.getPageRelevesProduction
  (0, 5 , filter).subscribe((relevesProduction) => {
    this.listRelevesProduction = relevesProduction;
    this.transformDataToByConsumedByMatTable();
   });
  // this.listClientsOrgin = this.listClients ;
}


checkOneActionInvoked() {
  const listSelectedReleveProduction: ReleveProduction[] = [];
console.log('selected clients are ');
console.log(this.selection.selected);
if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((releveElement) => {
  return releveElement.code;
});
this.listRelevesProduction.forEach(releve => {
  if (codes.findIndex(code => code === releve.code) > -1) {
    listSelectedReleveProduction.push(releve);
  }
  });
}
this.select.emit(listSelectedReleveProduction);

}
transformDataToByConsumedByMatTable() {
  {label: 'N° Relevé', value: 'numeroReleveProduction', title: 'N° Relevé'} ,
  {label: 'Date Début Production', value: 'dateDebutProduction', title: 'Date Début Production'},
   {label: 'Date Fin Production', value: 'dateFinProduction', title: 'Date Fin Production'},
  {label: 'Durée(Hours)', value: 'dureeHours', title: 'Durée(Hours)'},
  {label: 'Qte Produit Intacte', value: 'qteProduitIntacte', title: 'Qte Produit Intacte'},
  {label: 'qteProduitDefaille', value: 'qteProduitDefaille', title: 'Qte Produit Domagée'},
  {label: 'Qte En Production', value:  'qteEnCoursProduction', title: 'Qte En Production' },
  {label: 'Côut', value:  'cout', title: 'Coût' },
  {label: 'Code Article', value:  'codeArticle', title: 'Code Article' },
  {label: 'Libéllé Article', value:  'libelleArticle', title: 'Libéllé Article' },
  {label: 'Désignation Article', value:  'designationArticle', title: 'Désignation Article' },
  {label: 'Dépôt Stockage', value:  'depotStockage', title: 'Dépôt Stockage' },
  {label: 'Conditions Stockage', value:  'conditionStockage', title: 'Conditions Stockage' },
  {label: 'Responsable', value:  'responsable', title: 'Responsable' },
  {label: 'Déscription', value: 'description', title: 'Déscription' },
  {label: 'Etat', value:  'etat', title: 'Etat' }

  const listElements = [];
  this.dataRelevesProductionSource = null;
  if (this.listRelevesProduction !== undefined) {
   //  console.log('diffe of undefined');
    this.listRelevesProduction.forEach(releveProduction => {
           listElements.push( {
             code: releveProduction.code ,
             numeroReleveProduction: releveProduction.numeroReleveProduction,
             dateDebutProduction: releveProduction.dateDebutProduction,
             dateFinProduction: releveProduction.dateFinProduction,
             cout: releveProduction.cout,
             dureeHours: releveProduction.dureeHours,
             responsable: releveProduction.responsable ,
             conditionStockage: releveProduction.conditionsStockage,
             codeArticle: releveProduction.articleProduit.designation,
             designationArticle: releveProduction.articleProduit.designation,
             libelleArticle: releveProduction.articleProduit.libelle,
             depotStockage: releveProduction.depotStockage,
             
             qteEnCoursProduction: releveProduction.qteEnCoursProduction,
             qteProduitDefaille: releveProduction.qteProduitDefaille,
             qteProduitIntacte: releveProduction.qteProduitIntacte,
            description: releveProduction.description,
   etat: releveProduction.etat === 'Valide' ?
    'Validé' : releveProduction.etat === 'Annule' ? 'Annulé' :
    releveProduction.etat === 'EnCoursValidation' ?  'En Cours De Validation' :  releveProduction.etat === 'Initial' ? 'Initiale' : 'Ouvert'

      } );
    });
  }
    this.dataRelevesProductionSource = new MatTableDataSource<ReleveProductionElement>(listElements);
    this.dataRelevesProductionSource.sort = this.sort;
    this.dataRelevesProductionSource.paginator = this.paginator;
    this.relevesProductionService.getTotalRelevesProduction(null).subscribe((response) => {
    //  console.log('Total credits is ', response);
      this.dataRelevesProductionSource.paginator.length = response.totalRelevesProduction;
    });
}
 specifyListColumnsToBeDisplayed() {
 
  console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
  this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
?
   this.defaultColumnsDefinitions : [];
   this.selectedColumnsNames = [];
   
   if (this.selectedColumnsDefinitions !== undefined)
    {
      this.selectedColumnsDefinitions.forEach((colName) => { 
           const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
           const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
           const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
           this.columnsDefinitionsToDisplay.push(colDef);
          });
    }
   this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
  this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
             return colDef.displayTitle;
   });
   this.columnsNames = [];
   this.columnsNames[0] = 'select';
   this.columnsDefinitionsToDisplay .map((colDef) => {
    this.columnsNames.push(colDef.name);
   if (this.selectedColumnsDefinitions !== undefined) {
    this.selectedColumnsNames.push(colDef.displayTitle);
   }
});
   this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
    return colDef.displayTitleAr;
});
}
masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataRelevesProductionSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataRelevesProductionSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataRelevesProductionSource.filter = filterValue.trim().toLowerCase();
  if (this.dataRelevesProductionSource.paginator) {
    this.dataRelevesProductionSource.paginator.firstPage();
  }
}

loadAddReleveProductionComponent() {
  this.router.navigate(['/pms/suivie/ajouterReleveProduction']) ;

}
loadEditReleveProductionComponent() {
  this.router.navigate(['/pms/suivie/editerReleveProduction', this.selection.selected[ this.selection.selected.length - 1].code]) ;

}
supprimerRelevesProduction()
{
 
  if ( this.selection.selected.length !== 0) {
    this.deleteEnCours = true;
      this.selection.selected.forEach(function(releve, index) {
          this.relevesProductionService.deleteReleve(releve.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.deleteEnCours = false;
            
            this.openSnackBar( this.selection.selected.length + ' Element(s) Supprimé(s)');
             this.loadData(null);
            }
      }, this);
    } else {
      this.openSnackBar('Aucun Relevé Séléctionné !');
    }
}
refresh() {
  this.loadData(null);
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 
 toggleFilterPanel()
 {
    this.showFilter = !this.showFilter;
 }
 toggleShowColumnsMultiSelect()
 {
   this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
 }
 loadFiltredData()
 {
  //  console.log('the filter is ',this.journalFilter);
   this.loadData(this.releveProductionFilter);
 }

changePage($event) {
  console.log('page event is ', $event);
 this.journauxService.getJournaux($event.pageIndex, $event.pageSize, this.journalFilter).subscribe((journaux) => {
    this.listJournaux = journaux;
   // this.transformDataToByConsumedByMatTable();
   const listElements = [];
     console.log('diffe of undefined');
     this.listRelevesProduction.forEach(releveProduction => {
      listElements.push( {         
           code: releveProduction.code ,
        numeroReleveProduction: releveProduction.numeroReleveProduction,
        dateDebutProduction: releveProduction.dateDebutProduction,
        dateFinProduction: releveProduction.dateFinProduction,
        cout: releveProduction.cout,
        dureeHours: releveProduction.dureeHours,
        responsable: releveProduction.responsable ,
        conditionStockage: releveProduction.conditionsStockage,
        codeArticle: releveProduction.articleProduit.designation,
        designationArticle: releveProduction.articleProduit.designation,
        libelleArticle: releveProduction.articleProduit.libelle,
        depotStockage: releveProduction.depotStockage,
        
        qteEnCoursProduction: releveProduction.qteEnCoursProduction,
        qteProduitDefaille: releveProduction.qteProduitDefaille,
        qteProduitIntacte: releveProduction.qteProduitIntacte,
       description: releveProduction.description,
etat: releveProduction.etat === 'Valide' ?
'Validé' : releveProduction.etat === 'Annule' ? 'Annulé' :
releveProduction.etat === 'EnCoursValidation' ?  'En Cours De Validation' :  releveProduction.etat === 'Initial' ? 'Initiale' : 'Ouvert'


 } );

});
this.dataRelevesProductionSource = new MatTableDataSource<ReleveProductionElement>(listElements);
this.relevesProductionService.getTotalRelevesProduction(null).subscribe((response) => {
 console.log('Total credits is ', response);
  this.dataRelevesProductionSource.paginator.length = response.totalRelevesProduction;
});
});
}
}
