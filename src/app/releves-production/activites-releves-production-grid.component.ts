
import { Component, OnInit, Inject,Input, ViewChild, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {RelevesProductionService} from './releves-production.service';
import {ReleveProduction} from './releve-production.model';

import {ActiviteReleveProduction} from './activite-releve-production.model';
import { ActiviteReleveProductionElement } from './activite-releve-production.interface';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogAddActiviteReleveProductionComponent } from './popups/dialog-add-activite-releve-production.component';
import { DialogEditActiviteReleveProductionComponent } from './popups/dialog-edit-activite-releve-production.component';
@Component({
    selector: 'app-activites-releves-production-grid',
    templateUrl: 'activites-releves-production-grid.component.html',
  })
  export class ActivitesRelevesProductionGridComponent implements OnInit, OnChanges {
     private listActivitesRelevesProduction: ActiviteReleveProduction[] = [];
     private  dataActivitesRelevesProductionSource: MatTableDataSource<ActiviteReleveProductionElement>;
     private selection = new SelectionModel<ActiviteReleveProductionElement>(true, []);
    
     @Input()
     private releveProduction : ReleveProduction

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private relevesProductionService: RelevesProductionService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.relevesProductionService.getActivitesRelevesProductionByCodeReleve
      (this.releveProduction.code).subscribe((listActivitesRelevesProduction) => {
        this.listActivitesRelevesProduction = listActivitesRelevesProduction;
  // console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
  }
  ngOnChanges()
  {
    this.specifyListColumnsToBeDisplayed();
    
    this.relevesProductionService.getActivitesRelevesProductionByCodeReleve
    (this.releveProduction.code).subscribe((listActivitesReleveProduction) => {
      this.listActivitesRelevesProduction = listActivitesReleveProduction;
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }

  checkOneActionInvoked() {
    const listSelectedActiviteReleveProduction: ActiviteReleveProduction[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((activiteReleveProductionJElement) => {
    return activiteReleveProductionJElement.code;
  });
  this.listActivitesRelevesProduction.forEach(lj => {
    if (codes.findIndex(code => code === lj.code) > -1) {
      listSelectedActiviteReleveProduction.push(lj);
    }
    });
  }
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listActivitesRelevesProduction !== undefined) {

      this.listActivitesRelevesProduction.forEach(rP => {
             listElements.push( {code: rP.code ,
              numeroActiviteReleveProduction: rP.numeroActiviteReleveProduction,
                  dureeHours: rP.dureeHours,
                 dateDebut: rP.dateDebut,
                 dateFin: rP.dateFin,
                 nomenclature: rP.nomenclature,
                 cout: rP.cout,
                description: rP.description,
                etat: rP.etat,
        } );
      });
    }
      this.dataActivitesRelevesProductionSource= new MatTableDataSource<ActiviteReleveProductionElement>(listElements);
      this.dataActivitesRelevesProductionSource.sort = this.sort;
      this.dataActivitesRelevesProductionSource.paginator = this.paginator;
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [
       {name: 'numeroActiviteReleveProduction' , displayTitle: 'N°ActiviteReleveProduction'},
     {name: 'dureeHours' , displayTitle: 'Durée(Heures)'},
     {name: 'nomenclature' , displayTitle: 'Nomenclature'},
     {name: 'cout' , displayTitle: 'Coût'},
     { name : 'etat' , displayTitle : 'Etat'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames = [];
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataActivitesRelevesProductionSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataActivitesRelevesProductionSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataActivitesRelevesProductionSource.filter = filterValue.trim().toLowerCase();
    if (this.dataActivitesRelevesProductionSource.paginator) {
      this.dataActivitesRelevesProductionSource.paginator.firstPage();
    }
  }
  showAddActiviteReleveProductionDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeReleveProduction: this.releveProduction.code
   };
 
   const dialogRef = this.dialog.open(DialogAddActiviteReleveProductionComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(lAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditActiviteReleveProductionDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeActiviteReleveProduction : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditActiviteReleveProductionComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(lAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucune Activité  Séléctionnée!');
    }
    }
    supprimerActivitesReleveProduction()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(actRelP, index) {
       this.relevesProductionService.
       deleteActiviteReleveProduction(actRelP.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar(' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucune Activité  Séléctionnée!');
        }
    }
    refresh() {
      this.relevesProductionService.getActivitesRelevesProductionByCodeReleve
      (this.releveProduction.code).subscribe((listActivitesReleveProduction) => {
        this.listActivitesRelevesProduction = listActivitesReleveProduction;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
