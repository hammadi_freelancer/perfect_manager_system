


export interface MaterielReleveProductionElement {
         code: string ;
         numeroReleveProduction: string ;
         numeroMaterielReleveProduction: string ;
         reference: string ;
         marque: string ;
         matricule: string ;
         dureeExploitationHours: number ;
         dateAchats: string;
         dateFabrication: string;
          description?: string;
          etat?: string;
          
}
