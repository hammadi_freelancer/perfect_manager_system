
import { BaseEntity } from '../common/base-entity.model' ;

import { Article } from '../stocks/articles/article.model' ;

 
export class ActiviteReleveProduction extends BaseEntity {
    constructor(
        public code?: string,
        public codeReleveProduction?: string,
        public numeroReleveProduction?: string,
         public numeroActiviteReleveProduction?: string,
         public nomenclature?: string,
         public dateDebutObject?: Date,
         public dateDebut?: string,
         public dateFinObject?: Date,
         public dateFin?: string,
         public dureeHours?: number,
         public articleProduit?: Article,
         public qteProduitIntacte?: number,
         public qteProduitDefaille?: number,
         public qteEnCoursProduction?: number,
         public cout?: number,
          public etat?: string,
     ) {
         super();
        
       }
  
}
