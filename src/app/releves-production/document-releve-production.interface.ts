


export interface DocumentReleveProductionElement {
         code: string ;
         dateReception: string ;
         numeroDocument: number;
         typeDocument?: string;
          description?: string;
}
