import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';
import { HttpParams } from '@angular/common/http';

import {ReleveProduction} from './releve-production.model' ;
// import { TrancheCredit } from './tranche-credit.model';
import { DocumentReleveProduction } from './document-releve-production.model';
import { PaiementReleveProduction } from './paiement-releve-production.model';
import { ActiviteReleveProduction } from './activite-releve-production.model';
import { MaterielReleveProduction } from './materiel-releve-production.model';

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_RELEVES_PRODUCTION = 
'http://localhost:8082/relevesProduction/getRelevesProduction';

const  URL_GET_PAGE_RELEVES_PRODUCTION = 
'http://localhost:8082/relevesProduction/getPageRelevesProduction';

const URL_GET_RELEVE_PRODUCTION = 'http://localhost:8082/relevesProduction/getReleveProduction';
const  URL_ADD_RELEVE_PRODUCTION = 'http://localhost:8082/relevesProduction/addReleveProduction';
const  URL_UPDATE_RELEVE_PRODUCTION = 'http://localhost:8082/relevesProduction/updateReleveProduction';
const  URL_DELETE_RELEVE_PRODUCTION = 'http://localhost:8082/relevesProduction/deleteReleveProduction';
const  URL_GET_TOTAL_RELEVES_PRODUCTION = 'http://localhost:8082/relevesProduction/getTotalRelevesProduction';



  const  URL_ADD_DOCUMENT_RELEVE_PRODUCTION = 'http://localhost:8082/documentsRelevesProduction/addDocumentReleveProduction';
  const  URL_GET_LIST_DOCUMENT_RELEVE_PRODUCTION = 'http://localhost:8082/documentsRelevesProduction/getDocumentsRelevesProduction';
  const  URL_DELETE_DOCUCMENT_RELEVE_PRODUCTION = 'http://localhost:8082/documentsRelevesProduction/deleteDocumentReleveProduction';
  const  URL_UPDATE_DOCUMENT_RELEVE_PRODUCTION = 'http://localhost:8082/documentsRelevesProduction/updateDocumentReleveProduction';
  const URL_GET_DOCUMENT_RELEVE_PRODUCTION = 'http://localhost:8082/documentsRelevesProduction/getDocumentReleveProduction';
  const URL_GET_LIST_DOCUMENT_RELEVE_PRODUCTION_BY_CODE_RELEVE_PRODUCTION =
   'http://localhost:8082/documentsRelevesProduction/getDocumentsRelevesProductionByCodeReleve';


   const  URL_ADD_PAIEMENT_RELEVE_PRODUCTION = 'http://localhost:8082/paiementsRelevesProduction/addPaiementReleveProduction';
   const  URL_GET_LIST_PAIEMENT_RELEVE_PRODUCTION = 'http://localhost:8082/paiementsRelevesProduction/getPaiementsRelevesProduction';
   const  URL_DELETE_PAIEMENT_RELEVE_PRODUCTION = 'http://localhost:8082/paiementsRelevesProduction/deletePaiementReleveProduction';
   const  URL_UPDATE_PAIEMENT_RELEVE_PRODUCTION = 'http://localhost:8082/paiementsRelevesProduction/updatePaiementReleveProduction';
   const URL_GET_PAIEMENT_RELEVE_PRODUCTION = 'http://localhost:8082/paiementsRelevesProduction/getPaiementReleveProduction';
   const URL_GET_LIST_PAIEMENT_RELEVE_PRODUCTION_BY_CODE_RELEVE_PRODUCTION =
    'http://localhost:8082/paiementsRelevesProduction/getPaiementsRelevesProductionByCodeReleve';

    const  URL_ADD_MATERIEL_RELEVE_PRODUCTION = 'http://localhost:8082/materielsRelevesProduction/addMaterielReleveProduction';
    const  URL_GET_LIST_MATERIEL_RELEVE_PRODUCTION = 'http://localhost:8082/materielsRelevesProduction/getMaterielsRelevesProduction';
    const  URL_DELETE_MATERIEL_RELEVE_PRODUCTION = 'http://localhost:8082/materielsRelevesProduction/deleteMaterielReleveProduction';
    const  URL_UPDATE_MATERIEL_RELEVE_PRODUCTION = 'http://localhost:8082/materielsRelevesProduction/updateMaterielReleveProduction';
    const URL_GET_MATERIEL_RELEVE_PRODUCTION = 'http://localhost:8082/materielsRelevesProduction/getMaterielReleveProduction';
    const URL_GET_LIST_MATERIEL_RELEVE_PRODUCTION_BY_CODE_RELEVE_PRODUCTION =
     'http://localhost:8082/materielsRelevesProduction/getMaterielsRelevesProductionByCodeReleve';

     const  URL_ADD_ACTIVITE_RELEVE_PRODUCTION = 'http://localhost:8082/activitesRelevesProduction/addActiviteReleveProduction';
     const  URL_GET_LIST_ACTIVITE_RELEVE_PRODUCTION = 'http://localhost:8082/activitesRelevesProduction/getActivitesRelevesProduction';
     const  URL_DELETE_ACTIVITE_RELEVE_PRODUCTION = 'http://localhost:8082/activitesRelevesProduction/deleteActiviteReleveProduction';
     const  URL_UPDATE_ACTIVITE_RELEVE_PRODUCTION = 'http://localhost:8082/activitesRelevesProduction/updateActiviteReleveProduction';
     const URL_GET_ACTIVITE_RELEVE_PRODUCTION = 'http://localhost:8082/activitesRelevesProduction/getActiviteReleveProduction';
     const URL_GET_LIST_ACTIVITE_RELEVE_PRODUCTION_BY_CODE_RELEVE_PRODUCTION =
      'http://localhost:8082/activitesRelevesProduction/getActivitesRelevesProductionByCodeReleve';

@Injectable({
  providedIn: 'root'
})
export class RelevesProductionService {

  constructor(private httpClient: HttpClient) {

   }

   getTotalRelevesProduction(filter): Observable<any> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('filter', filterStr);
        return this.httpClient
        .get<any>(URL_GET_TOTAL_RELEVES_PRODUCTION, {params});
       }
   getPageRelevesProduction(pageNumber, nbElementsPerPage, filter): Observable<ReleveProduction[]> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('pageNumber', pageNumber)
    .set('nbElementsPerPage', nbElementsPerPage)
    .set('sort', 'name')
    .set('filter', filterStr);
    return this.httpClient
    .get<ReleveProduction[]>(URL_GET_PAGE_RELEVES_PRODUCTION, {params});
   }
   addReleveProduction(relProd: ReleveProduction): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_RELEVE_PRODUCTION, relProd);
  }
  updateReleveProduction(relProd: ReleveProduction): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_RELEVE_PRODUCTION, relProd);
  }
  deleteReleveProduction(codeReleveProduction): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_RELEVE_PRODUCTION + '/' + codeReleveProduction);
  }
  getReleveProduction(codeReleveProduction): Observable<ReleveProduction> {
    return this.httpClient.get<ReleveProduction>(URL_GET_RELEVE_PRODUCTION + '/' + codeReleveProduction);
  }


  addDocumentReleveProduction(documentReleveProduction: DocumentReleveProduction): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_DOCUMENT_RELEVE_PRODUCTION, documentReleveProduction);
    
  }
  updateDocumentReleveProduction(docRelProd: DocumentReleveProduction): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_DOCUMENT_RELEVE_PRODUCTION, docRelProd);
  }
  deleteDocumentReleveProduction(codeDocumentReleveProduction): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_DOCUCMENT_RELEVE_PRODUCTION + '/' + codeDocumentReleveProduction);
  }
  getDocumentReleveProduction(codeDocumentReleveProduction): Observable<DocumentReleveProduction> {
    return this.httpClient.get<DocumentReleveProduction>(URL_GET_DOCUMENT_RELEVE_PRODUCTION + '/' + codeDocumentReleveProduction);
  }
  getDocumentsRelevesProduction(): Observable<DocumentReleveProduction[]> {
    return this.httpClient
    .get<DocumentReleveProduction[]>(URL_GET_LIST_DOCUMENT_RELEVE_PRODUCTION);
   }

  getDocumentsRelevesProductionByCodeReleve(codeDocumentReleveProduction): Observable<DocumentReleveProduction[]> {
    return this.httpClient.get<DocumentReleveProduction[]>(URL_GET_LIST_DOCUMENT_RELEVE_PRODUCTION_BY_CODE_RELEVE_PRODUCTION
      + '/' + codeDocumentReleveProduction);
  }

  addPaiementReleveProduction(paiReleveProduction: PaiementReleveProduction): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_PAIEMENT_RELEVE_PRODUCTION, paiReleveProduction);
    
  }
  updatePaiementReleveProduction(paiReleveProduction: PaiementReleveProduction): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_PAIEMENT_RELEVE_PRODUCTION, paiReleveProduction);
  }
  deletePaiementReleveProduction(codePaiementReleveProduction): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_PAIEMENT_RELEVE_PRODUCTION + '/' + codePaiementReleveProduction);
  }
  getPaiementReleveProduction(codePaiementReleveProduction): Observable<PaiementReleveProduction> {
    return this.httpClient.get<PaiementReleveProduction>(URL_GET_PAIEMENT_RELEVE_PRODUCTION + '/' + codePaiementReleveProduction);
  }
  getPaiementsRelevesProduction(): Observable<PaiementReleveProduction[]> {
    return this.httpClient
    .get<PaiementReleveProduction[]>(URL_GET_LIST_PAIEMENT_RELEVE_PRODUCTION);
   }

  getPaiementsRelevesProductionByCodeReleve(codePaiementReleveProduction): Observable<PaiementReleveProduction[]> {
    return this.httpClient.get<PaiementReleveProduction[]>(URL_GET_LIST_PAIEMENT_RELEVE_PRODUCTION_BY_CODE_RELEVE_PRODUCTION
      + '/' + codePaiementReleveProduction);
  }


  addActiviteReleveProduction(actReleveProduction: ActiviteReleveProduction): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_ACTIVITE_RELEVE_PRODUCTION, actReleveProduction);
    
  }
  updateActiviteReleveProduction(actReleveProduction: ActiviteReleveProduction): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_ACTIVITE_RELEVE_PRODUCTION , actReleveProduction);
  }
  deleteActiviteReleveProduction(codeActiviteReleveProduction): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_ACTIVITE_RELEVE_PRODUCTION+ '/' + codeActiviteReleveProduction);
  }
  getActiviteReleveProduction(codeActiviteReleveProduction): Observable<ActiviteReleveProduction> {
    return this.httpClient.get<ActiviteReleveProduction>(URL_GET_ACTIVITE_RELEVE_PRODUCTION+ '/' + codeActiviteReleveProduction);
  }
  getActivitesRelevesProduction(): Observable<ActiviteReleveProduction[]> {
    return this.httpClient
    .get<ActiviteReleveProduction[]>(URL_GET_LIST_ACTIVITE_RELEVE_PRODUCTION);
   }

  getActivitesRelevesProductionByCodeReleve(codeActReleveProduction): Observable<ActiviteReleveProduction[]> {
    return this.httpClient.get<ActiviteReleveProduction[]>(URL_GET_LIST_ACTIVITE_RELEVE_PRODUCTION_BY_CODE_RELEVE_PRODUCTION
      + '/' + codeActReleveProduction);
  }

  addMaterielReleveProduction(actReleveProduction: MaterielReleveProduction): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_MATERIEL_RELEVE_PRODUCTION, actReleveProduction);
    
  }
  updateMaterielReleveProduction(actReleveProduction: MaterielReleveProduction): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_MATERIEL_RELEVE_PRODUCTION, actReleveProduction);
  }
  deleteMaterielReleveProduction(codeMaterielReleveProduction): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_MATERIEL_RELEVE_PRODUCTION+ '/' + codeMaterielReleveProduction);
  }
  getMaterielReleveProduction(codeMaterielReleveProduction): Observable<MaterielReleveProduction> {
    return this.httpClient.get<MaterielReleveProduction>(URL_GET_MATERIEL_RELEVE_PRODUCTION 
      + '/' + codeMaterielReleveProduction);
  }
  getMaterielsRelevesProduction(): Observable<MaterielReleveProduction[]> {
    return this.httpClient
    .get<MaterielReleveProduction[]>(URL_GET_LIST_MATERIEL_RELEVE_PRODUCTION);
   }

  getMaterielsRelevesProductionByCodeReleve(codeMatReleveProduction): Observable<ActiviteReleveProduction[]> {
    return this.httpClient.get<MaterielReleveProduction[]>(URL_GET_LIST_MATERIEL_RELEVE_PRODUCTION_BY_CODE_RELEVE_PRODUCTION
      + '/' + codeMatReleveProduction);
  }



}
