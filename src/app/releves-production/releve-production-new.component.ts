import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {ReleveProduction} from './releve-production.model';

import {RelevesProductionService} from './releves-production.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {MatSnackBar} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Article} from '../stocks/articles/article.model';
import { ArticleFilter } from '../stocks/articles/article.filter';
import { UnMesureFilter } from '../stocks/un-mesures/un-mesures.filter';

@Component({
  selector: 'app-releve-production-new',
  templateUrl: './releve-production-new.component.html',
  // styleUrls: ['./players.component.css']
})
export class ReleveProductionNewComponent implements OnInit {

 private releveProduction: ReleveProduction =  new ReleveProduction();
 private saveEnCours = false;
 private listArticles: Article[];
 private articleFilter: ArticleFilter;
 private unMesureFilter : UnMesureFilter;

private etatsReleveProduction: any[] = [
  {value: 'Ouvert', viewValue: 'Ouvert'},
  {value: 'Initial', viewValue: 'Initiale'},
  {value: 'EnCoursValidation', viewValue: 'En Cours de Validation'},
  {value: 'Valide', viewValue: 'Validé'},
  {value: 'Cloture', viewValue: 'Cloturé'}
];
  constructor( private route: ActivatedRoute,
    private router: Router, private relevesProductionService: RelevesProductionService,
   private matSnackBar: MatSnackBar) {
  }

  ngOnInit() {
       this.releveProduction.etat = 'Initial';
       this.listArticles = this.route.snapshot.data.articles;
       // this.listCodesArticles  = this.listArticles.map((article) => article.code);
        // this.clientFilter = new ClientFilter(this.listClients);
        this.articleFilter = new ArticleFilter(this.route.snapshot.data.articles,
         [], [] );
          this.unMesureFilter = new UnMesureFilter(this.route.snapshot.data.unMesures);
          
  }
  saveReleveProduction() {

   /* if( this.releveProduction.etat === undefined || this.releveProduction.etat === 'Ouvert' ) {
      this.releveProduction.etat = 'Initial';
    }*/
    // this.saveEnCours = true;
    this.saveEnCours = true;
    this.relevesProductionService.addReleveProduction(this.releveProduction).subscribe((response) => {
      this.saveEnCours = false;
      
        if (response.error_message ===  undefined) {
          console.log('undefined error message');
          // this.save.emit(response);
          this.releveProduction =  new ReleveProduction();
          this.openSnackBar(  'relevé production Enregistré');
        } else {
         // show error message
         this.openSnackBar(  'Erreurs: Opération Echouée');
         
        }
    });
  }


  vider() {
    this.releveProduction = new ReleveProduction();
  }
  openSnackBar(messageToDisplay) {
   this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
       verticalPosition: 'top'});
  }
loadGridRelevesProduction() {
  this.router.navigate(['/pms/production/relevesProduction']) ;
}
fillDataLibelleArticle()  {
  
    this.listArticles.forEach((article, index) => {
               if (this.releveProduction.articleProduit.code === article.code) {
                this.releveProduction.articleProduit.designation = article.designation;
                this.releveProduction.articleProduit.libelle = article.libelle;
                }
  
          } , this);
 }
}
