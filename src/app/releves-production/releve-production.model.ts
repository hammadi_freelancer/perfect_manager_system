
import { BaseEntity } from '../common/base-entity.model' ;
import { Article } from '../stocks/articles/article.model' ;

type Etat = 'Initial' | 'Valide' | 'Annule' | 'EnCoursValidation';

export class ReleveProduction extends BaseEntity {
    constructor(
        public code?: string,
        public numeroReleveProduction?: string,
        public dateDebutProductionObject?: Date,
        public dateDebutProduction?: string,
        public dateFinProductionObject?: Date,
        public dateFinProduction?: string,
         public dureeHours?: number,
         public articleProduit?: Article,
         public qteProduitIntacte?: number,
         public qteProduitDefaille?: number,
         public qteEnCoursProduction?: number,
         public cout?: number,
         public depotStockage?: string,
         public conditionsStockage?: string,
         public responsable?: string,
         public etat?: Etat,
    // public listTranches?: Tranche[]
     ) {
         super();
      
    }
 
}
