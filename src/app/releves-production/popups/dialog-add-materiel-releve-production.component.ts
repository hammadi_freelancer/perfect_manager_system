
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {RelevesProductionService} from '../releves-production.service';
import {ReleveProduction} from '../releve-production.model';

@Component({
    selector: 'app-dialog-add-materiel-releve-production',
    templateUrl: 'dialog-add-materiel-releve-production.component.html',
  })
  export class DialogAddMaterielReleveProductionComponent implements OnInit {
     private releveProduction: ReleveProduction;
     private saveEnCours = false;

     constructor(  private materielsRelevesService: RelevesProductionService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.releveProduction = new ReleveProduction();
    }
    saveMaterielReleveProduction() 
    {
      this.ligneJournal.codeJournal = this.data.codeJournal;
       this.saveEnCours = true;
       this.journauxService.addLigneJournal(this.ligneJournal).subscribe((response) => {
         this.saveEnCours = false;
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.ligneJournal = new LigneJournal();
             this.openSnackBar(  'Ligne Enregistrée');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
 
  }
