
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {JournauxService} from '../journaux.service';
import {DocumentJournal} from '../document-journal.model';
import { DocumentJournalElement } from '../document-journal.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-documents-journaux',
    templateUrl: 'dialog-list-documents-journaux.component.html',
  })
  export class DialogListDocumentsJournauxComponent implements OnInit {
     private listDocumentsJournaux: DocumentJournal[] = [];
     private  dataDocumentsJournauxSource: MatTableDataSource<DocumentJournalElement>;
     private selection = new SelectionModel<DocumentJournalElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private journauxService: JournauxService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      this.journauxService.getDocumentsJournauxByCodeJournal(this.data.codeJournal).subscribe((listDocumentsJRS) => {
        this.listDocumentsJournaux = listDocumentsJRS;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }

  checkOneActionInvoked() {
    const listSelectedDocumentsJournaux: DocumentJournal[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentJelement) => {
    return documentJelement.code;
  });
  this.listDocumentsJournaux.forEach(documentJournal=> {
    if (codes.findIndex(code => code === documentJournal.code) > -1) {
      listSelectedDocumentsJournaux.push(documentJournal);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsJournaux !== undefined) {

      this.listDocumentsJournaux.forEach(docJ => {
             listElements.push( {code: docJ.code ,
          dateReception: docJ.dateReception,
          numeroDocument: docJ.numeroDocument,
          typeDocument: docJ.typeDocument,
          description: docJ.description
        } );
      });
    }
      this.dataDocumentsJournauxSource = new MatTableDataSource<DocumentJournalElement>(listElements);
      this.dataDocumentsJournauxSource.sort = this.sort;
      this.dataDocumentsJournauxSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    //  console.log('calling specifyListColumnsToBeDisplayed documents grid');
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
    
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
      console.log(this.columnsNames);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsJournauxSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsJournauxSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsJournauxSource.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsJournauxSource.paginator) {
      this.dataDocumentsJournauxSource.paginator.firstPage();
    }
  }
 




}
