
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {JournauxService} from '../journaux.service';
import {LigneJournal} from '../ligne-journal.model';

@Component({
    selector: 'app-dialog-edit-ligne-journal',
    templateUrl: 'dialog-edit-ligne-journal.component.html',
  })
  export class DialogEditLigneJournalComponent implements OnInit {
     private ligneJournal: LigneJournal;
     private updateEnCours = false;
     private etatsModesPaiements: any[] = [
      {value: 'Espece', viewValue: 'Espèce'},
      {value: 'Cheque', viewValue: 'Chèque'},
      {value: 'Virement', viewValue: 'Virement'},
      {value: 'Credit', viewValue: 'Crédit'}
    ];
    private etatsLignesJournal: any[] = [
      {value: 'Annule', viewValue: 'Annulé'},
      {value: 'Initial', viewValue: 'Initial'},
      {value: 'Valide', viewValue: 'Validé'}
    ];
     private typesOperations: any[] = [
      {value: 'AchatsMatieresPrimaires', viewValue: 'Achats Matières Primaires'},
      {value: 'AchatsMarchandises', viewValue: 'Achats Marchandises'},
      {value: 'AchatsFournituresBureau', viewValue: 'Achats Fournitures Bureau'},
      {value: 'AchatsMaterielles', viewValue: 'Achats Materielles'},
      {value: 'AutresAchats', viewValue: 'Autres Achats'},
      {value: 'Electricite', viewValue: 'Electricité'},
      {value: 'EauPotable', viewValue: 'Eau Potable'},
      {value: 'ConsommationTelephonique', viewValue: 'Consommation Téléphonique'},
      {value: 'Transport', viewValue: 'Transport'},
      {value: 'Location', viewValue: 'Locations'},
      {value: 'Impots', viewValue: 'Impots'},
      {value: 'AutresCharges', viewValue: 'Autres Charges'},
      {value: 'VentesMarchandises', viewValue: 'Ventes Marchandises'},
      {value: 'VentesProduitsFinis', viewValue: 'Ventes Produits Finis'},
      {value: 'VentesMatieresPrimaires', viewValue: 'Ventes Matieres Primaires'},
      {value: 'VentesProduitsSemiFinis', viewValue: 'Ventes Produits Semi Finis'},
      {value: 'VentesMateriellesEpuisées', viewValue: 'Ventes Materielles Epuisées'},
      {value: 'VentesLocaux', viewValue: 'Ventes Locaux'},
      {value: 'VentesProduitsNonConformes', viewValue: 'Ventes Produits Non Conformes'},
      {value: 'AutresVentes', viewValue: 'Autres Ventes'},

      {value: 'PaieSalaires', viewValue: 'Paie Salaires'},
      {value: 'PaieHoraires', viewValue: 'Paie Horaires'},
      {value: 'PaieCNSS', viewValue: 'Paie CNSS'},
      {value: 'PaiePrimes', viewValue: 'Paie Primes'},
      {value: 'PaieAvances', viewValue: 'Paie Avances'},
      {value: 'PaieRecompenses', viewValue: 'Paie Récompenses'},
      {value: 'PaieRecrutements', viewValue: 'Paie Recrutements'},
      {value: 'AutresPaies', viewValue: 'Autres Paies'},
    ];
   
     constructor(  private journauxService: JournauxService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {

        this.journauxService.getLigneJournal(this.data.codeLigneJournal).subscribe((ligneJournal) => {
            this.ligneJournal = ligneJournal;
            if (this.ligneJournal.etat === 'Valide' || this.ligneJournal.etat === 'Annule')
              {
                this.etatsLignesJournal.splice(1, 1);
              }
              if (this.ligneJournal.etat === 'Initial')
                {
                  this.etatsLignesJournal.splice(0, 1);
                }
     });
    }
    
    updateLigneJournal() 
    {
      this.updateEnCours = true;
       this.journauxService.updateLigneJournal(this.ligneJournal).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {

          if (this.ligneJournal.etat === 'Valide' || this.ligneJournal.etat === 'Annule' ) {
             //  this.tabsDisabled = false;
              this.etatsLignesJournal = [
                {value: 'Annule', viewValue: 'Annulé'},
                {value: 'Valide', viewValue: 'Validé'}
      
              ];
            }
         
             // this.save.emit(response);
             this.openSnackBar(  'Ligne Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }


  }
