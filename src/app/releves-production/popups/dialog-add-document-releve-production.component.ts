
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {JournauxService} from '../journaux.service';
import {DocumentJournal} from '../document-journal.model';

@Component({
    selector: 'app-dialog-add-document-journal',
    templateUrl: 'dialog-add-document-journal.component.html',
  })
  export class DialogAddDocumentJournalComponent implements OnInit {
     private documentJournal: DocumentJournal;
     private saveEnCours = false;
     private typesDoc: any[] = [
      {value: 'FactureVentes', viewValue: 'Facture Ventes'},
      {value: 'FactureAchats', viewValue: 'Facture Achats'},
      {value: 'FactureTelephonique', viewValue: 'Facture Téléphonique'},
      {value: 'FactureElectricite', viewValue: 'Facture Eléctricité'},
      {value: 'FactureEauPotable', viewValue: 'Facture Eau Potable'},
      {value: 'FichePaie', viewValue: 'Fiche Paie'},
      {value: 'BonLivraison', viewValue: 'Bon Livraison'},
      {value: 'BonReception', viewValue: 'Bon Récéption'},
      {value: 'RecuPayement', viewValue: 'Reçu Payement'},
      {value: 'Cheque', viewValue: 'Chèque'},
      {value: 'Traite', viewValue: 'Traite'},
      {value: 'Autre', viewValue: 'Autre'},
    ];
     constructor(  private journauxService: JournauxService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.documentJournal = new DocumentJournal();
    }
    saveDocumentJournal() 
    {
      this.documentJournal.codeJournal = this.data.codeJournal;
       console.log(this.documentJournal);
        this.saveEnCours = true;
       this.journauxService.addDocumentJournal(this.documentJournal).subscribe((response) => {
         this.saveEnCours = false;
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.documentJournal = new DocumentJournal();
             this.openSnackBar(  'Document Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs: Opération Echouée');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
     imageFace1Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentJournal.imageFace1 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imageFace2Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentJournal.imageFace2 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
  }
