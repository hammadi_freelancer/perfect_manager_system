
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {RelevesProductionService} from '../releves-production.service';
import {PaiementReleveProduction} from '../paiement-releve-production.model';

@Component({
    selector: 'app-dialog-add-paiement-releve-production',
    templateUrl: 'dialog-add-paiement-releve-production.component.html',
  })
  export class DialogAddPaiementReleveProductionComponent implements OnInit {
     private paiementReleveProduction: PaiementReleveProduction;
     
     constructor(  private  releveProductionService: ReleveProductionService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.paiementReleveProduction = new PaiementReleveProduction();
    }
    savePaiementReleveProduction
    () 
    {
      this.paiementReleveProduction.codeReleveProduction= this.data.codeReleveProduction;
      // console.log(this.payementFactureVentes);
      //  this.saveEnCours = true;
       this.releveProductionService.addPaiementReleveProduction(this.paiementReleveProduction).subscribe((response) => {
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.paiementReleveProduction = new PaiementReleveProduction();
             this.openSnackBar(  'Paiement Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  }
