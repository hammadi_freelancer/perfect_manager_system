
import { Component, OnInit, Inject, ViewChild, Input, AfterViewChecked, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {RelevesProductionService} from './releves-production.service';
import { ReleveProduction } from './releve-production.model';
import {PaiementReleveProduction} from './paiement-releve-production.model';
import { PaiementReleveProductionElement } from './paiement-releve-production.interface';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
 import { DialogAddPaiementReleveProductionComponent} from './popups/dialog-add-paiement-releve-production.component';
 // import { DialogEditPaiementReleveProductionComponent } from './popups/dialog-edit-paiement-releve-production.component';
@Component({
    selector: 'app-paiements-releves-production-grid',
    templateUrl: 'paiements-releves-production-grid.component.html',
  })
  export class PaiementsRelevesProductionGridComponent implements OnInit, OnChanges {
     private listPaiementsRelevesProduction: PaiementReleveProduction[] = [];
     private  dataPaiementsRelevesProductionSource: MatTableDataSource<PaiementReleveProductionElement>;
     private selection = new SelectionModel<PaiementReleveProductionElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];

     @Input()
     private releveProduction: ReleveProduction;

     constructor(  private releveProdService: RelevesProductionService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      // console.log('credit code after dipslay tab ', this.credit.code);
      this.releveProdService.getPaiementsRelevesProductionByCodeReleve
      (this.releveProduction.code).subscribe((listPaiementsRProduction) => {
        this.listPaiementsRelevesProduction= listPaiementsRProduction;
// console.log('recieving data from backend', this.listPaiementsCredits  );
        this.transformDataToByConsumedByMatTable();
         });

  }

  ngOnChanges() {
    this.releveProdService.getPaiementsRelevesProductionByCodeReleve(this.releveProduction.code).subscribe((listPaiementsRProduction) => {
      this.listPaiementsRelevesProduction = listPaiementsRProduction;
// console.log('recieving data from backend', this.listPaiementsCredits  );
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }
  checkOneActionInvoked() {
    const listSelectedPaiementsRelProduction: PaiementReleveProduction[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((payementRelElement) => {
    return payementRelElement.code;
  });
  this.listPaiementsRelevesProduction.forEach(payementR => {
    if (codes.findIndex(code => code === payementR.code) > -1) {
      listSelectedPaiementsRelProduction.push(payementR);
    }
    });
  }
  // this.select.emit(listSelectedPaiementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listPaiementsRelevesProduction !== undefined) {

      this.listPaiementsRelevesProduction.forEach(payementRelProduction => {
             listElements.push( {code: payementRelProduction.code ,
          dateOperation: payementRelProduction.dateOperation,
          montantPaye: payementRelProduction.montantPaye,
          modePaiement: payementRelProduction.modePayement,
          numeroCheque: payementRelProduction.numeroCheque,
          dateCheque: payementRelProduction.dateCheque,
          compte: payementRelProduction.compte,
          compteAdversaire: payementRelProduction.compteAdversaire,
          banque: payementRelProduction.banque,
          banqueAdversaire: payementRelProduction.banqueAdversaire,
          description: payementRelProduction.description
        } );
      });
    }
      this.dataPaiementsRelevesProductionSource = new MatTableDataSource<PaiementReleveProductionElement>(listElements);
      this.dataPaiementsRelevesProductionSource.sort = this.sort;
      this.dataPaiementsRelevesProductionSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'}, 
     {name: 'montantPaye' , displayTitle: 'Montant Payé'},
     {name: 'modePaiement' , displayTitle: 'Mode Paiement'},
      {name: 'numeroCheque' , displayTitle: 'N°Chèque'},
     {name: 'dateCheque' , displayTitle: 'Date Chèque'}, 
     {name: 'compte' , displayTitle: 'Compte'},
     {name: 'compteAdversaire' , displayTitle: 'Compte Adv.'},
      {name: 'banque' , displayTitle: 'Banque'},
       { name : 'banqueAdversaire' , displayTitle : 'Banque Adv.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataPaiementsRelevesProductionSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataPaiementsRelevesProductionSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataPaiementsRelevesProductionSource.filter = filterValue.trim().toLowerCase();
    if (this.dataPaiementsRelevesProductionSource.paginator) {
      this.dataPaiementsRelevesProductionSource.paginator.firstPage();
    }
  }

  showAddPaiementDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeReleveProduction : this.releveProduction.code
   };
 
   const dialogRef = this.dialog.open(DialogAddPaiementReleveProductionComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(payAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }

    supprimerPaiements()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(pay, index) {
              this.releveVenteService.deletePaiementReleveProduction(pay.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
        //  this.openSnackBar('Aucun Paiement Séléctionnée!');
        }
    }
    refresh() {
      this.releveProdService.getPaiementsRelevesProductionByCodeReleve(this.releveProduction.code).subscribe((listPaiementsRels) => {
        this.listPaiementsRelevesProduction = listPaiementsRels;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
















}
