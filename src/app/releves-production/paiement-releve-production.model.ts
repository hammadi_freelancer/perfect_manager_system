
import { BaseEntity } from '../common/base-entity.model' ;

type Etat = 'Initial' | 'Valide' | 'Annule' 
type ModePayement = 'Espece' | 'Cheque' | 'Virement'

export class PaiementReleveProduction extends BaseEntity {
    constructor(
        public code?: string,
         public codeReleveProduction?: string,
         public montantPaye?: number,
        public dateOperationObject?: Date,
        public modePayement?: ModePayement,
        public numeroCheque?: string,
        public dateChequeObject?: Date,
        public dateOperation?: string,
        public dateCheque?: string,
        
        public compte?: string,
        public compteAdversaire?: string,
        public banque?: string,
        public banqueAdversaire?: string,
     ) {
         super();
        
       }
  
}
