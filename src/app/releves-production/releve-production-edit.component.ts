import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,
  AfterViewInit , AfterContentInit,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {ReleveProduction} from './releve-production.model';

import {RelevesProductionService} from './releves-production.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { MatDatepickerInput } from '@angular/material/datepicker';
import { MatSnackBar } from '@angular/material';
import { MatDatepicker } from '@angular/material/datepicker';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-releve-production-edit',
  templateUrl: './releve-production-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class ReleveProductionEditComponent implements OnInit  {

  private releveProduction: ReleveProduction =  new ReleveProduction();
  private tabsDisabled = true;
  private updateEnCours = false;
  private etatsReleveProduction: any[] = [
    {value: 'Ouvert', viewValue: 'Ouvert'},
    {value: 'Initial', viewValue: 'Initiale'},
    {value: 'EnCoursValidation', viewValue: 'En Cours de Validation'},
    {value: 'Valide', viewValue: 'Validé'},
    {value: 'Cloture', viewValue: 'Cloturé'},
    {value: 'Annule', viewValue: 'Annulé'}
  ];
  constructor( private route: ActivatedRoute,
    private router: Router, private relevesProductionService: RelevesProductionService, private matSnackBar: MatSnackBar,
    public dialog: MatDialog
   ) {
  }
  ngOnInit() {
    const codeReleveProduction = this.route.snapshot.paramMap.get('codeReleveProduction');
    this.relevesProductionService.getReleveProduction(codeReleveProduction).subscribe((releveProduction) => {
             this.releveProduction = releveProduction;
             console.log('the date is ');
          
             if (this.releveProduction.etat === 'Initial')
              {
                this.tabsDisabled = false;
                this.etatsReleveProduction = [
                  {value: 'Initial', viewValue: 'Initial'},
                  {value: 'Valide', viewValue: 'Validé'},
                  {value: 'Annule', viewValue: 'Annulé'},
                  
                ];
              }
         
            if (this.releveProduction.etat === 'Valide')
              {
                this.tabsDisabled = false;
                this.etatsReleveProduction = [
                  {value: 'Annule', viewValue: 'Annulé'},
                  {value: 'Valide', viewValue: 'Validé'},
                  {value: 'Initial', viewValue: 'Initial'}
                  
                ];
              }
              if (this.releveProduction.etat === 'Annule')
                {
                  this.etatsReleveProduction = [
                    {value: 'Annule', viewValue: 'Annulé'},
                    {value: 'Valide', viewValue: 'Validé'},
                  ];
                  this.tabsDisabled = true;
               
                }
 
    });
  }
 
  loadAddReleveProductionComponent() {
    this.router.navigate(['/pms/production/ajouterReleveProduction']) ;
  }
  loadGridRelevesProduction() {
    this.router.navigate(['/pms/production/relevesProduction']) ;
  }
  supprimerReleveProduction() {
      this.relevesProductionService.deleteReleveProduction(this.releveProduction.code).subscribe((response) => {

        this.openSnackBar( ' Releve Production Supprimé');
        this.loadGridRelevesProduction() ;
            });
  }
updateReleveProduction() {
  this.updateEnCours = true;
  this.relevesProductionService.updateReleveProduction(this.releveProduction).subscribe((response) => {
    this.updateEnCours = false;
    if (response.error_message ===  undefined) {

      if (this.releveProduction.etat === 'Initial')
        {
         // this.tabsDisabled = true;
          this.etatsReleveProduction = [
            {value: 'Initial', viewValue: 'Initial'},
            {value: 'Valide', viewValue: 'Validé'},
            {value: 'Annule', viewValue: 'Annulé'},
            
          ];
        }
        
      if (this.releveProduction.etat === 'Valide')
        {
          this.tabsDisabled = false;
          this.etatsReleveProduction = [
            {value: 'Annule', viewValue: 'Annulé'},
            {value: 'Valide', viewValue: 'Validé'},
            {value: 'Initial', viewValue: 'Initiale'},
            
          ];
        }
        if (this.releveProduction.etat === 'Annule')
          {
            this.etatsReleveProduction = [
              {value: 'Annule', viewValue: 'Annulé'},
              {value: 'Valide', viewValue: 'Validé'},
            ];
            this.tabsDisabled = false;
         
          }
      this.openSnackBar( ' Relevé Production mis à jour ');
    
    } else {
      this.openSnackBar( ' Erreurs:Opération Echouée ');
    }
});
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

 selectedTabChanged($event) {
  if ($event.index === 4) { // Composition tab was selected
   // this.loadListArticles();
   } else if ($event.index === 1) {
    //  this.loadListSchemasConfiguration() ;
   }
  }
}
