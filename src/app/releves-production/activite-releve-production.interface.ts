


export interface ActiviteReleveProductionElement {
         code: string ;
         numeroActiviteReleveProduction: string ;
         nomenclature: string ;
         dureeHours: number ;
         dateDebut: string;
         dateFin: string;
         codeArticleProduit: string;
         libelleArticleProduit: string;
         designationArticleProduit: string;
         qteProduitIntacte: number;
         qteProduitDefaille: number;
         cout: number;
         etat: string ;
          description?: string;

}
