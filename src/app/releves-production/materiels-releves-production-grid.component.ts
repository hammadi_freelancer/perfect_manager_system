
import { Component, OnInit, Inject,Input, ViewChild, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {RelevesProductionService} from './releves-production.service';
import {ReleveProduction} from './releve-production.model';

import {MaterielReleveProduction} from './materiel-releve-production.model';
import { MaterielReleveProductionElement } from './materiel-releve-production.interface';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogAddMaterielReleveProductionComponent } from './popups/dialog-add-materiel-releve-production.component';
import { DialogEditMaterielReleveProductionComponent } from './popups/dialog-edit-materiel-releve-production.component';
@Component({
    selector: 'app-materiels-releves-production-grid',
    templateUrl: 'materiels-releves-production-grid.component.html',
  })
  export class MaterielsRelevesProductionGridComponent implements OnInit, OnChanges {
     private listMaterielsRelevesProduction: MaterielReleveProduction[] = [];
     private  dataMaterielsRelevesProductionSource: MatTableDataSource<MaterielReleveProductionElement>;
     private selection = new SelectionModel<MaterielReleveProductionElement>(true, []);
    
     @Input()
     private releveProduction : ReleveProduction;

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private relevesProductionService: RelevesProductionService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.relevesProductionService.getMaterielsRelevesProductionByCodeReleve(this.releveProduction.code).
      subscribe((listMaterielsRelevesProduction) => {
        this.listMaterielsRelevesProduction = listMaterielsRelevesProduction;
  // console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
  }
  ngOnChanges()
  {
    this.specifyListColumnsToBeDisplayed();
    
    this.relevesProductionService.getMaterielsRelevesProductionByCodeReleve
    (this.releveProduction.code).subscribe((listMaterielsRelevesProduction) => {
      this.listMaterielsRelevesProduction = listMaterielsRelevesProduction;
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }

  checkOneActionInvoked() {
    const listSelectedMaterielsRelevesProduction: MaterielReleveProduction[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((matElement) => {
    return matElement.code;
  });
  this.listMaterielsRelevesProduction.forEach(lj => {
    if (codes.findIndex(code => code === lj.code) > -1) {
      listSelectedMaterielsRelevesProduction.push(lj);
    }
    });
  }
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listMaterielsRelevesProduction!== undefined) {

      this.listMaterielsRelevesProduction.forEach(matRel => {
             listElements.push
             (
               {
            code: matRel.code ,
            numeroReleveProduction: matRel.numeroReleveProduction,
            numeroMaterielReleveProduction: matRel.numeroMaterielReleveProduction,
            reference: matRel.reference,
            marque: matRel.marque,
            matricule: matRel.matricule,
            dureeExploitationHours: matRel.dureeExploitationHours,
            dateAchats: matRel.dateAchats,
            dateFabrication: matRel.dateFabrication,
          description: matRel.description,
          etat: matRel.etat,
        } );
      });
    }

      this.dataMaterielsRelevesProductionSource = new MatTableDataSource<MaterielReleveProductionElement>(listElements);
      this.dataMaterielsRelevesProductionSource.sort = this.sort;
      this.dataMaterielsRelevesProductionSource.paginator = this.paginator;
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [
       {name: 'numeroMaterielReleveProduction' , displayTitle: 'N° Matrériel'},
     {name: 'matricule' , displayTitle: 'Matricule'},
     {name: 'reference' , displayTitle: 'Référence'},
     {name: 'dureeExploitationHours' , displayTitle: 'Durée Exploit.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames = [];
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataMaterielsRelevesProductionSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataMaterielsRelevesProductionSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataMaterielsRelevesProductionSource.filter = filterValue.trim().toLowerCase();
    if (this.dataMaterielsRelevesProductionSource.paginator) {
      this.dataMaterielsRelevesProductionSource.paginator.firstPage();
    }
  }
  showAddMaterielDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeReleveProduction : this.releveProduction.code
   };
 
   const dialogRef = this.dialog.open(DialogAddMaterielReleveProductionComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(lAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditMaterielDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeLigneJournal : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditMaterielReleveProductionComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(lAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Matériel Séléctionnée!');
    }
    }
    supprimerMateriels()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(releveProd, index) {
              this.relevesProductionService.deleteMaterielReleveProduction(releveProd.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar(' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Matériel Séléctionnée!');
        }
    }
    refresh() {
      this.relevesProductionService.getMaterielsRelevesProductionByCodeReleve(this.releveProduction.code).
      subscribe((listMaterielsRel) => {
        this.listMaterielsRelevesProduction = listMaterielsRel;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
