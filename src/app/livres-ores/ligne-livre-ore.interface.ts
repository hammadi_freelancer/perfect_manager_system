


export interface LigneLivreOreElement {
         code: string ;
         numeroLivreOre: string ;
         codeLivreOre: string ;
         dateJournal: string ;
         
         numeroJournal: string;
         montantAchats: number ;
         montantVentes: number;
         montantCharges: number;
         montantRH: number;
         etat: string;
          description?: string;
}
