import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,
  AfterViewInit , AfterContentInit,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {LivreOre} from './livre-ore.model';

import {LivresOresService} from './livres-ores.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { MatDatepickerInput } from '@angular/material/datepicker';
import { MatSnackBar } from '@angular/material';
import { MatDatepicker } from '@angular/material/datepicker';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-livre-ore-edit',
  templateUrl: './livre-ore-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class LivreOreEditComponent implements OnInit  {

  private livreOre: LivreOre =  new LivreOre();
  private tabsDisabled = true;
  private updateEnCours = false;
  private etatsLivreOre: any[] = [
    {value: 'Ouvert', viewValue: 'Ouvert'},
    {value: 'Initial', viewValue: 'Initiale'},
    {value: 'EnCoursValidation', viewValue: 'En Cours de Validation'},
    {value: 'Valide', viewValue: 'Validé'},
    {value: 'Cloture', viewValue: 'Cloturé'},
    {value: 'Annule', viewValue: 'Annulé'}
  ];
  constructor( private route: ActivatedRoute,
    private router: Router, private livresOresService: LivresOresService, private matSnackBar: MatSnackBar,
    public dialog: MatDialog
   ) {
  }
  ngOnInit() {
    const codeLivreOre= this.route.snapshot.paramMap.get('codeLivreOre');
    this.livresOresService.getLivreOre(codeLivreOre).subscribe((livreOre) => {
             this.livreOre = livreOre;
             console.log('the date is ');
          
             if (this.livreOre.etat === 'Initial')
              {
                this.tabsDisabled = true;
                this.etatsLivreOre= [
                  {value: 'Initial', viewValue: 'Initial'},
                  {value: 'Valide', viewValue: 'Validé'},
                  {value: 'Annule', viewValue: 'Annulé'},
                  
                ];
              }
              if (this.livreOre.etat === 'Cloture')
                {
                  this.tabsDisabled = false;
                  this.etatsLivreOre = [
                    {value: 'Cloture', viewValue: 'Cloturé'},
                    {value: 'Annule', viewValue: 'Annulé'},
                    
                  ];
                }
            if (this.livreOre.etat === 'Valide')
              {
                this.tabsDisabled = false;
                this.etatsLivreOre = [
                  {value: 'Annule', viewValue: 'Annulé'},
                  {value: 'Valide', viewValue: 'Validé'},
                  {value: 'Cloture', viewValue: 'Cloturé'},
                  
                ];
              }
              if (this.livreOre.etat === 'Annule')
                {
                  this.etatsLivreOre = [
                    {value: 'Annule', viewValue: 'Annulé'},
                    {value: 'Valide', viewValue: 'Validé'},
                  ];
                  this.tabsDisabled = true;
               
                }
 
    });
  }
 
  loadAddLivreOreComponent() {
    this.router.navigate(['/pms/suivie/ajouterLivreOre']) ;
  }
  loadGridLivresOres() {
    this.router.navigate(['/pms/suivie/livresOres']) ;
  }
  supprimerLivreOre() {
      this.livresOresService.deleteLivreOre(this.livreOre.code).subscribe((response) => {

        this.openSnackBar( 'Livre Ore Supprimé');
        this.loadGridLivresOres() ;
            });
  }
updateLivreOre() {
  this.updateEnCours = true;
  this.livresOresService.updateLivreOre(this.livreOre).subscribe((response) => {
    this.updateEnCours = false;
    if (response.error_message ===  undefined) {

      if (this.livreOre.etat === 'Initial')
        {
          this.tabsDisabled = true;
          this.etatsLivreOre = [
            {value: 'Initial', viewValue: 'Initial'},
            {value: 'Valide', viewValue: 'Validé'},
            {value: 'Annule', viewValue: 'Annulé'},
            
          ];
        }
        if (this.livreOre.etat === 'Cloture')
          {
            this.tabsDisabled = false;
            this.etatsLivreOre = [
              {value: 'Cloture', viewValue: 'Cloturé'},
              {value: 'Annule', viewValue: 'Annulé'},
              
            ];
          }
      if (this.livreOre.etat === 'Valide')
        {
          this.tabsDisabled = false;
          this.etatsLivreOre = [
            {value: 'Annule', viewValue: 'Annulé'},
            {value: 'Valide', viewValue: 'Validé'},
            {value: 'Cloture', viewValue: 'Cloturé'},
            
          ];
        }
        if (this.livreOre.etat === 'Annule')
          {
            this.etatsLivreOre = [
              {value: 'Annule', viewValue: 'Annulé'},
              {value: 'Valide', viewValue: 'Validé'},
            ];
            this.tabsDisabled = true;
         
          }
      this.openSnackBar( ' Livre Ore  Mis à Jour ');
    
    } else {
      this.openSnackBar( ' Erreurs:Opération Echouée ');
    }
});
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

 selectedTabChanged($event) {
  if ($event.index === 4) { // Composition tab was selected
   // this.loadListArticles();
   } else if ($event.index === 1) {
    //  this.loadListSchemasConfiguration() ;
   }
  }
}
