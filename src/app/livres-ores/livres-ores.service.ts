import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';
import { HttpParams } from '@angular/common/http';

import {LivreOre} from './livre-ore.model' ;
// import { TrancheCredit } from './tranche-credit.model';
import { DocumentLivreOre } from './document-livre-ore.model';
import { LigneLivreOre } from './ligne-livre-ore.model';

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_LIVRES_ORES = 'http://localhost:8082/livresOres/getLivresOres';
const URL_GET_LIVRE_ORE = 'http://localhost:8082/livresOres/getLivreOre';
const  URL_ADD_LIVRE_ORE = 'http://localhost:8082/livresOres/addLivreOre';
const  URL_UPDATE_LIVRE_ORE = 'http://localhost:8082/livresOres/updateLivreOre';
const  URL_DELETE_LIVRE_ORE = 'http://localhost:8082/livresOres/deleteLivreOre';
const  URL_GET_TOTAL_LIVRES_ORES = 'http://localhost:8082/livresOres/getTotalLivresOres';



  const  URL_ADD_DOCUMENT_LIVRE_ORE = 'http://localhost:8082/livresOres/documents/addDocumentLivreOre';
  const  URL_GET_LIST_DOCUMENT_LIVRE_ORE = 'http://localhost:8082/livresOres/documents/getDocumentsLivresOres';
  const  URL_DELETE_DOCUCMENT_LIVRE_ORE = 'http://localhost:8082/livresOres/documents/deleteDocumentLivreOre';
  const  URL_UPDATE_DOCUMENT_LIVRE_ORE = 'http://localhost:8082/livresOres/documents/updateDocumentLivreOre';
  const URL_GET_DOCUMENT_LIVRE_ORE = 'http://localhost:8082/livresOres/documents/getDocumentLivreOre';
  const URL_GET_LIST_DOCUMENT_LIVRE_ORE_BY_CODE_LIVRE_ORE =
   'http://localhost:8082/livresOres/documents/getDocumentsLivresOresByCodeLivreOre';


   const  URL_ADD_LIGNE_LIVRE_ORE = 'http://localhost:8082/livresOres/lignes/addLigneLivreOre';
   const  URL_GET_LIST_LIGNE_LIVRE_ORE = 'http://localhost:8082/livresOres/lignes/getLignesLivresOres';
   const  URL_DELETE_LIGNE_LIVRE_ORE = 'http://localhost:8082/livresOres/lignes/deleteLigneLivreOre';
   const  URL_UPDATE_LIGNE_LIVRE_ORE = 'http://localhost:8082/livresOres/lignes/updateLigneLivreOre';
   const URL_GET_LIGNE_LIVRE_ORE = 'http://localhost:8082/livresOres/lignes/getLigneLivreOre';
   const URL_GET_LIST_LIGNE_LIVRE_ORE_BY_CODE_LIVRE_ORE =
    'http://localhost:8082/livresOres/lignes/getLignesLivresOresByCodeLivreOre';

@Injectable({
  providedIn: 'root'
})
export class LivresOresService {

  constructor(private httpClient: HttpClient) {

   }

   getTotalLivresOres(filter): Observable<any> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('filter', filterStr);
        return this.httpClient
        .get<any>(URL_GET_TOTAL_LIVRES_ORES, {params});
       }
   getLivresOres(pageNumber, nbElementsPerPage, filter): Observable<LivreOre[]> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('pageNumber', pageNumber)
    .set('nbElementsPerPage', nbElementsPerPage)
    .set('sort', 'name')
    .set('filter', filterStr);
    return this.httpClient
    .get<LivreOre[]>(URL_GET_LIST_LIVRES_ORES, {params});
   }
   addLivreOre(livreOre: LivreOre): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_LIVRE_ORE, livreOre);
  }
  updateLivreOre(livreOre: LivreOre): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_LIVRE_ORE, livreOre);
  }
  deleteLivreOre(codeLivreOre): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_LIVRE_ORE + '/' + codeLivreOre);
  }
  getLivreOre(codeLivreOre): Observable<LivreOre> {
    return this.httpClient.get<LivreOre>(URL_GET_LIVRE_ORE + '/' + codeLivreOre);
  }


  addDocumentLivreOre(documentLivreOre: DocumentLivreOre): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_DOCUMENT_LIVRE_ORE, documentLivreOre);
    
  }
  updateDocumentLivreOre(docLivreOre: DocumentLivreOre): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_DOCUMENT_LIVRE_ORE, docLivreOre);
  }
  deleteDocumentLivreOre(codeDocumentLivreOre): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_DOCUCMENT_LIVRE_ORE + '/' + codeDocumentLivreOre);
  }
  getDocumentLivreOre(codeDocumentLivreOre): Observable<DocumentLivreOre> {
    return this.httpClient.get<DocumentLivreOre>(URL_GET_DOCUMENT_LIVRE_ORE + '/' + codeDocumentLivreOre);
  }
  getDocumentsLivresOres(): Observable<DocumentLivreOre[]> {
    return this.httpClient
    .get<DocumentLivreOre[]>(URL_GET_LIST_DOCUMENT_LIVRE_ORE);
   }
  getDocumentsLivresOresByCodeLivreOre(codeDocumentLivreOre): Observable<DocumentLivreOre[]> {
    return this.httpClient.get<DocumentLivreOre[]>(URL_GET_LIST_DOCUMENT_LIVRE_ORE_BY_CODE_LIVRE_ORE + '/' + codeDocumentLivreOre);
  }

// Lignes livresOres


addLigneLivreOre(ligneLivreOre: LigneLivreOre): Observable<any> {
  return this.httpClient.post<any>(URL_ADD_LIGNE_LIVRE_ORE, ligneLivreOre);
  
}
updateLigneLivreOre(ligneLivreOre: LigneLivreOre): Observable<any> {
  return this.httpClient.put<any>(URL_UPDATE_LIGNE_LIVRE_ORE, ligneLivreOre);
}
deleteLigneLivreOre(codeLigneLivreOre): Observable<any> {
  return this.httpClient.delete<any>(URL_DELETE_LIGNE_LIVRE_ORE + '/' + codeLigneLivreOre);
}
getLigneLivreOre(codeLigneLivreOre): Observable<LigneLivreOre> {
  return this.httpClient.get<LigneLivreOre>(URL_GET_LIGNE_LIVRE_ORE + '/' + codeLigneLivreOre);
}
getLignesLivresOres(): Observable<LigneLivreOre[]> {
  return this.httpClient
  .get<LigneLivreOre[]>(URL_GET_LIST_LIGNE_LIVRE_ORE_BY_CODE_LIVRE_ORE);
 }
getLignesLivresOresByCodeLivreOre(codeLigneLivreOre): Observable<LigneLivreOre[]> {
  return this.httpClient.get<LigneLivreOre[]>(URL_GET_LIST_LIGNE_LIVRE_ORE_BY_CODE_LIVRE_ORE + '/' + codeLigneLivreOre);
}





}
