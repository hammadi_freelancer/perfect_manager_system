
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {LivresOresService} from '../livres-ores.service';
import {DocumentLivreOre} from '../document-livre-ore.model';
import { DocumentLivreOreElement } from '../document-livre-ore.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-documents-livres-ores',
    templateUrl: 'dialog-list-documents-livres-ores.component.html',
  })
  export class DialogListDocumentsLivresOresComponent implements OnInit {
     private listDocumentsLivresOres: DocumentLivreOre[] = [];
     private  dataDocumentsLivresOresSource: MatTableDataSource<DocumentLivreOreElement>;
     private selection = new SelectionModel<DocumentLivreOreElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private livresOresService: LivresOresService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      this.livresOresService.getDocumentsLivresOresByCodeLivreOre(this.data.codeLivreOre).subscribe((listDocumentsJRS) => {
        this.listDocumentsLivresOres = listDocumentsJRS;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }

  checkOneActionInvoked() {
    const listSelectedDocumentsLivresOres: DocumentLivreOre[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentJelement) => {
    return documentJelement.code;
  });
  this.listDocumentsLivresOres.forEach(documentLivreOre=> {
    if (codes.findIndex(code => code === documentLivreOre.code) > -1) {
      listSelectedDocumentsLivresOres.push(documentLivreOre);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsLivresOres !== undefined) {

      this.listDocumentsLivresOres.forEach(docJ => {
             listElements.push( {code: docJ.code ,
          dateReception: docJ.dateReception,
          numeroDocument: docJ.numeroDocument,
          typeDocument: docJ.typeDocument,
          description: docJ.description
        } );
      });
    }
      this.dataDocumentsLivresOresSource = new MatTableDataSource<DocumentLivreOreElement>(listElements);
      this.dataDocumentsLivresOresSource.sort = this.sort;
      this.dataDocumentsLivresOresSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    //  console.log('calling specifyListColumnsToBeDisplayed documents grid');
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
    
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
      console.log(this.columnsNames);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsLivresOresSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsLivresOresSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsLivresOresSource.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsLivresOresSource.paginator) {
      this.dataDocumentsLivresOresSource.paginator.firstPage();
    }
  }
 




}
