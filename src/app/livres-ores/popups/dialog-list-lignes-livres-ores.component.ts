
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {LivresOresService} from '../livres-ores.service';
import {LigneLivreOre} from '../ligne-livre-ore.model';
import { LigneLivreOreElement } from '../ligne-livre-ore.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-lignes-livres-ores',
    templateUrl: 'dialog-list-lignes-livres-ores.component.html',
  })
  export class DialogListLignesLivresOresComponent implements OnInit {
     private listLignesLivresOres: LigneLivreOre[] = [];
     private  dataLignesLivresOresSource: MatTableDataSource<LigneLivreOreElement>;
     private selection = new SelectionModel<LigneLivreOreElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private livresOresService: LivresOresService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      this.livresOresService.getLignesLivresOresByCodeLivreOre(this.data.codeLivreOre).subscribe((listLignesLivresOres) => {
        this.listLignesLivresOres= listLignesLivresOres;
// console.log('recieving data from backend', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedLignesLivresOres: LigneLivreOre[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((ligneJlement) => {
    return ligneJlement.code;
  });
  this.listLignesLivresOres.forEach(lJour => {
    if (codes.findIndex(code => code === lJour.code) > -1) {
      listSelectedLignesLivresOres.push(lJour);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listLignesLivresOres !== undefined) {

      this.listLignesLivresOres.forEach(lingeLO => {
             listElements.push( {
               code: lingeLO.code ,
               numeroLivreOre: lingeLO.numeroLivreOre,
               codeLivreOre: lingeLO.codeLivreOre,
               numeroJournal: lingeLO.numeroJournal,
               dateJournal: lingeLO.dateJournal,
               montantAchats: lingeLO.montantAchats,
               montantVentes: lingeLO.montantVentes,
               montantCharges: lingeLO.montantCharges,
               montantRH: lingeLO.montantRH,
          description: lingeLO.description,
          etat: lingeLO.etat,
        } );
      });
    }
      this.dataLignesLivresOresSource = new MatTableDataSource<LigneLivreOreElement>(listElements);
      this.dataLignesLivresOresSource.sort = this.sort;
      this.dataLignesLivresOresSource.paginator = this.paginator;
  }
  specifyListColumnsToBeDisplayed() {
    this.columnsDefinitions = [
     {name: 'code' , displayTitle: 'N°Ligne'},
     {name: 'numeroJournal' , displayTitle: 'N°Journal'},
     {name: 'dateJournal' , displayTitle: 'Date Journal'},
     
    {name: 'montantAchats' , displayTitle: 'Montant Achats (en Dinars)'},
    {name: 'montantVentes' , displayTitle: 'Montant Ventes (en Dinars)'},
    {name: 'montantCharges' , displayTitle: 'Montant Charges (en Dinars)'},
    {name: 'montantRH' , displayTitle: 'Montant RH ( en Dinars)'},
    { name : 'etat' , displayTitle : 'Etat'},
      { name : 'description' , displayTitle : 'Déscription'},
     ];

    this.columnsTitles = this.columnsDefinitions.map((colDef) => {
              return colDef.displayTitle;
    });
    this.columnsNames[0] = 'select';
    this.columnsDefinitions.map((colDef) => {
     this.columnsNames.push(colDef.name);
 });
    this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
     return colDef.displayTitleAr;
 });
 }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataLignesLivresOresSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataLignesLivresOresSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataLignesLivresOresSource.filter = filterValue.trim().toLowerCase();
    if (this.dataLignesLivresOresSource.paginator) {
      this.dataLignesLivresOresSource.paginator.firstPage();
    }
  }

}
