
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {LivresOresService} from '../livres-ores.service';
import {LigneLivreOre} from '../ligne-livre-ore.model';

@Component({
    selector: 'app-dialog-edit-ligne-livre-ore',
    templateUrl: 'dialog-edit-ligne-livre-ore.component.html',
  })
  export class DialogEditLigneLivreOreComponent implements OnInit {
     private ligneLivreOre: LigneLivreOre;
     private updateEnCours = false;
    private etats: any[] = [
      {value: 'Annule', viewValue: 'Annulé'},
      {value: 'Initial', viewValue: 'Initial'},
      {value: 'Valide', viewValue: 'Validé'}
    ];

   
     constructor(  private livresOresService: LivresOresService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit() {

        this.livresOresService.getLigneLivreOre(this.data.codeLigneLivreOre).subscribe((ligneLivreOre) => {
            this.ligneLivreOre = ligneLivreOre;
            if (this.ligneLivreOre.etat === 'Valide' || this.ligneLivreOre.etat === 'Annule') {
                this.etats.splice(1, 1);
              }
              if (this.ligneLivreOre.etat === 'Initial') {
                  this.etats.splice(0, 1);
                }
     });
    }
    updateLigneLivreOre()  {
      
      this.updateEnCours = true;
       this.livresOresService.updateLigneLivreOre(this.ligneLivreOre).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
          if (this.ligneLivreOre.etat === 'Valide' || this.ligneLivreOre.etat === 'Annule' ) {
             //  this.tabsDisabled = false;
              this.etats = [
                {value: 'Annule', viewValue: 'Annulé'},
                {value: 'Valide', viewValue: 'Validé'}
      
              ];
            }
         
             // this.save.emit(response);
             this.openSnackBar(  'Ligne Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }


  }
