
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {LivresOresService} from '../livres-ores.service';
import {LigneLivreOre} from '../ligne-livre-ore.model';

@Component({
    selector: 'app-dialog-add-ligne-livre-ore',
    templateUrl: 'dialog-add-ligne-livre-ore.component.html',
  })
  export class DialogAddLigneLivreOreComponent implements OnInit {
     private ligneLivreOre: LigneLivreOre;
     private saveEnCours = false;
     private etats: any[] = [
      {value: 'Initial', viewValue: 'Initiale'},
      {value: 'Valide', viewValue: 'Validé'},
      {value: 'Annule', viewValue: 'Annulé'}
        ];
    /* private typesOperations: any[] = [
      {value: 'AchatsMatieresPrimaires', viewValue: 'Achats Matières Primaires'},
      {value: 'AchatsMarchandises', viewValue: 'Achats Marchandises'},
      {value: 'AchatsFournituresBureau', viewValue: 'Achats Fournitures Bureau'},
      {value: 'AchatsMaterielles', viewValue: 'Achats Materielles'},
      {value: 'AutresAchats', viewValue: 'Autres Achats'},
      {value: 'Electricite', viewValue: 'Electricité'},
      {value: 'EauPotable', viewValue: 'Eau Potable'},
      {value: 'ConsommationTelephonique', viewValue: 'Consommation Téléphonique'},
      {value: 'Transport', viewValue: 'Transport'},
      {value: 'Location', viewValue: 'Locations'},
      {value: 'Impots', viewValue: 'Impots'},
      {value: 'AutresCharges', viewValue: 'Autres Charges'},
      {value: 'VentesMarchandises', viewValue: 'Ventes Marchandises'},
      {value: 'VentesProduitsFinis', viewValue: 'Ventes Produits Finis'},
      {value: 'VentesMatieresPrimaires', viewValue: 'Ventes Matieres Primaires'},
      {value: 'VentesProduitsSemiFinis', viewValue: 'Ventes Produits Semi Finis'},
      {value: 'VentesMateriellesEpuisées', viewValue: 'Ventes Materielles Epuisées'},
      {value: 'VentesLocaux', viewValue: 'Ventes Locaux'},
      {value: 'VentesProduitsNonConformes', viewValue: 'Ventes Produits Non Conformes'},
      {value: 'AutresVentes', viewValue: 'Autres Ventes'},

      {value: 'PaieSalaires', viewValue: 'Paie Salaires'},
      {value: 'PaieHoraires', viewValue: 'Paie Horaires'},
      {value: 'PaieCNSS', viewValue: 'Paie CNSS'},
      {value: 'PaiePrimes', viewValue: 'Paie Primes'},
      {value: 'PaieAvances', viewValue: 'Paie Avances'},
      {value: 'PaieRecompenses', viewValue: 'Paie Récompenses'},
      {value: 'PaieRecrutements', viewValue: 'Paie Recrutements'},
      {value: 'AutresPaies', viewValue: 'Autres Paies'},
    ];*/
   




     constructor(  private livresOresService: LivresOresService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.ligneLivreOre = new LigneLivreOre();
    }
    saveLigneLivreOre() 
    {
      this.ligneLivreOre.codeLivreOre = this.data.codeLivreOre;
       this.saveEnCours = true;
       this.livresOresService.addLigneLivreOre(this.ligneLivreOre).subscribe((response) => {
         this.saveEnCours = false;
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.ligneLivreOre = new LigneLivreOre();
             this.openSnackBar(  'Ligne Enregistrée');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
 
  }
