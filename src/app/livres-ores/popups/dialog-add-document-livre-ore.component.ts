
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {LivresOresService} from '../livres-ores.service';
import {DocumentLivreOre} from '../document-livre-ore.model';

@Component({
    selector: 'app-dialog-add-document-livre-ore',
    templateUrl: 'dialog-add-document-livre-ore.component.html',
  })
  export class DialogAddDocumentLivreOreComponent implements OnInit {
     private documentLivreOre: DocumentLivreOre;
     private saveEnCours = false;
     private typesDoc: any[] = [
      {value: 'FactureVentes', viewValue: 'Facture Ventes'},
      {value: 'FactureAchats', viewValue: 'Facture Achats'},
      {value: 'FactureTelephonique', viewValue: 'Facture Téléphonique'},
      {value: 'FactureElectricite', viewValue: 'Facture Eléctricité'},
      {value: 'FactureEauPotable', viewValue: 'Facture Eau Potable'},
      {value: 'FichePaie', viewValue: 'Fiche Paie'},
      {value: 'BonLivraison', viewValue: 'Bon Livraison'},
      {value: 'BonReception', viewValue: 'Bon Récéption'},
      {value: 'RecuPayement', viewValue: 'Reçu Payement'},
      {value: 'Cheque', viewValue: 'Chèque'},
      {value: 'Traite', viewValue: 'Traite'},
      {value: 'Autre', viewValue: 'Autre'},
    ];
     constructor(  private livresOresService: LivresOresService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.documentLivreOre = new DocumentLivreOre();
    }
    saveDocumentLivreOre() 
    {
      this.documentLivreOre.codeLivreOre= this.data.codeLivreOre;
        this.saveEnCours = true;
       this.livresOresService.addDocumentLivreOre(this.documentLivreOre).subscribe((response) => {
         this.saveEnCours = false;
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.documentLivreOre = new DocumentLivreOre();
             this.openSnackBar(  'Document Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs: Opération Echouée');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
     imageFace1Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentLivreOre.imageFace1 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imageFace2Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentLivreOre.imageFace2 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
  }
