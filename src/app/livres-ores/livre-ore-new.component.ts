import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {LivreOre} from './livre-ore.model';

import {LivresOresService} from './livres-ores.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {MatSnackBar} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';


@Component({
  selector: 'app-livre-ore-new',
  templateUrl: './livre-ore-new.component.html',
  // styleUrls: ['./players.component.css']
})
export class LivreOreNewComponent implements OnInit {

 private livreOre: LivreOre =  new LivreOre();
 private saveEnCours = false;
private etatsLivreOre: any[] = [
  {value: 'Ouvert', viewValue: 'Ouvert'},
  {value: 'Initial', viewValue: 'Initiale'},
  {value: 'EnCoursValidation', viewValue: 'En Cours de Validation'},
  {value: 'Valide', viewValue: 'Validé'},
  {value: 'Cloture', viewValue: 'Cloturé'}
];
  constructor( private route: ActivatedRoute,
    private router: Router, private livresOresService: LivresOresService,
   private matSnackBar: MatSnackBar) {
  }

  ngOnInit() {
  
  }
  saveLivreOre() {

    if( this.livreOre.etat === undefined || this.livreOre.etat === 'Ouvert' ) {
      this.livreOre.etat = 'Initial';
    }
    // this.saveEnCours = true;
    this.saveEnCours = true;
    this.livresOresService.addLivreOre(this.livreOre).subscribe((response) => {
      this.saveEnCours = false;
      
        if (response.error_message ===  undefined) {
          console.log('undefined error message');
          // this.save.emit(response);
          this.livreOre =  new LivreOre();
          this.openSnackBar(  'LivreOre Enregistré');
        } else {
         // show error message
         this.openSnackBar(  'Erreurs: Opération Echouée');
         
        }
    });
  }


  vider() {
    this.livreOre = new LivreOre();
  }
  openSnackBar(messageToDisplay) {
   this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
       verticalPosition: 'top'});
  }
loadGridLivresOres() {
  this.router.navigate(['/pms/suivie/livresOres']) ;
}

}
