import { Component, OnInit, Output,ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {LivreOre} from './livre-ore.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {LivresOresService} from './livres-ores.service';
import { LivreOreElement } from './livre-ore.interface';
import { LivreOreFilter } from './livre-ore.filter';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {SelectItem} from 'primeng/api';

// import { DialogAddDocumentCreditComponent } from './dialog-add-document-credit.component';
@Component({
  selector: 'app-livres-ores-grid',
  templateUrl: './livres-ores-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class LivresOresGridComponent implements OnInit {

  private livreOre: LivreOre =  new LivreOre();
  private livreOreFilter: LivreOreFilter = new LivreOreFilter();
  
  private selection = new SelectionModel<LivreOreElement>(true, []);
  private columnsToSelect : SelectItem[];
   private  selectedColumnsDefinitions: string[];
  private selectedColumnsNames: string[];
  
@Output()
select: EventEmitter<LivreOre[]> = new EventEmitter<LivreOre[]>();

//private lastClientAdded: Client;

 private selectedLivreOre: LivreOre ;
private   showFilter = false;
private showSelectColumnsMultiSelect = false;

 @Input()
 private listLivresOres: LivreOre[] = [] ;

 private deleteEnCours = false;
 private checkedAll: false;
 private  dataLivresOresSource: MatTableDataSource<LivreOreElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay :ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
 private etatsLivreOre: any[] = [
  {value: 'Ouvert', viewValue: 'Ouvert'},
  {value: 'Initial', viewValue: 'Initiale'},
  {value: 'EnCoursValidation', viewValue: 'En Cours de Validation'},
  {value: 'Valide', viewValue: 'Validé'},
  {value: 'Cloture', viewValue: 'Cloturé'}
];
  constructor( private route: ActivatedRoute, private livresOresService: LivresOresService,
    private router: Router, private matSnackBar: MatSnackBar ) {
  }
  ngOnInit() {
    this.livresOresService.getLivresOres(0, 5, null).subscribe((livresOres) => {
      this.listLivresOres = livresOres;
      // this.listClients[this.listClients.length] = this.lastClientAdded;
      // this.listClientsOrgin = this.listClients ;
      this.defaultColumnsDefinitions = [{name: 'numeroLivreOre' , displayTitle: 'N°Livre Ore'},
      {name: 'dateLivreOre' , displayTitle: 'Date'},
      {name: 'periodeFrom' , displayTitle: 'Période de:'},
      {name: 'periodeTo' , displayTitle: 'à'},
       {name: 'montantVentes' , displayTitle: 'Valeur Ventes'},
       {name: 'montantAchats' , displayTitle: 'Valeur Achats'},
       {name: 'montantCharges' , displayTitle: 'Valeur Charges'},
       {name: 'montantRH' , displayTitle: 'Valeur R.Humaines'},
       { name : 'etat' , displayTitle : 'Etat'},
       { name : 'description' , displayTitle : 'Déscription'},
   
        ];
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  /*  this.columnsToSelect = [
      {label: 'N°Journal', value: {id: 1, name: 'numeroJournal', displayTitle: 'N°Journal'}},
      {label: 'Date Journal', value: {id: 2, name: 'dateJournal', displayTitle: 'Date Journal'}},
      {label: 'Valeur Ventes', value: {id: 3, name: 'montantVentes', displayTitle: 'Valeur Ventes'}},
      {label: 'Valeur Achats', value: {id: 4, name: 'montantAchats', displayTitle: 'Valeur Achats'}},
      {label: 'Valeur R.Humaines', value: {id: 5, name: 'montantRH', displayTitle: 'Valeur R.Humaines'}},
      {label: 'Valeur Charges', value: {id: 6, name: 'montantCharges', displayTitle: 'Valeur Charges'} },
      {label: 'Déscription', value: {id: 7, name: 'description', displayTitle: 'Déscription'} },
      {label: 'Etat', value: {id: 8, name: 'etat', displayTitle: 'Etat'} },
      

  ];*/

      this.columnsToSelect = [
      {label: 'N°Livre Ore', value: 'numeroLivreOre', title: 'N°Livre Ore'},
      {label: 'Date', value: 'dateLivreOre', title: 'Date'},
      {label: 'Période de', value: 'periodeFrom', title: 'Période de:'},
      {label: 'Période à', value: 'periodeTo', title: 'Période à:'},
      {label: 'Valeur Ventes', value: 'montantVentes', title: 'Valeur Ventes'},
      {label: 'Valeur Achats', value: 'montantAchats', title: 'Valeur Achats'},
      {label: 'Valeur R.Humaines', value: 'montantRH', title: 'Valeur R.Humaines'},
      {label: 'Valeur Charges', value:  'montantCharges', title: 'Valeur Charges' },
      {label: 'Déscription', value: 'description', title: 'Déscription' },
      {label: 'Etat', value:  'etat', title: 'Etat' },
      

  ];
  });
   /* this.creditsService.getCredits().subscribe((credits) => {
      console.log('list credits ', credits);
      // this.listCredits = credits;
      this.listCredits =  this.listCredits.map((credit) => {
        if (credit.client === 'undefined' || credit.client === null ) {
           credit.client = new Client();
         }
         return credit;
   });      this.listCreditsOrgin = this.listCredits ;
     });*/
    // this.listClientsOrgin = this.listClients ;

  }
  deleteLivreOre(livreOre) {
     //  console.log('call delete !', credit );
    this.livresOresService.deleteLivreOre(livreOre.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData( null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.livresOresService.getLivresOres(0, 5 , filter).subscribe((livresOres) => {
    this.listLivresOres = livresOres;
    this.transformDataToByConsumedByMatTable();
   });
  // this.listClientsOrgin = this.listClients ;
}


checkOneActionInvoked() {
  const listSelectedLivresOres: LivreOre[] = [];
console.log('selected clients are ');
console.log(this.selection.selected);
if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((livreOreElement) => {
  return livreOreElement.code;
});
this.listLivresOres.forEach(livreOre => {
  if (codes.findIndex(code => code === livreOre.code) > -1) {
    listSelectedLivresOres.push(livreOre);
  }
  });
}
this.select.emit(listSelectedLivresOres);

}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  this.dataLivresOresSource = null;
  if (this.listLivresOres !== undefined) {
   //  console.log('diffe of undefined');
    this.listLivresOres.forEach(livreOre => {
           listElements.push(
             {code: livreOre.code ,
            numeroLivreOre: livreOre.numeroLivreOre,
            dateLivreOre: livreOre.dateLivreOre ,
            periodeFrom: livreOre.periodeFrom ,
            periodeTo: livreOre.periodeTo ,
            montantVentes: livreOre.montantVentes,
            montantAchats: livreOre.montantAchats,
            montantRH: livreOre.montantRH,
            montantCharges: livreOre.montantCharges,
            description: livreOre.description,
   etat: livreOre.etat === 'Valide' ?  'Validé' : livreOre.etat === 'Annule' ? 'Annulé' :
   livreOre.etat === 'EnCoursValidation' ?  'En Cours De Validation' :   livreOre.etat === 'Cloture' ? 'Cloturé' :
   livreOre.etat === 'Initial' ? 'Initiale' : 'Ouvert'

      } );
    });
  }
    this.dataLivresOresSource = new MatTableDataSource<LivreOreElement>(listElements);
    this.dataLivresOresSource.sort = this.sort;
    this.dataLivresOresSource.paginator = this.paginator;
    this.livresOresService.getTotalLivresOres(null).subscribe((response) => {
    //  console.log('Total credits is ', response);
      this.dataLivresOresSource.paginator.length = response.totalLivresOres;
    });
}
 specifyListColumnsToBeDisplayed() {
 
  console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
  this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
?
   this.defaultColumnsDefinitions : [];
   this.selectedColumnsNames = [];
   
   if (this.selectedColumnsDefinitions !== undefined)
    {
      this.selectedColumnsDefinitions.forEach((colName) => { 
           const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
           const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
           const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
           this.columnsDefinitionsToDisplay.push(colDef);
          });
    }
   this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
  this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
             return colDef.displayTitle;
   });
   this.columnsNames = [];
   this.columnsNames[0] = 'select';
   this.columnsDefinitionsToDisplay .map((colDef) => {
    this.columnsNames.push(colDef.name);
   if (this.selectedColumnsDefinitions !== undefined) {
    this.selectedColumnsNames.push(colDef.displayTitle);
   }
});
   this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
    return colDef.displayTitleAr;
});
}
masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataLivresOresSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataLivresOresSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataLivresOresSource.filter = filterValue.trim().toLowerCase();
  if (this.dataLivresOresSource.paginator) {
    this.dataLivresOresSource.paginator.firstPage();
  }
}

loadAddLivreOreComponent() {
  this.router.navigate(['/pms/suivie/ajouterLivreOre']) ;

}
loadEditLivreOreComponent() {
  this.router.navigate(['/pms/suivie/editerLivreOre', this.selection.selected[ this.selection.selected.length - 1].code]) ;

}
supprimerLivreOre()
{
 
  if ( this.selection.selected.length !== 0) {
    this.deleteEnCours = true;
      this.selection.selected.forEach(function(livreOre, index) {
          this.livresOresService.deleteLivreOre(livreOre.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.deleteEnCours = false;
            
            this.openSnackBar( ' Element(s) Supprimé(s)');
             this.loadData(null);
            }
      }, this);
    } else {
      this.openSnackBar('Aucun Livre Ore Séléctionné !');
    }
}
refresh() {
  this.loadData(null);
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 
 toggleFilterPanel()
 {
    this.showFilter = !this.showFilter;
 }
 toggleShowColumnsMultiSelect()
 {
   this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
 }
 loadFiltredData()
 {
   console.log('the filter is ', this.livreOreFilter);
   this.loadData(this.livreOreFilter);
 }

changePage($event) {
  console.log('page event is ', $event);
 this.livresOresService.getLivresOres($event.pageIndex, $event.pageSize, this.livreOreFilter).subscribe((livresOres) => {
    this.listLivresOres = livresOres;
   // this.transformDataToByConsumedByMatTable();
   const listElements = [];
     console.log('diffe of undefined');
     this.listLivresOres.forEach(livreOre => {
      listElements.push( {
        code: livreOre.code ,
        numeroLivreOre: livreOre.numeroLivreOre,
        dateLivreOre: livreOre.dateLivreOre ,
        periodeFrom: livreOre.periodeFrom ,
        periodeTo: livreOre.periodeTo ,
        montantVentes: livreOre.montantVentes,
        montantAchats: livreOre.montantAchats,
        montantRH: livreOre.montantRH,
        montantCharges: livreOre.montantCharges,
        description: livreOre.description,
etat: livreOre.etat === 'Valide' ?  'Validé' : livreOre.etat === 'Annule' ? 'Annulé' :
livreOre.etat === 'EnCoursValidation' ?  'En Cours De Validation' :   livreOre.etat === 'Cloture' ? 'Cloturé' :
livreOre.etat === 'Initial' ? 'Initiale' : 'Ouvert'

      }
);

});
this.dataLivresOresSource = new MatTableDataSource<LivreOreElement>(listElements);
this.livresOresService.getTotalLivresOres(null).subscribe((response) => {
 console.log('Total credits is ', response);
  this.dataLivresOresSource.paginator.length = response.totalLivresOres;
});
});
}
}
