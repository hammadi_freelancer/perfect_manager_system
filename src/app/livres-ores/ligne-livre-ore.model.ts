
import { BaseEntity } from '../common/base-entity.model' ;

type TypeOperation =   'AchatsMatieresPrimaires' | 'AchatsMarchandises' |
'AchatsFournituresBureau' |'AchatsMaterielles' | 'AutresAchats'

| 'Electricite' |'EauPotable' | 'ConsommationTelephonique'
| 'Transport' |  'Location' | 'Impots' | 'AutresCharges' |

'VentesMarchandises' | 'VentesProduitsFinis'
| 'VentesMatieresPrimaires' |  'VentesProduitsSemiFinis' | 'VentesMateriellesEpuisées'
|'VentesLocaux' | 'VentesProduitsNonConformes' | 'AutresVentes' |

'PaieSalaires' | 'PaieHoraires' | 'PaieCNSS' | 'PaiePrimes' |'PaieAvances' |
'PaieRecompenses' |'PaieRecrutement' | 'AutresPaies'
 ;
 type Etat = 'Initial' | 'Valide' | 'Annule'
 
 type ModePaiement = 'Espece' | 'Cheque' | 'Virement' | 'Credit'
 
export class LigneLivreOre extends BaseEntity {
    constructor(
        public code?: string,
        public numeroLivreOre?: string,
        public codeLivreOre?: string,
        public dateJournalObject?: Date,
        public dateJournal?: string,
         public numeroJournal?: string,
         public montantAchats?: number,
         public montantVentes?: number,
         public montantRH?: number,
         public montantCharges?: number,
          public etat?: Etat,
     ) {
         super();
        
       }
  
}
