
import { BaseEntity } from '../common/base-entity.model' ;

type Etat = 'Initial' | 'Valide' | 'Annule' | 'EnCoursValidation' | 'Ouvert' | 'Cloture'

export class LivreOre extends BaseEntity {
    constructor(
        public code?: string,
        public numeroLivreOre?: string,
        public dateLivreOreObject?: Date,
        public dateLivreOre?: string,
        
        public periodeFromObject?: Date,
        public periodeFrom?: string,
        
        public periodeToObject?: Date,
        public periodeTo?: string,
        
         public montantVentes?: number,
         public montantAchats?: number,
         public montantRH?: number,
         public montantCharges?: number,
         public etat?: Etat,
    // public listTranches?: Tranche[]
     ) {
         super();
      
    }
 
}
