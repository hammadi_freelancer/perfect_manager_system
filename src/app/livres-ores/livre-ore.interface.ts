export interface LivreOreElement {
         code: string ;
         numeroLivreOre: string;
         dateLivreOre: string ;
         periodeFrom: string;
         periodeTo: string;
         montantVentes: number;
         montantAchats: number;
         montantRH: number;
         montantCharges: number;
         etat: string;
         description: string;
        }
