import { Injectable } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { LivreOreNewComponent } from './livre-ore-new.component';
import { LivreOreEditComponent } from './livre-ore-edit.component';
import { LivresOresGridComponent } from './livres-ores-grid.component';
import { HomeLivresOresComponent } from './home-livres-ores.component' ;


import {ModuleWithProviders  } from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';

 export const livresOresRoute: Routes = [
    {
        path: 'livresOres',
        component: LivresOresGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterLivreOre',
        component: LivreOreNewComponent,
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerLivreOre/:codeLivreOre',
        component: LivreOreEditComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
















];
// export const ventesRoutingModule: ModuleWithProviders = RouterModule.forRoot(ventesRoute);
// commonRouting.providers = [{ provide: APP_BASE_HREF, useValue: '/common'} ] ;


