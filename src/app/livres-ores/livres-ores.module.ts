import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
 import { AppComponent } from '../app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ReactiveFormsModule } from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatMenuModule} from '@angular/material/menu';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {TableModule} from 'primeng/table';
import {MatSelectModule} from '@angular/material/select';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTabsModule} from '@angular/material/tabs';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MultiSelectModule} from 'primeng/multiselect';

import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import { RouterModule, provideRoutes} from '@angular/router';



import { CommonModule } from '../common/common.module';
import { SharedComponentModule } from '../shared-components/shared-components.module';



// journaux components
import { LivreOreNewComponent } from './livre-ore-new.component';
import { LivreOreEditComponent } from './livre-ore-edit.component';
import { LivresOresGridComponent } from './livres-ores-grid.component';

import { DialogAddDocumentLivreOreComponent } from './popups/dialog-add-document-livre-ore.component';
import { DialogEditDocumentLivreOreComponent } from './popups/dialog-edit-document-livre-ore.component';
import { DialogListDocumentsLivresOresComponent } from './popups/dialog-list-documents-livres-ores.component';

import { DialogAddLigneLivreOreComponent } from './popups/dialog-add-ligne-livre-ore.component';
import { DialogEditLigneLivreOreComponent } from './popups/dialog-edit-ligne-livre-ore.component';
import { DialogListLignesLivresOresComponent } from './popups/dialog-list-lignes-livres-ores.component';

import { DocumentsLivresOresGridComponent } from './documents-livres-ores-grid.component';
import { LignesLivresOresGridComponent } from './lignes-livres-ores-grid.component';

import {MatSidenavModule} from '@angular/material/sidenav';
import {MatRadioModule} from '@angular/material/radio';



import {FormsModule } from '@angular/forms';
 import {APP_BASE_HREF} from '@angular/common';
 import { HomeLivresOresComponent } from './home-livres-ores.component' ;
/*const ENTITY_STATES = [
  ...commonRoute
];*/

 // const BASE_URL = [{provide: APP_BASE_HREF, useValue: '/common'}];
@NgModule({
  declarations: [
    LivreOreEditComponent,
    LivreOreNewComponent,
    LivresOresGridComponent,
    LignesLivresOresGridComponent,
    DocumentsLivresOresGridComponent,
    DialogListLignesLivresOresComponent,
    DialogEditLigneLivreOreComponent,
    DialogAddLigneLivreOreComponent,
    DialogListDocumentsLivresOresComponent,
    DialogEditDocumentLivreOreComponent,
    DialogAddDocumentLivreOreComponent,
    HomeLivresOresComponent,

  ],
  imports: [
    BrowserModule, HttpClientModule, MatDatepickerModule, MatNativeDateModule,
     BrowserAnimationsModule, ReactiveFormsModule, MatAutocompleteModule,
    MatInputModule, MatCardModule, MatCheckboxModule, MatButtonModule, MatIconModule,
    MatMenuModule, MatProgressSpinnerModule, MatExpansionModule, MatSortModule, MatSelectModule, MatPaginatorModule, MatTableModule,
    FormsModule, SharedComponentModule, MatTabsModule, MatProgressBarModule,
    MultiSelectModule,
    // ventesRoutingModule,
    CommonModule, TableModule , MatDialogModule,
     MatSidenavModule, MatRadioModule
],
  providers: [MatNativeDateModule ,
  ],
  exports : [
    RouterModule,
    HomeLivresOresComponent

  ],
  entryComponents : [
    DialogListLignesLivresOresComponent,
    DialogEditLigneLivreOreComponent,
    DialogAddLigneLivreOreComponent,
    DialogListDocumentsLivresOresComponent,
    DialogEditDocumentLivreOreComponent,
    DialogAddDocumentLivreOreComponent,


  ],
  bootstrap: [AppComponent]
})
export class LivresOresModule { }
