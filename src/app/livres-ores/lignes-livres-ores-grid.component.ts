
import { Component, OnInit, Inject,Input, ViewChild, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {LivresOresService} from './livres-ores.service';
import {LivreOre} from './livre-ore.model';
import {LigneLivreOre} from './ligne-livre-ore.model';
import { LigneLivreOreElement } from './ligne-livre-ore.interface';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogAddLigneLivreOreComponent } from './popups/dialog-add-ligne-livre-ore.component';
import { DialogEditLigneLivreOreComponent } from './popups/dialog-edit-ligne-livre-ore.component';
@Component({
    selector: 'app-lignes-livres-ores-grid',
    templateUrl: 'lignes-livres-ores-grid.component.html',
  })
  export class LignesLivresOresGridComponent implements OnInit, OnChanges {
     private listLignesLivresOres: LigneLivreOre[] = [];
     private  dataLignesLivresOresSource: MatTableDataSource<LigneLivreOreElement>;
     private selection = new SelectionModel<LigneLivreOreElement>(true, []);
    
     @Input()
     private livreOre : LivreOre;

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private livresOresService: LivresOresService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.livresOresService.getLignesLivresOresByCodeLivreOre(this.livreOre.code).subscribe((listLignesLV) => {
        this.listLignesLivresOres= listLignesLV;
  // console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
  }
  ngOnChanges()
  {
    this.specifyListColumnsToBeDisplayed();
    
    this.livresOresService.getLignesLivresOresByCodeLivreOre(this.livreOre.code).subscribe((listLignesLV) => {
      this.listLignesLivresOres = listLignesLV;
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }

  checkOneActionInvoked() {
    const listSelectedLignesLivreOres: LigneLivreOre[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((ligneLOlement) => {
    return ligneLOlement.code;
  });
  this.listLignesLivresOres.forEach(lO => {
    if (codes.findIndex(code => code === lO.code) > -1) {
      listSelectedLignesLivreOres.push(lO);
    }
    });
  }
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listLignesLivresOres !== undefined) {

      this.listLignesLivresOres.forEach(lingeLO => {
             listElements.push( {
               code: lingeLO.code ,
               numeroLivreOre: lingeLO.numeroLivreOre,
               codeLivreOre: lingeLO.codeLivreOre,
               numeroJournal: lingeLO.numeroJournal,
               dateJournal: lingeLO.dateJournal,
               montantAchats: lingeLO.montantAchats,
               montantVentes: lingeLO.montantVentes,
               montantCharges: lingeLO.montantCharges,
               montantRH: lingeLO.montantRH,
          description: lingeLO.description,
          etat: lingeLO.etat,
        } );
      });
    }
      this.dataLignesLivresOresSource = new MatTableDataSource<LigneLivreOreElement>(listElements);
      this.dataLignesLivresOresSource.sort = this.sort;
      this.dataLignesLivresOresSource.paginator = this.paginator;
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [
      {name: 'code' , displayTitle: 'N°Ligne'},
      {name: 'numeroJournal' , displayTitle: 'N°Journal'},
      {name: 'dateJournal' , displayTitle: 'Date Journal'},
      
     {name: 'montantAchats' , displayTitle: 'Montant Achats (en Dinars)'},
     {name: 'montantVentes' , displayTitle: 'Montant Ventes (en Dinars)'},
     {name: 'montantCharges' , displayTitle: 'Montant Charges (en Dinars)'},
     {name: 'montantRH' , displayTitle: 'Montant RH ( en Dinars)'},
     { name : 'etat' , displayTitle : 'Etat'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataLignesLivresOresSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataLignesLivresOresSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataLignesLivresOresSource.filter = filterValue.trim().toLowerCase();
    if (this.dataLignesLivresOresSource.paginator) {
      this.dataLignesLivresOresSource.paginator.firstPage();
    }
  }
  showAddLigneDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeLivreOre : this.livreOre.code
   };
 
   const dialogRef = this.dialog.open(DialogAddLigneLivreOreComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(lAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditLigneDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeLigneLivreOre : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditLigneLivreOreComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(lAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucune Ligne Séléctionnée!');
    }
    }
    supprimerLignes()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(llivOre, index) {
              this.livresOresService.deleteLigneLivreOre(llivOre.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar(' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucune Ligne Séléctionnée!');
        }
    }
    refresh() {
      this.livresOresService.getLignesLivresOresByCodeLivreOre(this.livreOre.code).subscribe((listLignes) => {
        this.listLignesLivresOres = listLignes;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
