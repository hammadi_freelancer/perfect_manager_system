
import { Component, OnInit, Inject,Input, ViewChild, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {LivresOresService} from './livres-ores.service';
import {LivreOre} from './livre-ore.model';

import {DocumentLivreOre} from './document-livre-ore.model';
import { DocumentLivreOreElement } from './document-livre-ore.interface';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogAddDocumentLivreOreComponent } from './popups/dialog-add-document-livre-ore.component';
import { DialogEditDocumentLivreOreComponent } from './popups/dialog-edit-document-livre-ore.component';
@Component({
    selector: 'app-documents-livres-ores-grid',
    templateUrl: 'documents-livres-ores-grid.component.html',
  })
  export class DocumentsLivresOresGridComponent implements OnInit, OnChanges {
     private listDocumentsLivresOres: DocumentLivreOre[] = [];
     private  dataDocumentsLivresOres: MatTableDataSource<DocumentLivreOreElement>;
     private selection = new SelectionModel<DocumentLivreOreElement>(true, []);
    
     @Input()
     private livreOre : LivreOre 

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private livresOresService: LivresOresService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.livresOresService.getDocumentsLivresOresByCodeLivreOre(this.livreOre.code).subscribe((listDocumentsLivresOres) => {
        this.listDocumentsLivresOres = listDocumentsLivresOres;
  // console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
  }
  ngOnChanges()
  {
    this.specifyListColumnsToBeDisplayed();
    
    this.livresOresService.getDocumentsLivresOresByCodeLivreOre(this.livreOre.code).subscribe((listDocumentsLivresOres) => {
      this.listDocumentsLivresOres = listDocumentsLivresOres;
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }

  checkOneActionInvoked() {
    const listSelectedDocumentsLivresOres: DocumentLivreOre[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentLelement) => {
    return documentLelement.code;
  });
  this.listDocumentsLivresOres.forEach(documentLO => {
    if (codes.findIndex(code => code === documentLO.code) > -1) {
      listSelectedDocumentsLivresOres.push(documentLO);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsLivresOres !== undefined) {

      this.listDocumentsLivresOres.forEach(docL => {
             listElements.push( {code: docL.code ,
          dateReception: docL.dateReception,
          numeroDocument: docL.numeroDocument,
          typeDocument: docL.typeDocument,
          description: docL
          .description
        } );
      });
    }
      this.dataDocumentsLivresOres = new MatTableDataSource<DocumentLivreOreElement>(listElements);
      this.dataDocumentsLivresOres.sort = this.sort;
      this.dataDocumentsLivresOres.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    //  console.log('calling specifyListColumnsToBeDisplayed documents grid');
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
    
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'},
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
      console.log(this.columnsNames);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsLivresOres.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsLivresOres.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsLivresOres.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsLivresOres.paginator) {
      this.dataDocumentsLivresOres.paginator.firstPage();
    }
  }
  showAddDocumentDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeLivreOre: this.livreOre.code
   };
 
   const dialogRef = this.dialog.open(DialogAddDocumentLivreOreComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(docAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditDocumentDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeDocumentLivreOre : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditDocumentLivreOreComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(docAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Document Séléctionnée!');
    }
    }
    supprimerDocuments()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(doc, index) {
              this.livresOresService.deleteDocumentLivreOre(doc.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Document Séléctionnée!');
        }
    }
    refresh() {
      this.livresOresService.getDocumentsLivresOresByCodeLivreOre(this.livreOre.code).subscribe((listDocumentsLivresOres) => {
        this.listDocumentsLivresOres = listDocumentsLivresOres;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
