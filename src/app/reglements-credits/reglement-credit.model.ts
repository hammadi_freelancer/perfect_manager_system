
import { BaseEntity } from '../common/base-entity.model' ;
// import { Tranche } from './tranche.model' ;
import { Client } from '../ventes/clients/client.model' ;
import { Fournisseur } from '../achats/fournisseurs/fournisseur.model' ;

import { PayementData } from './payement-data.model';
import { Article } from '../stocks/articles/article.model';
// import { LigneReleveVente } from './ligne-releve-vente.model';
// import { Credit } from '../credits/credit.model';

type  EtatReglementDette= 'Initial' |'Valide' | 'Annule'
// type  TypeOperationCourante= 'Achats' |'Ventes' |'' ;
type  ModePaiement= 'Comptant' |'Credit' | 'Cheque' | 'Avance' |'Credit_Comptant' | 'Donnation' | '';

export class ReglementCredit extends BaseEntity {
    constructor(
        public code?: string,
        public numeroReglementCredit?: string,
        public dateReglementCreditObject?: Date,
        public dateReglementCredit?: string,
        public valeurRegle?: number,
        public periodeFromObject?: Date,
        public periodeToObject?: Date,
        public mois?: string,
        public modePaiement?: string,
        public etat?: string,
        public client?: Client,
        public description?: string
     ) {
         super();
         this.client = new Client();
         this.valeurRegle = Number(0);
         
        //  this.listLignesRelVentes = [] ;
         // this.categoryClient = 'PERSONNE_PHYSIQUE';
    }
}
