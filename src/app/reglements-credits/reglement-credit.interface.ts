
export interface ReglementCreditElement {
   
         code: string;
         numeroReglementCredit: string;
         dateReglementCredit: string;
         valeurRegle: number;
         periodeFrom: string;
         periodeTo: string;
         mois: string;
         modePaiement: string;
         etat: string;
         description: string;
        raisonSocialeClient: string;
        nomClient: string;
        prenomClient: string;
        }
