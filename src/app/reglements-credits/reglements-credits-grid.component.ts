import { Component, OnInit, Output,ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {ReglementCredit} from './reglement-credit.model';
import {ReglementCreditFilter} from './reglement-credit.filter';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {ReglementsCreditsService} from './reglements-credits.service';
// import { ActionData } from './action-data.interface';
// import { Client } from '../../ventes/clients/client.model';
import { ReglementCreditElement } from './reglement-credit.interface';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
// import { DialogAddClientComponent } from '../ventes/clients/dialog-add-client.component';
import {SelectItem} from 'primeng/api';

@Component({
  selector: 'app-reglements-credits-grid',
  templateUrl: './reglements-credits-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class ReglementsCreditsGridComponent implements OnInit {

  private reglementCredit: ReglementCredit =  new ReglementCredit();
  private selection = new SelectionModel<ReglementCreditElement>(true, []);
  private reglementCreditFilter: ReglementCreditFilter = new ReglementCreditFilter();
  

  private columnsToSelect : SelectItem[];
   private  selectedColumnsDefinitions: string[];
  private selectedColumnsNames: string[];
  
@Output()
select: EventEmitter<ReglementCredit[]> = new EventEmitter<ReglementCredit[]>();


 selectedReglementCredit: ReglementCredit ;
 private   showFilter = false;
 // private   showFilterClient = false;
  private   showFilterClient = false;
 
 private showSelectColumnsMultiSelect = false;
 
 private etatsReglementsCredits: any[] = [
  {value: 'Initial', viewValue: 'Initiale'},
  {value: 'Valide', viewValue: 'Validée'},
  {value: 'Annule', viewValue: 'Annulée'},
  // {value: 'Regle', viewValue: 'Reglée'},
  // {value: 'NonRegle', viewValue: 'Non Reglée'},
 //  {value: 'PartiellementRegle', viewValue: 'Partiellement Reglée'},
];

private modesPaiements: any[] = [
  {value: 'Comptant', viewValue: 'Comptant'},
  {value: 'Cheque', viewValue: 'Chèque'},
  {value: 'Avance', viewValue: 'Avance'},
  {value: 'Virement', viewValue: 'Virement'}
];

 @Input()
 private listReglementsCredits: any = [] ;
 private checkedAll: false;

 private  dataReglementsCreditsSource: MatTableDataSource<ReglementCreditElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay :ColumnDefinition[] = [];
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
  constructor( private route: ActivatedRoute, private reglementsCreditsService: ReglementsCreditsService,
    private router: Router, private matSnackBar: MatSnackBar,
  private  dialog: MatDialog ) {
  }
  ngOnInit() {
    this.columnsToSelect = [
      {label: 'N° Règlement', value: 'numeroReglementCredit', title: 'N° Règlement'},
      {label: 'Date Règlement', value: 'dateReglementCredit', title: 'Date Règlement'},
      {label: 'Période De:', value: 'periodeFrom', title: 'Période De:'},
      {label: 'à', value: 'periodeTo', title: 'à'},
      {label: 'Valeur Règlé(DT)', value: 'valeurRegle', title: 'Valeur Règlé(DT)'},
      {label: 'Mode Paiement', value: 'modePaiement', title: 'Mode Paiement'},
      {label: 'Mois', value: 'mois', title: 'Mois'},
      {label: 'Rs.Soc.Client', value:  'raisonSocialeClient',
       title: 'Rs.Soc.Client' },
      {label: 'CIN Four.', value:  'cinClient', title: 'CIN Four.' },
      {label: 'Nom Four.', value:  'nomClient', title: 'Nom Four.' },
      {label: 'Prénom Four.', value:  'prenomClient', title: 'Prénom Four.' },
      {label: 'Déscription', value:  'description', title: 'Déscription' },
      {label: 'Etat', value:  'etat', title: 'Etat' },
  
  ];
  this.selectedColumnsDefinitions = [
    'numeroReglementCredit',
   'dateReglementCredit' ,
   'valeurRegle' ,
   'raisonSocialeClient'
    ];
    this.reglementsCreditsService.getPageReglementsCredits(0, 5, null).
    subscribe((regCredits) => {
      this.listReglementsCredits = regCredits;
  
      this.defaultColumnsDefinitions = [
        {name: 'numeroReglementCredit' , displayTitle: 'N° Règlement'},
        {name: 'dateReglementCredit' , displayTitle: 'Date Règlement'},
         {name: 'valeurRegle' , displayTitle: 'Valeur Règlé'},
        {name: 'cinClient' , displayTitle: 'CIN Client'},
        {name: 'nomClient' , displayTitle: 'Nom Client'},
        {name: 'prenomClient' , displayTitle: 'Prénom Client'},
        {name: 'raisonSocialeClient' , displayTitle: 'Raison Soc.'},
         {name: 'modePaiement' , displayTitle: 'Mode Paiement'},
         {name: 'description' , displayTitle: 'Déscription'},
         {name: 'etat', displayTitle: 'Etat' },
         
        ];
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });
  }
  deleteReglementCredit(reglementCredit) {
     //  console.log('call delete !', operationCourante );
    this.reglementsCreditsService.deleteReglementCredit(reglementCredit.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData(null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.reglementsCreditsService.getPageReglementsCredits(0, 5, filter).subscribe((reglementsCredits) => {
    this.listReglementsCredits= reglementsCredits;
    this.transformDataToByConsumedByMatTable();
   });
}


checkOneActionInvoked() {
  const listSelectedReglementsCredits: ReglementCredit[] = [];

if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((regElement) => {
  return regElement.code;
});
this.listReglementsCredits.forEach(RG => {
  if (codes.findIndex(code => code === RG.code) > -1) {
    listSelectedReglementsCredits.push(RG);
  }
  });
}
this.select.emit(listSelectedReglementsCredits);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listReglementsCredits !== undefined) {
    this.listReglementsCredits.forEach(regCredit => {
      listElements.push( {
        code: regCredit.code ,
        numeroReglementCredit: regCredit.numeroReglementCredit,
        cinClient: regCredit.client.cin,
        nomClient: regCredit.client.nom ,
        prenomClient: regCredit.client.prenom ,
        raisonSocialeClient: regCredit.client.raisonSociale ,
       valeurRegle: regCredit.valeurRegle ,
       mois: regCredit.mois ,
       dateReglementCredit: regCredit.dateReglementCredit,
       periodeFrom: regCredit.periodeFrom,
       periodeTo: regCredit.periodeTo,
        modePaiement: regCredit.modePaiement,
       etat: regCredit.etat,
       description: regCredit.description,
       
      } );
    });
  }
    this.dataReglementsCreditsSource = new MatTableDataSource<ReglementCreditElement>(listElements);
    this.dataReglementsCreditsSource.sort = this.sort;
    this.dataReglementsCreditsSource.paginator = this.paginator;
    this.reglementsCreditsService.getTotalReglementsCredits(this.reglementCreditFilter).subscribe((response) => {
    //   console.log('Total Releves de Ventes  is ', response);
    if (this.dataReglementsCreditsSource.paginator) {
       this.dataReglementsCreditsSource.paginator.length = response['totalReglementsCredits'];
  
        }    });
}

specifyListColumnsToBeDisplayed() {
  
   console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
   this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
 ?
    this.defaultColumnsDefinitions : [];
    this.selectedColumnsNames = [];
    
    if (this.selectedColumnsDefinitions !== undefined)
     {
       this.selectedColumnsDefinitions.forEach((colName) => { 
            const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
            const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
            const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
            this.columnsDefinitionsToDisplay.push(colDef);
           });
     }
    this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
   this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
              return colDef.displayTitle;
    });
    console.log('columns to disp:', this.columnsDefinitionsToDisplay);
    this.columnsNames = [];
    this.columnsNames[0] = 'select';
    this.columnsDefinitionsToDisplay .map((colDef) => {
     this.columnsNames.push(colDef.name);
    if (this.selectedColumnsDefinitions !== undefined) {
     this.selectedColumnsNames.push(colDef.displayTitle);
    }
 });
    this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
     return colDef.displayTitleAr;
 });
 }

masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataReglementsCreditsSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataReglementsCreditsSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataReglementsCreditsSource.filter = filterValue.trim().toLowerCase();
  if (this.dataReglementsCreditsSource.paginator) {
    this.dataReglementsCreditsSource.paginator.firstPage();
  }
}

loadAddReglementCreditComponent() {
  this.router.navigate(['/pms/suivie/ajouterReglementCredit']) ;

}
loadEditReglementCreditComponent() {
  this.router.navigate(['/pms/suivie/editerReglementCredit', this.selection.selected[ this.selection.selected.length - 1].code]) ;

}
supprimerReglementsCredits()
{

  if ( this.selection.selected.length !== 0) {
      this.selection.selected.forEach(function(regCredit, index) {

   this.reglementsCreditsService.deleteReglementCredit(regCredit.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.openSnackBar( this.selection.selected.length + ' Reglement(s) Credit(s) Supprimé(s)');
            this.selection.selected = [];
            this.loadData(null);
            }
      }, this);
    } else {
      this.openSnackBar('Aucun Règlement Séléctionnée !');
    }
}
refresh() {
  this.loadData(null);
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

 history()
 {
   
 }
 /*addClient()
 {
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
  };

  const dialogRef = this.dialog.open(DialogAddClientComponent,
    dialogConfig
  );
 }*/

 toggleFilterPanel()
 {
    this.showFilter = !this.showFilter;
 }
 toggleShowColumnsMultiSelect()
 {
   this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
 }
 loadFiltredData()
 {
  //  console.log('the filter is ', this.operationCouranteFilter);
   this.loadData(this.reglementCreditFilter);
 }
 attacherDocuments()
 {

 }
 changePage($event) {
  console.log('page event is ', $event);
 this.reglementsCreditsService.getPageReglementsCredits($event.pageIndex, $event.pageSize,
  this.reglementCreditFilter ).subscribe((regCredits) => {
    this.listReglementsCredits = regCredits;
   const listElements = [];
   this.listReglementsCredits.forEach(regCredit => {
    listElements.push( {
      code: regCredit.code ,
      numeroReglementCredit: regCredit.numeroReglementCredit,
      cinClient: regCredit.client.cin,
      nomClient: regCredit.client.nom ,
      prenomClient: regCredit.client.prenom ,
      raisonSocialeClient: regCredit.client.raisonSociale ,
     valeurRegle: regCredit.valeurRegle ,
     mois: regCredit.mois ,
     dateReglementCredit: regCredit.dateReglementCredit,
     periodeFrom: regCredit.periodeFrom,
     periodeTo: regCredit.periodeTo,
      modePaiement: regCredit.modePaiement,
     etat: regCredit.etat,
     description: regCredit.description
      
  });
});
     this.dataReglementsCreditsSource= new MatTableDataSource<ReglementCreditElement>(listElements);

     
     this.reglementsCreditsService.getTotalReglementsCredits(this.reglementCreditFilter)
     .subscribe((response) => {
      console.log('Total OPer Courantes  is ', response);
      if (this.dataReglementsCreditsSource.paginator) {
       this.dataReglementsCreditsSource.paginator.length = response['totalReglementsCredits'];
      }
     });

    });
}


activateFilterClient()
{
  this.showFilterClient = !this.showFilterClient ;
  
}

}
