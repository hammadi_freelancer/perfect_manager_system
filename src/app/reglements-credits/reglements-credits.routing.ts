import { Injectable } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
// operationsCourantes components
import { ReglementCreditNewComponent } from './reglement-credit-new.component';
import { ReglementCreditEditComponent } from './reglement-credit-edit.component';
import { ReglementsCreditsGridComponent } from './reglements-credits-grid.component';
import { ReglementCreditHomeComponent } from './reglement-credit-home.component';

import { ClientsResolver } from '../ventes/clients-resolver';

import {ModuleWithProviders  } from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';

 export const reglementsCreditsRoute: Routes = [
    {
        path: 'reglementsCredits',
        component: ReglementsCreditsGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterReglementCredit',
        component: ReglementCreditNewComponent,
        resolve: {
            clients: ClientsResolver,
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerReglementCredit/:codeReglementCredit',
        component: ReglementCreditEditComponent,
        resolve: {
            clients: ClientsResolver,
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
];
// export const ventesRoutingModule: ModuleWithProviders = RouterModule.forRoot(ventesRoute);
// commonRouting.providers = [{ provide: APP_BASE_HREF, useValue: '/common'} ] ;


