import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import {ReglementCredit} from './reglement-credit.model' ;
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_REGLEMENTS_CREDITS = 'http://localhost:8082/reglementsCredits/getReglementsCredits';
const  URL_GET_PAGE_REGLEMENTS_CREDITS= 'http://localhost:8082/reglementsCredits/getPageReglementsCredits';

const URL_GET_REGLEMENT_CREDIT = 'http://localhost:8082/reglementsCredits/getReglementCredit';
const  URL_ADD_REGLEMENT_CREDIT = 'http://localhost:8082/reglementsCredits/addReglementCredit';
const  URL_UPDATE_REGLEMENT_CREDIT  = 'http://localhost:8082/reglementsCredits/updateReglementCredit';
const  URL_DELETE_REGLEMENT_CREDIT= 'http://localhost:8082/reglementsCredits/deleteReglementCredit';
const  URL_GET_TOTAL_REGLEMENTS_CREDITS = 'http://localhost:8082/reglementsCredits/getTotalReglementsCredits';



@Injectable({
  providedIn: 'root'
})
export class ReglementsCreditsService {

  constructor(private httpClient: HttpClient) {

   }
   getTotalReglementsCredits(filter): Observable<any> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('filter', filterStr);
        return this.httpClient
        .get<any>(URL_GET_TOTAL_REGLEMENTS_CREDITS, {params});
   }
   getPageReglementsCredits(pageNumber, nbElementsPerPage, filter): Observable<ReglementCredit[]> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('pageNumber', pageNumber)
    .set('nbElementsPerPage', nbElementsPerPage)
    .set('filter', filterStr);
    return this.httpClient
    .get<ReglementCredit[]>(URL_GET_PAGE_REGLEMENTS_CREDITS, {params});
   }
   getReglementsCredits(): Observable<ReglementCredit[]> {
   
    return this.httpClient
    .get<ReglementCredit[]>(URL_GET_LIST_REGLEMENTS_CREDITS);
   }
   addReglementCredit(reglementCredit: ReglementCredit): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_REGLEMENT_CREDIT, reglementCredit);
  }
  updateReglementCredit(reglementCredit: ReglementCredit): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_REGLEMENT_CREDIT, reglementCredit);
  }
  deleteReglementCredit(codeReglementCredit): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_REGLEMENT_CREDIT + '/' + codeReglementCredit);
  }
  getReglementCredit(codeReglementCredit): Observable<ReglementCredit> {
    return this.httpClient.get<ReglementCredit>(URL_GET_REGLEMENT_CREDIT + '/' + codeReglementCredit);
  }
}
