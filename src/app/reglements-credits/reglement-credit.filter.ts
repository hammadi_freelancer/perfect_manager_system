

export class ReglementCreditFilter {
    numeroReglementCredit: string ;
    dateReglementCreditFromObject: Date;
    dateReglementCreditToObject: Date;
    periodeReglementCreditFromObject: Date;
    periodeReglementCreditToObject: Date;
    valeurRegele: number;
    nomClient: string;
    prenomClient: string;
    raisonClient: string;
    description: string;
    mois: string;
    modePaiement: string;
    etat: string;
   }
