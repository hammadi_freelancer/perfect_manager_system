import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,
  
    ViewChild } from '@angular/core';
  // import { ArticleService } from './article.service';
  import {ReglementCredit} from './reglement-credit.model';
  // import { ParametresVentes } from '../parametres-ventes.model';
  
  import {ReglementsCreditsService} from './reglements-credits.service';
  
  // import {VentesService} from '../ventes.service';
  import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
  
  import {FormControl} from '@angular/forms';
  import {map, startWith} from 'rxjs/operators';
  import {Observable} from 'rxjs';
  import { Router, ActivatedRoute, ParamMap } from '@angular/router';
  // import {ActionData} from './action-data.interface';
  import { ClientService } from '../ventes/clients/client.service';
  
  import { ClientFilter } from '../ventes/clients/client.filter';
  import { Client } from '../ventes/clients/client.model';
  
  
  
  
  import { ColumnDefinition } from '../common/column-definition.interface';
  
  import {MatSnackBar} from '@angular/material';
  import { MatExpansionPanel } from '@angular/material';
  import {ModePayementLibelle} from './mode-payement-libelle.interface';
  import {SelectionModel} from '@angular/cdk/collections';
  
  type LibelleModePayement = 'Credit'| 'Comptant' | 'Cheque' ;
  
  @Component({
    selector: 'app-reglement-credit-edit',
    templateUrl: './reglement-credit-edit.component.html',
    // styleUrls: ['./players.component.css']
  })
export class ReglementCreditEditComponent implements OnInit  {

  private reglementCredit: ReglementCredit =  new ReglementCredit();
  
  
  private updateEnCours = false;

  
  private clientsFilter: ClientFilter;
  
  private listClients: Client[];
  

    constructor( private route: ActivatedRoute,
      private router: Router, private regelementCreditService: ReglementsCreditsService,
     //   private clientService: ClientService,
       private clientService: ClientService,
       
      // private articleService: ArticleService,
     private matSnackBar: MatSnackBar, private elRef: ElementRef) {
    }
  ngOnInit() {

    const codeReglementCredit = this.route.snapshot.paramMap.get('codeReglementCredit');
    this.listClients = this.route.snapshot.data.clients;
    this.clientsFilter = new ClientFilter(this.listClients);
    this.regelementCreditService.getReglementCredit(codeReglementCredit).subscribe(
      (regCredit) =>  {
             this.reglementCredit = regCredit;
          
      });

     
  }
  loadGridReglementsCredits() {
    this.router.navigate(['/pms/suivie/reglementsCredits']) ;
  }
updateReglementCredit() {


  this.updateEnCours = true;
  this.regelementCreditService.updateReglementCredit(this.reglementCredit).subscribe((response) => {
    this.updateEnCours = false;
    if (response.error_message ===  undefined) {
      console.log('the response of update is ', response );
      if (this.reglementCredit.etat === 'Valide') {
      this.openSnackBar( ' Règlement mise à jour ');
    } else {
      this.openSnackBar( ' Erreurs!');
    }
}}
);
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 

 fillDataMatriculeRaisonClient() {
   const listFiltred  = this.listClients.filter((client) =>
    (client.registreCommerce === this.reglementCredit.client.registreCommerce));
   this.reglementCredit.client.matriculeFiscale = listFiltred[0].matriculeFiscale;
   this.reglementCredit.client.raisonSociale = listFiltred[0].raisonSociale;
 }
 fillDataMatriculeRegistreCommercialF()
 {
   const listFiltred  = this.listClients.filter((client) => (client.raisonSociale === 
     this.reglementCredit.client.raisonSociale));
   this.reglementCredit.client.matriculeFiscale = listFiltred[0].matriculeFiscale;
   this.reglementCredit.client.registreCommerce = listFiltred[0].registreCommerce;
 }
 fillDataRegistreCommercialRaisonF()
 {
   const listFiltred  = this.listClients.filter((client) => 
   (client.matriculeFiscale === this.reglementCredit.client.matriculeFiscale));
   this.reglementCredit.client.raisonSociale = listFiltred[0].raisonSociale;
   this.reglementCredit.client.registreCommerce = listFiltred[0].registreCommerce;
 }
 fillDataNomPrenomClient() {
   const listFiltred  = this.listClients.filter((client) => (client.cin === this.reglementCredit.client.cin));
   this.reglementCredit.client.nom = listFiltred[0].nom;
   this.reglementCredit.client.prenom = listFiltred[0].prenom;
 }
 fillDataCINPrenomClient()
 {
   const listFiltred  = this.listClients.filter((client) => (client.nom === this.reglementCredit.client.nom));
   this.reglementCredit.client.cin = listFiltred[0].cin;
   this.reglementCredit.client.prenom = listFiltred[0].prenom;
 }
 fillDataCINNomClient()
 {
   const listFiltred  = this.listClients.filter((client) => (client.prenom === this.reglementCredit.client.prenom));
   const num =   Math.floor(Math.random() * Math.floor(listFiltred.length));
   this.reglementCredit.client.cin = listFiltred[num].cin;
   this.reglementCredit.client.nom= listFiltred[num].nom;
 }

 



}
