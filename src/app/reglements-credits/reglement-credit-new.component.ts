import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {ReglementCredit} from './reglement-credit.model';
// import { ParametresVentes } from '../parametres-ventes.model';

import {ReglementsCreditsService} from './reglements-credits.service';

// import {VentesService} from '../ventes.service';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
// import {ActionData} from './action-data.interface';
// import { ClientService } from '../ventes/clients/client.service';
import { ClientService } from '../ventes/clients/client.service';

// import { ClientFilter } from '../ventes/clients/client.filter';
import { ClientFilter } from '../ventes/clients/client.filter';
import { Client } from '../ventes/clients/client.model';

// import { ArticleFilter } from '../stocks/articles/article.filter';

// import {Client} from '../ventes/clients/client.model';


import { ColumnDefinition } from '../common/column-definition.interface';

import {MatSnackBar} from '@angular/material';
import { MatExpansionPanel } from '@angular/material';
import {ModePayementLibelle} from './mode-payement-libelle.interface';
import {SelectionModel} from '@angular/cdk/collections';

type LibelleModePayement = 'Credit'| 'Comptant' | 'Cheque' ;

@Component({
  selector: 'app-reglement-credit-new',
  templateUrl: './reglement-credit-new.component.html',
  // styleUrls: ['./players.component.css']
})
export class ReglementCreditNewComponent implements OnInit {

 private reglementCredit: ReglementCredit =  new ReglementCredit();


private saveEnCours = false;



private clientsFilter: ClientFilter;

private listClients: Client[];



private etatsReglementsCredits: any[] = [
  {value: 'Initial', viewValue: 'Initiale'},
  {value: 'Valide', viewValue: 'Validée'},
  {value: 'Annule', viewValue: 'Annulée'},
  // {value: 'Regle', viewValue: 'Reglée'},
  // {value: 'NonRegle', viewValue: 'Non Reglée'},
 //  {value: 'PartiellementRegle', viewValue: 'Partiellement Reglée'},
];

private modesPaiements: any[] = [
  {value: 'Comptant', viewValue: 'Comptant'},
  {value: 'Cheque', viewValue: 'Chèque'},
  {value: 'Avance', viewValue: 'Avance'},
  {value: 'Virement', viewValue: 'Virement'}
];

private listMois: any[] = [
  {value: 'Janvier', viewValue: 'Janvier'},
  {value: 'Février', viewValue: 'Février'},
  {value: 'Mars', viewValue: 'Mars'},
  {value: 'Avril', viewValue: 'Avril'},
  {value: 'Mai', viewValue: 'Mai'},
  {value: 'Juin', viewValue: 'Juin'},
  {value: 'Juillet', viewValue: 'Juillet'},
  {value: 'Août', viewValue: 'Août'},
  {value: 'Septembre', viewValue: 'Septembre'},
  {value: 'Octobre', viewValue: 'Octobre'},
  {value: 'Novembre', viewValue: 'Novembre'},
  {value: 'Décembre', viewValue: 'Décembre'}
];


  constructor( private route: ActivatedRoute,
    private router: Router, private reglementsCreditsService: ReglementsCreditsService,
     private clientService: ClientService,
     
   private matSnackBar: MatSnackBar, private elRef: ElementRef) {
  }


  ngOnInit() {
   this.listClients = this.route.snapshot.data.clients;
    this.clientsFilter = new ClientFilter(this.listClients);
    

  }
  saveReglementCredit() {

    this.saveEnCours = true;
    this.reglementsCreditsService.addReglementCredit(this.reglementCredit).subscribe((response) => {
      this.saveEnCours = false;
      
        if (response.error_message ===  undefined) {
          console.log('undefined error message');
          // this.save.emit(response);
          this.reglementCredit = new  ReglementCredit();
          this.openSnackBar(  'Règlement Credit Enregistré');
        } else {
         // show error message
         this.openSnackBar(  'Erreurs:Opération Echouée');
         
        }
    });
  }
  fillDataMatriculeRaisonClient() {
    const listFiltred  = this.listClients.filter((client) =>
     (client.registreCommerce === this.reglementCredit.client.registreCommerce));
    this.reglementCredit.client.matriculeFiscale = listFiltred[0].matriculeFiscale;
    this.reglementCredit.client.raisonSociale = listFiltred[0].raisonSociale;
  }
  fillDataMatriculeRegistreCommercialF()
  {
    const listFiltred  = this.listClients.filter((client) => (client.raisonSociale === 
      this.reglementCredit.client.raisonSociale));
    this.reglementCredit.client.matriculeFiscale = listFiltred[0].matriculeFiscale;
    this.reglementCredit.client.registreCommerce = listFiltred[0].registreCommerce;
  }
  fillDataRegistreCommercialRaisonF()
  {
    const listFiltred  = this.listClients.filter((client) => 
    (client.matriculeFiscale === this.reglementCredit.client.matriculeFiscale));
    this.reglementCredit.client.raisonSociale = listFiltred[0].raisonSociale;
    this.reglementCredit.client.registreCommerce = listFiltred[0].registreCommerce;
  }
  fillDataNomPrenomClient() {
    const listFiltred  = this.listClients.filter((client) => (client.cin === this.reglementCredit.client.cin));
    this.reglementCredit.client.nom = listFiltred[0].nom;
    this.reglementCredit.client.prenom = listFiltred[0].prenom;
  }
  fillDataCINPrenomClient()
  {
    const listFiltred  = this.listClients.filter((client) => (client.nom === this.reglementCredit.client.nom));
    this.reglementCredit.client.cin = listFiltred[0].cin;
    this.reglementCredit.client.prenom = listFiltred[0].prenom;
  }
  fillDataCINNomClient()
  {
    const listFiltred  = this.listClients.filter((client) => (client.prenom === this.reglementCredit.client.prenom));
    const num =   Math.floor(Math.random() * Math.floor(listFiltred.length));
    this.reglementCredit.client.cin = listFiltred[num].cin;
    this.reglementCredit.client.nom= listFiltred[num].nom;
  }
  vider() {
    this.reglementCredit =  new ReglementCredit();
  }
  openSnackBar(messageToDisplay) {
   this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
       verticalPosition: 'top', panelClass: 'snackBarClass'});
  }
loadGridReglementsCredits() {
  this.router.navigate(['/pms/suivie/reglementsCredits']) ;
}
}
