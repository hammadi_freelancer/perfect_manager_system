import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ReactiveFormsModule } from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatGridListModule} from '@angular/material/grid-list';

import { RouterModule} from '@angular/router';

import {TableModule} from 'primeng/table';
import { UploadFileComponent} from './upload-file.component';
import { AlertMessageComponent} from './alert-message.component';

// import { payementsRoute } from './payements.route';
import {FormsModule } from '@angular/forms';
import {MatSnackBarModule} from '@angular/material/snack-bar';

import {APP_BASE_HREF} from '@angular/common';
// import { financeRoutingModule } from './finance.routing';

/*const ENTITY_STATES = [
  ...payementsRoute
];*/
@NgModule({
  declarations: [
    UploadFileComponent,
    AlertMessageComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, MatDatepickerModule, MatNativeDateModule,
     BrowserAnimationsModule, ReactiveFormsModule, MatAutocompleteModule,
    MatInputModule, MatCardModule, MatSelectModule, MatIconModule , MatCheckboxModule ,
    FormsModule, MatGridListModule,
     TableModule , MatSnackBarModule
   // RouterModule.forRoot(ENTITY_STATES, {useHash: false})
  ],
  providers: [MatNativeDateModule
],
  exports: [
    RouterModule,
    UploadFileComponent,
    AlertMessageComponent
  ],
  bootstrap: []
})
export class SharedComponentModule { }
