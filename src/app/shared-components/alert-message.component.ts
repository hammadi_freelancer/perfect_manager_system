import { Component, OnInit, Input, EventEmitter, Output, AfterContentInit, AfterViewInit,
    AfterViewChecked
    } from '@angular/core';
    // import { ArticleService } from './article.service';
    import {FormControl} from '@angular/forms';
    import {map, startWith} from 'rxjs/operators';
    import {Observable} from 'rxjs';
    import { Router, ActivatedRoute, ParamMap } from '@angular/router';
    import { AlertMessage } from './alert-message.interface';
    import {MatSnackBar} from '@angular/material';
    import { MatSnackBarVerticalPosition } from '@angular/material';

    @Component({
      selector: 'app-alert-message',
      templateUrl: './alert-message.component.html',
      // styleUrls: ['./players.component.css']
    })
    export class AlertMessageComponent implements OnInit {
    // private client: Client = new Client();
    @Input()
    private alertMessage: AlertMessage;


      constructor( private route: ActivatedRoute, private router: Router, private snackBar: MatSnackBar  ) {
      }
      ngOnInit() {
          this.openSnackBar();
      }
      openSnackBar() {
           let messageToDisplay = '';
          if (this.alertMessage.operation_name === 'UPDATE') {
                messageToDisplay = 'la mise à jour est fait avec succés ';
            }
            if (this.alertMessage.operation_name === 'ADD') {
                messageToDisplay = 'l\'ajout de l\'objet est fait avec succés';
            }
            if (this.alertMessage.operation_name === 'DELETE') {
                messageToDisplay = 'la suppression est fait avec succés';
            }
        messageToDisplay = '<span style=\' color:hotpink;\' >' + messageToDisplay + ' </span>';
        this.snackBar.open(messageToDisplay, '', {duration: 5000,
            verticalPosition: 'top'});
    }
     }
