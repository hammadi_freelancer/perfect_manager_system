
import { BaseEntity } from '../common/base-entity.model' ;

export class FileData extends BaseEntity {
    constructor(public code?: string, public name?: string, public type?: string, public auteur?: string,
         public description?: string, public dataUrl?: any,
        public codeDocument?: string , public codeAgent?: string // l'agent peut etre un fournisseur , un client , un utilisateur ...
       // code de document ou de document type associé à ce fichier
     ) {
        super();
    }
}
