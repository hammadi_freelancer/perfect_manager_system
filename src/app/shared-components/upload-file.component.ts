import { Component, OnInit, Input, EventEmitter, Output, AfterContentInit, AfterViewInit,
    AfterViewChecked
    } from '@angular/core';
    // import { ArticleService } from './article.service';
    import {FormControl} from '@angular/forms';
    import {map, startWith} from 'rxjs/operators';
    import {Observable} from 'rxjs';
    import { Router, ActivatedRoute, ParamMap } from '@angular/router';
    import { UploadFileService } from './upload-file.service' ;
    import { Document } from '../documents/document.model';
    import { FileData } from './file-data.model';
    @Component({
      selector: 'app-upload-files',
      templateUrl: './upload-file.component.html',
      // styleUrls: ['./players.component.css']
    })
    export class UploadFileComponent implements OnInit {
    // private client: Client = new Client();
    @Input()
    private classeur: Document;

    private nbreFiles = 1; // will be paramterable

    private filesData: FileData[];

    private file: any;

      constructor( private route: ActivatedRoute,
        private router: Router , private uploadFileService: UploadFileService ) {
      }
      ngOnInit() {
        this.filesData = [];
      }
      fileChanged($event) {
        this.file = $event.target.files[0];
       }
       uploadFile(indexFile) {
         const fileReader = new FileReader();
         fileReader.onload = (e) => {
           console.log(fileReader.result);
            const fileData = new FileData();
            fileData.name = '';
            fileData.auteur = '';
            fileData.type = 'png';
           // fileData.dateCreation = new Date(Date.now).toLocaleDateString();
           //  fileData.dateLastUpdate = new Date(Date.now).toLocaleDateString();
            fileData.codeDocument = this.classeur === undefined ? null : this.classeur.code;
            fileData.dataUrl = fileReader.result;
           this.filesData[indexFile] = fileData;
         };
         fileReader.readAsDataURL(this.file);
       // console.log('the type is ...', this.fileData);
       // this.document.filesData[i] = fileData;
       // console.log(typeof(t));
     }
     removeFile(indexFile) {
      this.filesData.splice(indexFile, 1);
    }
    saveFileData(indexFile) {
      console.log(' SAVE FILE DATA  IS CALLED ');
        console.log(this.filesData[0]);
        // this.saveEnCours = true;
        this.uploadFileService.addFileData(this.filesData[0]).subscribe((response) => {
            if (response.error_message ===  undefined) {
              // this.save.emit(response);
             //  this.document = new Document();
             this.filesData.splice(indexFile, 1);
            } else {
             // show error message
            }
        });
    }
    invokeBatchAddingsFiles() {
      console.log(' INVOKE BATCH ADDINGS FILES IS CALLED ');
       console.log(this.filesData);
      // this.saveEnCours = true;
      this.uploadFileService.batchAddingFiles(this.filesData).subscribe((response) => {
          if (response.error_message ===  undefined) {
            // this.save.emit(response);
           //  this.document = new Document();
           this.filesData = [];
          } else {
           // show error message
          }
      });
  }
    updateFileData(indexFile) {
      // this.documentService.updateDocument(this.document).subscribe((response) => {
      /*   if (response.error_message ===  undefined) {
          this.save.emit(response);
          this.document = new Document()  ;
          this.modeUpdate = false;
        } else {
         // show error message
        }
    });*/

    }

     }
