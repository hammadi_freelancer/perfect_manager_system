import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEventType, HttpResponse } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';
import { FileData } from './file-data.model';

const  URL_ADD_FILE_DATA = 'http://localhost:8082/shared-components/storage/addFileData';
const  URL_BATCH_ADDING_FILES = 'http://localhost:8082/shared-components/storage/batchAddingFiles';
const  URL_DELETE_FILE_DATA = 'http://localhost:8082/shared-components/storage/deleteFileData';
const  URL_GET_LIST_FILE_DATA = 'http://localhost:8082/shared-components/storage/getFilesData';

@Injectable({
  providedIn: 'root'
})
export class UploadFileService {
  constructor(private httpClient: HttpClient) {}


  addFileData(fileData: FileData): Observable<any> {
    return this.httpClient.post<FileData>(URL_ADD_FILE_DATA, fileData);
  }
  deleteFileData(codeFileData): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_FILE_DATA + '/' + codeFileData);
  }
  getFilesData(): Observable<FileData[]> {
    return this.httpClient
    .get<FileData[]>(URL_GET_LIST_FILE_DATA);
   }
  public batchAddingFiles(filesData: FileData[]): Observable<any> {
    return this.httpClient.post<FileData[]>(URL_BATCH_ADDING_FILES, filesData);
        // this will be the our resulting map
      /*  const status = {};
        files.forEach(file => {
          // create a new multipart-form for every file
          const formData: FormData = new FormData();
          formData.append('file', file, file.name);
          // create a http-post request and pass the form
          // tell it to report the upload progress
          const req = new HttpRequest('POST', URL_ADD_DOCUMENT, formData, {
            reportProgress: true
          });
          // create a new progress-subject for every file
          const progress = new Subject<number>();
          // send the http-request and subscribe for progress-updates
          this.http.request(req).subscribe(event => {
            if (event.type === HttpEventType.UploadProgress) {
              // calculate the progress percentage
              const percentDone = Math.round(100 * event.loaded / event.total);
              // pass the percentage into the progress-stream
              progress.next(percentDone);
            } else if (event instanceof HttpResponse) {
             // Close the progress-stream if we get an answer form the API
              // The upload is complete
              progress.complete();
            }
          });
          // Save every progress-observable in a map of all observables
          status[file.name] = {
            progress: progress.asObservable()
          };
        });
        // return the map of progress.observables
        return status;
      }*/
}
}
