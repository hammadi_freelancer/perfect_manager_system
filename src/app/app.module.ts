import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ReactiveFormsModule } from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatCardModule} from '@angular/material/card';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
 import { RouterModule} from '@angular/router';

import {FormsModule } from '@angular/forms';
import { HomeModule } from './home/home.module';
import { StocksModule } from './stocks/stocks.module';

import { CommonModule } from './common/common.module';
import { FinanceModule } from './finance/finance.module';
import { AchatsModule } from './achats/achats.module';
import { ProfilesModule } from './profiles/profiles.module';

import { VentesModule } from './ventes/ventes.module';
import { CaisseModule } from './caisse/caisse.module';
import { OrganisationsModule } from './organisations/organisations.module';
import { GroupesModule } from './groupes/groupes.module';
import { UtilisateursModule } from './utilisateurs/utilisateurs.module';
import { MarketingModule } from './marketing/marketing.module';
import { ReglementsCreditsModule } from './reglements-credits/reglements-credits.module';
import { ReglementsDettesModule } from './reglements-dettes/reglements-dettes.module';
import { HistoriquesClientsModule } from './historiques-clients/historiques-clients.module';
import { HistoriquesFournisseursModule } from './historiques-fournisseurs/historiques-fournisseurs.module';

import { RHModule } from './resources-humaines/resources-humaines.module';

// import { DashboardsModule } from './dashboards/dashboards.module';
import { appRoutingModule } from './app.routing';
 // import { commonRoute } from './common/common.route';
import {APP_BASE_HREF} from '@angular/common';
import { JwtInterceptor } from './jwt.interceptor';
import { AuthGuard} from './auth.guard';


/*const ENTITY_STATES = [
 ...commonRoute,
  ...payementsRoute
];*/

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, MatDatepickerModule, MatNativeDateModule,
     BrowserAnimationsModule, ReactiveFormsModule, MatAutocompleteModule,
    MatInputModule, MatCardModule, MatSelectModule, MatGridListModule,
    FormsModule,
    FinanceModule,
    CommonModule,
    AchatsModule,
    VentesModule,
    ProfilesModule,
    CaisseModule,
    MarketingModule,
    RHModule,
    // DashboardsModule,
    StocksModule,
    HomeModule,
    OrganisationsModule,
    HistoriquesClientsModule,
    HistoriquesFournisseursModule,
    ReglementsCreditsModule,
    ReglementsDettesModule,
    GroupesModule,
    UtilisateursModule,
    appRoutingModule,
   //  RouterModule.forRoot(ENTITY_STATES, { useHash : false}),
    /*[
      {
         path: 'app-article',
         component: ArticleComponent
      },
      {
        path: 'app-client',
        component: ClientComponent
     },

{
  path: 'app-articles-grid',
  component: ArticlesGridComponent
},
{
  path: 'app-clients-grid',
  component: ClientsGridComponent
},
{
  path: 'app-utilisateur',
  component: UtilisateurComponent
},
{
  path: 'gestionClients',
  component: GestionClientsComponent
},
{
  path: 'gestionArticles',
  component: GestionArticlesComponent
},
{ path: '**', redirectTo: '/' }
   ])
  ]
*/]
  ,
  providers: [
    MatNativeDateModule, AuthGuard,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }
 
  ],
  exports: [
   RouterModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
