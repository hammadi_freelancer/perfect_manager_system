import { Injectable } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { PromotionsGridComponent } from './promotions/promotions-grid.component';
import { PromotionNewComponent } from './promotions/promotion-new.component';
import { PromotionEditComponent } from './promotions/promotion-edit.component';

import { MannequinsGridComponent } from './mannequins/mannequins-grid.component';
import { MannequinNewComponent } from './mannequins/mannequin-new.component';
import { MannequinEditComponent } from './mannequins/mannequin-edit.component';

import { CompagnesPublicitairesGridComponent } from './compagnes-publicitaires/compagnes-publicitaires-grid.component';
import { CompagnePublicitaireNewComponent } from './compagnes-publicitaires/compagne-publicitaire-new.component';
import { CompagnePublicitaireEditComponent } from './compagnes-publicitaires/compagne-publicitaire-edit.component';

import {ModuleWithProviders  } from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';

 export const  marketingRoute: Routes = [
    {
        path: 'promotions',
        component: PromotionsGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterPromotion',
        component: PromotionNewComponent,
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerPromotion/:codePromotion',
        component: PromotionEditComponent,
        resolve: { 
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'mannequins',
        component: MannequinsGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterMannequin',
        component: MannequinNewComponent,
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerMannequin/:codeMannequin',
        component: MannequinEditComponent,
        resolve: { 
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'compagnesPublicitaires',
        component: CompagnesPublicitairesGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterCompagnePublicitaire',
        component: CompagnePublicitaireNewComponent,
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerCompagnePublicitaire/:codeCompagnePublicitaire',
        component: CompagnePublicitaireEditComponent,
        resolve: { 
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    }
];



