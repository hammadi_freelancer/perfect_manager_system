import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,
  AfterViewInit , AfterContentInit,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {Mannequin} from './mannequin.model';

import {MarketingService} from '../marketing.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { MatDatepickerInput } from '@angular/material/datepicker';
import { MatSnackBar } from '@angular/material';
import { MatDatepicker } from '@angular/material/datepicker';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-mannequin-edit',
  templateUrl: './mannequin-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class MannequinEditComponent implements OnInit  {

  private mannequin: Mannequin =  new Mannequin();
  private tabsDisabled = true;
  private updateEnCours = false;

  private etatsMannequin: any[] = [
    {value: 'Initial', viewValue: 'Initiale'},
    {value: 'Valide', viewValue: 'Validé'},
    {value: 'Collaborateur', viewValue: 'Collaborateur'},
    {value: 'Annule', viewValue: 'Annulé'}
  ];

  constructor( private route: ActivatedRoute,
    private router: Router, private marketingService: MarketingService, private matSnackBar: MatSnackBar,
    public dialog: MatDialog
   ) {
  }
  ngOnInit() {
    const codeMannequin = this.route.snapshot.paramMap.get('codeMannequin');
    this.marketingService.getMannequins(codeMannequin).subscribe((mannequin) => {
             this.mannequin = mannequin;
         
                  this.tabsDisabled = false;
 
    });
  }
 
  loadAddMannequinComponent() {
    this.router.navigate(['/pms/marketing/ajouterMannequin']) ;
  }
  loadGridMannequins() {
    this.router.navigate(['/pms/marketing/mannequins']) ;
  }

updateMannequin() {
  this.updateEnCours = true;
  this.marketingService.updateMannequin(this.mannequin).subscribe((response) => {
    this.updateEnCours = false;
    if (response.error_message ===  undefined) {
      this.openSnackBar( ' Mannequin  Mis à Jour ');
    } else {
      this.openSnackBar( ' Erreurs:Opération Echouée ');
    }
});
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

 selectedTabChanged($event) {
  if ($event.index === 4) { // Composition tab was selected
   // this.loadListArticles();
   } else if ($event.index === 1) {
    //  this.loadListSchemasConfiguration() ;
   }
  }
}
