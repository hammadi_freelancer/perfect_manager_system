
import { BaseEntity } from '../../common/base-entity.model' ;
// import { Tranche } from './tranche.model' ;
import { Mannequin } from './mannequin.model' ;
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
// import { Service } from '../../stocks/articles/article.service';

export const MODES_GENERATION_TRANCHES = ['AUTOMATIQUE', 'MANUELLE', ];

export class MannequinFilter  {
    public selectedCin: string;
    public selectedCode: string;
    
    public selectedNom: string;
    public selectedPrenom: string;
    private controlNoms = new FormControl();
    private controlPrenoms = new FormControl();
    private controlCins = new FormControl();

    
   //  private controlAdresse = new FormControl();
    private listNoms: string[] = [];
    private listPrenoms: string[] = [];
    private listCins: string[] = [];

    private filteredNoms: Observable<string[]>;
    private filteredPrenoms: Observable<string[]>;
    private filteredCins: Observable<string[]>;

    
    constructor (mannequins: Mannequin[]) {
      if (mannequins !== null) {
       //  const listClientsMorals = clients.filter((client) => (client.categoryClient === 'PERSONNE_MORALE'));
        // const listClientsPhysiques = clients.filter((client) => (client.categoryClient === 'PERSONNE_PHYSIQUE'));
        this.listCins = mannequins.filter((mannequin) =>
        (mannequin.cin !== undefined && mannequin.cin != null )).map((mannequin) => (mannequin.cin ));
        this.listNoms = mannequins.filter((mannequin) =>
        (mannequin.nom !== undefined && mannequin.nom != null )).map((mannequin) => (mannequin.nom ));

        // this.listNoms = clients.map((client) => (client.nom));
        this.listPrenoms = mannequins.filter((mannequin) =>
        (mannequin.prenom !== undefined && mannequin.prenom != null )).map((mannequin) => (mannequin.prenom ));
    

      this.filteredNoms = this.controlNoms.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterNoms(value))
        );
        this.filteredPrenoms = this.controlPrenoms.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterPrenoms(value))
        );
        this.filteredCins = this.controlCins.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filterCins(value))
        );



    }

  }
    private _filterNoms(value: string): string[] {
      let  filterValue = '';
      if (value !== undefined) {
        filterValue = value.toLowerCase();
      }
        return this.listNoms.filter(nom => nom.toLowerCase().includes(filterValue));
      }
      private _filterCins(value: string): string[] {
        let  filterValue = '';
        if (value !== undefined) {
          filterValue = value.toLowerCase();
        }
        return this.listCins.filter(cin => cin.toLowerCase().includes(filterValue));
      }
      private _filterPrenoms(value: string): string[] {
        let  filterValue = '';
        if (value !== undefined) {
          filterValue = value.toLowerCase();
        }
        return this.listPrenoms.filter(prenom => prenom.toLowerCase().includes(filterValue));
      }

      public getControlPrenoms(): FormControl {
        return this.controlPrenoms;
      }
      public getControlNoms(): FormControl {
        return this.controlNoms;
      }
      public getControlCins(): FormControl {
        return this.controlCins;
      }
      public getFiltredPrenoms(): Observable<string[]> {
        return this.filteredPrenoms;
      }
      public getFiltredNoms(): Observable<string[]> {
        return this.filteredNoms;
      }
      public getFiltredCins(): Observable<string[]> {
        return this.filteredCins;
      }
  
}





