
import { Component, OnInit, Inject,Input, ViewChild, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {MarketingService} from '../marketing.service';
import {Mannequin} from './mannequin.model';
import {DocumentMannequin} from './document-mannequin.model';
import { DocumentMannequinElement } from './document-mannequin.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogAddDocumentMannequinComponent } from './popups/dialog-add-document-mannequin.component';
import { DialogEditDocumentMannequinComponent } from './popups/dialog-edit-document-mannequin.component';
@Component({
    selector: 'app-documents-mannequins-grid',
    templateUrl: 'documents-mannequins-grid.component.html',
  })
  export class DocumentsMannequinsGridComponent implements OnInit, OnChanges {
     private listDocumentsMannequins: DocumentMannequin[] = [];
     private  dataDocumentsMannequins: MatTableDataSource<DocumentMannequinElement>;
     private selection = new SelectionModel<DocumentMannequinElement>(true, []);
    
     @Input()
     private mannequin : Mannequin 

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private marketingService: MarketingService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.marketingService.getDocumentsMannequinsByCodeMannequin(this.mannequin.code).subscribe((listDocumentsMannequins) => {
        this.listDocumentsMannequins = listDocumentsMannequins;
  // console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
  }
  ngOnChanges()
  {
    this.specifyListColumnsToBeDisplayed();
    
    this.marketingService.getDocumentsMannequinsByCodeMannequin(this.mannequin.code).subscribe((listDocumentsMannequins) => {
      this.listDocumentsMannequins = listDocumentsMannequins;
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }

  checkOneActionInvoked() {
    const listSelectedDocumentsMannequins: DocumentMannequin[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentLelement) => {
    return documentLelement.code;
  });
  this.listDocumentsMannequins.forEach(documentLO => {
    if (codes.findIndex(code => code === documentLO.code) > -1) {
      listSelectedDocumentsMannequins.push(documentLO);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsMannequins !== undefined) {

      this.listDocumentsMannequins.forEach(docL => {
             listElements.push( {code: docL.code ,
          dateReception: docL.dateReception,
          numeroDocument: docL.numeroDocument,
          typeDocument: docL.typeDocument,
          description: docL
          .description
        } );
      });
    }
      this.dataDocumentsMannequins = new MatTableDataSource<DocumentMannequinElement>(listElements);
      this.dataDocumentsMannequins.sort = this.sort;
      this.dataDocumentsMannequins.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    //  console.log('calling specifyListColumnsToBeDisplayed documents grid');
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
    
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'},
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
      console.log(this.columnsNames);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsMannequins.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsMannequins.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsMannequins.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsMannequins.paginator) {
      this.dataDocumentsMannequins.paginator.firstPage();
    }
  }
  showAddDocumentDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeMannequin: this.mannequin.code
   };
 
   const dialogRef = this.dialog.open(DialogAddDocumentMannequinComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(docAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditDocumentDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeDocumentMannequin : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditDocumentMannequinComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(docAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Document Séléctionnée!');
    }
    }
    supprimerDocuments()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(doc, index) {
              this.marketingService.deleteDocumentMannequin(doc.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Document Séléctionnée!');
        }
    }
    refresh() {
      this.marketingService.getDocumentsMannequinsByCodeMannequin(this.mannequin.code).subscribe((listDocumentsMannequins) => {
        this.listDocumentsMannequins = listDocumentsMannequins;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
