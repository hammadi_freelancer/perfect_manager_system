
import { BaseEntity } from '../../common/base-entity.model' ;

type TypeDocument = 
'Cheque' | 'Traite' |'CIN' |'AttestationTravail'
| 'CertificatNaissance' |
'FichePaie' | 'Autre';

export class DocumentMannequin extends BaseEntity {
    constructor(
        public code?: string,
         public codeMannequin?: string,
         public numeroDocument?: string,
        public dateReceptionObject?: Date,
        public dateReception?: string,
        public typeDocument?: TypeDocument,
        public fileExtension?: string,
        public imageFace1?: any,
        public imageFace2?: any,
     ) {
         super();
        
       }
  
}
