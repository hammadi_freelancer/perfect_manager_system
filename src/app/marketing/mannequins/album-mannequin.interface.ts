
export interface AlbumMannequinElement {
     code: string ;
     codeMannequin: string ;
    dateCreationObject: Date ;
    dateCreation: string ;
    titre: string ;
    classe: string ;
    imprime: boolean ;
    etat: string ;
}
