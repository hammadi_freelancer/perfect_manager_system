import { Component, OnInit, Output,ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {Mannequin} from './mannequin.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {MarketingService} from '../marketing.service';
import { MannequinElement } from './mannequin.interface';
import { MannequinDataFilter } from './mannequin-data.filter';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {SelectItem} from 'primeng/api';

// import { DialogAddDocumentCreditComponent } from './dialog-add-document-credit.component';
@Component({
  selector: 'app-mannequins-grid',
  templateUrl: './mannequins-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class MannequinsGridComponent implements OnInit {

  private mannequin: Mannequin =  new Mannequin();
  private mannequinDataFilter: MannequinDataFilter = new MannequinDataFilter();
  
  private selection = new SelectionModel<MannequinElement>(true, []);
  private columnsToSelect : SelectItem[];
   private  selectedColumnsDefinitions: string[];
  private selectedColumnsNames: string[];
  
@Output()
select: EventEmitter<Mannequin[]> = new EventEmitter<Mannequin[]>();

//private lastClientAdded: Client;

 private selectedMannequin: Mannequin ;
private   showFilter = false;
private showSelectColumnsMultiSelect = false;

 @Input()
 private listMannequins: Mannequin[] = [] ;

 private deleteEnCours = false;
 private checkedAll: false;
 private  dataMannequinsSource: MatTableDataSource<MannequinElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay :ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
 private etatsMannequin: any[] = [
  {value: 'Initial', viewValue: 'Initiale'},
  {value: 'Valide', viewValue: 'Validé'},
  {value: 'Collaborateur', viewValue: 'Collaborateur'},
  {value: 'Annule', viewValue: 'Annulé'}
];
  constructor( private route: ActivatedRoute, private marketingService: MarketingService,
    private router: Router, private matSnackBar: MatSnackBar ) {
  }
  ngOnInit() {
    this.marketingService.getPageMannequins(0, 5, null).subscribe((mannequins) => {
      this.listMannequins = mannequins;
      // this.listClients[this.listClients.length] = this.lastClientAdded;
      // this.listClientsOrgin = this.listClients ;
      this.defaultColumnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
      {name: 'nom' , displayTitle: 'Nom'},
      {name: 'prenom' , displayTitle: 'Prénom'},
      {name: 'cin' , displayTitle: 'CIN'},
       {name: 'dateNaissance' , displayTitle: 'Date Naissance'},
       {name: 'email' , displayTitle: 'Email'},
       {name: 'mobile' , displayTitle: 'Mobile'},
       {name: 'adresse' , displayTitle: 'Adresse'},
       {name: 'experienced' , displayTitle: 'Expériencé'},
       {name: 'experiences' , displayTitle: 'Expériences'},
       {name: 'shoes' , displayTitle: 'Chaussures'},
       {name: 'taille' , displayTitle: 'Taille(CM)'},
       {name: 'eyesColor' , displayTitle: 'Couleur Yeux'},
       {name: 'skinColor' , displayTitle: 'Couleur'},
       {name: 'hairColor' , displayTitle: 'Couleur Cheveux'},
       {name: 'disponibilite' , displayTitle: 'Disponibilité'},
       { name : 'etat' , displayTitle : 'Etat'},
       { name : 'description' , displayTitle : 'Déscription'},
   
        ];
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  /*  this.columnsToSelect = [
      {label: 'N°Journal', value: {id: 1, name: 'numeroJournal', displayTitle: 'N°Journal'}},
      {label: 'Date Journal', value: {id: 2, name: 'dateJournal', displayTitle: 'Date Journal'}},
      {label: 'Valeur Ventes', value: {id: 3, name: 'montantVentes', displayTitle: 'Valeur Ventes'}},
      {label: 'Valeur Achats', value: {id: 4, name: 'montantAchats', displayTitle: 'Valeur Achats'}},
      {label: 'Valeur R.Humaines', value: {id: 5, name: 'montantRH', displayTitle: 'Valeur R.Humaines'}},
      {label: 'Valeur Charges', value: {id: 6, name: 'montantCharges', displayTitle: 'Valeur Charges'} },
      {label: 'Déscription', value: {id: 7, name: 'description', displayTitle: 'Déscription'} },
      {label: 'Etat', value: {id: 8, name: 'etat', displayTitle: 'Etat'} },
      

  ];*/

      this.columnsToSelect = [
      {label: 'Code', value: 'code', title: 'Code'},
      {label: 'Nom', value: 'nom', title: 'Nom'},
      {label: 'Prénom', value: 'prenom', title: 'Prénom'},
      {label: 'CIN', value: 'cin', title: 'CIN'},
      {label: 'Date Naissance', value: 'dateNaissance', title: 'Date Naissance' },
      {label: 'Expériencé', value: 'experienced', title: 'Expériencé' },
      {label: 'Expériences', value: 'experiences', title: 'Expériences' },
      {label: 'Déscription', value: 'description', title: 'Déscription' },
      {label: 'Disponibilité', value: 'disponibilite', title: 'Disponibilité' },
      {label: 'Email', value: 'email', title: 'Email'},
      {label: 'Mobile', value: 'mobile', title: 'Mobile'},
      {label: 'Taille', value:  'taille', title: 'Taille' },
      {label: 'Chaussures', value: 'shoes', title: 'Chaussures' },
      {label: 'Couleur Yeux', value: 'eyesColor', title: 'Couleur Yeux' },
      {label: 'Couleur', value: 'skinColor', title: 'Couleur' },
      {label: 'Couleur Cheveux', value: 'hairColor', title: 'Couleur Cheveux' },
      {label: 'Etat', value:  'etat', title: 'Etat' }
      

  ];
  });
   /* this.creditsService.getCredits().subscribe((credits) => {
      console.log('list credits ', credits);
      // this.listCredits = credits;
      this.listCredits =  this.listCredits.map((credit) => {
        if (credit.client === 'undefined' || credit.client === null ) {
           credit.client = new Client();
         }
         return credit;
   });      this.listCreditsOrgin = this.listCredits ;
     });*/
    // this.listClientsOrgin = this.listClients ;

  }
  deleteMannequin(mannequin) {
     //  console.log('call delete !', credit );
    this.marketingService.deleteMannequin(mannequin.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData( null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.marketingService.getPageMannequins(0, 5 , filter).subscribe((mannequins) => {
    this.listMannequins = mannequins;
    this.transformDataToByConsumedByMatTable();
   });
  // this.listClientsOrgin = this.listClients ;
}


checkOneActionInvoked() {
  const listSelectedMannequins: Mannequin[] = [];
console.log('selected clients are ');
console.log(this.selection.selected);
if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((mannequinElement) => {
  return mannequinElement.code;
});
this.listMannequins.forEach(mannequin => {
  if (codes.findIndex(code => code === mannequin.code) > -1) {
    listSelectedMannequins.push(mannequin);
  }
  });
}
this.select.emit(listSelectedMannequins);

}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  this.dataMannequinsSource= null;
  if (this.listMannequins !== undefined) {
   //  console.log('diffe of undefined');
    this.listMannequins.forEach(mannequin => {
           listElements.push(
             {
               code: mannequin.code ,
            nom: mannequin.nom,
            prenom: mannequin.prenom ,
            cin: mannequin.cin ,
            dateNaissance: mannequin.dateNaissance ,
            mobile: mannequin.mobile,
            email: mannequin.email,
            adresse: mannequin.adresse,
            taille: mannequin.taille,
            shoes: mannequin.shoes,
            eyesColor: mannequin.eyesColor,
            skinColor: mannequin.skinColor,
            hairColor: mannequin.hairColor,
            experienced: mannequin.experienced,
            experiences: mannequin.experiences,
            disponibilite: mannequin.disponibilite,
            
            description: mannequin.description,
   etat: mannequin.etat === 'Valide' ?  'Validé' : mannequin.etat === 'Annule' ? 'Annulé' :
   mannequin.etat === 'Initial' ? 'Initiale' : 'Collaborateur'

      } );
    });
  }
    this.dataMannequinsSource = new MatTableDataSource<MannequinElement>(listElements);
    this.dataMannequinsSource.sort = this.sort;
    this.dataMannequinsSource.paginator = this.paginator;
    this.marketingService.getTotalMannequins(null).subscribe((response) => {
    //  console.log('Total credits is ', response);
      this.dataMannequinsSource.paginator.length = response.totalMannequins;
    });
}
 specifyListColumnsToBeDisplayed() {
 
  console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
  this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
?
   this.defaultColumnsDefinitions : [];
   this.selectedColumnsNames = [];
   
   if (this.selectedColumnsDefinitions !== undefined)
    {
      this.selectedColumnsDefinitions.forEach((colName) => { 
           const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
           const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
           const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
           this.columnsDefinitionsToDisplay.push(colDef);
          });
    }
   this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
  this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
             return colDef.displayTitle;
   });
   this.columnsNames = [];
   this.columnsNames[0] = 'select';
   this.columnsDefinitionsToDisplay .map((colDef) => {
    this.columnsNames.push(colDef.name);
   if (this.selectedColumnsDefinitions !== undefined) {
    this.selectedColumnsNames.push(colDef.displayTitle);
   }
});
   this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
    return colDef.displayTitleAr;
});
}
masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataMannequinsSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataMannequinsSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataMannequinsSource.filter = filterValue.trim().toLowerCase();
  if (this.dataMannequinsSource.paginator) {
    this.dataMannequinsSource.paginator.firstPage();
  }
}

loadAddMannequinComponent() {
  this.router.navigate(['/pms/marketing/ajouterMannequin']) ;

}
loadEditMannequinComponent() {
  this.router.navigate(['/pms/marketing/editerMannequin', this.selection.selected[ this.selection.selected.length - 1].code]) ;

}
supprimerMannequins()
{
 
  if ( this.selection.selected.length !== 0) {
    this.deleteEnCours = true;
      this.selection.selected.forEach(function(mannequin, index) {
          this.marketingService.deleteMannequin(mannequin.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.deleteEnCours = false;
            
            this.openSnackBar( ' Mannequin(s) Supprimé(s)');
             this.loadData(null);
            }
      }, this);
    } else {
      this.openSnackBar('Aucun Mannequin Séléctionné !');
    }
}
refresh() {
  this.loadData(null);
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 
 toggleFilterPanel()
 {
    this.showFilter = !this.showFilter;
 }
 toggleShowColumnsMultiSelect()
 {
   this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
 }
 loadFiltredData()
 {
   console.log('the filter is ', this.mannequinDataFilter);
   this.loadData(this.mannequinDataFilter);
 }

changePage($event) {
  console.log('page event is ', $event);
 this.marketingService.getPageMannequins($event.pageIndex, $event.pageSize, this.mannequinDataFilter).subscribe((mannequins) => {
    this.listMannequins = mannequins;
   // this.transformDataToByConsumedByMatTable();
   const listElements = [];
     console.log('diffe of undefined');
     this.listMannequins.forEach(mannequin => {
      listElements.push( {
        code: mannequin.code ,
        nom: mannequin.nom,
        prenom: mannequin.prenom ,
        cin: mannequin.cin ,
        dateNaissance: mannequin.dateNaissance ,
        mobile: mannequin.mobile,
        email: mannequin.email,
        adresse: mannequin.adresse,
        taille: mannequin.taille,
        shoes: mannequin.shoes,
        eyesColor: mannequin.eyesColor,
        skinColor: mannequin.skinColor,
        hairColor: mannequin.hairColor,
        experienced: mannequin.experienced,
        experiences: mannequin.experiences,
        disponibilite: mannequin.disponibilite,
        
        description: mannequin.description,
etat: mannequin.etat === 'Valide' ?  'Validé' : mannequin.etat === 'Annule' ? 'Annulé' :
mannequin.etat === 'Initial' ? 'Initiale' : 'Collaborateur'

      }
);

});
this.dataMannequinsSource = new MatTableDataSource<MannequinElement>(listElements);
this.marketingService.getTotalMannequins(null).subscribe((response) => {
 console.log('Total credits is ', response);
  this.dataMannequinsSource.paginator.length = response.totalMannequins;
});
});
}
}
