
import { BaseEntity } from '../../common/base-entity.model' ;

type Etat = 'Initial' | 'Valide' | 'Annule' | 'EnCoursValidation' | 'Ouvert' | 'Cloture'

export class Mannequin extends BaseEntity {
    constructor(
        public code?: string,
        public nom?: string,
        public prenom?: string,
        public cin?: string,
        public email?: string,
        public mobile?: string,
        public shoes?: string,
        public taille?: string,
        public eyesColor?: string,
        public skinColor?: string,
        public hairColor?: string,
        public albums?: any[],
        public profileImage1?: any,
        public profileImage2?: any,
        public adresse?: string,
        public experienced?: boolean,
        public experiences?: string,
        public disponibilite?: string,
        public dateNaissanceObject?: Date,
        public dateNaissance?: string,
         public etat?: Etat,
    // public listTranches?: Tranche[]
     ) {
         super();
      
    }
}
