import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {Mannequin} from './mannequin.model';

import {MarketingService} from '../marketing.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {MatSnackBar} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';


@Component({
  selector: 'app-mannequin-new',
  templateUrl: './mannequin-new.component.html',
  // styleUrls: ['./players.component.css']
})
export class MannequinNewComponent implements OnInit {

 private mannequin: Mannequin =  new Mannequin();
 private saveEnCours = false;
 private etatsMannequin: any[] = [
  {value: 'Initial', viewValue: 'Initiale'},
  {value: 'Valide', viewValue: 'Validé'},
  {value: 'Collaborateur', viewValue: 'Collaborateur'},
  {value: 'Annule', viewValue: 'Annulé'}
];
  constructor( private route: ActivatedRoute,
    private router: Router, private marketingService: MarketingService,
   private matSnackBar: MatSnackBar) {
  }

  ngOnInit() {
  
  }
  saveMannequin() {


    // this.saveEnCours = true;
    this.saveEnCours = true;
    this.marketingService.addMannequin(this.mannequin).subscribe((response) => {
      this.saveEnCours = false;
      
        if (response.error_message ===  undefined) {
          console.log('undefined error message');
          // this.save.emit(response);
          this.mannequin =  new Mannequin();
          this.openSnackBar(  'Mannequin Enregistré');
        } else {
         // show error message
         this.openSnackBar(  'Erreurs: Opération Echouée');
         
        }
    });
  }


  vider() {
    this.mannequin = new Mannequin();
  }
  openSnackBar(messageToDisplay) {
   this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
       verticalPosition: 'top'});
  }
loadGridMannequins() {
  this.router.navigate(['/pms/marketing/mannequins']) ;
}
fileChangedImageProfile1($event) {
  const file = $event.target.files[0];
  const fileReader = new FileReader();
  fileReader.onload = (e) => {
    console.log(fileReader.result);
     this.mannequin.profileImage1 = fileReader.result;
    // this.filesData[indexFile] = fileData;
  };
  fileReader.readAsDataURL(file);
 }
 fileChangedImageProfile2($event) {
  const file = $event.target.files[0];
  const fileReader = new FileReader();
  fileReader.onload = (e) => {
    console.log(fileReader.result);
     this.mannequin.profileImage2 = fileReader.result;
    // this.filesData[indexFile] = fileData;
  };
  fileReader.readAsDataURL(file);
 }
}
