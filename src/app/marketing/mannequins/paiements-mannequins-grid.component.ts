
import { Component, OnInit, Inject, ViewChild, Input, AfterViewChecked, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {MarketingService} from '../marketing.service';
import { Mannequin } from './mannequin.model';
import {PaiementMannequin} from './paiement-mannequin.model';
import { PaiementMannequinElement } from './paiement-mannequin.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
 import { DialogAddPaiementMannequinComponent} from './popups/dialog-add-paiement-mannequin.component';
 import { DialogEditPaiementMannequinComponent } from './popups/dialog-edit-paiement-mannequin.component';
@Component({
    selector: 'app-paiements-mannequins-grid',
    templateUrl: 'paiements-mannequins-grid.component.html',
  })
  export class PaiementsMannequinsGridComponent implements OnInit, OnChanges {
     private listPaiementsMannequins: PaiementMannequin[] = [];
     private  dataPaiementsMannequinsSource: MatTableDataSource<PaiementMannequinElement>;
     private selection = new SelectionModel<PaiementMannequinElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];

     @Input()
     private mannequin: Mannequin;

     constructor(  private marketingService: MarketingService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      // console.log('credit code after dipslay tab ', this.credit.code);
     //  console.log('the code releve achats is',this.releveAchats.code);
      this.marketingService.getPaiementsMannequinsByCodeMannequin(this.mannequin.code).subscribe((listPaiementsMannequins) => {
        this.listPaiementsMannequins= listPaiementsMannequins;
// console.log('recieving data from backend', this.listPayementsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }

  ngOnChanges() {
    console.log('the code releve achats is in ngOnChanges', this.mannequin.code);
    this.marketingService.getPaiementsMannequinsByCodeMannequin(this.mannequin.code).subscribe((listPayementsMannequins) => {
      this.listPaiementsMannequins = listPayementsMannequins;
// console.log('recieving data from backend', this.listPayementsCredits  );
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }
  checkOneActionInvoked() {
    const listSelectedPaiementsMannequins: PaiementMannequin[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((payementMElement) => {
    return payementMElement.code;
  });
  this.listPaiementsMannequins.forEach(payementM => {
    if (codes.findIndex(code => code === payementM.code) > -1) {
      listSelectedPaiementsMannequins.push(payementM);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listPaiementsMannequins !== undefined) {

      this.listPaiementsMannequins.forEach(paimentMannequin => {
             listElements.push( {code: paimentMannequin.code ,
          dateOperation: paimentMannequin.dateOperation,
          montantPaye: paimentMannequin.montantPaye,
          modePayement: paimentMannequin.modePayement,
          numeroCheque: paimentMannequin.numeroCheque,
          dateCheque: paimentMannequin.dateCheque,
          compte: paimentMannequin.compte,
          compteAdversaire: paimentMannequin.compteAdversaire,
          banque: paimentMannequin.banque,
          banqueAdversaire: paimentMannequin.banqueAdversaire,
          description: paimentMannequin.description
        } );
      });
    }
      this.dataPaiementsMannequinsSource= new MatTableDataSource<PaiementMannequinElement>(listElements);
      this.dataPaiementsMannequinsSource.sort = this.sort;
      this.dataPaiementsMannequinsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'}, 
     {name: 'montantPaye' , displayTitle: 'Montant Payé'},
     {name: 'modePayement' , displayTitle: 'Mode Payement'},
      {name: 'numeroCheque' , displayTitle: 'N°Chèque'},
     {name: 'dateCheque' , displayTitle: 'Date Chèque'}, 
     {name: 'compte' , displayTitle: 'Compte'},
     {name: 'compteAdversaire' , displayTitle: 'Compte Adv.'},
      {name: 'banque' , displayTitle: 'Banque'},
       { name : 'banqueAdversaire' , displayTitle : 'Banque Adv.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataPaiementsMannequinsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataPaiementsMannequinsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataPaiementsMannequinsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataPaiementsMannequinsSource.paginator) {
      this.dataPaiementsMannequinsSource.paginator.firstPage();
    }
  }

  showAddPayementDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeMannequin : this.mannequin.code
   };
 
   const dialogRef = this.dialog.open(DialogAddPaiementMannequinComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(payAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditPayementDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codePaiementMannequin: this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditPaiementMannequinComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(payAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Paiement Séléctionnée!');
    }
    }
    supprimerPayements()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(pay, index) {
              this.marketingService.deletePaiementMannequin(pay.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( ' Paiement(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Paiement Séléctionnée!');
        }
    }
    refresh() {
      this.marketingService.getPaiementsMannequinsByCodeMannequin(this.mannequin.code).subscribe((listPaiementsMannequins) => {
        this.listPaiementsMannequins= listPaiementsMannequins;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
















}
