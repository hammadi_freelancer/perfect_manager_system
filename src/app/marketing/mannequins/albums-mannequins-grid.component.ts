
import { Component, OnInit, Inject, ViewChild, Input, AfterViewChecked, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {MarketingService} from '../marketing.service';
import { Mannequin } from './mannequin.model';
import {AlbumMannequin} from './album-mannequin.model';
import { AlbumMannequinElement } from './album-mannequin.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
 import { DialogAddAlbumMannequinComponent} from './popups/dialog-add-album-mannequin.component';
 import { DialogEditAlbumMannequinComponent } from './popups/dialog-edit-album-mannequin.component';
@Component({
    selector: 'app-albums-mannequins-grid',
    templateUrl: 'albums-mannequins-grid.component.html',
  })
  export class AlbumsMannequinsGridComponent implements OnInit, OnChanges {
     private listAlbumsMannequins: AlbumMannequin[] = [];
     private  dataAlbumsMannequinsSource: MatTableDataSource<AlbumMannequinElement>;
     private selection = new SelectionModel<AlbumMannequinElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];

     @Input()
     private mannequin: Mannequin;

     constructor(  private marketingService: MarketingService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      // console.log('credit code after dipslay tab ', this.credit.code);
     //  console.log('the code releve achats is',this.releveAchats.code);
      this.marketingService.getAlbumsMannequinsByCodeMannequin(this.mannequin.code).
      subscribe((listAlbumsMannequins) => {
        this.listAlbumsMannequins= listAlbumsMannequins;
// console.log('recieving data from backend', this.listPayementsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }

  ngOnChanges() {
    console.log('the code releve achats is in ngOnChanges', this.mannequin.code);
    this.marketingService.getAlbumsMannequinsByCodeMannequin(this.mannequin.code).subscribe((listAlbumsMannequins) => {
      this.listAlbumsMannequins = listAlbumsMannequins;
// console.log('recieving data from backend', this.listPayementsCredits  );
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }
  checkOneActionInvoked() {
    const listSelectedAlbumsMannequins: AlbumMannequin[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((albumMElement) => {
    return albumMElement.code;
  });
  this.listAlbumsMannequins.forEach(albumM => {
    if (codes.findIndex(code => code === albumM.code) > -1) {
      listSelectedAlbumsMannequins.push(albumM);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listAlbumsMannequins !== undefined) {

      this.listAlbumsMannequins.forEach(albumMannequin => {
             listElements.push( {
               code: albumMannequin.code ,
               dateCreation: albumMannequin.dateCreation,
          titre: albumMannequin.titre,
          description: albumMannequin.description,
          classe: albumMannequin.classe,
          imprime: albumMannequin.imprime,

        } );
      });
    }
      this.dataAlbumsMannequinsSource= new MatTableDataSource<AlbumMannequinElement>(listElements);
      this.dataAlbumsMannequinsSource.sort = this.sort;
      this.dataAlbumsMannequinsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateCreation' , displayTitle: 'Date Création'}, 
     {name: 'titre' , displayTitle: 'Titre'},
     {name: 'description' , displayTitle: 'Déscription'},
      {name: 'classe' , displayTitle: 'Classe'},
     {name: 'imprime' , displayTitle: 'Imprimé'}
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataAlbumsMannequinsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataAlbumsMannequinsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataAlbumsMannequinsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataAlbumsMannequinsSource.paginator) {
      this.dataAlbumsMannequinsSource.paginator.firstPage();
    }
  }

  showAddAlbumMannequinDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeMannequin : this.mannequin.code
   };
 
   const dialogRef = this.dialog.open(DialogAddAlbumMannequinComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(payAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditAlbumMannequinDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeAlbumMannequin: this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditAlbumMannequinComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(albumAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucu Album Séléctionnée!');
    }
    }
    supprimerAlbums()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(album, index) {
              this.marketingService.deleteAlbumMannequin(album.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( ' Album(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Album Séléctionnée!');
        }
    }
    refresh() {
      this.marketingService.getAlbumsMannequinsByCodeMannequin(this.mannequin.code).subscribe((listAlbumsMannequins) => {
        this.listAlbumsMannequins= listAlbumsMannequins;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
















}
