
import { BaseEntity } from '../../common/base-entity.model' ;

type Etat = 'Initial' | 'Valide' | 'Annule' 
type ModePayement = 'Espece' | 'Cheque' | 'Virement'

export class AlbumMannequin extends BaseEntity {
    constructor(
        public code?: string,
         public codeMannequin?: string,
        public dateCreationObject?: Date,
        public dateCreation?: string,
        public titre?: string,
        public classe?: string,
        public imprime?: boolean,
        public photo1?: any,
        public photo2?: any,
        public photo3?: any,
        public photo4?: any,
        public photo5?: any,
        public photo6?: any,
        public etat?: Etat,
        
        
     ) {
         super();
        
       }
  
}
