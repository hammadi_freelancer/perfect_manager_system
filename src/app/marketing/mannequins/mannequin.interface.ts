export interface MannequinElement {
         code: string ;
         nom: string;
         prenom: string ;
         cin: string;
         email: string;
         mobile: string;
         shoes: string;
         taille: string;
         eyesColor: string;
         skinColor: string;
         hairColor: string;
         adresse: string;
         dateNaissance: string;
         experienced: string;
         experiences: string;
         disponibilite: string;
         
         etat: string;
        }
