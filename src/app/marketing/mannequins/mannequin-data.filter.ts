export class MannequinDataFilter {
    code: string;
    dateNaissanceFromObject: Date ;
    dateNaissanceToObject: Date;
    nom: string;
    prenom: string;
    cin: string;
    adresse: string;
    email: string;
    mobile: string;
    eyesColor: string;
    skinColor: string;
    hairColor: string;
    shoes: string;
    taille: string;
    disponibilite: boolean;
    experimented: boolean;
    experiences: string;
    description: string;
    etat: string;
   }
