
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {MarketingService} from '../../marketing.service';
import {DocumentMannequin} from '../document-mannequin.model';
import { DocumentMannequinElement } from '../document-mannequin.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-documents-mannequins',
    templateUrl: 'dialog-list-documents-mannequins.component.html',
  })
  export class DialogListDocumentsMannequinsComponent implements OnInit {
     private listDocumentsMannequins: DocumentMannequin[] = [];
     private  dataDocumentsMannequinsSource: MatTableDataSource<DocumentMannequinElement>;
     private selection = new SelectionModel<DocumentMannequinElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private marketingService: MarketingService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      this.marketingService.getDocumentsMannequinsByCodeMannequin(this.data.codeMannequin).subscribe((listDocuments) => {
        this.listDocumentsMannequins = listDocuments;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }

  checkOneActionInvoked() {
    const listSelectedDocumentsMannequins: DocumentMannequin[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentJelement) => {
    return documentJelement.code;
  });
  this.listDocumentsMannequins.forEach(documentMannequin=> {
    if (codes.findIndex(code => code === documentMannequin.code) > -1) {
      listSelectedDocumentsMannequins.push(documentMannequin);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsMannequins !== undefined) {

      this.listDocumentsMannequins.forEach(docJ => {
             listElements.push( {code: docJ.code ,
          dateReception: docJ.dateReception,
          numeroDocument: docJ.numeroDocument,
          typeDocument: docJ.typeDocument,
          description: docJ.description
        } );
      });
    }
      this.dataDocumentsMannequinsSource = new MatTableDataSource<DocumentMannequinElement>(listElements);
      this.dataDocumentsMannequinsSource.sort = this.sort;
      this.dataDocumentsMannequinsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    //  console.log('calling specifyListColumnsToBeDisplayed documents grid');
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
    
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
      console.log(this.columnsNames);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsMannequinsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsMannequinsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsMannequinsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsMannequinsSource.paginator) {
      this.dataDocumentsMannequinsSource.paginator.firstPage();
    }
  }
 




}
