
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {MarketingService} from '../../marketing.service';
import {AlbumMannequin} from '../album-mannequin.model';

@Component({
    selector: 'app-dialog-add-album-mannequin',
    templateUrl: 'dialog-add-album-mannequin.component.html',
  })
  export class DialogAddAlbumMannequinComponent implements OnInit {
     private albumMannequin: AlbumMannequin;
     private saveEnCours = false;
     private etatsAlbum: any[] = [
      {value: 'Initial', viewValue: 'Initiale'},
      {value: 'Annule', viewValue: 'Annulé'},
      {value: 'Valide', viewValue: 'Validé'},
   
    ];
     constructor(  private marketingService: MarketingService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.albumMannequin = new AlbumMannequin();
    }
    saveAlbumMannequin() 
    {
      this.albumMannequin.codeMannequin= this.data.codeMannequin;
        this.saveEnCours = true;
       this.marketingService.addAlbumMannequin(this.albumMannequin).subscribe((response) => {
         this.saveEnCours = false;
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.albumMannequin = new AlbumMannequin();
             this.openSnackBar(  'Album Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs: Opération Echouée');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
     imagePhoto1Changed($event) {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.albumMannequin.photo1 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imagePhoto2Changed($event) {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.albumMannequin.photo2 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imagePhoto3Changed($event) {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.albumMannequin.photo3 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }

     imagePhoto4Changed($event) {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.albumMannequin.photo4 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imagePhoto5Changed($event) {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.albumMannequin.photo5 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imagePhoto6Changed($event) {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.albumMannequin.photo6 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
  }
