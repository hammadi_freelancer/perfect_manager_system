
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {MarketingService} from '../../marketing.service';
import {PaiementMannequin} from '../paiement-mannequin.model';
import { PaiementMannequinElement } from '../paiement-mannequin.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-paiements-mannequins',
    templateUrl: 'dialog-list-paiements-mannequins.component.html',
  })
  export class DialogListPaiementsMannequinsComponent implements OnInit {
     private listPaiementsMannequins: PaiementMannequin[] = [];
     private  dataPaiementsMannequinsSource: MatTableDataSource<PaiementMannequinElement>;
     private selection = new SelectionModel<PaiementMannequinElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private marketingService: MarketingService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      this.marketingService.getPaiementsMannequinsByCodeMannequin(this.data.codeMannequin).subscribe((listPaiements) => {
        this.listPaiementsMannequins= listPaiements;
// console.log('recieving data from backend', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedPaiementsMannequins: PaiementMannequin[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((ligneJlement) => {
    return ligneJlement.code;
  });
  this.listPaiementsMannequins.forEach(lJour => {
    if (codes.findIndex(code => code === lJour.code) > -1) {
      listSelectedPaiementsMannequins.push(lJour);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listPaiementsMannequins !== undefined) {

      this.listPaiementsMannequins.forEach(paiementMannequin => {
             listElements.push( {code: paiementMannequin.code ,
          dateOperation: paiementMannequin.dateOperation,
          montantPaye: paiementMannequin.montantPaye,
          modePayement: paiementMannequin.modePayement,
          numeroCheque: paiementMannequin.numeroCheque,
          dateCheque: paiementMannequin.dateCheque,
          compte: paiementMannequin.compte,
          compteAdversaire: paiementMannequin.compteAdversaire,
          banque: paiementMannequin.banque,
          banqueAdversaire: paiementMannequin.banqueAdversaire,
          description: paiementMannequin.description
        } );
      });
    }
      this.dataPaiementsMannequinsSource = new MatTableDataSource<PaiementMannequinElement>(listElements);
      this.dataPaiementsMannequinsSource.sort = this.sort;
      this.dataPaiementsMannequinsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [
       {name: 'code' , displayTitle: 'Code'},
     {name: 'dateOperation' , displayTitle: 'Date Opération'}, 
     {name: 'montantPaye' , displayTitle: 'Montant Payé'},
     {name: 'modePayement' , displayTitle: 'Mode Payement'},
      {name: 'numeroCheque' , displayTitle: 'N°Chèque'},
     {name: 'dateCheque' , displayTitle: 'Date Chèque'}, 
     {name: 'compte' , displayTitle: 'Compte'},
     {name: 'compteAdversaire' , displayTitle: 'Compte Adv.'},
      {name: 'banque' , displayTitle: 'Banque'},
       { name : 'banqueAdversaire' , displayTitle : 'Banque Adv.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataPaiementsMannequinsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataPaiementsMannequinsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataPaiementsMannequinsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataPaiementsMannequinsSource.paginator) {
      this.dataPaiementsMannequinsSource.paginator.firstPage();
    }
  }

}
