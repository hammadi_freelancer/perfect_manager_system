
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {MarketingService} from '../../marketing.service';
import {PaiementMannequin} from '../paiement-mannequin.model';

@Component({
    selector: 'app-dialog-edit-paiement-mannequin',
    templateUrl: 'dialog-edit-paiement-mannequin.component.html',
  })
  export class DialogEditPaiementMannequinComponent implements OnInit {
     private paiementMannequin: PaiementMannequin;
     private updateEnCours = false;
    private etats: any[] = [
      {value: 'Annule', viewValue: 'Annulé'},
      {value: 'Initial', viewValue: 'Initial'},
      {value: 'Valide', viewValue: 'Validé'}
    ];

   
     constructor(  private marketingService: MarketingService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit() {

        this.marketingService.getPaiementMannequin(this.data.codePaiementMannequin).subscribe((paiementMannequin) => {
            this.paiementMannequin = paiementMannequin;
            if (this.paiementMannequin.etat === 'Valide' || this.paiementMannequin.etat === 'Annule') {
                this.etats.splice(1, 1);
              }
              if (this.paiementMannequin.etat === 'Initial') {
                  this.etats.splice(0, 1);
                }
     });
    }
    updatePaiementMannequin()  {
      
      this.updateEnCours = true;
       this.marketingService.updatePaiementMannequin(this.paiementMannequin).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
          if (this.paiementMannequin.etat === 'Valide' || this.paiementMannequin.etat === 'Annule' ) {
             //  this.tabsDisabled = false;
              this.etats = [
                {value: 'Annule', viewValue: 'Annulé'},
                {value: 'Valide', viewValue: 'Validé'}
      
              ];
            }
         
             // this.save.emit(response);
             this.openSnackBar(  'Paiement Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }


  }
