
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {MarketingService} from '../../marketing.service';
import {DocumentMannequin} from '../document-mannequin.model';

@Component({
    selector: 'app-dialog-add-document-mannequin',
    templateUrl: 'dialog-add-document-mannequin.component.html',
  })
  export class DialogAddDocumentMannequinComponent implements OnInit {
     private documentMannequin: DocumentMannequin;
     private saveEnCours = false;
     private typesDoc: any[] = [
      {value: 'CIN', viewValue: 'Carte Identité'},
      {value: 'AttestationTravail', viewValue: 'Attestation Travail'},
      {value: 'CertificatNaissance', viewValue: 'Cértificat Naissance'},
      {value: 'FichePaie', viewValue: 'Fiche Paie'},
      {value: 'Autre', viewValue: 'Autre'},
    ];
     constructor(  private marketingService: MarketingService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.documentMannequin = new DocumentMannequin();
    }
    saveDocumentMannequin() 
    {
      this.documentMannequin.codeMannequin= this.data.codeMannequin;
        this.saveEnCours = true;
       this.marketingService.addDocumentMannequin(this.documentMannequin).subscribe((response) => {
         this.saveEnCours = false;
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.documentMannequin = new DocumentMannequin();
             this.openSnackBar(  'Document Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs: Opération Echouée');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
     imageFace1Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentMannequin.imageFace1 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imageFace2Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentMannequin.imageFace2 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
  }
