
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {MarketingService} from '../../marketing.service';
import {AlbumMannequin} from '../album-mannequin.model';

@Component({
    selector: 'app-dialog-edit-album-mannequin',
    templateUrl: 'dialog-edit-album-mannequin.component.html',
  })
  export class DialogEditAlbumMannequinComponent implements OnInit {
     private albumMannequin: AlbumMannequin;
     private updateEnCours = false;
     private etatsAlbum: any[] = [
      {value: 'Initial', viewValue: 'Initiale'},
      {value: 'Annule', viewValue: 'Annulé'},
      {value: 'Valide', viewValue: 'Validé'},
   
    ]
     constructor(  private marketingService: MarketingService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {

        this.marketingService.getAlbumMannequin(this.data.codeAlbumMannequin).subscribe((album) => {
            this.albumMannequin = album;
     });
    }
    
    updateAlbumMannequin() 
    {
      this.updateEnCours = true;
       this.marketingService.updateAlbumMannequin(this.albumMannequin).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.openSnackBar(  'Album Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
  
     imagePhoto1Changed($event) {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.albumMannequin.photo1 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imagePhoto2Changed($event) {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.albumMannequin.photo2 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imagePhoto3Changed($event) {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.albumMannequin.photo3 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }

     imagePhoto4Changed($event) {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.albumMannequin.photo4 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imagePhoto5Changed($event) {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.albumMannequin.photo5 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imagePhoto6Changed($event) {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.albumMannequin.photo6 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
 
 
 
 
 
 
 
    }
