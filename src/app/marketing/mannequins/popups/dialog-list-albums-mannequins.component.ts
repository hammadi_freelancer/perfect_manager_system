
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {MarketingService} from '../../marketing.service';
import {AlbumMannequin} from '../album-mannequin.model';
import { AlbumMannequinElement } from '../album-mannequin.interface';
import { ColumnDefinition } from '../../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-albums-mannequins',
    templateUrl: 'dialog-list-albums-mannequins.component.html',
  })
  export class DialogListAlbumsMannequinsComponent implements OnInit {
     private listAlbumsMannequins: AlbumMannequin[] = [];
     private  dataAlbumsMannequinsSource: MatTableDataSource<AlbumMannequinElement>;
     private selection = new SelectionModel<AlbumMannequinElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private marketingService: MarketingService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      this.marketingService.getAlbumsMannequinsByCodeMannequin(this.data.codeMannequin).subscribe((listAlbums) => {
        this.listAlbumsMannequins = listAlbums;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }

  checkOneActionInvoked() {
    const listSelectedAlbumsMannequins: AlbumMannequin[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((alBelement) => {
    return alBelement.code;
  });
  this.listAlbumsMannequins.forEach(albumMannequin=> {
    if (codes.findIndex(code => code === albumMannequin.code) > -1) {
      listSelectedAlbumsMannequins.push(albumMannequin);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listAlbumsMannequins !== undefined) {

      this.listAlbumsMannequins.forEach(alMannequin => {
             listElements.push( 
               {
              code: alMannequin.code ,
          dateCreation: alMannequin.dateCreation,
          titre: alMannequin.titre,
          description: alMannequin.description,
          classe: alMannequin.classe,
          imprime: alMannequin.imprime,
        } );
      });
    }
      this.dataAlbumsMannequinsSource = new MatTableDataSource<AlbumMannequinElement>(listElements);
      this.dataAlbumsMannequinsSource.sort = this.sort;
      this.dataAlbumsMannequinsSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    //  console.log('calling specifyListColumnsToBeDisplayed documents grid');
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
    
     this.columnsDefinitions = [
       {name: 'code' , displayTitle: 'Code'},
     {name: 'dateCreation' , displayTitle: 'Date Création'},
     {name: 'titre' , displayTitle: 'Titre'},
     {name: 'description' , displayTitle: 'Déscription'},
      { name : 'classe' , displayTitle : 'Classe'},
      { name : 'imprime' , displayTitle : 'Imprimé'}
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
      console.log(this.columnsNames);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataAlbumsMannequinsSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataAlbumsMannequinsSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataAlbumsMannequinsSource.filter = filterValue.trim().toLowerCase();
    if (this.dataAlbumsMannequinsSource.paginator) {
      this.dataAlbumsMannequinsSource.paginator.firstPage();
    }
  }
 




}
