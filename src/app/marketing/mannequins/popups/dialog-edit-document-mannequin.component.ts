
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {MarketingService} from '../../marketing.service';
import {DocumentMannequin} from '../document-mannequin.model';

@Component({
    selector: 'app-dialog-edit-document-mannequin',
    templateUrl: 'dialog-edit-document-mannequin.component.html',
  })
  export class DialogEditDocumentMannequinComponent implements OnInit {
     private documentMannequin: DocumentMannequin;
     private updateEnCours = false;
     private typesDoc: any[] = [
      {value: 'CIN', viewValue: 'Carte Identité'},
      {value: 'AttestationTravail', viewValue: 'Attestation Travail'},
      {value: 'CertificatNaissance', viewValue: 'Cértificat Naissance'},
      {value: 'FichePaie', viewValue: 'Fiche Paie'},
      {value: 'Autre', viewValue: 'Autre'},
    ];
     constructor(  private marketingService: MarketingService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {

        this.marketingService.getDocumentMannequin(this.data.codeDocumentMannequin).subscribe((doc) => {
            this.documentMannequin = doc;
     });
    }
    
    updateDocumentMannequin() 
    {
      this.updateEnCours = true;
       this.marketingService.updateDocumentMannequin(this.documentMannequin).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.openSnackBar(  'Document Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
     imageFace1Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentMannequin.imageFace1 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imageFace2Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentMannequin.imageFace2 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
  }
