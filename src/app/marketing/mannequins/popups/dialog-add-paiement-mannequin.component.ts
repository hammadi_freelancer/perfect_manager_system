
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {MarketingService} from '../../marketing.service';
import {PaiementMannequin} from '../paiement-mannequin.model';

@Component({
    selector: 'app-dialog-add-paiement-mannequin',
    templateUrl: 'dialog-add-paiement-mannequin.component.html',
  })
  export class DialogAddPaiementMannequinComponent implements OnInit {
     private paiementMannequin: PaiementMannequin;
     private saveEnCours = false;
     private etats: any[] = [
      {value: 'Initial', viewValue: 'Initiale'},
      {value: 'Valide', viewValue: 'Validé'},
      {value: 'Annule', viewValue: 'Annulé'}
        ];
    /* private typesOperations: any[] = [
      {value: 'AchatsMatieresPrimaires', viewValue: 'Achats Matières Primaires'},
      {value: 'AchatsMarchandises', viewValue: 'Achats Marchandises'},
      {value: 'AchatsFournituresBureau', viewValue: 'Achats Fournitures Bureau'},
      {value: 'AchatsMaterielles', viewValue: 'Achats Materielles'},
      {value: 'AutresAchats', viewValue: 'Autres Achats'},
      {value: 'Electricite', viewValue: 'Electricité'},
      {value: 'EauPotable', viewValue: 'Eau Potable'},
      {value: 'ConsommationTelephonique', viewValue: 'Consommation Téléphonique'},
      {value: 'Transport', viewValue: 'Transport'},
      {value: 'Location', viewValue: 'Locations'},
      {value: 'Impots', viewValue: 'Impots'},
      {value: 'AutresCharges', viewValue: 'Autres Charges'},
      {value: 'VentesMarchandises', viewValue: 'Ventes Marchandises'},
      {value: 'VentesProduitsFinis', viewValue: 'Ventes Produits Finis'},
      {value: 'VentesMatieresPrimaires', viewValue: 'Ventes Matieres Primaires'},
      {value: 'VentesProduitsSemiFinis', viewValue: 'Ventes Produits Semi Finis'},
      {value: 'VentesMateriellesEpuisées', viewValue: 'Ventes Materielles Epuisées'},
      {value: 'VentesLocaux', viewValue: 'Ventes Locaux'},
      {value: 'VentesProduitsNonConformes', viewValue: 'Ventes Produits Non Conformes'},
      {value: 'AutresVentes', viewValue: 'Autres Ventes'},

      {value: 'PaieSalaires', viewValue: 'Paie Salaires'},
      {value: 'PaieHoraires', viewValue: 'Paie Horaires'},
      {value: 'PaieCNSS', viewValue: 'Paie CNSS'},
      {value: 'PaiePrimes', viewValue: 'Paie Primes'},
      {value: 'PaieAvances', viewValue: 'Paie Avances'},
      {value: 'PaieRecompenses', viewValue: 'Paie Récompenses'},
      {value: 'PaieRecrutements', viewValue: 'Paie Recrutements'},
      {value: 'AutresPaies', viewValue: 'Autres Paies'},
    ];*/
   




     constructor(  private marketingService: MarketingService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.paiementMannequin = new PaiementMannequin ();
    }
    savePaiementMannequin() 
    {
      this.paiementMannequin.codeMannequin = this.data.codeMannequin;
       this.saveEnCours = true;
       this.marketingService.addPaiementMannequin(this.paiementMannequin).subscribe((response) => {
         this.saveEnCours = false;
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.paiementMannequin = new PaiementMannequin();
             this.openSnackBar(  'Paiement Enregistrée');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
 
  }
