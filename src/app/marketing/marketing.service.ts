import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';

import {Promotion} from './promotions/promotion.model' ;
import {Mannequin} from './mannequins/mannequin.model' ;
import {AlbumMannequin} from './mannequins/album-mannequin.model' ;

import {CompagnePublicitaire} from './compagnes-publicitaires/compagne-publicitaire.model' ;
import {PaiementMannequin} from './mannequins/paiement-mannequin.model' ;
import {DocumentMannequin} from './mannequins/document-mannequin.model' ;

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';

const URL_GET_TOTAL_PROMOTIONS = 'http://localhost:8082/marketing/promotions/getTotalPromotions';

const  URL_GET_LIST_PROMOTIONS = 'http://localhost:8082/marketing/promotions/getPromotions';
const  URL_GET_PAGE_PROMOTIONS = 'http://localhost:8082/marketing/promotions/getPagePromotions';

const URL_ADD_PROMOTION = 'http://localhost:8082/marketing/promotions/addPromotion';
const URL_GET_PROMOTION = 'http://localhost:8082/marketing/promotions/getPromotion';

const URL_UPDATE_PROMOTION = 'http://localhost:8082/marketing/promotions/updatePromotion';
const URL_DELETE_PROMOTION = 'http://localhost:8082/marketing/promotions/deletePromotion';


const URL_GET_TOTAL_MANNEQUINS = 'http://localhost:8082/marketing/mannequins/getTotalMannequins';

const  URL_GET_LIST_MANNEQUINS = 'http://localhost:8082/marketing/mannequins/getMannequins';
const  URL_GET_PAGE_MANNEQUINS = 'http://localhost:8082/marketing/mannequins/getPageMannequins';

const URL_ADD_MANNEQUIN = 'http://localhost:8082/marketing/mannequins/addMannequin';
const URL_GET_MANNEQUIN = 'http://localhost:8082/marketing/mannequins/getMannequin';

const URL_UPDATE_MANNEQUIN= 'http://localhost:8082/marketing/mannequins/updateMannequin';
const URL_DELETE_MANNEQUIN= 'http://localhost:8082/marketing/mannequins/deleteMannequin';




const URL_GET_TOTAL_CAMPAGNES_PUBLICITAIRES = 'http://localhost:8082/marketing/compagnesPublicitaires/getTotalCompagnesPublicitaires';

const  URL_GET_LIST_CAMPAGNES_PUBLICITAIRES = 'http://localhost:8082/marketing/compagnesPublicitaires/getCompagnesPublicitaires';
const  URL_GET_PAGE_CAMPAGNES_PUBLICITAIRES = 'http://localhost:8082/marketing/compagnesPublicitaires/getPageCompagnesPublicitaires';

const URL_ADD_CAMPAGNE_PUBLICITAIRE = 'http://localhost:8082/marketing/compagnesPublicitaires/addCompagnePublicitaire';
const URL_GET_CAMPAGNE_PUBLICITAIRE = 'http://localhost:8082/marketing/getCompagnePublicitaire';

const URL_UPDATE_CAMPAGNE_PUBLICITAIRE = 'http://localhost:8082/marketing/compagnesPublicitaires/updateCompagnePublicitaire';
const URL_DELETE_CAMPAGNE_PUBLICITAIRE = 'http://localhost:8082/marketing/compagnesPublicitaires/deleteCompagnePublicitaire';


const URL_GET_TOTAL_PAIEMENTS_MANNEQUINS = 'http://localhost:8082/marketing/paiementsMannequins/getTotalPaiementsMannequins';

const  URL_GET_LIST_PAIEMENTS_MANNEQUINS = 'http://localhost:8082/marketing/paiementsMannequins/getPaiementsMannequins';
const  URL_GET_PAGE_PAIEMENTS_MANNEQUINS = 'http://localhost:8082/marketing/paiementsMannequins/getPagePaiementsMannequins';

const  URL_GET_PAIEMENTS_MANNEQUINS_BY_CODE_MANNEQUIN = 
'http://localhost:8082/marketing/paiementsMannequins/getPaiementsMannequinsByCodeMannequin';

const  URL_GET_DOCUMENTS_MANNEQUINS_BY_CODE_MANNEQUIN =
'http://localhost:8082/marketing/documentsMannequins/getDocumentsMannequinsByCodeMannequin';

const  URL_GET_ALBUMS_MANNEQUINS_BY_CODE_MANNEQUIN =
'http://localhost:8082/marketing/albumsMannequins/getAlbumsMannequinsByCodeMannequin';

const URL_ADD_PAIEMENT_MANNEQUIN = 'http://localhost:8082/marketing/paiementsMannequins/addPaiementMannequin';
const URL_GET_PAIEMENT_MANNEQUIN = 'http://localhost:8082/marketing/paiementsMannequins/getPaiementMannequin';

const URL_UPDATE_PAIEMENT_MANNEQUIN = 'http://localhost:8082/marketing/updateMannequin';
const URL_DELETE_PAIEMENT_MANNEQUIN = 'http://localhost:8082/marketing/deleteMannequin';

const URL_GET_TOTAL_DOCUMENTS_MANNEQUINS = 'http://localhost:8082/marketing/documentsMannequins/getTotalDocumentsMannequins';

const  URL_GET_LIST_DOCUMENTS_MANNEQUINS = 'http://localhost:8082/marketing/documentsMannequins/getDocumentsMannequins';
const  URL_GET_PAGE_DOCUMENTS_MANNEQUINS = 'http://localhost:8082/marketing/documentsMannequins/getPageDocumentsMannequins';

const URL_ADD_DOCUMENT_MANNEQUIN = 'http://localhost:8082/marketing/documentsMannequins/addDocumentMannequin';
const URL_GET_DOCUMENT_MANNEQUIN = 'http://localhost:8082/marketing/documentsMannequins/getDocumentMannequin';

const URL_UPDATE_DOCUMENT_MANNEQUIN = 'http://localhost:8082/marketing/documentsMannequins/updateDocumentMannequin';
const URL_DELETE_DOCUMENT_MANNEQUIN = 'http://localhost:8082/marketing/documentsMannequins/deleteDocumentMannequin';


const URL_GET_TOTAL_ALBUMS_MANNEQUINS = 'http://localhost:8082/marketing/albumsMannequins/getTotalAlbumsMannequins';

const  URL_GET_LIST_ALBUMS_MANNEQUINS = 'http://localhost:8082/marketing/albumsMannequins/getAlbumsMannequins';
const  URL_GET_PAGE_ALBUMS_MANNEQUINS = 'http://localhost:8082/marketing/albumsMannequins/getPageAlbumsMannequins';

const URL_ADD_ALBUM_MANNEQUIN = 'http://localhost:8082/marketing/albumsMannequins/addAlbumMannequin';
const URL_GET_ALBUM_MANNEQUIN = 'http://localhost:8082/marketing/albumsMannequins/getAlbumMannequin';

const URL_UPDATE_ALBUM_MANNEQUIN = 'http://localhost:8082/marketing/albumsMannequins/updateAlbumMannequin';
const URL_DELETE_ALBUM_MANNEQUIN = 'http://localhost:8082/marketing/albumsMannequins/deleteAlbumMannequin';



@Injectable({
  providedIn: 'root'
})
export class MarketingService {
 
  constructor(private httpClient: HttpClient) {

   }

   addPromotion(promotion: Promotion): Observable<any> {
    return this.httpClient.post<Promotion>(URL_ADD_PROMOTION, promotion);
  }
  
  getPromotions(codePromotion): Observable<Promotion> {
    return this.httpClient.get<Promotion>(URL_GET_PROMOTION + '/' + codePromotion);
  }
   updatePromotion(promotion: Promotion): Observable<any> {
    return this.httpClient.put<Promotion>(URL_UPDATE_PROMOTION, promotion);
  }
  deletePromotion(codePromotion): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_PROMOTION + '/' + codePromotion);
  }
    getPagePromotions(pageNumber, nbElementsPerPage, filter): Observable<Promotion[]> {
      const filterStr = JSON.stringify(filter);
      const params = new HttpParams()
      .set('pageNumber', pageNumber)
      .set('nbElementsPerPage', nbElementsPerPage)
      .set('sort', 'name')
      .set('filter', filterStr);
      return this.httpClient
      .get<Promotion[]>(URL_GET_PAGE_PROMOTIONS, {params});
     }
     getTotalPromotions(filter): Observable<any> {
      
          const filterStr = JSON.stringify(filter);
          const params = new HttpParams()
          .set('filter', filterStr);
          return this.httpClient
          .get<any>(URL_GET_TOTAL_PROMOTIONS, {params});
      }

      addCompagnePublicitaire(compagnePublicitaire: CompagnePublicitaire): Observable<any> {
        return this.httpClient.post<CompagnePublicitaire>(URL_ADD_CAMPAGNE_PUBLICITAIRE, compagnePublicitaire);
      }
      
      getCompagnePublicitaire(codeCompagnePublicitaire): Observable<CompagnePublicitaire> {
        return this.httpClient.get<CompagnePublicitaire>(URL_GET_CAMPAGNE_PUBLICITAIRE + '/' + codeCompagnePublicitaire);
      }
       updateCompagnePublicitaire(compagnePublicitaire: CompagnePublicitaire): Observable<any> {
        return this.httpClient.put<CompagnePublicitaire>(URL_UPDATE_CAMPAGNE_PUBLICITAIRE, compagnePublicitaire);
      }
      deleteCompagnePublicitaire(codeCompagnePublicitaire): Observable<any> {
        return this.httpClient.delete<any>(URL_DELETE_CAMPAGNE_PUBLICITAIRE + '/' + codeCompagnePublicitaire);
      }
        getPageCompagnesPublicitaires(pageNumber, nbElementsPerPage, filter): Observable<CompagnePublicitaire[]> {
          const filterStr = JSON.stringify(filter);
          const params = new HttpParams()
          .set('pageNumber', pageNumber)
          .set('nbElementsPerPage', nbElementsPerPage)
          .set('sort', 'name')
          .set('filter', filterStr);
          return this.httpClient
          .get<CompagnePublicitaire[]>(URL_GET_PAGE_CAMPAGNES_PUBLICITAIRES, {params});
         }
         getTotalCompagnesPublicitaires(filter): Observable<any> {
          
              const filterStr = JSON.stringify(filter);
              const params = new HttpParams()
              .set('filter', filterStr);
              return this.httpClient
              .get<any>(URL_GET_TOTAL_CAMPAGNES_PUBLICITAIRES, {params});
          }

          addMannequin(mannequin: Mannequin): Observable<any> {
            return this.httpClient.post<Mannequin>(URL_ADD_MANNEQUIN, mannequin);
          }
          
          getMannequins(codeMannequin): Observable<Mannequin> {
            return this.httpClient.get<Mannequin>(URL_GET_MANNEQUIN + '/' + codeMannequin);
          }
           updateMannequin(mannequin: Mannequin): Observable<any> {
            return this.httpClient.put<Mannequin>(URL_UPDATE_MANNEQUIN, mannequin);
          }
          deleteMannequin(codeMannequin): Observable<any> {
            return this.httpClient.delete<any>(URL_DELETE_MANNEQUIN + '/' + codeMannequin);
          }

          getPageMannequins(pageNumber, nbElementsPerPage, filter): Observable<Mannequin[]> {
            const filterStr = JSON.stringify(filter);
            const params = new HttpParams()
            .set('pageNumber', pageNumber)
            .set('nbElementsPerPage', nbElementsPerPage)
            .set('sort', 'name')
            .set('filter', filterStr);
            return this.httpClient
            .get<Mannequin[]>(URL_GET_PAGE_MANNEQUINS, {params});
           }
           getTotalMannequins(filter): Observable<any> {
            
                const filterStr = JSON.stringify(filter);
                const params = new HttpParams()
                .set('filter', filterStr);
                return this.httpClient
                .get<any>(URL_GET_TOTAL_MANNEQUINS, {params});
            }

            addPaiementMannequin(paiementMannequin: PaiementMannequin): Observable<any> {
              return this.httpClient.post<PaiementMannequin>(URL_ADD_PAIEMENT_MANNEQUIN, paiementMannequin);
            }
            getPaiementMannequin(codePaiementMannequin): Observable<PaiementMannequin> {
              return this.httpClient.get<PaiementMannequin>(URL_GET_PAIEMENT_MANNEQUIN + '/' + codePaiementMannequin);
            }
             updatePaiementMannequin(paiementMannequin: PaiementMannequin): Observable<any> {
              return this.httpClient.put<PaiementMannequin>(URL_UPDATE_PAIEMENT_MANNEQUIN, paiementMannequin);
            }
            deletePaiementMannequin(codePaiementMannequin): Observable<any> {
              return this.httpClient.delete<any>(URL_DELETE_PAIEMENT_MANNEQUIN+ '/' + codePaiementMannequin);
            }
  
            getPagePaiementsMannequins(pageNumber, nbElementsPerPage, filter): Observable<PaiementMannequin[]> {
              const filterStr = JSON.stringify(filter);
              const params = new HttpParams()
              .set('pageNumber', pageNumber)
              .set('nbElementsPerPage', nbElementsPerPage)
              .set('filter', filterStr);
              return this.httpClient
              .get<PaiementMannequin[]>(URL_GET_PAGE_PAIEMENTS_MANNEQUINS, {params});
             }
             getPaiementsMannequins( filter): Observable<PaiementMannequin[]> {
              const filterStr = JSON.stringify(filter);
              const params = new HttpParams()
              .set('filter', filterStr);
              return this.httpClient
              .get<PaiementMannequin[]>(URL_GET_PAGE_PAIEMENTS_MANNEQUINS, {params});
             }
             getTotalPaiementsMannequins(filter): Observable<any> {
              
                  const filterStr = JSON.stringify(filter);
                  const params = new HttpParams()
                  .set('filter', filterStr);
                  return this.httpClient
                  .get<any>(URL_GET_TOTAL_PAIEMENTS_MANNEQUINS, {params});
              }

              addDocumentMannequin(documentMannequin: DocumentMannequin): Observable<any> {
                return this.httpClient.post<DocumentMannequin>(URL_ADD_DOCUMENT_MANNEQUIN, documentMannequin);
              }
              getDocumentMannequin(codeDocumentMannequin): Observable<DocumentMannequin> {
                return this.httpClient.get<DocumentMannequin>(URL_GET_DOCUMENT_MANNEQUIN + '/' + codeDocumentMannequin);
              }
               updateDocumentMannequin(documentMannequin: DocumentMannequin): Observable<any> {
                return this.httpClient.put<DocumentMannequin>(URL_UPDATE_DOCUMENT_MANNEQUIN, documentMannequin);
              }
              deleteDocumentMannequin(codeDocumentMannequin): Observable<any> {
                return this.httpClient.delete<any>(URL_DELETE_DOCUMENT_MANNEQUIN+ '/' + codeDocumentMannequin);
              }
    
              getPageDocumentsMannequins(pageNumber, nbElementsPerPage, filter): Observable<DocumentMannequin[]> {
                const filterStr = JSON.stringify(filter);
                const params = new HttpParams()
                .set('pageNumber', pageNumber)
                .set('nbElementsPerPage', nbElementsPerPage)
                .set('filter', filterStr);
                return this.httpClient
                .get<DocumentMannequin[]>(URL_GET_PAGE_DOCUMENTS_MANNEQUINS, {params});
               }
               getDocumentsMannequins( filter): Observable<DocumentMannequin[]> {
                const filterStr = JSON.stringify(filter);
                const params = new HttpParams()
                .set('filter', filterStr);
                return this.httpClient
                .get<DocumentMannequin[]>(URL_GET_LIST_DOCUMENTS_MANNEQUINS, {params});
               }
               getTotalDocumentsMannequins(filter): Observable<any> {
                
                    const filterStr = JSON.stringify(filter);
                    const params = new HttpParams()
                    .set('filter', filterStr);
                    return this.httpClient
                    .
                    get<any>(URL_GET_TOTAL_DOCUMENTS_MANNEQUINS, {params});
                }
                 getDocumentsMannequinsByCodeMannequin(codeMannequin): Observable<DocumentMannequin[]> {
                  return this.httpClient.get<DocumentMannequin[]>(
                    URL_GET_DOCUMENTS_MANNEQUINS_BY_CODE_MANNEQUIN +
                     '/' + codeMannequin);
                }
                getPaiementsMannequinsByCodeMannequin(codeMannequin): Observable<PaiementMannequin[]> {
                  return this.httpClient.get<PaiementMannequin[]>(
                    URL_GET_PAIEMENTS_MANNEQUINS_BY_CODE_MANNEQUIN +
                     '/' + codeMannequin);
                }

                getAlbumsMannequinsByCodeMannequin(codeMannequin): Observable<AlbumMannequin[]> {
                  return this.httpClient.get<AlbumMannequin[]>(
                    URL_GET_ALBUMS_MANNEQUINS_BY_CODE_MANNEQUIN +
                     '/' + codeMannequin);
                }
                addAlbumMannequin(albumtMannequin: AlbumMannequin): Observable<any> {
                  return this.httpClient.post<AlbumMannequin>(URL_ADD_ALBUM_MANNEQUIN, albumtMannequin);
                }
                getAlbumMannequin(codeAlbumMannequin): Observable<AlbumMannequin> {
                  return this.httpClient.get<AlbumMannequin>(URL_GET_ALBUM_MANNEQUIN + '/' + codeAlbumMannequin);
                }
                 updateAlbumMannequin(albumMannequin: AlbumMannequin): Observable<any> {
                  return this.httpClient.put<AlbumMannequin>(URL_UPDATE_ALBUM_MANNEQUIN, albumMannequin);
                }
                deleteAlbumMannequin(codeAlbumMannequin): Observable<any> {
                  return this.httpClient.delete<any>(URL_DELETE_ALBUM_MANNEQUIN+ '/' + codeAlbumMannequin);
                }
      
                getPageAlbumsMannequins(pageNumber, nbElementsPerPage, filter): Observable<AlbumMannequin[]> {
                  const filterStr = JSON.stringify(filter);
                  const params = new HttpParams()
                  .set('pageNumber', pageNumber)
                  .set('nbElementsPerPage', nbElementsPerPage)
                  .set('filter', filterStr);
                  return this.httpClient
                  .get<AlbumMannequin[]>(URL_GET_PAGE_ALBUMS_MANNEQUINS, {params});
                 }
                 getAlbumsMannequins( filter): Observable<AlbumMannequin[]> {
                  const filterStr = JSON.stringify(filter);
                  const params = new HttpParams()
                  .set('filter', filterStr);
                  return this.httpClient
                  .get<AlbumMannequin[]>(URL_GET_LIST_ALBUMS_MANNEQUINS, {params});
                 }
                 getTotalAlbumsMannequins(filter): Observable<any> {
                  
                      const filterStr = JSON.stringify(filter);
                      const params = new HttpParams()
                      .set('filter', filterStr);
                      return this.httpClient
                      .
                      get<any>(URL_GET_TOTAL_ALBUMS_MANNEQUINS, {params});
                  }
                
             
}
