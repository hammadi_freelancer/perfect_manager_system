import { Component, OnInit, Input, EventEmitter, Output,
    AfterViewChecked
    } from '@angular/core';
    // import { ArticleService } from './article.service';
    import {Promotion} from '../promotions/promotion.model';
    import {MarketingService} from '../marketing.service';
    import {FormControl} from '@angular/forms';
    import {map, startWith} from 'rxjs/operators';
    import {Observable} from 'rxjs';
    import { Router, ActivatedRoute, ParamMap } from '@angular/router';
    import {MatSnackBar} from '@angular/material';
    import {ArticleFilter} from '../../stocks/articles/article.filter';
    import {Article} from '../../stocks/articles/article.model';
    
   //  import { SelectValues } from './select-values.interface';
    @Component({
      selector: 'app-promotion-new',
      templateUrl: './promotion-new.component.html',
      // styleUrls: ['./players.component.css']
    })
export class PromotionNewComponent implements OnInit {
    private promotion: Promotion = new Promotion();
    private articleFilter: ArticleFilter;
    private etatsPromotion: any[] = [
      {value: 'Initial', viewValue: 'Initial'},
      {value: 'Planifie', viewValue: 'Planifié'},
      {value: 'Valide', viewValue: 'Validé'},
      {value: 'Termine', viewValue: 'Terminé'}
    ];

    private listArticles: Article[];

      constructor( private route: ActivatedRoute,
        private router: Router ,
         private marketingService: MarketingService,
         private matSnackBar: MatSnackBar ) {

      }
      ngOnInit() {
        this.listArticles = this.route.snapshot.data.articles;
        
        //  this.factureVenteFilter = new FactureVenteFilter(this.listClients, []);
        this.articleFilter = new ArticleFilter(this.route.snapshot.data.articles,
         [], [] );
      }
    savePromotion() {
      
        this.marketingService.addPromotion(this.promotion).subscribe((response) => {
            if (response.error_message ===  undefined) {
              // this.save.emit(response);
              this.promotion = new Promotion();
              this.openSnackBar('Promotion Enregistré');
            } else {
             // show error message
             this.openSnackBar(  'Erreurs!');
             
            }
        });
      }
    
    loadGridPromotions() {
      this.router.navigate(['/pms/marketing/promotions']) ;
    }
    
       openSnackBar(messageToDisplay) {
        this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
            verticalPosition: 'top'});
       }
       fillDataLibelleArticle()  {
        
                this.listArticles.forEach((article, index) => {
                     if (this.promotion.article.code === article.code) {
                      this.promotion.article.designation = article.designation;
                      this.promotion.article.libelle = article.libelle;
                      
                      }
        
                } , this);
       }
       fileChangedAffiche1($event) {
        const file = $event.target.files[0];
        const fileReader = new FileReader();
        fileReader.onload = (e) => {
          console.log(fileReader.result);
           this.promotion.affiche1 = fileReader.result;
          // this.filesData[indexFile] = fileData;
        };
        fileReader.readAsDataURL(file);
       }
       fileChangedAffiche2($event) {
        const file = $event.target.files[0];
        const fileReader = new FileReader();
        fileReader.onload = (e) => {
          console.log(fileReader.result);
           this.promotion.affiche2 = fileReader.result;
          // this.filesData[indexFile] = fileData;
        };
        fileReader.readAsDataURL(file);
       }
    }
