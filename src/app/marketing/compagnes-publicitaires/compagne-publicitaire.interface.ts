
export interface CompagnePublicitaireElement {
        code: string ;
        description: string;
        dateCompagne: string;
        periodeFrom : string;
        periodeTo : string;
        codeArticle : string;
        libelleArticle : string;
        pourcentageRemise : number;
        prix : number;
        ancienPrix : number;
        etat : string;
       }
