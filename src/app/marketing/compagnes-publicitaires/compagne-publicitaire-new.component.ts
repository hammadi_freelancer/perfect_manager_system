import { Component, OnInit, Input, EventEmitter, Output,
  AfterViewChecked
  } from '@angular/core';
  // import { ArticleService } from './article.service';
  import {CompagnePublicitaire} from './compagne-publicitaire.model';
  import {MarketingService} from '../marketing.service';
  import {FormControl} from '@angular/forms';
  import {map, startWith} from 'rxjs/operators';
  import {Observable} from 'rxjs';
  import { Router, ActivatedRoute, ParamMap } from '@angular/router';
  import {MatSnackBar} from '@angular/material';
 //  import { SelectValues } from './select-values.interface';
  @Component({
    selector: 'app-compagne-publicitaire-new',
    templateUrl: './compagne-publicitaire-new.component.html',
    // styleUrls: ['./players.component.css']
  })
export class CompagnePublicitaireNewComponent implements OnInit {
  private compagnePublicitaire: CompagnePublicitaire = new CompagnePublicitaire();


    constructor( private route: ActivatedRoute,
      private router: Router ,
       private marketingService: MarketingService,
       private matSnackBar: MatSnackBar ) {
    }
    ngOnInit() {
   
    }
  saveCompagnePublicitaire() {
    
      this.marketingService.addCompagnePublicitaire(this.compagnePublicitaire).subscribe((response) => {
          if (response.error_message ===  undefined) {
            // this.save.emit(response);
            this.compagnePublicitaire = new CompagnePublicitaire();
            this.openSnackBar('Compagne Publicitaire Enregistré');
          } else {
           // show error message
           this.openSnackBar(  'Erreurs!');
           
          }
      });
    }
  
  loadGridCompagnesPublicitaires() {
    this.router.navigate(['/pms/marketing/compagnesPublicitaires']) ;
  }
  
     openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }

  }
