
// import { BaseEntity } from '../../common/base-entity.model' ;
import { Article } from '../../stocks/articles/article.model';

export class CompagnePublicitaire {
   constructor(
     public code ?: string,
     public description ?: string,
     public dateCompagnePublicitaireObject ?: Date,
     public dateCompagnePublicitaire ?: string,
     public periodeFromObject ?: Date,
     public periodeFrom ?: string,
     public periodeToObject ?: Date,
     public periodeTo ?: string,
     public article ?: Article,
    //  public mannequin? : Mannequin,
     public affiche1?: any,
     public affiche2?: any,
     public affiche3?: any,
     public affiche4?: any,
     public pourcentageRemise?: number,
     public prix?: number,
     public etat?: string,
     public userLastUpdatorCode?: string,
     public dateLastUpdate?: string

) {
    // super();
      // this.image = undefined;
}
}
