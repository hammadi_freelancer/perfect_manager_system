import { Component, OnInit, Output, ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
// import {AccessRight} from './access-right.model';
import {CompagnePublicitaire} from './compagne-publicitaire.model';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {MarketingService} from '../marketing.service';
import { CompagnePublicitaireElement } from './compagne-publicitaire.interface';

import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
// import { DialogAddDocumentClientComponent} from './popups';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {CompagnePublicitaireDataFilter } from './compagne-publicitaire-data.filter';
import { SelectItem } from 'primeng/api';
@Component({
  selector: 'app-compagnes-publicitaires-grid',
  templateUrl: './compagnes-publicitaires-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class CompagnesPublicitairesGridComponent implements OnInit {

  private compagnePublicitaire: CompagnePublicitaire =  new CompagnePublicitaire();
  private showFilter = false;
  private showSelectColumnsMultiSelect = false;
  private compagnePublicitaireDataFilter = new CompagnePublicitaireDataFilter();
  private selection = new SelectionModel<CompagnePublicitaire>(true, []);

  @Input()
  private listCompagnesPublicitaires: any = [] ;


 selectedCompagnePublicitaire: CompagnePublicitaire ;

private deleteEnCours = false;

 private  dataCompagnesPublicitairesSource: MatTableDataSource<CompagnePublicitaireElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay :ColumnDefinition[] = [];

 private columnsToSelect : SelectItem[];
  private  selectedColumnsDefinitions: string[];

 private selectedColumnsNames: string[];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];

  constructor( private route: ActivatedRoute, private marketingService: MarketingService,
    private router: Router, private matSnackBar: MatSnackBar, private dialog: MatDialog ) {
  }
  ngOnInit() {

    this.defaultColumnsDefinitions = [
      {name: 'code' , displayTitle: 'Code'},
     {name: 'description' , displayTitle: 'Déscription'},
     {name: 'dateCompagnePublicitaire' , displayTitle: 'Date'},
     {name: 'periodeFrom' , displayTitle: 'Période De:'},
     {name: 'periodeTo' , displayTitle: 'à:'},
     {name: 'codeArticle' , displayTitle: 'Code Art.'},
     {name: 'libelleArticle' , displayTitle: 'Libéllé Art.'},
     {name: 'prix' , displayTitle: 'Prix Vente'},
     {name: 'pourcentageRemise' , displayTitle: '%Remise'}
  ];
    this.columnsToSelect = [
      {label: 'Code', value: 'code', title: 'Code'},
      {label: 'Déscription', value: 'description', title: 'Déscription'},
      {label: 'Date', value: 'dateCompagnePublicitaire', title: 'Date'},
      {label: 'Période De:', value: 'periodeFrom', title: 'Période De:'},
      {label: 'à', value: 'periodeTo', title: 'à'},
      {label: 'Code Art.', value: 'codeArticle', title: 'Code Art.'},
      {label: 'Libéllé Art.', value: 'libelleArticle', title: 'Libéllé Art.'},
      {label: 'Prix', value: 'prix', title: 'Prix'},
      {label: '%Remise', value: 'pourcentageRemise', title: '%Remise'}
  ];
  this.selectedColumnsDefinitions = [
    'code',
   'description',
   'dateCompagnePublicitaire',
   'libelleArticle',
   'prix'
    ];
    this.marketingService.getPageCompagnesPublicitaires(0, 5, null).subscribe((comps) => {
      this.listCompagnesPublicitaires = comps;
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });
  }
  deleteCompagnePublicitaire(compagne) {
     // console.log('call delete !', client );
    this.marketingService.deleteCompagnePublicitaire(compagne.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData(null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.marketingService.getPageCompagnesPublicitaires(0, 5, filter).subscribe((comps) => {
    this.listCompagnesPublicitaires = comps;
    // this.listClients[this.listClients.length] = this.lastClientAdded;
    this.transformDataToByConsumedByMatTable();
   });
  // this.listClientsOrgin = this.listClients ;
}


checkOneActionInvoked() {
  const listSelectedCompagnesPublicitaires: CompagnePublicitaire[] = [];

if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((pElement) => {
  return pElement.code;
});
this.listCompagnesPublicitaires.forEach(gr => {
  if (codes.findIndex(code => code === gr.code) > -1) {
    listSelectedCompagnesPublicitaires.push(gr);
  }
  });
}
// this.select.emit(listSelectedClients);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listCompagnesPublicitaires !== undefined) {
    this.listCompagnesPublicitaires.forEach(cP => {
      listElements.push(
        {
          code: cP.code ,
          description: cP.description ,
          codeArticle: cP.article.code ,
          libelleArticle: cP.article.libelle ,
          prix: cP.prix ,
          pourcentageRemise: cP.pourcentageRemise ,
          periodeFrom: cP.periodeFrom ,
          periodeTo: cP.periodeTo ,
          dateCompagnePublicitaire: cP.dateCompagnePublicitaire 
      } );
    });
  }
    this.dataCompagnesPublicitairesSource = new MatTableDataSource<CompagnePublicitaireElement>(listElements);
    this.dataCompagnesPublicitairesSource.sort = this.sort;
    this.dataCompagnesPublicitairesSource.paginator = this.paginator;
    this.marketingService.getTotalCompagnesPublicitaires
    (this.compagnePublicitaireDataFilter).subscribe((response) => {
     //  console.log('Total Clients   is ', response);
     if (this.dataCompagnesPublicitairesSource.paginator !== undefined) {
       this.dataCompagnesPublicitairesSource.paginator.length = response['totalCompagnesPublicitaires'];
     }
     });
}

masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
    //   this.dataAccessRightsSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataCompagnesPublicitairesSource.data.length;
  return numSelected === numRows;
}
/*applyFilter(filterValue: string) {
  this.dataAccessRightsSource.filter = filterValue.trim().toLowerCase();
  if (this.dataAccessRightsSource.paginator) {
    this.dataAccessRightsSource.paginator.firstPage();
  }
}*/
loadAddCompagnePublicitaireComponent() {
  this.router.navigate(['/pms/marketing/ajouterCompagnePublicitaire']) ;

}
loadEditCompagnePublicitaireComponent() {
  if (this.selection.selected.length === 0) {
    this.openSnackBar(' Aucune Compagne Sélectionnée' );
    // this.loadData();
    } else {
  this.router.navigate(['/pms/marketing/editerCompagnePublicitaire',
  this.selection.selected[ this.selection.selected.length - 1].code]) ;
    }
}
supprimerCompagnesPublicitaires()
{
  if ( this.selection.selected.length !== 0) {
    this.deleteEnCours = true;
      this.selection.selected.forEach(function(profile, index) {
          this.profilesService.deleteProfileVentes(profile.code).subscribe((response) => {
            if (this.selection.selected.length - 1 === index) {
              this.deleteEnCours = false;
              this.openSnackBar( ' Compagne(s) Supprimé(s)');
               this.loadData(null);
              }
          });
    
      }, this);
    } else {
      this.openSnackBar('Aucun Compagne Séléctionné!');
    }
}
refresh()
{
  this.loadData(null);
  
}
  openSnackBar(messageToDisplay) {
 this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
     verticalPosition: 'top'});
}

changePage($event) {
  console.log('page event is ', $event);
 this.marketingService.getPageCompagnesPublicitaires($event.pageIndex, $event.pageSize,
  this.compagnePublicitaireDataFilter).subscribe((comps) => {
    this.listCompagnesPublicitaires= comps;
    const listElements = [];
 if (this.listCompagnesPublicitaires !== undefined) {
    this.listCompagnesPublicitaires.forEach(cP=> {
      listElements.push( {
        code: cP.code ,
        description: cP.description ,
        codeArticle: cP.article.code ,
        libelleArticle: cP.article.libelle ,
        prix: cP.prix ,
        pourcentageRemise: cP.pourcentageRemise ,
        periodeFrom: cP.periodeFrom ,
        periodeTo: cP.periodeTo ,
        dateCompagnePublicitaire: cP.dateCompagnePublicitaire 
      }
      );
    });
  }
     this.dataCompagnesPublicitairesSource= new MatTableDataSource<CompagnePublicitaireElement>(listElements);
     this.marketingService.getTotalCompagnesPublicitaires(this.compagnePublicitaireDataFilter).
     subscribe((response) => {
      console.log('Total clients is ', response);
      // this.dataClientsSource.paginator = this.paginator;
      if (this.dataCompagnesPublicitairesSource.paginator) {
        
       this.dataCompagnesPublicitairesSource.paginator.length = response.totalCompagnesPublicitaires;
      }
     });

    });
}
toggleFilterPanel() {
  this.showFilter = !this.showFilter;
}
toggleShowColumnsMultiSelect() {
 this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
}
loadFiltredData() {
 // console.log('the filter is ', this.utilisateurDataFilter);
 this.loadData(this.compagnePublicitaireDataFilter);
}

specifyListColumnsToBeDisplayed() {
  
   console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
   this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
 ?
    this.defaultColumnsDefinitions : [];
    this.selectedColumnsNames = [];
    
    if (this.selectedColumnsDefinitions !== undefined)
     {
       this.selectedColumnsDefinitions.forEach((colName) => { 
            const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
            const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
            const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
            this.columnsDefinitionsToDisplay.push(colDef);
           });
     }
    this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
   this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
              return colDef.displayTitle;
    });
    console.log('columns to disp:', this.columnsDefinitionsToDisplay);
    this.columnsNames = [];
    this.columnsNames[0] = 'select';
    this.columnsDefinitionsToDisplay .map((colDef) => {
     this.columnsNames.push(colDef.name);
    if (this.selectedColumnsDefinitions !== undefined) {
     this.selectedColumnsNames.push(colDef.displayTitle);
    }
 });
    this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
     return colDef.displayTitleAr;
 });
 }






}
