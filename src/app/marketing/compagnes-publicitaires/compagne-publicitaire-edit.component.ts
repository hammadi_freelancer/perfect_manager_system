import { Component, OnInit, Input, EventEmitter, Output, AfterContentChecked,
  AfterViewChecked
  } from '@angular/core';
  // import { ArticleService } from './article.service';
  
  import {CompagnePublicitaire } from './compagne-publicitaire.model';
  import {MarketingService } from '../marketing.service';
  
  import {FormControl} from '@angular/forms';
  import {map, startWith} from 'rxjs/operators';
  import {Observable} from 'rxjs';
  import { Router, ActivatedRoute, ParamMap } from '@angular/router';
  import {MatSnackBar} from '@angular/material';
  @Component({
    selector: 'app-compagne-publicitaire-edit',
    templateUrl: './compagne-publicitaire-edit.component.html',
    // styleUrls: ['./players.component.css']
  })
  export class CompagnePublicitaireEditComponent implements OnInit, AfterContentChecked {
  
  // private client: Client = new Client();
  
  
  private compagnePublicitaire:  CompagnePublicitaire = new  CompagnePublicitaire();
  
    constructor( private route: ActivatedRoute,
      private router: Router , 
      private marketingService: MarketingService, private matSnackBar: MatSnackBar,
     ) {
    }
    ngOnInit() {
    
      
        const codeCompagnePublicitaire = this.route.snapshot.paramMap.get('codeCompagnePublicitaire');
  
    }
  
    loadGridCompagnesPublicitaires() {
      this.router.navigate(['/pms/marketing/compagnesPublicitaires']) ;
    }
   /* supprimerUtilisateur() {
       
        this.utilisateurService.deleteUtilisateur(this.utilisateur.code).subscribe((response) => {
  
          this.openSnackBar( ' Element Supprimé');
          this.loadGridUtilisateurs() ;
              });
    }*/
  updateCompagnePublicitaire() {
  
    this.marketingService.updateCompagnePublicitaire(this.compagnePublicitaire).subscribe((response) => {
      if (response.error_message ===  undefined) {
        this.openSnackBar( ' Compagne Publicitaire mis à jour ');
      } else {
        this.openSnackBar( ' Erreurs.. ');
        
       // show error message
      }
  });
      
  }
  
   openSnackBar(messageToDisplay) {
    this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
        verticalPosition: 'top'});
   }
  
  ngAfterContentChecked() {
  
  }
  
  }
  
  
  