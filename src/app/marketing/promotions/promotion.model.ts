
// import { BaseEntity } from '../../common/base-entity.model' ;
import { Article } from '../../stocks/articles/article.model';
export class Promotion {
   constructor(
     public code ?: string,
     public description ?: string,
     public datePromotionObject ?: Date,
     public datePromotion ?: string,
     public periodeFromObject ?: Date,
     public periodeFrom ?: string,
     public periodeToObject ?: Date,
     public periodeTo ?: string,
     public article ?: Article,
     public affiche1?: any,
     public affiche2?: any,
     public pourcentageRemise?: number,
     public ancienPrix?: number,
     public nouveauPrix?: number,
     public etat?: string,
     public userLastUpdatorCode?: string,
     public dateLastUpdate?: string
) {
    // super();
      // this.image = undefined;
}
}
