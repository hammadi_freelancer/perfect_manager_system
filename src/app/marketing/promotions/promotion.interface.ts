
export interface PromotionElement {
         code: string ;
         description: string;
         datePromotion: string;
         periodeFrom : string;
         periodeTo : string;
         codeArticle : string;
         libelleArticle : string;
         pourcentageRemise : number;
         nouveauPrix : number;
         ancienPrix : number;
         etat : string;
        }
        