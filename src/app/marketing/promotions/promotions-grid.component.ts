import { Component, OnInit, Output, ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
// import {AccessRight} from './access-right.model';
import {Promotion} from './promotion.model';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {MarketingService} from '../marketing.service';
import { PromotionElement } from './promotion.interface';

// import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
// import { DialogAddDocumentClientComponent} from './popups';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {PromotionDataFilter } from './promotion-data.filter';
import { ColumnDefinition } from '../../common/column-definition.interface';

import { SelectItem } from 'primeng/api';
@Component({
  selector: 'app-promtions-grid',
  templateUrl: './promotions-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class PromotionsGridComponent implements OnInit {

  private promotion: Promotion =  new Promotion();
  private showFilter = false;
  private showSelectColumnsMultiSelect = false;
  private promotionDataFilter = new PromotionDataFilter();
  private selection = new SelectionModel<Promotion>(true, []);

  @Input()
  private listPromotions: any = [] ;



 selectedPromotion: Promotion ;

private deleteEnCours = false;



 // listClientsOrgin: any = [];
 private checkedAll: false;

 private  dataPromotionsSource: MatTableDataSource<PromotionElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay :ColumnDefinition[] = [];

 private columnsToSelect : SelectItem[];
  private  selectedColumnsDefinitions: string[];

 private selectedColumnsNames: string[];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];


  constructor( private route: ActivatedRoute, private marketingService: MarketingService,
    private router: Router, private matSnackBar: MatSnackBar, private dialog: MatDialog ) {
  }
  ngOnInit() {
    this.defaultColumnsDefinitions = [
      {name: 'code' , displayTitle: 'Code'},
     {name: 'description' , displayTitle: 'Déscription'},
     {name: 'datePromotion' , displayTitle: 'Date Promotion'},
     {name: 'periodeFrom' , displayTitle: 'Période de : '},
     {name: 'periodeTo' , displayTitle: 'Péridode à:'},
     {name: 'article' , displayTitle: 'Article'},
     {name: 'pourcentageRemise' , displayTitle: '%Remise'},
     {name: 'ancienPrix' , displayTitle: 'Prix Ancien'},
     {name: 'nouveauPrix' , displayTitle: 'Nouveau Prix'},
     {name: 'etat' , displayTitle: 'Etat'}
     
  ];

    this.columnsToSelect = [
      {label: 'Code', value: 'code', title: 'Code'},
      {label: 'Déscription', value: 'description', title: 'Déscription'},
      {label: 'Date Promotion', value: 'datePromotion', title: 'Date Promotion'},
      {label: 'Période de :', value: 'periodeFrom', title: 'Période de :'},
      {label: 'Péridode à:', value: 'periodeTo', title: 'Péridode à:'},
      {label: 'Article', value: 'article', title: 'Article'},
      {label: '%Remise', value: 'pourcentageRemise', title: '%Remise'},
      {label: 'Prix Ancien', value: 'ancienPrix', title: 'Prix Ancien'},
      {label: 'Nouveau Prix', value: 'nouveauPrix', title: 'Nouveau Prix'},
      {label: 'Etat', value: 'etat', title: 'Etat'},
  ];
  this.selectedColumnsDefinitions = [
    'code',
   'description',
   'article'
    ];
    this.marketingService.getPagePromotions(0, 5, null).subscribe((promotions) => {
      this.listPromotions = promotions;
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });
  

  }
  deletePromotion(promotion) {
     // console.log('call delete !', client );
    this.marketingService.deletePromotion(promotion.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData(null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.marketingService.getPagePromotions(0, 5, filter).subscribe((promotions) => {
    this.listPromotions = promotions;
    // this.listClients[this.listClients.length] = this.lastClientAdded;
    this.transformDataToByConsumedByMatTable();
   });
  // this.listClientsOrgin = this.listClients ;
}


checkOneActionInvoked() {
  const listSelectedPromotions: Promotion[] = [];

if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((pElement) => {
  return pElement.code;
});
this.listPromotions.forEach(gr => {
  if (codes.findIndex(code => code === gr.code) > -1) {
    listSelectedPromotions.push(gr);
  }
  });
}
// this.select.emit(listSelectedClients);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listPromotions !== undefined) {
    this.listPromotions.forEach(promotion => {
      listElements.push(
        {
          code: promotion.code ,
          description: promotion.description ,
          article: promotion.article.libelle ,
          datePromotion: promotion.datePromotion ,
          periodeFrom: promotion.periodeFrom ,
          periodeTo: promotion.periodeTo ,
          pourcentageRemise: promotion.pourcentageRemise ,
          ancienPrix: promotion.ancienPrix ,
          nouveauPrix: promotion.nouveauPrix ,
          etat: promotion.etat 
      } );
    });
  }
    this.dataPromotionsSource = new MatTableDataSource<PromotionElement>(listElements);
    this.dataPromotionsSource.sort = this.sort;
    this.dataPromotionsSource.paginator = this.paginator;
    this.marketingService.getTotalPromotions(this.promotionDataFilter).subscribe((response) => {
     //  console.log('Total Clients   is ', response);
     if (this.dataPromotionsSource.paginator !== undefined) {
       this.dataPromotionsSource.paginator.length = response['totalPromotions'];
     }
     });
}

masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
    //   this.dataAccessRightsSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataPromotionsSource.data.length;
  return numSelected === numRows;
}
/*applyFilter(filterValue: string) {
  this.dataAccessRightsSource.filter = filterValue.trim().toLowerCase();
  if (this.dataAccessRightsSource.paginator) {
    this.dataAccessRightsSource.paginator.firstPage();
  }
}*/
loadAddPromotionComponent() {
  this.router.navigate(['/pms/marketing/ajouterPromotion']) ;

}
loadEditPromotionComponent() {
  if (this.selection.selected.length === 0) {
    this.openSnackBar(' Aucun Promotion sélectionné' );
    // this.loadData();
    } else
   {
  this.router.navigate(['/pms/marketing/editerPromotion', this.selection.selected[ this.selection.selected.length - 1].code]) ;
    }
}
supprimerPromotions()
{
  if ( this.selection.selected.length !== 0) {
    this.deleteEnCours = true;
      this.selection.selected.forEach(function(promotion, index) {
          this.marketingService.deletePromotion(promotion.code).subscribe((response) => {
            if (this.selection.selected.length - 1 === index) {
              this.deleteEnCours = false;
              this.openSnackBar( ' Promotion(s) Supprimé(s)');
               this.loadData(null);
              }
          });
    
      }, this);
    } else {
      this.openSnackBar('Aucun Promotion Séléctionné!');
    }
}
refresh()
{
  this.loadData(null);
  
}
  openSnackBar(messageToDisplay) {
 this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
     verticalPosition: 'top'});
}

changePage($event) {
  console.log('page event is ', $event);
 this.marketingService.getPagePromotions($event.pageIndex, $event.pageSize,
  this.promotionDataFilter).subscribe((promotions) => {
    this.listPromotions = promotions;
    const listElements = [];
 if (this.listPromotions !== undefined) {
    this.listPromotions.forEach(promotion => {
      listElements.push( {
        code: promotion.code ,
        description: promotion.description ,
        article: promotion.article.libelle ,
        datePromotion: promotion.datePromotion ,
        periodeFrom: promotion.periodeFrom ,
        periodeTo: promotion.periodeTo ,
        pourcentageRemise: promotion.pourcentageRemise ,
        ancienPrix: promotion.ancienPrix ,
        nouveauPrix: promotion.nouveauPrix ,
        etat: promotion.etat 
      }
      );
    });
  }
     this.dataPromotionsSource= new MatTableDataSource<PromotionElement>(listElements);
     this.marketingService.getTotalPromotions(this.promotionDataFilter).
     subscribe((response) => {
      console.log('Total clients is ', response);
      // this.dataClientsSource.paginator = this.paginator;
      if (this.dataPromotionsSource.paginator) {
        
       this.dataPromotionsSource.paginator.length = response.totalPromotions;
      }
     });

    });
}
toggleFilterPanel() {
  this.showFilter = !this.showFilter;
}
toggleShowColumnsMultiSelect() {
 this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
}
loadFiltredData() {
 // console.log('the filter is ', this.utilisateurDataFilter);
 this.loadData(this.promotionDataFilter);
}

specifyListColumnsToBeDisplayed() {
  
   console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
   this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
 ?
    this.defaultColumnsDefinitions : [];
    this.selectedColumnsNames = [];
    
    if (this.selectedColumnsDefinitions !== undefined)
     {
       this.selectedColumnsDefinitions.forEach((colName) => { 
            const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
            const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
            const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
            this.columnsDefinitionsToDisplay.push(colDef);
           });
     }
    this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
   this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
              return colDef.displayTitle;
    });
    console.log('columns to disp:', this.columnsDefinitionsToDisplay);
    this.columnsNames = [];
    this.columnsNames[0] = 'select';
    this.columnsDefinitionsToDisplay .map((colDef) => {
     this.columnsNames.push(colDef.name);
    if (this.selectedColumnsDefinitions !== undefined) {
     this.selectedColumnsNames.push(colDef.displayTitle);
    }
 });
    this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
     return colDef.displayTitleAr;
 });
 }






}
