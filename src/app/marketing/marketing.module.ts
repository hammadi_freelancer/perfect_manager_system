import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
 import { AppComponent } from '../app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ReactiveFormsModule } from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatChipsModule} from '@angular/material/chips';
import {MatMenuModule} from '@angular/material/menu';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {TableModule} from 'primeng/table';
import {MatSortModule} from '@angular/material/sort';
import {MatSelectModule} from '@angular/material/select';
import {MatTabsModule} from '@angular/material/tabs';
import {PickListModule} from 'primeng/picklist';
import {MultiSelectModule} from 'primeng/multiselect';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatDialogModule} from '@angular/material/dialog';

// import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';
import { PromotionsGridComponent } from './promotions/promotions-grid.component';
import { PromotionNewComponent } from './promotions/promotion-new.component';
import { PromotionEditComponent } from './promotions/promotion-edit.component';

import { MannequinsGridComponent } from './mannequins/mannequins-grid.component';
import { MannequinNewComponent } from './mannequins/mannequin-new.component';
import { MannequinEditComponent } from './mannequins/mannequin-edit.component';

import { CompagnesPublicitairesGridComponent } from './compagnes-publicitaires/compagnes-publicitaires-grid.component';
import { CompagnePublicitaireNewComponent } from './compagnes-publicitaires/compagne-publicitaire-new.component';
import { CompagnePublicitaireEditComponent } from './compagnes-publicitaires/compagne-publicitaire-edit.component';
import { HomeCompagnesPublicitairesComponent } from './compagnes-publicitaires/home-compagne-publicitaire.component';
import { HomePromotionComponent } from './promotions/home-promotion.component';
import { HomeMannequinsComponent } from './mannequins/home-mannequins.component';

import { DocumentsMannequinsGridComponent } from './mannequins/documents-mannequins-grid.component';
import { PaiementsMannequinsGridComponent } from './mannequins/paiements-mannequins-grid.component';
import { AlbumsMannequinsGridComponent } from './mannequins/albums-mannequins-grid.component';


import {DialogAddAlbumMannequinComponent } from './mannequins/popups/dialog-add-album-mannequin.component';
import {DialogEditAlbumMannequinComponent } from './mannequins/popups/dialog-edit-album-mannequin.component';
import {DialogListAlbumsMannequinsComponent } from './mannequins/popups/dialog-list-albums-mannequins.component';


import {DialogAddDocumentMannequinComponent } from './mannequins/popups/dialog-add-document-mannequin.component';
import {DialogEditDocumentMannequinComponent } from './mannequins/popups/dialog-edit-document-mannequin.component';
import {DialogListDocumentsMannequinsComponent } from './mannequins/popups/dialog-list-documents-mannequins.component';


import {DialogAddPaiementMannequinComponent } from './mannequins/popups/dialog-add-paiement-mannequin.component';
import {DialogEditPaiementMannequinComponent } from './mannequins/popups/dialog-edit-paiement-mannequin.component';
import {DialogListPaiementsMannequinsComponent } from './mannequins/popups/dialog-list-paiements-mannequins.component';



 import { JwtModule, JWT_OPTIONS  } from '@auth0/angular-jwt';
 import { JwtHelperService } from '@auth0/angular-jwt';
import {FormsModule } from '@angular/forms';
// import { commonRoute } from './common.route';
 import {APP_BASE_HREF} from '@angular/common';

  //  import { Resolver } from '../utilisateurs/ut';
   
 // const BASE_URL = [{provide: APP_BASE_HREF, useValue: '/common'}];



 import {MatPaginatorModule} from '@angular/material/paginator';
 import { RouterModule, provideRoutes} from '@angular/router';
 // import { CommunicationService } from './magasins/communication.service';
 import {MatSidenavModule} from '@angular/material/sidenav';
 import {MatRadioModule} from '@angular/material/radio';
@NgModule({
  declarations: [
  
    CompagnesPublicitairesGridComponent,
    CompagnePublicitaireNewComponent,
    CompagnePublicitaireEditComponent,
    HomeCompagnesPublicitairesComponent,
    
    PromotionsGridComponent,
    PromotionNewComponent,
    PromotionEditComponent,
    HomePromotionComponent,

    MannequinsGridComponent,
    MannequinNewComponent,
    MannequinEditComponent,
    HomeMannequinsComponent,


    DocumentsMannequinsGridComponent,
    PaiementsMannequinsGridComponent,
    AlbumsMannequinsGridComponent,
    DialogListPaiementsMannequinsComponent,
    DialogListAlbumsMannequinsComponent,
    DialogListDocumentsMannequinsComponent,

    DialogEditPaiementMannequinComponent,
    DialogAddPaiementMannequinComponent,
    DialogEditDocumentMannequinComponent,
    DialogAddDocumentMannequinComponent,
    DialogEditAlbumMannequinComponent,
    DialogAddAlbumMannequinComponent
    
 
  ],
  imports: [
    BrowserModule, HttpClientModule, MatDatepickerModule, MatNativeDateModule,
     BrowserAnimationsModule, ReactiveFormsModule, MatAutocompleteModule,
    MatInputModule, MatCardModule, MatCheckboxModule, MatButtonModule, MatIconModule,
    MatMenuModule, MatProgressSpinnerModule, MatTableModule, RouterModule,
    FormsModule, TableModule , MatSortModule, MatSelectModule, MatTabsModule,
     PickListModule, MatPaginatorModule, MatSidenavModule, MatRadioModule, MatChipsModule,
     JwtModule, MultiSelectModule, MatExpansionModule, MatProgressBarModule, MatDialogModule
     

],
  providers: [MatNativeDateModule, JwtHelperService ,  // BASE_URL
  ],
  exports : [
    RouterModule,

  ],
  entryComponents : [
    DialogListPaiementsMannequinsComponent,
    DialogListAlbumsMannequinsComponent,
    DialogListDocumentsMannequinsComponent,

    DialogEditPaiementMannequinComponent,
    DialogAddPaiementMannequinComponent,
    DialogEditDocumentMannequinComponent,
    DialogAddDocumentMannequinComponent,
    DialogEditAlbumMannequinComponent,
    DialogAddAlbumMannequinComponent

  ],
  bootstrap: [AppComponent]
})
export class MarketingModule { }
