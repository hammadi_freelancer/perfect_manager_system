import { Component, OnInit, AfterViewInit , ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { DocumentType } from './document-type.model';
import { DocumentTypeService } from './document-type.service';
import { DocumentTypesGridComponent} from './documents-types-grid.component';



@Component({
  selector: 'app-gestion-documents-types',
  templateUrl: './gestion-documents-types.component.html',
  // styleUrls: ['./players.component.css']
})
export class GestionDocumentsTypesComponent implements OnInit, AfterViewInit  {

 //  @ViewChild(ClientsGridComponent) clientsGrid;

private documentType: DocumentType = new DocumentType();
private selectedDocumentType: DocumentType = new DocumentType();
private addedDocumentType: DocumentType = new DocumentType();
private listDocumentTypes: DocumentType[];

  constructor( private route: ActivatedRoute,
    private router: Router, private documentTypeService: DocumentTypeService) {
  }
  ngOnInit() {
  }
  ngAfterViewInit() {
    // this.selectedClient = this.clientsGrid.selectedClient;
   //  console.log(this.clientsGrid.selectedClient);
  }
  selectDocumentType($event) {
    console.log('selectDocumentType invoked');
    console.log($event);
    this.selectedDocumentType = $event;
    this.documentTypeService.getDocumentsTypes().subscribe((documentsTypes) => {
      this.listDocumentTypes = documentsTypes;
      // this.listClients[this.listClients.length] = this.lastClientAdded;
      // this.listClientsOrgin = this.listClients ;
     });
  }
  addDocumentType($event) {
   // console.log('addClient invoked');
    // console.log($event);
    this.addedDocumentType = $event;
    this.documentTypeService.getDocumentsTypes().subscribe((documentsTypes) => {
      this.listDocumentTypes = documentsTypes;
      // this.listClients[this.listClients.length] = this.lastClientAdded;
      // this.listClientsOrgin = this.listClients ;
     });
  }
}
