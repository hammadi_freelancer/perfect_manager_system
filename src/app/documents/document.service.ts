import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';

import {Document} from './document.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_DOCUMENTS = 'http://localhost:8082/documents/getDocuments';
const URL_ADD_DOCUMENT = 'http://localhost:8082/documents/addDocument';
const URL_UPDATE_DOCUMENT = 'http://localhost:8082/documents/updateDocument';
const URL_DELETE_DOCUMENT = 'http://localhost:8082/documents/deleteDocument';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  constructor(private httpClient: HttpClient) {

   }

   getDocuments(): Observable<Document[]> {
    return this.httpClient
    .get<Document[]>(URL_GET_LIST_DOCUMENTS);
   }

   addDocument(document: Document): Observable<any> {
    return this.httpClient.post<Document>(URL_ADD_DOCUMENT, document);
  }
  updateDocument(document: Document): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_DOCUMENT, document);
  }
  deleteDocument(codeDocument): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_DOCUMENT + '/' + codeDocument);
  }

}
