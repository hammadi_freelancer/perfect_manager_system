import { Component, OnInit, AfterViewInit, EventEmitter, Output, Input } from '@angular/core';
// import { ArticleService } from './article.service';
import {DocumentType} from './document-type.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {DocumentTypeService} from './document-type.service';



@Component({
  selector: 'app-documents-types-grid',
  templateUrl: './documents-types-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class DocumentTypesGridComponent implements OnInit {

private documentType: DocumentType = new DocumentType();

@Output()
select: EventEmitter<DocumentType> = new EventEmitter<DocumentType>();

//private lastClientAdded: Client;

 selectedDocumentType: DocumentType ;
 @Input()
 private listDocumentTypes: any = [] ;
 private listDocumentTypeOrgin: any = [];
 filterCIN: any;
  constructor( private route: ActivatedRoute, private documentTypesService: DocumentTypeService,
    private router: Router ) {
  }
  ngOnInit() {
    this.documentTypesService.getDocumentsTypes().subscribe((documentsTypes) => {
      this.listDocumentTypes = documentsTypes;
      // this.listClients[this.listClients.length] = this.lastClientAdded;
      this.listDocumentTypeOrgin = this.listDocumentTypes ;
     });
    // this.listClientsOrgin = this.listClients ;

  }

  selectionnerDocumentType(i) {
   // console.log('click !', i);
    this.select.emit(this.listDocumentTypes[i]);
  }

  deleteDocumentType(i) {
      console.log('call delete !', i);
    this.documentTypesService.deleteDocumentType(this.listDocumentTypes[i].code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.lodData();

      } else {
       // show error message
      }
     });

  }
lodData() {
  this.documentTypesService.getDocumentsTypes().subscribe((documentsTypes) => {
    this.listDocumentTypes = documentsTypes;
    // this.listClients[this.listClients.length] = this.lastClientAdded;
    this.listDocumentTypeOrgin = this.listDocumentTypes ;
   });
  // this.listClientsOrgin = this.listClients ;
}

}
