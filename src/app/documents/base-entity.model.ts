export class BaseEntity {
    constructor(public  dateCreation?: string, public dateLastUpdate?: string, public userIdCreator?: string,
        public userIdLastUpdator?: string, public state?: string // annule valide
    ) {}
}
