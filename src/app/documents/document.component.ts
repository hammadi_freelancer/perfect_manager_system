import { Component, OnInit, Input, EventEmitter, Output, AfterContentInit, AfterViewInit, AfterContentChecked,
  AfterViewChecked
  } from '@angular/core';
  // import { ArticleService } from './article.service';
  import {DocumentType} from './document-type.model';
  import {Document} from './document.model';
  import {DocumentTypeService} from './document-type.service';
  import {DocumentService} from './document.service';
  import {FormControl} from '@angular/forms';
  import {map, startWith} from 'rxjs/operators';
  import {Observable} from 'rxjs';
  import { Router, ActivatedRoute, ParamMap } from '@angular/router';
  @Component({
    selector: 'app-document',
    templateUrl: './document.component.html',
    // styleUrls: ['./players.component.css']
  })
  export class DocumentComponent implements OnInit, AfterContentChecked {
  // private client: Client = new Client();
  @Input()
  private document: Document;
  @Input()
  private listDocuments: any = [] ;
  @Output()
  save: EventEmitter<Document> = new EventEmitter<Document>();
  private modeUpdate = false;
  private userStartWriting = false;
  private saveEnCours = false;
  private file: any;
  private fileData: any;
    constructor( private route: ActivatedRoute,
      private router: Router , private documentService: DocumentService ) {
    }
    ngOnInit() {
    }
  saveDocument() {
      console.log(this.document);
      this.saveEnCours = true;
      this.documentService.addDocument(this.document).subscribe((response) => {
          if (response.error_message ===  undefined) {
            this.save.emit(response);
            this.document = new Document();
          } else {
           // show error message
          }
      });
  }
  updateDocument() {
    console.log(this.document);
    this.documentService.updateDocument(this.document).subscribe((response) => {
      if (response.error_message ===  undefined) {
        this.save.emit(response);
        this.document = new Document()  ;
        this.modeUpdate = false;
      } else {
       // show error message
      }
  });
    /*this.clientService.addClient(this.client).subscribe((response) => {
        if (response.error_message ===  undefined) {
          this.save.emit(response);
          this.client = new Client();
        } else {
         // show error message
        }
    });*/
  }
  ngAfterContentChecked() {
    // console.log('selected client', this.client);
    if (this.document.code !==  undefined ) {
      let  existCode = false;
      const currentIntsance = this;
      if (this.listDocuments !== undefined) {
     existCode = this.listDocuments.some(function(document, index) {
        return document.code === currentIntsance.document.code;
    });
      }
        if (existCode) {
        this.modeUpdate = true;
            console.log('selected document', this.modeUpdate);
        }
    }
    if (this.modeUpdate === false && (this.document.code !==  undefined) && (this.document.code !== '') ) {
        this.userStartWriting = true;
      } else {
        this.userStartWriting = false;
      }
  }


  }

