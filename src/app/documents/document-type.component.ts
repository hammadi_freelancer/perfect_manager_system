import { Component, OnInit, Input, EventEmitter, Output, AfterContentInit, AfterViewInit, AfterContentChecked,
  AfterViewChecked
  } from '@angular/core';
  // import { ArticleService } from './article.service';
  import {DocumentType} from './document-type.model';
  import {DocumentTypeService} from './document-type.service';
  import {FormControl} from '@angular/forms';
  import {map, startWith} from 'rxjs/operators';
  import {Observable} from 'rxjs';
  import { Router, ActivatedRoute, ParamMap } from '@angular/router';
  @Component({
    selector: 'app-document-type',
    templateUrl: './document-type.component.html',
    // styleUrls: ['./players.component.css']
  })
  export class DocumentTypeComponent implements OnInit, AfterContentChecked {
  // private client: Client = new Client();
  @Input()
  private documentType: DocumentType;
  @Input()
  private listDocumentTypes: any = [] ;
  @Output()
  save: EventEmitter<DocumentType> = new EventEmitter<DocumentType>();
  private modeUpdate = false;
  private userStartWriting = false;
  private saveEnCours = false;
    constructor( private route: ActivatedRoute,
      private router: Router , private documentTypeService: DocumentTypeService ) {
    }
    ngOnInit() {
    }
  saveDocumentType() {
      console.log(this.documentType);
      this.saveEnCours = true;
      this.documentTypeService.addDocumentType(this.documentType).subscribe((response) => {
          if (response.error_message ===  undefined) {
            this.save.emit(response);
            this.documentType = new DocumentType();
          } else {
           // show error message
          }
      });
  }
  updateDocumentType() {
    console.log(this.documentType);
    this.documentTypeService.updateDocumentType(this.documentType).subscribe((response) => {
      if (response.error_message ===  undefined) {
        this.save.emit(response);
        this.documentType = new DocumentType();
        this.modeUpdate = false;
      } else {
       // show error message
      }
  });
    /*this.clientService.addClient(this.client).subscribe((response) => {
        if (response.error_message ===  undefined) {
          this.save.emit(response);
          this.client = new Client();
        } else {
         // show error message
        }
    });*/
  }
  ngAfterContentChecked() {
    // console.log('selected client', this.client);
    if (this.documentType.code !==  undefined ) {
      let  existCode = false;
      const currentIntsance = this;
      if (this.listDocumentTypes !== undefined) {
     existCode = this.listDocumentTypes.some(function(documentType, index) {
        return documentType.code === currentIntsance.documentType.code;
    });
      }
        if (existCode) {
        this.modeUpdate = true;
            console.log('selected documentType', this.modeUpdate);
        }
    }
    if (this.modeUpdate === false && (this.documentType.code !==  undefined) && (this.documentType.code !== '') ) {
        this.userStartWriting = true;
      } else {
        this.userStartWriting = false;
      }
  }
  }

