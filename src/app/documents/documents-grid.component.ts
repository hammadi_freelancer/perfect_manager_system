import { Component, OnInit, AfterViewInit, EventEmitter, Output, Input } from '@angular/core';
// import { ArticleService } from './article.service';
import {Document} from './document.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {DocumentService} from './document.service';



@Component({
  selector: 'app-documents-grid',
  templateUrl: './documents-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class DocumentsGridComponent implements OnInit {

private document: Document = new Document();

@Output()
select: EventEmitter<Document> = new EventEmitter<Document>();

//private lastClientAdded: Client;

 selectedDocument: Document ;
 @Input()
 private listDocuments: any = [] ;
 private listDocumentsOrgin: any = [];
 filterCIN: any;
  constructor( private route: ActivatedRoute, private documentService: DocumentService,
    private router: Router ) {
  }
  ngOnInit() {
    this.documentService.getDocuments().subscribe((documents) => {
      this.listDocuments = documents;
      // this.listClients[this.listClients.length] = this.lastClientAdded;
      this.listDocuments = this.listDocuments ;
     });
    // this.listClientsOrgin = this.listClients ;

  }

  selectionnerDocument(i) {
   // console.log('click !', i);
    this.select.emit(this.listDocuments[i]);
  }

  deleteDocument(i) {
      console.log('call delete !', i);
    this.documentService.deleteDocument(this.listDocuments[i].code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.lodData();

      } else {
       // show error message
      }
     });

  }
lodData() {
  this.documentService.getDocuments().subscribe((documents) => {
    this.listDocuments = documents;
    // this.listClients[this.listClients.length] = this.lastClientAdded;
    this.listDocumentsOrgin = this.listDocuments ;
   });
  // this.listClientsOrgin = this.listClients ;
}

}
