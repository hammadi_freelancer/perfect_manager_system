
import { BaseEntity } from './base-entity.model' ;

export class DocumentType extends BaseEntity {
    constructor(public code?: string, public description?: string, public type?: string,
        public dateDelivraison?: string // annule valide
    ) {
        super();
    }
}
