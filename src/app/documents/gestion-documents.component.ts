import { Component, OnInit, AfterViewInit , ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Document } from './document.model';
import { DocumentService } from './document.service';
import { DocumentsGridComponent} from './documents-grid.component';



@Component({
  selector: 'app-gestion-documents',
  templateUrl: './gestion-documents.component.html',
  // styleUrls: ['./players.component.css']
})
export class GestionDocumentsComponent implements OnInit, AfterViewInit  {

 //  @ViewChild(ClientsGridComponent) clientsGrid;

private document: Document = new Document();
private selectedDocument: Document = new Document();
private addedDocument: Document = new Document();
private listDocuments: Document[];
private left: true;
  constructor( private route: ActivatedRoute,
    private router: Router, private documentService: DocumentService) {
  }
  ngOnInit() {
  }
  ngAfterViewInit() {
    // this.selectedClient = this.clientsGrid.selectedClient;
   //  console.log(this.clientsGrid.selectedClient);
  }
  selectDocument($event) {
    console.log('selectDocumentType invoked');
    console.log($event);
    this.selectedDocument = $event;
    this.documentService.getDocuments().subscribe((documents) => {
      this.listDocuments = documents;
      // this.listClients[this.listClients.length] = this.lastClientAdded;
      // this.listClientsOrgin = this.listClients ;
     });
  }
  addDocument($event) {
   // console.log('addClient invoked');
    // console.log($event);
    this.addedDocument = $event;
    this.documentService.getDocuments().subscribe((documents) => {
      this.listDocuments = documents;
      // this.listClients[this.listClients.length] = this.lastClientAdded;
      // this.listClientsOrgin = this.listClients ;
     });
  }
}
