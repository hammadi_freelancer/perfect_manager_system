import { Injectable } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders  } from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';
import { GestionDocumentsTypesComponent } from './gestion-documents-types.component';
import { GestionDocumentsComponent } from './gestion-documents.component';

import { DocumentComponent } from './document.component';
import { DocumentsGridComponent } from './documents-grid.component';
import { MainComponent } from './main.component';

 const documentsRoute: Routes = [
    {
        path: 'documents/main',
        component : MainComponent,
        data: {
           // authorities: ['ROLE_ASSET_ASSIGNMENT'],
            // pageTitle: 'safeleasegtwApp.asset.home.titleAssign'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'documents/ajouterDocument',
        component: DocumentComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'documents/gestionDocumentsTypes',
        component : GestionDocumentsTypesComponent,
        data: {
           // authorities: ['ROLE_ASSET_ASSIGNMENT'],
            // pageTitle: 'safeleasegtwApp.asset.home.titleAssign'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'documents/gestionDocuments',
        component : GestionDocumentsComponent,
        data: {
           // authorities: ['ROLE_ASSET_ASSIGNMENT'],
            // pageTitle: 'safeleasegtwApp.asset.home.titleAssign'
        },
        // canActivate: [UserRouteAccessService]
    }
];
export const documentsRoutingModule: ModuleWithProviders = RouterModule.forRoot(documentsRoute);
// commonRouting.providers = [{ provide: APP_BASE_HREF, useValue: '/common'} ] ;


