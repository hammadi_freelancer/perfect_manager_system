
import { BaseEntity } from './base-entity.model' ;
import { DocumentType } from './document-type.model' ;

export class Document extends BaseEntity {
    constructor(public code?: string, public description?: string, public type?: DocumentType,
        public dateDelivraison?: string , public filesData?: any[] // annule valide
    ) {
        super();
      this.filesData = [];
    }
}
