import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
 import { AppComponent } from '../app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ReactiveFormsModule } from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatInputModule} from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatMenuModule} from '@angular/material/menu';
import { RouterModule, provideRoutes} from '@angular/router';
import { DocumentTypesGridComponent } from './documents-types-grid.component';
import { DocumentTypeComponent} from './document-type.component';
import { GestionDocumentsTypesComponent } from './gestion-documents-types.component';
import { DocumentsGridComponent } from './documents-grid.component';
import { DocumentComponent} from './document.component';
import { GestionDocumentsComponent } from './gestion-documents.component';
import { MainComponent } from './main.component';
import { SharedComponentModule } from '../shared-components/shared-components.module';
import {MatSidenavModule} from '@angular/material/sidenav';

// import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

//  import { ArticleComponent } from './articles/article.component';
// import { ClientComponent } from './clients/client.component';
// import { PayementComponent } from './payements/payement.component';
// import { PayementsGridComponent} from './payements/payements-grid.component';
// import { ClientsGridComponent} from './clients/clients-grid.component';
// import { ArticlesGridComponent} from './articles/articles-grid.component';
// import { GestionClientsComponent} from './clients/gestion-clients.component';
// import { GestionArticlesComponent} from './articles/gestion-articles.component';

// import { PayementService } from './payements/payement.service';
// import { UtilisateurComponent } from './utilisateurs/utilisateur.component';
import {FormsModule } from '@angular/forms';
// import { commonRoute } from './common.route';
 import {APP_BASE_HREF} from '@angular/common';
 import {  documentsRoutingModule} from './documents.routing' ;
/*const ENTITY_STATES = [
  ...commonRoute
];*/

 // const BASE_URL = [{provide: APP_BASE_HREF, useValue: '/common'}];
@NgModule({
  declarations: [
    DocumentTypeComponent,
    DocumentTypesGridComponent,
    GestionDocumentsTypesComponent,
    DocumentsGridComponent,
    DocumentComponent,
    GestionDocumentsComponent,
    MainComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, MatDatepickerModule, MatNativeDateModule,
     BrowserAnimationsModule, ReactiveFormsModule, MatAutocompleteModule,
    MatInputModule, MatCardModule, MatCheckboxModule, MatMenuModule, MatButtonModule ,
    MatIconModule , MatSidenavModule ,
    FormsModule, SharedComponentModule,
    // RouterModule.forRoot(ENTITY_STATES, {useHash: true})
    documentsRoutingModule
],
  providers: [MatNativeDateModule,  // BASE_URL
  ],
  exports : [
    RouterModule,
    DocumentTypeComponent,
    DocumentTypesGridComponent,
    GestionDocumentsTypesComponent,
    DocumentComponent,
    DocumentsGridComponent,
    GestionDocumentsComponent,
    MainComponent
  ],
  bootstrap: [AppComponent]
})
export class DocumentsModule { }
