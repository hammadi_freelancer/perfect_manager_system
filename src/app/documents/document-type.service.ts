import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';

import {DocumentType} from './document-type.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_DOCUMENTS_TYPES = 'http://localhost:8082/documentsTypes/getDocumentsTypes';
const URL_ADD_DOCUMENT_TYPE = 'http://localhost:8082/documentsTypes/addDocumentType';
const URL_UPDATE_DOCUMENT_TYPE = 'http://localhost:8082/documentsTypes/updateDocumentType';
const URL_DELETE_DOCUMENT_TYPE = 'http://localhost:8082/documentsTypes/deleteDocumentType';

@Injectable({
  providedIn: 'root'
})
export class DocumentTypeService {

  constructor(private httpClient: HttpClient) {

   }

   getDocumentsTypes(): Observable<DocumentType[]> {
    return this.httpClient
    .get<DocumentType[]>(URL_GET_LIST_DOCUMENTS_TYPES);
   }

   addDocumentType(documentType: DocumentType): Observable<any> {
    return this.httpClient.post<DocumentType>(URL_ADD_DOCUMENT_TYPE, documentType);
  }
  updateDocumentType(documentType: DocumentType): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_DOCUMENT_TYPE, documentType);
  }
  deleteDocumentType(codeDocumentType): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_DOCUMENT_TYPE + '/' + codeDocumentType);
  }

}
