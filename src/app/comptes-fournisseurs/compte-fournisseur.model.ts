
import { BaseEntity } from '../common/base-entity.model' ;
import { Fournisseur } from '../achats/fournisseurs/fournisseur.model';
type Etat = 'Initial' | 'Valide' | 'Annule' | 'EnCoursValidation' | 'Ouvert' | 'Cloture'

export class CompteFournisseur extends BaseEntity {
    constructor(
        public code?: string,
       
        public numeroCompteFournisseur?: string,
        public fournisseur ?: Fournisseur,
        public profession ?: string,
        public chiffreAffaires ?: number,
        public valeurAchats ?: number,
        public valeurDettes ?: number,
        public valeurPaiements ?: number,
        public dateOuvertureObject ?: Date,
        public dateOuverture ?: string,
        
        public dateClotureObject ?: Date ,
        public dateCloture ?: string ,
        
         public etat?: Etat,
    // public listTranches?: Tranche[]
     ) {
         super();
         this.chiffreAffaires =  Number(0);
         this.valeurAchats =  Number(0);
         this.valeurPaiements =  Number(0);
         this.valeurDettes =  Number(0);
         this.fournisseur = new Fournisseur();
    }
 
}
