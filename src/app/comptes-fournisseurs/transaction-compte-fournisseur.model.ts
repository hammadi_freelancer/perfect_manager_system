
import { BaseEntity } from '../common/base-entity.model' ;

type TypeTransaction =   'Paiement' | 'Dette' |
'Achat' |'';


 type Etat = 'Initial' | 'Valide' | 'Annule'

export class TransactionCompteFournisseur extends BaseEntity {
    constructor(
        public code?: string,
        public codeCompteFournisseur?: string,
         public dateTransactionObject?: Date,
         public dateTransaction?: string,
         public valeurTransaction?: number,
         public type?: TypeTransaction,
     ) {
         super();
       }
}
