
import { Component, OnInit, Inject,Input, ViewChild, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {ComptesFournisseursService} from './comptes-fournisseurs.service';
import {CompteFournisseur} from './compte-fournisseur.model';

import {TransactionCompteFournisseur} from './transaction-compte-fournisseur.model';
import { TransactionCompteFournisseurElement } from './transaction-compte-fournisseur.interface';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogAddTransactionCompteFournisseurComponent } from  './popups/dialog-add-transaction-compte-fournisseur.component';
import { DialogEditTransactionCompteFournisseurComponent } from  './popups/dialog-edit-transaction-compte-fournisseur.component';
@Component({
    selector: 'app-transactions-comptes-fournisseurs-grid',
    templateUrl: 'transactions-comptes-fournisseurs-grid.component.html',
  })
  export class TransactionsComptesFournisseursGridComponent implements OnInit, OnChanges {
     private listTransactionsComptesFournisseurs: TransactionCompteFournisseur[] = [];
     private  dataTransactionsComptesFournisseursSource: MatTableDataSource<TransactionCompteFournisseurElement>;
     private selection = new SelectionModel<TransactionCompteFournisseurElement>(true, []);
    
     @Input()
     private compteFournisseur : CompteFournisseur

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private comptesFournisseursService: ComptesFournisseursService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.comptesFournisseursService.getTransactionsCompteFournisseurByCodeCompteFournisseur
      (this.compteFournisseur.code).subscribe((listTrans) => {
        this.listTransactionsComptesFournisseurs= listTrans;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
  }
  ngOnChanges()
  {
    this.specifyListColumnsToBeDisplayed();
    
    this.comptesFournisseursService.getTransactionsCompteFournisseurByCodeCompteFournisseur
    (this.compteFournisseur.code).subscribe((listTrans) => {
      this.listTransactionsComptesFournisseurs = listTrans;
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }

  checkOneActionInvoked() {
    const listSelectedTransactions: TransactionCompteFournisseur[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((transCC) => {
    return transCC.code;
  });
  this.listTransactionsComptesFournisseurs.forEach(tr => {
    if (codes.findIndex(code => code === tr.code) > -1) {
      listSelectedTransactions.push(tr);
    }
    });
  }
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listTransactionsComptesFournisseurs !== undefined) {

      this.listTransactionsComptesFournisseurs.forEach(tr => {
             listElements.push( {code: tr.code ,
              dateTransaction: tr.dateTransaction,
              valeurTransaction: tr.valeurTransaction,
               type: tr.type,
        } );
      });
    }
      this.dataTransactionsComptesFournisseursSource = new MatTableDataSource<TransactionCompteFournisseurElement>(listElements);
      this.dataTransactionsComptesFournisseursSource.sort = this.sort;
      this.dataTransactionsComptesFournisseursSource.paginator = this.paginator;
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [
       {name: 'code' , displayTitle: 'Code'},
     {name: 'dateTransaction' , displayTitle: 'Date'},
     {name: 'valeurTransaction' , displayTitle: 'Valeur'},
     {name: 'type' , displayTitle: 'Type'},
   
      ];

     
     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataTransactionsComptesFournisseursSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataTransactionsComptesFournisseursSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataTransactionsComptesFournisseursSource.filter = filterValue.trim().toLowerCase();
    if (this.dataTransactionsComptesFournisseursSource.paginator) {
      this.dataTransactionsComptesFournisseursSource.paginator.firstPage();
    }
  }
  showAddTransactionDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeCompteFournisseur : this.compteFournisseur.code
   };
 
   const dialogRef = this.dialog.open(DialogAddTransactionCompteFournisseurComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(trAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditTransactionDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeTransactionCompteFournisseur : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditTransactionCompteFournisseurComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(trAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucune Transaction Séléctionnée!');
    }
    }
    supprimerTransactions()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(trC, index) {
              this.comptesFournisseursService.deleteTransactionCompteFournisseur(trC.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar(' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucune Transaction Séléctionnée!');
        }
    }
    refresh() {
      this.comptesFournisseursService.getTransactionsCompteFournisseurByCodeCompteFournisseur
      (this.compteFournisseur.code).subscribe((listTr) => {
        this.listTransactionsComptesFournisseurs = listTr;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
