import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {CompteFournisseur} from './compte-fournisseur.model';

import {ComptesFournisseursService} from './comptes-fournisseurs.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {MatSnackBar} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { FournisseurFilter } from '../achats/fournisseurs/fournisseur.filter';


import {Fournisseur} from '../achats/fournisseurs/fournisseur.model';

@Component({
  selector: 'app-compte-fournisseur-new',
  templateUrl: './compte-fournisseur-new.component.html',
  // styleUrls: ['./players.component.css']
})
export class CompteFournisseurNewComponent implements OnInit {

 private compteFournisseur: CompteFournisseur =  new CompteFournisseur();
 private saveEnCours = false;
 private fournisseurFilter: FournisseurFilter;
 
 private listFournisseurs: Fournisseur[];
private etatsCompteFournisseur: any[] = [
  {value: 'Ouvert', viewValue: 'Ouvert'},
  {value: 'Initial', viewValue: 'Initiale'},
  {value: 'EnCoursValidation', viewValue: 'En Cours de Validation'},
  {value: 'Valide', viewValue: 'Validé'},
  {value: 'Cloture', viewValue: 'Cloturé'}
];
  constructor( private route: ActivatedRoute,
    private router: Router, private comptesFournisseursService: ComptesFournisseursService,
   private matSnackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.listFournisseurs = this.route.snapshot.data.fournisseurs;
    
     this.fournisseurFilter = new FournisseurFilter(this.listFournisseurs);
  }
  saveCompteFournisseur() {

    if( this.compteFournisseur.etat === undefined || this.compteFournisseur.etat === 'Ouvert' ) {
      this.compteFournisseur.etat = 'Initial';
    }
    // this.saveEnCours = true;
    this.saveEnCours = true;
    this.comptesFournisseursService.addCompteFournisseur(this.compteFournisseur).subscribe((response) => {
      this.saveEnCours = false;
      
        if (response.error_message ===  undefined) {
          console.log('undefined error message');
          // this.save.emit(response);
          this.compteFournisseur =  new CompteFournisseur();
          this.openSnackBar(  'Compte Fournisseur Enregistré');
        } else {
         // show error message
         this.openSnackBar(  'Erreurs: Opération Echouée');
         
        }
    });
  }


  vider() {
    this.compteFournisseur = new CompteFournisseur();
  }
  openSnackBar(messageToDisplay) {
   this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
       verticalPosition: 'top'});
  }
loadGridComptesFournisseurs() {
  this.router.navigate(['/pms/finance/comptesFournisseurs']) ;
}
fillDataNomPrenomFournisseur()
{
  const listFiltred  = this.listFournisseurs.filter((fournisseur) => (fournisseur.cin === this.compteFournisseur.fournisseur.cin));
  this.compteFournisseur.fournisseur.nom = listFiltred[0].nom;
  this.compteFournisseur.fournisseur.prenom = listFiltred[0].prenom;
}
fillDataMatriculeRaisonFournisseur() {
  const listFiltred  = this.listFournisseurs.filter((fournisseur) => 
  (fournisseur.registreCommerce === this.compteFournisseur.fournisseur.registreCommerce));
  this.compteFournisseur.fournisseur.matriculeFiscale = listFiltred[0].matriculeFiscale;
  this.compteFournisseur.fournisseur.raisonSociale = listFiltred[0].raisonSociale;
}
}
