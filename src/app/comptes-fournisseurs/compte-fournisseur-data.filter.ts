
export class CompteFournisseurDataFilter
{
    numeroCompteFournisseur: string;
    profession: string;
    chiffreAffaires: string;
    dateOuvertureFromObject: Date;
    dateOuvertureToObject: Date;
    dateClotureFromObject: Date;
    dateClotureToObject: Date;
    etat: string;
    cinFournisseur: string;
    nomFournisseur: string;
    prenomFournisseur: string;
    raisonFournisseur: string;
    matriculeFournisseur: string;
    registreFournisseur: string; 
}

