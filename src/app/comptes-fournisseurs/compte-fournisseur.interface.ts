export interface CompteFournisseurElement {
         
         code: string ;
         numeroCompteFournisseur: string;
         cinFournisseur: string ;
         nomFournisseur: string ;
         prenomFournisseur: string ;
         raisonSocialFournisseur: string ;
         profession: string ;
         dateOuverture: string ;
         dateCloture: string ;
         chiffreAffaires: number;
         valeurAchats: number;
         valeurDettes: number;
         valeurPaiements: number;
         etat: string ;
         description: string ;

        }
