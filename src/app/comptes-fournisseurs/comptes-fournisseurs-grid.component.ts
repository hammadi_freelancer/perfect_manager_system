import { Component, OnInit, Output,ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {CompteFournisseur} from './compte-fournisseur.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {ComptesFournisseursService} from './comptes-fournisseurs.service';
import { CompteFournisseurElement } from './compte-fournisseur.interface';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import {CompteFournisseurDataFilter} from './compte-fournisseur-data.filter';
import { SelectItem } from 'primeng/api';

// import { DialogAddDocumentCreditComponent } from './dialog-add-document-credit.component';
@Component({
  selector: 'app-comptes-fournisseurs-grid',
  templateUrl: './comptes-fournisseurs-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class ComptesFournisseursGridComponent implements OnInit {

  private compteFournisseur: CompteFournisseur =  new CompteFournisseur();
  private compteFournisseurDataFilter = new CompteFournisseurDataFilter();
   private showSelectColumnsMultiSelect = false;
   private showFilter = false;
   private showFilterFournisseur = false;
  private selection = new SelectionModel<CompteFournisseurElement>(true, []);
  
@Output()
select: EventEmitter<CompteFournisseur[]> = new EventEmitter<CompteFournisseur[]>();

//private lastFournisseurAdded: Fournisseur;

 selectedCompteFournisseur: CompteFournisseur ;


 @Input()
 private listComptesFournisseurs: CompteFournisseur[] = [] ;
 private deleteEnCours = false;
 private checkedAll: false;
 private  dataComptesFournisseursSource: MatTableDataSource<CompteFournisseurElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
 
 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay :ColumnDefinition[] = [];

 private columnsToSelect : SelectItem[];
  private  selectedColumnsDefinitions: string[];

 private selectedColumnsNames: string[];
  constructor( private route: ActivatedRoute, private comptesFournisseursService: ComptesFournisseursService,
    private router: Router, private matSnackBar: MatSnackBar ) {
  }
  ngOnInit() {
    this.columnsToSelect = [
      {label: 'N° Compte', value: 'numeroCompteFournisseur', title: 'N°Compte'},
      {label: 'Date Ouverture', value: 'dateOuverture', title: 'Date Ouverture'},
      {label: 'Nom Fournisseur', value: 'nomFournisseur', title: 'Nom Fournisseur'},
      {label: 'Prénom Fournisseur', value: 'prenomFournisseur', title: 'Prénom Fournisseur'},
      {label: 'CIN Fournisseur', value: 'cinFournisseur', title: 'CIN Fournisseur'},  
      {label: 'Valeur Ventes', value: 'valeurVentes', title: 'Valeur Ventes'},
      {label: 'Valeur Paiements', value: 'valeurPaiements', title: 'Valeur Paiements'},
      {label: 'Valeur Credits', value: 'valeurCredits', title: 'Valeur Credits'},
      {label: 'Rs.Soc.Fournisseur', value:  'raisonFournisseur', title: 'Rs.Soc.Fournisseur' },
      {label: 'Matr.Fournisseur', value:  'matriculeFournisseur', title: 'Matr.Fournisseur' },
      {label: 'Reg.Fournisseur', value:  'registreFournisseur', title: 'Reg.Fournisseur' },
      {label: 'Date Clôture', value: 'dateCloture', title: 'Date Clôture' },
      {label: 'Déscription', value: 'description', title: 'Déscription' },
      {label: 'Etat', value:  'etat', title: 'Etat' },
  
  ];
  this.selectedColumnsDefinitions = [
    'numeroCompteFournisseur',
    'nomFournisseur' , 
    'prenomFournisseur', 
    'cinFournisseur' , 
   'valeurVentes' , 
   'valeurPaiements',
    'etat'
    ];
    this.comptesFournisseursService.getPageComptesFournisseurs(0, 5, null).
    subscribe((comptesFournisseurs) => {
      this.listComptesFournisseurs = comptesFournisseurs;
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });
  }
  deleteCompteFournisseur(compteFournisseur) {
     //  console.log('call delete !', credit );
    this.comptesFournisseursService.deleteCompteFournisseur(compteFournisseur.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.fournisseur = new Fournisseur();
        console.log('call delete !', response);
        this.loadData(null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.comptesFournisseursService.getPageComptesFournisseurs(0, 5 ,filter).
  subscribe((comptesFournisseurs) => {
    this.listComptesFournisseurs = comptesFournisseurs;
    this.transformDataToByConsumedByMatTable();
   });
  // this.listFournisseursOrgin = this.listFournisseurs ;
}


checkOneActionInvoked() {
  const listSelectedComptesFournisseurs: CompteFournisseur[] = [];

if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((compteElement) => {
  return compteElement.code;
});
this.listComptesFournisseurs.forEach(compteCl => {
  if (codes.findIndex(code => code === compteCl.code) > -1) {
    listSelectedComptesFournisseurs.push(compteCl);
  }
  });
}
this.select.emit(listSelectedComptesFournisseurs);

}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  this.dataComptesFournisseursSource = null;
  if (this.listComptesFournisseurs !== undefined) {
   
    this.listComptesFournisseurs.forEach(compteFournisseur => {
           listElements.push( {code: compteFournisseur.code ,
            numeroCompteFournisseur: compteFournisseur.numeroCompteFournisseur,
            dateOuverture: compteFournisseur.dateOuverture ,
            dateCloture: compteFournisseur.dateCloture ,
            cinFournisseur: compteFournisseur.fournisseur.cin,
            nomFournisseur: compteFournisseur.fournisseur.nom,
            prenomFournisseur: compteFournisseur.fournisseur.prenom,
            raisonSocialFournisseur: compteFournisseur.fournisseur.raisonSociale,
            chiffreAffaires: compteFournisseur.chiffreAffaires ,
            profession: compteFournisseur.profession ,
            valeurAchats: compteFournisseur.valeurAchats ,
            valeurDettes: compteFournisseur.valeurDettes ,
            valeurPaiements: compteFournisseur.valeurPaiements ,
            etat: compteFournisseur.etat === 'Valide' ?  'Validé' :
            compteFournisseur.etat === 'Annule' ? 'Annulé' :
            compteFournisseur.etat === 'EnCoursValidation' ?
             'En Cours De Validation' :   compteFournisseur.etat === 'Cloture' ? 'Cloturé' :
            compteFournisseur.etat === 'Initial' ? 'Initiale' : 'Ouvert',
            description: compteFournisseur.description,

      } );
    });
  }
    this.dataComptesFournisseursSource = new MatTableDataSource<CompteFournisseurElement>(listElements);
    this.dataComptesFournisseursSource.sort = this.sort;
    this.dataComptesFournisseursSource.paginator = this.paginator;
    this.comptesFournisseursService.getTotalComptesFournisseurs(this.compteFournisseurDataFilter)
    .subscribe((response) => {
    //  console.log('Total credits is ', response);
      this.dataComptesFournisseursSource.paginator.length = response.totalComptesFournisseurs;
    });
}
specifyListColumnsToBeDisplayed() {
  
   console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
   this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
 ?
    this.defaultColumnsDefinitions : [];
    this.selectedColumnsNames = [];
    
    if (this.selectedColumnsDefinitions !== undefined)
     {
       this.selectedColumnsDefinitions.forEach((colName) => { 
            const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
            const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
            const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
            this.columnsDefinitionsToDisplay.push(colDef);
           });
     }
    this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
   this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
              return colDef.displayTitle;
    });
    console.log('columns to disp:', this.columnsDefinitionsToDisplay);
    this.columnsNames = [];
    this.columnsNames[0] = 'select';
    this.columnsDefinitionsToDisplay .map((colDef) => {
     this.columnsNames.push(colDef.name);
    if (this.selectedColumnsDefinitions !== undefined) {
     this.selectedColumnsNames.push(colDef.displayTitle);
    }
 });
    this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
     return colDef.displayTitleAr;
 });
 }

masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataComptesFournisseursSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataComptesFournisseursSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataComptesFournisseursSource.filter = filterValue.trim().toLowerCase();
  if (this.dataComptesFournisseursSource.paginator) {
    this.dataComptesFournisseursSource.paginator.firstPage();
  }
}

loadAddCompteFournisseurComponent() {
  this.router.navigate(['/pms/finance/ajouterCompteFournisseur']) ;

}
loadEditCompteFournisseurComponent() {
  this.router.navigate(['/pms/finance/editerCompteFournisseur', this.selection.selected[ this.selection.selected.length - 1].code]) ;

}
supprimerComptesFournisseurs()
{
 
  if ( this.selection.selected.length !== 0) {
    this.deleteEnCours = true;
      this.selection.selected.forEach(function(compteFournisseur, index) {
          this.comptesFournisseursService.deleteCompteFournisseur(compteFournisseur.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.deleteEnCours = false;
            
            this.openSnackBar( ' Element(s) Supprimé(s)');
             this.loadData(null);
            }
      }, this);
    } else {
      this.openSnackBar('Aucun Compte Séléctionné !');
    }
}
refresh() {
  this.loadData(null);
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

changePage($event) {
  console.log('page event is ', $event);
 this.comptesFournisseursService.getPageComptesFournisseurs($event.pageIndex, $event.pageSize,
  this.compteFournisseurDataFilter ).subscribe((comptesFournisseurs) => {
    this.listComptesFournisseurs = comptesFournisseurs;
   const listElements = [];
     this.listComptesFournisseurs.forEach(compteFournisseur => {
      listElements.push( {code: compteFournisseur.code ,
       numeroCompteFournisseur: compteFournisseur.numeroCompteFournisseur,
       dateOuverture: compteFournisseur.dateOuverture ,
       dateCloture: compteFournisseur.dateCloture ,
       cinFournisseur: compteFournisseur.fournisseur.cin,
       nomFournisseur: compteFournisseur.fournisseur.nom,
       prenomFournisseur: compteFournisseur.fournisseur.prenom,
       raisonSocialFournisseur: compteFournisseur.fournisseur.raisonSociale,
       chiffreAffaires: compteFournisseur.chiffreAffaires ,
       profession: compteFournisseur.profession ,
       valeurAchats: compteFournisseur.valeurAchats ,
       valeurDettes: compteFournisseur.valeurDettes ,
       valeurPaiements: compteFournisseur.valeurPaiements ,
       etat: compteFournisseur.etat === 'Valide' ?  'Validé' :
       compteFournisseur.etat === 'Annule' ? 'Annulé' :
       compteFournisseur.etat === 'EnCoursValidation' ?
        'En Cours De Validation' :   compteFournisseur.etat === 'Cloture' ? 'Cloturé' :
       compteFournisseur.etat === 'Initial' ? 'Initiale' : 'Ouvert',
       description: compteFournisseur.description,

 } );

});
this.dataComptesFournisseursSource = new MatTableDataSource<CompteFournisseurElement>(listElements);
this.comptesFournisseursService.getTotalComptesFournisseurs(this.compteFournisseurDataFilter).
subscribe((response) => {
 console.log('Total credits is ', response);
  this.dataComptesFournisseursSource.paginator.length = response.totalComptesFournisseurs;
});
});
}
toggleFilterPanel() {
  this.showFilter = !this.showFilter;
}
toggleShowColumnsMultiSelect() {
 this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
}
loadFiltredData() {
 console.log('the filter is ', this.compteFournisseurDataFilter);
 this.loadData(this.compteFournisseurDataFilter);
}
activateFilterFournisseur()
{
  this.showFilterFournisseur = !this.showFilterFournisseur ;
  
}
}
