import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,
  AfterViewInit , AfterContentInit,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {CompteFournisseur} from './compte-fournisseur.model';

import {ComptesFournisseursService} from './comptes-fournisseurs.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { MatDatepickerInput } from '@angular/material/datepicker';
import { MatSnackBar } from '@angular/material';
import { MatDatepicker } from '@angular/material/datepicker';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-compte-fournisseur-edit',
  templateUrl: './compte-fournisseur-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class CompteFournisseurEditComponent implements OnInit  {

  private compteFournisseur: CompteFournisseur =  new CompteFournisseur();
  private tabsDisabled = true;
  private updateEnCours = false;
  private etatsCompteFournisseur: any[] = [
    {value: 'Initial', viewValue: 'Initiale'},
    {value: 'EnCoursValidation', viewValue: 'En Cours de Validation'},
    {value: 'Valide', viewValue: 'Validé'},
    {value: 'Cloture', viewValue: 'Cloturé'},
    {value: 'Annule', viewValue: 'Annulé'}
  ];
  constructor( private route: ActivatedRoute,
    private router: Router, private comptesFournisseursService: ComptesFournisseursService, private matSnackBar: MatSnackBar,
    public dialog: MatDialog
   ) {
  }
  ngOnInit() {
    const codeCompteFournisseur = this.route.snapshot.paramMap.get('codeCompteFournisseur');
    this.comptesFournisseursService.getCompteFournisseur(codeCompteFournisseur).subscribe((compteFournisseur) => {
             this.compteFournisseur = compteFournisseur;
          
             if (this.compteFournisseur.etat === 'Initial')
              {
                this.tabsDisabled = true;
                this.etatsCompteFournisseur = [
                  {value: 'Initial', viewValue: 'Initial'},
                  {value: 'Valide', viewValue: 'Validé'},
                  {value: 'Annule', viewValue: 'Annulé'},
                  
                ];
              }
              if (this.compteFournisseur.etat === 'Cloture')
                {
                  this.tabsDisabled = false;
                  this.etatsCompteFournisseur= [
                    {value: 'Cloture', viewValue: 'Cloturé'},
                    {value: 'Annule', viewValue: 'Annulé'},
                    
                  ];
                }
            if (this.compteFournisseur.etat === 'Valide')
              {
                this.tabsDisabled = false;
                this.etatsCompteFournisseur = [
                  {value: 'Annule', viewValue: 'Annulé'},
                  {value: 'Valide', viewValue: 'Validé'},
                  {value: 'Cloture', viewValue: 'Cloturé'},
                  
                ];
              }
              if (this.compteFournisseur.etat === 'Annule')
                {
                  this.etatsCompteFournisseur = [
                    {value: 'Annule', viewValue: 'Annulé'},
                    {value: 'Valide', viewValue: 'Validé'},
                  ];
                  this.tabsDisabled = true;
               
                }
 
    });
  }
 
  loadAddCompteFournisseurComponent() {
    this.router.navigate(['/pms/finance/ajouterCompteFournisseur']) ;
  }
  loadGridComptesFournisseurs() {
    this.router.navigate(['/pms/finance/comptesFournisseurs']) ;
  }
  supprimerCompteFournisseur() {
      this.comptesFournisseursService.deleteCompteFournisseur(this.compteFournisseur.code).subscribe((response) => {

        this.openSnackBar( ' Compte Fournisseur Supprimé');
        this.loadGridComptesFournisseurs() ;
            });
  }
updateCompteFournisseur() {
  this.updateEnCours = true;
  this.comptesFournisseursService.updateCompteFournisseur(this.compteFournisseur).subscribe((response) => {
    this.updateEnCours = false;
    if (response.error_message ===  undefined) {

      if (this.compteFournisseur.etat === 'Initial')
        {
          this.tabsDisabled = true;
          this.etatsCompteFournisseur = [
            {value: 'Initial', viewValue: 'Initial'},
            {value: 'Valide', viewValue: 'Validé'},
            {value: 'Annule', viewValue: 'Annulé'},
            
          ];
        }
        if (this.compteFournisseur.etat === 'Cloture')
          {
            this.tabsDisabled = false;
            this.etatsCompteFournisseur = [
              {value: 'Cloture', viewValue: 'Cloturé'},
              {value: 'Annule', viewValue: 'Annulé'},
              
            ];
          }
      if (this.compteFournisseur.etat === 'Valide')
        {
          this.tabsDisabled = false;
          this.etatsCompteFournisseur = [
            {value: 'Annule', viewValue: 'Annulé'},
            {value: 'Valide', viewValue: 'Validé'},
            {value: 'Cloture', viewValue: 'Cloturé'},
            
          ];
        }
        if (this.compteFournisseur.etat === 'Annule')
          {
            this.etatsCompteFournisseur = [
              {value: 'Annule', viewValue: 'Annulé'},
              {value: 'Valide', viewValue: 'Validé'},
            ];
            this.tabsDisabled = true;
         
          }
      this.openSnackBar( ' Compte Fournisseur mis à jour ');
    
    } else {
      this.openSnackBar( ' Erreurs:Opération Echouée ');
    }
});
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

 selectedTabChanged($event) {
  if ($event.index === 4) { // Composition tab was selected
   // this.loadListArticles();
   } else if ($event.index === 1) {
    //  this.loadListSchemasConfiguration() ;
   }
  }
}
