
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ComptesFournisseursService} from '../comptes-fournisseurs.service';
import {DocumentCompteFournisseur} from '../document-compte-fournisseur.model';

@Component({
    selector: 'app-dialog-add-document-compte-fournisseur',
    templateUrl: 'dialog-add-document-compte-fournisseur.component.html',
  })
  export class DialogAddDocumentCompteFournisseurComponent implements OnInit {
     private documentCompteFournisseur: DocumentCompteFournisseur;
     private saveEnCours = false;
     private typesDoc: any[] = [
      {value: 'FactureAchats', viewValue: 'Facture Achats'},
      {value: 'BonReception', viewValue: 'Bon Réception'},
      {value: 'RecuPayement', viewValue: 'Reçu Payement'},
      {value: 'Cheque', viewValue: 'Chèque'},
      {value: 'Traite', viewValue: 'Traite'},
      {value: 'Autre', viewValue: 'Autre'},
    ];
     constructor(  private comptesFournisseursService: ComptesFournisseursService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.documentCompteFournisseur = new DocumentCompteFournisseur();
    }
    saveDocumentCompteFournisseur() 
    {
      this.documentCompteFournisseur.codeCompteFournisseur = this.data.codeCompteFournisseur;
       console.log(this.documentCompteFournisseur);
        this.saveEnCours = true;
       this.comptesFournisseursService.addDocumentCompteFournisseur
       (this.documentCompteFournisseur).subscribe((response) => {
         this.saveEnCours = false;
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.documentCompteFournisseur = new DocumentCompteFournisseur();
             this.openSnackBar(  'Document Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs: Opération Echouée');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
     imageFace1Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentCompteFournisseur.imageFace1 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imageFace2Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentCompteFournisseur.imageFace2 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
  }
