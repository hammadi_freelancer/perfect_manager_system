
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ComptesFournisseursService} from '../comptes-fournisseurs.service';
import {TransactionCompteFournisseur} from '../transaction-compte-fournisseur.model';
import { TransactionCompteFournisseurElement } from '../transaction-compte-fournisseur.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-transactions-comptes-fournisseurs',
    templateUrl: 'dialog-list-transactions-comptes-fournisseurs.component.html',
  })
  export class DialogListTransactionsComptesFournisseursComponent implements OnInit {
     private listTransactionsComptesFournisseurs: TransactionCompteFournisseur[] = [];
     private  dataTransactionsComptesFournisseursSource: MatTableDataSource<TransactionCompteFournisseurElement>;
     private selection = new SelectionModel<TransactionCompteFournisseurElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private comptesFournisseursService: ComptesFournisseursService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      this.comptesFournisseursService.getTransactionsCompteFournisseurByCodeCompteFournisseur(this.data.codeCompteFournisseur).
      subscribe((listTr) => {
        this.listTransactionsComptesFournisseurs = listTr;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedTransactionsComptesFournisseurs: TransactionCompteFournisseur[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((trCC) => {
    return trCC.code;
  });
  this.listTransactionsComptesFournisseurs.forEach(trC => {
    if (codes.findIndex(code => code === trC.code) > -1) {
      listSelectedTransactionsComptesFournisseurs.push(trC);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listTransactionsComptesFournisseurs !== undefined) {

      this.listTransactionsComptesFournisseurs.forEach(tr => {
             listElements.push( {code: tr.code ,
          dateTransaction: tr.dateTransaction,
          type: tr.type,
          valeurTransaction: tr.valeurTransaction,
       
          
        } );
      });
    }
      this.dataTransactionsComptesFournisseursSource = new MatTableDataSource<TransactionCompteFournisseurElement>(listElements);
      this.dataTransactionsComptesFournisseursSource.sort = this.sort;
      this.dataTransactionsComptesFournisseursSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'valeurTransaction' , displayTitle: 'Valeur'},
     {name: 'type' , displayTitle: 'Type'},
     {name: 'dateTransaction' , displayTitle: 'Date'},
       
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataTransactionsComptesFournisseursSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataTransactionsComptesFournisseursSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataTransactionsComptesFournisseursSource.filter = filterValue.trim().toLowerCase();
    if (this.dataTransactionsComptesFournisseursSource.paginator) {
      this.dataTransactionsComptesFournisseursSource.paginator.firstPage();
    }
  }

}
