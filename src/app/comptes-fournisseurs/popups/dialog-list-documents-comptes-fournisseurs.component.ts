
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ComptesFournisseursService} from '../comptes-fournisseurs.service';
import {DocumentCompteFournisseur} from '../document-compte-fournisseur.model';
import { DocumentCompteFournisseurElement } from '../document-compte-fournisseur.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-documents-comptes-fournisseurs',
    templateUrl: 'dialog-list-documents-comptes-fournisseurs.component.html',
  })
  export class DialogListDocumentsComptesFournisseursComponent implements OnInit {
     private listDocumentsComptesFournisseurs: DocumentCompteFournisseur[] = [];
     private  dataDocumentsComptesFournisseursSource: MatTableDataSource<DocumentCompteFournisseurElement>;
     private selection = new SelectionModel<DocumentCompteFournisseurElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private comptesFournisseursService: ComptesFournisseursService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      this.comptesFournisseursService.
      getDocumentsComptesFournisseursByCodeCompteFournisseur(this.data.codeCompteFournisseur).subscribe((listDocumentsCC) => {
        this.listDocumentsComptesFournisseurs = listDocumentsCC;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }

  checkOneActionInvoked() {
    const listSelectedDocumentsFournisseurs: DocumentCompteFournisseur[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentCelement) => {
    return documentCelement.code;
  });
  this.listDocumentsComptesFournisseurs.forEach(documentCC=> {
    if (codes.findIndex(code => code === documentCC.code) > -1) {
      listSelectedDocumentsFournisseurs.push(documentCC);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsComptesFournisseurs !== undefined) {

      this.listDocumentsComptesFournisseurs.forEach(docCC => {
             listElements.push( {code: docCC.code ,
          dateReception: docCC.dateReception,
          numeroDocument: docCC.numeroDocument,
          typeDocument: docCC.typeDocument,
          description: docCC.description
        } );
      });
    }
      this.dataDocumentsComptesFournisseursSource = new MatTableDataSource<DocumentCompteFournisseurElement>(listElements);
      this.dataDocumentsComptesFournisseursSource.sort = this.sort;
      this.dataDocumentsComptesFournisseursSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    //  console.log('calling specifyListColumnsToBeDisplayed documents grid');
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
    
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
      console.log(this.columnsNames);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsComptesFournisseursSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsComptesFournisseursSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsComptesFournisseursSource.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsComptesFournisseursSource.paginator) {
      this.dataDocumentsComptesFournisseursSource.paginator.firstPage();
    }
  }
 




}
