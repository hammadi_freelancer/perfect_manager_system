
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ComptesFournisseursService} from '../comptes-fournisseurs.service';
import {TransactionCompteFournisseur} from '../transaction-compte-fournisseur.model';

@Component({
    selector: 'app-dialog-edit-transaction-compte-fournisseur',
    templateUrl: 'dialog-edit-transaction-compte-fournisseur.component.html',
  })
  export class DialogEditTransactionCompteFournisseurComponent implements OnInit {
     private transactionCompteFournisseur: TransactionCompteFournisseur;
     private updateEnCours = false;
 
     private typesTransactions: any[] = [
      {value: 'Ventes', viewValue: 'Ventes'},
      {value: 'Paiements', viewValue: 'Paiements'},
      {value: 'Credits', viewValue: 'Credits'},
     ]
   
     constructor(  private comptesFournisseursService: ComptesFournisseursService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {

        this.comptesFournisseursService.getTransactionCompteFournisseur
        (this.data.codeTransactionCompteFournisseur).subscribe((transaction) => {
            this.transactionCompteFournisseur = transaction;
     });
    }
    
    updateTransactionCompteFournisseur() 
    {
      this.updateEnCours = true;
       this.comptesFournisseursService.updateTransactionCompteFournisseur(this.transactionCompteFournisseur).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {

             this.openSnackBar(  'Transaction Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }


  }
