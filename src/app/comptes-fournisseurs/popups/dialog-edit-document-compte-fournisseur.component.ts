
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ComptesFournisseursService} from '../comptes-fournisseurs.service';
import {DocumentCompteFournisseur} from '../document-compte-fournisseur.model';

@Component({
    selector: 'app-dialog-edit-document-compte-fournisseur',
    templateUrl: 'dialog-edit-document-compte-fournisseur.component.html',
  })
  export class DialogEditDocumentCompteFournisseurComponent implements OnInit {
     private documentCompteFournisseur: DocumentCompteFournisseur;
     private updateEnCours = false;
     private typesDoc: any[] = [
      {value: 'FactureAchats', viewValue: 'Facture Achats'},
      {value: 'BonReception', viewValue: 'Bon Récéption'},
      {value: 'RecuPayement', viewValue: 'Reçu Payement'},
      {value: 'Cheque', viewValue: 'Chèque'},
      {value: 'Traite', viewValue: 'Traite'},
      {value: 'Autre', viewValue: 'Autre'},
    ];
     constructor(  private comptesFournisseursService: ComptesFournisseursService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit() {

 this.comptesFournisseursService.getDocumentCompteFournisseur(this.data.codeDocumentCompteFournisseur).subscribe((doc) => {
            this.documentCompteFournisseur = doc;
     });
    }
    
    updateDocumentCompteFournisseur() 
    {
      this.updateEnCours = true;
       this.comptesFournisseursService.updateDocumentCompteFournisseur(this.documentCompteFournisseur).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.openSnackBar(  'Document Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
     imageFace1Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentCompteFournisseur.imageFace1 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imageFace2Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentCompteFournisseur.imageFace2 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
  }
