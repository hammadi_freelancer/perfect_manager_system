
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {ComptesFournisseursService} from '../comptes-fournisseurs.service';
import {CompteFournisseur} from '../compte-fournisseur.model';
import {TransactionCompteFournisseur} from '../transaction-compte-fournisseur.model';

@Component({
    selector: 'app-dialog-add-transaction-compte-fournisseur',
    templateUrl: 'dialog-add-transaction-compte-fournisseur.component.html',
  })
  export class DialogAddTransactionCompteFournisseurComponent implements OnInit {
     private transactionCompteFournisseur: TransactionCompteFournisseur;
     private saveEnCours = false;
     private typesTransactions: any[] = [
      {value: 'Achats', viewValue: 'Achats'},
      {value: 'Paiements', viewValue: 'Paiements'},
      {value: 'Dettes', viewValue: 'Dettes'},
     ]
  
     constructor(  private comptesFournisseursService: ComptesFournisseursService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.transactionCompteFournisseur = new TransactionCompteFournisseur();
    }
    saveTransactionCompteFournisseur() 
    {
      this.transactionCompteFournisseur.codeCompteFournisseur = this.data.codeCompteFournisseur;
       this.saveEnCours = true;
       this.comptesFournisseursService.addTransactionCompteFournisseur(this.transactionCompteFournisseur).subscribe((response) => {
         this.saveEnCours = false;
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.transactionCompteFournisseur = new TransactionCompteFournisseur();
             this.openSnackBar(  'Transaction Enregistrée');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
 
  }
