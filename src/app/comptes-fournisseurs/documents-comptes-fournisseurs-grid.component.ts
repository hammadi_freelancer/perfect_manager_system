
import { Component, OnInit, Inject,Input, ViewChild, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {ComptesFournisseursService} from './comptes-fournisseurs.service';
import {CompteFournisseur} from './compte-fournisseur.model';

import {DocumentCompteFournisseur} from './document-compte-fournisseur.model';
import { DocumentCompteFournisseurElement } from './document-compte-fournisseur.interface';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogAddDocumentCompteFournisseurComponent } from './popups/dialog-add-document-compte-fournisseur.component';
import { DialogEditDocumentCompteFournisseurComponent } from './popups/dialog-edit-document-compte-fournisseur.component';
@Component({
    selector: 'app-documents-comptes-fournisseurs-grid',
    templateUrl: 'documents-comptes-fournisseurs-grid.component.html',
  })
  export class DocumentsComptesFournisseursGridComponent implements OnInit, OnChanges {
     private listDocumentsComptesFournisseurs: DocumentCompteFournisseur[] = [];
     private  dataDocumentsComptesFournisseursSource: MatTableDataSource<DocumentCompteFournisseurElement>;
     private selection = new SelectionModel<DocumentCompteFournisseurElement>(true, []);
    
     @Input()
     private compteFournisseur : CompteFournisseur

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private comptesFournisseursService: ComptesFournisseursService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.comptesFournisseursService.getDocumentsComptesFournisseursByCodeCompteFournisseur(this.compteFournisseur.code).
      subscribe((listDocumentsCC) => {
        this.listDocumentsComptesFournisseurs = listDocumentsCC;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
  }
  ngOnChanges()
  {
    this.specifyListColumnsToBeDisplayed();
    
    this.comptesFournisseursService.
    getDocumentsComptesFournisseursByCodeCompteFournisseur(this.compteFournisseur.code).subscribe((listDocumentsCC) => {
      this.listDocumentsComptesFournisseurs= listDocumentsCC;
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }

  checkOneActionInvoked() {
    const listSelectedDocumentsComptesFournisseurs: DocumentCompteFournisseur[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentCC) => {
    return documentCC.code;
  });
  this.listDocumentsComptesFournisseurs.forEach(document=> {
    if (codes.findIndex(code => code === document.code) > -1) {
      listSelectedDocumentsComptesFournisseurs.push(document);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsComptesFournisseurs !== undefined) {

      this.listDocumentsComptesFournisseurs.forEach(docC => {
             listElements.push( {code: docC.code ,
          dateReception: docC.dateReception,
          numeroDocument: docC.numeroDocument,
          typeDocument: docC.typeDocument,
          description: docC.description
        } );
      });
    }
      this.dataDocumentsComptesFournisseursSource = new MatTableDataSource<DocumentCompteFournisseurElement>(listElements);
      this.dataDocumentsComptesFournisseursSource.sort = this.sort;
      this.dataDocumentsComptesFournisseursSource.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    //  console.log('calling specifyListColumnsToBeDisplayed documents grid');
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
    
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'}, 
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
      console.log(this.columnsNames);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsComptesFournisseursSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsComptesFournisseursSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsComptesFournisseursSource.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsComptesFournisseursSource.paginator) {
      this.dataDocumentsComptesFournisseursSource.paginator.firstPage();
    }
  }
  showAddDocumentDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeCompteFournisseur : this.compteFournisseur.code
   };
 
   const dialogRef = this.dialog.open(DialogAddDocumentCompteFournisseurComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(docAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditDocumentDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeDocumentCompteFournisseur : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditDocumentCompteFournisseurComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(docAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Document Séléctionnée!');
    }
    }
    supprimerDocuments()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(doc, index) {
              this.comptesFournisseursService.deleteDocumentCompteFournisseur(doc.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Document Séléctionnée!');
        }
    }
    refresh() {
      this.comptesFournisseursService.getDocumentsComptesFournisseursByCodeCompteFournisseur(this.compteFournisseur.code).
      subscribe((listDocsComptesFournisseurs) => {
        this.listDocumentsComptesFournisseurs = listDocsComptesFournisseurs;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
