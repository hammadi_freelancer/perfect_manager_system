import { Injectable } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { CompteFournisseurNewComponent } from './compte-fournisseur-new.component';
import { CompteFournisseurEditComponent } from './compte-fournisseur-edit.component';
import { ComptesFournisseursGridComponent } from './comptes-fournisseurs-grid.component';
import { HomeComptesFournisseursComponent} from './home-comptes-fournisseurs.component' ;
import { FournisseursResolver} from '../achats/fournisseurs-resolver' ;


import {ModuleWithProviders  } from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';

 export const comptesFournisseursRoute: Routes = [
    {
        path: 'comptesFournisseurs',
        component: ComptesFournisseursGridComponent,
        resolve: {
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterCompteFournisseur',
        component: CompteFournisseurNewComponent,
        resolve : {
            fournisseurs: FournisseursResolver,
            
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerCompteFournisseur/:codeCompteFournisseur',
        component: CompteFournisseurEditComponent,
        resolve: {
            fournisseurs: FournisseursResolver,
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },

];
// export const ventesRoutingModule: ModuleWithProviders = RouterModule.forRoot(ventesRoute);
// commonRouting.providers = [{ provide: APP_BASE_HREF, useValue: '/common'} ] ;


