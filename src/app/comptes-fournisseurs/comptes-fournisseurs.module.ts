import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
 import { AppComponent } from '../app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ReactiveFormsModule } from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatMenuModule} from '@angular/material/menu';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {TableModule} from 'primeng/table';
import {MatSelectModule} from '@angular/material/select';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTabsModule} from '@angular/material/tabs';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MultiSelectModule} from 'primeng/multiselect';

import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import { RouterModule, provideRoutes} from '@angular/router';



import { CommonModule } from '../common/common.module';
import { SharedComponentModule } from '../shared-components/shared-components.module';



// comptes fournisseurs components
import { CompteFournisseurNewComponent } from './compte-fournisseur-new.component';
import { CompteFournisseurEditComponent } from './compte-fournisseur-edit.component';
import { ComptesFournisseursGridComponent } from './comptes-fournisseurs-grid.component';

import { DialogAddDocumentCompteFournisseurComponent } from './popups/dialog-add-document-compte-fournisseur.component';
import { DialogEditDocumentCompteFournisseurComponent } from './popups/dialog-edit-document-compte-fournisseur.component';
import { DialogListDocumentsComptesFournisseursComponent } from './popups/dialog-list-documents-comptes-fournisseurs.component';

import { DialogAddTransactionCompteFournisseurComponent } from './popups/dialog-add-transaction-compte-fournisseur.component';
import { DialogEditTransactionCompteFournisseurComponent } from './popups/dialog-edit-transaction-compte-fournisseur.component';
import { DialogListTransactionsComptesFournisseursComponent } from './popups/dialog-list-transactions-comptes-fournisseurs.component';

import { DocumentsComptesFournisseursGridComponent } from './documents-comptes-fournisseurs-grid.component';
import { TransactionsComptesFournisseursGridComponent} from './transactions-comptes-fournisseurs-grid.component';

import {MatSidenavModule} from '@angular/material/sidenav';
import {MatRadioModule} from '@angular/material/radio';



import {FormsModule } from '@angular/forms';
 import {APP_BASE_HREF} from '@angular/common';
 import { HomeComptesFournisseursComponent } from './home-comptes-fournisseurs.component' ;
/*const ENTITY_STATES = [
  ...commonRoute
];*/

 // const BASE_URL = [{provide: APP_BASE_HREF, useValue: '/common'}];
@NgModule({
  declarations: [
    CompteFournisseurEditComponent,
    CompteFournisseurNewComponent,
    ComptesFournisseursGridComponent,
    TransactionsComptesFournisseursGridComponent,
    DocumentsComptesFournisseursGridComponent,
    DialogListTransactionsComptesFournisseursComponent,
    DialogEditTransactionCompteFournisseurComponent,
    DialogAddTransactionCompteFournisseurComponent,
    DialogListDocumentsComptesFournisseursComponent,
    DialogEditDocumentCompteFournisseurComponent,
    DialogAddDocumentCompteFournisseurComponent,
    HomeComptesFournisseursComponent,

  ],
  imports: [
    BrowserModule, HttpClientModule, MatDatepickerModule, MatNativeDateModule,
     BrowserAnimationsModule, ReactiveFormsModule, MatAutocompleteModule,
    MatInputModule, MatCardModule, MatCheckboxModule, MatButtonModule, MatIconModule,
    MatMenuModule, MatProgressSpinnerModule, MatExpansionModule, MatSortModule, MatSelectModule, MatPaginatorModule, MatTableModule,
    FormsModule, SharedComponentModule, MatTabsModule, MatProgressBarModule, MultiSelectModule,
    // ventesRoutingModule,
    CommonModule, TableModule , MatDialogModule,
     MatSidenavModule, MatRadioModule
],
  providers: [MatNativeDateModule ,
  ],
  exports : [
    RouterModule,
    HomeComptesFournisseursComponent

  ],
  entryComponents : [
    DialogListTransactionsComptesFournisseursComponent,
    DialogEditTransactionCompteFournisseurComponent,
    DialogAddTransactionCompteFournisseurComponent,
    DialogListDocumentsComptesFournisseursComponent,
    DialogEditDocumentCompteFournisseurComponent,
    DialogAddDocumentCompteFournisseurComponent,


  ],
  bootstrap: [AppComponent]
})
export class ComptesFournisseursModule { }
