
export interface TransactionCompteFournisseurElement {
         code: string ;
         codeCompteFournisseur: string ;
         dateTransaction: string;
         valeurTransaction: number;
         type: string;
          description?: string;

}
