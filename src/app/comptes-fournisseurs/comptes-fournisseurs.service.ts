import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';
import { HttpParams } from '@angular/common/http';

import {CompteFournisseur} from './compte-fournisseur.model' ;
// import { TrancheCredit } from './tranche-credit.model';
import { DocumentCompteFournisseur } from './document-compte-fournisseur.model';
import { TransactionCompteFournisseur } from './transaction-compte-fournisseur.model';

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_PAGE_COMPTES_FOURNISSEURS= 'http://localhost:8082/finance/comptesFournisseurs/getPageComptesFournisseurs';
const URL_GET_COMPTE_FOURNISSEUR = 'http://localhost:8082/finance/comptesFournisseurs/getCompteFournisseur';
const  URL_ADD_COMPTE_FOURNISSEUR  = 'http://localhost:8082/finance/comptesFournisseurs/addCompteFournisseur';
const  URL_UPDATE_COMPTE_FOURNISSEUR  = 'http://localhost:8082/finance/comptesFournisseurs/updateCompteFournisseur';
const  URL_DELETE_COMPTE_FOURNISSEUR  = 'http://localhost:8082/finance/comptesFournisseurs/deleteCompteFournisseur';
const  URL_GET_TOTAL_COMPTES_FOURNISSEURS = 'http://localhost:8082/finance/comptesFournisseurs/getTotalComptesFournisseurs';



  const  URL_ADD_DOCUMENT_COMPTE_FOURNISSEUR  = 
  'http://localhost:8082/finance/documentsComptesFournisseurs/addDocumentCompteFournisseur';
  const  URL_GET_LIST_DOCUMENT_COMPTE_FOURNISSEUR  =
   'http://localhost:8082/finance/documentsComptesFournisseurs/getDocumentsComptesFournisseurs';
  const  URL_DELETE_DOCUCMENT_COMPTE_FOURNISSEUR  = 
  'http://localhost:8082/finance/documentsComptesFournisseurs/deleteDocumentCompteFournisseur';
  const  URL_UPDATE_DOCUMENT_COMPTE_FOURNISSEUR  = 
  'http://localhost:8082/finance/documentsComptesFournisseurs/updateDocumentCompteFournisseur';
  const URL_GET_DOCUMENT_COMPTE_FOURNISSEUR  =
   'http://localhost:8082/finance/documentsComptesFournisseurs/getDocumentCompteFournisseur';
  const URL_GET_LIST_DOCUMENT_COMPTE_FOURNISSEUR_BY_CODE_COMPTE_FOURNISSEUR =
   'http://localhost:8082/finance/documentsComptesFournisseurs/getDocumentsComptesFournisseursByCodeCompteFournisseur';


   const  URL_ADD_TRANSACTION_COMPTE_FOURNISSEUR  = 
   'http://localhost:8082/finance/transactionsComptesFournisseurs/addTransactionCompteFournisseur';
   const  URL_GET_LIST_TRANSACTION_COMPTE_FOURNISSEUR  = 
   'http://localhost:8082/finance/transactionsComptesFournisseurs/getTransactionsComptesFournisseurs';
   const  URL_DELETE_TRANSACTION_COMPTE_FOURNISSEUR  =
    'http://localhost:8082/finance/transactionsComptesFournisseurs/deleteTransactionCompteFournisseur';
   const  URL_UPDATE_TRANSACTION_COMPTE_FOURNISSEUR  = 
   'http://localhost:8082/finance/transactionsComptesFournisseurs/updateTransactionCompteFournisseur';
   const URL_GET_TRANSACTION_COMPTE_FOURNISSEUR  = 
   'http://localhost:8082/finance/transactionsComptesFournisseurs/getTransactionCompteFournisseur';
   const URL_GET_LIST_TRANSACTIONS_COMPTE_FOURNISSEUR_BY_CODE_COMPTE_FOURNISSEUR  =
    'http://localhost:8082/finance/transactionsComptesFournisseurs/getTransactionsComptesFournisseursByCodeCompteFournisseur';

@Injectable({
  providedIn: 'root'
})
export class ComptesFournisseursService {

  constructor(private httpClient: HttpClient) {

   }

   getTotalComptesFournisseurs(filter): Observable<any> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('filter', filterStr);
        return this.httpClient
        .get<any>(URL_GET_TOTAL_COMPTES_FOURNISSEURS, {params}) ;
       }
   getPageComptesFournisseurs(pageNumber, nbElementsPerPage, filter): Observable<CompteFournisseur[]> {


    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('pageNumber', pageNumber)
    .set('nbElementsPerPage', nbElementsPerPage)
    .set('filter', filterStr);
    return this.httpClient
    .get<CompteFournisseur[]>(URL_GET_PAGE_COMPTES_FOURNISSEURS, {params});
   }
   addCompteFournisseur(compteFournisseur: CompteFournisseur): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_COMPTE_FOURNISSEUR, compteFournisseur);
  }
  updateCompteFournisseur(compteFournisseur: CompteFournisseur): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_COMPTE_FOURNISSEUR, compteFournisseur);
  }
  deleteCompteFournisseur(codeCompteFournisseur): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_COMPTE_FOURNISSEUR + '/' + codeCompteFournisseur);
  }
  getCompteFournisseur(codeCompteFournisseur): Observable<CompteFournisseur> {
    return this.httpClient.get<CompteFournisseur>(URL_GET_COMPTE_FOURNISSEUR + '/' + codeCompteFournisseur);
  }

// documents comptes fournisseurs

  addDocumentCompteFournisseur(documentCompteFournisseur: DocumentCompteFournisseur): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_DOCUMENT_COMPTE_FOURNISSEUR, documentCompteFournisseur);
    
  }
  updateDocumentCompteFournisseur(docCompteFournisseur: DocumentCompteFournisseur): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_DOCUMENT_COMPTE_FOURNISSEUR, docCompteFournisseur);
  }
  deleteDocumentCompteFournisseur(codeDocumentCompteFournisseur): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_DOCUCMENT_COMPTE_FOURNISSEUR + '/' + codeDocumentCompteFournisseur);
  }
  getDocumentCompteFournisseur(codeDocumentCompteFournisseur): Observable<DocumentCompteFournisseur> {
    return this.httpClient.get<DocumentCompteFournisseur>
    (URL_GET_DOCUMENT_COMPTE_FOURNISSEUR + '/' + codeDocumentCompteFournisseur);
  }
  getDocumentsComptesFournisseurs(): Observable<DocumentCompteFournisseur[]> {
    return this.httpClient
    .get<DocumentCompteFournisseur[]>(URL_GET_LIST_DOCUMENT_COMPTE_FOURNISSEUR);
   }
  getDocumentsComptesFournisseursByCodeCompteFournisseur(codeCompteFournisseur): Observable<DocumentCompteFournisseur[]> {
    return this.httpClient.get<DocumentCompteFournisseur[]>(
      URL_GET_LIST_DOCUMENT_COMPTE_FOURNISSEUR_BY_CODE_COMPTE_FOURNISSEUR + '/' + codeCompteFournisseur);
  }

// transactions comptes fournisseurs 


addTransactionCompteFournisseur(transactionCompteFournisseur: TransactionCompteFournisseur): Observable<any> {
  return this.httpClient.post<any>(URL_ADD_TRANSACTION_COMPTE_FOURNISSEUR, transactionCompteFournisseur);
  
}
updateTransactionCompteFournisseur(transactionCompteFournisseur: TransactionCompteFournisseur): Observable<any> {
  return this.httpClient.put<any>(URL_UPDATE_TRANSACTION_COMPTE_FOURNISSEUR, transactionCompteFournisseur);
}
deleteTransactionCompteFournisseur(codeTransactionCompteFournisseur): Observable<any> {
  return this.httpClient.delete<any>(URL_DELETE_TRANSACTION_COMPTE_FOURNISSEUR + '/' + codeTransactionCompteFournisseur);
}
getTransactionCompteFournisseur(codeTransactionCompteFournisseur): Observable<TransactionCompteFournisseur> {
  return this.httpClient.get<TransactionCompteFournisseur>(URL_GET_TRANSACTION_COMPTE_FOURNISSEUR + '/' + codeTransactionCompteFournisseur);
}
getTransactionsComptesFournisseurs(): Observable<TransactionCompteFournisseur[]> {
  return this.httpClient
  .get<TransactionCompteFournisseur[]>(URL_GET_LIST_TRANSACTION_COMPTE_FOURNISSEUR);
 }
getTransactionsCompteFournisseurByCodeCompteFournisseur(codeCompteFournisseur): Observable<TransactionCompteFournisseur[]> {
  return this.httpClient.get<TransactionCompteFournisseur[]>(URL_GET_LIST_TRANSACTIONS_COMPTE_FOURNISSEUR_BY_CODE_COMPTE_FOURNISSEUR
    + '/' + codeCompteFournisseur);
}





}
