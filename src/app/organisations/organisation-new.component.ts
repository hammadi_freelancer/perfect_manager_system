import { Component, OnInit, Input, EventEmitter, Output,
    AfterViewChecked
    } from '@angular/core';
    // import { ArticleService } from './article.service';
    import {Organisation} from './organisation.model';
    import {OrganisationService} from './organisation.service';
    import {FormControl} from '@angular/forms';
    import {map, startWith} from 'rxjs/operators';
    import {Observable} from 'rxjs';
    import { Router, ActivatedRoute, ParamMap } from '@angular/router';
    import {MatSnackBar} from '@angular/material';
    import { MotifView } from './motif-view.interface';
    
    @Component({
      selector: 'app-organisation-new',
      templateUrl: './organisation-new.component.html',
      // styleUrls: ['./players.component.css']
    })
export class OrganisationNewComponent implements OnInit {

    private organisation: Organisation = new Organisation();
    @Input()
    private listOrganisations: any = [] ;
 
    private file: any;

  ;
      constructor( private route: ActivatedRoute,
        private router: Router , private organisationService: OrganisationService, private matSnackBar: MatSnackBar ) {
      }
      ngOnInit() {
     
      }
    saveOrganisation() {
       
        console.log(this.organisation);
        this.organisationService.addOrganisation(this.organisation).subscribe((response) => {
            if (response.error_message ===  undefined) {
              // this.save.emit(response);
              this.organisation = new Organisation();
              this.openSnackBar(  'Organisation Enregistré');
            } else {
             // show error message
             this.openSnackBar(  'Erreurs!');
             
            }
        });
    }
    loadGridOrganisations() {
      this.router.navigate(['/pms//administration/organisations']) ;
    }
    fileChanged($event) {
        this.file = $event.target.files[0];
        const fileReader = new FileReader();
        fileReader.onload = (e) => {
          console.log(fileReader.result);
           this.organisation.logo = fileReader.result;
          // this.filesData[indexFile] = fileData;
        };
        fileReader.readAsDataURL(this.file);
       }
       vider() {
         this.organisation = new Organisation();
       }
       openSnackBar(messageToDisplay) {
        this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
            verticalPosition: 'top'});
       }
 
    }
