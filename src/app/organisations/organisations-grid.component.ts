import { Component, OnInit, Output, ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {Organisation} from './organisation.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {OrganisationService} from './organisation.service';

import { OrganisationElement } from './organisation.interface';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
// import { DialogAddDocumentClientComponent} from './popups';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {OrganisationDataFilter } from './organisation-data.filter';
import { SelectItem } from 'primeng/api';
@Component({
  selector: 'app-organisations-grid',
  templateUrl: './organisations-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class OrganisationsGridComponent implements OnInit {

  private organisations: Organisation =  new Organisation();
  private showFilter = false;
  private showSelectColumnsMultiSelect = false;
  private organisationDataFilter = new OrganisationDataFilter();
  private selection = new SelectionModel<Organisation>(true, []);
  


 selectedOrganisation: Organisation ;

private deleteEnCours = false;

 @Input()
 private listOrganisations: any = [] ;

 // listClientsOrgin: any = [];
 private checkedAll: false;
 private showParticulier = false;
 private showEntreprise = false;
 private  dataOrganisationsSource: MatTableDataSource<OrganisationElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private columnsDefinitions: ColumnDefinition[] = [];
 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay :ColumnDefinition[] = [];

 private columnsToSelect : SelectItem[];
  private  selectedColumnsDefinitions: string[];

 private selectedColumnsNames: string[];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];

  constructor( private route: ActivatedRoute, private organisationsService: OrganisationService,
    private router: Router, private matSnackBar: MatSnackBar, private dialog: MatDialog ) {
  }
  ngOnInit() {
    this.defaultColumnsDefinitions = [
      {name: 'code' , displayTitle: 'Code'},
    {name: 'raisonSociale' , displayTitle: 'Raison Sociale'},
    {name: 'chiffreAffaire' , displayTitle: 'Chiffre Affaire'},
    {name: 'matriculeFiscale' , displayTitle: 'Matricule Fiscale'},
    {name: 'registreCommerce' , displayTitle: 'Registre Commerce'},
    {name: 'responsable' , displayTitle: 'Responsable'},
    {name: 'telFix' , displayTitle: 'Téléphone'},
    {name: 'email' , displayTitle: 'email'},
    {name: 'adresse' , displayTitle: 'adresse'}
  ];
    
    this.columnsToSelect = [
      {label: 'Code', value: 'code', title: 'Code'},

      {label: 'Raison Sociale', value: 'raisonSociale', title: 'Raison Sociale'},

      {label: 'Chiffre Affaire', value: 'chiffreAffaire', title: 'Chiffre Affaire'},
      {label: 'Matricule Fiscale', value: 'matriculeFiscale', title: 'Matricule Fiscale'},
      {label: 'Registre Commerce', value: 'registreCommerce', title: 'Registre Commerce'},
      {label: 'Responsable', value: 'responsable', title: 'Responsable'},
      {label: 'Téléphone', value: 'telFix', title: 'Téléphone'},
      {label: 'Email', value: 'email', title: 'Email'},
      {label: 'Adresse', value: 'adresse', title: 'Adresse'}
      
      
  ];
  this.selectedColumnsDefinitions = [
    'code',
   'raisonSociale',
   'chiffreAffaire',
   'responsable',
   'telFix',
   'adresse'
    ];


    this.organisationsService.getPageOrganisations(0, 5, null).subscribe((orgs) => {
      this.listOrganisations= orgs;
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });
  

  }
  deleteOrganisation(organisation) {
     // console.log('call delete !', client );
    this.organisationsService.deleteOrganisation(organisation.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData(null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.organisationsService.getPageOrganisations(0, 5, filter).subscribe((orgs) => {
    this.listOrganisations = orgs;
    // this.listClients[this.listClients.length] = this.lastClientAdded;
    this.transformDataToByConsumedByMatTable();
   });
  // this.listClientsOrgin = this.listClients ;
}


checkOneActionInvoked() {
  const listSelectedOrganisations: Organisation[] = [];

if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((orgElement) => {
  return orgElement.code;
});
this.listOrganisations.forEach(r => {
  if (codes.findIndex(code => code === r.code) > -1) {
    listSelectedOrganisations.push(r);
  }
  });
}
// this.select.emit(listSelectedClients);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listOrganisations !== undefined) {
    this.listOrganisations.forEach(org => {
      listElements.push( 
        {
          code: org.code ,
          raisonSociale: org.raisonSociale ,
          chiffreAffaire: org.chiffreAffaire,
          responsable: org.responsable,
          telFix: org.telFix,
          adresse: org.adresse,
          email: org.email,
          
      } 
    );
    });
  }

    this.dataOrganisationsSource = new MatTableDataSource<OrganisationElement>(listElements);
    this.dataOrganisationsSource.sort = this.sort;
    this.dataOrganisationsSource.paginator = this.paginator;
    this.organisationsService.getTotalOrganisations(this.organisationDataFilter).subscribe((response) => {
     //  console.log('Total Clients   is ', response);
     if (this.dataOrganisationsSource.paginator !== undefined) {
       this.dataOrganisationsSource.paginator.length = response['totalOrganisations'];
     }
     });
}

masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
    //   this.dataAccessRightsSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataOrganisationsSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataOrganisationsSource.filter = filterValue.trim().toLowerCase();
  if (this.dataOrganisationsSource.paginator) {
    this.dataOrganisationsSource.paginator.firstPage();
  }
}
loadAddOrganisationComponent() {
  this.router.navigate(['/pms/administration/ajouterOrganisation']) ;

}
loadEditOrganisationComponent() {
  if (this.selection.selected.length === 0) {
    this.openSnackBar(' Aucune Organisation Séléctionnée sélectionnée' );
    // this.loadData();
    } else
   {
  this.router.navigate(['/pms/administration/editerOrganisation', this.selection.selected[ this.selection.selected.length - 1].code]) ;
    }
}
supprimerOrganisations()
{
  if ( this.selection.selected.length !== 0) {
    this.deleteEnCours = true;
      this.selection.selected.forEach(function(org, index) {
          this.organisationService.deleteOrganisation(org.code).subscribe((response) => {
            if (this.selection.selected.length - 1 === index) {
              this.deleteEnCours = false;
              this.openSnackBar( ' Element(s) Supprimé(s)');
               this.loadData(null);
              }
          });
         
      }, this);
    } else {
      this.openSnackBar('Aucun Droit Séléctionné!');
    }
}
refresh()
{
  this.loadData(null);
  
}
  openSnackBar(messageToDisplay) {
 this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
     verticalPosition: 'top'});
}

changePage($event) {
  console.log('page event is ', $event);
 this.organisationsService.getPageOrganisations($event.pageIndex, $event.pageSize,
  this.organisationDataFilter)
 .subscribe((org) => {
    this.listOrganisations = org;
    const listElements = [];
 if (this.listOrganisations !== undefined) {
    this.listOrganisations.forEach(org => {
      listElements.push( {
        code: org.code ,
        raisonSociale: org.raisonSociale ,
        chiffreAffaire: org.chiffreAffaire,
        responsable: org.responsable,
        telFix: org.telFix,
        adresse: org.adresse,
        email: org.email,
      
      } 
      );
    });
  }
     this.dataOrganisationsSource = new MatTableDataSource<OrganisationElement>(listElements);
     this.organisationsService.getTotalOrganisations(this.organisationDataFilter).
     subscribe((response) => {
      if (this.dataOrganisationsSource.paginator) {
        
       this.dataOrganisationsSource.paginator.length = response.totalOrganisations;
      }
     });

    });
}
toggleFilterPanel() {
  this.showFilter = !this.showFilter;
}
toggleShowColumnsMultiSelect() {
 this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
}
loadFiltredData() {
 // console.log('the filter is ', this.accessRightDataFilter);
 this.loadData(this.organisationDataFilter);
}

specifyListColumnsToBeDisplayed() {
  
   console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
   this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
 ?
    this.defaultColumnsDefinitions : [];
    this.selectedColumnsNames = [];
    
    if (this.selectedColumnsDefinitions !== undefined)
     {
       this.selectedColumnsDefinitions.forEach((colName) => { 
            const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
            const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
            const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
            this.columnsDefinitionsToDisplay.push(colDef);
           });
     }
    this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
   this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
              return colDef.displayTitle;
    });
    console.log('columns to disp:', this.columnsDefinitionsToDisplay);
    this.columnsNames = [];
    this.columnsNames[0] = 'select';
    this.columnsDefinitionsToDisplay .map((colDef) => {
     this.columnsNames.push(colDef.name);
    if (this.selectedColumnsDefinitions !== undefined) {
     this.selectedColumnsNames.push(colDef.displayTitle);
    }
 });
    this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
     return colDef.displayTitleAr;
 });
 }






}
