

export class OrganisationDataFilter {
 
        code: string;
        raisonSociale: string;
        chiffreAffaire: number;
        registreCommerciale: number;
        adresse: string;
        domaineActivite: string;
        mobile: string;
        telFix: string;
        email: string;
        responsable: string;
}
