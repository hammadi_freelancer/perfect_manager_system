// type Motif = 'VenteEspece'| 'VenteVirement' | 'PayementCredit' | 'VirementCredit' | 'FinancementInterne' | 'FinancementExterne';

export interface OrganisationElement {
         code: string ;
         description: string;
         chiffreAffaire: number;
         raisonSociale: string;
         responsable: string;
         email: string;
         mobile: string;
         telFix: string;
         adresse: string;
         domaineActivite: string;
        }
        