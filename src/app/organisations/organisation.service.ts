import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';

import {Organisation} from './organisation.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';

const  URL_GET_LIST_ORGANISATIONS = 'http://localhost:8082/organisations/getOrganisations';
const URL_ADD_ORGANISATION = 'http://localhost:8082/organisations/addOrganisation';
const URL_GET_ORGANISATION = 'http://localhost:8082/organisations/getOrganisation';

const URL_UPDATE_ORGANISATION = 'http://localhost:8082/organisations/updateOrganisation';
const URL_DELETE_ORGANISATION = 'http://localhost:8082/organisations/deleteOrganisation';
const  URL_GET_PAGE_ORGANISATIONS = 'http://localhost:8082/organisations/getPageOrganisations';
const  URL_GET_TOTAL_ORGANISATIONS = 'http://localhost:8082/organisations/getTotalOrganisations';

@Injectable({
  providedIn: 'root'
})
export class OrganisationService {



  constructor(private httpClient: HttpClient) {

   }

   getOrganisations(): Observable<Organisation[]> {
    return this.httpClient
    .get<Organisation[]>(URL_GET_LIST_ORGANISATIONS);
   }
 
   addOrganisation(organisation: Organisation): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_ORGANISATION, organisation);
  }
  updateOrganisation(organisation: Organisation): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_ORGANISATION, organisation);
  }
  deleteOrganisation(codeOrganisation): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_ORGANISATION + '/' + codeOrganisation);
  }
  getOrganisation(codeOrganisation): Observable<Organisation> {
    return this.httpClient.get<Organisation>(URL_GET_ORGANISATION + '/' + codeOrganisation);
  }



  getPageOrganisations(pageNumber, nbElementsPerPage, filter): Observable<Organisation[]> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('pageNumber', pageNumber)
    .set('nbElementsPerPage', nbElementsPerPage)
    .set('sort', 'name')
    .set('filter', filterStr);
    return this.httpClient
    .get<Organisation[]>(URL_GET_PAGE_ORGANISATIONS, {params});
   }
   getTotalOrganisations(filter): Observable<any> {
    
        const filterStr = JSON.stringify(filter);
        const params = new HttpParams()
        .set('filter', filterStr);
        return this.httpClient
        .get<any>(URL_GET_TOTAL_ORGANISATIONS, {params});
    }


}
