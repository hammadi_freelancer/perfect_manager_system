

import { BaseEntity } from '../common/base-entity.model' ;

// type Motif = 'AchatEspece'| 'AchatVirement' | 'PayementCredit' | 'VirementCredit';

export class Organisation extends BaseEntity {
   constructor(
       public code?: string,
       public raisonSociale?: string,
       public chiffreAffaire?: number,
       public registreCommerciale?: number,
       public adresse?: string,
       public domaineActivite?: string,
       public mobile?: string ,
       public telFix?: string,
       public email?: string ,
       public responsable?: string,
       public logo?: any
       
) {
    super();
}
}
