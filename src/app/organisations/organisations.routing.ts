import { Injectable } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { OrganisationsGridComponent } from './organisations-grid.component';
import { OrganisationEditComponent } from './organisation-edit.component';
import { OrganisationNewComponent } from './organisation-new.component';

 
import {ModuleWithProviders  } from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';

export const organisationsRoute: Routes = [
    {
        path: 'organisations',
        component: OrganisationsGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterOrganisation',
        component: OrganisationNewComponent,
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerOrganisation/:codeOrganisation',
        component: OrganisationEditComponent,
        resolve: {
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    }
];



