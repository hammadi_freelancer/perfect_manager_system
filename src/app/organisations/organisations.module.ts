import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
 import { AppComponent } from '../app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ReactiveFormsModule } from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatMenuModule} from '@angular/material/menu';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {TableModule} from 'primeng/table';
import {MatSortModule} from '@angular/material/sort';
import {MatSelectModule} from '@angular/material/select';
import {MatTabsModule} from '@angular/material/tabs';
import {PickListModule} from 'primeng/picklist';
import {MultiSelectModule} from 'primeng/multiselect';

import {MatPaginatorModule} from '@angular/material/paginator';
import { RouterModule, provideRoutes} from '@angular/router';
// import { CommunicationService } from './magasins/communication.service';
import { CommonModule } from '../common/common.module';
import { SharedComponentModule } from '../shared-components/shared-components.module';

import {MatSidenavModule} from '@angular/material/sidenav';
import {MatRadioModule} from '@angular/material/radio';





// import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

//  import { ArticleComponent } from './articles/article.component';
// import { ClientComponent } from './clients/client.component';
// import { PayementComponent } from './payements/payement.component';
// import { PayementsGridComponent} from './payements/payements-grid.component';
// import { ClientsGridComponent} from './clients/clients-grid.component';
// import { ArticlesGridComponent} from './articles/articles-grid.component';
// import { GestionClientsComponent} from './clients/gestion-clients.component';
 
// import { PayementService } from './payements/payement.service';
// import { UtilisateurComponent } from './utilisateurs/utilisateur.component';
import {FormsModule } from '@angular/forms';
// import { commonRoute } from './common.route';
 import {APP_BASE_HREF} from '@angular/common';
 import { organisationsRoute} from './organisations.routing' ;
 import { HomeOrganisationComponent } from './home-organisations.component' ;
 import { OrganisationsGridComponent } from './organisations-grid.component';
 import { OrganisationEditComponent } from './organisation-edit.component';
 import { OrganisationNewComponent } from './organisation-new.component';
/*const ENTITY_STATES = [
  ...commonRoute
];*/

 // const BASE_URL = [{provide: APP_BASE_HREF, useValue: '/common'}];
@NgModule({
  declarations: [
     OrganisationsGridComponent,
     OrganisationEditComponent,
     OrganisationNewComponent,
     HomeOrganisationComponent
    
  ],
  imports: [
    BrowserModule, HttpClientModule, MatDatepickerModule, MatNativeDateModule,
     BrowserAnimationsModule, ReactiveFormsModule, MatAutocompleteModule,
    MatInputModule, MatCardModule, MatCheckboxModule, MatButtonModule, MatIconModule,
    MatMenuModule, MatProgressSpinnerModule, MatSortModule, MatPaginatorModule, MatTableModule,
    FormsModule, SharedComponentModule, MatSelectModule, MatDatepickerModule,
    MultiSelectModule,
   //  stocksRoutingModule,
    CommonModule, TableModule, MatSidenavModule, MatRadioModule
],
  providers: [MatNativeDateModule  // BASE_URL
  ],
  exports : [
    RouterModule,
    HomeOrganisationComponent
    // GestionClientsComponent,
    //  GestionDocumentsTypesComponent,
    //  DocumentTypesGridComponent,
   //  DocumentTypeComponent,

  ],
  bootstrap: [AppComponent]
})
export class OrganisationsModule { }
