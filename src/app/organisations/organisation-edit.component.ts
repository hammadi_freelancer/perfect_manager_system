import { Component, OnInit, Input, EventEmitter, Output, AfterContentChecked,
AfterViewChecked
} from '@angular/core';
// import { ArticleService } from './article.service';
import {Organisation} from './organisation.model';
import {OrganisationService} from './organisation.service';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {MatSnackBar} from '@angular/material';
// import { MotifView } from './motif-view.interface';
@Component({
  selector: 'app-organisation-edit',
  templateUrl: './organisation-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class OrganisationEditComponent implements OnInit, AfterContentChecked {

// private client: Client = new Client();


private organisation: Organisation = new Organisation();
/*private motifs: MotifView[] = [
  {value: 'AchatsEspece', viewValue: 'Achats Espèce'},
  {value: 'AchatsVirement', viewValue: 'Achats Virement'},
  {value: 'PayementCredit', viewValue: 'Payement Credit'},
  {value: 'PayementEmployees', viewValue: 'Payement Employées'},
  
  
];*/



  constructor( private route: ActivatedRoute,
    private router: Router , private organisationService: OrganisationService, private matSnackBar: MatSnackBar,
    
   ) {
  }
  ngOnInit() {
      const codeOrganisation = this.route.snapshot.paramMap.get('codeOrganisation');
      this.organisationService.getOrganisation(codeOrganisation).subscribe((organisation) => {
               this.organisation = organisation;
      });
     
  }
  loadAddOrganisationComponent() {
    this.router.navigate(['/pms/organisations/ajouterOrganisation']) ;
  
  }
  loadGridOrganisations() {
    this.router.navigate(['/pms/organisations']) ;
  }
  supprimerOrganisation() {
     
      this.organisationService.deleteOrganisation(this.organisation.code).subscribe((response) => {

        this.openSnackBar( ' Element Supprimé');
        this.loadGridOrganisations() ;
            });
  }
updateOrganisation() {

  console.log(this.organisation);
  this.organisationService.updateOrganisation(this.organisation).subscribe((response) => {
    if (response.error_message ===  undefined) {
      console.log('the response of update is ', response );
      this.openSnackBar( ' Element mis à jour ');
      
      //  this.save.emit(response);
      // this.client = new Client();
      // this.modeUpdate = false;
    } else {
      this.openSnackBar( ' Erreurs.. ');
      
     // show error message
    }
});
}

 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

ngAfterContentChecked() {

}
}


