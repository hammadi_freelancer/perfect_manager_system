export interface InventaireElement {
         code: string ;
         numeroInventaire: string;
         dateInventaire: string ;
         periodeFrom: string;
         periodeTo: string;
         montantVentes: number;
         montantAchats: number;
         montantRH: number;
         montantCharges: number;
         etat: string;
         description: string;
        }
