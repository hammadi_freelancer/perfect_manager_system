
import { BaseEntity } from '../common/base-entity.model' ;

import { Article } from '../stocks/articles/article.model' ;
import { UnMesure} from '../stocks/un-mesures/un-mesure.model' ;

 type Etat = 'Initial' | 'Valide' | 'Annule'
 
 
export class StockInventaire extends BaseEntity {
    constructor(
        public code?: string,
        public numeroInventaire?: string,
        public codeInventaire?: string,
        public stockFinal?: number,
        public stockInitial?: number,
        public valeurStockInitial?: number, 
         public article?: Article,
         public unMesure?: UnMesure,
         public valeurStockFinal?: number,
          public etat?: Etat,
     ) {
         super();
        
       }
  
}
