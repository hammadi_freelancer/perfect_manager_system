import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,
  AfterViewInit , AfterContentInit,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {Inventaire} from './inventaire.model';

import {InventairesService} from './inventaires.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { MatDatepickerInput } from '@angular/material/datepicker';
import { MatSnackBar } from '@angular/material';
import { MatDatepicker } from '@angular/material/datepicker';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-inventaire-edit',
  templateUrl: './inventaire-edit.component.html',
  // styleUrls: ['./players.component.css']
})
export class InventaireEditComponent implements OnInit  {

  private inventaire: Inventaire =  new Inventaire();
  private tabsDisabled = true;
  private updateEnCours = false;
  private etatsInventaire: any[] = [
    {value: 'Ouvert', viewValue: 'Ouvert'},
    {value: 'Initial', viewValue: 'Initiale'},
    {value: 'EnCoursValidation', viewValue: 'En Cours de Validation'},
    {value: 'Valide', viewValue: 'Validé'},
    {value: 'Cloture', viewValue: 'Cloturé'},
    {value: 'Annule', viewValue: 'Annulé'}
  ];
  constructor( private route: ActivatedRoute,
    private router: Router, private inventairesService: InventairesService, private matSnackBar: MatSnackBar,
    public dialog: MatDialog
   ) {
  }
  ngOnInit() {
    const codeInventaire= this.route.snapshot.paramMap.get('codeInventaire');
    this.inventairesService.getInventaire(codeInventaire).subscribe((inventaire) => {
             this.inventaire = inventaire;
             console.log('the date is ');
             if (this.inventaire.etat === 'Initial')
              {
                this.tabsDisabled = true;
                this.etatsInventaire = [
                  {value: 'Initial', viewValue: 'Initial'},
                  {value: 'Valide', viewValue: 'Validé'},
                  {value: 'Annule', viewValue: 'Annulé'},
                  
                ];
              }
              if (this.inventaire.etat === 'Cloture')
                {
                  this.tabsDisabled = false;
                  this.etatsInventaire = [
                    {value: 'Cloture', viewValue: 'Cloturé'},
                    {value: 'Annule', viewValue: 'Annulé'},
                    
                  ];
                }
            if (this.inventaire.etat === 'Valide')
              {
                this.tabsDisabled = false;
                this.etatsInventaire = [
                  {value: 'Annule', viewValue: 'Annulé'},
                  {value: 'Valide', viewValue: 'Validé'},
                  {value: 'Cloture', viewValue: 'Cloturé'},
                  
                ];
              }
              if (this.inventaire.etat === 'Annule')
                {
                  this.etatsInventaire= [
                    {value: 'Annule', viewValue: 'Annulé'},
                    {value: 'Valide', viewValue: 'Validé'},
                  ];
                  this.tabsDisabled = true;
               
                }
 
    });
  }
 
  loadAddInventaireComponent() {
    this.router.navigate(['/pms/suivie/ajouterInventaire']) ;
  }
  loadGridInventaires() {
    this.router.navigate(['/pms/suivie/inventaires']) ;
  }
  supprimerInventaires() {
      this.inventairesService.deleteInventaire(this.inventaire.code).subscribe((response) => {

        this.openSnackBar( 'Inventaire Supprimé');
        this.loadGridInventaires() ;
            });
  }
updateInventaire() {
  this.updateEnCours = true;
  this.inventairesService.updateInventaire(this.inventaire).subscribe((response) => {
    this.updateEnCours = false;
    if (response.error_message ===  undefined) {

      if (this.inventaire.etat === 'Initial')
        {
          this.tabsDisabled = true;
          this.etatsInventaire = [
            {value: 'Initial', viewValue: 'Initial'},
            {value: 'Valide', viewValue: 'Validé'},
            {value: 'Annule', viewValue: 'Annulé'},
            
          ];
        }
        if (this.inventaire.etat === 'Cloture')
          {
            this.tabsDisabled = false;
            this.etatsInventaire = [
              {value: 'Cloture', viewValue: 'Cloturé'},
              {value: 'Annule', viewValue: 'Annulé'},
              
            ];
          }
      if (this.inventaire.etat === 'Valide')
        {
          this.tabsDisabled = false;
          this.etatsInventaire = [
            {value: 'Annule', viewValue: 'Annulé'},
            {value: 'Valide', viewValue: 'Validé'},
            {value: 'Cloture', viewValue: 'Cloturé'},
            
          ];
        }
        if (this.inventaire.etat === 'Annule')
          {
            this.etatsInventaire = [
              {value: 'Annule', viewValue: 'Annulé'},
              {value: 'Valide', viewValue: 'Validé'},
            ];
            this.tabsDisabled = true;
         
          }
      this.openSnackBar( 'Inventaire Mis à Jour ');
    
    } else {
      this.openSnackBar( ' Erreurs:Opération Echouée ');
    }
});
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

 selectedTabChanged($event) {
  if ($event.index === 4) { // Composition tab was selected
   // this.loadListArticles();
   } else if ($event.index === 1) {
    //  this.loadListSchemasConfiguration() ;
   }
  }
}
