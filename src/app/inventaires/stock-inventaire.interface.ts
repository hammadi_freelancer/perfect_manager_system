
export interface StockInventaireElement {
         code: string ;
         numeroInventaire: string ;
         codeInventaire: string ;
         codeArticle: string ;
         libelleArticle: string ;
         designationArticle: string ;
         stockInitial: number ;
         stockFinal: number;
         valeurStockInitial: number;
         valeurStockFinal: number;
         codeUnMes: string;
         etat: string;
          description?: string;
}
