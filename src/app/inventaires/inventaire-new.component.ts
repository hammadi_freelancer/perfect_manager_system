import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {Inventaire} from './inventaire.model';

import {InventairesService} from './inventaires.service';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {MatSnackBar} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';


@Component({
  selector: 'app-inventaire-new',
  templateUrl: './inventaire-new.component.html',
  // styleUrls: ['./players.component.css']
})
export class InventaireNewComponent implements OnInit {

 private inventaire: Inventaire =  new Inventaire();
 private saveEnCours = false;
private etatsInventaire: any[] = [
  {value: 'Ouvert', viewValue: 'Ouvert'},
  {value: 'Initial', viewValue: 'Initiale'},
  {value: 'EnCoursValidation', viewValue: 'En Cours de Validation'},
  {value: 'Valide', viewValue: 'Validé'},
  {value: 'Cloture', viewValue: 'Cloturé'}
];
  constructor( private route: ActivatedRoute,
    private router: Router, private inventairesService: InventairesService,
   private matSnackBar: MatSnackBar) {
  }

  ngOnInit() {
  
  }
  saveInventaire() {

    if( this.inventaire.etat === undefined || this.inventaire.etat === 'Ouvert' ) {
      this.inventaire.etat = 'Initial';
    }
    // this.saveEnCours = true;
    this.saveEnCours = true;
    this.inventairesService.addInventaire(this.inventaire).subscribe((response) => {
      this.saveEnCours = false;
        if (response.error_message ===  undefined) {
          console.log('undefined error message');
          // this.save.emit(response);
          this.inventaire =  new Inventaire();
          this.openSnackBar(  'Inventaire Enregistré');
        } else {
         // show error message
         this.openSnackBar(  'Erreurs: Opération Echouée');
         
        }
    });
  }


  vider() {
    this.inventaire = new Inventaire();
  }
  openSnackBar(messageToDisplay) {
   this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
       verticalPosition: 'top'});
  }
loadGridInventaires() {
  this.router.navigate(['/pms/suivie/inventaires']) ;
}

}
