


export interface MaterielInventaireElement {
         code: string ;
         numeroInventaire: string ;
         codeInventaire: string ;
         dateAchats: string ;
         libelle: string ;
         cout: number ;
         etat: string;
          description?: string;
}
