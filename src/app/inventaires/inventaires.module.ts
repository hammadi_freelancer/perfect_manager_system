import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
 import { AppComponent } from '../app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ReactiveFormsModule } from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatMenuModule} from '@angular/material/menu';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {TableModule} from 'primeng/table';
import {MatSelectModule} from '@angular/material/select';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTabsModule} from '@angular/material/tabs';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MultiSelectModule} from 'primeng/multiselect';

import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import { RouterModule, provideRoutes} from '@angular/router';



import { CommonModule } from '../common/common.module';
import { SharedComponentModule } from '../shared-components/shared-components.module';



// journaux components
import { InventaireNewComponent } from './inventaire-new.component';
import { InventaireEditComponent } from './inventaire-edit.component';
import { InventairesGridComponent } from './inventaires-grid.component';

import { DialogAddDocumentInventaireComponent } from './popups/dialog-add-document-inventaire.component';
import { DialogEditDocumentInventaireComponent } from './popups/dialog-edit-document-inventaire.component';
import { DialogListDocumentsInventairesComponent } from './popups/dialog-list-documents-inventaires.component';

import { DialogAddMaterielInventaireComponent } from './popups/dialog-add-materiel-inventaire.component';
import { DialogEditMaterielInventaireComponent } from './popups/dialog-edit-materiel-inventaire.component';
import { DialogListMaterielsInventairesComponent } from './popups/dialog-list-materiels-inventaires.component';


import { DialogAddImmobilierInventaireComponent } from './popups/dialog-add-immobilier-inventaire.component';
import { DialogEditImmobilierInventaireComponent } from './popups/dialog-edit-immobilier-inventaire.component';
import { DialogListImmobiliersInventairesComponent } from './popups/dialog-list-immobiliers-inventaires.component';


import { DialogAddStockInventaireComponent } from './popups/dialog-add-stock-inventaire.component';
import { DialogEditStockInventaireComponent } from './popups/dialog-edit-stock-inventaire.component';
import { DialogListStocksInventairesComponent } from './popups/dialog-list-stocks-inventaires.component';


import { MaterielsInventairesGridComponent } from './materiels-inventaires-grid.component';
import { ImmobiliersInventairesGridComponent } from './immobiliers-inventaires-grid.component';
import { DocumentsInventairesGridComponent } from './documents-inventaires-grid.component';
import { StocksInventairesGridComponent } from './stocks-inventaires-grid.component';

import {MatSidenavModule} from '@angular/material/sidenav';
import {MatRadioModule} from '@angular/material/radio';



import {FormsModule } from '@angular/forms';
 import {APP_BASE_HREF} from '@angular/common';
 import { HomeInventairesComponent } from './home-inventaires.component' ;
/*const ENTITY_STATES = [
  ...commonRoute
];*/

 // const BASE_URL = [{provide: APP_BASE_HREF, useValue: '/common'}];
@NgModule({
  declarations: [
    InventaireEditComponent,
    InventaireNewComponent,
    InventairesGridComponent,
    MaterielsInventairesGridComponent,
    DocumentsInventairesGridComponent,
    StocksInventairesGridComponent,
    ImmobiliersInventairesGridComponent,
    DialogListStocksInventairesComponent,
    DialogListImmobiliersInventairesComponent,
    DialogListMaterielsInventairesComponent,
    DialogListDocumentsInventairesComponent,

    DialogAddStockInventaireComponent,
    DialogEditStockInventaireComponent,


    DialogAddMaterielInventaireComponent,
    DialogEditMaterielInventaireComponent,


    DialogAddImmobilierInventaireComponent,
    DialogEditImmobilierInventaireComponent,
   
    DialogAddDocumentInventaireComponent,
    DialogEditDocumentInventaireComponent
    
    // HomeLivresOresComponent

  ],
  imports: [
    BrowserModule, HttpClientModule, MatDatepickerModule, MatNativeDateModule,
     BrowserAnimationsModule, ReactiveFormsModule, MatAutocompleteModule,
    MatInputModule, MatCardModule, MatCheckboxModule, MatButtonModule, MatIconModule,
    MatMenuModule, MatProgressSpinnerModule, MatExpansionModule, MatSortModule, MatSelectModule, MatPaginatorModule, MatTableModule,
    FormsModule, SharedComponentModule, MatTabsModule, MatProgressBarModule,
    MultiSelectModule,
    // ventesRoutingModule,
    CommonModule, TableModule , MatDialogModule,
     MatSidenavModule, MatRadioModule
],
  providers: [MatNativeDateModule ,
  ],
  exports : [
    RouterModule,
    // HomeLivresOresComponent

  ],
  entryComponents : [
    DialogListStocksInventairesComponent,
    DialogListImmobiliersInventairesComponent,
    DialogListMaterielsInventairesComponent,
    DialogListDocumentsInventairesComponent,

    DialogAddStockInventaireComponent,
    DialogEditStockInventaireComponent,


    DialogAddMaterielInventaireComponent,
    DialogEditMaterielInventaireComponent,


    DialogAddImmobilierInventaireComponent,
    DialogEditImmobilierInventaireComponent,
   
    DialogAddDocumentInventaireComponent,
    DialogEditDocumentInventaireComponent
  ],
  bootstrap: [AppComponent]
})
export class InventairesModule { }
