
import { Component, OnInit, Inject,Input, ViewChild, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {InventairesService} from './inventaires.service';
import {Inventaire} from './inventaire.model';
import {MaterielInventaire} from './materiel-inventaire.model';
import { MaterielInventaireElement } from './materiel-inventaire.interface';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogAddMaterielInventaireComponent } from './popups/dialog-add-materiel-inventaire.component';
import { DialogEditMaterielInventaireComponent } from './popups/dialog-edit-materiel-inventaire.component';
@Component({
    selector: 'app-materiels-inventaires-grid',
    templateUrl: 'materiels-inventaires-grid.component.html',
  })
  export class MaterielsInventairesGridComponent implements OnInit, OnChanges {
     private listMaterielsInventaires: MaterielInventaire[] = [];
     private  dataMaterielsInventairesSource: MatTableDataSource<MaterielInventaireElement>;
     private selection = new SelectionModel<MaterielInventaireElement>(true, []);
    
     @Input()
     private inventaire :Inventaire;

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private inventairesService: InventairesService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.inventairesService.getMaterielsInventairesByCodeInventaire(this.inventaire.code).subscribe((listMatInv) => {
        this.listMaterielsInventaires= listMatInv;
  // console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
  }
  ngOnChanges()
  {
    this.specifyListColumnsToBeDisplayed();
    
    this.inventairesService.getMaterielsInventairesByCodeInventaire(this.inventaire.code).subscribe((listMatInv) => {
      this.listMaterielsInventaires = listMatInv;
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }

  checkOneActionInvoked() {
    const listSelectedMaterielsInventaires: MaterielInventaire[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((matINV) => {
    return matINV.code;
  });
  this.listMaterielsInventaires.forEach(lO => {
    if (codes.findIndex(code => code === lO.code) > -1) {
      listSelectedMaterielsInventaires.push(lO);
    }
    });
  }
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listMaterielsInventaires !== undefined) {

      this.listMaterielsInventaires.forEach(matINV => {
             listElements.push( {
               code: matINV.code ,
               dateAchats: matINV.dateAchats,
               codeInventaire: matINV.codeInventaire,
               cout: matINV.cout,
               libelle: matINV.libelle,
          description: matINV.description,
          etat: matINV.etat,
        } );
      });
    }
      this.dataMaterielsInventairesSource = new MatTableDataSource<MaterielInventaireElement>(listElements);
      this.dataMaterielsInventairesSource.sort = this.sort;
      this.dataMaterielsInventairesSource.paginator = this.paginator;
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [
      {name: 'code' , displayTitle: 'N°Matériel'},
     {name: 'libelle' , displayTitle: 'Libéllé'},
     {name: 'cout' , displayTitle: 'Côut'},
     {name: 'dateAchats' , displayTitle: 'Date Achats'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames = [];
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataMaterielsInventairesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataMaterielsInventairesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataMaterielsInventairesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataMaterielsInventairesSource.paginator) {
      this.dataMaterielsInventairesSource.paginator.firstPage();
    }
  }
  showAddMaterielInventaireDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeInventaire : this.inventaire.code
   };
 
   const dialogRef = this.dialog.open(DialogAddMaterielInventaireComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(lAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditMaterielInventaireDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeMaterielInventaire : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditMaterielInventaireComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(lAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Matériel Séléctionnée!');
    }
    }
    supprimerMaterielsInventaires()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(matrINV, index) {
              this.inventairesService.deleteMaterielInventaire(matrINV.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar(' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Matériel Séléctionnée!');
        }
    }
    refresh() {
      this.inventairesService.getMaterielsInventairesByCodeInventaire(this.inventaire.code).subscribe((listMats) => {
        this.listMaterielsInventaires = listMats;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
