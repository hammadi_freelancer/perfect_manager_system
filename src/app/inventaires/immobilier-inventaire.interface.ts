


export interface ImmobilierInventaireElement {
         code: string ;
         numeroInventaire: string ;
         dateAchats: string ;
         dateDebutExploitation: string;
         cout: number;
         superficie: number ;
         adresse: string;
         etat: string;
          description?: string;
}
