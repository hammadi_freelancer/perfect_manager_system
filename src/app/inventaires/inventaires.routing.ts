import { Injectable } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { InventaireNewComponent } from './inventaire-new.component';
import { InventaireEditComponent } from './inventaire-edit.component';
import { InventairesGridComponent } from './inventaires-grid.component';
import { HomeInventairesComponent } from './home-inventaires.component' ;


import {ModuleWithProviders  } from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';

 export const inventairesRoute: Routes = [
    {
        path: 'inventaires',
        component: InventairesGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterInventaire',
        component: InventaireNewComponent,
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerInventaire/:codeInventaire',
        component: InventaireEditComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
















];
// export const ventesRoutingModule: ModuleWithProviders = RouterModule.forRoot(ventesRoute);
// commonRouting.providers = [{ provide: APP_BASE_HREF, useValue: '/common'} ] ;


