
import { BaseEntity } from '../common/base-entity.model' ;


 type Etat = 'Initial' | 'Valide' | 'Annule'
 
 
export class MaterielInventaire extends BaseEntity {
    constructor(
        public code?: string,
        public numeroInventaire?: string,
        public codeInventaire?: string,
        public dateAchatsObject?: Date,
        public dateAchats?: string,
         public cout?: number,
         public libelle?: string,
          public etat?: Etat,
     ) {
         super();
        
       }
  
}
