
import { Component, OnInit, Inject,Input, ViewChild, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {InventairesService} from './inventaires.service';
import {Inventaire} from './inventaire.model';
import {StockInventaire} from './stock-inventaire.model';
import { StockInventaireElement } from './stock-inventaire.interface';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogAddStockInventaireComponent } from './popups/dialog-add-stock-inventaire.component';
import { DialogEditStockInventaireComponent } from './popups/dialog-edit-stock-inventaire.component';
@Component({
    selector: 'app-stocks-inventaires-grid',
    templateUrl: 'stocks-inventaires-grid.component.html',
  })
  export class StocksInventairesGridComponent implements OnInit, OnChanges {
     private listStocksInventaires: StockInventaire[] = [];
     private  dataStocksInventairesSource: MatTableDataSource<StockInventaireElement>;
     private selection = new SelectionModel<StockInventaireElement>(true, []);
    
     @Input()
     private inventaire : Inventaire;

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private inventairesService: InventairesService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.inventairesService.getStocksInventairesByCodeInventaire(this.inventaire.code).subscribe((listStkInv) => {
        this.listStocksInventaires = listStkInv;
  // console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
  }
  ngOnChanges()
  {
    this.specifyListColumnsToBeDisplayed();
    
    this.inventairesService.getStocksInventairesByCodeInventaire(this.inventaire.code).subscribe((listStkInv) => {
      this.listStocksInventaires = listStkInv;
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }

  checkOneActionInvoked() {
    const listSelectedStocksInventaires: StockInventaire[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((stlement) => {
    return stlement.code;
  });
  this.listStocksInventaires.forEach(lO => {
    if (codes.findIndex(code => code === lO.code) > -1) {
      listSelectedStocksInventaires.push(lO);
    }
    });
  }
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listStocksInventaires !== undefined) {

      this.listStocksInventaires.forEach(stINV => {
             listElements.push( {
               code: stINV.code ,
               numeroInventaire: stINV.numeroInventaire,
               codeInventaire: stINV.codeInventaire,
               stockInitial: stINV.stockInitial,
               stockFinal: stINV.stockFinal,
               codeArticle: stINV.article.code,
               designationArticle: stINV.article.designation,
               valeurStockInitial: stINV.valeurStockInitial,
               valeurStockFinal: stINV.valeurStockFinal,
               codeUnMes : stINV.unMesure.code,
              description: stINV.description,
          etat: stINV.etat,
        } );
      });
    }
      this.dataStocksInventairesSource = new MatTableDataSource<StockInventaireElement>(listElements);
      this.dataStocksInventairesSource.sort = this.sort;
      this.dataStocksInventairesSource.paginator = this.paginator;
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [
      {name: 'code' , displayTitle: 'Code'},
      {name: 'stockInitial' , displayTitle: 'Stock Initial'},
      {name: 'stockFinal' , displayTitle: 'Stock Final'},
     {name: 'codeArticle' , displayTitle: 'Code Art.'},
     {name: 'designationArticle' , displayTitle: 'Désignation Art.'},
     {name: 'codeUnMes' , displayTitle: 'UN'},
     
     {name: 'valeurTotal' , displayTitle: 'Valeur Total'},
     { name : 'etat' , displayTitle : 'Etat'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];
     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames = [];     
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataStocksInventairesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataStocksInventairesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataStocksInventairesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataStocksInventairesSource.paginator) {
      this.dataStocksInventairesSource.paginator.firstPage();
    }
  }
  showAddStockInventaireDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeInventaire : this.inventaire.code
   };
 
   const dialogRef = this.dialog.open(DialogAddStockInventaireComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(lAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditStockInventaireDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeStockInventaire : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditStockInventaireComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(lAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Stock Inventaire  Séléctionnée!');
    }
    }
    supprimerStocksInventaires()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(sInv, index) {
              this.stocksInventairesService.deleteStockInventaire(sInv.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar(' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Stock Séléctionné!');
        }
    }
    refresh() {
      this.inventairesService.getStocksInventairesByCodeInventaire(this.inventaire.code).
      subscribe((listStk) => {
        this.listStocksInventaires = listStk;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
