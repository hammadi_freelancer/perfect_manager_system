


export interface DocumentInventaireElement {
         code: string ;
         dateReception: string ;
         numeroDocument: number;
         typeDocument?: string;
          description?: string;
}
