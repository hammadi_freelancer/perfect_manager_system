
import { Component, OnInit, Inject,Input, ViewChild, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {InventairesService} from './inventaires.service';
import {Inventaire} from './inventaire.model';
import {ImmobilierInventaire} from './immobilier-inventaire.model';
import { ImmobilierInventaireElement } from './immobilier-inventaire.interface';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogAddImmobilierInventaireComponent } from './popups/dialog-add-immobilier-inventaire.component';
import { DialogEditImmobilierInventaireComponent } from './popups/dialog-edit-immobilier-inventaire.component';
@Component({
    selector: 'app-immobiliers-inventaires-grid',
    templateUrl: 'immobiliers-inventaires-grid.component.html',
  })
  export class ImmobiliersInventairesGridComponent implements OnInit, OnChanges {
     private listImmobiliersInventaires: ImmobilierInventaire[] = [];
     private  dataImmobiliersInventairesSource: MatTableDataSource<ImmobilierInventaireElement>;
     private selection = new SelectionModel<ImmobilierInventaireElement>(true, []);
    
     @Input()
     private inventaire : Inventaire;

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private inventairesService: InventairesService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.inventairesService.getImmobiliersInventairesByCodeInventaire(this.inventaire.code).subscribe((listImm) => {
        this.listImmobiliersInventaires= listImm;
  // console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
  }
  ngOnChanges()
  {
    this.specifyListColumnsToBeDisplayed();
    
    this.inventairesService.getImmobiliersInventairesByCodeInventaire(this.inventaire.code).subscribe((listIMM) => {
      this.listImmobiliersInventaires = listIMM;
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }

  checkOneActionInvoked() {
    const listSelectedImmobliersInventaires: ImmobilierInventaire[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((immlement) => {
    return immlement.code;
  });
  this.listImmobiliersInventaires.forEach(lO => {
    if (codes.findIndex(code => code === lO.code) > -1) {
      listSelectedImmobliersInventaires.push(lO);
    }
    });
  }
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listImmobiliersInventaires !== undefined) {

      this.listImmobiliersInventaires.forEach(immInv => {
             listElements.push( {
               code: immInv.code ,
               codeInventaire: immInv.codeInventaire,
               numeroInventaire: immInv.numeroInventaire,
               dateAchats: immInv.dateAchats,
               dateDebutExploitation: immInv.dateDebutExploitation,
               cout: immInv.cout,
               adresse: immInv.adresse,
               superficie: immInv.superficie,
               
              description: immInv.description,
              etat: immInv.etat,
        } );
      });
    }
      this.dataImmobiliersInventairesSource = new MatTableDataSource<ImmobilierInventaireElement>(listElements);
      this.dataImmobiliersInventairesSource.sort = this.sort;
      this.dataImmobiliersInventairesSource.paginator = this.paginator;
  }
   specifyListColumnsToBeDisplayed() {
     this.columnsDefinitions = [
      {name: 'code' , displayTitle: 'Code'},
      {name: 'dateAchats' , displayTitle: 'Date Achats'},
      {name: 'cout' , displayTitle: 'Coût'},
     {name: 'adresse' , displayTitle: '@Adresse'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];
      this.columnsNames = [];
     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataImmobiliersInventairesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataImmobiliersInventairesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataImmobiliersInventairesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataImmobiliersInventairesSource.paginator) {
      this.dataImmobiliersInventairesSource.paginator.firstPage();
    }
  }
  showAddImmobilierInventaireDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeInventaire : this.inventaire.code
   };
 
   const dialogRef = this.dialog.open(DialogAddImmobilierInventaireComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(lAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditImmobilierInventaireDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeImmobilierInventaire : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditImmobilierInventaireComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(lAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Immobilier Séléctionnée!');
    }
    }
    supprimerImmobiliersInventaires()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(immInv, index) {
              this.immobiliersInventaires.deleteImmobilierInventaire(immInv.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar(' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Immobilier Séléctionnée!');
        }
    }
    refresh() {
      this.inventairesService.getImmobiliersInventairesByCodeInventaire(this.inventaire.code).subscribe((listImm) => {
        this.listImmobiliersInventaires = listImm;
// console.log('recieving data from backend', this.listTranchesCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
