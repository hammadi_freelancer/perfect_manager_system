
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {InventairesService} from '../inventaires.service';
import {ImmobilierInventaire} from '../immobilier-inventaire.model';

@Component({
    selector: 'app-dialog-edit-immobilier-inventaire',
    templateUrl: 'dialog-edit-immobilier-inventaire.component.html',
  })
  export class DialogEditImmobilierInventaireComponent implements OnInit {
     private immobilierInventaire: ImmobilierInventaire;
     private updateEnCours = false;
    private etats: any[] = [
      {value: 'Annule', viewValue: 'Annulé'},
      {value: 'Initial', viewValue: 'Initial'},
      {value: 'Valide', viewValue: 'Validé'}
    ];

   
     constructor(  private inventairesService: InventairesService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit() {

        this.inventairesService.getImmobilierInventaire(this.data.codeImmobilierInventaire).subscribe((immINV) => {
            this.immobilierInventaire = immINV;
            /*if (this.immobilierInventaire.etat === 'Valide' || this.immobilierInventaire.etat === 'Annule') {
                this.etats.splice(1, 1);
              }
              if (this.immobilierInventaire.etat === 'Initial') {
                  this.etats.splice(0, 1);
                }*/
     });
    }
    updateImmobilierInventaire()  {
      
      this.updateEnCours = true;
       this.inventairesService.updateImmobilierInventaire(this.immobilierInventaire).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
        /*  if (this.immobilierInventaire.etat === 'Valide' || this.ligneLivreOre.etat === 'Annule' ) {
             //  this.tabsDisabled = false;
              this.etats = [
                {value: 'Annule', viewValue: 'Annulé'},
                {value: 'Valide', viewValue: 'Validé'}
      
              ];
            }*/
         
             // this.save.emit(response);
             this.openSnackBar(  'Immobilier Mise à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }


  }
