
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {InventairesService} from '../inventaires.service';
import {MaterielInventaire} from '../materiel-inventaire.model';

@Component({
    selector: 'app-dialog-edit-materiel-inventaire',
    templateUrl: 'dialog-edit-materiel-inventaire.component.html',
  })
  export class DialogEditMaterielInventaireComponent implements OnInit {
     private materielInventaire: MaterielInventaire;
     private updateEnCours = false;
    private etats: any[] = [
      {value: 'Annule', viewValue: 'Annulé'},
      {value: 'Initial', viewValue: 'Initial'},
      {value: 'Valide', viewValue: 'Validé'}
    ];
     constructor(  private inventairesService: InventairesService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit() {

        this.inventairesService.getMaterielInventaire(this.data.codeMaterielInventaire).subscribe((materielInventaire) => {
            this.materielInventaire = materielInventaire;
            if (this.materielInventaire.etat === 'Valide' || this.materielInventaire.etat === 'Annule') {
                this.etats.splice(1, 1);
              }
              if (this.materielInventaire.etat === 'Initial') {
                  this.etats.splice(0, 1);
                }
     });
    }
    updateMaterielInventaire()  {
      
      this.updateEnCours = true;
       this.inventairesService.updateMaterielInventaire(this.materielInventaire).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
          if (this.materielInventaire.etat === 'Valide' || this.materielInventaire.etat === 'Annule' ) {
             //  this.tabsDisabled = false;
              this.etats = [
                {value: 'Annule', viewValue: 'Annulé'},
                {value: 'Valide', viewValue: 'Validé'}
      
              ];
            }
         
             // this.save.emit(response);
             this.openSnackBar(  'Matériel  Mis à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }


  }
