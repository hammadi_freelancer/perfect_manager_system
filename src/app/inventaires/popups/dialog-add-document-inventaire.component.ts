
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {InventairesService} from '../inventaires.service';
import {DocumentInventaire} from '../document-inventaire.model';

@Component({
    selector: 'app-dialog-add-document-inventaire',
    templateUrl: 'dialog-add-document-inventaire.component.html',
  })
  export class DialogAddDocumentInventaireComponent implements OnInit {
     private documentInventaire: DocumentInventaire;
     private saveEnCours = false;
     private typesDoc: any[] = [
      {value: 'FactureVentes', viewValue: 'Facture Ventes'},
      {value: 'FactureAchats', viewValue: 'Facture Achats'},
      {value: 'FactureTelephonique', viewValue: 'Facture Téléphonique'},
      {value: 'FactureElectricite', viewValue: 'Facture Eléctricité'},
      {value: 'FactureEauPotable', viewValue: 'Facture Eau Potable'},
      {value: 'FichePaie', viewValue: 'Fiche Paie'},
      {value: 'BonLivraison', viewValue: 'Bon Livraison'},
      {value: 'BonReception', viewValue: 'Bon Récéption'},
      {value: 'RecuPayement', viewValue: 'Reçu Payement'},
      {value: 'Cheque', viewValue: 'Chèque'},
      {value: 'Traite', viewValue: 'Traite'},
      {value: 'Autre', viewValue: 'Autre'},
    ];
     constructor(  private inventairesService: InventairesService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.documentInventaire = new DocumentInventaire();
    }
    saveDocumentInventaire() 
    {
      this.documentInventaire.codeInventaire = this.data.codeInventaire;
        this.saveEnCours = true;
       this.inventairesService.addDocumentInventaire(this.documentInventaire).subscribe((response) => {
         this.saveEnCours = false;
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.documentInventaire = new DocumentInventaire();
             this.openSnackBar(  'Document Enregistré');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs: Opération Echouée');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
     imageFace1Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentInventaire.imageFace1 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
     imageFace2Changed($event)
     {
      const fileUploaded = $event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => {
        console.log(fileReader.result);
         this.documentInventaire.imageFace2 = fileReader.result;
        // this.filesData[indexFile] = fileData;
      };
      fileReader.readAsDataURL(fileUploaded);
     }
  }
