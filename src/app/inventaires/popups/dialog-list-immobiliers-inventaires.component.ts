
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {InventairesService} from '../inventaires.service';
import {Inventaire} from '../inventaire.model';
import {ImmobilierInventaire} from '../immobilier-inventaire.model';

import { ImmobilierInventaireElement } from '../immobilier-inventaire.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-immobiliers-inventaires',
    templateUrl: 'dialog-list-immobiliers-inventaires.component.html',
  })
  export class DialogListImmobiliersInventairesComponent implements OnInit {
     private listImmobiliersInventaires: ImmobilierInventaire[] = [];
     private  dataImmobiliersInventairesSource: MatTableDataSource<ImmobilierInventaireElement>;
     private selection = new SelectionModel<ImmobilierInventaireElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private inventairesService: InventairesService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit() {
      this.inventairesService.getImmobiliersInventairesByCodeInventaire(this.data.codeInventaire).
      subscribe((listImmobiliersInventaires) => {
        this.listImmobiliersInventaires = listImmobiliersInventaires;
// console.log('recieving data from backend', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedImmobiliersInventaires: ImmobilierInventaire[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((immlement) => {
    return immlement.code;
  });
  this.listImmobiliersInventaires.forEach(im => {
    if (codes.findIndex(code => code === im.code) > -1) {
      listSelectedImmobiliersInventaires.push(im);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listImmobiliersInventaires !== undefined) {

      this.listImmobiliersInventaires.forEach(immINV => {
             listElements.push( {
               code: immINV.code ,
               dateAchats: immINV.dateAchats,
               dateDebutExploitation: immINV.dateDebutExploitation,
               adresse: immINV.adresse,
               superficie: immINV.superficie,
               cout: immINV.cout,
          description: immINV.description,
          etat: immINV.etat,
        } );
      });
    }
      this.dataImmobiliersInventairesSource = new MatTableDataSource<ImmobilierInventaireElement>(listElements);
      this.dataImmobiliersInventairesSource.sort = this.sort;
      this.dataImmobiliersInventairesSource.paginator = this.paginator;
  }
  specifyListColumnsToBeDisplayed() {
    this.columnsDefinitions = [
     {name: 'code' , displayTitle: 'Code'},
     {name: 'dateAchats' , displayTitle: 'Date Achats'},
     {name: 'dateDebutExploitation' , displayTitle: 'Date Début Exploitation'},
    {name: 'adresse' , displayTitle: 'Adresse'},
    {name: 'superficie' , displayTitle: 'Supérficie'},
    {name: 'cout' , displayTitle: 'Côut'},
    {name: 'description' , displayTitle: 'Déscription'}
     ];

    this.columnsTitles = this.columnsDefinitions.map((colDef) => {
              return colDef.displayTitle;
    });
    this.columnsNames[0] = 'select';
    this.columnsDefinitions.map((colDef) => {
     this.columnsNames.push(colDef.name);
 });
    this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
     return colDef.displayTitleAr;
 });
 }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataImmobiliersInventairesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataImmobiliersInventairesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataImmobiliersInventairesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataImmobiliersInventairesSource.paginator) {
      this.dataImmobiliersInventairesSource.paginator.firstPage();
    }
  }

}
