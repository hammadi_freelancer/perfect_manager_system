
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {InventairesService} from '../inventaires.service';
import {Inventaire} from '../inventaire.model';
import {StockInventaire} from '../stock-inventaire.model';

@Component({
    selector: 'app-dialog-add-stock-inventaire',
    templateUrl: 'dialog-add-stock-inventaire.component.html',
  })
  export class DialogAddStockInventaireComponent implements OnInit {
     private stockInventaire: StockInventaire;
     private saveEnCours = false;
     private etats: any[] = [
      {value: 'Initial', viewValue: 'Initiale'},
      {value: 'Valide', viewValue: 'Validé'},
      {value: 'Annule', viewValue: 'Annulé'}
        ];
    /* private typesOperations: any[] = [
      {value: 'AchatsMatieresPrimaires', viewValue: 'Achats Matières Primaires'},
      {value: 'AchatsMarchandises', viewValue: 'Achats Marchandises'},
      {value: 'AchatsFournituresBureau', viewValue: 'Achats Fournitures Bureau'},
      {value: 'AchatsMaterielles', viewValue: 'Achats Materielles'},
      {value: 'AutresAchats', viewValue: 'Autres Achats'},
      {value: 'Electricite', viewValue: 'Electricité'},
      {value: 'EauPotable', viewValue: 'Eau Potable'},
      {value: 'ConsommationTelephonique', viewValue: 'Consommation Téléphonique'},
      {value: 'Transport', viewValue: 'Transport'},
      {value: 'Location', viewValue: 'Locations'},
      {value: 'Impots', viewValue: 'Impots'},
      {value: 'AutresCharges', viewValue: 'Autres Charges'},
      {value: 'VentesMarchandises', viewValue: 'Ventes Marchandises'},
      {value: 'VentesProduitsFinis', viewValue: 'Ventes Produits Finis'},
      {value: 'VentesMatieresPrimaires', viewValue: 'Ventes Matieres Primaires'},
      {value: 'VentesProduitsSemiFinis', viewValue: 'Ventes Produits Semi Finis'},
      {value: 'VentesMateriellesEpuisées', viewValue: 'Ventes Materielles Epuisées'},
      {value: 'VentesLocaux', viewValue: 'Ventes Locaux'},
      {value: 'VentesProduitsNonConformes', viewValue: 'Ventes Produits Non Conformes'},
      {value: 'AutresVentes', viewValue: 'Autres Ventes'},

      {value: 'PaieSalaires', viewValue: 'Paie Salaires'},
      {value: 'PaieHoraires', viewValue: 'Paie Horaires'},
      {value: 'PaieCNSS', viewValue: 'Paie CNSS'},
      {value: 'PaiePrimes', viewValue: 'Paie Primes'},
      {value: 'PaieAvances', viewValue: 'Paie Avances'},
      {value: 'PaieRecompenses', viewValue: 'Paie Récompenses'},
      {value: 'PaieRecrutements', viewValue: 'Paie Recrutements'},
      {value: 'AutresPaies', viewValue: 'Autres Paies'},
    ];*/
   




     constructor(  private inventairesService: InventairesService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.stockInventaire = new StockInventaire();
    }
    saveStockInventaire()
    {
      this.stockInventaire.codeInventaire = this.data.codeInventaire;
       this.saveEnCours = true;
       this.inventairesService.addStockInventaire(this.stockInventaire).subscribe((response) => {
         this.saveEnCours = false;
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.stockInventaire = new StockInventaire();
             this.openSnackBar(  'Stock Enregistrée');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
 
  }
