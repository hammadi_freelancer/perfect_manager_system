
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {InventairesService} from '../inventaires.service';
import {Inventaire} from '../inventaire.model';
import {MaterielInventaire} from '../materiel-inventaire.model';

import { MaterielInventaireElement } from '../materiel-inventaire.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-materiels-inventaires',
    templateUrl: 'dialog-list-materiels-inventaires.component.html',
  })
  export class DialogListMaterielsInventairesComponent implements OnInit {
     private listMaterielsInventaires: MaterielInventaire[] = [];
     private  dataMaterielsInventairesSource: MatTableDataSource<MaterielInventaireElement>;
     private selection = new SelectionModel<MaterielInventaireElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private inventairesService: InventairesService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      this.inventairesService.getMaterielsInventairesByCodeInventaire(this.data.codeInventaire).subscribe((listMatInv) => {
        this.listMaterielsInventaires = listMatInv;
// console.log('recieving data from backend', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedMaterielsInventaires: MaterielInventaire[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((matInvlement) => {
    return matInvlement.code;
  });
  this.listMaterielsInventaires.forEach(lMatInv => {
    if (codes.findIndex(code => code === lMatInv.code) > -1) {
      listSelectedMaterielsInventaires.push(lMatInv);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listMaterielsInventaires !== undefined) {

      this.listMaterielsInventaires.forEach(matInv => {
             listElements.push( {
               code: matInv.code ,
               dateAchats: matInv.dateAchats,
               libelle: matInv.libelle,
               cout: matInv.cout,
               numeroInventaire: matInv.numeroInventaire,
               codeInventaire: matInv.codeInventaire,
          description: matInv.description,
          etat: matInv.etat,
        } );
      });
    }
      this.dataMaterielsInventairesSource = new MatTableDataSource<MaterielInventaireElement>(listElements);
      this.dataMaterielsInventairesSource.sort = this.sort;
      this.dataMaterielsInventairesSource.paginator = this.paginator;
  }
  specifyListColumnsToBeDisplayed() {
    this.columnsDefinitions = [
     {name: 'code' , displayTitle: 'Code'},
     {name: 'dateAchats' , displayTitle: 'Date Achats'},
     {name: 'libelle' , displayTitle: 'Libéllé'},
    {name: 'cout' , displayTitle: 'Coût'},
      { name : 'description' , displayTitle : 'Déscription'},
     ];

    this.columnsTitles = this.columnsDefinitions.map((colDef) => {
              return colDef.displayTitle;
    });
    this.columnsNames[0] = 'select';
    this.columnsDefinitions.map((colDef) => {
     this.columnsNames.push(colDef.name);
 });
    this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
     return colDef.displayTitleAr;
 });
 }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataMaterielsInventairesSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataMaterielsInventairesSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataMaterielsInventairesSource.filter = filterValue.trim().toLowerCase();
    if (this.dataMaterielsInventairesSource.paginator) {
      this.dataMaterielsInventairesSource.paginator.firstPage();
    }
  }

}
