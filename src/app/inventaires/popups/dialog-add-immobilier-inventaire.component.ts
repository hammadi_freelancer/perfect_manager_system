
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {InventairesService} from '../inventaires.service';
import {ImmobilierInventaire} from '../immobilier-inventaire.model';

@Component({
    selector: 'app-dialog-add-immobilier-inventaire',
    templateUrl: 'dialog-add-immobilier-inventaire.component.html',
  })
  export class DialogAddImmobilierInventaireComponent implements OnInit {
     private immobilierInventaire: ImmobilierInventaire;
     private saveEnCours = false;
     private etats: any[] = [
      {value: 'Initial', viewValue: 'Initiale'},
      {value: 'Valide', viewValue: 'Validé'},
      {value: 'Annule', viewValue: 'Annulé'}
        ];
     constructor(  private inventairesService: InventairesService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
           this.immobilierInventaire = new ImmobilierInventaire();
    }
    saveImmobilierInventaire() 
    {
      this.immobilierInventaire.codeInventaire = this.data.codeInventaire;
       this.saveEnCours = true;
       this.inventairesService.addImmobilierInventaire(this.immobilierInventaire).subscribe((response) => {
         this.saveEnCours = false;
           if (response.error_message ===  undefined) {
             // this.save.emit(response);
             this.immobilierInventaire = new ImmobilierInventaire();
             this.openSnackBar(  'Immobilier Enregistrée');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }
 
  }
