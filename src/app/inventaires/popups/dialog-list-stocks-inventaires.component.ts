
import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {InventairesService} from '../inventaires.service';
import {StockInventaire} from '../stock-inventaire.model';
import { StockInventaireElement } from '../stock-inventaire.interface';
import { ColumnDefinition } from '../../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
    selector: 'app-dialog-list-stocks-inventaires',
    templateUrl: 'dialog-list-stocks-inventaires.component.html',
  })
  export class DialogListStocksInventairesComponent implements OnInit {
     private listStocksInventaires: StockInventaire[] = [];
     private  dataStocksInventaireSource: MatTableDataSource<StockInventaireElement>;
     private selection = new SelectionModel<StockInventaireElement>(true, []);
     
     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private inventairesService: InventairesService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit()
    {
      this.inventairesService.getStocksInventairesByCodeInventaire(this.data.codeInventaire).subscribe((listStocksInventaires) => {
        this.listStocksInventaires = listStocksInventaires;
// console.log('recieving data from backend', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });

  }


  checkOneActionInvoked() {
    const listSelectedStocksInventaires: StockInventaire[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((stklement) => {
    return stklement.code;
  });
  this.listStocksInventaires.forEach(stk => {
    if (codes.findIndex(code => code === stk.code) > -1) {
      listSelectedStocksInventaires.push(stk);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listStocksInventaires !== undefined) {

      this.listStocksInventaires.forEach(stkInv => {
             listElements.push( {
               code: stkInv.code ,
               stockInitial: stkInv.stockInitial,
               stockFinal: stkInv.stockFinal,
               valeurStockInitial: stkInv.valeurStockInitial,
               valeurStockFinal: stkInv.valeurStockFinal,
               codeArticle: stkInv.article.code,
               designation: stkInv.article.designation,
          description: stkInv.description,
          etat: stkInv.etat,
        } );
      });
    }
      this.dataStocksInventaireSource = new MatTableDataSource<StockInventaireElement>(listElements);
      this.dataStocksInventaireSource.sort = this.sort;
      this.dataStocksInventaireSource.paginator = this.paginator;
  }
  specifyListColumnsToBeDisplayed() {
    this.columnsDefinitions = [
     {name: 'code' , displayTitle: 'Code'},
     {name: 'stockInitial' , displayTitle: 'Stock Initial'},
     {name: 'stockFinal' , displayTitle: 'Stock Final'},
     {name: 'valeurStockInitial' , displayTitle: 'Valeur Stk. Int.'},
     {name: 'valeurStockFinal' , displayTitle: 'Valeur Stk. Fn.'},
     {name: 'code' , displayTitle: 'Code Art.'},
     {name: 'designation' , displayTitle: 'Désignation'},
      { name : 'description' , displayTitle : 'Déscription'},
     ];

    this.columnsTitles = this.columnsDefinitions.map((colDef) => {
              return colDef.displayTitle;
    });
    this.columnsNames[0] = 'select';
    this.columnsDefinitions.map((colDef) => {
     this.columnsNames.push(colDef.name);
 });
    this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
     return colDef.displayTitleAr;
 });
 }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataStocksInventaireSource.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataStocksInventaireSource.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataStocksInventaireSource.filter = filterValue.trim().toLowerCase();
    if (this.dataStocksInventaireSource.paginator) {
      this.dataStocksInventaireSource.paginator.firstPage();
    }
  }

}
