
import { Component, OnInit, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

import {MatSnackBar} from '@angular/material';
import {InventairesService} from '../inventaires.service';
import {StockInventaire} from '../stock-inventaire.model';

@Component({
    selector: 'app-dialog-edit-stock-inventaire',
    templateUrl: 'dialog-edit-stock-inventaire.component.html',
  })
  export class DialogEditStockInventaireComponent implements OnInit {
     private stockInventaire: StockInventaire;
     private updateEnCours = false;
    private etats: any[] = [
      {value: 'Annule', viewValue: 'Annulé'},
      {value: 'Initial', viewValue: 'Initial'},
      {value: 'Valide', viewValue: 'Validé'}
    ];

   
     constructor(  private inventairesService: InventairesService,
     private matSnackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data: any
      ) {
    }
    ngOnInit() {

        this.inventairesService.getStockInventaire(this.data.codeStockInventaire).subscribe((stkINV) => {
            this.stockInventaire = stkINV;
            if (this.stockInventaire.etat === 'Valide' || this.stockInventaire.etat === 'Annule') {
                this.etats.splice(1, 1);
              }
              if (this.stockInventaire.etat === 'Initial') {
                  this.etats.splice(0, 1);
                }
     });
    }
    updateStockInventaire()  {
      
      this.updateEnCours = true;
       this.inventairesService.updateStockInventaire(this.stockInventaire).subscribe((response) => {
        this.updateEnCours = false;
        if (response.error_message ===  undefined) {
          if (this.stockInventaire.etat === 'Valide' || this.stockInventaire.etat === 'Annule' ) {
             //  this.tabsDisabled = false;
              this.etats = [
                {value: 'Annule', viewValue: 'Annulé'},
                {value: 'Valide', viewValue: 'Validé'}
      
              ];
            }
         
             // this.save.emit(response);
             this.openSnackBar(  'Stock Mis à Jour');
           } else {
            // show error message
            this.openSnackBar(  'Erreurs!');
            
           }
       });
   }
    openSnackBar(messageToDisplay) {
      this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
          verticalPosition: 'top'});
     }


  }
