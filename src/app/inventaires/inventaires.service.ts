import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';
import { HttpParams } from '@angular/common/http';

import {Inventaire} from './inventaire.model' ;
// import { TrancheCredit } from './tranche-credit.model';
import { DocumentInventaire } from './document-inventaire.model';
import { ImmobilierInventaire } from './immobilier-inventaire.model';
import { MaterielInventaire } from './materiel-inventaire.model';
import { StockInventaire } from './stock-inventaire.model';

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_INVENTAIRES = 'http://localhost:8082/inventaires/getInventaires';
const URL_GET_INVENTAIRE = 'http://localhost:8082/inventaires/getInventaire';
const  URL_ADD_INVENTAIRE = 'http://localhost:8082/inventaires/addInventaire';
const  URL_UPDATE_INVENTAIRE = 'http://localhost:8082/inventaires/updateInventaire';
const  URL_DELETE_INVENTAIRE = 'http://localhost:8082/inventaires/deleteInventaire';
const  URL_GET_TOTAL_INVENTAIRES = 'http://localhost:8082/inventaires/getTotalInventaires';



  const  URL_ADD_DOCUMENT_INVENTAIRE = 'http://localhost:8082/inventaires/documents/addDocumentInventaire';
  const  URL_GET_LIST_DOCUMENT_INVENTAIRE = 'http://localhost:8082/inventaires/documents/getDocumentsLivresOres';
  const  URL_DELETE_DOCUCMENT_INVENTAIRE = 'http://localhost:8082/inventaires/documents/deleteDocumentInventaire';
  const  URL_UPDATE_DOCUMENT_INVENTAIRE = 'http://localhost:8082/inventaires/documents/updateDocumentInventaire';
  const URL_GET_DOCUMENT_INVENTAIRE = 'http://localhost:8082/inventaires/documents/getDocumentInventaire';
  const URL_GET_LIST_DOCUMENT_INVENTAIRE_BY_CODE_INVENTAIRE =
   'http://localhost:8082/inventaires/documents/getDocumentsInventairesByCodeInventaire';


   const  URL_ADD_STOCK_INVENTAIRE = 'http://localhost:8082/inventaires/stocksInventaires/addStockInventaire';
   const  URL_GET_LIST_STOCK_INVENTAIRE = 'http://localhost:8082/inventaires/stocksInventaires/getStocksInventaires';
   const  URL_DELETE_STOCK_INVENTAIRE = 'http://localhost:8082/inventaires/stocksInventaires/deleteStockInventaire';
   const  URL_UPDATE_STOCK_INVENTAIRE = 'http://localhost:8082/inventaires/stocksInventaires/updateStockInventaire';
   const URL_GET_STOCK_INVENTAIRE = 'http://localhost:8082/inventaires/stocksInventaires/getStockInventaire';
   const URL_GET_LIST_STOCK_INVENTAIRE_BY_CODE_INVENTAIRE =
    'http://localhost:8082/inventaires/stocksInventaires/getStocksInventairesByCodeInventaire';

    const  URL_ADD_MATERIEL_INVENTAIRE = 'http://localhost:8082/inventaires/materielsInventaires/addMaterielInventaire';
    const  URL_GET_LIST_MATERIEL_INVENTAIRE = 'http://localhost:8082/inventaires/materielsInventaires/getMaterielsInventaires';
    const  URL_DELETE_MATERIEL_INVENTAIRE = 'http://localhost:8082/inventaires/materielsInventaires/deleteMaterielInventaire';
    const  URL_UPDATE_MATERIEL_INVENTAIRE = 'http://localhost:8082/inventaires/materielsInventaires/updateMaterielInventaire';
    const URL_GET_MATERIEL_INVENTAIRE = 'http://localhost:8082/inventaires/materielsInventaires/getMaterielInventaire';
    const URL_GET_LIST_MATERIEL_INVENTAIRE_BY_CODE_INVENTAIRE =
     'http://localhost:8082/inventaires/materielsInventaires/getMaterielsInventairesByCodeInventaire';

     const  URL_ADD_IMMOBILIER_INVENTAIRE = 'http://localhost:8082/inventaires/immobiliersInventaires/addImmobilierInventaire';
     const  URL_GET_LIST_IMMOBILIER_INVENTAIRE = 'http://localhost:8082/inventaires/immobiliersInventaires/getImmobiliersInventaires';
     const  URL_DELETE_IMMOBILIER_INVENTAIRE = 'http://localhost:8082/inventaires/immobiliersInventaires/deleteImmobilierInventaire';
     const  URL_UPDATE_IMMOBILIER_INVENTAIRE = 'http://localhost:8082/inventaires/immobiliersInventaires/updateImmobilierInventaire';
     const URL_GET_IMMOBILIER_INVENTAIRE = 'http://localhost:8082/inventaires/immobiliersInventaires/getImmobilierInventaire';
     const URL_GET_LIST_IMMOBILIER_INVENTAIRE_BY_CODE_INVENTAIRE =
      'http://localhost:8082/inventaires/immobiliersInventaires/getImmobiliersInventairesByCodeInventaire';



@Injectable({
  providedIn: 'root'
})
export class InventairesService {

  constructor(private httpClient: HttpClient) {

   }

   getTotalInventaires(filter): Observable<any> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('filter', filterStr);
        return this.httpClient
        .get<any>(URL_GET_TOTAL_INVENTAIRES, {params});
       }
   getInventaires(pageNumber, nbElementsPerPage, filter): Observable<Inventaire[]> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('pageNumber', pageNumber)
    .set('nbElementsPerPage', nbElementsPerPage)
    .set('sort', 'name')
    .set('filter', filterStr);
    return this.httpClient
    .get<Inventaire[]>(URL_GET_LIST_INVENTAIRES, {params});
   }
   addInventaire(inventaire: Inventaire): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_INVENTAIRE, inventaire);
  }
  updateInventaire(inventaire: Inventaire): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_INVENTAIRE, inventaire);
  }
  deleteInventaire(codeInventaire): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_INVENTAIRE + '/' + codeInventaire);
  }
  getInventaire(codeInventaire): Observable<Inventaire> {
    return this.httpClient.get<Inventaire>(URL_GET_INVENTAIRE + '/' + codeInventaire);
  }


  addDocumentInventaire(documentInventaire: DocumentInventaire): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_DOCUMENT_INVENTAIRE, documentInventaire);
    
  }
  updateDocumentInventaire(docInventaire: DocumentInventaire): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_DOCUMENT_INVENTAIRE, docInventaire);
  }
  deleteDocumentInventaire(codeDocumentInventaire): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_DOCUCMENT_INVENTAIRE + '/' + codeDocumentInventaire);
  }
  getDocumentInventaire(codeDocumentInventaire): Observable<DocumentInventaire> {
    return this.httpClient.get<DocumentInventaire>(URL_GET_DOCUMENT_INVENTAIRE + '/' + codeDocumentInventaire);
  }
  getDocumentsInventaires(): Observable<DocumentInventaire[]> {
    return this.httpClient
    .get<DocumentInventaire[]>(URL_GET_LIST_DOCUMENT_INVENTAIRE);
   }
  getDocumentsInventairesByCodeInventaire(codeDocumentInventaire): Observable<DocumentInventaire[]> {
    return this.httpClient.get<DocumentInventaire[]>(URL_GET_LIST_DOCUMENT_INVENTAIRE_BY_CODE_INVENTAIRE + '/' + codeDocumentInventaire);
  }

// Lignes inventaires


addStockInventaire(stockInventaire: StockInventaire): Observable<any> {
  return this.httpClient.post<any>(URL_ADD_STOCK_INVENTAIRE, stockInventaire);
  
}
updateStockInventaire(stockInventaire: StockInventaire): Observable<any> {
  return this.httpClient.put<any>(URL_UPDATE_STOCK_INVENTAIRE, stockInventaire);
}
deleteStockInventaire(codeStockInventaire): Observable<any> {
  return this.httpClient.delete<any>(URL_DELETE_STOCK_INVENTAIRE + '/' + codeStockInventaire);
}
getStockInventaire(codeStockInventaire): Observable<StockInventaire> {
  return this.httpClient.get<StockInventaire>(URL_GET_STOCK_INVENTAIRE + '/' + codeStockInventaire);
}
getStocksInventaires(): Observable<StockInventaire[]> {
  return this.httpClient
  .get<StockInventaire[]>(URL_GET_LIST_STOCK_INVENTAIRE);
 }
getStocksInventairesByCodeInventaire(codeInventaire): Observable<StockInventaire[]> {
  return this.httpClient.get<StockInventaire[]>(URL_GET_LIST_STOCK_INVENTAIRE_BY_CODE_INVENTAIRE + '/' + codeInventaire);
}



addMaterielInventaire(materielInventaire: MaterielInventaire): Observable<any> {
  return this.httpClient.post<any>(URL_ADD_MATERIEL_INVENTAIRE, materielInventaire);
  
}
updateMaterielInventaire(materielInventaire: MaterielInventaire): Observable<any> {
  return this.httpClient.put<any>(URL_UPDATE_MATERIEL_INVENTAIRE, materielInventaire);
}
deleteMaterielInventaire(codeMaterielInventaire): Observable<any> {
  return this.httpClient.delete<any>(URL_DELETE_MATERIEL_INVENTAIRE + '/' + codeMaterielInventaire);
}
getMaterielInventaire(codeMaterielInventaire): Observable<StockInventaire> {
  return this.httpClient.get<StockInventaire>(URL_GET_MATERIEL_INVENTAIRE + '/' + codeMaterielInventaire);
}
getMaterielsInventaires(): Observable<MaterielInventaire[]> {
  return this.httpClient
  .get<MaterielInventaire[]>(URL_GET_LIST_MATERIEL_INVENTAIRE);
 }
getMaterielsInventairesByCodeInventaire(codeInventaire): Observable<MaterielInventaire[]> {
  return this.httpClient.get<MaterielInventaire[]>(URL_GET_LIST_MATERIEL_INVENTAIRE_BY_CODE_INVENTAIRE + '/' + codeInventaire);
}


addImmobilierInventaire(immobilierInventaire: ImmobilierInventaire): Observable<any> {
  return this.httpClient.post<any>(URL_ADD_IMMOBILIER_INVENTAIRE, immobilierInventaire);
  
}
updateImmobilierInventaire(immobilierInventaire: ImmobilierInventaire): Observable<any> {
  return this.httpClient.put<any>(URL_UPDATE_IMMOBILIER_INVENTAIRE, immobilierInventaire);
}
deleteImmobilierInventaire(codeImmobilierInventaire): Observable<any> {
  return this.httpClient.delete<any>(URL_DELETE_IMMOBILIER_INVENTAIRE +
    '/' + codeImmobilierInventaire);
}
getImmobilierInventaire(codeImmobilierInventaire): Observable<ImmobilierInventaire> {
  return this.httpClient.get<ImmobilierInventaire>(URL_GET_IMMOBILIER_INVENTAIRE + '/' + codeImmobilierInventaire);
}
getImmobiliersInventaires(): Observable<ImmobilierInventaire[]> {
  return this.httpClient
  .get<ImmobilierInventaire[]>(URL_GET_LIST_IMMOBILIER_INVENTAIRE);
 }
 getImmobiliersInventairesByCodeInventaire(codeInventaire): Observable<ImmobilierInventaire[]> {
  return this.httpClient.get<ImmobilierInventaire[]>(URL_GET_LIST_IMMOBILIER_INVENTAIRE_BY_CODE_INVENTAIRE + '/' + codeInventaire);
}

}
