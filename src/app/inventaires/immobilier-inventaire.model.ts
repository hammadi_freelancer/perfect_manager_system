
import { BaseEntity } from '../common/base-entity.model' ;


 type Etat = 'Initial' | 'Valide' | 'Annule'
 
 
 
export class ImmobilierInventaire extends BaseEntity {
    constructor(
        public code?: string,
        public numeroInventaire?: string,
        public codeInventaire?: string,
        public dateAchatsObject?: Date,
        public dateDebutExploitationObject?: Date,
        public dateAchats?: string,
        public dateDebutExploitation?: string,
         public cout?: number,
         public superficie?: number,
         public adresse?: string,
          public etat?: Etat,
     ) {
         super();
        
       }
  
}
