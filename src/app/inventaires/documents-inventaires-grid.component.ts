
import { Component, OnInit, Inject,Input, ViewChild, OnChanges} from '@angular/core';

import {MatSnackBar} from '@angular/material';
import {InventairesService} from './inventaires.service';
import {Inventaire} from './inventaire.model';

import {DocumentInventaire} from './document-inventaire.model';
import { DocumentInventaireElement } from './document-inventaire.interface';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef} from '@angular/material';
import {MAT_DIALOG_DATA} from '@angular/material';

import {SelectionModel} from '@angular/cdk/collections';
import { DialogAddDocumentInventaireComponent } from './popups/dialog-add-document-inventaire.component';
import { DialogEditDocumentInventaireComponent } from './popups/dialog-edit-document-inventaire.component';
@Component({
    selector: 'app-documents-inventaires-grid',
    templateUrl: 'documents-inventaires-grid.component.html',
  })
  export class DocumentsInventairesGridComponent implements OnInit, OnChanges {
     private listDocumentsInventaires: DocumentInventaire[] = [];
     private  dataDocumentsInventaires: MatTableDataSource<DocumentInventaireElement>;
     private selection = new SelectionModel<DocumentInventaireElement>(true, []);
    
     @Input()
     private inventaire : Inventaire ;

     @ViewChild(MatPaginator) paginator: MatPaginator; 
     @ViewChild(MatSort) sort: MatSort;
     private columnsDefinitions: ColumnDefinition[] = [];
     private columnsTitles: string[] = [];
     private columnsTitlesAr: string[] = [];
     private columnsNames: string[] = [];
     constructor(  private inventairesService: InventairesService,
     private matSnackBar: MatSnackBar,
     public dialog: MatDialog
      ) {
    }
    ngOnInit()
    {
      this.specifyListColumnsToBeDisplayed();
      
      this.inventairesService.getDocumentsInventairesByCodeInventaire(this.inventaire.code).subscribe((listDocumentsInventaires) => {
        this.listDocumentsInventaires = listDocumentsInventaires;
  // console.log('recieving data from getDocumentsCreditsByCodeCredit', this.listDocumentsCredits  );
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
  }
  ngOnChanges()
  {
    this.specifyListColumnsToBeDisplayed();
    
    this.inventairesService.getDocumentsInventairesByCodeInventaire(this.inventaire.code).subscribe((listDocumentsInventaires) => {
      this.listDocumentsInventaires = listDocumentsInventaires;
      this.specifyListColumnsToBeDisplayed();
      this.transformDataToByConsumedByMatTable();
       });
  }

  checkOneActionInvoked() {
    const listSelectedDocumentsInventaires: DocumentInventaire[] = [];
  console.log(this.selection.selected);
  if (this.selection.selected.length !== 0 ) {
  const codes: string[] = this.selection.selected.map((documentLelement) => {
    return documentLelement.code;
  });
  this.listDocumentsInventaires.forEach(documentLO => {
    if (codes.findIndex(code => code === documentLO.code) > -1) {
      listSelectedDocumentsInventaires.push(documentLO);
    }
    });
  }
  // this.select.emit(listSelectedPayementsCredits);
  
  }
  transformDataToByConsumedByMatTable() {
    const listElements = [];
    if (this.listDocumentsInventaires !== undefined) {

      this.listDocumentsInventaires.forEach(docL => {
             listElements.push( {code: docL.code ,
          dateReception: docL.dateReception,
          numeroDocument: docL.numeroDocument,
          typeDocument: docL.typeDocument,
          description: docL
          .description
        } );
      });
    }
      this.dataDocumentsInventaires = new MatTableDataSource<DocumentInventaireElement>(listElements);
      this.dataDocumentsInventaires.sort = this.sort;
      this.dataDocumentsInventaires.paginator = this.paginator;
      
      
  }
   specifyListColumnsToBeDisplayed() {
    //  console.log('calling specifyListColumnsToBeDisplayed documents grid');
    this.columnsTitles = [];
    this.columnsNames = [];
    this.columnsTitlesAr  = [];
    
     this.columnsDefinitions = [{name: 'code' , displayTitle: 'Code'},
     {name: 'dateReception' , displayTitle: 'Date Réception'},
     {name: 'numeroDocument' , displayTitle: 'N°'},
     {name: 'typeDocument' , displayTitle: 'Type Doc.'},
       { name : 'description' , displayTitle : 'Déscription'},
      ];

     this.columnsTitles = this.columnsDefinitions.map((colDef) => {
               return colDef.displayTitle;
     });
     this.columnsNames = [];
     this.columnsNames[0] = 'select';
     this.columnsDefinitions.map((colDef) => {
      this.columnsNames.push(colDef.name);
      console.log(this.columnsNames);
  });
     this.columnsTitlesAr = this.columnsDefinitions.map((colDef) => {
      return colDef.displayTitleAr;
  });
  }
  masterToggle() {
    console.log('get selected');
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataDocumentsInventaires.data.forEach(row => this.selection.select(row));
        this.checkOneActionInvoked();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataDocumentsInventaires.data.length;
    return numSelected === numRows;
  }
  applyFilter(filterValue: string) {
    this.dataDocumentsInventaires.filter = filterValue.trim().toLowerCase();
    if (this.dataDocumentsInventaires.paginator) {
      this.dataDocumentsInventaires.paginator.firstPage();
    }
  }
  showAddDocumentDialog()
  {
     
   const dialogConfig = new MatDialogConfig();
   
   dialogConfig.disableClose = false;
   dialogConfig.autoFocus = true;
   dialogConfig.position = {
     top : '0'
   };
   dialogConfig.data = {
     codeInventaire: this.inventaire.code
   };
 
   const dialogRef = this.dialog.open(DialogAddDocumentInventaireComponent,
     dialogConfig
   );
   dialogRef.afterClosed().subscribe(docAdded => {
    console.log('The dialog was closed');
     this.refresh() ;

  });
 }
 openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 showEditDocumentDialog() {
    
    if ( this.selection.selected.length !== 0) {
        
      const dialogConfig = new MatDialogConfig();
      
      dialogConfig.disableClose = false;
      dialogConfig.autoFocus = true;
      dialogConfig.position = {
        top : '0'
      };
      dialogConfig.data = {
        codeDocumentInventaire : this.selection.selected[0].code
      };
    
      const dialogRef = this.dialog.open(DialogEditDocumentInventaireComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe(docAdded => {
        console.log('The dialog was closed');
         this.refresh() ;
    
      });
    } else {
      this.openSnackBar('Aucun Document Séléctionnée!');
    }
    }
    supprimerDocuments()
    {
      console.log(this.selection.selected.length);
      console.log(this.selection.selected);
      if ( this.selection.selected.length !== 0) {
          this.selection.selected.forEach(function(doc, index) {
              this.inventairesService.deleteDocumentInventaire(doc.code).subscribe((response) => {
    
              });
              if (this.selection.selected.length - 1 === index) {
                this.openSnackBar( ' Element(s) Supprimé(s)');
                 this. refresh();
                }
          }, this);
        } else {
          this.openSnackBar('Aucun Document Séléctionnée!');
        }
    }
    refresh() {
      this.inventairesService.getDocumentsInventairesByCodeInventaire(this.inventaire.code).subscribe((listDocumentsInvs) => {
        this.listDocumentsInventaires = listDocumentsInvs;
        this.specifyListColumnsToBeDisplayed();
        this.transformDataToByConsumedByMatTable();
         });
    }
}
