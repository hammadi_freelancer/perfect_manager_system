import { Component, OnInit, Output,ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {Inventaire} from './inventaire.model';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {InventairesService} from './inventaires.service';
import { InventaireElement } from './inventaire.interface';
import { InventaireFilter } from './inventaire.filter';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {SelectItem} from 'primeng/api';

// import { DialogAddDocumentCreditComponent } from './dialog-add-document-credit.component';
@Component({
  selector: 'app-inventaires-grid',
  templateUrl: './inventaires-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class InventairesGridComponent implements OnInit {

  private inventaire: Inventaire =  new Inventaire();
  private inventaireFilter: InventaireFilter = new InventaireFilter();
  
  private selection = new SelectionModel<InventaireElement>(true, []);
  private columnsToSelect : SelectItem[];
   private  selectedColumnsDefinitions: string[];
  private selectedColumnsNames: string[];
  
@Output()
select: EventEmitter<Inventaire[]> = new EventEmitter<Inventaire[]>();

//private lastClientAdded: Client;

 private selectedInventaire: Inventaire ;
private   showFilter = false;
private showSelectColumnsMultiSelect = false;

 @Input()
 private listInventaires: Inventaire[] = [] ;

 private deleteEnCours = false;
 private checkedAll: false;
 private  dataInventairesSource: MatTableDataSource<InventaireElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay :ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
 private etatsInventaire: any[] = [
  {value: 'Ouvert', viewValue: 'Ouvert'},
  {value: 'Initial', viewValue: 'Initiale'},
  {value: 'EnCoursValidation', viewValue: 'En Cours de Validation'},
  {value: 'Valide', viewValue: 'Validé'},
  {value: 'Cloture', viewValue: 'Cloturé'}
];
  constructor( private route: ActivatedRoute, private inventairesService: InventairesService,
    private router: Router, private matSnackBar: MatSnackBar ) {
  }
  ngOnInit() {
    this.inventairesService.getInventaires(0, 5, null).subscribe((inventaires) => {
      this.listInventaires = inventaires;
      // this.listClients[this.listClients.length] = this.lastClientAdded;
      // this.listClientsOrgin = this.listClients ;
      this.defaultColumnsDefinitions = [{name: 'numeroInventaire' , displayTitle: 'N°Inventaire'},
      {name: 'dateInventaire' , displayTitle: 'Date'},
      {name: 'periodeFrom' , displayTitle: 'Période de:'},
      {name: 'periodeTo' , displayTitle: 'à'},
       {name: 'montantVentes' , displayTitle: 'Valeur Ventes'},
       {name: 'montantAchats' , displayTitle: 'Valeur Achats'},
       {name: 'montantCharges' , displayTitle: 'Valeur Charges'},
       {name: 'montantRH' , displayTitle: 'Valeur R.Humaines'},
       { name : 'etat' , displayTitle : 'Etat'},
       { name : 'description' , displayTitle : 'Déscription'},
   
        ];
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  /*  this.columnsToSelect = [
      {label: 'N°Journal', value: {id: 1, name: 'numeroJournal', displayTitle: 'N°Journal'}},
      {label: 'Date Journal', value: {id: 2, name: 'dateJournal', displayTitle: 'Date Journal'}},
      {label: 'Valeur Ventes', value: {id: 3, name: 'montantVentes', displayTitle: 'Valeur Ventes'}},
      {label: 'Valeur Achats', value: {id: 4, name: 'montantAchats', displayTitle: 'Valeur Achats'}},
      {label: 'Valeur R.Humaines', value: {id: 5, name: 'montantRH', displayTitle: 'Valeur R.Humaines'}},
      {label: 'Valeur Charges', value: {id: 6, name: 'montantCharges', displayTitle: 'Valeur Charges'} },
      {label: 'Déscription', value: {id: 7, name: 'description', displayTitle: 'Déscription'} },
      {label: 'Etat', value: {id: 8, name: 'etat', displayTitle: 'Etat'} },
      

  ];*/

      this.columnsToSelect = [
      {label: 'N°Inventaire', value: 'numeroLivreOre', title: 'N°Inventaire'},
      {label: 'Date', value: 'dateInventaire', title: 'Date'},
      {label: 'Période de', value: 'periodeFrom', title: 'Période de:'},
      {label: 'Période à', value: 'periodeTo', title: 'Période à:'},
      {label: 'Valeur Ventes', value: 'montantVentes', title: 'Valeur Ventes'},
      {label: 'Valeur Achats', value: 'montantAchats', title: 'Valeur Achats'},
      {label: 'Valeur R.Humaines', value: 'montantRH', title: 'Valeur R.Humaines'},
      {label: 'Valeur Charges', value:  'montantCharges', title: 'Valeur Charges' },
      {label: 'Déscription', value: 'description', title: 'Déscription' },
      {label: 'Etat', value:  'etat', title: 'Etat' },
      

  ];
  });
   /* this.creditsService.getCredits().subscribe((credits) => {
      console.log('list credits ', credits);
      // this.listCredits = credits;
      this.listCredits =  this.listCredits.map((credit) => {
        if (credit.client === 'undefined' || credit.client === null ) {
           credit.client = new Client();
         }
         return credit;
   });      this.listCreditsOrgin = this.listCredits ;
     });*/
    // this.listClientsOrgin = this.listClients ;

  }
  deleteInventaire(inventaire) {
     //  console.log('call delete !', credit );
    this.inventairesService.deleteInventaire(inventaire.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.client = new Client();
        console.log('call delete !', response);
        this.loadData( null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.inventairesService.getInventaires(0, 5 , filter).subscribe((inventaires) => {
    this.listInventaires = inventaires;
    this.transformDataToByConsumedByMatTable();
   });
  // this.listClientsOrgin = this.listClients ;
}


checkOneActionInvoked() {
  const listSelectedInventaires: Inventaire[] = [];
console.log('selected clients are ');
console.log(this.selection.selected);
if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((inventaireElement) => {
  return inventaireElement.code;
});
this.listInventaires.forEach(inv => {
  if (codes.findIndex(code => code === inv.code) > -1) {
    listSelectedInventaires.push(inv);
  }
  });
}
this.select.emit(listSelectedInventaires);

}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  this.dataInventairesSource = null;
  if (this.listInventaires !== undefined) {
   //  console.log('diffe of undefined');
    this.listInventaires.forEach(inventaire => {
           listElements.push(
             {code: inventaire.code ,
            numeroInventaire: inventaire.numeroInventaire,
            dateInventaire: inventaire.dateInventaire ,
            periodeFrom: inventaire.periodeFrom ,
            periodeTo: inventaire.periodeTo ,
            montantVentes: inventaire.montantVentes,
            montantAchats: inventaire.montantAchats,
            montantRH: inventaire.montantRH,
            montantCharges: inventaire.montantCharges,
            description: inventaire.description,
   etat: inventaire.etat === 'Valide' ?  'Validé' : inventaire.etat === 'Annule' ? 'Annulé' :
   inventaire.etat === 'EnCoursValidation' ?  'En Cours De Validation' :   inventaire.etat === 'Cloture' ? 'Cloturé' :
   inventaire.etat === 'Initial' ? 'Initiale' : 'Ouvert'

      } );
    });
  }
    this.dataInventairesSource = new MatTableDataSource<InventaireElement>(listElements);
    this.dataInventairesSource.sort = this.sort;
    this.dataInventairesSource.paginator = this.paginator;
    this.inventairesService.getTotalInventaires(null).subscribe((response) => {
    //  console.log('Total credits is ', response);
      this.dataInventairesSource.paginator.length = response.totalInventaires;
    });
}
 specifyListColumnsToBeDisplayed() {
 
  console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
  this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
?
   this.defaultColumnsDefinitions : [];
   this.selectedColumnsNames = [];
   
   if (this.selectedColumnsDefinitions !== undefined)
    {
      this.selectedColumnsDefinitions.forEach((colName) => { 
           const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
           const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
           const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
           this.columnsDefinitionsToDisplay.push(colDef);
          });
    }
   this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
  this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
             return colDef.displayTitle;
   });
   this.columnsNames = [];
   this.columnsNames[0] = 'select';
   this.columnsDefinitionsToDisplay .map((colDef) => {
    this.columnsNames.push(colDef.name);
   if (this.selectedColumnsDefinitions !== undefined) {
    this.selectedColumnsNames.push(colDef.displayTitle);
   }
});
   this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
    return colDef.displayTitleAr;
});
}
masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataInventairesSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataInventairesSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataInventairesSource.filter = filterValue.trim().toLowerCase();
  if (this.dataInventairesSource.paginator) {
    this.dataInventairesSource.paginator.firstPage();
  }
}

loadAddInventaireComponent() {
  this.router.navigate(['/pms/suivie/ajouterInventaire']) ;

}
loadEditInventaireComponent() {
  this.router.navigate(['/pms/suivie/editerInventaire', this.selection.selected[ this.selection.selected.length - 1].code]) ;

}
supprimerInventaires()
{
 
  if ( this.selection.selected.length !== 0) {
    this.deleteEnCours = true;
      this.selection.selected.forEach(function(inventaire, index) {
          this.inventairesService.deleteInventaire(inventaire.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.deleteEnCours = false;
            
            this.openSnackBar( ' Element(s) Supprimé(s)');
             this.loadData(null);
            }
      }, this);
    } else {
      this.openSnackBar('Aucun Livre Ore Séléctionné !');
    }
}
refresh() {
  this.loadData(null);
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 
 toggleFilterPanel()
 {
    this.showFilter = !this.showFilter;
 }
 toggleShowColumnsMultiSelect()
 {
   this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
 }
 loadFiltredData()
 {
   console.log('the filter is ', this.inventaireFilter);
   this.loadData(this.inventaireFilter);
 }

changePage($event) {
  console.log('page event is ', $event);
 this.inventairesService.getInventaires($event.pageIndex, $event.pageSize, this.inventaireFilter).subscribe((inventaires) => {
    this.listInventaires = inventaires;
   const listElements = [];
     console.log('diffe of undefined');
     this.listInventaires.forEach(inventaire => {
      listElements.push( {
        code: inventaire.code ,
        numeroInventaire: inventaire.numeroInventaire,
        dateInventaire: inventaire.dateInventaire ,
        periodeFrom: inventaire.periodeFrom ,
        periodeTo: inventaire.periodeTo ,
        montantVentes: inventaire.montantVentes,
        montantAchats: inventaire.montantAchats,
        montantRH: inventaire.montantRH,
        montantCharges: inventaire.montantCharges,
        description: inventaire.description,
etat: inventaire.etat === 'Valide' ?  'Validé' : inventaire.etat === 'Annule' ? 'Annulé' :
inventaire.etat === 'EnCoursValidation' ?  'En Cours De Validation' :   inventaire.etat === 'Cloture' ? 'Cloturé' :
inventaire.etat === 'Initial' ? 'Initiale' : 'Ouvert'

      }
);

});
this.dataInventairesSource = new MatTableDataSource<InventaireElement>(listElements);
this.inventairesService.getTotalInventaires(null).subscribe((response) => {
 console.log('Total credits is ', response);
  this.dataInventairesSource.paginator.length = response.totalInventaires;
});
});
}
}
