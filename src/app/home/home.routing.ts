import { Injectable } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
// import { GestionClientsComponent } from './clients/gestion-clients.component';
// import { FactureVenteComponent } from './facture-vente/facture-vente.component';
 import { HomeComponent } from './home.component';
 // //import { DashboardsModule } from '../dashboards/dashboards.module';
 // import { DashboardVenteComponent} from '../dashboards/dashboard-vente.component';
import {ModuleWithProviders  } from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';

 const homeRoute: Routes = [
    {
        path: 'home/welcome',
        component: HomeComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    }
];
export const homeRoutingModule: ModuleWithProviders = RouterModule.forRoot(homeRoute);
// commonRouting.providers = [{ provide: APP_BASE_HREF, useValue: '/common'} ] ;


