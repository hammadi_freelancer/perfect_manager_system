import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class HomeService {

 private  resourceUrlListFacturesVente = 'http://localhost:8082/listFacturesVentes';
 private resourceUrlAddFactureVente = 'http://localhost:8082/addFactureVente';

  constructor(private httpClient: HttpClient) {

   }


  /* getClients(): Array<Client> {
    const listClients = new Array();
    listClients.push(new Client('MMPP', 'Adam', 'Smith' , '989998', 'Cite 4', '788999'  ));
    listClients.push(new Client('NNNN', 'Jhon', 'Dark' , '55666', 'Cite 6', '3456666'  ));
    listClients.push(new Client('SSS', 'Filipe', 'Gonzalez' , '4444', 'Cite 9', '4555555'  ));
    return listClients;
}*/
}
