import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
 import { AppComponent } from '../app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ReactiveFormsModule } from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatMenuModule} from '@angular/material/menu';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {TableModule} from 'primeng/table';
import { RouterModule, provideRoutes} from '@angular/router';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';


 import { DocumentsModule } from '../documents/documents.module';
 import { FinanceModule } from '../finance/finance.module';
 import { VentesModule } from '../ventes/ventes.module';
 import { AchatsModule } from '../achats/achats.module';
 import { StocksModule } from '../stocks/stocks.module';
 import { JournauxModule} from '../journaux/journaux.module';
 import { OperationsCourantesModule} from '../operations-courantes/operations-courantes.module';
 import { ComptesClientsModule} from '../comptes-clients/comptes-clients.module';
 import { ComptesFournisseursModule} from '../comptes-fournisseurs/comptes-fournisseurs.module';
 
import { UtilisateursModule } from '../utilisateurs/utilisateurs.module';
import { LivresOresModule} from '../livres-ores/livres-ores.module';
import { InventairesModule } from '../inventaires/inventaires.module';

import { HomeComponent} from './home.component';
import { homeRoutingModule } from './home.routing' ;

import {MatSidenavModule} from '@angular/material/sidenav';






import {FormsModule } from '@angular/forms';
// import { commonRoute } from './common.route';
 import {APP_BASE_HREF} from '@angular/common';

@NgModule({
  declarations: [

    HomeComponent

  ],
  imports: [
    BrowserModule, HttpClientModule, MatDatepickerModule, MatNativeDateModule,
     BrowserAnimationsModule, ReactiveFormsModule, MatAutocompleteModule,
    MatInputModule, MatCardModule, MatCheckboxModule, MatButtonModule, MatIconModule,
    MatMenuModule, MatProgressSpinnerModule,
    MatDividerModule, MatListModule,
    FormsModule, homeRoutingModule, AchatsModule,
   // ventesRoutingModule,
   // CommonModule, TableModule ,
    DocumentsModule, UtilisateursModule,
    FinanceModule, VentesModule, StocksModule,
    JournauxModule,
    InventairesModule,
    OperationsCourantesModule,
    ComptesClientsModule,
    ComptesFournisseursModule, LivresOresModule,
    MatSidenavModule
],
  providers: [MatNativeDateModule  // BASE_URL
  ],
  exports : [
    RouterModule,
    // GestionClientsComponent,
     // GestionDocumentsTypesComponent,
    //  DocumentTypesGridComponent,
    // DocumentTypeComponent

  ],
  bootstrap: [AppComponent]
})
export class HomeModule { }
