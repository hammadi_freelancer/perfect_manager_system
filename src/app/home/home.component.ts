import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';

const ACTIONS = ['VENTES_MODULE','ACHATS_MODULE','STOCKS_MODULE',

  'ARTICLES','MAGASINS','FOURNISSEURS','CLIENTS','CREDITS_VENTES','CREDITS_ACHATS','HOME'];
@Component({
  selector: 'app-root',
  templateUrl: './home.component.html',
  styleUrls: []
})
export class HomeComponent implements OnInit {
  // title = 'football-players';
  private showMenu = true;
  private isAuthenticated = false;
  private showLoginComponent = 'true';
  private menuItemSelected = 'HOME';
  private styleStocksMenu = '';
  private styleVentesMenu = '';
  private suivieHidden = true;
  private rhHidden = true;
  private ventesHidden = true;
  private achatsHidden = true;
  private stocksHidden = true;
  private financeHidden = true;
  private administrationHidden = true;
  private organisationHidden = true;
  private caisseHidden = true;
  private marketingHidden = true;
  

  @ViewChild('SUIVIEitems') suivieItems : ElementRef;
  
  @ViewChild('STOCKSitems') stocksItems : ElementRef;

  @ViewChild('VENTESitems') ventesItems: ElementRef;
  
  @ViewChild('ACHATSitems') achatsItems: ElementRef;
  @ViewChild('CAISSEitems') caisseItems : ElementRef;
  @ViewChild('RHitems') rhItems : ElementRef;
  @ViewChild('UTILISATEURSitems') utilisateursItems : ElementRef;
 //  @ViewChild('ORGANISATIONSitems') organisationsItems : ElementRef;
  @ViewChild('FINANCEitems') financeItems : ElementRef;
  @ViewChild('MARKETINGitems') marketingItems : ElementRef;
  
  @ViewChild('#ClientsItems ') clientsIems :ElementRef;
  @ViewChild('MenuStocks') menuStocks : ElementRef;
  @ViewChild('MenuSuivie') menuSuivie : ElementRef;
  
  @ViewChild('MenuAchats') menuAchats : ElementRef;
  @ViewChild('MenuVentes') menuVentes : ElementRef;
  @ViewChild('MenuCaisse') menuCaisse : ElementRef;
  @ViewChild('MenuFinance') menuFinance : ElementRef;
  @ViewChild('MenuMarketing') menuMarketing : ElementRef;
  
  @ViewChild('MenuRH') menuRH : ElementRef;
  @ViewChild('MenuUtilisateurs') menuUtilisateurs : ElementRef;
  // @ViewChild('MenuOrganisations') menuOrganisations : ElementRef;
  

 //  @ViewChild('ARTICLES') articlesPanel: ElementRef;
 //  @ViewChild('MAGASINS') magasinsPanel: ElementRef;
 //  @ViewChild('FOURNISSEURS') fournisseursPanel: ElementRef;
  // @ViewChild('CLIENTS') clientsPanel: ElementRef;
  // @ViewChild('CREDITS') creditsPanel: ElementRef;
  
  
 // @ViewChild('PANELHEADER') panelHeader : ElementRef;
  constructor(private route: ActivatedRoute,
    private router: Router ) {
  }
  ngOnInit() {
    // this.loadDataPlayers();
    // this.loadDataTeams();
    this.showLoginComponent = this.route.snapshot.paramMap.get('showLoginComponent');
    
    
  }
  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.isAuthenticated = false;
    this.showMenu = false;
    this.router.navigate(['/pms']) ;
    
  }

  show(menuItemName)
  {
    this.menuItemSelected = menuItemName;


    if(this.menuItemSelected === 'SUIVIE_MODULE')
      {
        this.router.navigate(['/pms']) ;
        
 
       this.suivieItems.nativeElement.hidden = !this.suivieItems.nativeElement.hidden ;
       this.suivieHidden = this.suivieItems.nativeElement.hidden ;
      
     // this.clientsPanel.nativeElement.hidden = true;
      //  this.fournisseursPanel.nativeElement.hidden = true;
       this.achatsItems.nativeElement.hidden = true;
       this.stocksItems.nativeElement.hidden = true;
       this.caisseItems.nativeElement.hidden = true;
       this.rhItems.nativeElement.hidden = true;
       this.utilisateursItems.nativeElement.hidden = true;
       // this.organisationsItems.nativeElement.hidden = true;
       this.ventesItems.nativeElement.hidden = true;
       this.financeItems.nativeElement.hidden = true;
       
       this.menuAchats.nativeElement.hidden = true;
       this.menuStocks.nativeElement.hidden = true;
       this.menuCaisse.nativeElement.hidden = true;
       this.menuRH.nativeElement.hidden = true;
       this.menuUtilisateurs.nativeElement.hidden = true;
      // this.menuOrganisations.nativeElement.hidden = true;
       this.menuVentes.nativeElement.hidden = true;
       this.menuFinance.nativeElement.hidden = true;
       this.menuMarketing.nativeElement.hidden = true;
       
     if(this.suivieItems.nativeElement.hidden)
         {
           this.menuAchats.nativeElement.hidden = false;
           this.menuStocks.nativeElement.hidden = false;
           this.menuCaisse.nativeElement.hidden = false;
           this.menuRH.nativeElement.hidden = false;
           this.menuUtilisateurs.nativeElement.hidden = false;
          //  this.menuOrganisations.nativeElement.hidden = false;
           this.menuVentes.nativeElement.hidden = false;
           this.menuFinance.nativeElement.hidden = true;
           this.menuFinance.nativeElement.hidden = false;
           this.menuMarketing.nativeElement.hidden = false;
           
           this.ventesHidden = true;
           
         }
      //  this.articlesPanel.nativeElement.hidden = true;
      //  this.magasinsPanel.nativeElement.hidden = true;
       // this.creditsPanel.nativeElement.hidden = true;
 
      }




    if(this.menuItemSelected === 'VENTES_MODULE')
     {
      
      this.ventesItems.nativeElement.hidden = !this.ventesItems.nativeElement.hidden ;
      this.ventesHidden = this.ventesItems.nativeElement.hidden;
      this.router.navigate(['/pms']) ;
      
      // this.ventesItems.nativeElement.hidden = false;
    //  this.clientsPanel.nativeElement.hidden = true;
      // this.fournisseursPanel.nativeElement.hidden = true;
      this.achatsItems.nativeElement.hidden = true;
      this.stocksItems.nativeElement.hidden = true;
      this.caisseItems.nativeElement.hidden = true;
      this.rhItems.nativeElement.hidden = true;
      this.utilisateursItems.nativeElement.hidden = true;
     //  this.organisationsItems.nativeElement.hidden = true;
      this.suivieItems.nativeElement.hidden = true;
      this.financeItems.nativeElement.hidden = true;
      
      this.menuAchats.nativeElement.hidden = true;
      this.menuStocks.nativeElement.hidden = true;
      this.menuCaisse.nativeElement.hidden = true;
      this.menuRH.nativeElement.hidden = true;
      this.menuUtilisateurs.nativeElement.hidden = true;
    //   this.menuOrganisations.nativeElement.hidden = true;
      this.menuSuivie.nativeElement.hidden = true;
      this.menuFinance.nativeElement.hidden = true;
      this.menuMarketing.nativeElement.hidden = true;
      
      
      
      if(this.ventesItems.nativeElement.hidden)
        {
          this.menuAchats.nativeElement.hidden = false;
          this.menuStocks.nativeElement.hidden = false;
          this.menuCaisse.nativeElement.hidden = false;
          this.menuRH.nativeElement.hidden = false;
          this.menuUtilisateurs.nativeElement.hidden = false;
         //  this.menuOrganisations.nativeElement.hidden = false;
          this.menuSuivie.nativeElement.hidden = false;
          this.menuFinance.nativeElement.hidden = false;
          this.menuMarketing.nativeElement.hidden = false;
          
        }
     //  this.articlesPanel.nativeElement.hidden = true;
     //  this.magasinsPanel.nativeElement.hidden = true;
      // this.creditsPanel.nativeElement.hidden = true;

     }
     if(this.menuItemSelected === 'ACHATS_MODULE')
      {
        this.router.navigate(['/pms']) ;
        
       this.achatsItems.nativeElement.hidden = !this.achatsItems.nativeElement.hidden ;
       this.achatsHidden = this.achatsItems.nativeElement.hidden;
       
       // this.clientsPanel.nativeElement.hidden = true;
       // this.fournisseursPanel.nativeElement.hidden = true;
      this.caisseItems.nativeElement.hidden = true;       
       this.ventesItems.nativeElement.hidden = true;
       this.stocksItems.nativeElement.hidden = true;
       this.rhItems.nativeElement.hidden = true;       
       this.utilisateursItems.nativeElement.hidden = true;
       // this.organisationsItems.nativeElement.hidden = true;
       this.suivieItems.nativeElement.hidden = true;
       this.financeItems.nativeElement.hidden = true;
       
       this.menuVentes.nativeElement.hidden = true;
       this.menuStocks.nativeElement.hidden = true;
       this.menuCaisse.nativeElement.hidden = true;
       this.menuRH.nativeElement.hidden = true;
       this.menuUtilisateurs.nativeElement.hidden = true;
       // this.menuOrganisations.nativeElement.hidden = true;
       this.menuSuivie.nativeElement.hidden = true;
       this.menuFinance.nativeElement.hidden = true;
       this.menuMarketing.nativeElement.hidden = true;
       
       if(this.achatsItems.nativeElement.hidden)
        {
          this.menuVentes.nativeElement.hidden = false;
          this.menuStocks.nativeElement.hidden = false;
          this.menuCaisse.nativeElement.hidden = false;
          this.menuRH.nativeElement.hidden = false;
          this.menuUtilisateurs.nativeElement.hidden = false;
          // this.menuOrganisations.nativeElement.hidden = false;
          this.menuSuivie.nativeElement.hidden = false;
          this.menuFinance.nativeElement.hidden = false;
          this.menuMarketing.nativeElement.hidden = false;
          
          
        }
      //  this.articlesPanel.nativeElement.hidden = true;
      //  this.magasinsPanel.nativeElement.hidden = true;
      // this.creditsPanel.nativeElement.hidden = true;
 
      }

      if(this.menuItemSelected === 'FINANCE_MODULE')
        {
         
         this.financeItems.nativeElement.hidden = !this.financeItems.nativeElement.hidden ;
         this.financeHidden = this.financeItems.nativeElement.hidden;
         this.router.navigate(['/pms']) ;
         this.achatsItems.nativeElement.hidden = true;
         this.ventesItems.nativeElement.hidden = true;
         
         this.stocksItems.nativeElement.hidden = true;
         this.caisseItems.nativeElement.hidden = true;
         this.rhItems.nativeElement.hidden = true;
         this.utilisateursItems.nativeElement.hidden = true;
       //  this.organisationsItems.nativeElement.hidden = true;
         this.suivieItems.nativeElement.hidden = true;
         
         this.menuAchats.nativeElement.hidden = true;
         this.menuStocks.nativeElement.hidden = true;
         this.menuCaisse.nativeElement.hidden = true;
         this.menuRH.nativeElement.hidden = true;
         this.menuUtilisateurs.nativeElement.hidden = true;
        //  this.menuOrganisations.nativeElement.hidden = true;
         this.menuSuivie.nativeElement.hidden = true;
         this.menuVentes.nativeElement.hidden = true;
         this.menuMarketing.nativeElement.hidden = true;
         
         if(this.financeItems.nativeElement.hidden)
           {
             this.menuAchats.nativeElement.hidden = false;
             this.menuStocks.nativeElement.hidden = false;
             this.menuCaisse.nativeElement.hidden = false;
             this.menuRH.nativeElement.hidden = false;
             this.menuUtilisateurs.nativeElement.hidden = false;
            //  this.menuOrganisations.nativeElement.hidden = false;
             this.menuSuivie.nativeElement.hidden = false;
             this.menuVentes.nativeElement.hidden = false;
             this.menuMarketing.nativeElement.hidden = false;
             
             
           }
        //  this.articlesPanel.nativeElement.hidden = true;
        //  this.magasinsPanel.nativeElement.hidden = true;
         // this.creditsPanel.nativeElement.hidden = true;
   
        }
        if(this.menuItemSelected === 'MARKETING_MODULE')
          {
            this.router.navigate(['/pms']) ;
            
           this.marketingItems.nativeElement.hidden = !this.marketingItems.nativeElement.hidden ;
           this.marketingHidden = this.marketingItems.nativeElement.hidden;
           
           // this.clientsPanel.nativeElement.hidden = true;
           // this.fournisseursPanel.nativeElement.hidden = true;
          this.caisseItems.nativeElement.hidden = true;       
           this.ventesItems.nativeElement.hidden = true;
           this.stocksItems.nativeElement.hidden = true;
           this.rhItems.nativeElement.hidden = true;       
           this.utilisateursItems.nativeElement.hidden = true;
           this.achatsItems.nativeElement.hidden = true;
           
           // this.organisationsItems.nativeElement.hidden = true;
           this.suivieItems.nativeElement.hidden = true;
           this.financeItems.nativeElement.hidden = true;
           
           this.menuVentes.nativeElement.hidden = true;
           this.menuStocks.nativeElement.hidden = true;
           this.menuCaisse.nativeElement.hidden = true;
           this.menuRH.nativeElement.hidden = true;
           this.menuUtilisateurs.nativeElement.hidden = true;
           // this.menuOrganisations.nativeElement.hidden = true;
           this.menuSuivie.nativeElement.hidden = true;
           this.menuFinance.nativeElement.hidden = true;
           this.menuAchats.nativeElement.hidden = true;
           
           if (this.marketingItems.nativeElement.hidden) {
              this.menuVentes.nativeElement.hidden = false;
              this.menuStocks.nativeElement.hidden = false;
              this.menuCaisse.nativeElement.hidden = false;
              this.menuRH.nativeElement.hidden = false;
              this.menuUtilisateurs.nativeElement.hidden = false;
              // this.menuOrganisations.nativeElement.hidden = false;
              this.menuSuivie.nativeElement.hidden = false;
              this.menuFinance.nativeElement.hidden = false;
              this.menuAchats.nativeElement.hidden = false;
              
              
            }
          //  this.articlesPanel.nativeElement.hidden = true;
          //  this.magasinsPanel.nativeElement.hidden = true;
          // this.creditsPanel.nativeElement.hidden = true;
     
          }
    
      if(this.menuItemSelected === 'CAISSE_MODULE')
        {
          this.router.navigate(['/pms']) ;
          
         this.caisseItems.nativeElement.hidden = !this.caisseItems.nativeElement.hidden ;
         this.caisseHidden = this.caisseItems.nativeElement.hidden;
         // this.clientsPanel.nativeElement.hidden = true;
        //  this.fournisseursPanel.nativeElement.hidden = true;
         this.achatsItems.nativeElement.hidden = true;
         this.ventesItems.nativeElement.hidden = true;
         this.stocksItems.nativeElement.hidden = true;
         this.rhItems.nativeElement.hidden = true;
         this.utilisateursItems.nativeElement.hidden = true;
        //  this.organisationsItems.nativeElement.hidden = true;
         this.suivieItems.nativeElement.hidden = true;
         this.financeItems.nativeElement.hidden = true;
         
         this.menuAchats.nativeElement.hidden = true;
         this.menuVentes.nativeElement.hidden = true;
         this.menuStocks.nativeElement.hidden = true;
         this.menuRH.nativeElement.hidden = true;
         this.menuUtilisateurs.nativeElement.hidden = true;
        //  this.menuOrganisations.nativeElement.hidden = true;
         this.menuSuivie.nativeElement.hidden = true;
         this.menuFinance.nativeElement.hidden = true;
         this.menuMarketing.nativeElement.hidden = true;
         
         if(this.caisseItems.nativeElement.hidden)
          {
            this.menuVentes.nativeElement.hidden = false;
            this.menuAchats.nativeElement.hidden = false;
            this.menuStocks.nativeElement.hidden = false;
            this.menuRH.nativeElement.hidden = false;
            this.menuUtilisateurs.nativeElement.hidden = false;
           //  this.menuOrganisations.nativeElement.hidden = false;
            this.menuSuivie.nativeElement.hidden = false;
            this.menuFinance.nativeElement.hidden = false;
            this.menuMarketing.nativeElement.hidden = false;
            
          }
         // this.styleStocksMenu = 'padding-top:3px';
         // this.styleVentesMenu = 'display:none';
         // this.articlesPanel.nativeElement.hidden = true;
         // this.magasinsPanel.nativeElement.hidden = true;
         // this.creditsPanel.nativeElement.hidden = true;
   
        }

        if(this.menuItemSelected === 'RH_MODULE')
          {
            this.router.navigate(['/pms']) ;
            
           this.rhItems.nativeElement.hidden = !this.rhItems.nativeElement.hidden ;
           this.rhHidden =  this.rhItems.nativeElement.hidden;
           // this.clientsPanel.nativeElement.hidden = true;
          //  this.fournisseursPanel.nativeElement.hidden = true;
          this.achatsItems.nativeElement.hidden = true;
           this.ventesItems.nativeElement.hidden = true;
           this.stocksItems.nativeElement.hidden = true;
           this.caisseItems.nativeElement.hidden = true;
           this.utilisateursItems.nativeElement.hidden = true;
          //  this.organisationsItems.nativeElement.hidden = true;
           this.suivieItems.nativeElement.hidden = true;
           this.financeItems.nativeElement.hidden = true;
           
          // this.rhItems.nativeElement.hidden = false;
           
           this.menuAchats.nativeElement.hidden = true;
           this.menuVentes.nativeElement.hidden = true;
           this.menuStocks.nativeElement.hidden = true;
          this.menuCaisse.nativeElement.hidden = true;
          this.menuUtilisateurs.nativeElement.hidden = true;
         //  this.menuOrganisations.nativeElement.hidden = true;
          this.menuSuivie.nativeElement.hidden = true;
          this.menuFinance.nativeElement.hidden = true;
          this.menuMarketing.nativeElement.hidden = true;
          
           
           if(this.rhItems.nativeElement.hidden)
            {
              this.menuVentes.nativeElement.hidden = false;
              this.menuAchats.nativeElement.hidden = false;
              this.menuStocks.nativeElement.hidden = false;
              this.menuCaisse.nativeElement.hidden = false;
              this.menuUtilisateurs.nativeElement.hidden = false;
             // this.menuOrganisations.nativeElement.hidden = false;
              this.menuSuivie.nativeElement.hidden = false;
              this.menuFinance.nativeElement.hidden = false;
              this.menuMarketing.nativeElement.hidden = false;
              
            }
           // this.styleStocksMenu = 'padding-top:3px';
           // this.styleVentesMenu = 'display:none';
           // this.articlesPanel.nativeElement.hidden = true;
           // this.magasinsPanel.nativeElement.hidden = true;
           // this.creditsPanel.nativeElement.hidden = true;
     
          }



          if(this.menuItemSelected === 'UTILISATEURS_MODULE')
            {
              this.router.navigate(['/pms']) ;
              
             this.utilisateursItems.nativeElement.hidden = !this.utilisateursItems.nativeElement.hidden ;
             // this.clientsPanel.nativeElement.hidden = true;
            //  this.fournisseursPanel.nativeElement.hidden = true;
             this.achatsItems.nativeElement.hidden = true;
             this.ventesItems.nativeElement.hidden = true;
             this.stocksItems.nativeElement.hidden = true;
             this.caisseItems.nativeElement.hidden = true;
             this.rhItems.nativeElement.hidden = true;
            //  this.organisationsItems.nativeElement.hidden = true;
             this.suivieItems.nativeElement.hidden = true;
             this.financeItems.nativeElement.hidden = true;
             
            this.rhItems.nativeElement.hidden = false;
             
             this.menuAchats.nativeElement.hidden = true;
             this.menuVentes.nativeElement.hidden = true;
             this.menuStocks.nativeElement.hidden = true;
            this.menuCaisse.nativeElement.hidden = true;
            this.menuRH.nativeElement.hidden = true;
           //  this.menuOrganisations.nativeElement.hidden = true;
            this.menuSuivie.nativeElement.hidden = true;
            this.menuFinance.nativeElement.hidden = true;
            this.menuMarketing.nativeElement.hidden = true;
            
             
             if(this.utilisateursItems.nativeElement.hidden)
              {
                this.menuVentes.nativeElement.hidden = false;
                this.menuAchats.nativeElement.hidden = false;
                this.menuStocks.nativeElement.hidden = false;
                this.menuCaisse.nativeElement.hidden = false;
                this.menuRH.nativeElement.hidden = false;
               //  this.menuOrganisations.nativeElement.hidden = false;
                this.menuSuivie.nativeElement.hidden = false;
                this.menuFinance.nativeElement.hidden = false;
                this.menuMarketing.nativeElement.hidden = false;
                
                
              }
             // this.styleStocksMenu = 'padding-top:3px';
             // this.styleVentesMenu = 'display:none';
             // this.articlesPanel.nativeElement.hidden = true;
             // this.magasinsPanel.nativeElement.hidden = true;
             // this.creditsPanel.nativeElement.hidden = true;
       
            }


        if(this.menuItemSelected === 'ORGANISATIONS_MODULE')
          {
            this.router.navigate(['/pms']) ;
            
               // this.organisationsItems.nativeElement.hidden = !this.organisationsItems.nativeElement.hidden ;
              //  this.organisationHidden = this.organisationsItems.nativeElement.hidden;
               // this.clientsPanel.nativeElement.hidden = true;
              //  this.fournisseursPanel.nativeElement.hidden = true;
               this.achatsItems.nativeElement.hidden = true;
               this.ventesItems.nativeElement.hidden = true;
               this.stocksItems.nativeElement.hidden = true;
               this.caisseItems.nativeElement.hidden = true;
               this.rhItems.nativeElement.hidden = true;
               this.utilisateursItems.nativeElement.hidden = true;
               this.suivieItems.nativeElement.hidden = true;
               this.financeItems.nativeElement.hidden = true;
               
               this.rhItems.nativeElement.hidden = true;
               
               this.menuAchats.nativeElement.hidden = true;
               this.menuVentes.nativeElement.hidden = true;
               this.menuStocks.nativeElement.hidden = true;
              this.menuCaisse.nativeElement.hidden = true;
              this.menuRH.nativeElement.hidden = true;
              this.menuUtilisateurs.nativeElement.hidden = true;
              this.menuSuivie.nativeElement.hidden = true;
              this.menuFinance.nativeElement.hidden = true;
              this.menuMarketing.nativeElement.hidden = true;
              
               
              /* if(this.organisationsItems.nativeElement.hidden)
                {
                  this.menuVentes.nativeElement.hidden = false;
                  this.menuAchats.nativeElement.hidden = false;
                  this.menuStocks.nativeElement.hidden = false;
                  this.menuCaisse.nativeElement.hidden = false;
                  this.menuRH.nativeElement.hidden = false;
                  this.menuUtilisateurs.nativeElement.hidden = false;
                  this.menuSuivie.nativeElement.hidden = false;             
                  this.menuFinance.nativeElement.hidden = false;
                  
                }*/
               // this.styleStocksMenu = 'padding-top:3px';
               // this.styleVentesMenu = 'display:none';
               // this.articlesPanel.nativeElement.hidden = true;
               // this.magasinsPanel.nativeElement.hidden = true;
               // this.creditsPanel.nativeElement.hidden = true;
        
          }

      if(this.menuItemSelected === 'STOCKS_MODULE')
        {
          this.router.navigate(['/pms']) ;
          
         this.stocksItems.nativeElement.hidden = !this.stocksItems.nativeElement.hidden ;
         this.stocksHidden =  this.stocksItems.nativeElement.hidden;
         // this.clientsPanel.nativeElement.hidden = true;
        //  this.fournisseursPanel.nativeElement.hidden = true;
      this.caisseItems.nativeElement.hidden = true;
         this.achatsItems.nativeElement.hidden = true;
         this.ventesItems.nativeElement.hidden = true;
         this.rhItems.nativeElement.hidden = true; 
         this.financeItems.nativeElement.hidden = true;
         
         this.menuAchats.nativeElement.hidden = true;
         this.menuVentes.nativeElement.hidden = true;
         this.menuCaisse.nativeElement.hidden = true;
         this.menuRH.nativeElement.hidden = true;
         this.suivieItems.nativeElement.hidden = true;
         
         this.utilisateursItems.nativeElement.hidden = true;
         this.menuUtilisateurs.nativeElement.hidden = true;
         // this.menuOrganisations.nativeElement.hidden = true;
        //  this.organisationsItems.nativeElement.hidden = true;
         this.menuSuivie.nativeElement.hidden = true;
         this.menuFinance.nativeElement.hidden = true;
         this.menuMarketing.nativeElement.hidden = true;
         
         if(this.stocksItems.nativeElement.hidden)
          {
            this.menuVentes.nativeElement.hidden = false;
            this.menuAchats.nativeElement.hidden = false;
            this.menuCaisse.nativeElement.hidden = false;
            this.menuRH.nativeElement.hidden = false;
            this.menuUtilisateurs.nativeElement.hidden = false;
           //  this.menuOrganisations.nativeElement.hidden = false;
            this.menuSuivie.nativeElement.hidden = false;
            this.menuFinance.nativeElement.hidden = false;
            this.menuMarketing.nativeElement.hidden = false;
            
            
          }
         // this.styleStocksMenu = 'padding-top:3px';
         // this.styleVentesMenu = 'display:none';
         // this.articlesPanel.nativeElement.hidden = true;
         // this.magasinsPanel.nativeElement.hidden = true;
         // this.creditsPanel.nativeElement.hidden = true;
   
        }
       /* if(this.menuItemSelected === 'CLIENTS')
          {
            this.ventesItems.nativeElement.hidden = false;
           this.achatsItems.nativeElement.hidden = true;
           this.stocksItems.nativeElement.hidden = true;
          }
          if(this.menuItemSelected === 'ARTICLES')
            {
              this.stocksItems.nativeElement.hidden = false;
             this.achatsItems.nativeElement.hidden = true;
             this.ventesItems.nativeElement.hidden = true;
            }
           if(this.menuItemSelected === 'MAGASINS')
              {
                this.stocksItems.nativeElement.hidden = false;
               this.achatsItems.nativeElement.hidden = true;
               this.ventesItems.nativeElement.hidden = true;
              }*/
          
           /* if(this.menuItemSelected === 'FOURNISSEURS')
              {
                this.ventesItems.nativeElement.hidden = true;
               this.achatsItems.nativeElement.hidden = false;
               this.ventesItems.nativeElement.hidden = true;
               this.stocksItems.nativeElement.hidden = true;
             
               
               
              }*/
              if(this.menuItemSelected === 'CLIENTS')
                {
                 // this.ventesItems.nativeElement.hidden = false;
                  
                 // this.fournisseursPanel.nativeElement.hidden = true;
                 //this.achatsItems.nativeElement.hidden = true;
                 // this.clientsPanel.nativeElement.hidden = true;
                 //this.stocksItems.nativeElement.hidden = true;
                 this.clientsIems.nativeElement.hidden = false;
                //  this.articlesPanel.nativeElement.hidden = true;
                 // this.magasinsPanel.nativeElement.hidden = true;
                 // this.creditsPanel.nativeElement.hidden = false;
                 
                }
               /* if(this.menuItemSelected === 'MENU_PRINCIPAL')
                  {
                    this.ventesItems.nativeElement.hidden = true;
                    
                   // this.fournisseursPanel.nativeElement.hidden = true;
                   this.achatsItems.nativeElement.hidden = true;
                   // this.clientsPanel.nativeElement.hidden = true;
                   this.stocksItems.nativeElement.hidden = true;
                   this.menuVentes.nativeElement.hidden = false;
                   this.menuAchats.nativeElement.hidden = false;
                   this.menuStocks.nativeElement.hidden = false;
                   this.menuCaisse.nativeElement.hidden = false;
                  //  this.articlesPanel.nativeElement.hidden = true;
                   // this.magasinsPanel.nativeElement.hidden = true;
                   // this.creditsPanel.nativeElement.hidden = false;
                   
                  }*/

  }
  navigateToGeneraleModule() {

  }

  navigateToFinanceModule() {
  }

  navigateToVenteModule() {
  }

  navigateToAchatsModule() {
  }

  navigateToDocumentsModule() {
  }

  navigateToStocksModule() {
  }
  authenticationDone()
  {
    this.isAuthenticated = true;
    // je vais loader le composant journal 
  }
}
