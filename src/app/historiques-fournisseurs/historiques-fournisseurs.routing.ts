import { Injectable } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
// operationsCourantes components
import { HistoriqueFournisseurNewComponent } from './historique-fournisseur-new.component';
import { HistoriqueFournisseurEditComponent } from './historique-fournisseur-edit.component';
import { HistoriquesFournisseursGridComponent} from './historiques-fournisseurs-grid.component';
import { HistoriqueFournisseurHomeComponent} from './historique-fournisseur-home.component';

import { FournisseursResolver } from '../achats/fournisseurs-resolver';

import {ModuleWithProviders  } from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';

 export const historiquesFournisseursRoute: Routes = [
    {
        path: 'historiquesFournisseurs',
        component: HistoriquesFournisseursGridComponent,
        resolve: {
           // 'pagingParams': AssetResolvePagingParams
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'ajouterHistoriqueFournisseur',
        component: HistoriqueFournisseurNewComponent,
        resolve: {
            fournisseurs: FournisseursResolver,
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
    {
        path: 'editerHistoriqueFournisseur/:codeHistoriqueFournisseur',
        component: HistoriqueFournisseurEditComponent,
        resolve: {
            fournisseurs: FournisseursResolver,
        },
        data: {
           // authorities: ['ROLE_VIEW_ASSET'],
           // pageTitle: 'safeleasegtwApp.asset.home.title'
        },
        // canActivate: [UserRouteAccessService]
    },
];
// export const ventesRoutingModule: ModuleWithProviders = RouterModule.forRoot(ventesRoute);
// commonRouting.providers = [{ provide: APP_BASE_HREF, useValue: '/common'} ] ;


