import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,
  
    ViewChild } from '@angular/core';
  // import { ArticleService } from './article.service';
  import {HistoriqueFournisseur} from './historique-fournisseur.model';
  // import { ParametresVentes } from '../parametres-ventes.model';
  
  import {HistoriquesFournisseursService} from './historiques-fournisseurs.service';
  
  // import {VentesService} from '../ventes.service';
  import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
  
  import {FormControl} from '@angular/forms';
  import {map, startWith} from 'rxjs/operators';
  import {Observable} from 'rxjs';
  import { Router, ActivatedRoute, ParamMap } from '@angular/router';
  // import {ActionData} from './action-data.interface';
  import { FournisseurService } from '../achats/fournisseurs/fournisseur.service';
  
  import { FournisseurFilter } from '../achats/fournisseurs/fournisseur.filter';
  import { Fournisseur } from '../achats/fournisseurs/fournisseur.model';
  
  
  
  
  import { ColumnDefinition } from '../common/column-definition.interface';
  
  import {MatSnackBar} from '@angular/material';
  import { MatExpansionPanel } from '@angular/material';
  import {ModePayementLibelle} from './mode-payement-libelle.interface';
  import {SelectionModel} from '@angular/cdk/collections';
  
  type LibelleModePayement = 'Credit'| 'Comptant' | 'Cheque' ;
  
  @Component({
    selector: 'app-historique-fournisseur-edit',
    templateUrl: './historique-fournisseur-edit.component.html',
    // styleUrls: ['./players.component.css']
  })
export class HistoriqueFournisseurEditComponent implements OnInit  {

  private historiqueFournisseur: HistoriqueFournisseur =  new HistoriqueFournisseur();
  
  
  private updateEnCours = false;

  
  private fournisseurFilter: FournisseurFilter;
  
  private listFournisseurs: Fournisseur[];
  

    constructor( private route: ActivatedRoute,
      private router: Router, private historiqueFournisseurService: HistoriquesFournisseursService,
     //   private fournisseurService: FournisseurService,
       private fournisseurService: FournisseurService,
       
      // private articleService: ArticleService,
     private matSnackBar: MatSnackBar, private elRef: ElementRef) {
    }
  ngOnInit() {

    const codeHistoriqueFournisseur = this.route.snapshot.paramMap.get('codeHistoriqueFournisseur');
    
    this.historiqueFournisseurService.getHistoriqueFournisseur(codeHistoriqueFournisseur).subscribe(
      (histFournisseur) =>  {
             this.historiqueFournisseur = histFournisseur;
            this.listFournisseurs = this.route.snapshot.data.fournisseurs;
            this.fournisseurFilter = new FournisseurFilter(this.listFournisseurs);
      });

     
  }
  loadGridHistoriquesFournisseurs() {
    this.router.navigate(['/pms/suivie/historiquesFournisseurs']) ;
  }
updateHistoriqueFournisseur() {


  this.updateEnCours = true;
  this.historiqueFournisseurService.updateHistoriqueFournisseur(this.historiqueFournisseur).subscribe((response) => {
    this.updateEnCours = false;
    if (response.error_message ===  undefined) {
      console.log('the response of update is ', response );
      this.openSnackBar( ' Historique Fournisseur mise à jour ');
    } else {
      this.openSnackBar( ' Erreurs!');
    }
}
);
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }
 

 fillDataMatriculeRaisonFournisseur() {
   const listFiltred  = this.listFournisseurs.filter((fournisseur) =>
    (fournisseur.registreCommerce === this.historiqueFournisseur.fournisseur.registreCommerce));
   this.historiqueFournisseur.fournisseur.matriculeFiscale = listFiltred[0].matriculeFiscale;
   this.historiqueFournisseur.fournisseur.raisonSociale = listFiltred[0].raisonSociale;
 }
 fillDataMatriculeRegistreCommercialF()
 {
   const listFiltred  = this.listFournisseurs.filter((fournisseur) => (fournisseur.raisonSociale === 
     this.historiqueFournisseur.fournisseur.raisonSociale));
   this.historiqueFournisseur.fournisseur.matriculeFiscale = listFiltred[0].matriculeFiscale;
   this.historiqueFournisseur.fournisseur.registreCommerce = listFiltred[0].registreCommerce;
 }
 fillDataRegistreCommercialRaisonF()
 {
   const listFiltred  = this.listFournisseurs.filter((fournisseur) => 
   (fournisseur.matriculeFiscale === this.historiqueFournisseur.fournisseur.matriculeFiscale));
   this.historiqueFournisseur.fournisseur.raisonSociale = listFiltred[0].raisonSociale;
   this.historiqueFournisseur.fournisseur.registreCommerce = listFiltred[0].registreCommerce;
 }
 fillDataNomPrenomFournisseur() {
   const listFiltred  = this.listFournisseurs.filter((fournisseur) => (fournisseur.cin === this.historiqueFournisseur.fournisseur.cin));
   this.historiqueFournisseur.fournisseur.nom = listFiltred[0].nom;
   this.historiqueFournisseur.fournisseur.prenom = listFiltred[0].prenom;
 }
 fillDataCINPrenomFournisseur()
 {
   const listFiltred  = this.listFournisseurs.filter((fournisseur) => (fournisseur.nom === this.historiqueFournisseur.fournisseur.nom));
   this.historiqueFournisseur.fournisseur.cin = listFiltred[0].cin;
   this.historiqueFournisseur.fournisseur.prenom = listFiltred[0].prenom;
 }
 fillDataCINNomFournisseur()
 {
   const listFiltred  = 
   this.listFournisseurs.filter((fournisseur) => 
   (fournisseur.prenom === this.historiqueFournisseur.fournisseur.prenom));
   const num =   Math.floor(Math.random() * Math.floor(listFiltred.length));
   this.historiqueFournisseur.fournisseur.cin = listFiltred[num].cin;
   this.historiqueFournisseur.fournisseur.nom= listFiltred[num].nom;
 }

 



}
