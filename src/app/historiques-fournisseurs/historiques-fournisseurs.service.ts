import { Injectable } from '@angular/core';
import { Http, Response, ResponseContentType } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import {HistoriqueFournisseur} from './historique-fournisseur.model' ;
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const  URL_GET_LIST_HISTORIQUES_FOURNISSEURS = 'http://localhost:8082/historiquesFournisseurs/getHistoriquesFournisseurs';
const  URL_GET_PAGE_HISTORIQUES_FOURNISSEURS= 'http://localhost:8082/historiquesFournisseurs/getPageHistoriquesFournisseurs';

const URL_GET_HISTORIQUE_FOURNISSEUR = 'http://localhost:8082/historiquesFournisseurs/getHistoriqueFournisseur';
const  URL_ADD_HISTORIQUE_FOURNISSEUR = 'http://localhost:8082/historiquesFournisseurs/addHistoriqueFournisseur';
const  URL_UPDATE_HISTORIQUE_FOURNISSEUR  = 'http://localhost:8082/historiquesFournisseurs/updateHistoriqueFournisseur';
const  URL_DELETE_HISTORIQUE_FOURNISSEUR= 'http://localhost:8082/historiquesFournisseurs/deleteHistoriqueFournisseur';
const  URL_GET_TOTAL_HISTORIQUES_FOURNISSEURS = 'http://localhost:8082/historiquesFournisseurs/getTotalHistoriquesFournisseurs';



@Injectable({
  providedIn: 'root'
})
export class HistoriquesFournisseursService {

  constructor(private httpClient: HttpClient) {

   }
   getTotalHistoriquesFournisseurs(filter): Observable<any> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('filter', filterStr);
        return this.httpClient
        .get<any>(URL_GET_TOTAL_HISTORIQUES_FOURNISSEURS, {params});
   }
   getPageHistoriquesFournisseurs(pageNumber, nbElementsPerPage, filter): Observable<HistoriqueFournisseur[]> {
    const filterStr = JSON.stringify(filter);
    const params = new HttpParams()
    .set('pageNumber', pageNumber)
    .set('nbElementsPerPage', nbElementsPerPage)
    .set('filter', filterStr);
    return this.httpClient
    .get<HistoriqueFournisseur[]>(URL_GET_PAGE_HISTORIQUES_FOURNISSEURS, {params});
   }
   getHistoriquesFournisseurs(): Observable<HistoriqueFournisseur[]> {
   
    return this.httpClient
    .get<HistoriqueFournisseur[]>(URL_GET_LIST_HISTORIQUES_FOURNISSEURS);
   }
   addHistoriqueFournisseur(historiqueClient: HistoriqueFournisseur): Observable<any> {
    return this.httpClient.post<any>(URL_ADD_HISTORIQUE_FOURNISSEUR, historiqueClient);
  }
  updateHistoriqueFournisseur(historiqueClient: HistoriqueFournisseur): Observable<any> {
    return this.httpClient.put<any>(URL_UPDATE_HISTORIQUE_FOURNISSEUR, historiqueClient);
  }
  deleteHistoriqueFournisseur(codeHistoriqueFournisseur): Observable<any> {
    return this.httpClient.delete<any>(URL_DELETE_HISTORIQUE_FOURNISSEUR + '/' + codeHistoriqueFournisseur);
  }
  getHistoriqueFournisseur(codeHistoriqueFournisseur): Observable<HistoriqueFournisseur> {
    return this.httpClient.get<HistoriqueFournisseur>(URL_GET_HISTORIQUE_FOURNISSEUR + '/' + codeHistoriqueFournisseur);
  }
}
