import { Component, OnInit, Output,ViewChild, Input, EventEmitter  } from '@angular/core';
// import { ArticleService } from './article.service';
import {HistoriqueFournisseur} from './historique-fournisseur.model';
import {HistoriqueFournisseurFilter} from './historique-fournisseur.filter';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {HistoriquesFournisseursService} from './historiques-fournisseurs.service';
// import { ActionData } from './action-data.interface';
// import { Fournisseur } from '../../ventes/fournisseurs/fournisseur.model';
import { HistoriqueFournisseurElement } from './historique-fournisseur.interface';
import { ColumnDefinition } from '../common/column-definition.interface';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSnackBar} from '@angular/material';
import {MatDialog,MatDialogConfig,  MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
// import { DialogAddFournisseurComponent } from '../ventes/fournisseurs/dialog-add-fournisseur.component';
import {SelectItem} from 'primeng/api';

@Component({
  selector: 'app-historiques-fournisseurs-grid',
  templateUrl: './historiques-fournisseurs-grid.component.html',
  // styleUrls: ['./players.component.css']
})
export class HistoriquesFournisseursGridComponent implements OnInit {

  private historiqueFournisseur: HistoriqueFournisseur =  new HistoriqueFournisseur();
  private selection = new SelectionModel<HistoriqueFournisseurElement>(true, []);
  private historiqueFournisseurFilter: HistoriqueFournisseurFilter = new HistoriqueFournisseurFilter();
  

  private columnsToSelect : SelectItem[];
   private  selectedColumnsDefinitions: string[];
  private selectedColumnsNames: string[];
  
@Output()
select: EventEmitter<HistoriqueFournisseur[]> = new EventEmitter<HistoriqueFournisseur[]>();


 selectedHistoriqueFournisseur: HistoriqueFournisseur ;
 private   showFilter = false;
 // private   showFilterFournisseur = false;
  private   showFilterFournisseur = false;
 
 private showSelectColumnsMultiSelect = false;
 




 @Input()
 private listHistoriquesFournisseurs: any = [] ;
 private checkedAll: false;

 private  dataHistoriquesFournisseursSource: MatTableDataSource<HistoriqueFournisseurElement>;
 @ViewChild(MatPaginator) paginator: MatPaginator; 
 @ViewChild(MatSort) sort: MatSort;
 private defaultColumnsDefinitions: ColumnDefinition[] = [];
 private columnsDefinitionsToDisplay :ColumnDefinition[] = [];
 private columnsDefinitions: ColumnDefinition[] = [];
 private columnsTitles: string[] = [];
 private columnsTitlesAr: string[] = [];
 private columnsNames: string[] = [];
  constructor( private route: ActivatedRoute, private historiquesFournisseursService: HistoriquesFournisseursService,
    private router: Router, private matSnackBar: MatSnackBar,
  private  dialog: MatDialog ) {
  }
  ngOnInit() {
    this.columnsToSelect = [
      {label: 'N° Historique', value: 'numeroHistoriqueFournisseur', title: 'N° Historique'},
      {label: 'Date Ventes', value: 'dateVentes', title: 'Date Ventes'},
      {label: 'Date Règlement', value: 'dateReglement', title: 'Date Règlement'},
      {label: 'Montant Mis En Valeur', value: 'montantMisEnValeur', title: 'Montant Mis En Valeur'},
      {label: 'Infos Complémentaires', value: 'additionalInfos', title: 'Infos Complémentaires'},
      {label: 'Rs.Soc.Fournisseur', value:  'raisonSocialeFournisseur',  title: 'Rs.Soc.Fournisseur' },
      {label: 'CIN Fournisseur', value:  'cinFournisseur', title: 'CIN Fournisseur' },
      {label: 'Nom Fournisseur', value:  'nomFournisseur', title: 'Nom Fournisseur' },
      {label: 'Prénom Fournisseur', value:  'prenomFournisseur', title: 'Prénom Fournisseur' },
      {label: 'Déscription', value:  'description', title: 'Déscription' }
  
  ];
  this.selectedColumnsDefinitions = [
    'numeroHistoriqueFournisseur',
   'dateVentes' ,
   'montantMisEnValeur' ,
   'cinFournisseur'
    ];
    this.historiquesFournisseursService.getPageHistoriquesFournisseurs(0, 5, null).
    subscribe((regCredits) => {
      this.listHistoriquesFournisseurs = regCredits;
  
      this.defaultColumnsDefinitions = [
        {name: 'numeroHistoriqueFournisseur' , displayTitle: 'N° Historique'},
        {name: 'dateVentes' , displayTitle: 'Date Ventes'},
         {name: 'dateReglement' , displayTitle: 'Date Règlement'},
         {name: 'montantMisEnValeur' , displayTitle: 'Montant Mis en Valeur'},
        {name: 'cinFournisseur' , displayTitle: 'CIN Fournisseur'},
        {name: 'nomFournisseur' , displayTitle: 'Nom Fournisseur'},
        {name: 'prenomFournisseur' , displayTitle: 'Prénom Fournisseur'},
        {name: 'raisonSocialeFournisseur' , displayTitle: 'Raison Soc.'},
         {name: 'additionalInfos' , displayTitle: 'Infos Complémentaires'},
         {name: 'description' , displayTitle: 'Déscription'}
         
        ];
    this.specifyListColumnsToBeDisplayed();
    this.transformDataToByConsumedByMatTable();
  });
  }
  deleteHistoriqueFournisseur(historiqueFournisseur) {
     //  console.log('call delete !', operationCourante );
    this.historiquesFournisseursService.deleteHistoriqueFournisseur(historiqueFournisseur.code).subscribe((response) => {
      if (response.error_message ===  undefined) {
        // this.save.emit(response);
        // this.fournisseur = new Fournisseur();
        console.log('call delete !', response);
        this.loadData(null);

      } else {
       // show error message
      }
     });

  }
loadData(filter) {
  this.historiquesFournisseursService.getPageHistoriquesFournisseurs(0, 5, filter).subscribe((historiquesFournisseurs) => {
    this.listHistoriquesFournisseurs= historiquesFournisseurs;
    this.transformDataToByConsumedByMatTable();
   });
}


checkOneActionInvoked() {
  const listSelectedHistoriquesFournisseurs: HistoriqueFournisseur[] = [];

if (this.selection.selected.length !== 0 ) {
const codes: string[] = this.selection.selected.map((regElement) => {
  return regElement.code;
});
this.listHistoriquesFournisseurs.forEach(RG => {
  if (codes.findIndex(code => code === RG.code) > -1) {
    listSelectedHistoriquesFournisseurs.push(RG);
  }
  });
}
this.select.emit(listSelectedHistoriquesFournisseurs);
}
transformDataToByConsumedByMatTable() {
  const listElements = [];
  if (this.listHistoriquesFournisseurs !== undefined) {
    this.listHistoriquesFournisseurs.forEach(histFournisseur => {
      listElements.push( {
        code: histFournisseur.code ,
        numeroHistoriqueFournisseur: histFournisseur.numeroHistoriqueFournisseur,
        cinFournisseur: histFournisseur.fournisseur.cin,
        nomFournisseur: histFournisseur.fournisseur.nom ,
        prenomFournisseur: histFournisseur.fournisseur.prenom ,
        raisonSocialeFournisseur: histFournisseur.fournisseur.raisonSociale ,
       montantMisEnValeur: histFournisseur.montantMisEnValeur ,
       dateVentes: histFournisseur.dateVentes ,
       dateReglement: histFournisseur.dateReglement ,
       additionalInfos: histFournisseur.additionalInfos ,
    
       description: histFournisseur.description
       
      } );
    });
  }
    this.dataHistoriquesFournisseursSource = new MatTableDataSource<HistoriqueFournisseurElement>(listElements);
    this.dataHistoriquesFournisseursSource.sort = this.sort;
    this.dataHistoriquesFournisseursSource.paginator = this.paginator;
    this.historiquesFournisseursService.getTotalHistoriquesFournisseurs(this.historiqueFournisseurFilter).subscribe((response) => {
    //   console.log('Total Releves de Ventes  is ', response);
    if (this.dataHistoriquesFournisseursSource.paginator) {
       this.dataHistoriquesFournisseursSource.paginator.length = response['totalHistoriquesFournisseurs'];
  
        }    });
}

specifyListColumnsToBeDisplayed() {
  
   console.log('selected colums definitions are :', this.selectedColumnsDefinitions);
   this.columnsDefinitionsToDisplay = this.selectedColumnsDefinitions === undefined
 ?
    this.defaultColumnsDefinitions : [];
    this.selectedColumnsNames = [];
    
    if (this.selectedColumnsDefinitions !== undefined)
     {
       this.selectedColumnsDefinitions.forEach((colName) => { 
            const title =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitle;
            const titleAR =     this.defaultColumnsDefinitions.find(( columDef) => (columDef.name === colName )).displayTitleAr;
            const colDef = {name: colName, displayTitle: title, displayTitleAr: titleAR};
            this.columnsDefinitionsToDisplay.push(colDef);
           });
     }
    this.columnsDefinitionsToDisplay=   this.columnsDefinitionsToDisplay.filter((colDef)=> (colDef !== undefined));
   this.columnsTitles =  this.columnsDefinitionsToDisplay .map((colDef) => {
              return colDef.displayTitle;
    });
    console.log('columns to disp:', this.columnsDefinitionsToDisplay);
    this.columnsNames = [];
    this.columnsNames[0] = 'select';
    this.columnsDefinitionsToDisplay .map((colDef) => {
     this.columnsNames.push(colDef.name);
    if (this.selectedColumnsDefinitions !== undefined) {
     this.selectedColumnsNames.push(colDef.displayTitle);
    }
 });
    this.columnsTitlesAr =  this.columnsDefinitionsToDisplay .map((colDef) => {
     return colDef.displayTitleAr;
 });
 }

masterToggle() {
  console.log('get selected');
  this.isAllSelected() ?
      this.selection.clear() :
      this.dataHistoriquesFournisseursSource.data.forEach(row => this.selection.select(row));
      this.checkOneActionInvoked();
}
isAllSelected() {
  const numSelected = this.selection.selected.length;
  const numRows = this.dataHistoriquesFournisseursSource.data.length;
  return numSelected === numRows;
}
applyFilter(filterValue: string) {
  this.dataHistoriquesFournisseursSource.filter = filterValue.trim().toLowerCase();
  if (this.dataHistoriquesFournisseursSource.paginator) {
    this.dataHistoriquesFournisseursSource.paginator.firstPage();
  }
}

loadAddHistoriqueFournisseurComponent() {
  this.router.navigate(['/pms/suivie/ajouterHistoriqueFournisseur']) ;

}
loadEditHistoriqueFournisseurComponent() {
  this.router.navigate(['/pms/suivie/editerHistoriqueFournisseur', this.selection.selected[ this.selection.selected.length - 1].code]) ;

}
supprimerHistoriquesFournisseurs()
{

  if ( this.selection.selected.length !== 0) {
      this.selection.selected.forEach(function(regCredit, index) {

   this.historiquesFournisseursService.deleteHistoriqueFournisseur(regCredit.code).subscribe((response) => {

          });
          if (this.selection.selected.length - 1 === index) {
            this.openSnackBar( this.selection.selected.length + ' Historique(s) Fournisseur(s) Supprimé(s)');
            this.selection.selected = [];
            this.loadData(null);
            }
      }, this);
    } else {
      this.openSnackBar('Aucun Historique Séléctionnée !');
    }
}
refresh() {
  this.loadData(null);
}
openSnackBar(messageToDisplay) {
  this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
      verticalPosition: 'top'});
 }

 history()
 {
   
 }
 /*addFournisseur()
 {
  const dialogConfig = new MatDialogConfig();
  
  dialogConfig.disableClose = false;
  dialogConfig.autoFocus = true;
  dialogConfig.position = {
  };

  const dialogRef = this.dialog.open(DialogAddFournisseurComponent,
    dialogConfig
  );
 }*/

 toggleFilterPanel()
 {
    this.showFilter = !this.showFilter;
 }
 toggleShowColumnsMultiSelect()
 {
   this.showSelectColumnsMultiSelect = ! this.showSelectColumnsMultiSelect ;
 }
 loadFiltredData()
 {
  //  console.log('the filter is ', this.operationCouranteFilter);
   this.loadData(this.historiqueFournisseurFilter);
 }
 attacherDocuments()
 {

 }
 changePage($event) {
  console.log('page event is ', $event);
 this.historiquesFournisseursService.getPageHistoriquesFournisseurs($event.pageIndex, $event.pageSize,
  this.historiqueFournisseurFilter ).subscribe((histsFournisseurs) => {
    this.listHistoriquesFournisseurs = histsFournisseurs;
   const listElements = [];
   this.listHistoriquesFournisseurs.forEach(histFournisseur => {
    listElements.push( {
      code: histFournisseur.code ,
      numeroHistoriqueFournisseur: histFournisseur.numeroHistoriqueFournisseur,
      cinFournisseur: histFournisseur.fournisseur.cin,
      nomFournisseur: histFournisseur.fournisseur.nom ,
      prenomFournisseur: histFournisseur.fournisseur.prenom ,
      raisonSocialeFournisseur: histFournisseur.fournisseur.raisonSociale ,
     montantMisEnValeur: histFournisseur.montantMisEnValeur ,
     dateVentes: histFournisseur.dateVentes ,
     dateReglement: histFournisseur.dateReglement ,
     additionalInfos: histFournisseur.additionInfos ,
  
     description: histFournisseur.description
      
  });
});
     this.dataHistoriquesFournisseursSource= new MatTableDataSource<HistoriqueFournisseurElement>(listElements);

     
     this.historiquesFournisseursService.getTotalHistoriquesFournisseurs(this.historiqueFournisseurFilter)
     .subscribe((response) => {
      console.log('Total OPer Courantes  is ', response);
      if (this.dataHistoriquesFournisseursSource.paginator) {
       this.dataHistoriquesFournisseursSource.paginator.length = response['totalHistoriquesFournisseurs'];
      }
     });

    });
}


activateFilterFournisseur()
{
  this.showFilterFournisseur = !this.showFilterFournisseur ;
  
}

}
