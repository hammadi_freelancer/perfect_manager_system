import { Component, OnInit, AfterContentChecked, Input, Output, EventEmitter, ElementRef ,

  ViewChild } from '@angular/core';
// import { ArticleService } from './article.service';
import {HistoriqueFournisseur} from './historique-fournisseur.model';
// import { ParametresVentes } from '../parametres-ventes.model';

import {HistoriquesFournisseursService} from './historiques-fournisseurs.service';

// import {VentesService} from '../ventes.service';
import {MatPaginator,MatSort, MatTableDataSource} from '@angular/material';

import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
// import {ActionData} from './action-data.interface';
// import { FournisseurService } from '../ventes/fournisseurs/fournisseur.service';
import { FournisseurService } from '../achats/fournisseurs/fournisseur.service';

// import { FournisseurFilter } from '../ventes/fournisseurs/fournisseur.filter';
import { FournisseurFilter } from '../achats/fournisseurs/fournisseur.filter';
import { Fournisseur } from '../achats/fournisseurs/fournisseur.model';

// import { ArticleFilter } from '../stocks/articles/article.filter';

// import {Fournisseur} from '../ventes/fournisseurs/fournisseur.model';


import { ColumnDefinition } from '../common/column-definition.interface';

import {MatSnackBar} from '@angular/material';
import { MatExpansionPanel } from '@angular/material';
import {ModePayementLibelle} from './mode-payement-libelle.interface';
import {SelectionModel} from '@angular/cdk/collections';

type LibelleModePayement = 'Credit'| 'Comptant' | 'Cheque' ;

@Component({
  selector: 'app-historique-fournisseur-new',
  templateUrl: './historique-fournisseur-new.component.html',
  // styleUrls: ['./players.component.css']
})
export class HistoriqueFournisseurNewComponent implements OnInit {

 private historiqueFournisseur: HistoriqueFournisseur =  new HistoriqueFournisseur();


private saveEnCours = false;



private fournisseurFilter: FournisseurFilter;

private listFournisseurs: Fournisseur[];



private etatsHistoriquesFournisseurs: any[] = [
  {value: 'Initial', viewValue: 'Initiale'},
  {value: 'Valide', viewValue: 'Validée'},
  {value: 'Annule', viewValue: 'Annulée'},
  // {value: 'Regle', viewValue: 'Reglée'},
  // {value: 'NonRegle', viewValue: 'Non Reglée'},
 //  {value: 'PartiellementRegle', viewValue: 'Partiellement Reglée'},
];






  constructor( private route: ActivatedRoute,
    private router: Router, private historiquesFournisseursService: HistoriquesFournisseursService,
     private fournisseurService: FournisseurService,
     
   private matSnackBar: MatSnackBar, private elRef: ElementRef) {
  }


  ngOnInit() {
   this.listFournisseurs = this.route.snapshot.data.fournisseurs;
    this.fournisseurFilter = new FournisseurFilter(this.listFournisseurs);
    

  }
  saveHistoriqueFournisseur() {

    this.saveEnCours = true;
    this.historiquesFournisseursService.addHistoriqueFournisseur(this.historiqueFournisseur).subscribe((response) => {
      this.saveEnCours = false;
      
        if (response.error_message ===  undefined) {
          console.log('undefined error message');
          // this.save.emit(response);
          this.historiqueFournisseur = new  HistoriqueFournisseur();
          this.openSnackBar(  'Historique Fournisseur Enregistré');
        } else {
         // show error message
         this.openSnackBar(  'Erreurs:Opération Echouée');
         
        }
    });
  }
  fillDataMatriculeRaisonFournisseur() {
    const listFiltred  = this.listFournisseurs.filter((fournisseur) =>
     (fournisseur.registreCommerce === this.historiqueFournisseur.fournisseur.registreCommerce));
    this.historiqueFournisseur.fournisseur.matriculeFiscale = listFiltred[0].matriculeFiscale;
    this.historiqueFournisseur.fournisseur.raisonSociale = listFiltred[0].raisonSociale;
  }
  fillDataMatriculeRegistreCommercialF()
  {
    const listFiltred  = this.listFournisseurs.filter((fournisseur) => (fournisseur.raisonSociale === 
      this.historiqueFournisseur.fournisseur.raisonSociale));
    this.historiqueFournisseur.fournisseur.matriculeFiscale = listFiltred[0].matriculeFiscale;
    this.historiqueFournisseur.fournisseur.registreCommerce = listFiltred[0].registreCommerce;
  }
  fillDataRegistreCommercialRaisonF()
  {
    const listFiltred  = this.listFournisseurs.filter((fournisseur) => 
    (fournisseur.matriculeFiscale === this.historiqueFournisseur.fournisseur.matriculeFiscale));
    this.historiqueFournisseur.fournisseur.raisonSociale = listFiltred[0].raisonSociale;
    this.historiqueFournisseur.fournisseur.registreCommerce = listFiltred[0].registreCommerce;
  }
  fillDataNomPrenomFournisseur() {
    const listFiltred  = this.listFournisseurs.filter((fournisseur) =>
     (fournisseur.cin === this.historiqueFournisseur.fournisseur.cin));
    this.historiqueFournisseur.fournisseur.nom = listFiltred[0].nom;
    this.historiqueFournisseur.fournisseur.prenom = listFiltred[0].prenom;
  }
  fillDataCINPrenomFournisseur()
  {
    const listFiltred  = this.listFournisseurs.filter((fournisseur) => 
    (fournisseur.nom === this.historiqueFournisseur.fournisseur.nom));
    this.historiqueFournisseur.fournisseur.cin = listFiltred[0].cin;
    this.historiqueFournisseur.fournisseur.prenom = listFiltred[0].prenom;
  }
  fillDataCINNomFournisseur()
  {
    const listFiltred  = this.listFournisseurs.filter((fournisseur) =>
     (fournisseur.prenom === this.historiqueFournisseur.fournisseur.prenom));
    const num =   Math.floor(Math.random() * Math.floor(listFiltred.length));
    this.historiqueFournisseur.fournisseur.cin = listFiltred[num].cin;
    this.historiqueFournisseur.fournisseur.nom= listFiltred[num].nom;
  }
  vider() {
    this.historiqueFournisseur =  new HistoriqueFournisseur();
  }
  openSnackBar(messageToDisplay) {
   this.matSnackBar.open(messageToDisplay, '', {duration: 5000,
       verticalPosition: 'top', panelClass: 'snackBarClass'});
  }
loadGridHistoriquesFournisseurs() {
  this.router.navigate(['/pms/suivie/historiquesFournisseurs']) ;
}
}
