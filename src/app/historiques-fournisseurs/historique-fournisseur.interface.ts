export interface HistoriqueFournisseurElement {
         code: string;
         numeroHistoriqueFournisseur: string;
         dateVentes: string;
         dateReglement: string;
         montantMisEnValeur: number;
         cinFournisseur: string;
         nomFournisseur: string;
          prenomFournisseur: string;
         raisonSocialeFournisseur: string;
         additionalInfos : string;
         etat: string;
         description: string;
        }
