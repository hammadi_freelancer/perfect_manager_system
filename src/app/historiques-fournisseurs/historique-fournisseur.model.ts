
import { BaseEntity } from '../common/base-entity.model' ;
// import { Tranche } from './tranche.model' ;
import { Fournisseur } from '../achats/fournisseurs/fournisseur.model' ;

import { PayementData } from './payement-data.model';
// import { LigneReleveVente } from './ligne-releve-vente.model';
// import { Credit } from '../credits/credit.model';

// type  TypeOperationCourante= 'Achats' |'Ventes' |'' ;

export class  HistoriqueFournisseur extends BaseEntity {
    constructor(
        public code?: string,
        public numeroHistoriqueFournisseur?: string,
        public dateVentesObject?: Date,
        public dateReglementObject?: Date,
        public montantMisEnValeur?: number,
        public additionalInfos?: string,
        public etat?: string,
        public fournisseur?: Fournisseur,
        public description?: string
     ) {
         super();
         this.fournisseur = new Fournisseur();
         this.montantMisEnValeur = Number(0);
         
        //  this.listLignesRelVentes = [] ;
         // this.categoryClient = 'PERSONNE_PHYSIQUE';
    }
}
