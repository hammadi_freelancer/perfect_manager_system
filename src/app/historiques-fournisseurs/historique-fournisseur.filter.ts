

export class HistoriqueFournisseurFilter {
    code: string;
    numeroHistoriqueFournisseur: string;
    dateVentes: string;
    dateReglement: string;
    montantMisEnValeur: number;
    cinFournisseur: string;
    nomFournisseur: string;
     prenomFournisseur: string;
    raisonSocialeFournisseur: string;
    additionInfos : string;
    etat: string;
    description: string;
   }
