
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var documentsInventaires = require('../model/documents-inventaires').DocumentsInventaires;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addDocumentInventaire',function(req,res,next){
    
       console.log(" ADD DOCUMENT INVENTAIRE   IS CALLED") ;
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = documentsInventaires.addDocumentInventaire(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,documentInventaireAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_DOCUMENT_INVENTAIRE',
                error_content: err
            });
         }else{
       
        return res.json(documentInventaireAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateDocumentInventaire',function(req,res,next){
    
      // console.log(" UPDATE DOCUMENT CREDIT  IS CALLED") ;
       console.log(" THE DOCUMENT INVENTAIRE   TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result =documentsInventaires.updateDocumentInventaire(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,documentInventaireUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_DOCUMENT_INVENTAIRE',
                error_content: err
            });
         }else{
       
        return res.json(documentInventaireUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getDocumentsInventaires',function(req,res,next){
 
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    documentsInventaires.getListDocumentsInventaires(
        req.app.locals.dataBase,decoded.userData.code, function(err,listDocumentsInventaires){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_INVENTAIRES',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsInventaires);
          }
      });

  });
  router.get('/getDocumentInventaire/:codeDocumentInventaire',function(req,res,next){
    console.log("GET DOCUMENT INVENTAIRE   IS CALLED");
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    documentsInventaires.getDocumentInventaire(
        req.app.locals.dataBase,req.params['codeDocumentInventaire'],decoded.userData.code,
     function(err,documentlo){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_DOCUMENT_INVENTAIRE',
               error_content: err
           });
        }else{
      
       return res.json(documentlo);
        }
    });
})

  router.delete('/deleteDocumentInventaire/:codeDocumentInventaire',function(req,res,next){
    
       console.log(" DELETE DOCUMENT INVENTAIRE   IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = documentsInventaires.deleteDocumentInventaire(
        req.app.locals.dataBase,req.params['codeDocumentInventaire'],decoded.userData.code,
        function(err,codeDocumentInventaire){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_DOCUMENT_INVENTAIRE',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeDocumentInventaire']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getDocumentsInventairesByCodeInventaire/:codeInventaire',function(req,res,next){
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
    documentsInventaires.getListDocumentsInventairesByCodeInventaire(
        req.app.locals.dataBase,decoded.userData.code,req.params['codeInventaire'],
         function(err,listDocumentsInventaires){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_INVENTAIRES',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsInventaires);
          }
      });

  });
  module.exports.routerDocumentsInventaires = router;