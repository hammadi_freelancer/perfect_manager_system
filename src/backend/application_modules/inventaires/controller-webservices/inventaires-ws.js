
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var inventaires = require('../model/inventaires').Inventaires;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addInventaire',function(req,res,next){
    
    console.log(" ADD INVENTAIRE IS CALLED") ;
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = inventaires.addInventaire(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,inventaireAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_INVENTAIRE',
                error_content: err
            });
         }else{
       
        return res.json(inventaireAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateInventaire',function(req,res,next){
    
       console.log(" UPDATE INVENTAIRE IS CALLED") ;
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = inventaires.updateInventaire(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,inventaireUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_INVENTAIRE',
                error_content: err
            });
         }else{
       
        return res.json(inventaireUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getInventaires',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
      console.log("GET LIST LIVRES ORES  IS CALLED");
    inventaires.getListInventaires(
        req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
        req.query.filter,function(err,listInventaires){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_INVENTAIRES',
                 error_content: err
             });
          }else{
        
         return res.json(listInventaires);
          }
      });

  });
  router.get('/getInventaire/:codeInventaire',function(req,res,next){
    console.log("GET INVENTAIRE IS CALLED");
  //  console.log(req.params['codeCredit']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    inventaires.getInventaire(
        req.app.locals.dataBase,req.params['codeInventaire'],decoded.userData.code, function(err,inventaire){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_INVENTAIRE',
               error_content: err
           });
        }else{
      
       return res.json(inventaire);
        }
    });
})

  router.delete('/deleteInventaire/:codeInventaire',function(req,res,next){
    
       console.log(" DELETE INVENTAIRE IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = inventaires.deleteInventaire(
        req.app.locals.dataBase,req.params['codeInventaire'],decoded.userData.code, function(err,codeInventaire){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_INVENTAIRE',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeInventaire']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });



 router.get('/getTotalInventaires',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    inventaires.getTotalInventaires(
        req.app.locals.dataBase,decoded.userData.code,function(err,totalInventaires){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_INVENTAIRES',
                 error_content: err
             });
          }else{
        
         return res.json({'totalInventaires':totalInventaires});
          }
      });

  });
  module.exports.routerInventaires = router;