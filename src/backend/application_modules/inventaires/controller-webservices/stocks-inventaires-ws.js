
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var stocksInventaires = require('../model/stocks-inventaires').StocksInventaires;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addStockInventaire',function(req,res,next){
    
    console.log(" ADD STOCK INVENTAIRE  IS CALLED") ;
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = stocksInventaires.addStockInventaire(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,stockInvAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_STOCK_INVENTAIRE',
                error_content: err
            });
         }else{
       
        return res.json(stockInvAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateStockInventaire',function(req,res,next){
    
       console.log(" UPDATE STOCK INVENTAIRE   IS CALLED") ;
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = stocksInventaires.updateStockInventaire(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,stockUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_STOCK_INVENTAIRE',
                error_content: err
            });
         }else{
       
        return res.json(stockUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getStocksInventaires',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
      console.log("GET LIST STOCKS INVENTAIRES  IS CALLED");
    stocksInventaires.getListStocksInventaires(
        req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
        req.query.filter,function(err,listStocksInventaires){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_STOCKS_INVENTAIRES',
                 error_content: err
             });
          }else{
        
         return res.json(listStocksInventaires);
          }
      });

  });
  router.get('/getStockInventaire/:codeStockInventaire',function(req,res,next){
    console.log("GET STOCK INVENTAIRE   IS CALLED");
  //  console.log(req.params['codeCredit']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    stocksInventaires.getStockInventaire(
        req.app.locals.dataBase,req.params['codeStockInventaire'],decoded.userData.code, function(err,stockInv){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_STOCK_INVENTAIRE',
               error_content: err
           });
        }else{
      
       return res.json(stockInv);
        }
    });
})

  router.delete('/deleteStockInventaire/:codeStockInventaire',function(req,res,next){
    
       console.log(" DELETE STOCK INVENTAIRE   IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = stocksInventaires.deleteStockInventaire(
        req.app.locals.dataBase,req.params['codeStockInventaire'],decoded.userData.code, function(err,codeStockInventaire){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_STOCK_INVENTAIRE',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeStockInventaire']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });



 router.get('/getTotalStocksInventaires',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    stocksInventaires.getTotalStocksInventaires(
        req.app.locals.dataBase,decoded.userData.code,function(err,totalStocksInventaires){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_STOCKS_INVENTAIRES',
                 error_content: err
             });
          }else{
        
         return res.json({'totalStocksInventaires':totalStocksInventaires});
          }
      });

  });
  module.exports.routerStocksInventaires = router;