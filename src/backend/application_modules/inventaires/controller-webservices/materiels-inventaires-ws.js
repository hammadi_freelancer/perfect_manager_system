
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var materielsInventaires = require('../model/materiels-inventaires').MaterielsInventaires;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addMaterielInventaire',function(req,res,next){
    
    console.log(" ADD MATERIEL INVENTAIRE IS CALLED") ;
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = materielsInventaires.addMaterielInventaire(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,matrAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_MATERIEL_INVENTAIRE',
                error_content: err
            });
         }else{
       
        return res.json(matrAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateMaterielInventaire',function(req,res,next){
    
       console.log(" UPDATE MATERIEL INVENTAIRE IS CALLED") ;
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = materielsInventaires.updateMaterielInventaire(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,matUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_MATERIEL_INVENTAIRE',
                error_content: err
            });
         }else{
       
        return res.json(matUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getMaterielsInventaires',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
      console.log("GET LIST LIVRES ORES  IS CALLED");
    materielsInventaires.getListMaterielsInventaires(
        req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
        req.query.filter,function(err,listMaterielsInventaires){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_MATERIELS_INVENTAIRES',
                 error_content: err
             });
          }else{
        
         return res.json(listMaterielsInventaires);
          }
      });

  });
  router.get('/getMaterielInventaire/:codeMaterielInventaire',function(req,res,next){
    console.log("GET MATERIEL INVENTAIRE IS CALLED");
  //  console.log(req.params['codeCredit']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    materielsInventaires.getMaterielInventaire(
        req.app.locals.dataBase,req.params['codeMaterielInventaire'],decoded.userData.code, function(err,livreOre){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_MATERIEL_INVENTAIRE',
               error_content: err
           });
        }else{
      
       return res.json(livreOre);
        }
    });
})

  router.delete('/deleteMaterielInventaire/:codeMaterielInventaire',function(req,res,next){
    
       console.log(" DELETE MATERIEL INVENTAIRE IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = materielsInventaires.deleteMaterielInventaire(
        req.app.locals.dataBase,req.params['codeMaterielInventaire'],decoded.userData.code, function(err,codeMaterielInventaire){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_MATERIEL_INVENTAIRE',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeMaterielInventaire']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });



 router.get('/getTotalMaterielsInventaires',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    materielsInventaires.getTotalMaterielsInventaires(
        req.app.locals.dataBase,decoded.userData.code,function(err,totalMaterielsInventaires){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_MATERIELS_INVENTAIRES',
                 error_content: err
             });
          }else{
        
         return res.json({'totalMaterielsInventaires':totalMaterielsInventaires});
          }
      });

  });
  module.exports.routerMaterielsInventaires = router;