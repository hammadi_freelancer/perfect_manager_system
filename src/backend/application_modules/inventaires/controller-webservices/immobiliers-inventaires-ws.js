
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var immobiliersInventaires= require('../model/immobiliers-inventaires').ImmobiliersInventaires;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addImmobilierInventaire',function(req,res,next){
    
       console.log(" ADD  IMMOBILIER INVENTAIRE  IS CALLED") ;
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = immobiliersInventaires.addImmobilierInventaire(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,immobilierInventaireAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_IMMOBILIER_INVENTAIRE',
                error_content: err
            });
         }else{
       
        return res.json(immobilierInventaireAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateImmobilierInventaire',function(req,res,next){
    
      // console.log(" UPDATE DOCUMENT CREDIT  IS CALLED") ;
       console.log(" THE  IMMOBILIER INVENTAIRE   TO UPDATE IS :",req.body);
    
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result =immobiliersInventaires.updateImmobilierInventaire(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,immobilierInvUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_IMMOBILIER_INVENTAIRE',
                error_content: err
            });
         }else{
       
        return res.json(immobilierInvUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getImmobiliersInventaires',function(req,res,next){
 
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
    immobiliersInventaires.getListImmobiliersInventaires(decoded.userData.code, function(err,listImmobiliersInventaires){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_IMMOBILIERS_INVENTAIRES',
                 error_content: err
             });
          }else{
        
         return res.json(listImmobiliersInventaires);
          }
      });

  });
  router.get('/getImmobilierInventaire/:codeImmobilierInventaire',function(req,res,next){
    console.log("GET  IMMOBILIER INVENTAIRE   IS CALLED");
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    immobiliersInventaires.getImmobilierInventaire(req.params['codeImmobilierInventaire'],decoded.userData.code,
     function(err,immobilierInventaire){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_IMMOBILIER_INVENTAIRE ',
               error_content: err
           });
        }else{
      
       return res.json(immobilierInventaire);
        }
    });
})

  router.delete('/deleteImmobilierInventaire/:codeImmobilierInventaire',function(req,res,next){
    
       console.log(" DELETE LIGNE IMMOBILIER INVENTAIRE   IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = immobiliersInventaires.deleteImmobilierInventaire(req.params['codeImmobilierInventaire'],decoded.userData.code,
        function(err,codeImmobilierInventaire){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_IMMOBILIER_INVENTAIRE',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeImmobilierInventaire']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getImmobiliersInventairesByCodeInventaire/:codeInventaire',function(req,res,next){
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
    immobiliersInventaires.getListImmobiliersInventairesByCodeInventaire(decoded.userData.code,req.params['codeInventaire'],
         function(err,listImmobiliersInventaires){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_IMMOBILIERS_INVENTAIRES_BY_CODE_INVENTAIRE',
                 error_content: err
             });
          }else{
        
         return res.json(listImmobiliersInventaires);
          }
      });

  });
  module.exports.routerImmobiliersInventaires = router;