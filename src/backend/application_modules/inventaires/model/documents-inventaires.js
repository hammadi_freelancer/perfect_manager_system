//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var DocumentsInventaires= {
addDocumentInventaire : function (db,documentInventaire,codeUser,callback){
    if(documentInventaire != undefined){
        documentInventaire.code  = utils.isUndefined(documentInventaire.code)?shortid.generate():documentInventaire.code;
        documentInventaire.dateReceptionObject = utils.isUndefined(documentInventaire.dateReceptionObject)? 
        new Date():documentInventaire.dateReceptionObject;
        if(utils.isUndefined(documentInventaire.numeroDocument))
            {
               const currentD =  moment(new Date());
              // const genereKey = shortid.generate();
              const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.minutes().toString()+
              currentD.secondes().toString();
              documentInventaire.numeroDocument = 'DOCINV' +currentD.day().toLocaleString()+ generatedCode;
            }
            documentInventaire.userCreatorCode = codeUser;
            documentInventaire.userLastUpdatorCode = codeUser;
            documentInventaire.dateCreation = moment(new Date).format('L');
            documentInventaire.dateLastUpdate = moment(new Date).format('L');
            db.collection('DocumentInventaire').insertOne(
                documentInventaire,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,documentInventaire);
                   }
                }
              ) ;
             // db.close();
             
   

}else{
callback(true,null);
}
},
updateDocumentInventaire: function (db,documentInventaire,codeUser, callback){

   
    documentInventaire.dateReceptionObject = utils.isUndefined(documentInventaire.dateReceptionObject)? 
    new Date():documentInventaire.dateReceptionObject;

    const criteria = { "code" : documentInventaire.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"dateReceptionObject" : documentInventaire.dateReceptionObject, 
    "codeInventaire" : documentInventaire.codeInventaire, 
    "description" : documentInventaire.description,
     "codeOrganisation" : documentInventaire.codeOrganisation,
     "typeDocument" : documentInventaire.typeDocument, // cheque, fiche paie, fiche electricié , fiche eau potabe, fiche téléphonique, autres
     "numeroDocument" : documentInventaire.numeroDocument, 
     "imageFace1":documentInventaire.imageFace1,
     "imageFace2" : documentInventaire.imageFace2 , 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": documentInventaire.dateLastUpdate, 
    } ;
     
            db.collection('DocumentInventaire').updateOne( criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                     callback(true,null);
                    }else{
 
                     callback(false,result);
                    }
                 }
            ) ;
    
    
    },
getListDocumentsInventaires : function (db,codeUser,callback)
{
    let listDocumentsInventaires = [];

   var cursor =  db.collection('DocumentInventaire').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(docInventaire,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        docInventaire.dateReception = moment(docInventaire.dateReceptionObject).format('L');
        
        listDocumentsInventaires.push(docInventaire);
   }).then(function(){
    // console.log('list documents credits',listDocumentsCredits);
    callback(null,listDocumentsInventaires); 
   });
   

     //return [];
},
deleteDocumentInventaire: function (db,codeDocumentInventaire,codeUser,callback){
    const criteria = { "code" : codeDocumentInventaire, "userCreatorCode":codeUser };
        
            db.collection('DocumentInventaire').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                     callback(true,null);
                    }else{
 
                     callback(false,result);
                    }
                 }
            ) ;
    
},

getDocumentInventaire: function (db,codeDocumentInventaire,codeUser,callback)
{
    const criteria = { "code" : codeDocumentInventaire, "userCreatorCode":codeUser };
 
       const docLivOre =  db.collection('DocumentInventaire').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            });
                  
},


getListDocumentsInventairesByCodeInventaire : function (db,codeUser,codeInventaire,callback)
{
    let listDocumentsInventaires = [];

   var cursor =  db.collection('DocumentInventaire').find( 
       {"userCreatorCode" : codeUser, "codeInventaire": codeInventaire},
       
    );
   cursor.forEach(function(documentInventaire,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        documentInventaire.dateReception = moment(documentInventaire.dateReceptionObject).format('L');
        
        listDocumentsInventaires.push(documentInventaire);
   }).then(function(){
   // console.log('list credits',listCredits);
    callback(null,listDocumentsInventaires); 
   });
   
}

     //return [];



}
module.exports.DocumentsInventaires = DocumentsInventaires;