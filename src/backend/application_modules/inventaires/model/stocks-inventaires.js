//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var StocksInventaires = {
addStockInventaire : function (db,stockInventaire,codeUser,callback){
    if(stockInventaire != undefined){
        stockInventaire.code  = utils.isUndefined(stockInventaire.code)?shortid.generate():stockInventaire.code;
        if(utils.isUndefined(stockInventaire.numeroStockInventaire))
            {
               const currentD =  moment(new Date());
              // const genereKey = shortid.generate();
              const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.minutes().toString()+
              currentD.secondes().toString();
              stockInventaire.numeroStockInventaire = 'STKINV' +currentD.day().toLocaleString()+ generatedCode;
            }
       
            stockInventaire.userCreatorCode = codeUser;
            stockInventaire.userLastUpdatorCode = codeUser;
            stockInventaire.dateCreation = moment(new Date).format('L');
            stockInventaire.dateLastUpdate = moment(new Date).format('L');
            db.collection('StockInventaire').insertOne(
                stockInventaire,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
             // db.close();
             
    

}else{
callback(true,null);
}
},
updateStockInventaire: function (db,stockInventaire,codeUser, callback){

   
    stockInventaire.dateLastUpdate = moment(new Date).format('L');
    
    stockInventaire.dateStockInventaireObject = utils.isUndefined(stockInventaire.dateStockInventaireObject)? 
    new Date():stockInventaire.dateStockInventaireObject;
    if(utils.isUndefined(stockInventaire.numeroStockInventaire))
        {
           const currentD =  moment(new Date());
          // const genereKey = shortid.generate();
           const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.minutes().toString() ;
           stockInventaire.numeroStockInventaire = 'STKINV' +currentD.day().toLocaleString()+ generatedCode;
        }
   
 

    const criteria = { "code" : stockInventaire.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
   //  "dateStockInventaireObject" : stockInventaire.dateStockInventaireObject,  
    "stockInitial" : stockInventaire.stockInitial, 
    "stockFinal" : stockInventaire.stockFinal ,
     "article" : stockInventaire.article, 
     "description" : stockInventaire.description,
      "etat": stockInventaire.etat,// ouvert/initial/en cours de validation /validé /en cours de validation /cloturé
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": stockInventaire.dateLastUpdate, 
    } ;
            db.collection('StockInventaire').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },function(stockInventaire,err){
                    if(err)
                    callback(false,err);
                    else
                        callback(true,true);
                    
                    
                }
            ) ;
     
    
    },
    
getListStocksInventaires : function (db,codeUser,pageNumber, nbElementsPerPage , filter,callback)
{
    let listStocksInventaires = [];
   
  //console.log('filter recieved is :',filter);
  let filterObject = JSON.parse(filter);
 
   let filterData = {
       "userCreatorCode" : codeUser
   };
   const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
   
   var cursor =  db.collection('StockInventaire').find( 
    conditionBuilt
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(stockInventaire,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
       // stockInventaire.dateStockInventaire= moment(stockInventaire.dateStockInventaireObject).format('L');
       // stockInventaire.periodeFrom= moment(stockInventaire.periodeFromObject).format('L');
       //  stockInventaire.periodeTo= moment(stockInventaire.periodeToObject).format('L');
       listStocksInventaires.push(stockInventaire);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listStocksInventaires); 
   });
     //return [];
},
deleteStockInventaire: function (db,codeStockInventaire,codeUser,callback){
    const criteria = { "code" : codeStockInventaire, "userCreatorCode":codeUser };
    db.collection('StockInventaire').deleteOne(
        criteria , function(err,result){
            if(err){
                
                callback(null,null);
            }else{
                callback(false,result);
            }
        }
    ) ;
},

getStockInventaire: function (db,codeStockInventaire,codeUser,callback)
{
    const criteria = { "code" : codeStockInventaire, "userCreatorCode":codeUser };
  
       const stockInventaire=  db.collection('StockInventaire').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
        // callback(false,client);
                 
},
getTotalStocksInventaires : function (db,codeUser,filter, callback)
{
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
   var totalStocksInventaires =  db.collection('StockInventaire').find( 
    conditionBuilt
).count(function(err,result){
        callback(null,result); 
    }
);
   


     //return [];
},
getPageStocksInventaires : function (db,codeUser,pageNumber, nbElementsPerPage ,filter, callback)
{
   //  console.log('filter OperationCourantes',filter);
    let listStocksInventaires = [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
      console.log('condition',conditionBuilt);
      
   var cursor =  db.collection('StockInventaire').find( conditionBuilt
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(stockInventaire,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        // stockInventaire.dateStockInventaire= moment(stockInventaire.dateStockInventaireObject).format('L');
       //  stockInventaire.periodeFrom= moment(stockInventaire.periodeFromObject).format('L');
       //  stockInventaire.periodeTo= moment(stockInventaire.periodeToObject).format('L');
        
        listStocksInventaires.push(stockInventaire);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listStocksInventaires); 
   });
     //return [];
},
buildFilterCondition(filterObject,filterData)
{
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.numeroStockInventaire))
        {
                filterData["numeroStockInventaire"] = filterObject.numeroStockInventaire;
        }
         
        if(!utils.isUndefined(filterObject.etat))
          {
                 
                    filterData["etat"] = filterObject.etat;
                    
         }
         if(!utils.isUndefined(filterObject.stockInitial))
             {
                    
                       filterData["stockInitial"] = filterObject.stockInitial ;
                       
            }
            if(!utils.isUndefined(filterObject.montantAchats))
                {
                       
                          filterData["stockFinal"] = filterObject.stockFinal ;
                          
               }
               if(!utils.isUndefined(filterObject.libelleArticle))
                {
                       
                          filterData["article.libelle"] = filterObject.libelleArticle ;
                          
               }
    
               if(!utils.isUndefined(filterObject.codeArticle))
                {
                       
                          filterData["article.code"] = filterObject.codeArticle ;
                          
               }
    
               if(!utils.isUndefined(filterObject.designationArticle))
                {
                       
                          filterData["article.designation"] = filterObject.designationArticle
                           ;
                          
               }
       
     
        }
        return filterData;
},
getListStocksInventairesByCodeInventaire : function (db,codeUser,codeInventaire,callback)
{
    let listStocksInventaires= [];

   var cursor =  db.collection('StockInventaire').find( 
       {"userCreatorCode" : codeUser, "codeInventaire": codeInventaire},
       
    );
   cursor.forEach(function(stockInventaire,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
     
        listStocksInventaires.push(stockInventaire);
   }).then(function(){
   // console.log('list credits',listCredits);
    callback(null,listStocksInventaires); 
   });
   
},

}
module.exports.StocksInventaires = StocksInventaires;