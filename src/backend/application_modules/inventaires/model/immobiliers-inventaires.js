//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var ImmobiliersInventaires = {
addImmobilierInventaire : function (db,immobilierInventaire,codeUser,callback){
    if(immobilierInventaire != undefined){
        immobilierInventaire.code  = utils.isUndefined(immobilierInventaire.code)?shortid.generate():immobilierInventaire.code;

        immobilierInventaire.dateAchatsObject = utils.isUndefined(immobilierInventaire.dateAchatsObject)? 
        new Date():immobilierInventaire.dateAchatsObject;

        if(utils.isUndefined(immobilierInventaire.numeroImmobilierInventaire))
            {
               const currentD =  moment(new Date());
              // const genereKey = shortid.generate();
              const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.minutes().toString()+
              currentD.secondes().toString();
              immobilierInventaire.numeroImmobilierInventaire = 'IMMINV' +currentD.day().toLocaleString()+ generatedCode;
            }
       
            immobilierInventaire.userCreatorCode = codeUser;
            immobilierInventaire.userLastUpdatorCode = codeUser;
            immobilierInventaire.dateCreation = moment(new Date).format('L');
            immobilierInventaire.dateLastUpdate = moment(new Date).format('L');
            db.collection('ImmobilierInventaire').insertOne(
                immobilierInventaire,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
             // db.close();
             
    

}else{
callback(true,null);
}
},
updateImmobilierInventaire: function (db,immobilierInventaire,codeUser, callback){

   
    immobilierInventaire.dateLastUpdate = moment(new Date).format('L');
    
    immobilierInventaire.dateAchatsObject = utils.isUndefined(immobilierInventaire.dateAchatsObject)? 
    new Date():immobilierInventaire.dateAchatsObject;
    if(utils.isUndefined(immobilierInventaire.numeroImmobilierInventaire))
        {
           const currentD =  moment(new Date());
          // const genereKey = shortid.generate();
           const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.minutes().toString() ;
           immobilierInventaire.numeroImmobilierInventaire = 'IMMINV' +currentD.day().toLocaleString()+ generatedCode;
        }
   
 

    const criteria = { "code" : immobilierInventaire.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
    "dateAchatsObject" : immobilierInventaire.dateAchatsObject,  
    "cout" : immobilierInventaire.cout, 
    "dateDebutExploitationObject" : immobilierInventaire.dateDebutExploitationObject ,
     "adresse" : immobilierInventaire.adresse, 
     "superficie":immobilierInventaire.superficie,
     "description" : immobilierInventaire.description,
      "etat": immobilierInventaire.etat,// ouvert/initial/en cours de validation /validé /en cours de validation /cloturé
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": immobilierInventaire.dateLastUpdate, 
    } ;
            db.collection('ImmobilierInventaire').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },function(immobilierInventaire,err){
                    if(err)
                    callback(false,err);
                    else
                        callback(true,true);
                    
                    
                }
            ) ;
     
    
    },
    
getListImmobiliersInventaires : function (db,codeUser, filter,callback)
{
    let listImmobiliersInventaires = [];
   
  //console.log('filter recieved is :',filter);
  let filterObject = JSON.parse(filter);
 
   let filterData = {
       "userCreatorCode" : codeUser
   };
   const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
   
   var cursor =  db.collection('ImmobilierInventaire').find( 
    conditionBuilt
    );
   cursor.forEach(function(immobilierInventaire,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        immobilierInventaire.dateAchats= moment(immobilierInventaire.dateAchatsObject).format('L');
        immobilierInventaire.dateDebutExploitation= moment(immobilierInventaire.dateDebutExploitationObject).format('L');
       listImmobiliersInventaires.push(immobilierInventaire);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listImmobiliersInventaires); 
   });
     //return [];
},
deleteImmobilierInventaire: function (db,codeImmobilierInventaire,codeUser,callback){
    const criteria = { "code" : codeImmobilierInventaire, "userCreatorCode":codeUser };
    db.collection('ImmobilierInventaire').deleteOne(
        criteria , function(err,result){
            if(err){
                
                callback(null,null);
            }else{
                callback(false,result);
            }
        }
    ) ;
},

getImmobilierInventaire: function (db,codeImmobilierInventaire,codeUser,callback)
{
    const criteria = { "code" : codeImmobilierInventaire, "userCreatorCode":codeUser };
  
       const immobilierInventaire=  db.collection('ImmobilierInventaire').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
        // callback(false,client);
                 
},
getTotalImmobiliersInventaires : function (db,codeUser,filter, callback)
{
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
   var totalImmobiliersInventaires =  db.collection('ImmobilierInventaire').find( 
    conditionBuilt
).count(function(err,result){
        callback(null,result); 
    }
);
   


     //return [];
},
getListImmobiliersInventairesByCodeInventaire : function (db,codeUser,codeInventaire,callback)
{
    let listImmobiliersInventaires = [];

   var cursor =  db.collection('ImmobilierInventaire').find( 
       {"userCreatorCode" : codeUser, "codeInventaire": codeInventaire},
       
    );
   cursor.forEach(function(immobilierInventaire,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        immobilierInventaire.dateAchats = moment(immobilierInventaire.dateAchatsObject).format('L');
        immobilierInventaire.dateDebutExploitation = moment(immobilierInventaire.dateDebutExploitationObject).format('L');
        listImmobiliersInventaires.push(immobilierInventaire);
   }).then(function(){
   // console.log('list credits',listCredits);
    callback(null,listImmobiliersInventaires); 
   });
   
},
getPageImmobiliersInventaires : function (db,codeUser,pageNumber, nbElementsPerPage ,filter, callback)
{
   //  console.log('filter OperationCourantes',filter);
    let listImmobiliersInventaires = [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
      console.log('condition',conditionBuilt);
      
   var cursor =  db.collection('ImmobilierInventaire').find( conditionBuilt
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(immobilierInventaire,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        immobilierInventaire.dateAchats= moment(immobilierInventaire.dateAchatsObject).format('L');
        immobilierInventaire.dateDebutExploitation= moment(immobilierInventaire.dateDebutExploitationObject).format('L');
        listImmobiliersInventaires.push(immobilierInventaire);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listImmobiliersInventaires); 
   });
     //return [];
},
buildFilterCondition(filterObject,filterData)
{
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.numeroImmobilierInventaire))
        {
                filterData["numeroImmobilierInventaire"] = filterObject.numeroImmobilierInventaire;
        }
         
        if(!utils.isUndefined(filterObject.etat))
          {
                 
                    filterData["etat"] = filterObject.etat;
                    
         }
         if(!utils.isUndefined(filterObject.cout))
             {
                    
                       filterData["cout"] = filterObject.cout ;
                       
            }
            if(!utils.isUndefined(filterObject.adresse))
                {
                       
                          filterData["adresse"] = filterObject.adresse ;
                          
               }
               if(!utils.isUndefined(filterObject.superficie))
                {
                       
                          filterData["superficie"] = filterObject.superficie ;
                          
               }
        }
        return filterData;
}

}
module.exports.ImmobiliersInventaires = ImmobiliersInventaires;