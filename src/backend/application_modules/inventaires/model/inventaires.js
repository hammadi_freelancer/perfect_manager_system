//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var Inventaires = {
addInventaire: function (db,inventaire,codeUser,callback){
    if(inventaire != undefined){
        inventaire.code  = utils.isUndefined(inventaire.code)?shortid.generate():inventaire.code;

        inventaire.dateInventaireObject = utils.isUndefined(inventaire.dateInventaireObject)? 
        new Date():inventaire.dateInventaireObject;

        if(utils.isUndefined(inventaire.numeroInventaire))
            {
               const currentD =  moment(new Date());
              // const genereKey = shortid.generate();
              const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.minutes().toString()+
              currentD.secondes().toString();
              inventaire.numeroInventaire = 'INVT' +currentD.day().toLocaleString()+ generatedCode;
            }
       
            inventaire.userCreatorCode = codeUser;
            inventaire.userLastUpdatorCode = codeUser;
            inventaire.dateCreation = moment(new Date).format('L');
            inventaire.dateLastUpdate = moment(new Date).format('L');
            db.collection('Inventaire').insertOne(
                inventaire,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
             // db.close();
             
    

}else{
callback(true,null);
}
},
updateInventaire: function (db,inventaire,codeUser, callback){

   
    inventaire.dateLastUpdate = moment(new Date).format('L');
    
    inventaire.dateInventaireObject = utils.isUndefined(inventaire.dateInventaireObject)? 
    new Date():inventaire.dateInventaireObject;
    if(utils.isUndefined(inventaire.numeroInventaire))
        {
           const currentD =  moment(new Date());
          // const genereKey = shortid.generate();
           const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.minutes().toString() ;
           inventaire.numeroInventaire = 'INVT' +currentD.day().toLocaleString()+ generatedCode;
        }
   
 

    const criteria = { "code" : inventaire.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
    "dateInventaireObject" : inventaire.dateInventaireObject,  
    "montantVentes" : inventaire.montantVentes, 
    "montantAchats" : inventaire.montantAchats ,
     "montantRH" : inventaire.montantRH, 
     "montantCharges":inventaire.montantCharges,
     "description" : inventaire.description,
      "etat": inventaire.etat,// ouvert/initial/en cours de validation /validé /en cours de validation /cloturé
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": inventaire.dateLastUpdate, 
    } ;
            db.collection('Inventaire').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },function(inventaire,err){
                    if(err)
                    callback(false,err);
                    else
                        callback(true,true);
                    
                    
                }
            ) ;
     
    
    },
    
getListInventaires : function (db,codeUser,pageNumber, nbElementsPerPage , filter,callback)
{
    let listInventaires = [];
   
  //console.log('filter recieved is :',filter);
  let filterObject = JSON.parse(filter);
 
   let filterData = {
       "userCreatorCode" : codeUser
   };
   const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
   
   var cursor =  db.collection('Inventaire').find( 
    conditionBuilt
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(inventaire,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        inventaire.dateInventaire= moment(inventaire.dateInventaireObject).format('L');
        inventaire.periodeFrom= moment(inventaire.periodeFromObject).format('L');
        inventaire.periodeTo= moment(inventaire.periodeToObject).format('L');
       listInventaires.push(inventaire);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listInventaires); 
   });
     //return [];
},
deleteInventaire: function (db,codeInventaire,codeUser,callback){
    const criteria = { "code" : codeInventaire, "userCreatorCode":codeUser };
    db.collection('Inventaire').deleteOne(
        criteria , function(err,result){
            if(err){
                
                callback(null,null);
            }else{
                callback(false,result);
            }
        }
    ) ;
},

getInventaire: function (db,codeInventaire,codeUser,callback)
{
    const criteria = { "code" : codeInventaire, "userCreatorCode":codeUser };
  
       const inventaire=  db.collection('Inventaire').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
        // callback(false,client);
                 
},
getTotalInventaires : function (db,codeUser,filter, callback)
{
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
   var totalInventaires =  db.collection('Inventaire').find( 
    conditionBuilt
).count(function(err,result){
        callback(null,result); 
    }
);
   


     //return [];
},
getPageInventaires : function (db,codeUser,pageNumber, nbElementsPerPage ,filter, callback)
{
   //  console.log('filter OperationCourantes',filter);
    let listInventaires = [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
      console.log('condition',conditionBuilt);
      
   var cursor =  db.collection('Inventaire').find( conditionBuilt
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(inventaire,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        inventaire.dateInventaire= moment(inventaire.dateInventaireObject).format('L');
        inventaire.periodeFrom= moment(inventaire.periodeFromObject).format('L');
        inventaire.periodeTo= moment(inventaire.periodeToObject).format('L');
        
        listInventaires.push(inventaire);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listInventaires); 
   });
     //return [];
},
buildFilterCondition(filterObject,filterData)
{
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.numeroInventaire))
        {
                filterData["numeroInventaire"] = filterObject.numeroInventaire;
        }
         
        if(!utils.isUndefined(filterObject.etat))
          {
                 
                    filterData["etat"] = filterObject.etat;
                    
         }
         if(!utils.isUndefined(filterObject.montantVentes))
             {
                    
                       filterData["montantVentes"] = filterObject.montantVentes ;
                       
            }
            if(!utils.isUndefined(filterObject.montantAchats))
                {
                       
                          filterData["montantAchats"] = filterObject.montantAchats ;
                          
               }
               if(!utils.isUndefined(filterObject.montantCharges))
                {
                       
                          filterData["montantCharges"] = filterObject.montantCharges ;
                          
               }
            if(!utils.isUndefined(filterObject.dateInventaireToObject))
             {
                    
                       filterData["dateInventaireObject"] = {$lt: filterObject.dateInventaireToObject};
                       
            }
   
       
     
        }
        return filterData;
}

}
module.exports.Inventaires = Inventaires;