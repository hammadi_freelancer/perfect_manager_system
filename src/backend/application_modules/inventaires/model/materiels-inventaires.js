//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var MaterielsInventaires= {
addMatrielInventaire : function (db,materielInventaire,codeUser,callback){
    if(materielInventaire != undefined){
        materielInventaire.code  = utils.isUndefined(materielInventaire.code)?shortid.generate():materielInventaire.code;
        if(utils.isUndefined(materielInventaire.numeroMaterielInventaire))
            {
               const currentD =  moment(new Date());
              // const genereKey = shortid.generate();
              const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.minutes().toString()+
              currentD.secondes().toString();
              materielInventaire.numeroMaterielInventaire = 'MATINV' +currentD.day().toLocaleString()+ generatedCode;
            }
            materielInventaire.userCreatorCode = codeUser;
            materielInventaire.userLastUpdatorCode = codeUser;
            materielInventaire.dateCreation = moment(new Date).format('L');
            materielInventaire.dateLastUpdate = moment(new Date).format('L');
            db.collection('MatrielInventaire ').insertOne(
                materielInventaire,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,materielInventaire);
                   }
                }
              ) ;
             // db.close();
             
      

}else{
callback(true,null);
}
},
updateMaterielInventaire: function (db,materielInventaire,codeUser, callback){
    const criteria = { "code" : materielInventaire.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
    "code" : materielInventaire.code, 
    "numeroInventaire" : materielInventaire.numeroInventaire, 
    "dateAchatsObject" : materielInventaire.dateAchatsObject, 
    "description" : materielInventaire.description,
     "codeOrganisation" : materielInventaire.codeOrganisation,
     "cout" : materielInventaire.cout, 
     "libelle" : materielInventaire.libelle, 
     "etat":materielInventaire.etat,
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": materielInventaire.dateLastUpdate, 
    } ;
            db.collection('MaterielInventaire').updateOne(
                criteria,  { 
                     $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                     callback(true,null);
                    }else{
                     callback(false,materielInventaire);
                    }
                 }

            ) ;
      
    
    },
getListMaterielsInventaires: function (db,codeUser,callback)
{
    let listMaterielsInventaires = [];
    
   var cursor =  db.collection('MaterielInventaire').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(materielInventaire,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        
        listMaterielsInventaires.push(materielInventaire);
   }).then(function(){
    // console.log('list documents credits',listDocumentsCredits);
    callback(null,listMaterielsInventaires); 
   });

},

getListMaterielsInventairesByCodeInventaire : function (db,codeUser,codeInventaire,callback)
{
    let listMaterielsInventaires = [];

   var cursor =  db.collection('MaterielInventaire').find( 
       {"userCreatorCode" : codeUser, "codeInventaire": codeInventaire},
       
    );
   cursor.forEach(function(materielInventaire,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        materielInventaire.dateAchats = moment(materielInventaire.dateAchatsObject).format('L');
        
        listMaterielsInventaires.push(materielInventaire);
   }).then(function(){
   // console.log('list credits',listCredits);
    callback(null,listMaterielsInventaires); 
   });
   
},
deleteMaterielInventaire: function (db,codeMaterielInventaire,codeUser,callback){
    const criteria = { "code" : codeMaterielInventaire, "userCreatorCode":codeUser };
      
        
            db.collection('MaterielInventaire').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                     callback(true,null);
                    }else{
 
                        callback(false,codeMaterielInventaire);                    }
                 }
            ) ;
},

getMaterielInventaire: function (db,codeMaterielInventaire,codeUser,callback)
{
    const criteria = { "code" : codeMaterielInventaire, "userCreatorCode":codeUser };
    
       const matInV =  db.collection('MaterielInventaire').findOne(
            criteria , function(err,result){
                if(err){
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
          
},



}
module.exports.MaterielsInventaires = MaterielsInventaires;