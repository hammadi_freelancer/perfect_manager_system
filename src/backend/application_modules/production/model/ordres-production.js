//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');
//we need to have article as a json object : {code :'mmm', design :'ddd'}
var OrdresProduction = {
addOrdreProduction : function (db,ordreProduction, codeUser,callback){
    if(ordreProduction != undefined){

     ordreProduction.code  = utils.isUndefined(ordreProduction.code)?shortid.generate():ordreProduction.code;

     ordreProduction.userCreatorCode = codeUser;
     ordreProduction.userLastUpdatorCode = codeUser;
     ordreProduction.dateCreation = moment(new Date).format('L');
     ordreProduction.dateLastUpdate = moment(new Date).format('L');

     ordreProduction.dateDebutProductionProgrammeObject = utils.isUndefined(ordreProduction.dateDebutProductionProgrammeObject)? 
     new Date():ordreProduction.dateDebutProductionProgrammeObject;


     ordreProduction.dateFinProductionProgrammeObject = utils.isUndefined(ordreProduction.dateFinProductionProgrammeObject)? 
     new Date():ordreProduction.dateFinProductionProgrammeObject;

     ordreProduction.dateDebutProductionEffectifObject = utils.isUndefined(ordreProduction.dateDebutProductionEffectifObject)? 
     new Date():ordreProduction.dateDebutProductionEffectifObject;


     ordreProduction.dateFinProductionEffectifObject = utils.isUndefined(ordreProduction.dateFinProductionEffectifObject)? 
     new Date():ordreProduction.dateFinProductionEffectifObject;

     if(utils.isUndefined(ordreProduction.numeroOrdreProduction))
   {
     const currentD =  moment(new Date());
          // const genereKey = shortid.generate();
     const generatedCode = currentD.year().toString() +'/'+currentD.utc(true).month().toString()+currentD.minutes().toString() ;
     ordreProduction.numeroOrdreProduction = 'ORDPROD' + generatedCode;

    }
   //   ordreProduction.dateExpiration = utils.isUndefined(ordreProduction.dateExpiration)? 
    //  ordreProduction.dateExpiration : moment(ordreProduction.dateExpiration ).format('L');
 
        db.collection('OrdreProduction').insertOne(
            ordreProduction,
            function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            } 
          ) ;
         // db.close();
       //  callback(false,article);
    }

//return false;
},
updateOrdreProduction: function (db,ordreProduction,codeUser, callback){
    
    const criteria = { "code" : ordreProduction.code,"userCreatorCode" : codeUser };
    



    ordreProduction.dateDebutProductionProgrammeObject = utils.isUndefined(ordreProduction.dateDebutProductionProgrammeObject)? 
    new Date():ordreProduction.dateDebutProductionProgrammeObject;


    ordreProduction.dateFinProductionProgrammeObject = utils.isUndefined(ordreProduction.dateFinProductionProgrammeObject)? 
    new Date():ordreProduction.dateFinProductionProgrammeObject;

    ordreProduction.dateDebutProductionEffectifObject = utils.isUndefined(ordreProduction.dateDebutProductionEffectifObject)? 
    new Date():ordreProduction.dateDebutProductionEffectifObject;


    ordreProduction.dateFinProductionEffectifObject = utils.isUndefined(ordreProduction.dateFinProductionEffectifObject)? 
    new Date():ordreProduction.dateFinProductionEffectifObject;


    const dataToUpdate = {
        "dateDebutProductionEffectifObject" : ordreProduction.dateDebutProductionEffectifObject,
        "dateFinProductionEffectifObject" : ordreProduction.dateFinProductionEffectifObject,
        "dateFinProductionProgrammeObject" : ordreProduction.dateFinProductionProgrammeObject,
        "dateDebutProductionProgrammeObject" : ordreProduction.dateDebutProductionProgrammeObject,
        
        "dureeHoursEffectif" : ordreProduction.dureeHoursEffectif,
        "dureeHoursProgrammee" : ordreProduction.dureeHoursProgrammee,
        "articleProduit" : ordreProduction.articleProduit,

        "qteProduitIntacteEffectif" : ordreProduction.qteProduitIntacteEffectif,
        "qteProduitDefailleEffectif" : ordreProduction.qteProduitDefailleEffectif,
        "qteProduitIntacteProgramme" : ordreProduction.qteProduitIntacteProgramme,
        "qteProduitDefailleProgramme" : ordreProduction.qteProduitDefailleProgramme,
        "unMesure" : ordreProduction.unMesure,
        "description" : ordreProduction.description,
        "conditionsStockages" : ordreProduction.conditionsStockages,
        "responsable" : ordreProduction.responsable,
        "depotStockage" : ordreProduction.conditionsStockages,
        "cout" : ordreProduction.cout,
        "etat" : ordreProduction.etat
        
} ;
     
            db.collection('OrdreProduction').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }    
            ) ;
     
    
    },
    getListOrdresProduction : function (db,codeUser, callback)
    {
        let listOrdresProduction = [];
      
       var cursor =  db.collection('OrdreProduction').find(
        {"userCreatorCode" : codeUser}
       );
       cursor.forEach(function(ordreProduction,err){
           if(err)
            {
                console.log('errors produced :', err);
    
            }
            ordreProduction.dateDebutProductionEffectif= moment(ordreProduction.dateDebutProductionEffectifObject).format('L');
            ordreProduction.dateFinProductionEffectif = moment(ordreProduction.dateFinProductionEffectifObject).format('L');
            ordreProduction.dateDebutProductionProgramme= moment(ordreProduction.dateDebutProductionProgrammeObject).format('L');
            ordreProduction.dateFinProductionProgramme = moment(ordreProduction.dateFinProductionProgrammeObject).format('L');
            // article.dateExpiration = moment(article.dateExpirationObject).format('L');
            // article.dateFabrication = moment(article.dateFabricationObject).format('L');
         
                        listOrdresProduction.push(ordreProduction);
       }).then(function(){
       //  console.log('list clients',listClients);
        callback(null,listOrdresProduction); 
       });
       
   
         //return [];
    },

deleteOrdreProduction: function (db,codeOrdreProduction,codeUser,callback){
    const criteria = { "code" : codeOrdreProduction, "userCreatorCode":codeUser };
    
      
            db.collection('OrdreProduction').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                } 
            ) ;
    
},
getPageOrdresProduction : function (db,codeUser,pageNumber,nbElementsPerPage, filter,callback)
{
    let listOrdresProduction = [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var cursor =  db.collection('OrdreProduction').find(
      condition
   ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
   .limit(Number(nbElementsPerPage) );
   
   cursor.forEach(function(ordreProduction,err){
       if(err)
        {
            console.log('errors produced :', err);

        }
        ordreProduction.dateDebutProductionEffectif= moment(ordreProduction.dateDebutProductionEffectifObject).format('L');
        ordreProduction.dateFinProductionEffectif = moment(ordreProduction.dateFinProductionEffectifObject).format('L');
        ordreProduction.dateDebutProductionProgramme= moment(ordreProduction.dateDebutProductionProgrammeObject).format('L');
        ordreProduction.dateFinProductionProgramme = moment(ordreProduction.dateFinProductionProgrammeObject).format('L');
   
      
        listOrdresProduction.push(ordreProduction);
   }).then(function(){
   //  console.log('list clients',listClients);
    callback(null,listOrdresProduction); 
   });
   


},
getTotalOrdresProduction: function (db,codeUser,filter, callback)
{
   
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var totalOrdresProduction=  db.collection('OrdreProduction').find( 
     condition
    ).count(function(err,result){
        callback(null,result); 
    });

   

},


getOrdreProduction: function (db,codeOrdreProduction,codeUser,callback)
{
   // const criteria = { "code" : codeArticle };
    const criteria = { "code" : codeOrdreProduction, "userCreatorCode":codeUser };
    
 
       const ordreProduction =  db.collection('OrdreProduction').findOne(
            criteria , function(err,result){
                if(err){
                    callback(null,null);
                }else{
                    callback(false,result);
                }
            }
        ) ;
                   
},

buildFilterCondition(filterObject,filterData)
{
  
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.code))
        {
                filterData["numeroOrdreProduction"] = filterObject.numeroOrdreProduction;
        }
         
     
         if(!utils.isUndefined(filterObject.articleCode))
             {
                    
                       filterData["articleProduit.code"] =filterObject.articleCode;
                       
            }
           

                   if(!utils.isUndefined(filterObject.etat))
                    {
                           
                              filterData["etat"] = filterObject.etat ;
                              
                   }



        }
        return filterData;
},

};

module.exports.OrdresProduction = OrdresProduction;