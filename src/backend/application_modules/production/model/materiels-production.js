//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');
//we need to have article as a json object : {code :'mmm', design :'ddd'}
var MaterielsProduction = {
addMaterielProduction : function (db,materielProduction, codeUser,callback){
    if(materielProduction != undefined){
        materielProduction.code  = utils.isUndefined(materielProduction.code)?shortid.generate():materielProduction.code;
        materielProduction.userCreatorCode = codeUser;
        materielProduction.userLastUpdatorCode = codeUser;
        materielProduction.dateCreation = moment(new Date).format('L');
        materielProduction.dateLastUpdate = moment(new Date).format('L');

        materielProduction.dateAchatsObject = utils.isUndefined(materielProduction.dateAchatsObject)? 
        new Date():materielProduction.dateAchatsObject;
    
        materielProduction.dateFabricationObject = utils.isUndefined(materielProduction.dateFabricationObject)? 
        new Date():materielProduction.dateFabricationObject;
       

     if(utils.isUndefined(materielProduction.numeroMaterielProduction))
   {
     const currentD =  moment(new Date());
          // const genereKey = shortid.generate();
     const generatedCode = currentD.year().toString() +'/'+currentD.utc(true).month().toString()+currentD.minutes().toString() ;
     materielProduction.numeroMaterielProduction = 'MATPROD' + generatedCode;

    }
   //   releveProduction.dateExpiration = utils.isUndefined(releveProduction.dateExpiration)? 
    //  releveProduction.dateExpiration : moment(releveProduction.dateExpiration ).format('L');
 
        db.collection('MaterielProduction').insertOne(
            materielProduction,
            function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            } 
          ) ;
         // db.close();
       //  callback(false,article);
    }

//return false;
},
updateMaterielProduction: function (db,materielProduction,codeUser, callback){
    
    const criteria = { "code" : materielProduction.code,"userCreatorCode" : codeUser };
    
    materielProduction.dateAchatsObject = utils.isUndefined(materielProduction.dateAchatsObject)? 
    new Date():materielProduction.dateAchatsObject;

    materielProduction.dateFabricationObject = utils.isUndefined(materielProduction.dateFabricationObject)? 
    new Date():materielProduction.dateFabricationObject;

    const dataToUpdate = {
        "dateAchatsObject" : materielProduction.dateAchatsObject,
        "dateFabricationObject" : materielProduction.dateFabricationObject,
        "matricule" : materielProduction.matricule,
        "reference" : materielProduction.reference,
        "marque" : materielProduction.marque,
        "description" : activiteProduction.description,
        "etat" : activiteProduction.etat
        
} ;
     
            db.collection('MaterielProduction').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }    
            ) ;
     
    
    },
    getListMaterielsProduction : function (db,codeUser, callback)
    {
        let listMaterielsProduction = [];
      
       var cursor =  db.collection('MaterielProduction').find(
        {"userCreatorCode" : codeUser}
       );
       cursor.forEach(function(materielProduction,err){
           if(err)
            {
                console.log('errors produced :', err);
    
            }
            materielProduction.dateAchats = moment(materielProduction.dateAchatsObject).format('L');
            materielProduction.dateFabrication = moment(materielProduction.dateFabricationObject).format('L');
            // article.dateExpiration = moment(article.dateExpirationObject).format('L');
            // article.dateFabrication = moment(article.dateFabricationObject).format('L');
         
            listMaterielsProduction.push(materielProduction);
       }).then(function(){
       //  console.log('list clients',listClients);
        callback(null,listMaterielsProduction); 
       });
       
   
         //return [];
    },

deleteMaterielProduction: function (db,codeMaterielProduction,codeUser,callback){
    const criteria = { "code" : codeMaterielProduction, "userCreatorCode":codeUser };
    
      
            db.collection('MaterielProduction').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                } 
            ) ;
    
},
getPageMaterielsProduction : function (db,codeUser,pageNumber,nbElementsPerPage, filter,callback)
{
    let listMaterielsProduction = [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var cursor =  db.collection('MaterielProduction').find(
      condition
   ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
   .limit(Number(nbElementsPerPage) );
   
   cursor.forEach(function(materielProduction,err){
       if(err)
        {
            console.log('errors produced :', err);

        }
        materielProduction.dateAchats = moment(materielProduction.dateAchatsObject).format('L');
        materielProduction.dateFabrication = moment(materielProduction.dateFabricationObject).format('L');
   
      
        listMaterielsProduction.push(materielProduction);
   }).then(function(){
   //  console.log('list clients',listClients);
    callback(null,listMaterielsProduction); 
   });
   


},
getTotalMaterielsProduction: function (db,codeUser,filter, callback)
{
   
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var totalMaterielsProduction=  db.collection('MaterielProduction').find( 
     condition
    ).count(function(err,result){
        callback(null,result); 
    });

   

},


getMaterielProduction: function (db,codeMaterielProduction,codeUser,callback)
{
   // const criteria = { "code" : codeArticle };
    const criteria = { "code" : codeMaterielProduction, "userCreatorCode":codeUser };
    
 
       const materielProduction =  db.collection('MaterielProduction').findOne(
            criteria , function(err,result){
                if(err){
                    callback(null,null);
                }else{
                    callback(false,result);
                }
            }
        ) ;
                   
},

buildFilterCondition(filterObject,filterData)
{
  
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.code))
        {
                filterData["numeroMaterielProduction"] = filterObject.numeroMaterielProduction;
        }
         
        if(!utils.isUndefined(filterObject.marque))
          {
                 
                    filterData["marque"] = filterObject.marque;
                    
         }
         if(!utils.isUndefined(filterObject.matricule))
             {
                    
                       filterData["matricule"] =filterObject.matricule;
                       
            }
                   if(!utils.isUndefined(filterObject.etat))
                    {
                           
                              filterData["etat"] = filterObject.etat ;
                              
                   }



        }
        return filterData;
},

};

module.exports.MaterielsProduction = MaterielsProduction;