//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var PaiementsRelevesProduction = {
addPaiementReleveProduction : function (db,paiementReleveProduction,codeUser,callback){
    if(paiementReleveProduction != undefined){
        paiementReleveProduction.code  = utils.isUndefined(paiementReleveProduction.code)?shortid.generate():paiementReleveProduction.code;
        paiementReleveProduction.dateOperationObject = utils.isUndefined(paiementReleveProduction.dateOperationObject)? 
        new Date():paiementReleveProduction.dateOperationObject;
        paiementReleveProduction.userCreatorCode = codeUser;
        paiementReleveProduction.userLastUpdatorCode = codeUser;
        paiementReleveProduction.dateCreation = moment(new Date).format('L');
        paiementReleveProduction.dateLastUpdate = moment(new Date).format('L');
        
     
            db.collection('PaiementReleveProduction').insertOne(
                paiementReleveProduction,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,paiementReleveProduction);
                   }
                }
              ) ;


}else{
callback(true,null);
}
},
updatePaiementReleveProduction: function (db,paiementReleveProduction,codeUser, callback){

   
    paiementReleveProduction.dateOperationObject = utils.isUndefined(paiementReleveProduction.dateOperationObject)? 
    new Date():paiementReleveProduction.dateOperationObject;
    paiementReleveProduction.dateLastUpdate = moment(new Date).format('L');
    
    const criteria = { "code" : paiementReleveProduction.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"montantPaye" : paiementReleveProduction.montantPaye, 
    "codeReleveProduction" : paiementReleveProduction.codeReleveProduction, 
    "description" : paiementReleveProduction.description,
     "codeOrganisation" : paiementReleveProduction.codeOrganisation,
    "dateOperationObject" : paiementReleveProduction.dateOperationObject ,
     "modePaiement" : paiementReleveProduction.modePaiement, // cheque , virement , espece 
     "numeroCheque" : paiementReleveProduction.numeroCheque, 
     "compte":paiementReleveProduction.compte,
     "compteAdversaire" : paiementReleveProduction.compteAdversaire , 
     "banque" : paiementReleveProduction.banque ,
      "banqueAdversaire" : paiementReleveProduction.banqueAdversaire, 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": paiementReleveProduction.dateLastUpdate, 
    } ;
       
            db.collection('PaiementReleveProduction').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
      
    
    },
getListPaiementsRelevesProduction : function (db,codeUser,callback)
{
    let listPaiementsRelevesProduction  = [];
    
   var cursor =  db.collection('PaiementReleveProduction').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(paiementReleveProduction,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        paiementReleveProduction.dateOperation = moment(paiementReleveProduction.dateOperationObject).format('L');
        
        listPaiementsRelevesProduction.push(paiementReleveProduction);
   }).then(function(){
  //  console.log('list credits',listPaiementsFacturesVentes);
    callback(null,listPaiementsRelevesProduction); 
   });
},
deletePaiementReleveProduction: function (db,codePaiementReleveProduction,codeUser,callback){
    const criteria = { "code" : codePaiementReleveProduction, "userCreatorCode":codeUser };
       
            db.collection('PaiementReleveProduction').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
   
},

getPaiementReleveProduction: function (db,codePaiementReleveProduction,codeUser,callback)
{
    const criteria = { "code" : codePaiementReleveProduction, "userCreatorCode":codeUser };
  
       const paiementReleveProduction =  db.collection('PaiementReleveProduction').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                     
},

getListPaiementsRelevesProductionByCodeReleve : function (db,codeReleve, codeUser,callback)
{
    let listPaiementsReleveProduction = [];
  
   var cursor =  db.collection('PaiementReleveProduction').find( 
       {"userCreatorCode" : codeUser, "codeReleveProduction": codeReleve}
    );
   cursor.forEach(function(paiementReleveProduction,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        paiementReleveProduction.dateOperation = moment(paiementReleveProduction.dateOperationObject).format('L');
        paiementReleveProduction.dateCheque = moment(paiementReleveProduction.dateChequeObject).format('L');
        
        listPaiementsReleveProduction.push(paiementReleveProduction);
   }).then(function(){
   // console.log('list credits',listCredits);
    callback(null,listPaiementsReleveProduction); 
   });

},
}
module.exports.PaiementsRelevesProduction = PaiementsRelevesProduction;