//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var DocumentsRelevesProduction = {
addDocumentReleveProduction : function (db,documentReleveProduction,codeUser,callback){
    if(documentReleveProduction != undefined){
        documentReleveProduction.code  = utils.isUndefined(documentReleveProduction.code)?shortid.generate():documentReleveProduction.code;
        documentReleveProduction.dateReceptionObject = utils.isUndefined(documentReleveProduction.dateReceptionObject)? 
        new Date():documentReleveProduction.dateReceptionObject;
        documentReleveProduction.userCreatorCode = codeUser;
        documentReleveProduction.userLastUpdatorCode = codeUser;
        documentReleveProduction.dateCreation = moment(new Date).format('L');
        documentReleveProduction.dateLastUpdate = moment(new Date).format('L');
    
            db.collection('DocumentReleveProduction').insertOne(
                documentReleveProduction,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,documentReleveProduction);
                   }
                }
              ) ;
   

}else{
callback(true,null);
}
},
updateDocumentReleveProduction: function (db,documentReleveProduction,codeUser, callback){

   
    documentReleveProduction.dateReceptionObject = utils.isUndefined(documentReleveProduction.dateReceptionObject)? 
    new Date():documentReleveProduction.dateReceptionObject;

    const criteria = { "code" : documentReleveProduction.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"dateReceptionObject" : documentReleveProduction.dateReceptionObject, 
    "codeReleveProduction" : documentReleveProduction.codeReleveProduction, 
    "description" : documentReleveProduction.description,
     "codeOrganisation" : documentReleveProduction.codeOrganisation,
     "typeDocument" : documentReleveProduction.typeDocument, // cheque , virement , espece 
     "numeroDocument" : documentReleveProduction.numeroDocument, 
     "imageFace1":documentReleveProduction.imageFace1,
     "imageFace2" : documentReleveProduction.imageFace2 , 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": documentReleveProduction.dateLastUpdate, 
    } ;
       
            db.collection('DocumentReleveProduction').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
    
    },
getListDocumentsRelevesProduction : function (db,codeUser,callback)
{
    let listDocumentsRelevesProduction = [];
   
   var cursor =  db.collection('DocumentReleveProduction').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(docRelProduction,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        docRelProduction.dateReception = moment(docRelProduction.dateReceptionObject).format('L');
        
        listDocumentsRelevesProduction.push(docRelProduction);
   }).then(function(){
    //console.log('list documents credits',listDocumentsCredits);
    callback(null,listDocumentsRelevesProduction); 
   });
   

},
deleteDocumentReleveProduction: function (db,codeDocumentReleveProduction,codeUser,callback){
    const criteria = { "code" : codeDocumentReleveProduction, "userCreatorCode":codeUser };
      
            db.collection('DocumentReleveProduction').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;

},

getDocumentReleveProduction: function (db,codeDocumentReleveProduction,codeUser,callback)
{
    const criteria = { "code" : codeDocumentReleveProduction, "userCreatorCode":codeUser };

       const doc =  db.collection('DocumentReleveProduction').findOne(
            criteria , function(err,result){
                if(err){
                   
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                  
},


getListDocumentsReleveProductionByCodeReleve : function (db,codeUser,codeReleveProduction,callback)
{
    let listDocumentsReleveProduction = [];
  
   var cursor =  db.collection('DocumentReleveProduction').find( 
       {"userCreatorCode" : codeUser, "codeReleveProduction": codeReleveProduction}
    );
   cursor.forEach(function(documentRV,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        documentRV.dateReception = moment(documentRV.dateReceptionObject).format('L');
        
        listDocumentsReleveProduction.push(documentRV);
   }).then(function(){
  // console.log('list doc credits',listCredits);
    callback(null,listDocumentsReleveProduction); 
   });

},


}
module.exports.DocumentsRelevesProduction = DocumentsRelevesProduction;