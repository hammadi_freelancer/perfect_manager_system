//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var DocumentsOrdresProduction = {
addDocumentOrdreProduction : function (db,documentOrdreProduction,codeUser,callback){
    if(documentOrdreProduction != undefined){
        documentOrdreProduction.code  = utils.isUndefined(documentOrdreProduction.code)?shortid.generate():documentOrdreProduction.code;
        documentOrdreProduction.dateReceptionObject = utils.isUndefined(documentOrdreProduction.dateReceptionObject)? 
        new Date():documentOrdreProduction.dateReceptionObject;
        documentOrdreProduction.userCreatorCode = codeUser;
        documentOrdreProduction.userLastUpdatorCode = codeUser;
        documentOrdreProduction.dateCreation = moment(new Date).format('L');
        documentOrdreProduction.dateLastUpdate = moment(new Date).format('L');
    
            db.collection('DocumentOrdreProduction').insertOne(
                documentOrdreProduction,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,documentOrdreProduction);
                   }
                }
              ) ;
   

}else{
callback(true,null);
}
},
updateDocumentOrdreProduction: function (db,documentOrdreProduction,codeUser, callback){

   
    documentOrdreProduction.dateReceptionObject = utils.isUndefined(documentOrdreProduction.dateReceptionObject)? 
    new Date():documentOrdreProduction.dateReceptionObject;

    const criteria = { "code" : documentOrdreProduction.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"dateReceptionObject" : documentOrdreProduction.dateReceptionObject, 
    "codeOrdreProduction" : documentOrdreProduction.codeOrdreProduction, 
    "description" : documentOrdreProduction.description,
     "codeOrganisation" : documentOrdreProduction.codeOrganisation,
     "typeDocument" : documentOrdreProduction.typeDocument, // cheque , virement , espece 
     "numeroDocument" : documentOrdreProduction.numeroDocument, 
     "imageFace1":documentOrdreProduction.imageFace1,
     "imageFace2" : documentOrdreProduction.imageFace2 , 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": documentOrdreProduction.dateLastUpdate, 
    } ;
       
            db.collection('DocumentOrdreProduction').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
    
    },
getListDocumentsOrdresProduction : function (db,codeUser,callback)
{
    let listDocumentsOrdresProduction = [];
   
   var cursor =  db.collection('DocumentOrdreProduction').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(docRelProduction,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        docRelProduction.dateReception = moment(docRelProduction.dateReceptionObject).format('L');
        
        listDocumentsOrdresProduction.push(docRelProduction);
   }).then(function(){
    //console.log('list documents credits',listDocumentsCredits);
    callback(null,listDocumentsOrdresProduction); 
   });
   

},
deleteDocumentOrdreProduction: function (db,codeDocumentOrdreProduction,codeUser,callback){
    const criteria = { "code" : codeDocumentOrdreProduction, "userCreatorCode":codeUser };
      
            db.collection('DocumentOrdreProduction').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;

},

getDocumentOrdreProduction: function (db,codeDocumentOrdreProduction,codeUser,callback)
{
    const criteria = { "code" : codeDocumentOrdreProduction, "userCreatorCode":codeUser };

       const doc =  db.collection('DocumentOrdreProduction').findOne(
            criteria , function(err,result){
                if(err){
                   
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                  
},


getListDocumentsOrdreProductionByCodeOrdre : function (db,codeUser,codeOrdreProduction,callback)
{
    let listDocumentsOrdreProduction = [];
  
   var cursor =  db.collection('DocumentOrdreProduction').find( 
       {"userCreatorCode" : codeUser, "codeOrdreProduction": codeOrdreProduction}
    );
   cursor.forEach(function(documentRV,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        documentRV.dateReception = moment(documentRV.dateReceptionObject).format('L');
        
        listDocumentsOrdreProduction.push(documentRV);
   }).then(function(){
  // console.log('list doc credits',listCredits);
    callback(null,listDocumentsOrdreProduction); 
   });

},


}
module.exports.DocumentsOrdresProduction = DocumentsOrdresProduction;