//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');
//we need to have article as a json object : {code :'mmm', design :'ddd'}
var ActivitesProduction = {
addActiviteProduction : function (db,activiteProduction, codeUser,callback){
    if(activiteProduction != undefined){
        activiteProduction.code  = utils.isUndefined(activiteProduction.code)?shortid.generate():activiteProduction.code;
        activiteProduction.userCreatorCode = codeUser;
        activiteProduction.userLastUpdatorCode = codeUser;
        activiteProduction.dateCreation = moment(new Date).format('L');
        activiteProduction.dateLastUpdate = moment(new Date).format('L');

        activiteProduction.dateDebutObject = utils.isUndefined(activiteProduction.dateDebutObject)? 
     new Date():activiteProduction.dateDebutObject;
     activiteProduction.dateFinObject = utils.isUndefined(activiteProduction.dateFinObject)? 
     new Date():activiteProduction.dateFinObject;

     if(utils.isUndefined(activiteProduction.numeroActiviteProduction))
   {
     const currentD =  moment(new Date());
          // const genereKey = shortid.generate();
     const generatedCode = currentD.year().toString() +'/'+currentD.utc(true).month().toString()+currentD.minutes().toString() ;
     activiteProduction.numeroActiviteProduction = 'ACTPROD' + generatedCode;

    }
   //   releveProduction.dateExpiration = utils.isUndefined(releveProduction.dateExpiration)? 
    //  releveProduction.dateExpiration : moment(releveProduction.dateExpiration ).format('L');
 
        db.collection('ActiviteProduction').insertOne(
            activiteProduction,
            function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            } 
          ) ;
         // db.close();
       //  callback(false,article);
    }

//return false;
},
updateActiviteProduction: function (db,activiteProduction,codeUser, callback){
    
    const criteria = { "code" : activiteProduction.code,"userCreatorCode" : codeUser };
    
    activiteProduction.dateDebutObject = utils.isUndefined(activiteProduction.dateDebutObject)? 
    new Date():activiteProduction.dateDebutObject;

    activiteProduction.dateFinObject = utils.isUndefined(activiteProduction.dateFinObject)? 
    new Date():activiteProduction.dateFinObject;

    const dataToUpdate = {
        "dateDebutObject" : activiteProduction.dateDebutObject,
        "dateFinObject" : activiteProduction.dateFinObject,
        "nomenclature" : activiteProduction.nomenclature,
        "dureeHours" : activiteProduction.dureeHours,
        "articleProduit" : activiteProduction.articleProduit,
        "qteProduitIntacte" : activiteProduction.qteProduitIntacte,
        "qteProduitDefaille" : activiteProduction.qteProduitDefaille,
        "unMesure" : activiteProduction.unMesure,
        "description" : activiteProduction.description,
        "conditionsStockages" : activiteProduction.conditionsStockages,
        "responsable" : activiteProduction.responsable,
        "depotStockage" : activiteProduction.conditionsStockages,
        "cout" : activiteProduction.cout,
        "etat" : activiteProduction.etat
        
} ;
     
            db.collection('ActiviteProduction').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }    
            ) ;
     
    
    },
    getListActivitesProduction : function (db,codeUser, callback)
    {
        let listActivitesProduction = [];
      
       var cursor =  db.collection('ActiviteProduction').find(
        {"userCreatorCode" : codeUser}
       );
       cursor.forEach(function(activiteProduction,err){
           if(err)
            {
                console.log('errors produced :', err);
    
            }
            activiteProduction.dateDebut = moment(activiteProduction.dateDebutObject).format('L');
            activiteProduction.dateFin = moment(activiteProduction.dateFinObject).format('L');
            // article.dateExpiration = moment(article.dateExpirationObject).format('L');
            // article.dateFabrication = moment(article.dateFabricationObject).format('L');
         
            listActivitesProduction.push(activiteProduction);
       }).then(function(){
       //  console.log('list clients',listClients);
        callback(null,listActivitesProduction); 
       });
       
   
         //return [];
    },

deleteActiviteProduction: function (db,codeActiviteProduction,codeUser,callback){
    const criteria = { "code" : codeActiviteProduction, "userCreatorCode":codeUser };
    
      
            db.collection('ActiviteProduction').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                } 
            ) ;
    
},
getPageActivitesProduction : function (db,codeUser,pageNumber,nbElementsPerPage, filter,callback)
{
    let listActivitesProduction = [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var cursor =  db.collection('ActiviteProduction').find(
      condition
   ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
   .limit(Number(nbElementsPerPage) );
   
   cursor.forEach(function(activiteProduction,err){
       if(err)
        {
            console.log('errors produced :', err);

        }
        activiteProduction.dateDebut = moment(activiteProduction.dateDebutObject).format('L');
        activiteProduction.dateFin = moment(activiteProduction.dateFinObject).format('L');
   
      
        listActivitesProduction.push(activiteProduction);
   }).then(function(){
   //  console.log('list clients',listClients);
    callback(null,listActivitesProduction); 
   });
   


},
getTotalActivitesProduction: function (db,codeUser,filter, callback)
{
   
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var totalActivitesProduction=  db.collection('ActiviteProduction').find( 
     condition
    ).count(function(err,result){
        callback(null,result); 
    });

   

},


getActiviteProduction: function (db,codeActiviteProduction,codeUser,callback)
{
   // const criteria = { "code" : codeArticle };
    const criteria = { "code" : codeActiviteProduction, "userCreatorCode":codeUser };
    
 
       const activiteProduction =  db.collection('ActiviteProduction').findOne(
            criteria , function(err,result){
                if(err){
                    callback(null,null);
                }else{
                    callback(false,result);
                }
            }
        ) ;
                   
},

buildFilterCondition(filterObject,filterData)
{
  
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.code))
        {
                filterData["numeroActiviteProduction"] = filterObject.numeroActiviteProduction;
        }
         
        if(!utils.isUndefined(filterObject.dureeHours))
          {
                 
                    filterData["dureeHours"] = filterObject.dureeHours;
                    
         }
         if(!utils.isUndefined(filterObject.articleCode))
             {
                    
                       filterData["articleProduit.code"] =filterObject.articleCode;
                       
            }
           
            if(!utils.isUndefined(filterObject.nomenclature))
                {
                       
                          filterData["nomenclature"] =filterObject.nomenclature;
                          
               }
              
                   if(!utils.isUndefined(filterObject.etat))
                    {
                           
                              filterData["etat"] = filterObject.etat ;
                              
                   }



        }
        return filterData;
},

};

module.exports.ActivitesProduction = ActivitesProduction;