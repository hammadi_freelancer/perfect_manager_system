//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var PaiementsOrdresProduction = {
addPaiementOrdreProduction : function (db,paiementOrdreProduction,codeUser,callback){
    if(paiementOrdreProduction != undefined){
        paiementOrdreProduction.code  = utils.isUndefined(paiementOrdreProduction.code)?shortid.generate():paiementOrdreProduction.code;
        paiementOrdreProduction.dateOperationObject = utils.isUndefined(paiementOrdreProduction.dateOperationObject)? 
        new Date():paiementOrdreProduction.dateOperationObject;
        paiementOrdreProduction.userCreatorCode = codeUser;
        paiementOrdreProduction.userLastUpdatorCode = codeUser;
        paiementOrdreProduction.dateCreation = moment(new Date).format('L');
        paiementOrdreProduction.dateLastUpdate = moment(new Date).format('L');
        
     
            db.collection('PaiementOrdreProduction').insertOne(
                paiementOrdreProduction,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,paiementOrdreProduction);
                   }
                }
              ) ;


}else{
callback(true,null);
}
},
updatePaiementOrdreProduction: function (db,paiementOrdreProduction,codeUser, callback){

   
    paiementOrdreProduction.dateOperationObject = utils.isUndefined(paiementOrdreProduction.dateOperationObject)? 
    new Date():paiementOrdreProduction.dateOperationObject;
    paiementOrdreProduction.dateLastUpdate = moment(new Date).format('L');
    
    const criteria = { "code" : paiementOrdreProduction.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"montantPaye" : paiementOrdreProduction.montantPaye, 
    "codeOrdreProduction" : paiementOrdreProduction.codeOrdreProduction, 
    "description" : paiementOrdreProduction.description,
     "codeOrganisation" : paiementOrdreProduction.codeOrganisation,
    "dateOperationObject" : paiementOrdreProduction.dateOperationObject ,
     "modePaiement" : paiementOrdreProduction.modePaiement, // cheque , virement , espece 
     "numeroCheque" : paiementOrdreProduction.numeroCheque, 
     "compte":paiementOrdreProduction.compte,
     "compteAdversaire" : paiementOrdreProduction.compteAdversaire , 
     "banque" : paiementOrdreProduction.banque ,
      "banqueAdversaire" : paiementOrdreProduction.banqueAdversaire, 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": paiementOrdreProduction.dateLastUpdate, 
    } ;
       
            db.collection('PaiementOrdreProduction').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
      
    
    },
getListPaiementsOrdresProduction : function (db,codeUser,callback)
{
    let listPaiementsOrdresProduction  = [];
    
   var cursor =  db.collection('PaiementOrdreProduction').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(paiementOrdreProduction,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        paiementOrdreProduction.dateOperation = moment(paiementOrdreProduction.dateOperationObject).format('L');
        
        listPaiementsOrdresProduction.push(paiementOrdreProduction);
   }).then(function(){
  //  console.log('list credits',listPaiementsFacturesVentes);
    callback(null,listPaiementsOrdresProduction); 
   });
},
deletePaiementOrdreProduction: function (db,codePaiementOrdreProduction,codeUser,callback){
    const criteria = { "code" : codePaiementOrdreProduction, "userCreatorCode":codeUser };
       
            db.collection('PaiementOrdreProduction').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
   
},

getPaiementOrdreProduction: function (db,codePaiementOrdreProduction,codeUser,callback)
{
    const criteria = { "code" : codePaiementOrdreProduction, "userCreatorCode":codeUser };
  
       const paiementOrdreProduction =  db.collection('PaiementOrdreProduction').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                     
},

getListPaiementsOrdresProductionByCodeOrdre : function (db,codeOrdre, codeUser,callback)
{
    let listPaiementsOrdreProduction = [];
  
   var cursor =  db.collection('PaiementOrdreProduction').find( 
       {"userCreatorCode" : codeUser, "codeOrdreProduction": codeOrdre}
    );
   cursor.forEach(function(paiementOrdreProduction,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        paiementOrdreProduction.dateOperation = moment(paiementOrdreProduction.dateOperationObject).format('L');
        paiementOrdreProduction.dateCheque = moment(paiementOrdreProduction.dateChequeObject).format('L');
        
        listPaiementsOrdreProduction.push(paiementOrdreProduction);
   }).then(function(){
   // console.log('list credits',listCredits);
    callback(null,listPaiementsOrdreProduction); 
   });

},
}
module.exports.PaiementsOrdresProduction = PaiementsOrdresProduction;