//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');
//we need to have article as a json object : {code :'mmm', design :'ddd'}
var RelevesProduction = {
addReleveProduction : function (db,releveProduction, codeUser,callback){
    if(releveProduction != undefined){

     releveProduction.code  = utils.isUndefined(releveProduction.code)?shortid.generate():releveProduction.code;

     releveProduction.userCreatorCode = codeUser;
     releveProduction.userLastUpdatorCode = codeUser;
     releveProduction.dateCreation = moment(new Date).format('L');
     releveProduction.dateLastUpdate = moment(new Date).format('L');

     releveProduction.dateDebutProductionObject = utils.isUndefined(releveProduction.dateDebutProductionObject)? 
     new Date():releveProduction.dateDebutProductionObject;

     releveProduction.dateFinProductionObject = utils.isUndefined(releveProduction.dateFinProductionObject)? 
     new Date():releveProduction.dateFinProductionObject;
     if(utils.isUndefined(releveProduction.numeroReleveProduction))
   {
     const currentD =  moment(new Date());
          // const genereKey = shortid.generate();
     const generatedCode = currentD.year().toString() +'/'+currentD.utc(true).month().toString()+currentD.minutes().toString() ;
     releveProduction.numeroReleveProduction = 'RELVPROD' + generatedCode;

    }
   //   releveProduction.dateExpiration = utils.isUndefined(releveProduction.dateExpiration)? 
    //  releveProduction.dateExpiration : moment(releveProduction.dateExpiration ).format('L');
 
        db.collection('ReleveProduction').insertOne(
            releveProduction,
            function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            } 
          ) ;
         // db.close();
       //  callback(false,article);
    }

//return false;
},
updateReleveProduction: function (db,releveProduction,codeUser, callback){
    
    const criteria = { "code" : releveProduction.code,"userCreatorCode" : codeUser };
    
    releveProduction.dateDebutProductionObject = utils.isUndefined(releveProduction.dateDebutProductionObject)? 
    new Date():releveProduction.dateDebutProductionObject;

    releveProduction.dateFinProductionObject = utils.isUndefined(releveProduction.dateFinProductionObject)? 
    new Date():releveProduction.dateFinProductionObject;

    const dataToUpdate = {
        "dateDebutProductionObject" : releveProduction.dateDebutProductionObject,
        "dateFinProductionObject" : releveProduction.dateFinProductionObject,
        "dateDisponibiliteObject" : releveProduction.dateDisponibiliteObject,
        "dureeHours" : releveProduction.dureeHours,
        "articleProduit" : releveProduction.articleProduit,
        "qteProduitIntacte" : releveProduction.qteProduitIntacte,
        "qteProduitDefaille" : releveProduction.qteProduitDefaille,
        "qteEnCoursProduction" : releveProduction.qteEnCoursProduction,
        "unMesure" : releveProduction.unMesure,
        "description" : releveProduction.description,
        "conditionsStockages" : releveProduction.conditionsStockages,
        "responsable" : releveProduction.responsable,
        "depotStockage" : releveProduction.conditionsStockages,
        "cout" : releveProduction.cout,
        "etat" : releveProduction.etat
        
} ;
     
            db.collection('ReleveProduction').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }    
            ) ;
     
    
    },
    getListRelevesProduction : function (db,codeUser, callback)
    {
        let listRelevesProduction = [];
      
       var cursor =  db.collection('ReleveProduction').find(
        {"userCreatorCode" : codeUser}
       );
       cursor.forEach(function(releveProduction,err){
           if(err)
            {
                console.log('errors produced :', err);
    
            }
            releveProduction.dateDebutProduction = moment(releveProduction.dateDebutProductionObject).format('L');
            releveProduction.dateFinProduction = moment(releveProduction.dateFinProductionObject).format('L');
            // article.dateExpiration = moment(article.dateExpirationObject).format('L');
            // article.dateFabrication = moment(article.dateFabricationObject).format('L');
         
                        listRelevesProduction.push(releveProduction);
       }).then(function(){
       //  console.log('list clients',listClients);
        callback(null,listRelevesProduction); 
       });
       
   
         //return [];
    },

deleteReleveProduction: function (db,codeReleveProduction,codeUser,callback){
    const criteria = { "code" : codeReleveProduction, "userCreatorCode":codeUser };
    
      
            db.collection('ReleveProduction').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                } 
            ) ;
    
},
getPageRelevesProduction : function (db,codeUser,pageNumber,nbElementsPerPage, filter,callback)
{
    let listRelevesProduction = [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var cursor =  db.collection('ReleveProduction').find(
      condition
   ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
   .limit(Number(nbElementsPerPage) );
   
   cursor.forEach(function(releveProduction,err){
       if(err)
        {
            console.log('errors produced :', err);

        }
        releveProduction.dateDebutProduction = moment(releveProduction.dateDebutProductionObject).format('L');
        releveProduction.dateFinProduction = moment(releveProduction.dateFinProductionObject).format('L');
   
      
        listRelevesProduction.push(releveProduction);
   }).then(function(){
   //  console.log('list clients',listClients);
    callback(null,listRelevesProduction); 
   });
   


},
getTotalRelevesProduction: function (db,codeUser,filter, callback)
{
   
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var totalRelevesProduction=  db.collection('ReleveProduction').find( 
     condition
    ).count(function(err,result){
        callback(null,result); 
    });

   

},


getReleveProduction: function (db,codeReleveProduction,codeUser,callback)
{
   // const criteria = { "code" : codeArticle };
    const criteria = { "code" : codeReleveProduction, "userCreatorCode":codeUser };
    
 
       const releveProduction =  db.collection('ReleveProduction').findOne(
            criteria , function(err,result){
                if(err){
                    callback(null,null);
                }else{
                    callback(false,result);
                }
            }
        ) ;
                   
},

buildFilterCondition(filterObject,filterData)
{
  
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.code))
        {
                filterData["numeroReleveProduction"] = filterObject.numeroReleveProduction;
        }
         
        if(!utils.isUndefined(filterObject.dureeHours))
          {
                 
                    filterData["dureeHours"] = filterObject.dureeHours;
                    
         }
         if(!utils.isUndefined(filterObject.articleCode))
             {
                    
                       filterData["articleProduit.code"] =filterObject.articleCode;
                       
            }
           

                   if(!utils.isUndefined(filterObject.etat))
                    {
                           
                              filterData["etat"] = filterObject.etat ;
                              
                   }



        }
        return filterData;
},

};

module.exports.RelevesProduction = RelevesProduction;