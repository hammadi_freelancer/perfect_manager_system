
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var documentsOrdresProduction = require('../model/documents-ordres-production').DocumentsOrdresProduction;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addDocumentOrdreProduction',function(req,res,next){
    
       console.log(" ADD DOCUMENT ORDRE PRODUCTION  IS CALLED") ;
       console.log(" THE DOCUMENT ORDRE PRODUCTION  TO ADDED IS :",req.body);
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = documentsOrdresProduction.addDocumentOrdreProduction(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,documentRAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_DOCUMENT_ORDRE_PRODUCTION',
                error_content: err
            });
         }else{
       
        return res.json(documentRAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateDocumentOrdreProduction',function(req,res,next){
    
       console.log(" UPDATE DOCUMENT ORDRE PRODUCTION  IS CALLED") ;
       // console.log(" THE DOCUMENT CREDIT  TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result =documentsOrdresProduction.updateDocumentOrdreProduction(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,documentRAUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_DOCUMENT_ORDRE_PRODUCTION',
                error_content: err
            });
         }else{
       
        return res.json(documentRAUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getDocumentsOrdresProduction',function(req,res,next){
 
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    documentsOrdresProduction.getListDocumentsOrdresProduction(
        req.app.locals.dataBase,decoded.userData.code, function(err,listDocumentsRelProductions){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_ORDRES_PRODUCTION',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsRelProductions);
          }
      });

  });
  router.get('/getDocumentOrdreProduction/:codeDocumentOrdreProduction',function(req,res,next){
    console.log("GET DOCUMENT REL PRODUCTION IS CALLED");
   
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    documentsOrdresProduction.getDocumentOrdreProduction(
        req.app.locals.dataBase,req.params['codeDocumentOrdreProduction'],decoded.userData.code,
     function(err,documentRProductions){
      // console.log("GET  DOCUMENT CREDIT : RESULT");
       //console.log(documentCredit);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_DOCUMENT_ORDRE_PRODUCTION',
               error_content: err
           });
        }else{
      
       return res.json(documentRProductions);
        }
    });
})

  router.delete('/deleteDocumentOrdreProduction/:codeDocumentOrdreProduction',function(req,res,next){
    
       console.log(" DELETE DOCUMENT ORDRE PRODUCTION  IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = documentsOrdresProduction.deleteDocumentOrdreProduction(req.app.locals.dataBase,
        req.params['codeDocumentOrdreProduction'],decoded.userData.code,
        function(err,codeDocumentOrdreProduction){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_DOCUMENT_ORDRE_PRODUCTION',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeDocumentOrdreProduction']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getDocumentsOrdresProductionByCodeOrdre/:codeOrdre',function(req,res,next){
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    documentsOrdresProduction.getListDocumentsOrdreProductionByCodeOrdre(req.app.locals.dataBase,
        decoded.userData.code,req.params['codeOrdre'],
         function(err,listDocumentsRelProductions){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_ORDRES_PRODUCTION',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsRelProductions);
          }
      });

  });
  module.exports.routerDocumentsOrdresProduction = router;