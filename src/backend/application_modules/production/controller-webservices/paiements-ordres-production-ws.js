
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var paiementsOrdresProduction = require('../model/paiements-ordres-production').PaiementsOrdresProduction;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addPaiementOrdreProduction',function(req,res,next){
    
       console.log(" ADD PAYEMENT ORDRE PRODUCTION IS CALLED") ;
       console.log(" THE PAYEMENT ORDRE PRODUCTION TO ADDED IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = paiementsOrdresProduction.addPaiementOrdreProduction(req.app.locals.dataBase,req.body,decoded.userData.code,
        function(err,paiementRelVenteAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_PAYEMENT_REL_PRODUCTION',
                error_content: err
            });
         }else{
       
        return res.json(paiementRelVenteAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updatePaiementOrdreProduction',function(req,res,next){
    
       console.log(" UPDATE PAYEMENT RELEVE PRODUCTION  IS CALLED") ;
       console.log("THE PAYEMENT RELEVE PRODUCTION TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = paiementsOrdresProduction.updatePaiementOrdreProduction(
        req.app.locals.dataBase,req.body,decoded.userData.code,
         function(err,paiementsRelVentesUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_PAYEMENT_RELEVES_PRODUCTION',
                error_content: err
            });
         }else{
       
        return res.json(paiementsRelVentesUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getPaiementsOrdresProduction',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
  
    paiementsOrdresProduction.getListPaiementsOrdresProduction(
        req.app.locals.dataBase,decoded.userData.code, function(err,listPaiementsRelVentes){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_PAYEMENTS_RELEVES_PRODUCTION',
                 error_content: err
             });
          }else{
        
         return res.json(listPaiementsRelVentes);
          }
      });

  });
  router.get('/getPaiementOrdreProduction/:codePaiementOrdreProduction',function(req,res,next){
    console.log("GET PAYEMENT ORDRE PRODUCTION IS CALLED");
    console.log(req.params['codePaiementOrdreProduction']);
   
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    paiementsOrdresProduction.getPaiementOrdreProduction(
        req.app.locals.dataBase,req.params['codePaiementOrdreProduction'],decoded.userData.code,
     function(err,paiementRelVentes){
       console.log("GET  PAYEMENT RELEVE PRODUCTION : RESULT");
       console.log(paiementRelVentes);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_PAYEMENT_RELEVE_PRODUCTION',
               error_content: err
           });
        }else{
      
       return res.json(paiementRelVentes);
        }
    });
})

  router.delete('/deletePaiementOrdreProduction/:codePaiementOrdreProduction',function(req,res,next){
    
       console.log(" DELETE PAYEMENT RELEVE PRODUCTION IS CALLED") ;
       console.log(" THE PAYEMENT RELEVE PRODUCTION TO DELETE IS :",req.params['codePaiementOrdreProduction']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = paiementsOrdresProduction.deletePaiementOrdreProduction(
        req.app.locals.dataBase,req.params['codePaiementOrdreProduction'],decoded.userData.code,
        function(err,codePaiementOrdreProduction){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_PAYEMENT_RELEVE_PRODUCTION',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codePaiementOrdreProduction']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

   router.get('/getPaiementsOrdreProductionByCodeOrdre/:codeOrdreProduction',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    paiementsOrdresProduction.getListPaiementsOrdresProductionByCodeOrdre(
        req.app.locals.dataBase,req.params['codeOrdreProduction'],decoded.userData.code, function(err,listPaiementsRelVen){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_PAYEMENTS_RELEVE_PRODUCTION_BY_CODE_RELEVE',
                 error_content: err
             });
          }else{
        
         return res.json(listPaiementsRelVen);
          }
      });

  })

  module.exports.routerPaiementsOrdresProduction = router;