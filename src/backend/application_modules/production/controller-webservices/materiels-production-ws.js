

var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var materielsProduction = require('../model/materiels-production').MaterielsProduction;
var jwt = require('jsonwebtoken');
var utils = require('../../utils/utils');

router.post('/addMaterielProduction',function(req,res,next){
    

       console.log(" ADD MATERIEL PRODUCTION IS CALLED") ;
       console.log(" THE MATERIEL PRODUCTION TO ADDED IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = materielsProduction.addMaterielProduction(req.app.locals.dataBase,req.body,decoded.userData.code,
        function(err,materielProduction){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_MATERIEL_PRODUCTION',
                error_content: err
            });
         }else{
       
        return res.json(materielProduction);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.put('/updateMaterielProduction',function(req,res,next){
    
       console.log(" UPDATE MATERIEL PRODUCTION IS CALLED") ;
       console.log(" THE MATERIEL PRODUCTION TO UPDATE IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = materielsProduction.updateMaterielProduction
       (req.app.locals.dataBase,req.body,decoded.userData.code,function(err,materielProduction){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_MATERIEL_PRODUCTION',
                error_content: err
            });
         }else{
       
        return res.json(materielProduction);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getMaterielsProduction',function(req,res,next){
    console.log("GET LIST MATERIELS PRODUCTION IS CALLED");
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    materielsProductions.getListMaterielsProduction(req.app.locals.dataBase,decoded.userData.code,function(err,listRelProd){
       console.log("GET LIST REL PROD : RESULT");
       console.log(listRelProd);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_MATERIELS_PRODUCTION',
               error_content: err
           });
        }else{
      
       return res.json(listRelProd);
        }
    });
});
router.get('/getMaterielProduction/:codeMaterielProduction',function(req,res,next){
    let hasRight = true;
    console.log("GET MATERIEL PRODUCTION IS CALLED");
    console.log(req.params['codeMaterielProduction']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
        console.log('user data is ');
        console.log(decoded.userData.accessRights );
        //if(utils.isdecoded.userData.accessRights !== undefined)
        if(!utils.isUndefined(decoded.userData.accessRights ))
            {
              let accessRightsArticle =  decoded.userData.accessRights.filter( (accessRight)=>
                (accessRight.entityName ==='MaterielProduction') );

                if(!utils.isUndefined(accessRightsArticle ))
         {
            let accessRightsGet=  accessRightsArticle.filter(
                 (accessRightArt)=>
        (
            accessRightArt.rights.filter((righ)=>(right==='Modification')).length !== 0) 
        );
        if(accessRightsGet.length === 0){
            hasRight = false;

         }
            }
        }

        if(hasRight){
    materielsProduction.getMaterielProduction(req.app.locals.dataBase,req.params['codeMaterielProduction'],
    decoded.userData.code,function(err,materielProduction){
       console.log("GET  MATERIEL PRODUCTION : RESULT");
       console.log(materielProduction);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_MATERIEL_PRODUCTION',
               error_content: err
           });
        }else{
      
       return res.json(materielProduction);
        }
    });
}else
{
    return  res.json({
        error_message : 'EDIT_MATERIEL_PRODUCTION_ACCESS_DENIED',
        error_content: 'ACCESS_DENIED'
    });
}
});
/*router.post('/getFiltredClients',function(req,res,next){
    console.log("GET FILTRED LIST CLIENTS IS CALLED");
    clients.getFiltredListClients(req.body,function(err,listFiltredClients){
       console.log("GET FILTRED LIST CLIENTS : RESULT");
       console.log(listFiltredClients);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_FILTRED_CLIENTS',
               error_content: err
           });
        }else{
      
       return res.json(listFiltredClients);
        }
    });
})*/
router.delete('/deleteMaterielProduction/:codeMaterielProduction',function(req,res,next){
    
       console.log(" DELETE MATERIEL PRODUCTION IS CALLED") ;
       console.log(" THE MATERIEL PRODUCTION TO DELETE IS :",req.params['codeMaterielProduction']);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = materielsProduction.deleteMaterielProduction(req.app.locals.dataBase,req.params['codeMaterielProduction'],decoded.userData.code,function(err,codeRel){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_MATERIEL_PRODUCTION',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeMaterielProduction']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.get('/getPageMaterielsProduction',function(req,res,next){
   //  console.log("GET LIST CLIENTS IS CALLED");
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
        console.log('user data is ', decoded.userData);
    materielsProduction.getPageMaterielsProduction(req.app.locals.dataBase,decoded.userData.code,
         req.query.pageNumber, req.query.nbElementsPerPage ,
        req.query.filter,function(err,listRelProd){
      //  console.log("GET LIST CLIENTS : RESULT");
      //  console.log(listArticles);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_PAGE_MATERIELS_PRODUCTION',
               error_content: err
           });
        }else{
      
       return res.json(listRelProd);
        }
    });
});
router.get('/getTotalMaterielsProduction',function(req,res,next){
   
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    materielsProduction.getTotalMaterielsProduction(req.app.locals.dataBase,decoded.userData.code,
        req.query.filter, function(err,totalRelProduction){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_MATERIELS_PRODUCTION',
                 error_content: err
             });
          }else{
        
         return res.json({'totalMaterielsProduction':totalMaterielsProduction});
          }
      });

  });
module.exports.materielsProductionRouter= router;