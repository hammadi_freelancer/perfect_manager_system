

var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var activitesProduction = require('../model/activites-production').ActivitesProduction;
var jwt = require('jsonwebtoken');
var utils = require('../../utils/utils');

router.post('/addActiviteProduction',function(req,res,next){
    

       console.log(" ADD ACTIVITE PRODUCTION IS CALLED") ;
       console.log(" THE ACTIVITE PRODUCTION TO ADDED IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = activitesProduction.addActiviteProduction(req.app.locals.dataBase,req.body,decoded.userData.code,
        function(err,activiteProduction){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_ACTIVITE_PRODUCTION',
                error_content: err
            });
         }else{
       
        return res.json(activiteProduction);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.put('/updateActiviteProduction',function(req,res,next){
    
       console.log(" UPDATE ACTIVITE PRODUCTION IS CALLED") ;
       console.log(" THE ACTIVITE PRODUCTION TO UPDATE IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = activitesProduction.updateActiviteProduction
       (req.app.locals.dataBase,req.body,decoded.userData.code,function(err,activiteProduction){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_ACTIVITE_PRODUCTION',
                error_content: err
            });
         }else{
       
        return res.json(activiteProduction);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getActivitesProduction',function(req,res,next){
    console.log("GET LIST ACTIVITES PRODUCTION IS CALLED");
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    activitesProductions.getListActivitesProduction(req.app.locals.dataBase,decoded.userData.code,function(err,listRelProd){
       console.log("GET LIST REL PROD : RESULT");
       console.log(listRelProd);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_ACTIVITES_PRODUCTION',
               error_content: err
           });
        }else{
      
       return res.json(listRelProd);
        }
    });
});
router.get('/getActiviteProduction/:codeActiviteProduction',function(req,res,next){
    let hasRight = true;
    console.log("GET ACTIVITE PRODUCTION IS CALLED");
    console.log(req.params['codeActiviteProduction']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
        console.log('user data is ');
        console.log(decoded.userData.accessRights );
        //if(utils.isdecoded.userData.accessRights !== undefined)
        if(!utils.isUndefined(decoded.userData.accessRights ))
            {
              let accessRightsArticle =  decoded.userData.accessRights.filter( (accessRight)=>
                (accessRight.entityName ==='ActiviteProduction') );

                if(!utils.isUndefined(accessRightsArticle ))
         {
            let accessRightsGet=  accessRightsArticle.filter(
                 (accessRightArt)=>
        (
            accessRightArt.rights.filter((righ)=>(right==='Modification')).length !== 0) 
        );
        if(accessRightsGet.length === 0){
            hasRight = false;

         }
            }
        }

        if(hasRight){
    activitesProduction.getActiviteProduction(req.app.locals.dataBase,req.params['codeActiviteProduction'],
    decoded.userData.code,function(err,activiteProduction){
       console.log("GET  ACTIVITE PRODUCTION : RESULT");
       console.log(activiteProduction);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_ACTIVITE_PRODUCTION',
               error_content: err
           });
        }else{
      
       return res.json(activiteProduction);
        }
    });
}else
{
    return  res.json({
        error_message : 'EDIT_ACTIVITE_PRODUCTION_ACCESS_DENIED',
        error_content: 'ACCESS_DENIED'
    });
}
});
/*router.post('/getFiltredClients',function(req,res,next){
    console.log("GET FILTRED LIST CLIENTS IS CALLED");
    clients.getFiltredListClients(req.body,function(err,listFiltredClients){
       console.log("GET FILTRED LIST CLIENTS : RESULT");
       console.log(listFiltredClients);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_FILTRED_CLIENTS',
               error_content: err
           });
        }else{
      
       return res.json(listFiltredClients);
        }
    });
})*/
router.delete('/deleteActiviteProduction/:codeActiviteProduction',function(req,res,next){
    
       console.log(" DELETE ACTIVITE PRODUCTION IS CALLED") ;
       console.log(" THE ACTIVITE PRODUCTION TO DELETE IS :",req.params['codeActiviteProduction']);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = activitesProduction.deleteActiviteProduction(req.app.locals.dataBase,req.params['codeActiviteProduction'],decoded.userData.code,function(err,codeRel){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_ACTIVITE_PRODUCTION',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeActiviteProduction']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.get('/getPageActivitesProduction',function(req,res,next){
   //  console.log("GET LIST CLIENTS IS CALLED");
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
        console.log('user data is ', decoded.userData);
    activitesProduction.getPageActivitesProduction(req.app.locals.dataBase,decoded.userData.code,
         req.query.pageNumber, req.query.nbElementsPerPage ,
        req.query.filter,function(err,listRelProd){
      //  console.log("GET LIST CLIENTS : RESULT");
      //  console.log(listArticles);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_PAGE_ACTIVITES_PRODUCTION',
               error_content: err
           });
        }else{
      
       return res.json(listRelProd);
        }
    });
});
router.get('/getTotalActivitesProduction',function(req,res,next){
   
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    activitesProduction.getTotalActivitesProduction(req.app.locals.dataBase,decoded.userData.code,
        req.query.filter, function(err,totalRelProduction){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_ACTIVITES_PRODUCTION',
                 error_content: err
             });
          }else{
        
         return res.json({'totalActivitesProduction':totalActivitesProduction});
          }
      });

  });
module.exports.activitesProductionRouter= router;