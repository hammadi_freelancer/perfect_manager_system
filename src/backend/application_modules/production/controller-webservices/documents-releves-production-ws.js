
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var documentsRelevesProduction = require('../model/documents-releves-production').DocumentsRelevesProduction;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addDocumentReleveProduction',function(req,res,next){
    
       console.log(" ADD DOCUMENT RELEVE PRODUCTION  IS CALLED") ;
       console.log(" THE DOCUMENT RELEVE PRODUCTION  TO ADDED IS :",req.body);
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = documentsRelevesProduction.addDocumentReleveProduction(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,documentRAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_DOCUMENT_RELEVE_PRODUCTION',
                error_content: err
            });
         }else{
       
        return res.json(documentRAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateDocumentReleveProduction',function(req,res,next){
    
       console.log(" UPDATE DOCUMENT RELEVE PRODUCTION  IS CALLED") ;
       // console.log(" THE DOCUMENT CREDIT  TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result =documentsRelevesProduction.updateDocumentReleveProduction(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,documentRAUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_DOCUMENT_RELEVE_PRODUCTION',
                error_content: err
            });
         }else{
       
        return res.json(documentRAUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getDocumentsRelevesProduction',function(req,res,next){
 
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    documentsRelevesProduction.getListDocumentsRelevesProduction(
        req.app.locals.dataBase,decoded.userData.code, function(err,listDocumentsRelProductions){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_RELEVES_PRODUCTION',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsRelProductions);
          }
      });

  });
  router.get('/getDocumentReleveProduction/:codeDocumentReleveProduction',function(req,res,next){
    console.log("GET DOCUMENT REL PRODUCTION IS CALLED");
   
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    documentsRelevesProduction.getDocumentReleveProduction(
        req.app.locals.dataBase,req.params['codeDocumentReleveProduction'],decoded.userData.code,
     function(err,documentRProductions){
      // console.log("GET  DOCUMENT CREDIT : RESULT");
       //console.log(documentCredit);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_DOCUMENT_RELEVE_PRODUCTION',
               error_content: err
           });
        }else{
      
       return res.json(documentRProductions);
        }
    });
})

  router.delete('/deleteDocumentReleveProduction/:codeDocumentReleveProduction',function(req,res,next){
    
       console.log(" DELETE DOCUMENT RELEVE PRODUCTION  IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = documentsRelevesProduction.deleteDocumentReleveProduction(req.app.locals.dataBase,
        req.params['codeDocumentReleveProduction'],decoded.userData.code,
        function(err,codeDocumentReleveProduction){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_DOCUMENT_RELEVE_PRODUCTION',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeDocumentReleveProduction']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getDocumentsRelevesProductionByCodeReleve/:codeReleve',function(req,res,next){
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    documentsRelevesProduction.getListDocumentsReleveProductionByCodeReleve(req.app.locals.dataBase,
        decoded.userData.code,req.params['codeReleve'],
         function(err,listDocumentsRelProductions){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_RELEVES_PRODUCTION',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsRelProductions);
          }
      });

  });
  module.exports.routerDocumentsRelevesProduction = router;