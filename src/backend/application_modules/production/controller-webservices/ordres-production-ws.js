

var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var ordresProduction = require('../model/ordres-production').OrdresProduction;
var jwt = require('jsonwebtoken');
var utils = require('../../utils/utils');

router.post('/addOrdreProduction',function(req,res,next){
    

       console.log(" ADD ORDRE PRODUCTION IS CALLED") ;
       console.log(" THE ORDRE PRODUCTION TO ADDED IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = ordresProduction.addOrdreProduction(req.app.locals.dataBase,req.body,decoded.userData.code,
        function(err,ordreProduction){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_ORDRE_PRODUCTION',
                error_content: err
            });
         }else{
       
        return res.json(ordreProduction);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.put('/updateOrdreProduction',function(req,res,next){
    
       console.log(" UPDATE ORDRE PRODUCTION IS CALLED") ;
       console.log(" THE ORDRE PRODUCTION TO UPDATE IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = ordresProduction.updateOrdreProduction
       (req.app.locals.dataBase,req.body,decoded.userData.code,function(err,ordreProduction){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_ORDRE_PRODUCTION',
                error_content: err
            });
         }else{
       
        return res.json(ordreProduction);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getOrdresProduction',function(req,res,next){
    console.log("GET LIST ORDRES PRODUCTION IS CALLED");
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    ordresProductions.getListOrdresProduction(req.app.locals.dataBase,decoded.userData.code,function(err,listRelProd){
       console.log("GET LIST REL PROD : RESULT");
       console.log(listRelProd);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_ORDRES_PRODUCTION',
               error_content: err
           });
        }else{
      
       return res.json(listRelProd);
        }
    });
});
router.get('/getOrdreProduction/:codeOrdreProduction',function(req,res,next){
    let hasRight = true;
    console.log("GET ORDRE PRODUCTION IS CALLED");
    console.log(req.params['codeOrdreProduction']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
        console.log('user data is ');
        console.log(decoded.userData.accessRights );
        //if(utils.isdecoded.userData.accessRights !== undefined)
        if(!utils.isUndefined(decoded.userData.accessRights ))
            {
              let accessRightsArticle =  decoded.userData.accessRights.filter( (accessRight)=>
                (accessRight.entityName ==='OrdreProduction') );

                if(!utils.isUndefined(accessRightsArticle ))
         {
            let accessRightsGet=  accessRightsArticle.filter(
                 (accessRightArt)=>
        (
            accessRightArt.rights.filter((righ)=>(right==='Modification')).length !== 0) 
        );
        if(accessRightsGet.length === 0){
            hasRight = false;

         }
            }
        }

        if(hasRight){
    ordresProduction.getOrdreProduction(req.app.locals.dataBase,req.params['codeOrdreProduction'],
    decoded.userData.code,function(err,ordreProduction){
       console.log("GET  ORDRE PRODUCTION : RESULT");
       console.log(ordreProduction);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_ORDRE_PRODUCTION',
               error_content: err
           });
        }else{
      
       return res.json(ordreProduction);
        }
    });
}else
{
    return  res.json({
        error_message : 'EDIT_ORDRE_PRODUCTION_ACCESS_DENIED',
        error_content: 'ACCESS_DENIED'
    });
}
});
/*router.post('/getFiltredClients',function(req,res,next){
    console.log("GET FILTRED LIST CLIENTS IS CALLED");
    clients.getFiltredListClients(req.body,function(err,listFiltredClients){
       console.log("GET FILTRED LIST CLIENTS : RESULT");
       console.log(listFiltredClients);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_FILTRED_CLIENTS',
               error_content: err
           });
        }else{
      
       return res.json(listFiltredClients);
        }
    });
})*/
router.delete('/deleteOrdreProduction/:codeOrdreProduction',function(req,res,next){
    
       console.log(" DELETE ORDRE PRODUCTION IS CALLED") ;
       console.log(" THE ORDRE PRODUCTION TO DELETE IS :",req.params['codeOrdreProduction']);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = ordresProduction.deleteOrdreProduction(req.app.locals.dataBase,req.params['codeOrdreProduction'],decoded.userData.code,function(err,codeRel){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_ORDRE_PRODUCTION',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeOrdreProduction']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.get('/getPageOrdresProduction',function(req,res,next){
   //  console.log("GET LIST CLIENTS IS CALLED");
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
        console.log('user data is ', decoded.userData);
    ordresProduction.getPageOrdresProduction(req.app.locals.dataBase,decoded.userData.code,
         req.query.pageNumber, req.query.nbElementsPerPage ,
        req.query.filter,function(err,listRelProd){
      //  console.log("GET LIST CLIENTS : RESULT");
      //  console.log(listArticles);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_PAGE_ORDRES_PRODUCTION',
               error_content: err
           });
        }else{
      
       return res.json(listRelProd);
        }
    });
});
router.get('/getTotalOrdresProduction',function(req,res,next){
   
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    ordresProduction.getTotalOrdresProduction(req.app.locals.dataBase,decoded.userData.code,
        req.query.filter, function(err,totalRelProduction){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_ORDRES_PRODUCTION',
                 error_content: err
             });
          }else{
        
         return res.json({'totalOrdresProduction':totalOrdresProduction});
          }
      });

  });
module.exports.ordresProductionRouter= router;