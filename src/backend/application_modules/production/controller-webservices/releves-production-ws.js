

var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var relevesProduction = require('../model/releves-production').RelevesProduction;
var jwt = require('jsonwebtoken');
var utils = require('../../utils/utils');

router.post('/addReleveProduction',function(req,res,next){
    

       console.log(" ADD RELEVE PRODUCTION IS CALLED") ;
       console.log(" THE RELEVE PRODUCTION TO ADDED IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = relevesProduction.addReleveProduction(req.app.locals.dataBase,req.body,decoded.userData.code,
        function(err,releveProduction){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_RELEVE_PRODUCTION',
                error_content: err
            });
         }else{
       
        return res.json(releveProduction);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.put('/updateReleveProduction',function(req,res,next){
    
       console.log(" UPDATE RELEVE PRODUCTION IS CALLED") ;
       console.log(" THE RELEVE PRODUCTION TO UPDATE IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = relevesProduction.updateReleveProduction
       (req.app.locals.dataBase,req.body,decoded.userData.code,function(err,releveProduction){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_RELEVE_PRODUCTION',
                error_content: err
            });
         }else{
       
        return res.json(releveProduction);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getRelevesProduction',function(req,res,next){
    console.log("GET LIST RELEVES PRODUCTION IS CALLED");
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    relevesProductions.getListRelevesProduction(req.app.locals.dataBase,decoded.userData.code,function(err,listRelProd){
       console.log("GET LIST REL PROD : RESULT");
       console.log(listRelProd);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_RELEVES_PRODUCTION',
               error_content: err
           });
        }else{
      
       return res.json(listRelProd);
        }
    });
});
router.get('/getReleveProduction/:codeReleveProduction',function(req,res,next){
    let hasRight = true;
    console.log("GET RELEVE PRODUCTION IS CALLED");
    console.log(req.params['codeReleveProduction']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
        console.log('user data is ');
        console.log(decoded.userData.accessRights );
        //if(utils.isdecoded.userData.accessRights !== undefined)
        if(!utils.isUndefined(decoded.userData.accessRights ))
            {
              let accessRightsArticle =  decoded.userData.accessRights.filter( (accessRight)=>
                (accessRight.entityName ==='ReleveProduction') );

                if(!utils.isUndefined(accessRightsArticle ))
         {
            let accessRightsGet=  accessRightsArticle.filter(
                 (accessRightArt)=>
        (
            accessRightArt.rights.filter((righ)=>(right==='Modification')).length !== 0) 
        );
        if(accessRightsGet.length === 0){
            hasRight = false;

         }
            }
        }

        if(hasRight){
    relevesProduction.getReleveProduction(req.app.locals.dataBase,req.params['codeReleveProduction'],
    decoded.userData.code,function(err,releveProduction){
       console.log("GET  RELEVE PRODUCTION : RESULT");
       console.log(releveProduction);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_RELEVE_PRODUCTION',
               error_content: err
           });
        }else{
      
       return res.json(releveProduction);
        }
    });
}else
{
    return  res.json({
        error_message : 'EDIT_RELEVE_PRODUCTION_ACCESS_DENIED',
        error_content: 'ACCESS_DENIED'
    });
}
});
/*router.post('/getFiltredClients',function(req,res,next){
    console.log("GET FILTRED LIST CLIENTS IS CALLED");
    clients.getFiltredListClients(req.body,function(err,listFiltredClients){
       console.log("GET FILTRED LIST CLIENTS : RESULT");
       console.log(listFiltredClients);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_FILTRED_CLIENTS',
               error_content: err
           });
        }else{
      
       return res.json(listFiltredClients);
        }
    });
})*/
router.delete('/deleteReleveProduction/:codeReleveProduction',function(req,res,next){
    
       console.log(" DELETE RELEVE PRODUCTION IS CALLED") ;
       console.log(" THE RELEVE PRODUCTION TO DELETE IS :",req.params['codeReleveProduction']);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = relevesProduction.deleteReleveProduction(req.app.locals.dataBase,req.params['codeReleveProduction'],decoded.userData.code,function(err,codeRel){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_RELEVE_PRODUCTION',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeReleveProduction']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.get('/getPageRelevesProduction',function(req,res,next){
   //  console.log("GET LIST CLIENTS IS CALLED");
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
        console.log('user data is ', decoded.userData);
    relevesProduction.getPageRelevesProduction(req.app.locals.dataBase,decoded.userData.code,
         req.query.pageNumber, req.query.nbElementsPerPage ,
        req.query.filter,function(err,listRelProd){
      //  console.log("GET LIST CLIENTS : RESULT");
      //  console.log(listArticles);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_PAGE_RELEVES_PRODUCTION',
               error_content: err
           });
        }else{
      
       return res.json(listRelProd);
        }
    });
});
router.get('/getTotalRelevesProduction',function(req,res,next){
   
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    relevesProduction.getTotalRelevesProduction(req.app.locals.dataBase,decoded.userData.code,
        req.query.filter, function(err,totalRelProduction){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_RELEVES_PRODUCTION',
                 error_content: err
             });
          }else{
        
         return res.json({'totalRelevesProduction':totalRelevesProduction});
          }
      });

  });
module.exports.relevesProductionRouter= router;