
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var paiementsRelevesProduction = require('../model/paiements-releves-production').PaiementsRelevesProduction;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addPaiementReleveProduction',function(req,res,next){
    
       console.log(" ADD PAYEMENT RELEVE VENTE IS CALLED") ;
       console.log(" THE PAYEMENT RELEVE VENTE TO ADDED IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = paiementsRelevesProduction.addPaiementReleveProduction(req.app.locals.dataBase,req.body,decoded.userData.code,
        function(err,paiementRelVenteAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_PAYEMENT_REL_PRODUCTION',
                error_content: err
            });
         }else{
       
        return res.json(paiementRelVenteAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updatePaiementReleveProduction',function(req,res,next){
    
       console.log(" UPDATE PAYEMENT RELEVE PRODUCTION  IS CALLED") ;
       console.log("THE PAYEMENT RELEVE PRODUCTION TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = paiementsRelevesProduction.updatePaiementReleveProduction(
        req.app.locals.dataBase,req.body,decoded.userData.code,
         function(err,paiementsRelVentesUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_PAYEMENT_RELEVES_PRODUCTION',
                error_content: err
            });
         }else{
       
        return res.json(paiementsRelVentesUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getPaiementsRelevesProduction',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
  
    paiementsRelevesProduction.getListPaiementsRelevesProduction(
        req.app.locals.dataBase,decoded.userData.code, function(err,listPaiementsRelVentes){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_PAYEMENTS_RELEVES_PRODUCTION',
                 error_content: err
             });
          }else{
        
         return res.json(listPaiementsRelVentes);
          }
      });

  });
  router.get('/getPaiementReleveProduction/:codePaiementReleveProduction',function(req,res,next){
    console.log("GET PAYEMENT RELEVE VENTE IS CALLED");
    console.log(req.params['codePaiementReleveProduction']);
   
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    paiementsRelevesProduction.getPaiementReleveProduction(
        req.app.locals.dataBase,req.params['codePaiementReleveProduction'],decoded.userData.code,
     function(err,paiementRelVentes){
       console.log("GET  PAYEMENT RELEVE PRODUCTION : RESULT");
       console.log(paiementRelVentes);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_PAYEMENT_RELEVE_PRODUCTION',
               error_content: err
           });
        }else{
      
       return res.json(paiementRelVentes);
        }
    });
})

  router.delete('/deletePaiementReleveProduction/:codePaiementReleveProduction',function(req,res,next){
    
       console.log(" DELETE PAYEMENT RELEVE PRODUCTION IS CALLED") ;
       console.log(" THE PAYEMENT RELEVE PRODUCTION TO DELETE IS :",req.params['codePaiementReleveProduction']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = paiementsRelevesProduction.deletePaiementReleveProduction(
        req.app.locals.dataBase,req.params['codePaiementReleveProduction'],decoded.userData.code,
        function(err,codePaiementReleveProduction){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_PAYEMENT_RELEVE_PRODUCTION',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codePaiementReleveProduction']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

   router.get('/getPaiementsReleveProductionByCodeReleve/:codeReleveProduction',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    paiementsRelevesProduction.getListPaiementsRelevesProductionByCodeReleve(
        req.app.locals.dataBase,req.params['codeReleveProduction'],decoded.userData.code, function(err,listPaiementsRelVen){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_PAYEMENTS_RELEVE_PRODUCTION_BY_CODE_RELEVE',
                 error_content: err
             });
          }else{
        
         return res.json(listPaiementsRelVen);
          }
      });

  })

  module.exports.routerPaiementsRelevesProduction = router;