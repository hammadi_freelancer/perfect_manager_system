
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var bonsLivraisons = require('../model/bons-livraisons').BonsLivraisons;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addBonLivraison',function(req,res,next){
    
    console.log(" ADD BON LIVRAISON IS CALLED") ;
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = bonsLivraisons.addBonLivraison(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,bonLivraisonAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_BON_LIVRAISON',
                error_content: err
            });
         }else{
       
        return res.json(bonLivraisonAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateBonLivraison',function(req,res,next){
    
       console.log(" UPDATE BON LIVRAISON IS CALLED") ;
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = bonsLivraisons.updateBonLivraison(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,bonLivraisonAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_BON_LIVRAISON',
                error_content: err
            });
         }else{
       
        return res.json(bonLivraisonAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getBonsLivraisons',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
      console.log("GET LIST BONS LIVRAISONS IS CALLED");
    bonsLivraisons.getListBonsLivraisons(
        req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
        function(err,bonsLivraisons){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_BONS_LIVRAISONS',
                 error_content: err
             });
          }else{
        
         return res.json(bonsLivraisons);
          }
      });

  });
  router.get('/getPageBonsLivraisons',function(req,res,next){
    
        var decoded = jwt.decode(req.headers.authorization.slice( 
            req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
      
          console.log("GET PAGE BONS LIVRAISONS IS CALLED");
        bonsLivraisons.getPageBonsLivraisons(
            req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
            function(err,listBonsLivraisons){
              if(err){
                 return  res.json({
                     error_message : 'ERROR_GET_PAGE_BONS_LIVRAISONS',
                     error_content: err
                 });
              }else{
            
             return res.json(listBonsLivraisons);
              }
          });
    
      });
  router.get('/getBonLivraison/:codeBonLivraison',function(req,res,next){
    console.log("GET BON LIVRAISON IS CALLED");
  //  console.log(req.params['codeCredit']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    bonsLivraisons.getBonLivraison(
        req.app.locals.dataBase,req.params['codeBonLivraison'],
        decoded.userData.code, function(err,bonLivraison){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_BON_LIVRAISON',
               error_content: err
           });
        }else{
      
       return res.json(bonLivraison);
        }
    });
})

  router.delete('/deleteBonLivraison/:codeBonLivraison',function(req,res,next){
    
       console.log(" DELETE BON LIVRAISON IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = bonsLivraisons.deleteBonLivraison(
        req.app.locals.dataBase,req.params['codeBonLivraison'],decoded.userData.code, function(err,codeBonLivraison){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_BON_LIVRAISON',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeBonLivraison']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });



 router.get('/getTotalBonsLivraisons',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    bonsLivraisons.getTotalBonsLivraisons(
        req.app.locals.dataBase,decoded.userData.code,function(err,totalBonsLivraisons){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_BONS_LIVRAISONS',
                 error_content: err
             });
          }else{
        
         return res.json({'totalBonsLivraisons':totalBonsLivraisons});
          }
      });

  });
  module.exports.routerBonsLivraisons = router;