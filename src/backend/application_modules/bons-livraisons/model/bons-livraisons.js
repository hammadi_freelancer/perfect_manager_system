//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var BonsLivraisons = {
addBonLivraison : function (db,bonLivraison,codeUser,callback){
    if(bonLivraison != undefined){
        bonLivraison.code  = utils.isUndefined(bonLivraison.code)?shortid.generate():bonLivraison.code;

        bonLivraison.dateOperationObject = utils.isUndefined(bonLivraison.dateOperationObject)? 
        new Date():bonLivraison.dateOperationObject;
        if(utils.isUndefined(bonLivraison.numeroBonLivraison))
            {
               const currentD =  moment(new Date());
              // const genereKey = shortid.generate();
              const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.day().toString()+
              shortid.generate();
              bonLivraison.numeroBonLivraison = 'BL' +generatedCode;
            }
       
            bonLivraison.userCreatorCode = codeUser;
            bonLivraison.userLastUpdatorCode = codeUser;
            bonLivraison.dateCreation = moment(new Date).format('L');
            bonLivraison.dateLastUpdate = moment(new Date).format('L');
            db.collection('BonLivraison').insertOne(
                bonLivraison,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
             // db.close();
             
    

}else{
callback(true,null);
}
},
updateBonLivraison: function (db,bonLivraison,codeUser, callback){

   
    bonLivraison.dateLastUpdate = moment(new Date).format('L');
    
    bonLivraison.dateOperationObject = utils.isUndefined(bonLivraison.dateOperationObject)? 
    new Date():bonLivraison.dateOperationObject;
    const criteria = { "code" : bonLivraison.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
    "dateOperationObject" : bonLivraison.dateOperationObject, 
    "dateDebutObject" : bonLivraison.dateDebutObject, 
    "dateFinObject" : bonLivraison.dateFinObject, 
     "client": bonLivraison.client,
     "montantTotal": bonLivraison.montantTotal,
     "marchandisesLivrees": bonLivraison.marchandisesLivrees,
     "montantTotalRegle": bonLivraison.montantTotalRegle,
     "lignesBonLivraison": bonLivraison.lignesBonLivraison,
     "etat": bonLivraison.etat,

       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": bonLivraison.dateLastUpdate, 
    } ;
            db.collection('BonLivraison').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },function(bonLivraison,err){
                    if(err)
                    callback(false,err);
                    else
                        callback(true,true);
                    
                    
                }
            ) ;
     
    
    },
getListBonsLivraisons: function (db,codeUser,pageNumber, nbElementsPerPage , callback)
{
    let listBonsLivraisons = [];
   
   var cursor =  db.collection('BonLivraison').find( 
       {"userCreatorCode" : codeUser}
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(bonLivraison,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        bonLivraison.dateOperation= moment(bonLivraison.dateOperationObject).format('L');
       // credit.dateDebutPayement = moment(credit.dateDebutPayementObject).format('L');
        
       listBonsLivraisons.push(bonLivraison);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listBonsLivraisons); 
   });
     //return [];
},
deleteBonLivraison: function (db,codeBonLivraison,codeUser,callback){
    const criteria = { "code" : codeBonLivraison, "userCreatorCode":codeUser };
    
    db.collection('BonLivraison').deleteOne(
        criteria , function(err,result){
            if(err){
                
                callback(null,null);
            }else{
                callback(false,result);
            }
        }
    ) ;
},

getBonLivraison: function (db,codeBonLivraison,codeUser,callback)
{
    const criteria = { "code" : codeBonLivraison, "userCreatorCode":codeUser };
  
       const bonL =  db.collection('BonLivraison').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
        // callback(false,client);
                 
},
getTotalBonsLivraisons : function (db,codeUser, callback)
{
   var totalBonsLivraisons =  db.collection('BonLivraison').find( 
       {"userCreatorCode" : codeUser}
    ).count(function(err,result){
        callback(null,result); 
    }
);
   


     //return [];
},
getPageBonsLivraisons : function (db,codeUser,pageNumber, nbElementsPerPage , callback)
{
    let listBonsLivraisons = [];
   
   var cursor =  db.collection('BonLivraison').find( 
       {"userCreatorCode" : codeUser}
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(bonLivraison,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        bonLivraison.dateOperation= moment(bonLivraison.dateOperationObject).format('L');
       // credit.dateDebutPayement = moment(credit.dateDebutPayementObject).format('L');
        
       listBonsLivraisons.push(bonLivraison);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listBonsLivraisons); 
   });
     //return [];
},
}
module.exports.BonsLivraisons= BonsLivraisons;