

var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var filesData = require('../model/files-data').FilesData;

router.post('/addFileData',function(req,res,next){
    
       console.log(" ADD FILE DATA  IS CALLED") ;
       console.log(" THE FILE DATA  TO ADDED IS :",req.body);
       var result = filesData.addFileData(req.body,function(err,fileDataAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_FILE_DATA',
                error_content: err
            });
         }else{
       
        return res.json(fileDataAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.put('/updateFileData',function(req,res,next){
    
       console.log(" UPDATE FILE DATA  IS CALLED") ;
       console.log(" THE FILE DATA  TO UPDATE IS :",req.body);
       var result = filesData.updateFileData(req.body,function(err,fileDataUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_FILE_DATA',
                error_content: err
            });
         }else{
       
        return res.json(fileDataUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getFilesData',function(req,res,next){
    console.log("GET LIST FILES DATA  IS CALLED");
    filesData.getListFilesData(function(err,listFilesData){
       console.log("GET LIST FILES DATA  : RESULT");
       console.log(listFilesData);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_FILES_DATA',
               error_content: err
           });
        }else{
      
       return res.json(listFilesData);
        }
    });
})

router.delete('/deleteFileData/:codeFileData',function(req,res,next){
    
       console.log(" DELETE FILE DATA  IS CALLED") ;
       console.log(" THE FILE DATA  TO DELETE IS :",req.params['codeFileData']);
       var result = filesData.deleteFileData(req.params['codeFileData'],function(err,codeFileData){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_FILE_DATA',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeFileData']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

module.exports.filesDataRouter = router;