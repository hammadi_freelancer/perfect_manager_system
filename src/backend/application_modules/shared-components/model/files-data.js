//var db = require('../common/MongoDbConnection').getDBConnection;
const mongoClient = require('mongodb').MongoClient;
const url ='mongodb://localhost:27017';
const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
//we need to have article as a json object : {code :'mmm', design :'ddd'}
var FilesData = {
addFileData : function (fileData, callback){
    if(fileData != undefined){
        fileData.code  = fileData.code===undefined?shortid.generate():fileData.code;
    mongoClient.connect(url,function(err,clientDB){
        if(err)
         {
            callback(true,"ERROR_DATABASE_CONNECTION");
         }
        console.log('the client connected is ');
        console.log(clientDB)
        const db = clientDB.db(dbName);
        db.collection('FileData').insertOne(
            fileData
          ) ;
         // db.close();
         clientDB.close();
         callback(false,fileData);
    });

       //return true;  
}
//return false;
},
updateFileData: function (fileData, callback){
    const criteria = { "code" : fileData.code };
    const dataToUpdate = {"type" : fileData.type, "description" : fileData.description,
    "dateDelivraison" : fileData.dateDelivraison, "auteur" : fileData.auteur } ;
        mongoClient.connect(url,function(err,clientDB){
            if(err)
             {
                callback(true,"ERROR_DATABASE_CONNECTION");
             }
            console.log('the client connected is ');
            console.log(clientDB)
            const db = clientDB.db(dbName);
            db.collection('FileData').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                }
            ) ;
             clientDB.close();
             callback(false,fileData);
        });
    
    },
getListFilesData : function (callback)
{
    let listFilesData = [];
    mongoClient.connect(url,function(err,clientDB){
        if(err)
        {
            callback("ERROR_DATABASE_CONNECTION",[]);
        }else{
        const db = clientDB.db(dbName);   
   var cursor =  db.collection('FileData').find();
   cursor.forEach(function(fileData,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        listFilesData.push(fileData);
   }).then(function(){
    console.log('list Files Data',listFilesData);
    callback(null,listFilesData); 
   });
   
}
});
     //return [];
},
deleteFileData: function (codeFileData,callback){
    const criteria = { "code" : codeFileData };
        mongoClient.connect(url,function(err,clientDB){
            if(err)
             {
                callback(true,"ERROR_DATABASE_CONNECTION");
             }
            console.log('the client connected is ');
            console.log(clientDB)
            const db = clientDB.db(dbName);
            db.collection('FileData').deleteOne(
                criteria 
            ) ;
             clientDB.close();
             callback(false,codeFileData);
        });
},
getFilesDataWithCode: function (code)
{
   return  db.collection('FileData').findOne(
        
         { 'code' : code});                     
},
// to be enhanced later 
getFiltredListFilesData: function(fileData, callback){
    //const criteria = { "code" : fileData.code };
    const filter = {"type" : fileData.type, "description" : fileData.description,
    "dateDelivraison" : fileData.dateDelivraison, "auteur" : fileData.auteur } ;
        mongoClient.connect(url,function(err,clientDB){
            if(err)
             {
                callback(true,"ERROR_DATABASE_CONNECTION");
             }
            console.log('the client connected is ');
            console.log(clientDB)
            const db = clientDB.db(dbName);
           const list =  db.collection('FileData').find(
                criteria,
                { 
                    $get: filter
                }
            ) ;
             clientDB.close();
             callback(false,list);
        });
    
    }};
module.exports.FilesData = FilesData;