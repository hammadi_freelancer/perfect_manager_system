
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var bonsReceptions = require('../model/bons-receptions').BonsReceptions;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addBonReception',function(req,res,next){
    
    console.log(" ADD BON RECEPTION IS CALLED") ;
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = bonsReceptions.addBonReception(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,bonReceptionAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_BON_RECEPTION',
                error_content: err
            });
         }else{
       
        return res.json(bonReceptionAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateBonReception',function(req,res,next){
    
       console.log(" UPDATE BON RECEPTION IS CALLED") ;
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = bonsReceptions.updateBonReception(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,bonReceptionAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_BON_RECEPTION',
                error_content: err
            });
         }else{
       
        return res.json(bonReceptionAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getBonsReceptions',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
      console.log("GET LIST BONS RECEPTIONS IS CALLED");
    bonsReceptions.getListBonsReceptions(
        req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
        function(err,bonsReceptions){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_BONS_RECEPTIONS',
                 error_content: err
             });
          }else{
        
         return res.json(bonsReceptions);
          }
      });

  });
  router.get('/getPageBonsReceptions',function(req,res,next){
    
        var decoded = jwt.decode(req.headers.authorization.slice( 
            req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
      
          console.log("GET PAGE BONS RECEPTIONS IS CALLED");
        bonsReceptions.getPageBonsReceptions(
            req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
            function(err,listBonsReceptions){
              if(err){
                 return  res.json({
                     error_message : 'ERROR_GET_PAGE_BONS_RECEPTIONS',
                     error_content: err
                 });
              }else{
            
             return res.json(listBonsReceptions);
              }
          });
    
      });
  router.get('/getBonReception/:codeBonReception',function(req,res,next){
    console.log("GET BON RECEPTION IS CALLED");
  //  console.log(req.params['codeCredit']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    bonsReceptions.getBonReception(
        req.app.locals.dataBase,req.params['codeBonReception'],
        decoded.userData.code, function(err,bonReception){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_BON_RECEPTION',
               error_content: err
           });
        }else{
      
       return res.json(bonReception);
        }
    });
})

  router.delete('/deleteBonReception/:codeBonReception',function(req,res,next){
    
       console.log(" DELETE BON RECEPTION IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = bonsReceptions.deleteBonReception(
        req.app.locals.dataBase,req.params['codeBonReception'],decoded.userData.code, function(err,codeBonReception){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_BON_RECEPTION',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeBonReception']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });



 router.get('/getTotalBonsReceptions',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    bonsReceptions.getTotalBonsReceptions(
        req.app.locals.dataBase,decoded.userData.code,function(err,totalBonsReceptions){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_BONS_RECEPTIONS',
                 error_content: err
             });
          }else{
        
         return res.json({'totalBonsReceptions':totalBonsReceptions});
          }
      });

  });
  module.exports.routerBonsReceptions = router;