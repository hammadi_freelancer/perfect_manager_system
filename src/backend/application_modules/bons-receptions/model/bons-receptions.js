//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var BonsReceptions = {
addBonReception : function (db,bonReception,codeUser,callback){
    if(bonReception != undefined){
        bonReception.code  = utils.isUndefined(bonReception.code)?shortid.generate():bonReception.code;

        bonReception.dateOperationObject = utils.isUndefined(bonReception.dateOperationObject)? 
        new Date():bonReception.dateOperationObject;
        if(utils.isUndefined(bonReception.numeroBonReception))
            {
               const currentD =  moment(new Date());
              // const genereKey = shortid.generate();
              const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.day().toString()+
              shortid.generate();
              bonReception.numeroBonReception = 'BR' +generatedCode;
            }
       
            bonReception.userCreatorCode = codeUser;
            bonReception.userLastUpdatorCode = codeUser;
            bonReception.dateCreation = moment(new Date).format('L');
            bonReception.dateLastUpdate = moment(new Date).format('L');
            db.collection('BonReception').insertOne(
                bonReception,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
             // db.close();
             
    

}else{
callback(true,null);
}
},
updateBonReception: function (db,bonReception,codeUser, callback){

   
    bonReception.dateLastUpdate = moment(new Date).format('L');
    
    bonReception.dateOperationObject = utils.isUndefined(bonReception.dateOperationObject)? 
    new Date():bonReception.dateOperationObject;
    const criteria = { "code" : bonReception.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
    "dateOperationObject" : bonReception.dateOperationObject, 
    "dateDebutObject" : bonReception.dateDebutObject, 
    "dateFinObject" : bonReception.dateFinObject, 
     "fournisseur": bonReception.fournisseur,
     "montantTotal": bonReception.montantTotal,
     "marchandisesRecues": bonReception.marchandisesRecues,
     "montantTotalRegle": bonReception.montantTotalRegle,
     "lignesBonReception": bonReception.lignesBonReception,
     "etat": bonReception.etat,

       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": bonReception.dateLastUpdate, 
    } ;
            db.collection('BonReception').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },function(bonReception,err){
                    if(err)
                    callback(false,err);
                    else
                        callback(true,true);
                    
                    
                }
            ) ;
     
    
    },
getListBonsReceptions: function (db,codeUser,pageNumber, nbElementsPerPage , callback)
{
    let listBonsReceptions = [];
   
   var cursor =  db.collection('BonReception').find( 
       {"userCreatorCode" : codeUser}
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(bonReception,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        bonReception.dateOperation= moment(bonReception.dateOperationObject).format('L');
       // credit.dateDebutPayement = moment(credit.dateDebutPayementObject).format('L');
        
       listBonsReceptions.push(bonReception);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listBonsReceptions); 
   });
     //return [];
},
deleteBonReception: function (db,codeBonReception,codeUser,callback){
    const criteria = { "code" : codeBonReception, "userCreatorCode":codeUser };
    
    db.collection('BonReception').deleteOne(
        criteria , function(err,result){
            if(err){
                
                callback(null,null);
            }else{
                callback(false,result);
            }
        }
    ) ;
},

getBonReception: function (db,codeBonReception,codeUser,callback)
{
    const criteria = { "code" : codeBonReception, "userCreatorCode":codeUser };
  
       const bonL =  db.collection('BonReception').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
        // callback(false,client);
                 
},
getTotalBonsReceptions : function (db,codeUser, callback)
{
   var totalBonsReceptions =  db.collection('BonReception').find( 
       {"userCreatorCode" : codeUser}
    ).count(function(err,result){
        callback(null,result); 
    }
);
   


     //return [];
},
getPageBonsReceptions : function (db,codeUser,pageNumber, nbElementsPerPage , callback)
{
    let listBonsReceptions = [];
   
   var cursor =  db.collection('BonReception').find( 
       {"userCreatorCode" : codeUser}
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(bonReception,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        bonReception.dateOperation= moment(bonReception.dateOperationObject).format('L');
       // credit.dateDebutPayement = moment(credit.dateDebutPayementObject).format('L');
        
       listBonsReceptions.push(bonReception);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listBonsReceptions); 
   });
     //return [];
},
}
module.exports.BonsReceptions= BonsReceptions;