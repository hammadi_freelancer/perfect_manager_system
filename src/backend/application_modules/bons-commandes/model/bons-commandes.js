//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var BonsCommandes = {
addBonCommande : function (db,bonCommande,codeUser,callback){
    if(bonCommande != undefined){
        bonCommande.code  = utils.isUndefined(bonCommande.code)?shortid.generate():bonCommande.code;

        bonCommande.dateOperationObject = utils.isUndefined(bonCommande.dateOperationObject)? 
        new Date():bonCommande.dateOperationObject;
        if(utils.isUndefined(bonCommande.numeroBonCommande))
            {
               const currentD =  moment(new Date());
              // const genereKey = shortid.generate();
              const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.day().toString()+
              shortid.generate();
              bonCommande.numeroBonCommande = 'BC' +generatedCode;
            }
       
            bonCommande.userCreatorCode = codeUser;
            bonCommande.userLastUpdatorCode = codeUser;
            bonCommande.dateCreation = moment(new Date).format('L');
            bonCommande.dateLastUpdate = moment(new Date).format('L');
            db.collection('BonCommande').insertOne(
                bonCommande,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
             // db.close();
             
    

}else{
callback(true,null);
}
},
updateBonCommande: function (db,bonCommande,codeUser, callback){

   
    bonCommande.dateLastUpdate = moment(new Date).format('L');
    
    bonCommande.dateOperationObject = utils.isUndefined(bonCommande.dateOperationObject)? 
    new Date():bonCommande.dateOperationObject;
    const criteria = { "code" : bonCommande.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
    "dateOperationObject" : bonCommande.dateOperationObject, 
    "dateDebutObject" : bonCommande.dateDebutObject, 
    "dateFinObject" : bonCommande.dateFinObject, 
     "client": bonCommande.client,
     "montantTotal": bonCommande.montantTotal,
     "marchandisesLivrees": bonCommande.marchandisesLivrees,
     "montantTotalRegle": bonCommande.montantTotalRegle,
     "lignesBonCommande": bonCommande.lignesBonCommande,
     "etat": bonCommande.etat,

       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": bonCommande.dateLastUpdate, 
    } ;
            db.collection('BonCommande').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },function(bonCommande,err){
                    if(err)
                    callback(false,err);
                    else
                        callback(true,true);
                    
                    
                }
            ) ;
     
    
    },
getListBonsCommandes: function (db,codeUser,pageNumber, nbElementsPerPage , callback)
{
    let listBonsCommandes = [];
   
   var cursor =  db.collection('BonCommande').find( 
       {"userCreatorCode" : codeUser}
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(bonCommande,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        bonCommande.dateOperation= moment(bonCommande.dateOperationObject).format('L');
       // credit.dateDebutPayement = moment(credit.dateDebutPayementObject).format('L');
        
       listBonsCommandes.push(bonCommande);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listBonsCommandes); 
   });
     //return [];
},
deleteBonCommande: function (db,codeBonCommande,codeUser,callback){
    const criteria = { "code" : codeBonCommande, "userCreatorCode":codeUser };
    
    db.collection('BonCommande').deleteOne(
        criteria , function(err,result){
            if(err){
                
                callback(null,null);
            }else{
                callback(false,result);
            }
        }
    ) ;
},

getBonCommande: function (db,codeBonCommande,codeUser,callback)
{
    const criteria = { "code" : codeBonCommande, "userCreatorCode":codeUser };
  
       const bonL =  db.collection('BonCommande').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
        // callback(false,client);
                 
},
getTotalBonsCommandes : function (db,codeUser, callback)
{
   var totalBonsCommandes =  db.collection('BonCommande').find( 
       {"userCreatorCode" : codeUser}
    ).count(function(err,result){
        callback(null,result); 
    }
);
   


     //return [];
},
getPageBonsCommandes : function (db,codeUser,pageNumber, nbElementsPerPage , callback)
{
    let listBonsCommandes = [];
   
   var cursor =  db.collection('BonCommande').find( 
       {"userCreatorCode" : codeUser}
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(bonCommande,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        bonCommande.dateOperation= moment(bonCommande.dateOperationObject).format('L');
       // credit.dateDebutPayement = moment(credit.dateDebutPayementObject).format('L');
        
       listBonsCommandes.push(bonCommande);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listBonsCommandes); 
   });
     //return [];
},
}
module.exports.BonsCommandes= BonsCommandes;