
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var bonsCommandes = require('../model/bons-commandes').BonsCommandes;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addBonCommande',function(req,res,next){
    
    console.log(" ADD BON COMMANDE IS CALLED") ;
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = bonsCommandes.addBonCommande(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,bonCommandeAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_BON_COMMANDE',
                error_content: err
            });
         }else{
       
        return res.json(bonCommandeAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateBonCommande',function(req,res,next){
    
       console.log(" UPDATE BON COMMANDE IS CALLED") ;
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = bonsCommandes.updateBonCommande(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,bonCommandeAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_BON_COMMANDE',
                error_content: err
            });
         }else{
       
        return res.json(bonCommandeAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getBonsCommandes',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
      console.log("GET LIST BONS COMMANDES IS CALLED");
    bonsCommandes.getListBonsCommandes(
        req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
        function(err,bonsCommandes){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_BONS_COMMANDES',
                 error_content: err
             });
          }else{
        
         return res.json(bonsCommandes);
          }
      });

  });
  router.get('/getPageBonsCommandes',function(req,res,next){
    
        var decoded = jwt.decode(req.headers.authorization.slice( 
            req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
      
          console.log("GET PAGE BONS COMMANDES IS CALLED");
        bonsCommandes.getPageBonsCommandes(
            req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
            function(err,listBonsCommandes){
              if(err){
                 return  res.json({
                     error_message : 'ERROR_GET_PAGE_BONS_COMMANDES',
                     error_content: err
                 });
              }else{
            
             return res.json(listBonsCommandes);
              }
          });
    
      });
  router.get('/getBonCommande/:codeBonCommande',function(req,res,next){
    console.log("GET BON COMMANDE IS CALLED");
  //  console.log(req.params['codeCredit']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    bonsCommandes.getBonCommande(
        req.app.locals.dataBase,req.params['codeBonCommande'],
        decoded.userData.code, function(err,bonCommande){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_BON_COMMANDE',
               error_content: err
           });
        }else{
      
       return res.json(bonCommande);
        }
    });
})

  router.delete('/deleteBonCommande/:codeBonCommande',function(req,res,next){
    
       console.log(" DELETE BON COMMANDE IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = bonsCommandes.deleteBonCommande(
        req.app.locals.dataBase,req.params['codeBonCommande'],decoded.userData.code, function(err,codeBonCommande){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_BON_COMMANDE',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeBonCommande']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });



 router.get('/getTotalBonsCommandes',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    bonsCommandes.getTotalBonsCommandes(
        req.app.locals.dataBase,decoded.userData.code,function(err,totalBonsCommandes){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_BONS_COMMANDES',
                 error_content: err
             });
          }else{
        
         return res.json({'totalBonsCommandes':totalBonsCommandes});
          }
      });

  });
  module.exports.routerBonsCommandes = router;