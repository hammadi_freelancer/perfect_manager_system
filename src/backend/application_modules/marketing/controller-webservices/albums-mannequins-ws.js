
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var albumsMannequins = require('../model/albums-mannequins').AlbumsMannequins;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addAlbumMannequin',function(req,res,next){
    
       console.log(" ADD ALBUM MANNEQUIN  IS CALLED") ;
       console.log(" THE ALBUM MANNEQUIN  TO ADDED IS :",req.body);
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = albumsMannequins.addAlbumMannequin(req.app.locals.dataBase,req.body,decoded.userData.code,function(err,albumAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_ALBUM_MANNEQUIN',
                error_content: err
            });
         }else{
       
        return res.json(albumAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateAlbumMannequin',function(req,res,next){
    
       console.log(" UPDATE ALBUM MANNEQUIN IS CALLED") ;
       // console.log(" THE DOCUMENT CREDIT  TO UPDATE IS :",req.body);
    
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result =albumsMannequins.updateAlbumMannequin(req.app.locals.dataBase,req.body,decoded.userData.code, function(err,albumUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_ALBUM_MANNEQUIN',
                error_content: err
            });
         }else{
       
        return res.json(albumUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getAlbumsMannequins',function(req,res,next){
 
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    albumsMannequins.getListAlbumsMannequins(req.app.locals.dataBase,decoded.userData.code, function(err,listAlbumsMannequins){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_ALBUMS_MANNEQUINS',
                 error_content: err
             });
          }else{
        
         return res.json(listAlbumsMannequins);
          }
      });

  });
  router.get('/getAlbumMannequin/:codeAlbumMannequin',function(req,res,next){
    console.log("GET ALBUM MANNEQUIN IS CALLED");
   
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    albumsMannequins.getAlbumMannequin(req.app.locals.dataBase,req.params['codeAlbumMannequin'],decoded.userData.code,
     function(err,albumMannequin){
      // console.log("GET  DOCUMENT CREDIT : RESULT");
       //console.log(documentCredit);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_ALBUM_MANNEQUIN',
               error_content: err
           });
        }else{
      
       return res.json(albumMannequin);
        }
    });
})

  router.delete('/deleteAlbumMannequin/:codeAlbumMannequin',function(req,res,next){
    
       console.log(" DELETE ALBUM MANNEQUIN  IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = albumsMannequins.deleteAlbumMannequin(req.app.locals.dataBase,req.params['codeAlbumMannequin'],
       decoded.userData.code,
        function(err,codeAlbumMannequin){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_ALBUM_MANNEQUIN',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeAlbumMannequin']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getAlbumsMannequinsByCodeMannequin/:codeMannequin',function(req,res,next){
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    albumsMannequins.getListAlbumsMannequinsByCodeMannequin(req.app.locals.dataBase,decoded.userData.code,req.params['codeAlbumMannequin'],
         function(err,listAlbumsMannequins){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_ALBUMS_MANNEQUINS',
                 error_content: err
             });
          }else{
        
         return res.json(listAlbumsMannequins);
          }
      });

  });
  module.exports.routerAlbumsMannequins = router;