
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var compagnesPublicitaires = require('../model/compagnes-publicitaires').CompagnesPublicitaires;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addCompagnePublicitaire',function(req,res,next){
    
    console.log(" ADD COMPAGNE PUBLICITAIRE IS CALLED") ;
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = compagnesPublicitaires.addCompagnePublicitaire(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,compgneAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_COMPAGNE_PUBLICITAIRE',
                error_content: err
            });
         }else{
       
        return res.json(compgneAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateCompagnePublicitaire',function(req,res,next){
    
       console.log(" UPDATE COMPAGNE PUBLICITAIRE  IS CALLED") ;
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = compagnesPublicitaires.updateCompagnePublicitaire(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,compagneUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_COMPAGNE_PUBLICITAIRE',
                error_content: err
            });
         }else{
       
        return res.json(compagneUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getCompagnesPublicitaires',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
      // console.log("GET LIST PROFILES  ACHATS IS CALLED");
    compagnesPublicitaires.getListCompagnesPublicitaires(
        req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
        req.query.filter,  function(err,listComps){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_COMPAGNES_PUBLICITAIRES',
                 error_content: err
             });
          }else{
        
         return res.json(listComps);
          }
      });

  });
  router.get('/getPageCompagnesPublicitaires',function(req,res,next){
    
        var decoded = jwt.decode(req.headers.authorization.slice( 
            req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
      
         //  console.log("GET PAGE PROFILES ACHATS IS CALLED");

          compagnesPublicitaires.getPageCompagnesPublicitaires(
            req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
            req.query.filter, function(err,listComps){
              if(err){
                 return  res.json({
                     error_message : 'ERROR_GET_PAGE_COMPAGNES_PUBLICITAIRES',
                     error_content: err
                 });
              }else{
            
             return res.json(listComps);
              }
          });
    
      });
  router.get('/getCompagnePublicitaire/:codeCompagnePublicitaire',function(req,res,next){
    console.log("GET COMPAGNE PUBLICITAIRE  IS CALLED");
  //  console.log(req.params['codeCredit']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
        compagnesPublicitaires.getCompagnePublicitaire(
        req.app.locals.dataBase,req.params['codeCompagnePublicitaire'],
        decoded.userData.code, function(err,compagne){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_COMPAGNE_PUBLICITAIRE',
               error_content: err
           });
        }else{
      
       return res.json(compagne);
        }
    });
})

  router.delete('/deleteCompagnePublicitaire/:codeCompagnePublicitaire',function(req,res,next){
    
       console.log(" DELETE COMPAGNE IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = compagnesPublicitaires.deleteCompagnePublicitaire(
        req.app.locals.dataBase,req.params['codeCompagnePublicitaire'],decoded.userData.code, function(err,codeCompagnePublicitaire){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_COMPAGNE_PUBLICITAIRE',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeCompagnePublicitaire']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });



 router.get('/getTotalCompagnesPublicitaires',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
        compagnesPublicitaires.getTotalCompagnesPublicitaires(
        req.app.locals.dataBase,decoded.userData.code,req.query.filter,
        function(err,totalComps){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_COMPAGNES_PUBLICITAIRES',
                 error_content: err
             });
          }else{
        
         return res.json({'totalCompagnesPublicitaires':totalComps});
          }
      });

  });
  module.exports.routerCompagnesPublicitaires = router;