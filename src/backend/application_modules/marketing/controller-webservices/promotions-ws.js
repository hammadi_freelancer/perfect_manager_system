
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var promotions = require('../model/promotions').Promotions;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addPromotion',function(req,res,next){
    
    console.log(" ADD PROMOTION IS CALLED") ;
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = promotions.addPromotion(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,promotionAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_PROMOTION',
                error_content: err
            });
         }else{
       
        return res.json(promotionAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updatePromotion',function(req,res,next){
    
       console.log(" UPDATE PROMOTION  IS CALLED") ;
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = promotions.updatePromotion(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,promotionUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_PROMOTION',
                error_content: err
            });
         }else{
       
        return res.json(promotionUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getPromotions',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
      console.log("GET LIST PROMOTIONS IS CALLED");
    promotions.getListPromotions(
        req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
        req.query.filter,  function(err,listProms){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_PROMOTIONS',
                 error_content: err
             });
          }else{
        
         return res.json(listProms);
          }
      });

  });
  router.get('/getPagePromotions',function(req,res,next){
    
        var decoded = jwt.decode(req.headers.authorization.slice( 
            req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
          console.log("GET PAGE PROMOTIONS IS CALLED");

        promotions.getPagePromotions(
            req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
            req.query.filter, function(err,listPromotions){
              if(err){
                 return  res.json({
                     error_message : 'ERROR_GET_PAGE_PROMOTIONS',
                     error_content: err
                 });
              }else{
            
             return res.json(listPromotions);
              }
          });
    
      });
  router.get('/getPromotion/:codePromotion',function(req,res,next){
    console.log("GET PROMOTION  IS CALLED");
  //  console.log(req.params['codeCredit']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
            promotions.getPromotion(
        req.app.locals.dataBase,req.params['codePromotion'],
        decoded.userData.code, function(err,promotion){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_PROMOTION',
               error_content: err
           });
        }else{
      
       return res.json(promotion);
        }
    });
})

  router.delete('/deletePromotion/:codePromotion',function(req,res,next){
    
       console.log(" DELETE PROMOTION  IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = promotions.deletePromotion(
        req.app.locals.dataBase,req.params['codePromotion'],decoded.userData.code, function(err,codePromotion){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_PROMOTION',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codePromotion']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });



 router.get('/getTotalPromotions',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    promotions.getTotalPromotions(
        req.app.locals.dataBase,decoded.userData.code,req.query.filter,
        function(err,totalPromotions){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_PROMOTIONS',
                 error_content: err
             });
          }else{
        
         return res.json({'totalPromotions':totalPromotions});
          }
      });

  });
  module.exports.routerPromotions = router;