
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var mannequins = require('../model/mannequins').Mannequins;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addMannequin',function(req,res,next){
    
    console.log(" ADD MANNEQUIN IS CALLED") ;
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = mannequins.addMannequin(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,mannequinAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_MANNEQUIN',
                error_content: err
            });
         }else{
       
        return res.json(mannequinAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateMannequin',function(req,res,next){
    
       console.log(" UPDATE MANNEQUIN  IS CALLED") ;
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = mannequins.updateMannequin(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,mannequinUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_MANNEQUIN',
                error_content: err
            });
         }else{
       
        return res.json(mannequinUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getMannequins',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
      console.log("GET LIST MANNEQUINS IS CALLED");
    mannequins.getListMannequins(
        req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
        req.query.filter,  function(err,listProms){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_MANNEQUINS',
                 error_content: err
             });
          }else{
        
         return res.json(listProms);
          }
      });

  });
  router.get('/getPageMannequins',function(req,res,next){
    
        var decoded = jwt.decode(req.headers.authorization.slice( 
            req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
          console.log("GET PAGE MANNEQUINS IS CALLED");

        mannequins.getPageMannequins(
            req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
            req.query.filter, function(err,listMannequins){
              if(err){
                 return  res.json({
                     error_message : 'ERROR_GET_PAGE_MANNEQUINS',
                     error_content: err
                 });
              }else{
            
             return res.json(listMannequins);
              }
          });
    
      });
  router.get('/getMannequin/:codeMannequin',function(req,res,next){
    console.log("GET MANNEQUIN  IS CALLED");
  //  console.log(req.params['codeCredit']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
            mannequins.getMannequin(
        req.app.locals.dataBase,req.params['codeMannequin'],
        decoded.userData.code, function(err,mannequin){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_MANNEQUIN',
               error_content: err
           });
        }else{
      
       return res.json(mannequin);
        }
    });
})

  router.delete('/deleteMannequin/:codeMannequin',function(req,res,next){
    
       console.log(" DELETE MANNEQUIN  IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = mannequins.deleteMannequin(
        req.app.locals.dataBase,req.params['codeMannequin'],decoded.userData.code, function(err,codeMannequin){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_MANNEQUIN',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeMannequin']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });



 router.get('/getTotalMannequins',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    mannequins.getTotalMannequins(
        req.app.locals.dataBase,decoded.userData.code,req.query.filter,
        function(err,totalMannequins){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_MANNEQUINS',
                 error_content: err
             });
          }else{
        
         return res.json({'totalMannequins':totalMannequins});
          }
      });

  });
  module.exports.routerMannequins = router;