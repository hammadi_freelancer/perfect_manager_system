
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var documentsMannequins = require('../model/documents-mannequins').DocumentsMannequins;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addDocumentMannequin',function(req,res,next){
    
       console.log(" ADD DOCUMENT MANNEQUIN  IS CALLED") ;
       console.log(" THE DOCUMENT MANNEQUIN  TO ADDED IS :",req.body);
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = documentsMannequins.addDocumentMannequin(req.app.locals.dataBase,req.body,decoded.userData.code,function(err,documentmAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_DOCUMENT_MANNEQUIN',
                error_content: err
            });
         }else{
       
        return res.json(documentmAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateDocumentMannequin',function(req,res,next){
    
       console.log(" UPDATE DOCUMENT MANNEQUIN IS CALLED") ;
       // console.log(" THE DOCUMENT CREDIT  TO UPDATE IS :",req.body);
    
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result =documentsMannequins.updateDocumentMannequin(req.app.locals.dataBase,req.body,decoded.userData.code, function(err,documentmUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_DOCUMENT_MANNEQUIN',
                error_content: err
            });
         }else{
       
        return res.json(documentmUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getDocumentsMannequins',function(req,res,next){
 
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    documentsMannequins.getListDocumentsMannequins(req.app.locals.dataBase,decoded.userData.code, function(err,listDocumentsMannequins){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_MANNEQUINS',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsMannequins);
          }
      });

  });
  router.get('/getDocumentMannequin/:codeDocumentMannequin',function(req,res,next){
    console.log("GET DOCUMENT MANNEQUIN IS CALLED");
   
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    documentsMannequins.getDocumentMannequin(req.app.locals.dataBase,req.params['codeDocumentMannequin'],decoded.userData.code,
     function(err,documentMannequin){
      // console.log("GET  DOCUMENT CREDIT : RESULT");
       //console.log(documentCredit);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_DOCUMENT_MANNEQUIN',
               error_content: err
           });
        }else{
      
       return res.json(documentMannequin);
        }
    });
})

  router.delete('/deleteDocumentMannequin/:codeDocumentMannequin',function(req,res,next){
    
       console.log(" DELETE DOCUMENT MANNEQUIN  IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = documentsMannequins.deleteDocumentMannequin(req.app.locals.dataBase,req.params['codeDocumentMannequin'],
       decoded.userData.code,
        function(err,codeDocumentMannequin){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_DOCUMENT_MANNEQUIN',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeDocumentMannequin']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getDocumentsMannequinsByCodeMannequin/:codeMannequin',function(req,res,next){
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    documentsMannequins.getListDocumentsMannequinsByCodeMannequin(req.app.locals.dataBase,decoded.userData.code,req.params['codeMannequin'],
         function(err,listDocumentsMannequins){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_MANNEQUINS',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsMannequins);
          }
      });

  });
  module.exports.routerDocumentsMannequins = router;