
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var paiementsMannequins = require('../model/paiements-mannequins').PaiementsMannequins;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addPaiementMannequin',function(req,res,next){
    
       console.log(" ADD PAIEMENT MANNEQUIN   IS CALLED") ;
       console.log(" THE PAIEMENT MANNEQUIN  TO ADDED IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = paiementsMannequins.addPaiementMannequin(req.app.locals.dataBase,req.body,decoded.userData.code,
        function(err,paiementMannequinAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_PAIEMENT_MANNEQUIN',
                error_content: err
            });
         }else{
       
        return res.json(paiementMannequinAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updatePaiementMannequin',function(req,res,next){
    
       console.log(" UPDATE PAIEMENT MANNEQUIN   IS CALLED") ;
       console.log("THE PAIEMENT MANNEQUIN TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = paiementsMannequins.updatePaiementMannequin(
        req.app.locals.dataBase,req.body,decoded.userData.code,
         function(err,paiementMannequinUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_PAIEMENT_MANNEQUIN',
                error_content: err
            });
         }else{
       
        return res.json(paiementMannequinUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getPaiementsMannequins',function(req,res,next){
 
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    paiementsMannequins.getListPaiementsMannequins(
        req.app.locals.dataBase,decoded.userData.code, function(err,listPaiementsMannequins){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_PAIEMENTS_MANNEQUINS',
                 error_content: err
             });
          }else{
        
         return res.json(listPaiementsMannequins);
          }
      });

  });
  router.get('/getPaiementMannequin/:codePaiementMannequin',function(req,res,next){
    console.log("GET PAIEMENT MANNEQUIN  IS CALLED");
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    paiementsMannequins.getPaiementMannequin(
        req.app.locals.dataBase,req.params['codePaiementMannequin'],decoded.userData.code,
     function(err,paiementMannequin){
       console.log("GET  PAIEMENT MANNEQUIN  : RESULT");
       console.log(paiementMannequin);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_PAIEMENT_MANNEQUIN',
               error_content: err
           });
        }else{
      
       return res.json(paiementMannequin);
        }
    });
})

  router.delete('/deletePaiementMannequin/:codePaiementMannequin',function(req,res,next){
    
       console.log(" DELETE PAIEMENT MANNEQUIN IS CALLED") ;
       console.log(" THE PAIEMENT MANNEQUIN TO DELETE IS :",req.params['codePaiementMannequin']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = paiementsMannequins.deletePaiementMannequin(
        req.app.locals.dataBase,req.params['codePaiementMannequin'],decoded.userData.code,
        function(err,codePaiementMannequin){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_PAIEMENT_MANNEQUIN',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codePaiementMannequin']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

   router.get('/getPaiementsMannequinsByCodeMannequin/:codeMannequin',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    paiementsMannequins.getListPaiementsMannequins(
        req.app.locals.dataBase,req.params['codeMannequin'],decoded.userData.code, function(err,listPaiementsMannequins){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_PAIEMENTS_MANNEQUINS_BY_CODE_MANNEQUIN',
                 error_content: err
             });
          }else{
        
         return res.json(listPaiementsMannequins);
          }
      });

  })

  module.exports.routerPaiementsMannequins = router;