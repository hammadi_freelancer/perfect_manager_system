//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var DocumentsMannequins = {
addDocumentMannequin : function (db,documentMannequin,codeUser,callback){
    if(documentMannequin != undefined){
        documentMannequin.code  = utils.isUndefined(documentMannequin.code)?shortid.generate():documentMannequin.code;
        documentMannequin.dateReceptionObject = utils.isUndefined(documentMannequin.dateReceptionObject)? 
        new Date():documentMannequin.dateReceptionObject;
        documentMannequin.userCreatorCode = codeUser;
        documentMannequin.userLastUpdatorCode = codeUser;
        documentMannequin.dateCreation = moment(new Date).format('L');
        documentMannequin.dateLastUpdate = moment(new Date).format('L');
    
            db.collection('DocumentMannequin').insertOne(
                documentMannequin,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,documentMannequin);
                   }
                }
              ) ;
   

}else{
callback(true,null);
}
},
updateDocumentMannequin: function (db,documentMannequin,codeUser, callback){

   
    documentMannequin.dateReceptionObject = utils.isUndefined(documentMannequin.dateReceptionObject)? 
    new Date():documentMannequin.dateReceptionObject;

    const criteria = { "code" : documentMannequin.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"dateReceptionObject" : documentMannequin.dateReceptionObject, 
    "codeMannequin" : documentMannequin.codeMannequin, 
    "description" : documentMannequin.description,
     "codeOrganisation" : documentMannequin.codeOrganisation,
     "typeDocument" : documentMannequin.typeDocument, // cheque , virement , espece 
     "numeroDocument" : documentMannequin.numeroDocument, 
     "imageFace1":documentMannequin.imageFace1,
     "imageFace2" : documentMannequin.imageFace2 , 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": documentMannequin.dateLastUpdate, 
    } ;
       
            db.collection('DocumentMannequin').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
    
    },
getListDocumentsMannequins : function (db,codeUser,callback)
{
    let listDocumentsMannequins = [];
   
   var cursor =  db.collection('DocumentMannequin').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(docMannequin,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        docMannequin.dateReception = moment(docMannequin.dateReceptionObject).format('L');
        
        listDocumentsMannequins.push(docMannequin);
   }).then(function(){
    //console.log('list documents credits',listDocumentsCredits);
    callback(null,listDocumentsMannequins); 
   });
   

},
deleteDocumentMannequin: function (db,codeDocumentMannequin,codeUser,callback){
    const criteria = { "code" : codeDocumentMannequin, "userCreatorCode":codeUser };
      
            db.collection('DocumentMannequin').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;

},

getDocumentMannequin: function (db,codeDocumentMannequin,codeUser,callback)
{
    const criteria = { "code" : codeDocumentMannequin, "userCreatorCode":codeUser };

       const doc =  db.collection('DocumentMannequin').findOne(
            criteria , function(err,result){
                if(err){
                   
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                  
},


getListDocumentsMannequinsByCodeMannequin : function (db,codeUser,codeMannequin,callback)
{
    let listDocumentsMannequins = [];
  
   var cursor =  db.collection('DocumentMannequin').find( 
       {"userCreatorCode" : codeUser, "codeMannequin": codeMannequin}
    );
   cursor.forEach(function(documentRV,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        documentRV.dateReception = moment(documentRV.dateReceptionObject).format('L');
        
        listDocumentsMannequins.push(documentRV);
   }).then(function(){
  // console.log('list doc credits',listCredits);
    callback(null,listDocumentsMannequins); 
   });

},


}
module.exports.DocumentsMannequins = DocumentsMannequins;