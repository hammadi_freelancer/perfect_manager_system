//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var AlbumsMannequins = {
addAlbumMannequin : function (db,albumMannequin,codeUser,callback){
    if(albumMannequin != undefined){
        albumMannequin.code  = utils.isUndefined(albumMannequin.code)?shortid.generate():albumMannequin.code;
        albumMannequin.dateOperationObject = utils.isUndefined(albumMannequin.dateOperationObject)? 
        new Date():albumMannequin.dateOperationObject;
        albumMannequin.userCreatorCode = codeUser;
        albumMannequin.userLastUpdatorCode = codeUser;
        albumMannequin.dateCreation = moment(new Date).format('L');
        albumMannequin.dateLastUpdate = moment(new Date).format('L');
            db.collection('AlbumMannequin').insertOne(
                albumMannequin,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,albumMannequin);
                   }
                }
              ) ;


}else{
callback(true,null);
}
},
updateAlbumMannequin: function (db,albumMannequin,codeUser, callback){

   
    albumMannequin.dateOperationObject = utils.isUndefined(albumMannequin.dateOperationObject)? 
    new Date():albumMannequin.dateOperationObject;
    albumMannequin.dateLastUpdate = moment(new Date).format('L');
    
    const criteria = { "code" : albumMannequin.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
    "description" : albumMannequin.description,
     "codeOrganisation" : albumMannequin.codeOrganisation,
    "dateCreationObject" : albumMannequin.dateCreationObject ,
     "titre" : albumMannequin.titre, // cheque , virement , espece 
     "classe" : albumMannequin.classe,
     "photo1" : albumMannequin.photo1,
     "photo2" : albumMannequin.photo2,
     "photo3" : albumMannequin.photo3,
     "photo4" : albumMannequin.photo4,
     "photo5" : albumMannequin.photo5,
     "photo6" : albumMannequin.photo6,
     "imprime" : albumMannequin.imprime,
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": paiementMannequin.dateLastUpdate, 
    } ;
       
            db.collection('AlbumMannequin').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
      
    
    },
getListAlbumsMannequins : function (db,codeUser,callback)
{
    let listAlbumsMannequins  = [];
    
   var cursor =  db.collection('AlbumMannequin').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(albumMannequin,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        albumMannequin.dateCreation = moment(albumMannequin.dateCreationObject).format('L');
        
        listAlbumsMannequins.push(albumMannequin);
   }).then(function(){
  //  console.log('list credits',listPayementsFacturesVentes);
    callback(null,listPayementsRelevesVentes); 
   });
},
deleteAlbumMannequin: function (db,codeAlbumMannequin,codeUser,callback){
    const criteria = { "code" : codeAlbumMannequin, "userCreatorCode":codeUser };
       
            db.collection('AlbumMannequin').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
   
},

getAlbumMannequin: function (db,codeAlbumMannequin,codeUser,callback)
{
    const criteria = { "code" : codeAlbumMannequin, "userCreatorCode":codeUser };
  
       const albumMannequin =  db.collection('AlbumMannequin').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                     
},

getListAlbumsMannequinsByCodeMannequin: function (db,codeMannequin, codeUser,callback)
{
    let listAlbumsMannequins = [];
  
   var cursor =  db.collection('AlbumMannequin').find( 
       {"userCreatorCode" : codeUser, "codeMannequin": codeMannequin}
    );
   cursor.forEach(function(albumMannequin,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        albumMannequin.dateCreation = moment(albumMannequin.dateCreationObject).format('L');
        
        listAlbumsMannequins.push(albumMannequin);
   }).then(function(){
   // console.log('list credits',listCredits);
    callback(null,listAlbumsMannequins); 
   });

},
}
module.exports.AlbumsMannequins = AlbumsMannequins;