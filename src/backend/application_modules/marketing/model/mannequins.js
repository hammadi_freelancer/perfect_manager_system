//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var Mannequins = {
addMannequin: function (db,mannequin,codeUser,callback){
    if(mannequin != undefined){
        mannequin.code  = utils.isUndefined(mannequin.code)?shortid.generate():mannequin.code;

    
        mannequin.userCreatorCode = codeUser;
        mannequin.userLastUpdatorCode = codeUser;
        mannequin.dateCreation = moment(new Date).format('L');
        mannequin.dateLastUpdate = moment(new Date).format('L');
            db.collection('Mannequin').insertOne(
                mannequin,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
             // db.close();
             
    

}else{
callback(true,null);
}
},
updateMannequin: function (db,mannequin,codeUser, callback){
  
    const criteria = { "code" : mannequin.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
   "description" :mannequin.description,
   "nom" :mannequin.nom,
   "prenom" :mannequin.prenom,
   "dateNaissanceObject" :mannequin.dateNaissanceObject,
   "mobile" :mannequin.mobile,
   "email" :mannequin.email,
   "adresse" :mannequin.adresse,
   "cin" :mannequin.cin,
    "album" : mannequin.album,
    "imageProfile" : mannequin.imageProfile,
    "taille" : mannequin.taille,
    "shoes" : mannequin.shoes,
    "eyesColor" : mannequin.eyesColor,
    "hairColor" : mannequin.hairColor,
    "skinColor" : mannequin.skin,
    "principalDomain" :mannequin.principalDomain,
    "experienced" :mannequin.experienced,
    "experiences" :mannequin.experiences,
      "etat": mannequin.etat,// ouvert/initial/en cours de validation /validé /en cours de validation /cloturé
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": mannequin.dateLastUpdate, 
    
    } 
            db.collection('Mannequin').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },function(mannequin,err){
                    if(err)
                    callback(false,err);
                    else
                        callback(true,true);
                    
                    
                }
            ) ;
     
    
    },
getListMannequins: function (db,codeUser,pageNumber, nbElementsPerPage ,filter, callback)
{
    
    let listMannequins = [];
    
   //console.log('filter recieved is :',filter);
   let filterObject = JSON.parse(filter);
  
    let filterData = {
        "userCreatorCode" : codeUser
    };
    const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
    
    var cursor =  db.collection('Mannequin').find( 
     conditionBuilt
     ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
     .limit(Number(nbElementsPerPage) );
    cursor.forEach(function(mannequin,err){
        if(err)
         {
             console.log('errors produced :', err);
             callback(null,false); 
             
         }
        // credit.dateDebutPayement = moment(credit.dateDebutPayementObject).format('L');
         
        listMannequins.push(mannequin);
    }).then(function(){
     // console.log('list credits',listCredits);
     callback(null,listMannequins); 
    });
      //return [];
},

getTotalMannequins : function (db,codeUser,filter, callback)
{
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
   var totalMannequins=  db.collection('Mannequin').find( 
    conditionBuilt
).count(function(err,result){
        callback(null,result); 
    }
);
   


     //return [];
},
getPageMannequins : function (db,codeUser,pageNumber, nbElementsPerPage ,filter, callback)
{
    let listMannequins= [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
      console.log('condition',conditionBuilt);
      
      
   var cursor =  db.collection('Mannequin').find( conditionBuilt
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(mannequin,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        listMannequins.push(mannequin);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listMannequins); 
   });
     //return [];
},
buildFilterCondition(filterObject,filterData)
{
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.description))
        {
                filterData["description"] = filterObject.description;
        }
        if(!utils.isUndefined(filterObject.code))
            {
                    filterData["code"] = filterObject.code;
            }
            if(!utils.isUndefined(filterObject.nom))
                {
                        filterData["nom"] = filterObject.nom;
                }
                if(!utils.isUndefined(filterObject.prenom))
                    {
                            filterData["prenom"] = filterObject.prenom;
                    }
                if(!utils.isUndefined(filterObject.cin))
             {
                                filterData["cin"] = filterObject.cin;
             }
             if(!utils.isUndefined(filterObject.adresse))
                {
                                   filterData["adresse"] = filterObject.adresse;
                }

        if(!utils.isUndefined(filterObject.email))
             {
                                       filterData["email"] = filterObject.email;
            }
            if(!utils.isUndefined(filterObject.dateNaissanceFromObject))
                {
                                          filterData["dateNaissanceObject"] = {$gt: filterObject.dateNaissanceFromObject};
               }
        if(!utils.isUndefined(filterObject.dateNaissanceToObject))
         {
                        filterData["dateNaissanceObject"] = {$lt: filterObject.dateNaissanceToObject};
         }
         if(!utils.isUndefined(filterObject.taille))
            {
                           filterData["taille"] = filterObject.taille;
            }
            if(!utils.isUndefined(filterObject.shoes))
                {
                               filterData["shoes"] = filterObject.shoes;
                }
                if(!utils.isUndefined(filterObject.eyesColor))
                    {
                                   filterData["eyesColor"] = filterObject.eyesColor;
                    }
         if(!utils.isUndefined(filterObject.skinColor))
            {
                        filterData["skinColor"] = filterObject.skinColor;
        }
        if(!utils.isUndefined(filterObject.hairColor))
            {
                        filterData["hairColor"] = filterObject.hairColor;
        }   
        }
        
     return filterData;
 },
deleteMannequin: function (db,codeMannequin,codeUser,callback){
    const criteria = { "code" : codeJournal, "userCreatorCode":codeUser };
    
    db.collection('Mannequin').deleteOne(
        criteria , function(err,result){
            if(err){
                
                callback(null,null);
            }else{
                callback(false,result);
            }
        }
    ) ;
},

getMannequin: function (db,codeMannequin,codeUser,callback)
{
    const criteria = { "code" : codeMannequin, "userCreatorCode":codeUser };
  
       const mannequin =  db.collection('Mannequin').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
        // callback(false,client);
                 
}
}
module.exports.Mannequins = Mannequins;