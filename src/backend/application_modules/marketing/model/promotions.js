//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var Promotions = {
addPromotion: function (db,promotion,codeUser,callback){
    if(profileAchats != undefined){
        promotion.code  = utils.isUndefined(promotion.code)?shortid.generate():promotion.code;

        promotion.datePromotionObject = utils.isUndefined(promotion.datePromotionObject)? 
        new Date():promotion.datePromotionObject;
        promotion.userCreatorCode = codeUser;
        promotion.userLastUpdatorCode = codeUser;
        promotion.dateCreation = moment(new Date).format('L');
        promotion.dateLastUpdate = moment(new Date).format('L');
            db.collection('Promotion').insertOne(
                promotion,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
             // db.close();
             
    

}else{
callback(true,null);
}
},
updatePromotion: function (db,promotion,codeUser, callback){
  
    const criteria = { "code" : promotion.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
   "description" :promotion.description,
   "periodeFromObject" :promotion.periodeFromObject,
   "periodeToObject" :promotion.periodeToObject,
   "article" :promotion.article,
   "affiche1" :promotion.affiche1,
   "affiche2" :promotion.affiche2,
   
   "pourcentageRemise" :promotion.pourcentageRemise,
   "ancienPrix" :promotion.ancienPrix,
   "nouveauPrix" :promotion.nouveauPrix,
      "etat": promotion.etat,// ouvert/initial/en cours de validation /validé /en cours de validation /cloturé
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": promotion.dateLastUpdate, 
    
    } 
            db.collection('Promotion').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },function(promotion,err){
                    if(err)
                    callback(false,err);
                    else
                        callback(true,true);
                    
                    
                }
            ) ;
     
    
    },
getListPromotions: function (db,codeUser,pageNumber, nbElementsPerPage ,filter, callback)
{
    
    let listPromotions = [];
    
   //console.log('filter recieved is :',filter);
   let filterObject = JSON.parse(filter);
  
    let filterData = {
        "userCreatorCode" : codeUser
    };
    const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
    
    var cursor =  db.collection('Promotion').find( 
     conditionBuilt
     ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
     .limit(Number(nbElementsPerPage) );
    cursor.forEach(function(promotion,err){
        if(err)
         {
             console.log('errors produced :', err);
             callback(null,false); 
             
         }
        // credit.dateDebutPayement = moment(credit.dateDebutPayementObject).format('L');
         
        listPromotions.push(promotion);
    }).then(function(){
     // console.log('list credits',listCredits);
     callback(null,listPromotions); 
    });
      //return [];
},

getTotalPromotions : function (db,codeUser,filter, callback)
{
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
   var totalPromotions=  db.collection('Promotion').find( 
    conditionBuilt
).count(function(err,result){
        callback(null,result); 
    }
);
   


     //return [];
},
getPagePromotions : function (db,codeUser,pageNumber, nbElementsPerPage ,filter, callback)
{
    let listPromotions= [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
      console.log('condition',conditionBuilt);
      
      
   var cursor =  db.collection('Promotion').find( conditionBuilt
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(promotion,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        listPromotions.push(promotion);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listPromotions); 
   });
     //return [];
},
buildFilterCondition(filterObject,filterData)
{
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.description))
        {
                filterData["description"] = filterObject.description;
        }
        if(!utils.isUndefined(filterObject.code))
            {
                    filterData["code"] = filterObject.code;
            }
       }
        
     return filterData;
 },
deletePromotion: function (db,codePromotion,codeUser,callback){
    const criteria = { "code" : codeJournal, "userCreatorCode":codeUser };
    
    db.collection('Promotion').deleteOne(
        criteria , function(err,result){
            if(err){
                
                callback(null,null);
            }else{
                callback(false,result);
            }
        }
    ) ;
},

getPromotion: function (db,codePromotion,codeUser,callback)
{
    const criteria = { "code" : codePromotion, "userCreatorCode":codeUser };
  
       const promotion =  db.collection('Promotion').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
        // callback(false,client);
                 
}
}
module.exports.Promotions = Promotions;