//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var PaiementsMannequins = {
addPaiementMannequin : function (db,paiementMannequin,codeUser,callback){
    if(paiementMannequin != undefined){
        paiementMannequin.code  = utils.isUndefined(paiementMannequin.code)?shortid.generate():paiementMannequin.code;
        paiementMannequin.dateOperationObject = utils.isUndefined(paiementMannequin.dateOperationObject)? 
        new Date():paiementMannequin.dateOperationObject;
        paiementMannequin.userCreatorCode = codeUser;
        paiementMannequin.userLastUpdatorCode = codeUser;
        paiementMannequin.dateCreation = moment(new Date).format('L');
        paiementMannequin.dateLastUpdate = moment(new Date).format('L');
        
     
            db.collection('PaiementMannequin').insertOne(
                paiementMannequin,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,paiementMannequin);
                   }
                }
              ) ;


}else{
callback(true,null);
}
},
updatePaiementMannequin: function (db,paiementMannequin,codeUser, callback){

   
    paiementMannequin.dateOperationObject = utils.isUndefined(paiementMannequin.dateOperationObject)? 
    new Date():paiementMannequin.dateOperationObject;
    paiementMannequin.dateLastUpdate = moment(new Date).format('L');
    
    const criteria = { "code" : paiementMannequin.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"montantPaye" : paiementMannequin.montantPaye, 
    "codeMannequin" : paiementMannequin.codeMannequin, 
    "description" : paiementMannequin.description,
     "codeOrganisation" : paiementMannequin.codeOrganisation,
    "dateOperationObject" : paiementMannequin.dateOperationObject ,
     "modePayement" : paiementMannequin.modePayement, // cheque , virement , espece 
     "numeroCheque" : paiementMannequin.numeroCheque, 
     "compte":paiementMannequin.compte,
     "compteAdversaire" : paiementMannequin.compteAdversaire , 
     "banque" : paiementMannequin.banque ,
      "banqueAdversaire" : paiementMannequin.banqueAdversaire, 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": paiementMannequin.dateLastUpdate, 
    } ;
       
            db.collection('PaiementMannequin').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
      
    
    },
getListPaiementsMannequins : function (db,codeUser,callback)
{
    let listPaiementsMannequins  = [];
    
   var cursor =  db.collection('PaiementMannequin').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(paiementMannequin,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        paiementMannequin.dateOperation = moment(paiementMannequin.dateOperationObject).format('L');
        
        listPaiementsMannequins.push(paiementMannequin);
   }).then(function(){
  //  console.log('list credits',listPayementsFacturesVentes);
    callback(null,listPayementsRelevesVentes); 
   });
},
deletePaiementMannequin: function (db,codePaiementMannequin,codeUser,callback){
    const criteria = { "code" : codePaiementMannequin, "userCreatorCode":codeUser };
       
            db.collection('PaiementMannequin').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
   
},

getPaiementMannequin: function (db,codePaiementMannequin,codeUser,callback)
{
    const criteria = { "code" : codePaiementMannequin, "userCreatorCode":codeUser };
  
       const payementMannequin =  db.collection('PaiementMannequin').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                     
},

getListPaiementsMannequinsByCodeMannequin: function (db,codeMannequin, codeUser,callback)
{
    let listPaiementsMannequins = [];
  
   var cursor =  db.collection('PaiementMannequin').find( 
       {"userCreatorCode" : codeUser, "codeMannequin": codeMannequin}
    );
   cursor.forEach(function(paiementMannequin,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        paiementMannequin.dateOperation = moment(paiementMannequin.dateOperationObject).format('L');
        paiementMannequin.dateCheque = moment(paiementMannequin.dateChequeObject).format('L');
        
        listPaiementsMannequins.push(paiementMannequin);
   }).then(function(){
   // console.log('list credits',listCredits);
    callback(null,listPaiementsMannequins); 
   });

},
}
module.exports.PaiementsMannequins = PaiementsMannequins;