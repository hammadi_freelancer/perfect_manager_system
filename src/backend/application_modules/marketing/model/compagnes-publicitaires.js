//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var CompagnesPublicitaires = {
addCompagnePublicitaire: function (db,compagnePublicitaire,codeUser,callback){
    if(compagnePublicitaire != undefined){
        compagnePublicitaire.code  = utils.isUndefined(compagnePublicitaire.code)?shortid.generate():compagnePublicitaire.code;

        compagnePublicitaire.dateCompagnePublicitaireObject = 
        utils.isUndefined(compagnePublicitaire.dateCompagnePublicitaireObject)? 
        new Date():compagnePublicitaire.dateCompagnePublicitaireObject;
        compagnePublicitaire.userCreatorCode = codeUser;
        compagnePublicitaire.userLastUpdatorCode = codeUser;
        compagnePublicitaire.dateCreation = moment(new Date).format('L');
        compagnePublicitaire.dateLastUpdate = moment(new Date).format('L');
            db.collection('CompagnePublicitaire').insertOne(
                compagnePublicitaire,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
             // db.close();
             
    

}else{
callback(true,null);
}
},
updateCompagnePublicitaire: function (db,compagnePublicitaire,codeUser, callback){
  
    const criteria = { "code" : compagnePublicitaire.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
   "description" :compagnePublicitaire.description,
   "periodeFromObject" :compagnePublicitaire.periodeFromObject,
   "periodeToObject" :compagnePublicitaire.periodeToObject,
   "mannequin" : compagnePublicitaire.mannequin,
   "article" :compagnePublicitaire.article,
   "affiche1" :compagnePublicitaire.affiche1,
   "affiche2" :compagnePublicitaire.affiche2,
   "affiche3" :compagnePublicitaire.affiche3,
   "affiche4" :compagnePublicitaire.affiche4,
   "pourcentageRemise" :compagnePublicitaire.pourcentageRemise,
   "prix" :compagnePublicitaire.prix,
      "etat": compagnePublicitaire.etat,// ouvert/initial/en cours de validation /validé /en cours de validation /cloturé
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": compagnePublicitaire.dateLastUpdate, 
    
    } 
            db.collection('CompagnePublicitaire').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },function(comp,err){
                    if(err)
                    callback(false,err);
                    else
                        callback(true,true);
                    
                    
                }
            ) ;
     
    
    },
getListCompagnesPublicitaires: function (db,codeUser,pageNumber, nbElementsPerPage ,filter, callback)
{
    
    let listCompagnesPublicitaires = [];
    
   //console.log('filter recieved is :',filter);
   let filterObject = JSON.parse(filter);
  
    let filterData = {
        "userCreatorCode" : codeUser
    };
    const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
    
    var cursor =  db.collection('CompagnePublicitaire').find( 
     conditionBuilt
     ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
     .limit(Number(nbElementsPerPage) );
    cursor.forEach(function(compaigne,err){
        if(err)
         {
             console.log('errors produced :', err);
             callback(null,false); 
             
         }
        // credit.dateDebutPayement = moment(credit.dateDebutPayementObject).format('L');
         
        listCompagnesPublicitaires.push(compaigne);
    }).then(function(){
     // console.log('list credits',listCredits);
     callback(null,listCompagnesPublicitaires); 
    });
      //return [];
},

getTotalCompagnesPublicitaires : function (db,codeUser,filter, callback)
{
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
   var totalCompagne=  db.collection('CompagnePublicitaire').find( 
    conditionBuilt
).count(function(err,result){
        callback(null,result); 
    }
);
   


     //return [];
},
getPageCompagnesPublicitaires : function (db,codeUser,pageNumber, nbElementsPerPage ,filter, callback)
{
    let listCompagnesPublicitaires= [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
      console.log('condition',conditionBuilt);
      
      
   var cursor =  db.collection('CompagnePublicitaire').find( conditionBuilt
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(compagne,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        listCompagnesPublicitaires.push(compagne);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listCompagnesPublicitaires); 
   });
     //return [];
},
buildFilterCondition(filterObject,filterData)
{
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.description))
        {
                filterData["description"] = filterObject.description;
        }
        if(!utils.isUndefined(filterObject.code))
            {
                    filterData["code"] = filterObject.code;
            }
       }
        
     return filterData;
 },
deleteCompagnePublicitaire: function (db,codeCompagne,codeUser,callback){
    const criteria = { "code" : codeCompagne, "userCreatorCode":codeUser };
    
    db.collection('CompagnePublicitaire').deleteOne(
        criteria , function(err,result){
            if(err){
                
                callback(null,null);
            }else{
                callback(false,result);
            }
        }
    ) ;
},

getCompagnePublicitaire: function (db,codeCompagne,codeUser,callback)
{
    const criteria = { "code" : codeCompagne, "userCreatorCode":codeUser };
  
       const compagne =  db.collection('CompagnePublicitaire').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
        // callback(false,client);
                 
}
}
module.exports.CompagnesPublicitaires = CompagnesPublicitaires;