//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');
var jwt = require('jsonwebtoken');
//we need to have article as a json object : {code :'mmm', design :'ddd'}
var Groupes = {
addGroupe : function (db,groupe, codeUser,callback){
    if(groupe != undefined){

        groupe.code  = utils.isUndefined(groupe.code)?shortid.generate():groupe.code;
      
        groupe.userCreatorCode = codeUser;
        groupe.userLastUpdatorCode = codeUser;
        groupe.dateCreation = moment(new Date).format('L');
        groupe.dateLastUpdate = moment(new Date).format('L');
        
        db.collection('Groupe').insertOne(
            groupe,
            function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            } 
          ) ;
     
}
//return false;
},
updateGroupe: function (db,groupe,codeUser, callback){
    const criteria = { "code" : groupe.code };
    
   // roles : ADMIN , USER , VISITOR
    const dataToUpdate = {
        "nom" : groupe.nom,
   "description" : groupe.description,
    "utilisateurs": groupe.utilisateurs,
    "dateLastUpdate" : moment(new Date).format('L')
       } ;
          //  groupe.dateLastUpdate = moment(new Date).format('L');

            db.collection('Groupe').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                } 
            ) ;
      
    
    },
getListGroupes : function (db,codeUser,callback)
{
    let listGroupes = [];
    let filterData = {
        "userCreatorCode" : codeUser
    };
   var cursor =  db.collection('Groupe').find(
    filterData
   );
   cursor.forEach(function(groupe,err){
       if(err)
        {
            console.log('errors produced :', err);

        }
        listGroupes.push(groupe);
   }).then(function(){
    callback(null,listGroupes); 
   });
   
},





deleteGroupe: function (db,codeGroupe,codeUser,callback){
    const criteria = { "code" : codeGroupe };
      
            db.collection('Groupe').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }    
            ) ;
        
},
getGroupe: function (db,codeGroupe,codeUser,callback)
{
    const criteria = { "code" : codeGroupe };

       const groupe =  db.collection('Groupe').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                      
},
getTotalGroupes : function (db,codeUser,filter, callback)
{
       
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
   var totalGroupes =  db.collection('Groupe').find( 
    condition
    ).count(function(err,result){
        callback(null,result); 
    });   //return [];
},
buildFilterCondition(filterObject,filterData)
{
  
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.cin))
        {
                filterData["code"] = filterObject.code;
        }
         
        if(!utils.isUndefined(filterObject.nom))
          {
                 
                    filterData["nom"] = filterObject.nom;
                    
         }
         if(!utils.isUndefined(filterObject.description))
             {
                    
                       filterData["description"] =filterObject.description;
                       
            }
           
        }
        return filterData;
},
getPageGroupes : function (db,codeUser,pageNumber,nbElementsPerPage, filter,callback)
{
    let listGroupes = [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var cursor =  db.collection('Groupe').find(
      condition
   ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
   .limit(Number(nbElementsPerPage) );
   
   cursor.forEach(function(groupe,err){
       if(err)
        {
            console.log('errors produced :', err);

        }
        // utilisateur.dateNaissance = moment(utilisateur.dateNaissanceObject).format('L');
        // utilisateur.dateInscription = moment(utilisateur.dateInscriptionObject).format('L');
        listGroupes.push(groupe);
   }).then(function(){
   //  console.log('list clients',listClients);
    callback(null,listGroupes); 
   });
   


}
}
module.exports.Groupes = Groupes;