//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');
var jwt = require('jsonwebtoken');
//we need to have article as a json object : {code :'mmm', design :'ddd'}
var AccessRights = {
addAccessRight : function (db,accessRight,codeUser, callback){
    if(accessRight != undefined){

        accessRight.code  = utils.isUndefined(accessRight.code)?shortid.generate():accessRight.code;
        accessRight.userCreatorCode = codeUser;
        accessRight.userLastUpdatorCode = codeUser;
        accessRight.dateCreation = moment(new Date).format('L');
        accessRight.dateLastUpdate = moment(new Date).format('L');
  
        db.collection('AccessRight').insertOne(
            accessRight,
            function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }    
          ) ;
         // db.close();
        


       //return true;  
}
//return false;
},
updateAccessRight: function (db,accessRight, codeUser,callback){
    const criteria = { "code" : accessRight.code };
   // roles : ADMIN , USER , VISITOR
    const dataToUpdate = {
        "entityName" : accessRight.entityName,
     "rights" : accessRight.rights,
     "dateLastUpdate":moment(new Date).format('L')
     } ;
      
            db.collection('AccessRight').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }    
            ) ;
          
 
    
    },
getListAccessRights : function (db,codeUser,callback)
{
    let listAccessRights = [];
    let filterData = {
        "userCreatorCode" : codeUser
    };
   var cursor =  db.collection('AccessRight').find(filterData);
   cursor.forEach(function(accessRight,err){
       if(err)
        {
            console.log('errors produced :', err);

        }
      
        listAccessRights.push(accessRight);
   }).then(function(){
    console.log('list access rights ',listAccessRights);
    callback(null,listAccessRights); 
   });
   

     //return [];
},
deleteAccessRight: function (db,codeAccessRight,codeUser,callback){
    const criteria = { "code" : codeAccessRight };
      
            db.collection('AccessRight').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                } 
            ) ;
   
},
getAccessRight: function (db,codeAccessRight,codeUser,callback)
{
    const criteria = { "code" : codeAccessRight};
  
       const accessRight =  db.collection('AccessRight').findOne(
            criteria , function(err,result){
                if(err){
                    callback(null,null);
                }else{
                    callback(false,result);
                }
            }
        ) ;

},
buildFilterCondition(filterObject,filterData)
{
  
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.code))
        {
                filterData["code"] = filterObject.code;
        }
         
        if(!utils.isUndefined(filterObject.entityName))
          {
                 
                    filterData["entityName"] = filterObject.entityName;
                    
         }
         if(!utils.isUndefined(filterObject.rights))
             {
                    
                       filterData["rights"] =filterObject.rights;
                       
            }
            if(!utils.isUndefined(filterObject.description))
                {
                       
                          filterData["description"] =filterObject.description;
                          
               }
            
        }
        return filterData;
},
getPageAccessRights : function (db,codeUser,pageNumber,nbElementsPerPage, filter,callback)
{
    let listAccessRights = [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var cursor =  db.collection('AccessRight').find(
      condition
   ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
   .limit(Number(nbElementsPerPage) );
   
   cursor.forEach(function(accessRight,err){
       if(err)
        {
            console.log('errors produced :', err);

        }
     
        listAccessRights.push(accessRight);
   }).then(function(){
   //  console.log('list clients',listClients);
    callback(null,listAccessRights); 
   });
},
getTotalAccessRights : function (db,codeUser,filter, callback)
{
       
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
   var totalAccessRights =  db.collection('AccessRight').find( 
    condition
    ).count(function(err,result){
        callback(null,result); 
    }
);

     //return [];
},

}
module.exports.AccessRights = AccessRights;