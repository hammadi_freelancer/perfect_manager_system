//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');
var jwt = require('jsonwebtoken');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var Utilisateurs = {
addUtilisateur : function (db,utilisateur,codeUser, callback){
    if(utilisateur != undefined){

 utilisateur.code  = utils.isUndefined(utilisateur.code)?shortid.generate():utilisateur.code;
      
 utilisateur.userCreatorCode = codeUser;
 utilisateur.userLastUpdatorCode = codeUser;
 utilisateur.dateCreation = moment(new Date).format('L');
 utilisateur.dateLastUpdate = moment(new Date).format('L');
        db.collection('Utilisateur').insertOne(
            utilisateur,
            function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            } 
          ) ;
     
}
//return false;
},
updateUtilisateur: function (db,utilisateur,codeUser, callback){
    const criteria = { "code" : utilisateur.code };
    
   // roles : ADMIN , USER , VISITOR
    const dataToUpdate = {"nom" : utilisateur.nom, "prenom" : utilisateur.prenom,  "cin" : utilisateur.cin,
    "dateNaissanceObject" : utilisateur.dateNaissance, "dateInscriptionObject" : utilisateur.dateInscription,
    "photo" : utilisateur.photo,  "mobile" : utilisateur.mobile,"adresse" : utilisateur.adresse,
     "login" : utilisateur.login,  "email" : utilisateur.email, "etat" : utilisateur.etat,
     "password" : utilisateur.password, "role" : utilisateur.role,"description" : utilisateur.description,
    "accessRights": utilisateur.accessRights} ;
       
            db.collection('Utilisateur').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                } 
            ) ;
      
    
    },
getListUtilisateurs : function (db,codeUser,callback)
{
    let listUtilisateurs = [];
 
   var cursor =  db.collection('Utilisateur').find();
   cursor.forEach(function(utilisateur,err){
       if(err)
        {
            console.log('errors produced :', err);

        }
        utilisateur.dateNaissance = moment(utilisateur.dateNaissanceObject).format('L');
        utilisateur.dateInscription = moment(utilisateur.dateInscriptionObject).format('L');
        listUtilisateurs.push(utilisateur);
   }).then(function(){
    console.log('list users',listUtilisateurs);
    callback(null,listUtilisateurs); 
   });
   
},





deleteUtilisateur: function (db,codeUtilisateur,codeUser,callback){
    const criteria = { "code" : codeUtilisateur };
      
            db.collection('Utilisateur').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }    
            ) ;
        
},
getUtilisateur: function (db,codeUtilisateur,codeUser,callback)
{
    const criteria = { "code" : codeUtilisateur };

       const utilisateur =  db.collection('Utilisateur').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                      
},


getUtilisateurWithLoginPassword: function (db,login,password,callback)
{
    const criteria = { "login" : login,"password": password};

       const utilisateur =  db.collection('Utilisateur').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                
},

addInscription : function (db,inscription,codeUser, callback){
    if(inscription != undefined){

        inscription.code  = utils.isUndefined(inscription.code)?shortid.generate():inscription.code;
        inscription.dateInscription = moment( new Date()).format('L');
        if(utils.isUndefined(inscription.numeroInscription))
            {
               const currentD =  moment(new Date());
              // const genereKey = shortid.generate();
               const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.minutes().toString() ;
               inscription.numeroInscription = 'INCRI' + generatedCode;
            }
       
            inscription.userCreatorCode = 'SUPER_ADMIN';
            inscription.userLastUpdatorCode = 'SUPER_ADMIN';
            inscription.dateCreation = moment(new Date).format('L');
            inscription.dateLastUpdate = moment(new Date).format('L');
      
   
        db.collection('Inscription').insertOne(
            inscription,
            function(err,result){
                if(err){
                    callback(null,null);
                }else{
                    callback(false,result);
                }
            }    
          ) ;
    
       //return true;  
}
//return false;
},
getListInscriptions : function (db, pageNumber, nbElementsPerPage, codeUser,callback )
{
    let listInscriptions = [];
  
   var cursor =  db.collection('Inscription').find(
     {  "userCreatorCode" : "SUPER_ADMIN"}
   ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
   .limit(Number(nbElementsPerPage) );;
   cursor.forEach(function(inscription,err){
       if(err)
        {
            console.log('errors produced :', err);

        }
      
        listInscriptions.push(inscription);
   }).then(function(){
    //console.log('list users',listUtilisateurs);
    callback(null,listInscriptions); 
   }); 
},
getInscription : function (db,codeInscription,codeUser,callback)
{
    const criteria = { "code" : codeInscription };
 
       const inscription =  db.collection('Inscription').findOne(
            criteria , function(err,result){
                if(err){
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                        
},
deleteInscription: function (db,codeInscription,codeUser,callback){
    const criteria = { "code" : codeInscription, "userCreatorCode":"SUPER_ADMIN" };
    
            db.collection('Inscription').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        callback(null,null);
                    }else{
                        callback(false,result);
                    }
                }    
            ) ;
},
getTotalInscriptions : function (db,codeUser, callback)
{
    let listInscriptions = [];

   var totalInscriptions =  db.collection('Inscription').find( 
       {"userCreatorCode" : "SUPER_ADMIN"}
    ).count(function(err,result){
        callback(null,result); 
    }
);
   
},
getTotalUtilisateurs : function (db,codeUser,filter, callback)
{
       
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
   var totalUsers =  db.collection('Utilisateur').find( 
    condition
    ).count(function(err,result){
        callback(null,result); 
    }
);

     //return [];
},
buildFilterCondition(filterObject,filterData)
{
  
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.cin))
        {
                filterData["cin"] = filterObject.cin;
        }
         
        if(!utils.isUndefined(filterObject.nom))
          {
                 
                    filterData["nom"] = filterObject.nom;
                    
         }
         if(!utils.isUndefined(filterObject.prenom))
             {
                    
                       filterData["prenom"] =filterObject.prenom;
                       
            }
            if(!utils.isUndefined(filterObject.email))
                {
                       
                          filterData["email"] =filterObject.email;
                          
               }
            if(!utils.isUndefined(filterObject.mobile))
             {
                       
                          filterData["mobile"] =filterObject.mobile;
                          
            }
            if(!utils.isUndefined(filterObject.adresse))
                {
                          
                             filterData["adresse"] =filterObject.adresse;
                             
               }
            if(!utils.isUndefined(filterObject.dateNaissanceFromObject))
             {
                    
                       filterData["dateNaissanceObject"] = {$gt:dateNaissanceFromObject};
                       
            }
            if(!utils.isUndefined(filterObject.dateNaissanceToObject))
             {
                    
                       filterData["dateNaissanceObject"] = {$lt:dateNaissanceToObject};
                       
            }
            if(!utils.isUndefined(filterObject.dateInscriptionToObject))
             {
                    
                filterData["dateInscriptionObject"] = {$lt:dateInscriptionToObject};
                
                       
            }
            if(!utils.isUndefined(filterObject.dateInscriptionFromObject))
                {
                       
                   filterData["dateInscriptionObject"] = {$lt:dateInscriptionFromObject};
                   
                          
               }


            if(!utils.isUndefined(filterObject.role))
             {
                    
                       filterData["role"] = filterObject.role ;
                       
             }
                   if(!utils.isUndefined(filterObject.etat))
                    {
                           
                              filterData["etat"] = filterObject.etat ;
                              
                   }



        }
        return filterData;
},
getPageUtilisateurs : function (db,codeUser,pageNumber,nbElementsPerPage, filter,callback)
{
    let listUtilisateurs = [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var cursor =  db.collection('Utilisateur').find(
      condition
   ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
   .limit(Number(nbElementsPerPage) );
   
   cursor.forEach(function(utilisateur,err){
       if(err)
        {
            console.log('errors produced :', err);

        }
        utilisateur.dateNaissance = moment(utilisateur.dateNaissanceObject).format('L');
        utilisateur.dateInscription = moment(utilisateur.dateInscriptionObject).format('L');
        listUtilisateurs.push(utilisateur);
   }
).then(function(){
   //  console.log('list clients',listClients);
   if(listUtilisateurs.length < nbElementsPerPage)
    {
         db.collection('Utilisateur').findOne(
        {
            "code" : codeUser
        }, 
        function(err,utilisateurCourant){
            utilisateurCourant.dateNaissance = moment(utilisateurCourant.dateNaissanceObject).format('L');
            utilisateurCourant.dateInscription = moment(utilisateurCourant.dateInscriptionObject).format('L');
            listUtilisateurs.push(utilisateurCourant);
            callback(null,listUtilisateurs); 
            
        });
    }
    else
        {
            callback(null,listUtilisateurs); 
            
        }
   });
}
}
module.exports.Utilisateurs = Utilisateurs;