

var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var utilisateurs = require('../model/utilisateurs').Utilisateurs;
var jwt = require('jsonwebtoken');

router.post('/addUtilisateur',function(req,res,next){
    
       console.log(" ADD UTILISATEUR IS CALLED") ;
       console.log(" THE UTILISATEUR TO ADDED IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = utilisateurs.addUtilisateur(req.app.locals.dataBase,req.body,
        decoded.userData.code,function(err,utilisateurAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_UTILISATEUR',
                error_content: err
            });
         }else{
       
        return res.json(utilisateurAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.put('/updateUtilisateur',function(req,res,next){
    
       console.log(" UPDATE UTILISATEUR IS CALLED") ;
       console.log(" THE UTILISATEUR TO UPDATE IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = utilisateurs.updateUtilisateur(req.app.locals.dataBase,req.body,
        decoded.userData.code,function(err,utilisateurUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_UTILISATEUR',
                error_content: err
            });
         }else{
       
        return res.json(utilisateurUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getUtilisateurs',function(req,res,next){
    console.log("GET LIST UTILISATEURS IS CALLED");
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    utilisateurs.getListUtilisateurs(req.app.locals.dataBase,
        decoded.userData.code,function(err,listUtilisateurs){
       console.log("GET LIST UTILISATEURS : RESULT");
       console.log(listUtilisateurs);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_UTILISATEURS',
               error_content: err
           });
        }else{
      
       return res.json(listUtilisateurs);
        }
    });
});
router.get('/getPageUtilisateurs',function(req,res,next){
    //  console.log("GET LIST CLIENTS IS CALLED");
     var decoded = jwt.decode(req.headers.authorization.slice( 
         req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
     utilisateurs.getPageUtilisateurs(req.app.locals.dataBase,decoded.userData.code, 
        req.query.pageNumber, req.query.nbElementsPerPage ,
         req.query.filter,function(err,listUtilisateurs){
       //  console.log("GET LIST CLIENTS : RESULT");
       //  console.log(listArticles);
         if(err){
            return  res.json({
                error_message : 'ERROR_GET_PAGE_UTILISATEURS',
                error_content: err
            });
         }else{
       console.log('Page Users:',listUtilisateurs);
        return res.json(listUtilisateurs);
         }
     });
 });
router.get('/getUtilisateur/:codeUtilisateur',function(req,res,next){
    console.log("GET UTILISATEUR IS CALLED");
    console.log(req.params['codeUtilisateur']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    utilisateurs.getUtilisateur(req.app.locals.dataBase,req.params['codeUtilisateur'],
    decoded.userData.code,function(err,utilisateur){
       console.log("GET  UTILISATEUR : RESULT");
       console.log(utilisateur);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_UTILISATEUR',
               error_content: err
           });
        }else{
      
       return res.json(utilisateur);
        }
    });
})
/*router.post('/getFiltredClients',function(req,res,next){
    console.log("GET FILTRED LIST CLIENTS IS CALLED");
    clients.getFiltredListClients(req.body,function(err,listFiltredClients){
       console.log("GET FILTRED LIST CLIENTS : RESULT");
       console.log(listFiltredClients);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_FILTRED_CLIENTS',
               error_content: err
           });
        }else{
      
       return res.json(listFiltredClients);
        }
    });
})*/
router.delete('/deleteUtilisateur/:codeUtilisateur',function(req,res,next){
    
       console.log(" DELETE UTILISATEUR IS CALLED") ;
       console.log(" THE UTILISATEUR TO DELETE IS :",req.params['codeUtilisateur']);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = utilisateurs.deleteUtilisateur(req.app.locals.dataBase,
        req.params['codeUtilisateur'],decoded.userData.code,
        function(err,codeUtilisateur){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_UTILISATEUR',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeUtilisateur']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.post('/addInscription',function(req,res,next){
    
       console.log(" ADD INSCRI IS CALLED") ;
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = utilisateurs.addInscription(req.app.locals.dataBase,req.body,
        decoded.userData.code,function(err,inscriAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_INSCRIPTION',
                error_content: err
            });
         }else{
       
        return res.json(inscriAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

   router.get('/getInscriptions',function(req,res,next){
    console.log("GET LIST INSCRI IS CALLED");
    
    utilisateurs.getListInscriptions(req.app.locals.dataBase,req.query.pageNumber, req.query.nbElementsPerPage ,
        decoded.userData.code,function(err,listInscriptions){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_INSCRIPTIONS',
               error_content: err
           });
        }else{
      
       return res.json(listInscriptions);
        }
    });
});
router.get('/getInscription/:codeInscription',function(req,res,next){
    console.log("GET INSCRI IS CALLED");
    console.log(req.params['codeInscription']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    inscriptions.getInscription(req.app.locals.dataBase,req.params['codeInscription'],
    decoded.userData.code,function(err,inscription){
       console.log(inscription);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_INSCRIPTION',
               error_content: err
           });
        }else{
      
       return res.json(inscription);
        }
    });
});
router.delete('/deleteInscription/:codeInscription',function(req,res,next){
    
       console.log(" DELETE INSCRO IS CALLED") ;
       console.log(" THE INSCRIP TO DELETE IS :",req.params['codeInscription']);
       
    //var decoded = jwt.decode(req.headers.authorization.slice( 
       // req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = utilisateurs.deleteInscription(req.app.locals.dataBase,req.params['codeInscription'],decoded.userData.code, function(err,codeInscription){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_INSCRIPTION',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeInscription']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.get('/getTotalInscriptions',function(req,res,next){
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    utilisateurs.getTotalInscriptions(req.app.locals.dataBase,decoded.userData.code,function(err,totalInscriptions){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_INSCRIPTIONS',
                 error_content: err
             });
          }else{
        
         return res.json({'totalInscriptions':totalInscriptions});
          }
      });

  });
  router.get('/getTotalUtilisateurs',function(req,res,next){
    
     var decoded = jwt.decode(req.headers.authorization.slice( 
         req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
     utilisateurs.getTotalUtilisateurs(req.app.locals.dataBase,decoded.userData.code,
         req.query.filter, function(err,totalUtilisateurs){
           if(err){
              return  res.json({
                  error_message : 'ERROR_GET_TOTAL_UTILISATEURS',
                  error_content: err
              });
           }else{
         
          return res.json({'totalUtilisateurs':totalUtilisateurs});
           }
       });
 
   });
module.exports.utilisateursRouter = router;