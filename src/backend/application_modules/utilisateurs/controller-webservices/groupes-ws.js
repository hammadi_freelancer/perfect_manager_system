

var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var groupes = require('../model/groupes').Groupes;
var jwt = require('jsonwebtoken');

router.post('/addGroupe',function(req,res,next){
    
       console.log(" ADD GROUPE IS CALLED") ;
       console.log(" THE GROUPE TO ADDED IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = groupes.addGroupe(req.app.locals.dataBase,req.body,decoded.userData.code,
        function(err,groupeAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_GROUPE',
                error_content: err
            });
         }else{
       
        return res.json(groupeAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.put('/updateGroupe',function(req,res,next){
    
       console.log(" UPDATE GROUPE IS CALLED") ;
       console.log(" THE GROUPE TO UPDATE IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = groupes.updateGroupe(req.app.locals.dataBase,req.body,decoded.userData.code,
        function(err,groupeUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_GROUPE',
                error_content: err
            });
         }else{
       
        return res.json(groupeUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getGroupes',function(req,res,next){
    console.log("GET LIST GROUPES IS CALLED");
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    groupes.getListGroupes(req.app.locals.dataBase,decoded.userData.code,
        function(err,listGroupes){
       console.log("GET LIST GROUPES : RESULT");
       console.log(listGroupes);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_GROUPES',
               error_content: err
           });
        }else{
      
       return res.json(listGroupes);
        }
    });
});
router.get('/getPageGroupes',function(req,res,next){
    //  console.log("GET LIST CLIENTS IS CALLED");
     var decoded = jwt.decode(req.headers.authorization.slice( 
         req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
     groupes.getPageGroupes(req.app.locals.dataBase,decoded.userData.code, 
        req.query.pageNumber, req.query.nbElementsPerPage ,
         req.query.filter,
         function(err,listGroupes){
       //  console.log("GET LIST CLIENTS : RESULT");
       //  console.log(listArticles);
         if(err){
            return  res.json({
                error_message : 'ERROR_GET_PAGE_GROUPES',
                error_content: err
            });
         }else{
       
        return res.json(listGroupes);
         }
     });
 });
router.get('/getGroupe/:codeGroupe',function(req,res,next){
    console.log("GET GROUPE IS CALLED");
    console.log(req.params['codeGroupe']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    groupes.getGroupe(req.app.locals.dataBase,req.params['codeGroupe'],decoded.userData.code,
    function(err,groupe){
      //  console.log("GET  GROUPE : RESULT");
      //  console.log(groupe);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_GROUPE',
               error_content: err
           });
        }else{
      
       return res.json(groupe);
        }
    });
})
/*router.post('/getFiltredClients',function(req,res,next){
    console.log("GET FILTRED LIST CLIENTS IS CALLED");
    clients.getFiltredListClients(req.body,function(err,listFiltredClients){
       console.log("GET FILTRED LIST CLIENTS : RESULT");
       console.log(listFiltredClients);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_FILTRED_CLIENTS',
               error_content: err
           });
        }else{
      
       return res.json(listFiltredClients);
        }
    });
})*/
router.delete('/deleteGroupe/:codeGroupe',function(req,res,next){
    
       console.log(" DELETE GROUPE IS CALLED") ;
       console.log(" THE GROUPE TO DELETE IS :",req.params['codeGroupe']);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = groupes.deleteGroupe(req.app.locals.dataBase,req.params['codeGroupe'],
       decoded.userData.code,function(err,codeGroupe){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_GROUPE',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeGroupe']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
  router.get('/getTotalGroupes',function(req,res,next){
    
     var decoded = jwt.decode(req.headers.authorization.slice( 
         req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
     groupes.getTotalGroupes(req.app.locals.dataBase,decoded.userData.code,
         req.query.filter, 
         function(err,totalGroupes){
           if(err){
              return  res.json({
                  error_message : 'ERROR_GET_TOTAL_GROUPES',
                  error_content: err
              });
           }else{
         
          return res.json({'totalGroupes':totalGroupes});
           }
       });
 
   });
module.exports.groupesRouter = router;