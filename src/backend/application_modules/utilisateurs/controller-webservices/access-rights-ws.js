

var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var accessRights = require('../model/access-rights').AccessRights;
var jwt = require('jsonwebtoken');

router.post('/addAccessRight',function(req,res,next){
    
       console.log(" ADD ACCESS RIGHT IS CALLED") ;
       console.log(" THE ACCESS RIGHT TO ADDED IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = accessRights.addAccessRight(req.app.locals.dataBase,req.body,
        decoded.userData.code,function(err,accessRightAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_ACCESS_RIGHT',
                error_content: err
            });
         }else{
       
        return res.json(accessRightAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.put('/updateAccessRight',function(req,res,next){
    
       console.log(" UPDATE ACCESS RIGHT IS CALLED") ;
       console.log(" THE ACCESS RIGHT TO UPDATE IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = accessRights.updateAccessRight(req.app.locals.dataBase,req.body,
        decoded.userData.code,function(err,accessRightUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_ACCESS_RIGHT',
                error_content: err
            });
         }else{
       
        return res.json(accessRightUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getAccessRights',function(req,res,next){
    console.log("GET LIST ACCESS RIGHTS IS CALLED");
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    accessRights.getListAccessRights(req.app.locals.dataBase,
        decoded.userData.code,function(err,listAccessRights){
       console.log("GET LIST ACCESS RIGHTS: RESULT");
       console.log(listAccessRights);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_ACCESS_RIGHTS',
               error_content: err
           });
        }else{
      
       return res.json(listAccessRights);
        }
    });
})
router.get('/getAccessRight/:codeAccessRight',function(req,res,next){
    console.log("GET ACCESS RIGHT IS CALLED");
    console.log(req.params['codeAccessRight']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    accessRights.getAccessRight(req.app.locals.dataBase,req.params['codeAccessRight'],
    decoded.userData.code,function(err,accessRight){
       console.log("GET  ACCESS RIGHT : RESULT");
       console.log(accessRight);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_ACCESS_RIGHT',
               error_content: err
           });
        }else{
      
       return res.json(accessRight);
        }
    });
})
router.get('/getTotalAccessRights',function(req,res,next){
    
     var decoded = jwt.decode(req.headers.authorization.slice( 
         req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
         accessRights.getTotalAccessRights(req.app.locals.dataBase,decoded.userData.code,
         req.query.filter, 
         function(err,totalAccessRights){
           if(err){
              return  res.json({
                  error_message : 'ERROR_GET_TOTAL_ACCESS_RIGHTS',
                  error_content: err
              });
           }else{
         
          return res.json({'totalAccessRights':totalAccessRights});
           }
       });
 
   });
   router.get('/getPageAccessRights',function(req,res,next){
    //  console.log("GET LIST CLIENTS IS CALLED");
     var decoded = jwt.decode(req.headers.authorization.slice( 
         req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
     accessRights.getPageAccessRights(req.app.locals.dataBase,decoded.userData.code, 
        req.query.pageNumber, req.query.nbElementsPerPage ,
         req.query.filter,function(err,listAccessRights){
       //  console.log("GET LIST CLIENTS : RESULT");
       //  console.log(listArticles);
         if(err){
            return  res.json({
                error_message : 'ERROR_GET_PAGE_ACCESS_RIGHTS',
                error_content: err
            });
         }else{
       
        return res.json(listAccessRights);
         }
     });
 });
router.delete('/deleteAccessRight/:codeAccessRight',function(req,res,next){
    
       console.log(" DELETE ACCESS RIGHT IS CALLED") ;
       console.log(" THE ACCESS RIGHT TO DELETE IS :",req.params['codeAccessRight']);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = accessRights.deleteAccessRight(req.app.locals.dataBase,
        req.params['codeAccessRight'],
        decoded.userData.code,function(err,codeAccessRight){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_ACCESS_RIGHT',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeAccessRight']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

module.exports.accessRightsRouter = router;