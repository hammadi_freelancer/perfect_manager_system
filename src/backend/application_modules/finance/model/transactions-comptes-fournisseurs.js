var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

var TransactionsComptesFournisseurs = {
addTransactionCompteFournisseur : function (db,transactionCompteFournisseur,codeUser,callback){
    if(transactionCompteFournisseur != undefined){
        transactionCompteFournisseur.code  = utils.isUndefined(transactionCompteFournisseur.code)?'TR'+shortid.generate():transactionCompteFournisseur.code;
        transactionCompteFournisseur.dateTransactionObject = utils.isUndefined(transactionCompteFournisseur.dateTransactionObject)? 
        new Date():transactionCompteFournisseur.dateTransactionObject;
     
       
        transactionCompteFournisseur.userCreatorCode = codeUser;
        transactionCompteFournisseur.userLastUpdatorCode = codeUser;
        transactionCompteFournisseur.dateCreation = moment(new Date).format('L');
        transactionCompteFournisseur.dateLastUpdate = moment(new Date).format('L');
        
            db.collection('TransactionCompteFournisseur').insertOne(
                transactionCompteFournisseur,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,transactionCompteFournisseur);
                   }
                }
              ) ;


}else{
callback(true,null);
}
},
updateTransactionCompteFournisseur: function (db,transactionCompteFournisseur,codeUser, callback){

   
    transactionCompteFournisseur.dateTransactionObject = utils.isUndefined(transactionCompteFournisseur.dateTransactionObject)? 
    new Date():transactionCompteFournisseur.dateTransactionObject;
    transactionCompteFournisseur.dateLastUpdate = moment(new Date).format('L');
    
    const criteria = { "code" : transactionCompteFournisseur.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
    "dateTransactionObject" : transactionCompteFournisseur.dateTransactionObject, 
    "valeurTransaction" : transactionCompteFournisseur.valeurTransaction, 
    "type" : transactionCompteFournisseur.type, 
    "description" : transactionCompteFournisseur.description,
     "codeOrganisation" : transactionCompteFournisseur.codeOrganisation,
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": transactionCompteFournisseur.dateLastUpdate, 
    } ;
      
            db.collection('TransactionCompteFournisseur').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
    
    
    },
getListTransactionsComptesFournisseurs : function (db,codeUser,callback)
{
    let listTransactionsComptesFournisseurs = [];
    
   var cursor =  db.collection('TransactionCompteFournisseur').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(transCompteFournisseur,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        transCompteFournisseur.dateTransaction = moment(transCompteFournisseur.dateTransactionObject).format('L');
        listTransactionsComptesFournisseurs.push(transCompteFournisseur);
   }).then(function(){
   //  console.log('list payements credits',listPayementsCredits);
    callback(null,listTransactionsComptesFournisseurs); 
   });
   
},
getPageTransactionsComptesFournisseurs : function (db,codeUser,pageNumber, nbElementsPerPage , callback)
{
    let listTrComptesFournisseurs = [];
   
   var cursor =  db.collection('TransactionCompteFournisseur').find( 
       {"userCreatorCode" : codeUser}
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(trCompteFournisseur,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        trCompteFournisseur.dateTransaction = moment(trCompteFournisseur.dateTransactionObject).format('L');
        
        listTrComptesFournisseurs.push(trCompteFournisseur);
   }).then(function(){
    console.log('list tr compteFournisseur',listTrComptesFournisseurs);
    callback(null,listTrComptesFournisseurs); 
   });
   
},
deleteTransactionCompteFournisseur: function (db,codeTransactionCompteFournisseur,codeUser,callback){
    const criteria = { "code" : codeTransactionCompteFournisseur, "userCreatorCode":codeUser };
       
            db.collection('TransactionCompteFournisseur').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
    
},

getTransactionCompteFournisseur: function (db,codeTransactionCompteFournisseur,codeUser,callback)
{
    const criteria = { "code" : codeTransactionCompteFournisseur, "userCreatorCode":codeUser };
 
       const trCompteFournisseur =  db.collection('TransactionCompteFournisseur').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    callback(false,result);
                }
            }
        ) ;
                         
},

getTotalTransactionsComptesFournisseurs : function (db,codeUser, callback)
{
    let listTrComptesFournisseurs = [];
   
   var totalTransComptesFournisseurs =  db.collection('TransactionCompteFournisseur').find( 
       {"userCreatorCode" : codeUser}
    ).count(function(err,result){
        callback(null,result); 
    }
);

}
}
module.exports.TransactionsComptesFournisseurs = TransactionsComptesFournisseurs;