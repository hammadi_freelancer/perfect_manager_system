//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var PayementsRelevesAchats = {
addPayementReleveAchats : function (db,payementReleveAchats,codeUser,callback){
    if(payementReleveAchats!= undefined){
        payementReleveAchats.code  = utils.isUndefined(payementReleveAchats.code)?shortid.generate():payementReleveAchats.code;
        payementReleveAchats.dateOperationObject = utils.isUndefined(payementReleveAchats.dateOperationObject)? 
        new Date():payementReleveAchats.dateOperationObject;
        payementReleveAchats.userCreatorCode = codeUser;
        payementReleveAchats.userLastUpdatorCode = codeUser;
        payementReleveAchats.dateCreation = moment(new Date).format('L');
        payementReleveAchats.dateLastUpdate = moment(new Date).format('L');
        
     
            db.collection('PayementReleveAchats').insertOne(
                payementReleveAchats,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,payementReleveAchats);
                   }});
                
 

}
else {
callback(true,null);
}
},
updatePayementReleveAchats: function (db,payementReleveAchats,codeUser, callback){

   
    payementReleveAchats.dateOperationObject = utils.isUndefined(payementReleveAchats.dateOperationObject)? 
    new Date():payementReleveAchats.dateOperationObject;
    payementReleveAchats.dateLastUpdate = moment(new Date).format('L');
    
    const criteria = { "code" : payementReleveAchats.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"montantPaye" : payementReleveAchats.montantPaye, 
    "codeReleveAchats" : payementReleveAchats.codeReleveAchats, 
    "description" : payementReleveAchats.description,
     "codeOrganisation" : payementReleveAchats.codeOrganisation,
    "dateOperationObject" : payementReleveAchats.dateOperationObject ,
     "modePayement" : payementReleveAchats.modePayement, // cheque , virement , espece 
     "numeroCheque" : payementReleveAchats.numeroCheque, 
     "compte":payementReleveAchats.compte,
     "compteAdversaire" : payementReleveAchats.compteAdversaire , 
     "banque" : payementReleveAchats.banque ,
      "banqueAdversaire" : payementReleveAchats.banqueAdversaire, 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": payementReleveAchats.dateLastUpdate, 
    } ;
     
            db.collection('PayementReleveAchats').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
    
    },
getListPayementsRelevesAchats : function (db,codeUser,callback)
{
    let listPayementsRelevesAchats  = [];
   
   var cursor =  db.collection('PayementReleveAchats').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(payementReleveAchats,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        payementReleveAchats.dateOperation = moment(payementReleveAchats.dateOperationObject).format('L');
        
        listPayementsRelevesAchats.push(payementReleveAchats);
   }).then(function(){
  //  console.log('list credits',listPayementsFacturesVentes);
    callback(null,listPayementsRelevesAchats); 
   });

     //return [];
},
deletePayementReleveAchats: function (db,codePayementReleveAchats,codeUser,callback){
    const criteria = { "code" : codePayementReleveAchats, "userCreatorCode":codeUser };
        
            db.collection('PayementReleveAchats').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
            
       
},

getPayementReleveAchats: function (db,codePayementReleveAchats,codeUser,callback)
{
    console.log('Getting Payement Releve Achats With Code :',codePayementReleveAchats);

    const criteria = { "code" : codePayementReleveAchats, "userCreatorCode":codeUser };
 
       const payementReleveAchats =  db.collection('PayementReleveAchats').findOne(
            criteria , function(err,result){
                if(err){
                    console.log('Error in Getting Payement Releve Achats:',err)
                    callback(null,null);
                }else{
                    console.log(' Payement Releve Achats Retrieved is :',result)
                    
                    callback(false,result);
                }
            }
        ) ;
                   
},

getListPayementsRelevesAchatsByCodeReleve : function (db,codeReleveAchats, codeUser,callback)
{
    let listPayementsReleveAchats = [];
  
   var cursor =  db.collection('PayementReleveAchats').find( 
       {"userCreatorCode" : codeUser, "codeReleveAchats": codeReleveAchats}
    );
   cursor.forEach(function(payementReleveAchats,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        payementReleveAchats.dateOperation = moment(payementReleveAchats.dateOperationObject).format('L');
        payementReleveAchats.dateCheque = moment(payementReleveAchats.dateChequeObject).format('L');
        
        listPayementsReleveAchats.push(payementReleveAchats);
   }).then(function(){
   // console.log('list credits',listCredits);
    callback(null,listPayementsReleveAchats); 
   });
},
}
module.exports.PayementsRelevesAchats = PayementsRelevesAchats;