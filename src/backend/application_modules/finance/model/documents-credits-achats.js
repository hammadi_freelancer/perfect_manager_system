//var db = require('../common/MongoDbConnection').getDBConnection;
const mongoClient = require('mongodb').MongoClient;
const url ='mongodb://localhost:27017';
const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var DocumentsCreditsAchats = {
addDocumentCreditAchats : function (db,documentCreditAchats,codeUser,callback){
    if(documentCreditAchats != undefined){
        documentCreditAchats.code  = utils.isUndefined(documentCreditAchats.code)?shortid.generate():documentCreditAchats.code;
        documentCreditAchats.dateReceptionObject = utils.isUndefined(documentCreditAchats.dateReceptionObject)? 
        new Date():documentCreditAchats.dateReceptionObject;
        documentCreditAchats.userCreatorCode = codeUser;
        documentCreditAchats.userLastUpdatorCode = codeUser;
        documentCreditAchats.dateCreation = moment(new Date).format('L');
        documentCreditAchats.dateLastUpdate = moment(new Date).format('L');
        
      
            db.collection('DocumentCreditAchats').insertOne(
                documentCreditAchats,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,documentCreditAchats);
                   }
                }
              ) ;
             // db.close();
             
        };


},
updateDocumentCreditAchats: function (db,documentCreditAchats,codeUser, callback){

   
    documentCreditAchats.dateReceptionObject = utils.isUndefined(documentCreditAchats.dateReceptionObject)? 
    new Date():documentCreditAchats.dateReceptionObject;
    documentCreditAchats.dateLastUpdate = moment(new Date).format('L');
    const criteria = { "code" : documentCreditAchats.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"dateReceptionObject" : documentCreditAchats.dateReceptionObject, 
    "codeCreditAchats" : documentCreditAchats.codeCreditAchats, 
    "description" : documentCreditAchats.description,
     "codeOrganisation" : documentCreditAchats.codeOrganisation,
     "typeDocument" : documentCreditAchats.typeDocument, // cheque , virement , espece 
     "numeroDocument" : documentCreditAchats.numeroDocument, 
     "imageFace1":documentCreditAchats.imageFace1,
     "imageFace2" : documentCreditAchats.imageFace2 , 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": documentCreditAchats.dateLastUpdate, 
    } ;
        mongoClient.connect(url,function(err,clientDB){
            if(err)
             {
                callback(true,"ERROR_DATABASE_CONNECTION");
             }
            console.log('the client connected is ');
            console.log(clientDB)
            const db = clientDB.db(dbName);
            db.collection('DocumentCreditAchats').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                }
            ) ;
             clientDB.close();
             callback(false,documentCreditAchats);
        });
    
    },
getListDocumentsCreditsAchats : function (db,codeUser,callback)
{
    let listDocumentsCreditsAchats = [];
    mongoClient.connect(url,function(err,clientDB){
        if(err)
        {
            callback(err,[]);
        }else{
        const db = clientDB.db(dbName);   
   var cursor =  db.collection('DocumentCreditAchats').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(docCreditAchats,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        docCreditAchats.dateReception = moment(docCreditAchats.dateReceptionObject).format('L');
        
        listDocumentsCreditsAchats.push(docCreditAchats);
   }).then(function(){
    // console.log('list documents credits',listDocumentsCredits);
    callback(null,listDocumentsCreditsAchats); 
   });
   
}
});
     //return [];
},
deleteDocumentCreditAchats: function (db,codeDocumentCreditAchats,codeUser,callback){
    const criteria = { "code" : codeDocumentCreditAchats, "userCreatorCode":codeUser };
        mongoClient.connect(url,function(err,clientDB){
            if(err)
             {
                callback(true,"ERROR_DATABASE_CONNECTION");
             }
            console.log('the client connected is ');
            console.log(clientDB)
            const db = clientDB.db(dbName);
            db.collection('DocumentCreditAchats').deleteOne(
                criteria 
            ) ;
             clientDB.close();
             callback(false,codeDocumentCreditAchats);
        });
},

getDocumentCreditAchats: function (db,codeDocumentCreditAchats,codeUser,callback)
{
    const criteria = { "code" : codeDocumentCreditAchats, "userCreatorCode":codeUser };
    mongoClient.connect(url,function(err,clientDB){
        if(err)
         {
            callback(true,"ERROR_DATABASE_CONNECTION");
         }
        console.log('the client connected is ');
        console.log(clientDB)
        const db = clientDB.db(dbName);
       const credit =  db.collection('DocumentCreditAchats').findOne(
            criteria , function(err,result){
                if(err){
                    clientDB.close();
                    
                    callback(null,null);
                }else{
                    clientDB.close();
                    
                    callback(false,result);
                }
            }
        ) ;
        // callback(false,client);
    });                    
},


getListDocumentsCreditsAchatsByCodeCredit : function (db,codeUser,codeCreditAchats,callback)
{
    let listDocumentsCreditsAchats = [];
    mongoClient.connect(url,function(err,clientDB){
        if(err)
        {
            callback(err,[]);
        }else{
        const db = clientDB.db(dbName);   
   var cursor =  db.collection('DocumentCreditAchats').find( 
       {"userCreatorCode" : codeUser, "codeCreditAchats": codeCreditAchats}
    );
   cursor.forEach(function(documentCreditAchats,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        documentCreditAchats.dateReception = moment(documentCreditAchats.dateReceptionObject).format('L');
        
        listDocumentsCreditsAchats.push(documentCreditAchats);
   }).then(function(){
   // console.log('list credits',listCredits);
    callback(null,listDocumentsCreditsAchats); 
   });
   
}
});
     //return [];
},


}
module.exports.DocumentsCreditsAchats = DocumentsCreditsAchats;