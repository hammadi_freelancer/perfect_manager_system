
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var TransactionsComptesClients = {
addTransactionCompteClient : function (db,transactionCompteClient,codeUser,callback){
    if(transactionCompteClient != undefined){
        transactionCompteClient.code  = utils.isUndefined(transactionCompteClient.code)?'TR'+shortid.generate():transactionCompteClient.code;
        transactionCompteClient.dateTransactionObject = utils.isUndefined(transactionCompteClient.dateTransactionObject)? 
        new Date():transactionCompteClient.dateTransactionObject;
     
       
        transactionCompteClient.userCreatorCode = codeUser;
        transactionCompteClient.userLastUpdatorCode = codeUser;
        transactionCompteClient.dateCreation = moment(new Date).format('L');
        transactionCompteClient.dateLastUpdate = moment(new Date).format('L');
        
            db.collection('TransactionCompteClient').insertOne(
                transactionCompteClient,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,transactionCompteClient);
                   }
                }
              ) ;


}else{
callback(true,null);
}
},
updateTransactionCompteClient: function (db,transactionCompteClient,codeUser, callback){

   
    transactionCompteClient.dateTransactionObject = utils.isUndefined(transactionCompteClient.dateTransactionObject)? 
    new Date():transactionCompteClient.dateTransactionObject;
    transactionCompteClient.dateLastUpdate = moment(new Date).format('L');
    
    const criteria = { "code" : transactionCompteClient.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
    "dateTransactionObject" : transactionCompteClient.dateTransactionObject, 
    "valeurTransaction" : transactionCompteClient.valeurTransaction, 
    "type" : transactionCompteClient.type, 
    "description" : transactionCompteClient.description,
     "codeOrganisation" : transactionCompteClient.codeOrganisation,
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": transactionCompteClient.dateLastUpdate, 
    } ;
      
            db.collection('TransactionCompteClient').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
    
    
    },
getListTransactionsComptesClients : function (db,codeUser,callback)
{
    let listTransactionsComptesClients = [];
    
   var cursor =  db.collection('TransactionCompteClient').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(transCompteClient,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        transCompteClient.dateTransaction = moment(transCompteClient.dateTransactionObject).format('L');
        listTransactionsComptesClients.push(transCompteClient);
   }).then(function(){
   //  console.log('list payements credits',listPayementsCredits);
    callback(null,listTransactionsComptesClients); 
   });
   
},
getPageTransactionsComptesClients : function (db,codeUser,pageNumber, nbElementsPerPage , callback)
{
    let listTrComptesClients = [];
   
   var cursor =  db.collection('TransactionCompteClient').find( 
       {"userCreatorCode" : codeUser}
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(trCompteClient,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        trCompteClient.dateTransaction = moment(trCompteClient.dateTransactionObject).format('L');
        
        listTrComptesClients.push(trCompteClient);
   }).then(function(){
    console.log('list tr compteClient',listTrComptesClients);
    callback(null,listTrComptesClients); 
   });
   
},
deleteTransactionCompteClient: function (db,codeTransactionCompteClient,codeUser,callback){
    const criteria = { "code" : codeTransactionCompteClient, "userCreatorCode":codeUser };
       
            db.collection('TransactionCompteClient').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
    
},

getTransactionCompteClient: function (db,codeTransactionCompteClient,codeUser,callback)
{
    const criteria = { "code" : codeTransactionCompteClient, "userCreatorCode":codeUser };
 
       const trCompteClient =  db.collection('TransactionCompteClient').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    callback(false,result);
                }
            }
        ) ;
                         
},

getTotalTransactionsComptesClients : function (db,codeUser, callback)
{
    let listTrComptesClients = [];
   
   var totalTransComptesClients =  db.collection('TransactionCompteClient').find( 
       {"userCreatorCode" : codeUser}
    ).count(function(err,result){
        callback(null,result); 
    }
);

}
}
module.exports.TransactionsComptesClients = TransactionsComptesClients;