//var db = require('../common/MongoDbConnection').getDBConnection;
const mongoClient = require('mongodb').MongoClient;
const url ='mongodb://localhost:27017';
const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var TranchesCreditsAchats = {
addTrancheCreditAchats : function (db,trancheCreditAchats,codeUser,callback){
    if(trancheCreditAchats != undefined){
        trancheCreditAchats.code  = utils.isUndefined(trancheCreditAchats.code)?shortid.generate():trancheCreditAchats.code;
      //  documentCredit.datePayObject = utils.isUndefined(documentCredit.dateReceptionObject)? 
       //  new Date():documentCredit.dateReceptionObject;
       trancheCreditAchats.userCreatorCode = codeUser;
       trancheCreditAchats.userLastUpdatorCode = codeUser;
       trancheCreditAchats.dateCreation = moment(new Date).format('L');
       trancheCreditAchats.dateLastUpdate = moment(new Date).format('L');
        
     
            db.collection('TrancheCreditAchats').insertOne(
                trancheCreditAchats,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,trancheCreditAchats);
                   }
                }
              ) ;
             // db.close();
             

}else{
callback(true,null);
}
},
updateTrancheCreditAchats: function (db,trancheCreditAchats,codeUser, callback){

   
    

    const criteria = { "code" : trancheCreditAchats.code,"userCreatorCode" : codeUser };
  
    trancheCreditAchats.dateLastUpdate = moment(new Date).format('L');
    const dataToUpdate = {
    "codeCreditAchats" : trancheCreditAchats.codeCreditAchats, 
    "description" : trancheCreditAchats.description,
     "codeOrganisation" : trancheCreditAchats.codeOrganisation,
     "montantProgramme" : trancheCreditAchats.montantProgramme, // cheque , virement , espece 
     "montantPaye" : trancheCreditAchats.montantPaye, 
     "datePayementProgrammeObject":trancheCreditAchats.datePayementProgrammeObject,
     "datePayementObject" : trancheCreditAchats.datePayementObject , 
      "etat" : trancheCreditAchats.etat, // regle, partiellement regle , non regle 
        "codeDocumentCredit": trancheCreditAchats.codeDocumentCredit,
        "codePayementCredit": trancheCreditAchats.codePayementCredit,
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": trancheCreditAchats.dateLastUpdate, 
    } ;
       
            db.collection('TrancheCreditAchats').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
           
      
    
    },
getListTranchesCreditsAchats : function (db,codeUser,callback)
{
    let listTranchesCreditsAchats = [];
    
   var cursor =  db.collection('TrancheCreditAchats').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(trCreditAchats,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        trCreditAchats.datePayementProgramme = moment(trCreditAchats.datePayementProgrammeObject).format('L');
        trCreditAchats.datePayement= moment(trCreditAchats.datePayementObject).format('L');
        
        listTranchesCreditsAchats.push(trCreditAchats);
   }).then(function(){
    // console.log('list documents credits',listTranchesCredits);
    callback(null,listTranchesCreditsAchats); 
   });

},
deleteTrancheCreditAchats: function (db,codeTrancheCreditAchats,codeUser,callback){
    const criteria = { "code" : codeTrancheCreditAchats, "userCreatorCode":codeUser };
      
            db.collection('TrancheCreditAchats').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
           
       
},

getTrancheCreditAchats: function (db,codeTrancheCreditAchats,codeUser,callback)
{
    const criteria = { "code" : codeTrancheCreditAchats, "userCreatorCode":codeUser };
   
       const trCreditAchats =  db.collection('TrancheCreditAchats').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                    
},


getListTranchesCreditsAchatsByCodeCredit : function (db,codeUser,codeCreditAchats,callback)
{
    let listTranchesCreditsAchats = [];
  
   var cursor =  db.collection('TrancheCreditAchats').find( 
       {"userCreatorCode" : codeUser, "codeCreditAchats": codeCreditAchats}
    );
   cursor.forEach(function(trCreditAchats,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        trCreditAchats.datePayementProgramme = moment(trCreditAchats.datePayementProgrammeObject).format('L');
        trCreditAchats.datePayement= moment(trCreditAchats.datePayementObject).format('L');        
        listTranchesCreditsAchats.push(trCreditAchats);
   }).then(function(){
   // console.log('list credits',listCredits);
    callback(null,listTranchesCreditsAchats); 
   });
   

     //return [];
},


}
module.exports.TranchesCreditsAchats = TranchesCreditsAchats;