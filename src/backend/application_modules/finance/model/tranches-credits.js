//var db = require('../common/MongoDbConnection').getDBConnection;
const mongoClient = require('mongodb').MongoClient;
const url ='mongodb://localhost:27017';
const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var TranchesCredits = {
addTrancheCredit : function (db,trancheCredit,codeUser,callback){
    if(trancheCredit != undefined){
        trancheCredit.code  = utils.isUndefined(trancheCredit.code)?shortid.generate():trancheCredit.code;
      //  documentCredit.datePayObject = utils.isUndefined(documentCredit.dateReceptionObject)? 
       //  new Date():documentCredit.dateReceptionObject;
       trancheCredit.userCreatorCode = codeUser;
       trancheCredit.userLastUpdatorCode = codeUser;
       trancheCredit.dateCreation = moment(new Date).format('L');
       trancheCredit.dateLastUpdate = moment(new Date).format('L');
        
      
            db.collection('TrancheCredit').insertOne(
                trancheCredit,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,trancheCredit);
                   }
                }
              ) ;
      

}else{
callback(true,null);
}
},
updateTrancheCredit: function (db,trancheCredit,codeUser, callback){

   
    

    const criteria = { "code" : trancheCredit.code,"userCreatorCode" : codeUser };
  
    trancheCredit.dateLastUpdate = moment(new Date).format('L');
    const dataToUpdate = {
    "codeCredit" : trancheCredit.codeCredit, 
    "description" : trancheCredit.description,
     "codeOrganisation" : trancheCredit.codeOrganisation,
     "montantProgramme" : trancheCredit.montantProgramme, // cheque , virement , espece 
     "montantPaye" : trancheCredit.montantPaye, 
     "datePayementProgrammeObject":trancheCredit.datePayementProgrammeObject,
     "datePayementObject" : trancheCredit.datePayementObject , 
      "etat" : trancheCredit.etat, // regle, partiellement regle , non regle 
        "codeDocumentCredit": trancheCredit.codeDocumentCredit,
        "codePayementCredit": trancheCredit.codePayementCredit,
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": trancheCredit.dateLastUpdate, 
    } ;
   
            db.collection('TrancheCredit').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
          
      
    
    },
getListTranchesCredits : function (db,codeUser,callback)
{
    let listTranchesCredits = [];
     
   var cursor =  db.collection('TrancheCredit').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(trCredit,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        trCredit.datePayementProgramme = moment(trCredit.datePayementProgrammeObject).format('L');
        trCredit.datePayement= moment(trCredit.datePayementObject).format('L');
        
        listTranchesCredits.push(trCredit);
   }).then(function(){
    // console.log('list documents credits',listTranchesCredits);
    callback(null,listTranchesCredits); 
   });
   

     //return [];
},
deleteTrancheCredit: function (db,codeTrancheCredit,codeUser,callback){
    const criteria = { "code" : codeTrancheCredit, "userCreatorCode":codeUser };
       
            db.collection('TrancheCredit').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
      
},

getTrancheCredit: function (db,codeTrancheCredit,codeUser,callback)
{
    const criteria = { "code" : codeTrancheCredit, "userCreatorCode":codeUser };
  
       const trCredit =  db.collection('TrancheCredit').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                    
},


getListTranchesCreditsByCodeCredit : function (db,codeUser,codeCredit,callback)
{
    let listTranchesCredits = [];
   
   var cursor =  db.collection('TrancheCredit').find( 
       {"userCreatorCode" : codeUser, "codeCredit": codeCredit}
    );
   cursor.forEach(function(trCredit,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        trCredit.datePayementProgramme = moment(trCredit.datePayementProgrammeObject).format('L');
        trCredit.datePayement= moment(trCredit.datePayementObject).format('L');        
        listTranchesCredits.push(trCredit);
   }).then(function(){
   // console.log('list credits',listCredits);
    callback(null,listTranchesCredits); 
   });
   
}

}
module.exports.TranchesCredits = TranchesCredits;