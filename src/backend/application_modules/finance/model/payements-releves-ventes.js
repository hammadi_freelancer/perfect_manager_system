//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var PayementsRelevesVentes = {
addPayementReleveVentes : function (db,payementReleveVentes,codeUser,callback){
    if(payementReleveVentes != undefined){
        payementReleveVentes.code  = utils.isUndefined(payementReleveVentes.code)?shortid.generate():payementReleveVentes.code;
        payementReleveVentes.dateOperationObject = utils.isUndefined(payementReleveVentes.dateOperationObject)? 
        new Date():payementReleveVentes.dateOperationObject;
        payementReleveVentes.userCreatorCode = codeUser;
        payementReleveVentes.userLastUpdatorCode = codeUser;
        payementReleveVentes.dateCreation = moment(new Date).format('L');
        payementReleveVentes.dateLastUpdate = moment(new Date).format('L');
        
     
            db.collection('PayementReleveVentes').insertOne(
                payementReleveVentes,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,payementReleveVentes);
                   }
                }
              ) ;


}else{
callback(true,null);
}
},
updatePayementReleveVentes: function (db,payementReleveVentes,codeUser, callback){

   
    payementReleveVentes.dateOperationObject = utils.isUndefined(payementReleveVentes.dateOperationObject)? 
    new Date():payementReleveVentes.dateOperationObject;
    payementReleveVentes.dateLastUpdate = moment(new Date).format('L');
    
    const criteria = { "code" : payementReleveVentes.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"montantPaye" : payementReleveVentes.montantPaye, 
    "codeReleveVentes" : payementReleveVentes.codeReleveVentes, 
    "description" : payementReleveVentes.description,
     "codeOrganisation" : payementReleveVentes.codeOrganisation,
    "dateOperationObject" : payementReleveVentes.dateOperationObject ,
     "modePayement" : payementReleveVentes.modePayement, // cheque , virement , espece 
     "numeroCheque" : payementReleveVentes.numeroCheque, 
     "compte":payementReleveVentes.compte,
     "compteAdversaire" : payementReleveVentes.compteAdversaire , 
     "banque" : payementReleveVentes.banque ,
      "banqueAdversaire" : payementReleveVentes.banqueAdversaire, 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": payementReleveVentes.dateLastUpdate, 
    } ;
       
            db.collection('PayementReleveVentes').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
      
    
    },
getListPayementsRelevesVentes : function (db,codeUser,callback)
{
    let listPayementsRelevesVentes  = [];
    
   var cursor =  db.collection('PayementReleveVentes').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(payementReleveVentes,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        payementReleveVentes.dateOperation = moment(payementReleveVentes.dateOperationObject).format('L');
        
        listPayementsRelevesVentes.push(payementReleveVentes);
   }).then(function(){
  //  console.log('list credits',listPayementsFacturesVentes);
    callback(null,listPayementsRelevesVentes); 
   });
},
deletePayementReleveVentes: function (db,codePayementReleveVentes,codeUser,callback){
    const criteria = { "code" : codePayementReleveVentes, "userCreatorCode":codeUser };
       
            db.collection('PayementReleveVentes').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
   
},

getPayementReleveVentes: function (db,codePayementReleveVentes,codeUser,callback)
{
    const criteria = { "code" : codePayementReleveVentes, "userCreatorCode":codeUser };
  
       const payementReleveVentes =  db.collection('PayementReleveVentes').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                     
},

getListPayementsRelevesVentesByCodeReleve : function (db,codeReleve, codeUser,callback)
{
    let listPayementsReleveVentes = [];
  
   var cursor =  db.collection('PayementReleveVentes').find( 
       {"userCreatorCode" : codeUser, "codeReleveVentes": codeReleve}
    );
   cursor.forEach(function(payementReleveVentes,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        payementReleveVentes.dateOperation = moment(payementReleveVentes.dateOperationObject).format('L');
        payementReleveVentes.dateCheque = moment(payementReleveVentes.dateChequeObject).format('L');
        
        listPayementsReleveVentes.push(payementReleveVentes);
   }).then(function(){
   // console.log('list credits',listCredits);
    callback(null,listPayementsReleveVentes); 
   });

},
}
module.exports.PayementsRelevesVentes = PayementsRelevesVentes;