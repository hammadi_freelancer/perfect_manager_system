//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var PayementsFacturesVentes = {
addPayementFactureVente : function (db,payementFactureVente,codeUser,callback){
    if(payementFactureVente != undefined){
        payementFactureVente.code  = utils.isUndefined(payementFactureVente.code)?shortid.generate():payementFactureVente.code;
        payementFactureVente.dateOperationObject = utils.isUndefined(payementFactureVente.dateOperationObject)? 
        new Date():payementFactureVente.dateOperationObject;
        payementFactureVente.userCreatorCode = codeUser;
        payementFactureVente.userLastUpdatorCode = codeUser;
        payementFactureVente.dateCreation = moment(new Date).format('L');
        payementFactureVente.dateLastUpdate = moment(new Date).format('L');
        
       
            db.collection('PayementFactureVente').insertOne(
                payementFactureVente,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,payementFactureVente);
                   }
                }
              ) ;


}else{
callback(true,null);
}
},
updatePayementFactureVente: function (db,payementFactureVente,codeUser, callback){

   
    payementFactureVente.dateOperationObject = utils.isUndefined(payementFactureVente.dateOperationObject)? 
    new Date():payementFactureVente.dateOperationObject;
    payementFactureVente.dateLastUpdate = moment(new Date).format('L');
    
    const criteria = { "code" : payementFactureVente.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"montantPaye" : payementFactureVente.montantPaye, 
    "codeFactureVente" : payementFactureVente.codeFactureVente, 
    "description" : payementFactureVente.description,
     "codeOrganisation" : payementFactureVente.codeOrganisation,
    "dateOperationObject" : payementFactureVente.dateOperationObject ,
     "modePayement" : payementFactureVente.modePayement, // cheque , virement , espece 
     "numeroCheque" : payementFactureVente.numeroCheque, 
     "compte":payementFactureVente.compte,
     "compteAdversaire" : payementFactureVente.compteAdversaire , 
     "banque" : payementFactureVente.banque ,
      "banqueAdversaire" : payementFactureVente.banqueAdversaire, 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": payementFactureVente.dateLastUpdate, 
    } ;
       
            db.collection('PayementFactureVente').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
       
    
    },
getListPayementsFacturesVentes : function (db,codeUser,callback)
{
    let listPayementsFacturesVentes  = [];

   var cursor =  db.collection('PayementFactureVente').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(payementFactureVente,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        payementFactureVente.dateOperation = moment(payementFactureVente.dateOperationObject).format('L');
        
        listPayementsFacturesVentes.push(payementFactureVente);
   }).then(function(){
  //  console.log('list credits',listPayementsFacturesVentes);
    callback(null,listPayementsFacturesVentes); 
   });

},
deletePayementFactureVente: function (db,codePayementFactureVente,codeUser,callback){
    const criteria = { "code" : codePayementFactureVente, "userCreatorCode":codeUser };
        
            db.collection('PayementFactureVente').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
},

getPayementFactureVente: function (db,codePayementFactureVente,codeUser,callback)
{
    const criteria = { "code" : codePayementFactureVente, "userCreatorCode":codeUser };
 
       const payementFactureVente =  db.collection('PayementFactureVente').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                           
},

getListPayementsFactureVentesByCodeFactureVente : function (db,codeFactureVente, codeUser,callback)
{
    let listPayementsFactureVentes = [];
  
   var cursor =  db.collection('PayementFactureVente').find( 
       {"userCreatorCode" : codeUser, "codeFactureVentes": codeFactureVente}
    );
   cursor.forEach(function(payementFactureVente,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        payementFactureVente.dateOperation = moment(payementFactureVente.dateOperationObject).format('L');
        payementFactureVente.dateCheque = moment(payementFactureVente.dateChequeObject).format('L');
        
        listPayementsFactureVentes.push(payementFactureVente);
   }).then(function(){
   // console.log('list credits',listCredits);
    callback(null,listPayementsFactureVentes); 
   });

},
}
module.exports.PayementsFacturesVentes = PayementsFacturesVentes;