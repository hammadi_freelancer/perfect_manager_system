//var db = require('../common/MongoDbConnection').getDBConnection;
const mongoClient = require('mongodb').MongoClient;
const url ='mongodb://localhost:27017';
const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var DocumentsCredits = {
addDocumentCredit : function (db,documentCredit,codeUser,callback){
    if(documentCredit != undefined){
        documentCredit.code  = utils.isUndefined(documentCredit.code)?shortid.generate():documentCredit.code;
        documentCredit.dateReceptionObject = utils.isUndefined(documentCredit.dateReceptionObject)? 
        new Date():documentCredit.dateReceptionObject;
        documentCredit.userCreatorCode = codeUser;
        documentCredit.userLastUpdatorCode = codeUser;
        documentCredit.dateCreation = moment(new Date).format('L');
        documentCredit.dateLastUpdate = moment(new Date).format('L');
        
      
            db.collection('DocumentCredit').insertOne(
                documentCredit,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,documentCredit);
                   }
                }
              ) ;
             // db.close();
             
     

}else{
callback(true,null);
}
},
updateDocumentCredit: function (db,documentCredit,codeUser, callback){

   
    documentCredit.dateReceptionObject = utils.isUndefined(documentCredit.dateReceptionObject)? 
    new Date():documentCredit.dateReceptionObject;

    const criteria = { "code" : documentCredit.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"dateReceptionObject" : documentCredit.dateReceptionObject, 
    "codeCredit" : documentCredit.codeCredit, 
    "description" : documentCredit.description,
     "codeOrganisation" : documentCredit.codeOrganisation,
     "typeDocument" : documentCredit.typeDocument, // cheque , virement , espece 
     "numeroDocument" : documentCredit.numeroDocument, 
     "imageFace1":documentCredit.imageFace1,
     "imageFace2" : documentCredit.imageFace2 , 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": documentCredit.dateLastUpdate, 
    } ;
      
            db.collection('DocumentCredit').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
    
    
    },
getListDocumentsCredits : function (db,codeUser,callback)
{
    let listDocumentsCredits = [];
     
   var cursor =  db.collection('DocumentCredit').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(docCredit,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        docCredit.dateReception = moment(docCredit.dateReceptionObject).format('L');
        
        listDocumentsCredits.push(docCredit);
   }).then(function(){
    console.log('list documents credits',listDocumentsCredits);
    callback(null,listDocumentsCredits); 
   });
   
},
deleteDocumentCredit: function (db,codeDocumentCredit,codeUser,callback){
    const criteria = { "code" : codeDocumentCredit, "userCreatorCode":codeUser };
       
            db.collection('DocumentCredit').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
        
},

getDocumentCredit: function (db,codeDocumentCredit,codeUser,callback)
{
    const criteria = { "code" : codeDocumentCredit, "userCreatorCode":codeUser };
 
       const credit =  db.collection('DocumentCredit').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            });
                   
},


getListDocumentsCreditsByCodeCredit : function (db,codeUser,codeCredit,callback)
{
    let listDocumentsCredits = [];
    
   var cursor =  db.collection('DocumentCredit').find( 
       {"userCreatorCode" : codeUser, "codeCredit": codeCredit}
    );
   cursor.forEach(function(documentCredit,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        documentCredit.dateReception = moment(documentCredit.dateReceptionObject).format('L');
        
        listDocumentsCredits.push(documentCredit);
   }).then(function(){
  // console.log('list doc credits',listCredits);
    callback(null,listDocumentsCredits); 
   });
     //return [];
},


}
module.exports.DocumentsCredits = DocumentsCredits;