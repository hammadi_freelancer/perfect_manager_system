//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var PayementsFacturesAchats = {
addPayementFactureAchats : function (db,payementFactureAchats,codeUser,callback){
    if(payementFactureAchats != undefined){
        payementFactureAchats.code  = utils.isUndefined(payementFactureAchats.code)?shortid.generate():payementFactureAchats.code;
        payementFactureAchats.dateOperationObject = utils.isUndefined(payementFactureAchats.dateOperationObject)? 
        new Date():payementFactureAchats.dateOperationObject;
        payementFactureAchats.userCreatorCode = codeUser;
        payementFactureAchats.userLastUpdatorCode = codeUser;
        payementFactureAchats.dateCreation = moment(new Date).format('L');
        payementFactureAchats.dateLastUpdate = moment(new Date).format('L');
        
     
            db.collection('PayementFactureAchats').insertOne(
                payementFactureAchats,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,payementFactureAchats);
                   }
                }
              ) ;

}else{
callback(true,null);
}
},
updatePayementFactureAchats: function (db,payementFactureAchats,codeUser, callback){

   
    payementFactureAchats.dateOperationObject = utils.isUndefined(payementFactureAchats.dateOperationObject)? 
    new Date():payementFactureAchats.dateOperationObject;
    payementFactureAchats.dateLastUpdate = moment(new Date).format('L');
    
    const criteria = { "code" : payementFactureAchats.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"montantPaye" : payementFactureAchats.montantPaye, 
    "codeFactureAchats" : payementFactureAchats.codeFactureAchats, 
    "description" : payementFactureAchats.description,
     "codeOrganisation" : payementFactureAchats.codeOrganisation,
    "dateOperationObject" : payementFactureAchats.dateOperationObject ,
     "modePayement" : payementFactureAchats.modePayement, // cheque , virement , espece 
     "numeroCheque" : payementFactureAchats.numeroCheque, 
     "compte":payementFactureAchats.compte,
     "compteAdversaire" : payementFactureAchats.compteAdversaire , 
     "banque" : payementFactureAchats.banque ,
      "banqueAdversaire" : payementFactureAchats.banqueAdversaire, 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": payementFactureAchats.dateLastUpdate, 
    } ;
   
            db.collection('PayementFactureAchats').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
           
    
    
    },
getListPayementsFacturesAchats : function (db,codeUser,callback)
{
    let listPayementsFacturesAchats  = [];
  
   var cursor =  db.collection('PayementFactureAchats').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(payFactAchats,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        payFactAchats.dateOperation = moment(payFactAchats.dateOperationObject).format('L');
        
        listPayementsFacturesAchats.push(payFactAchats);
   }).then(function(){
  //  console.log('list credits',listPayementsFacturesVentes);
    callback(null,listPayementsFacturesAchats); 
   });

     //return [];
},
deletePayementFactureAchats: function (db,codePayementFactureAchats,codeUser,callback){
    const criteria = { "code" : codePayementFactureAchats, "userCreatorCode":codeUser };
       
            db.collection('PayementFactureAchats').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
      
},

getPayementFactureAchats: function (db,codePayementFactureAchats,codeUser,callback)
{
    const criteria = { "code" : codePayementFactureAchats, "userCreatorCode":codeUser };
  
       const payementFactureAchats =  db.collection('PayementFactureAchats').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                     
},

getListPayementsFactureAchatsByCodeFactureAchats: function (db,codeFactureAchats, codeUser,callback)
{
    let listPayementsFactureAchats = [];
    
   var cursor =  db.collection('PayementFactureAchats').find( 
       {"userCreatorCode" : codeUser, "codeFactureAchats": codeFactureAchats}
    );
   cursor.forEach(function(payementFactureAchats,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        payementFactureAchats.dateOperation = moment(payementFactureAchats.dateOperationObject).format('L');
        payementFactureAchats.dateCheque = moment(payementFactureAchats.dateChequeObject).format('L');
        
        listPayementsFactureAchats.push(payementFactureAchats);
   }).then(function(){
   // console.log('list credits',listCredits);
    callback(null,listPayementsFactureAchats); 
   });
   

     //return [];
},
}
module.exports.PayementsFacturesAchats = PayementsFacturesAchats;