//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var ComptesFournisseurs = {
addCompteFournisseur : function (db,compteFournisseur,codeUser,callback){
    if(compteFournisseur != undefined){
        compteFournisseur.code  = utils.isUndefined(compteFournisseur.code)?shortid.generate():compteFournisseur.code;
        compteFournisseur.dateOuvertureObject = utils.isUndefined(compteFournisseur.dateOuvertureObject)? 
        new Date():compteFournisseur.dateOuvertureObject;
        if(utils.isUndefined(compteFournisseur.numeroCompteFournisseur))
            {
               const currentD =  moment(new Date);
              // const genereKey = shortid.generate();
               const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.minutes().toString() ;
               compteFournisseur.numeroCompteFournisseur = 'COMPFOUR' + generatedCode;
            }
       
            compteFournisseur.userCreatorCode = codeUser;
            compteFournisseur.userLastUpdatorCode = codeUser;
            compteFournisseur.dateCreation = moment(new Date).format('L');
            compteFournisseur.dateLastUpdate = moment(new Date).format('L');
        
            db.collection('CompteFournisseur').insertOne(
                compteFournisseur,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,compteFournisseur);
                   }
                }
              ) ;


}else{
callback(true,null);
}
},
updateCompteFournisseur: function (db,compteFournisseur,codeUser, callback){

   
    compteFournisseur.dateOuvertureObject = utils.isUndefined(compteFournisseur.dateOuvertureObject)? 
    new Date():compteFournisseur.dateOuvertureObject;
    compteFournisseur.dateLastUpdate = moment(new Date).format('L');
    
    const criteria = { "code" : compteFournisseur.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
    "numeroCompteFournisseur" : compteFournisseur.numeroCompteFournisseur, 
    "fournisseur" : compteFournisseur.fournisseur, 
    "profession" : compteFournisseur.profession, 
    "chiffreAffaires" : compteFournisseur.chiffreAffaire, 
    "valeurVentes" : compteFournisseur.valeurVentes, 
    "valeurCredits" : compteFournisseur.valeurCredits, 
    "valeurPaiements" : compteFournisseur.valeurPaiements, 
    "dateOuvertureObject" : compteFournisseur.dateOuvertureObject ,
    "dateClotureObject" : compteFournisseur.dateClotureObject ,
    "description" : compteFournisseur.description,
     "codeOrganisation" : compteFournisseur.codeOrganisation,
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": compteFournisseur.dateLastUpdate, 
    } ;
      
            db.collection('CompteFournisseur').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
    
    
    },
getListComptesFournisseurs : function (db,codeUser,callback)
{
    let listComptesFournisseurs = [];
    
   var cursor =  db.collection('CompteFournisseur').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(compteFournisseur,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        compteFournisseur.dateOuverture = moment(compteFournisseur.dateOuvertureObject).format('L');
        compteFournisseur.dateCloture = moment(compteFournisseur.dateClotureObject).format('L');
        
        listComptesFournisseurs.push(compteFournisseur);
   }).then(function(){
   //  console.log('list payements credits',listPayementsCredits);
    callback(null,listComptesFournisseurs); 
   });
   
},
getPageComptesFournisseurs : function (db,codeUser,pageNumber, nbElementsPerPage ,filter, callback)
{
    let listComptesFournisseurs = [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var cursor =  db.collection('CompteFournisseur').find( 
    condition
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(compteFournisseur,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        compteFournisseur.dateOuverture = moment(compteFournisseur.dateOuvertureObject).format('L');
        compteFournisseur.dateCloture = moment(compteFournisseur.dateClotureObject).format('L');
        
        listComptesFournisseurs.push(compteFournisseur);
   }).then(function(){
    console.log(' listComptesFournisseurs',listComptesFournisseurs);
    callback(null,listComptesFournisseurs); 
   });
   
},
deleteCompteFournisseur: function (db,codeCompteFournisseur,codeUser,callback){
    const criteria = { "code" : codeCompteFournisseur, "userCreatorCode":codeUser };
       
            db.collection('CompteFournisseur').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
    
},

getCompteFournisseur: function (db,codeCompteFournisseur,codeUser,callback)
{
    const criteria = { "code" : codeCompteFournisseur, "userCreatorCode":codeUser };
 
       const compteFournisseur =  db.collection('CompteFournisseur').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    callback(false,result);
                }
            }
        ) ;
                         
},

getTotalComptesFournisseurs : function (db,codeUser,filter, callback)
{
    let listComptesFournisseurs = [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var totalComptesFournisseurs =  db.collection('CompteFournisseur').find( 
      condition
    ).count(function(err,result){
        callback(null,result); 
    }
);

},

buildFilterCondition(filterObject,filterData)
{
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.numeroCompteFournisseur))
        {
                filterData["numeroCompteFournisseur"] = filterObject.numeroCompteFournisseur;
        }
         
        if(!utils.isUndefined(filterObject.profession))
          {
                 
                    filterData["profession"] = filterObject.profession;
                    
         }
         if(!utils.isUndefined(filterObject.chiffreAffaires))
             {
                    
                       filterData["chiffreAffaires"] =filterObject.chiffreAffaires;
                       
            }
            if(!utils.isUndefined(filterObject.dateOuvertureFromObject))
                {
                       
                          filterData["dateOuvertureObject"] = {$gt:filterObject.dateOuvertureFromObject};
                          
               }
               if(!utils.isUndefined(filterObject.dateOuvertureToObject))
                {
                       
                          filterData["dateOuvertureObject"] = {$lt:filterObject.dateOuvertureToObject};
                          
               }

               if(!utils.isUndefined(filterObject.dateClotureFromObject))
                {
                       
                          filterData["dateClotureObject"] = {$gt:filterObject.dateClotureFromObject};
                          
               }
               if(!utils.isUndefined(filterObject.dateClotureToObject))
                {
                       
                          filterData["dateClotureObject"] = {$lt:filterObject.dateClotureToObject};
                          
               }

            if(!utils.isUndefined(filterObject.etat))
             {
                    
                       filterData["etat"] = filterObject.etat ;
                       
            }
       
            if(!utils.isUndefined(filterObject.cinFournisseur))
                    {
                           
                              filterData["fournisseur.cin"] = filterObject.cinFournisseur ;
                              
                   }
             if(!utils.isUndefined(filterObject.nomFournisseur))
                    {
                           
                              filterData["fournisseur.nom"] = filterObject.nomFournisseur ;
                              
                   }

             if(!utils.isUndefined(filterObject.prenomFournisseur))
              {
                           
                              filterData["fournisseur.prenom"] = filterObject.prenomFournisseur ;
                              
              }
              if(!utils.isUndefined(filterObject.raisonFournisseur))
                {
                             
                                filterData["fournisseur.raisonSociale"] = filterObject.raisonFournisseur;
                                
                }
                if(!utils.isUndefined(filterObject.matriculeFournisseur))
                    {
                                 
                                    filterData["fournisseur.matriculeFiscale"] = filterObject.matriculeFournisseur ;
                                    
                    }
            if(!utils.isUndefined(filterObject.registreFournisseur))
                        {
                                     
                            filterData["fournisseur.registreCommerce"] = filterObject.registreFournisseur ;
                                        
                        }


        }
        return filterData;
},










}
module.exports.ComptesFournisseurs = ComptesFournisseurs;