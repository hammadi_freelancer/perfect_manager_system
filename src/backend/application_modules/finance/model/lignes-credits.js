//var db = require('../common/MongoDbConnection').getDBConnection;
const mongoClient = require('mongodb').MongoClient;
const url ='mongodb://localhost:27017';
const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var LignesCredits = {
addLigneCredit : function (db,ligneCredit,codeUser,callback){
    if(ligneCredit != undefined){
        ligneCredit.code  = utils.isUndefined(ligneCredit.code)?shortid.generate():ligneCredit.code;
        ligneCredit.dateOperationObject = utils.isUndefined(ligneCredit.dateOperationObject)? 
        new Date():ligneCredit.dateOperationObject;
        ligneCredit.userCreatorCode = codeUser;
        ligneCredit.userLastUpdatorCode = codeUser;
        ligneCredit.dateCreation = moment(new Date).format('L');
        ligneCredit.dateLastUpdate = moment(new Date).format('L');
            db.collection('LigneCredit').insertOne(
                ligneCredit,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,ligneCredit);
                   }
                }
              ) ;

}else{
callback(true,null);
}
},
updateLigneCredit: function (db,ligneCredit,codeUser, callback){

   
    ligneCredit.dateOperationObject = utils.isUndefined(ligneCredit.dateOperationObject)? 
    new Date():ligneCredit.dateOperationObject;
    ligneCredit.dateLastUpdate = moment(new Date).format('L');
    
    const criteria = { "code" : ligneCredit.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"montantAPayer" : ligneCredit.montantAPayer, 
    "codeCredit" : ligneCredit.codeCredit, 
    "description" : ligneCredit.description,
    "etat" : ligneCredit.etat,
     "codeOrganisation" : ligneCredit.codeOrganisation,
    "dateOperationObject" : ligneCredit.dateOperationObject ,
    } ;
       
            db.collection('LigneCredit').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
           
    
    
    },
getListLignesCredits : function (db,codeUser,callback)
{
    let listLignesCredits = [];
   
   var cursor =  db.collection('LigneCredit').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(ligneCredit,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        ligneCredit.dateOperation = moment(ligneCredit.dateOperationObject).format('L');
        
        listLignesCredits.push(ligneCredit);
   }).then(function(){
    callback(null,listLignesCredits); 
   });

},
deleteLigneCredit: function (db,codeLigneCredit,codeUser,callback){
    const criteria = { "code" : codeLigneCredit, "userCreatorCode":codeUser };
 
            db.collection('LigneCredit').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
           
},

getLigneCredit: function (db,codeLigneCredit,codeUser,callback)
{
    const criteria = { "code" : codeLigneCredit, "userCreatorCode":codeUser };

       const credit =  db.collection('LigneCredit').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                     
},


getListLignesCreditsByCodeCredit : function (db,codeUser,codeCredit,callback)
{
    let listLignesCredits = [];

   var cursor =  db.collection('LigneCredit').find( 
       {"userCreatorCode" : codeUser, "codeCredit": codeCredit}
    );
   cursor.forEach(function(ligneCredit,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        ligneCredit.dateOperation = moment(ligneCredit.dateOperationObject).format('L');
        
        listLignesCredits.push(ligneCredit);
   }).then(function(){
   // console.log('list credits',listCredits);
    callback(null,listLignesCredits); 
   });
   
},


}
module.exports.LignesCredits = LignesCredits;