var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

var DocumentsComptesClients = {
addDocumentCompteClient : function (db,documentCompteClient,codeUser,callback){
    if(documentCompteClient != undefined){
        documentCompteClient.code  = utils.isUndefined(documentCompteClient.code)?'DOC@CCL'+shortid.generate():documentCompteClient.code;
        documentCompteClient.dateReceptionObject = utils.isUndefined(documentCompteClient.dateReceptionObject)? 
        new Date():documentCompteClient.dateReceptionObject;
     
       
        documentCompteClient.userCreatorCode = codeUser;
        documentCompteClient.userLastUpdatorCode = codeUser;
        documentCompteClient.dateCreation = moment(new Date).format('L');
        documentCompteClient.dateLastUpdate = moment(new Date).format('L');
        
            db.collection('DocumentCompteClient').insertOne(
                documentCompteClient,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,documentCompteClient);
                   }
                }
              ) ;


}else{
callback(true,null);
}
},
updateDocumentCompteClient: function (db,documentCompteClient,codeUser, callback){

   
    documentCompteClient.dateReceptionObject = utils.isUndefined(documentCompteClient.dateReceptionObject)? 
    new Date():documentCompteClient.dateReceptionObject;
    documentCompteClient.dateLastUpdate = moment(new Date).format('L');
    
    const criteria = { "code" : documentCompteClient.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
        "dateReceptionObject" : documentCompteClient.dateReceptionObject, 
        "codeCompteClient" : documentCompteClient.codeCompteClient, 
        "description" : documentCompteClient.description,
         "codeOrganisation" : documentCompteClient.codeOrganisation,
         "typeDocument" : documentCompteClient.typeDocument, // cheque , virement , espece 
         "numeroDocument" : documentCompteClient.numeroDocument, 
         "imageFace1":documentCompteClient.imageFace1,
         "imageFace2" : documentCompteClient.imageFace2 , 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": documentCompteClient.dateLastUpdate, 
    } ;
      
            db.collection('DocumentCompteClient').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
    
    
    },
getListDocumentsComptesClients : function (db,codeUser,callback)
{
    let listDocumentsComptesClients = [];
    
   var cursor =  db.collection('DocumentCompteClient').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(docCompteClient,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        docCompteClient.dateReception = moment(docCompteClient.dateReceptionObject).format('L');
        listDocumentsComptesClients.push(docCompteClient);
   }).then(function(){
   //  console.log('list payements credits',listPayementsCredits);
    callback(null,listDocumentsComptesClients); 
   });
   
},
getPageDocumentsComptesClients : function (db,codeUser,pageNumber, nbElementsPerPage , callback)
{
    let listDocComptesClients = [];
   
   var cursor =  db.collection('DocumentCompteClient').find( 
       {"userCreatorCode" : codeUser}
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(docCompteClient,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        docCompteClient.dateReception = moment(docCompteClient.dateReceptionObject).format('L');
        
        listDocComptesClients.push(docCompteClient);
   }).then(function(){
    console.log('list doc compteClient',listDocComptesClients);
    callback(null,listDocComptesClients); 
   });
   
},
deleteDocumentCompteClient: function (db,codeDocumentCompteClient,codeUser,callback){
    const criteria = { "code" : codeDocumentCompteClient, "userCreatorCode":codeUser };
       
            db.collection('DocumentCompteClient').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
    
},

getDocumentCompteClient: function (db,codeDocumentCompteClient,codeUser,callback)
{
    const criteria = { "code" : codeDocumentCompteClient, "userCreatorCode":codeUser };
 
       const docCompteClient =  db.collection('DocumentCompteClient').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    callback(false,result);
                }
            }
        ) ;
                         
},
getListDocumentsComptesClientsByCodeCompteClient : function (db,codeUser,codeCompteClient,callback)
{
    let listDocumentsComptesClients = [];
    
   var cursor =  db.collection('DocumentCompteClient').find( 
       {"userCreatorCode" : codeUser, "codeCompteClient": codeCompteClient}
    );
   cursor.forEach(function(documentCompteClient,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        documentCompteClient.dateReception = moment(documentCompteClient.dateReceptionObject).format('L');
        
        listDocumentsComptesClients.push(documentCompteClient);
   }).then(function(){
  // console.log('list doc credits',listCredits);
    callback(null,listDocumentsComptesClients); 
   });
     //return [];
},
getTotalDocumentsComptesClients : function (db,codeUser, callback)
{
    let listDocComptesClients = [];
   
   var totalDocComptesClients =  db.collection('DocumentCompteClient').find( 
       {"userCreatorCode" : codeUser}
    ).count(function(err,result){
        callback(null,result); 
    }
);

}
}
module.exports.DocumentsComptesClients = DocumentsComptesClients;