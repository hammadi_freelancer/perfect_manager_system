//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var Credits = {
addCredit : function (db,credit,codeUser,callback){
    if(credit != undefined){
        credit.code  = utils.isUndefined(credit.code)?shortid.generate():credit.code;

        credit.dateOperationObject = utils.isUndefined(credit.dateOperationObject)? 
        new Date():credit.dateOperationObject;
        credit.dateDebutPayementObject = utils.isUndefined(credit.dateDebutPayementObject)? 
        new Date():credit.dateDebutPayementObject;
        if(utils.isUndefined(credit.numeroCreditVente))
            {
               const currentD =  moment(new Date);
              // const genereKey = shortid.generate();
               const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.minutes().toString() ;
               credit.numeroCreditVente = 'CRDVEN' + generatedCode;
            }
        credit.userCreatorCode = codeUser;
        credit.userLastUpdatorCode = codeUser;
        credit.dateCreation = moment(new Date).format('L');
        credit.dateLastUpdate = moment(new Date).format('L');
        
            db.collection('Credit').insertOne(
                credit,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
  

}else{
callback(true,null);
}
},
updateCredit: function (db,credit,codeUser, callback){


    credit.dateLastUpdate = moment(new Date).format('L');
    
    credit.dateOperationObject = utils.isUndefined(credit.dateOperationObject)? 
    new Date():credit.dateOperationObject;
    credit.dateDebutPayementObject = utils.isUndefined(credit.dateDebutPayementObject)? 
    new Date():credit.dateDebutPayementObject;

    const criteria = { "code" : credit.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"client" : credit.client, 
     "descriptionEchange" : credit.descriptionEchange,
    "dateOperationObject" : credit.dateOperationObject ,
     "avance" : credit.avance, "montantReste" : credit.montantReste, 
     "montantAPayer":credit.montantAPayer,
     "periodTranche" : credit.periodTranche , "dateDebutPayementObject" : credit.dateDebutPayementObject ,
      "valeurTranche" : credit.valeurTranche, "nbreTranches" : credit.nbreTranches, "listTranches": credit.listTranches,
      "etat": credit.etat,
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": credit.dateLastUpdate, 
    } ;
       
            db.collection('Credit').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
          
    
    
    },
getListCredits : function (db,codeUser,pageNumber, nbElementsPerPage , filter,callback)
{
    let listCredits = [];

    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var cursor =  db.collection('Credit').find( 
     condition
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(credit,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        credit.dateOperation = moment(credit.dateOperationObject).format('L');
        credit.dateDebutPayement = moment(credit.dateDebutPayementObject).format('L');
        
        listCredits.push(credit);
   }).then(function(){
    console.log('list credits',listCredits);
    callback(null,listCredits); 
   });
   
},
deleteCredit: function (db,codeCredit,codeUser,callback){
    const criteria = { "code" : codeCredit, "userCreatorCode":codeUser };
      
            db.collection('Credit').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
 
},

getCredit: function (db,codeCredit,codeUser,callback)
{
    const criteria = { "code" : codeCredit, "userCreatorCode":codeUser };
 
       const credit =  db.collection('Credit').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                     
},
getTotalCredits : function (db,codeUser, filter,callback)
{
    let listCredits = [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var totalCredits =  db.collection('Credit').find( 
      condition
    ).count(function(err,result){
        callback(null,result); 
    }
);
   

     //return [];
},
buildFilterCondition(filterObject,filterData)
{
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.numeroCreditVente))
        {
                filterData["numeroCreditVente"] = filterObject.numeroCreditVente;
        }
         
        if(!utils.isUndefined(filterObject.montantAPayer))
          {
                 
                    filterData["montantAPayer"] = filterObject.montantAPayer;
                    
         }
         if(!utils.isUndefined(filterObject.descriptionEchange))
             {
                    
                       filterData["descriptionEchange"] =filterObject.descriptionEchange;
                       
            }
            if(!utils.isUndefined(filterObject.montantReste))
             {
                    
                       filterData["montantReste"] = filterObject.montantReste;
                       
            }
            if(!utils.isUndefined(filterObject.dateDebutPayementFromObject))
                {
                       
                          filterData["dateDebutPayementObject"] = {$gt:filterObject.dateDebutPayementFromObject};
                          
               }
               if(!utils.isUndefined(filterObject.dateDebutPayementToObject))
                {
                       
                          filterData["dateDebutPayementObject"] = {$lt:filterObject.dateDebutPayementToObject};
                          
               }
            if(!utils.isUndefined(filterObject.periodTranche))
             {
                    
                       filterData["periodTranche"] = filterObject.periodTranche ;
                       
            }
            if(!utils.isUndefined(filterObject.nbreTranches))
             {
                    
                       filterData["nbreTranches"] = filterObject.nbreTranches ;
                       
            }
            if(!utils.isUndefined(filterObject.etat))
             {
                    
                       filterData["etat"] = filterObject.etat ;
                       
            }
       
            if(!utils.isUndefined(filterObject.cinClient))
                    {
                           
                              filterData["client.cin"] = filterObject.cinClient ;
                              
                   }
             if(!utils.isUndefined(filterObject.nomClient))
                    {
                           
                              filterData["client.nom"] = filterObject.nomClient ;
                              
                   }

             if(!utils.isUndefined(filterObject.prenomClient))
              {
                           
                              filterData["client.prenom"] = filterObject.prenomClient ;
                              
              }
              if(!utils.isUndefined(filterObject.raisonClient))
                {
                             
                                filterData["client.raisonSociale"] = filterObject.raisonClient ;
                                
                }
                if(!utils.isUndefined(filterObject.matriculeClient))
                    {
                                 
                                    filterData["client.matriculeFiscale"] = filterObject.matriculeClient ;
                                    
                    }
            if(!utils.isUndefined(filterObject.registreClient))
                        {
                                     
                            filterData["client.registreCommerce"] = filterObject.registreClient ;
                                        
                        }


        }
        return filterData;
},








}
module.exports.Credits = Credits;