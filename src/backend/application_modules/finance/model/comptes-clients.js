var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

var ComptesClients = {
addCompteClient : function (db,compteClient,codeUser,callback){
    if(compteClient != undefined){
        compteClient.code  = utils.isUndefined(compteClient.code)?shortid.generate():compteClient.code;
        compteClient.dateOuvertureObject = utils.isUndefined(compteClient.dateOuvertureObject)? 
        new Date():compteClient.dateOuvertureObject;
        if(utils.isUndefined(compteClient.numeroCompteClient))
            {
               const currentD =  moment(new Date);
              // const genereKey = shortid.generate();
               const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.minutes().toString() ;
               compteClient.numeroCompteClient = 'COMPCL' + generatedCode;
            }
       
       compteClient.userCreatorCode = codeUser;
       compteClient.userLastUpdatorCode = codeUser;
       compteClient.dateCreation = moment(new Date).format('L');
       compteClient.dateLastUpdate = moment(new Date).format('L');
        
            db.collection('CompteClient').insertOne(
                compteClient,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,compteClient);
                   }
                }
              ) ;


}else{
callback(true,null);
}
},
updateCompteClient: function (db,compteClient,codeUser, callback){

   
    compteClient.dateOuvertureObject = utils.isUndefined(compteClient.dateOuvertureObject)? 
    new Date():compteClient.dateOuvertureObject;
    compteClient.dateLastUpdate = moment(new Date).format('L');
    
    const criteria = { "code" : compteClient.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
    "numeroCompteClient" : compteClient.numeroCompteClient, 
    "client" : compteClient.client, 
    "profession" : compteClient.profession, 
    "chiffreAffaires" : compteClient.chiffreAffaire, 
    "valeurVentes" : compteClient.valeurVentes, 
    "valeurCredits" : compteClient.valeurCredits, 
    "valeurPaiements" : compteClient.valeurPaiements, 
    "dateOuvertureObject" : compteClient.dateOuvertureObject ,
    "dateClotureObject" : compteClient.dateClotureObject ,
    "description" : compteClient.description,
     "codeOrganisation" : compteClient.codeOrganisation,
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": compteClient.dateLastUpdate, 
    } ;
      
            db.collection('CompteClient').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
    
    
    },
getListComptesClients : function (db,codeUser,callback)
{
    let listComptesClients = [];
    
   var cursor =  db.collection('CompteClient').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(compteClient,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        compteClient.dateOuverture = moment(compteClient.dateOuvertureObject).format('L');
        compteClient.dateCloture = moment(compteClient.dateClotureObject).format('L');
        
        listComptesClients.push(compteClient);
   }).then(function(){
   //  console.log('list payements credits',listPayementsCredits);
    callback(null,listComptesClients); 
   });
   
},
getPageComptesClients : function (db,codeUser,pageNumber, nbElementsPerPage ,filter, callback)
{
    let listComptesClients = [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var cursor =  db.collection('CompteClient').find(
       condition
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(compteClient,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        compteClient.dateOuverture = moment(compteClient.dateOuvertureObject).format('L');
        compteClient.dateCloture = moment(compteClient.dateClotureObject).format('L');
        
        listComptesClients.push(compteClient);
   }).then(function(){
    console.log('list compteClient',listComptesClients);
    callback(null,listComptesClients); 
   });
   
},
deleteCompteClient: function (db,codeCompteClient,codeUser,callback){
    const criteria = { "code" : codeCompteClient, "userCreatorCode":codeUser };
       
            db.collection('CompteClient').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
    
},

getCompteClient: function (db,codeCompteClient,codeUser,callback)
{
    const criteria = { "code" : codeCompteClient, "userCreatorCode":codeUser };
 
       const compteClient =  db.collection('CompteClient').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    callback(false,result);
                }
            }
        ) ;
                         
},

getTotalComptesClients : function (db,codeUser,filter, callback)
{
    let listComptesClients = [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var totalComptesClients =  db.collection('CompteClient').find( 
condition    ).count(function(err,result){
        callback(null,result); 
    }
);

},




buildFilterCondition(filterObject,filterData)
{
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.numeroCompteClient))
        {
                filterData["numeroCompteClient"] = filterObject.numeroCompteClient;
        }
         
        if(!utils.isUndefined(filterObject.profession))
          {
                 
                    filterData["profession"] = filterObject.profession;
                    
         }
         if(!utils.isUndefined(filterObject.chiffreAffaires))
             {
                    
                       filterData["chiffreAffaires"] =filterObject.chiffreAffaires;
                       
            }
            if(!utils.isUndefined(filterObject.dateOuvertureFromObject))
                {
                       
                          filterData["dateOuvertureObject"] = {$gt:filterObject.dateOuvertureFromObject};
                          
               }
               if(!utils.isUndefined(filterObject.dateOuvertureToObject))
                {
                       
                          filterData["dateOuvertureObject"] = {$lt:filterObject.dateOuvertureToObject};
                          
               }

               if(!utils.isUndefined(filterObject.dateClotureFromObject))
                {
                       
                          filterData["dateClotureObject"] = {$gt:filterObject.dateClotureFromObject};
                          
               }
               if(!utils.isUndefined(filterObject.dateClotureToObject))
                {
                       
                          filterData["dateClotureObject"] = {$lt:filterObject.dateClotureToObject};
                          
               }

            if(!utils.isUndefined(filterObject.etat))
             {
                    
                       filterData["etat"] = filterObject.etat ;
                       
            }
       
            if(!utils.isUndefined(filterObject.cinClient))
                    {
                           
                              filterData["client.cin"] = filterObject.cinClient ;
                              
                   }
             if(!utils.isUndefined(filterObject.nomClient))
                    {
                           
                              filterData["client.nom"] = filterObject.nomClient ;
                              
                   }

             if(!utils.isUndefined(filterObject.prenomClient))
              {
                           
                              filterData["client.prenom"] = filterObject.prenomClient ;
                              
              }
              if(!utils.isUndefined(filterObject.raisonClient))
                {
                             
                                filterData["client.raisonSociale"] = filterObject.raisonClient ;
                                
                }
                if(!utils.isUndefined(filterObject.matriculeClient))
                    {
                                 
                                    filterData["client.matriculeFiscale"] = filterObject.matriculeClient ;
                                    
                    }
            if(!utils.isUndefined(filterObject.registreClient))
                        {
                                     
                            filterData["client.registreCommerce"] = filterObject.registreClient ;
                                        
                        }


        }
        return filterData;
},




}
module.exports.ComptesClients = ComptesClients;