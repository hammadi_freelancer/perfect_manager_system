var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

var DocumentsComptesFournisseurs = {
addDocumentCompteFournisseur : function (db,documentCompteFournisseur,codeUser,callback){
    if(documentCompteFournisseur != undefined){
        documentCompteFournisseur.code  = utils.isUndefined(documentCompteFournisseur.code)?'DOC@CFR'+shortid.generate():documentCompteFournisseur.code;
        documentCompteFournisseur.dateReceptionObject = utils.isUndefined(documentCompteFournisseur.dateReceptionObject)? 
        new Date():documentCompteFournisseur.dateReceptionObject;
     
       
        documentCompteFournisseur.userCreatorCode = codeUser;
        documentCompteFournisseur.userLastUpdatorCode = codeUser;
        documentCompteFournisseur.dateCreation = moment(new Date).format('L');
        documentCompteFournisseur.dateLastUpdate = moment(new Date).format('L');
        
            db.collection('DocumentCompteFournisseur').insertOne(
                documentCompteFournisseur,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,documentCompteFournisseur);
                   }
                }
              ) ;


}else{
callback(true,null);
}
},
updateDocumentCompteFournisseur: function (db,documentCompteFournisseur,codeUser, callback){

   
    documentCompteFournisseur.dateReceptionObject = utils.isUndefined(documentCompteFournisseur.dateReceptionObject)? 
    new Date():documentCompteFournisseur.dateReceptionObject;
    documentCompteFournisseur.dateLastUpdate = moment(new Date).format('L');
    
    const criteria = { "code" : documentCompteFournisseur.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
        "dateReceptionObject" : documentCompteFournisseur.dateReceptionObject, 
        "codeCompteFournisseur" : documentCompteFournisseur.codeCompteFournisseur, 
        "description" : documentCompteFournisseur.description,
         "codeOrganisation" : documentCompteFournisseur.codeOrganisation,
         "typeDocument" : documentCompteFournisseur.typeDocument, // cheque , virement , espece 
         "numeroDocument" : documentCompteFournisseur.numeroDocument, 
         "imageFace1":documentCompteFournisseur.imageFace1,
         "imageFace2" : documentCompteFournisseur.imageFace2 , 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": documentCompteFournisseur.dateLastUpdate, 
    } ;
      
            db.collection('DocumentCompteFournisseur').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
    
    
    },
getListDocumentsComptesFournisseurs : function (db,codeUser,callback)
{
    let listDocumentsComptesFournisseurs = [];
    
   var cursor =  db.collection('DocumentCompteFournisseur').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(docCompteFournisseur,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        docCompteFournisseur.dateReception = moment(docCompteFournisseur.dateReceptionObject).format('L');
        listDocumentsComptesFournisseurs.push(docCompteFournisseur);
   }).then(function(){
   //  console.log('list payements credits',listPayementsCredits);
    callback(null,listDocumentsComptesFournisseurs); 
   });
   
},
getPageDocumentsComptesFournisseurs : function (db,codeUser,pageNumber, nbElementsPerPage , callback)
{
    let listDocComptesFournisseurs = [];
   
   var cursor =  db.collection('DocumentCompteFournisseur').find( 
       {"userCreatorCode" : codeUser}
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(docCompteFournisseur,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        docCompteFournisseur.dateReception = moment(docCompteFournisseur.dateReceptionObject).format('L');
        
        listDocComptesFournisseurs.push(docCompteFournisseur);
   }).then(function(){
    console.log('list doc compteFournisseur',listDocComptesFournisseurs);
    callback(null,listDocComptesFournisseurs); 
   });
   
},
deleteDocumentCompteFournisseur: function (db,codeDocumentCompteFournisseur,codeUser,callback){
    const criteria = { "code" : codeDocumentCompteFournisseur, "userCreatorCode":codeUser };
       
            db.collection('DocumentCompteFournisseur').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
    
},

getDocumentCompteFournisseur: function (db,codeDocumentCompteFournisseur,codeUser,callback)
{
    const criteria = { "code" : codeDocumentCompteFournisseur, "userCreatorCode":codeUser };
 
       const docCompteFournisseur =  db.collection('DocumentCompteFournisseur').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    callback(false,result);
                }
            }
        ) ;
                         
},
getListDocumentsComptesFournisseursByCodeCompteFournisseur : function (db,codeUser,codeCompteFournisseur,callback)
{
    let listDocumentsComptesFournisseurs = [];
    
   var cursor =  db.collection('DocumentCompteFournisseur').find( 
       {"userCreatorCode" : codeUser, "codeCompteFournisseur": codeCompteFournisseur}
    );
   cursor.forEach(function(documentCompteFournisseur,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        documentCompteFournisseur.dateReception = moment(documentCompteFournisseur.dateReceptionObject).format('L');
        
        listDocumentsComptesFournisseurs.push(documentCompteFournisseur);
   }).then(function(){
  // console.log('list doc credits',listCredits);
    callback(null,listDocumentsComptesFournisseurs); 
   });
     //return [];
},
getTotalDocumentsComptesFournisseurs : function (db,codeUser, callback)
{
    let listDocComptesFournisseurs = [];
   
   var totalDocComptesFournisseurs =  db.collection('DocumentCompteFournisseur').find( 
       {"userCreatorCode" : codeUser}
    ).count(function(err,result){
        callback(null,result); 
    }
);

}
}
module.exports.DocumentsComptesFournisseurs = DocumentsComptesFournisseurs;