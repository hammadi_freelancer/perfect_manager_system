//var db = require('../common/MongoDbConnection').getDBConnection;
const mongoClient = require('mongodb').MongoClient;
const url ='mongodb://localhost:27017';
const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var PayementsCreditsAchats = {
addPayementCreditAchats : function (db,payementCreditAchats,codeUser,callback){
    if(payementCreditAchats != undefined){
        payementCreditAchats.code  = utils.isUndefined(payementCreditAchats.code)?shortid.generate():payementCreditAchats.code;
        payementCreditAchats.dateOperationObject = utils.isUndefined(payementCreditAchats.dateOperationObject)? 
        new Date():payementCreditAchats.dateOperationObject;
        payementCreditAchats.userCreatorCode = codeUser;
        payementCreditAchats.userLastUpdatorCode = codeUser;
        payementCreditAchats.dateCreation = moment(new Date).format('L');
        payementCreditAchats.dateLastUpdate = moment(new Date).format('L');
        
    
            db.collection('PayementCreditAchats').insertOne(
                payementCreditAchats,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,payementCreditAchats);
                   }
                }
              ) ;
}else{
callback(true,null);
}
},
updatePayementCreditAchats: function (db,payementCreditAchats,codeUser, callback){

   
    payementCreditAchats.dateOperationObject = utils.isUndefined(payementCreditAchats.dateOperationObject)? 
    new Date():payementCreditAchats.dateOperationObject;
    payementCreditAchats.dateLastUpdate = moment(new Date).format('L');
    const criteria = { "code" : payementCreditAchats.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"montantPaye" : payementCreditAchats.montantPaye, 
    "codeCreditAchats" : payementCreditAchats.codeCreditAchats, 
    "description" : payementCreditAchats.description,
     "codeOrganisation" : payementCreditAchats.codeOrganisation,
    "dateOperationObject" : payementCreditAchats.dateOperationObject ,
     "modePayement" : payementCreditAchats.modePayement, // cheque , virement , espece 
     "numeroCheque" : payementCreditAchats.numeroCheque, 
     "compte":payementCreditAchats.compte,
     "compteAdversaire" : payementCreditAchats.compteAdversaire , 
     "banque" : payementCreditAchats.banque ,
      "banqueAdversaire" : payementCreditAchats.banqueAdversaire, 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": payementCreditAchats.dateLastUpdate, 
    } ;
       
            db.collection('PayementCreditAchats').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
    
    
    },
getListPayementsCreditsAchats : function (db,codeUser,callback)
{
    let listPayementsCreditsAchats = [];
    
   var cursor =  db.collection('PayementCreditAchats').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(payementCreditAchats,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        payementCreditAchats.dateOperation = moment(payementCreditAchats.dateOperationObject).format('L');
        
        listPayementsCreditsAchats.push(payementCreditAchats);
   }).then(function(){
    //console.log('list credits',listCredits);
    callback(null,listPayementsCreditsAchats); 
   });
   

},
deletePayementCreditAchats: function (db,codePayementCreditAchats,codeUser,callback){
    const criteria = { "code" : codePayementCreditAchats, "userCreatorCode":codeUser };
     
            db.collection('PayementCreditAchats').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
    
},

getPayementCreditAchats: function (db,codePayementCreditAchats,codeUser,callback)
{
    const criteria = { "code" : codePayementCreditAchats, "userCreatorCode":codeUser };
 
       const payementCreditAchats =  db.collection('PayementCreditAchats').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    callback(false,result);
                }
            }
        ) ;
                    
},

getListPayementsCreditsAchatsByCodeCredit : function (db,codeUser,codeCreditAchats ,callback)
{
    let listPayementsCreditsAchats = [];
    
   var cursor =  db.collection('PayementCreditAchats').find( 
       {"userCreatorCode" : codeUser, "codeCreditAchats": codeCreditAchats}
    );
   cursor.forEach(function(payementCreditAchats,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        payementCreditAchats.dateOperation = moment(payementCreditAchats.dateOperationObject).format('L');
        payementCreditAchats.dateCheque = moment(payementCreditAchats.dateChequeObject).format('L');
        
        listPayementsCreditsAchats.push(payementCreditAchats);
   }).then(function(){
   // console.log('list credits',listCredits);
    callback(null,listPayementsCreditsAchats); 
   });
   
}
}
module.exports.PayementsCreditsAchats = PayementsCreditsAchats;