//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var PayementsCredits = {
addPayementCredit : function (db,payementCredit,codeUser,callback){
    if(payementCredit != undefined){
        payementCredit.code  = utils.isUndefined(payementCredit.code)?shortid.generate():payementCredit.code;
        payementCredit.dateOperationObject = utils.isUndefined(payementCredit.dateOperationObject)? 
        new Date():payementCredit.dateOperationObject;
        payementCredit.userCreatorCode = codeUser;
        payementCredit.userLastUpdatorCode = codeUser;
        payementCredit.dateCreation = moment(new Date).format('L');
        payementCredit.dateLastUpdate = moment(new Date).format('L');
        
       /* mongoClient.connect(url,function(err,clientDB){
            if(err)
             {
                callback(true,"ERROR_DATABASE_CONNECTION");
             }
            console.log('the client connected is ');
            console.log(clientDB)
            const db = clientDB.db(dbName);*/
            db.collection('PayementCredit').insertOne(
                payementCredit,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,payementCredit);
                   }
                }
              ) ;
 

}else{
callback(true,null);
}
},
updatePayementCredit: function (db,payementCredit,codeUser, callback){

   
    payementCredit.dateOperationObject = utils.isUndefined(payementCredit.dateOperationObject)? 
    new Date():payementCredit.dateOperationObject;
    payementCredit.dateLastUpdate = moment(new Date).format('L');
    
    const criteria = { "code" : payementCredit.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"montantPaye" : payementCredit.montantPaye, 
    "codeCredit" : payementCredit.codeCredit, 
    "description" : payementCredit.description,
     "codeOrganisation" : payementCredit.codeOrganisation,
    "dateOperationObject" : payementCredit.dateOperationObject ,
     "modePayement" : payementCredit.modePayement, // cheque , virement , espece 
     "numeroCheque" : payementCredit.numeroCheque, 
     "compte":payementCredit.compte,
     "compteAdversaire" : payementCredit.compteAdversaire , 
     "banque" : payementCredit.banque ,
      "banqueAdversaire" : payementCredit.banqueAdversaire, 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": payementCredit.dateLastUpdate, 
    } ;
     
            db.collection('PayementCredit').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
      
    
    },
getListPayementsCredits : function (db,codeUser,callback)
{
    let listPayementsCredits = [];
    
   var cursor =  db.collection('PayementCredit').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(payementCredit,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        payementCredit.dateOperation = moment(payementCredit.dateOperationObject).format('L');
        payementCredit.dateCheque = moment(payementCredit.dateChequeObject).format('L');
        
        listPayementsCredits.push(payementCredit);
   }).then(function(){
    console.log('list payements credits',listPayementsCredits);
    callback(null,listPayementsCredits); 
   });
   

},
deletePayementCredit: function (db,codePayementCredit,codeUser,callback){
    const criteria = { "code" : codePayementCredit, "userCreatorCode":codeUser };
     
            db.collection('PayementCredit').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
 
},

getPayementCredit: function (db,codePayementCredit,codeUser,callback)
{
    const criteria = { "code" : codePayementCredit, "userCreatorCode":codeUser };
   
       const credit =  db.collection('PayementCredit').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                    
},


getListPayementsCreditsByCodeCredit : function (db,codeUser,codeCredit,callback)
{
    let listPayementsCredits = [];
  
   var cursor =  db.collection('PayementCredit').find( 
       {"userCreatorCode" : codeUser, "codeCredit": codeCredit}
    );
   cursor.forEach(function(payementCredit,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        payementCredit.dateOperation = moment(payementCredit.dateOperationObject).format('L');
        payementCredit.dateCheque = moment(payementCredit.dateChequeObject).format('L');
        
        listPayementsCredits.push(payementCredit);
   }).then(function(){
   // console.log('list credits',listCredits);
    callback(null,listPayementsCredits); 
   });
   
}


}
module.exports.PayementsCredits = PayementsCredits;