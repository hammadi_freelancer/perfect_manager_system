//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var PayementsDemandesVentes = {
addPayementDemandeVentes : function (db,payementDemandeVentes,codeUser,callback){
    if(payementDemandeVentes != undefined){
        payementDemandeVentes.code  = utils.isUndefined(payementDemandeVentes.code)?shortid.generate():payementDemandeVentes.code;
        payementDemandeVentes.dateOperationObject = utils.isUndefined(payementDemandeVentes.dateOperationObject)? 
        new Date():payementDemandeVentes.dateOperationObject;
        payementDemandeVentes.userCreatorCode = codeUser;
        payementDemandeVentes.userLastUpdatorCode = codeUser;
        payementDemandeVentes.dateCreation = moment(new Date).format('L');
        payementDemandeVentes.dateLastUpdate = moment(new Date).format('L');
        
      
            db.collection('PayementDemandeVentes').insertOne(
                payementDemandeVentes,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,payementDemandeVentes);
                   }
                }
              ) ;


}else{
callback(true,null);
}
},
updatePayementDemandeVentes: function (db,payementDemandeVentes,codeUser, callback){

   
    payementDemandeVentes.dateOperationObject = utils.isUndefined(payementDemandeVentes.dateOperationObject)? 
    new Date():payementDemandeVentes.dateOperationObject;
    payementDemandeVentes.dateLastUpdate = moment(new Date).format('L');
    
    const criteria = { "code" : payementDemandeVentes.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"montantPaye" : payementDemandeVentes.montantPaye, 
    "codeDemandeVentes" : payementDemandeVentes.codeDemandeVentes, 
    "description" : payementDemandeVentes.description,
     "codeOrganisation" : payementDemandeVentes.codeOrganisation,
    "dateOperationObject" : payementDemandeVentes.dateOperationObject ,
     "modePayement" : payementDemandeVentes.modePayement, // cheque , virement , espece 
     "numeroCheque" : payementDemandeVentes.numeroCheque, 
     "compte":payementDemandeVentes.compte,
     "compteAdversaire" : payementDemandeVentes.compteAdversaire , 
     "banque" : payementDemandeVentes.banque ,
      "banqueAdversaire" : payementDemandeVentes.banqueAdversaire, 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": payementDemandeVentes.dateLastUpdate, 
    } ;
      
            db.collection('PayementDemandeVentes').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
       
    
    },
getListPayementsDemandesVentes : function (db,codeUser,callback)
{
    let listPayementsDemandesVentes  = [];
   
   var cursor =  db.collection('PayementDemandeVentes').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(payementDemandeVentes,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        payementDemandeVentes.dateOperation = moment(payementDemandeVentes.dateOperationObject).format('L');
        
        listPayementsDemandesVentes.push(payementDemandeVentes);
   }).then(function(){
  //  console.log('list credits',listPayementsFacturesVentes);
    callback(null,listPayementsDemandesVentes); 
   });
   
     //return [];
},
deletePayementDemandeVentes: function (db,codePayementDemandeVentes,codeUser,callback){
    const criteria = { "code" : codePayementDemandeVentes, "userCreatorCode":codeUser };
      
            db.collection('PayementDemandeVentes').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;

},

getPayementDemandeVentes: function (db,codePayementDemandeVentes,codeUser,callback)
{
    const criteria = { "code" : codePayementDemandeVentes, "userCreatorCode":codeUser };
 
       const payementDemandeVentes =  db.collection('PayementDemandeVentes').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    callback(false,result);
                }
            }
        ) ;
                  
},

getListPayementsDemandesVentesByCodeDemande : function (db,codeDemande, codeUser,callback)
{
    let listPayementsDemandeVentes = [];
  
   var cursor =  db.collection('PayementDemandeVentes').find( 
       {"userCreatorCode" : codeUser, "codeDemandeVentes": codeDemandeVentes}
    );
   cursor.forEach(function(payementDemandeVentes,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        payementDemandeVentes.dateOperation = moment(payementDemandeVentes.dateOperationObject).format('L');
        payementDemandeVentes.dateCheque = moment(payementDemandeVentes.dateChequeObject).format('L');
        
        listPayementsDemandeVentes.push(payementDemandeVentes);
   }).then(function(){
   // console.log('list credits',listCredits);
    callback(null,listPayementsDemandeVentes); 
   });
   

     //return [];
},
}
module.exports.PayementsDemandesVentes = PayementsDemandesVentes;