//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var LignesCreditsAchats = {
addLigneCreditAchats : function (db,ligneCreditAchats,codeUser,callback){
    if(ligneCreditAchats != undefined){
        ligneCreditAchats.code  = utils.isUndefined(ligneCreditAchats.code)?shortid.generate():ligneCreditAchats.code;
        ligneCreditAchats.dateOperationObject = utils.isUndefined(ligneCreditAchats.dateOperationObject)? 
        new Date():ligneCreditAchats.dateOperationObject;
        ligneCreditAchats.userCreatorCode = codeUser;
        ligneCreditAchats.userLastUpdatorCode = codeUser;
        ligneCreditAchats.dateCreation = moment(new Date).format('L');
        ligneCreditAchats.dateLastUpdate = moment(new Date).format('L');
         db.collection('LigneCreditAchats').insertOne(
                ligneCreditAchats,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,ligneCreditAchats);
                   }
                }
              ) ;


}else{
callback(true,null);
}
},
updateLigneCreditAchats: function (db,ligneCreditAchats,codeUser, callback){

   
    ligneCreditAchats.dateOperationObject = utils.isUndefined(ligneCreditAchats.dateOperationObject)? 
    new Date():ligneCreditAchats.dateOperationObject;
    ligneCreditAchats.dateLastUpdate = moment(new Date).format('L');
    
    const criteria = { "code" : ligneCreditAchats.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"montantAPayer" : ligneCreditAchats.montantAPayer, 
    "codeCredit" : ligneCreditAchats.codeCreditAchats, 
    "description" : ligneCreditAchats.description,
    "etat" : ligneCreditAchats.etat,
     "codeOrganisation" : ligneCreditAchats.codeOrganisation,
    "dateOperationObject" : ligneCreditAchats.dateOperationObject ,
    } ;
        
            db.collection('LigneCreditAchats').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
    
    
    },
getListLignesCreditsAchats : function (db,codeUser,callback)
{
    let listLignesCreditsAchats = [];
     
   var cursor =  db.collection('LigneCreditAchats').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(ligneCreditAchats,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        ligneCreditAchats.dateOperation = moment(ligneCreditAchats.dateOperationObject).format('L');
        
        listLignesCreditsAchats.push(ligneCreditAchats);
   }).then(function(){
    callback(null,listLignesCreditsAchats); 
   });
     //return [];
},
deleteLigneCreditAchats: function (db,codeLigneCreditAchats,codeUser,callback){
    const criteria = { "code" : codeLigneCreditAchats, "userCreatorCode":codeUser };
    
            db.collection('LigneCreditAchats').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
},

getLigneCreditAchats: function (db,codeLigneCreditAchats,codeUser,callback)
{
    const criteria = { "code" : codeLigneCreditAchats, "userCreatorCode":codeUser };
 
       const credit =  db.collection('LigneCreditAchats').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                    
},


getListLignesCreditsAchatsByCodeCredit : function (db,codeUser,codeCreditAchats,callback)
{
    let listLignesCreditsAchats = [];
    
   var cursor =  db.collection('LigneCreditAchats').find( 
       {"userCreatorCode" : codeUser, "codeCreditAchats": codeCreditAchats}
    );
   cursor.forEach(function(ligneCreditAchats,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        ligneCreditAchats.dateOperation = moment(ligneCreditAchats.dateOperationObject).format('L');
        
        listLignesCreditsAchats.push(ligneCreditAchats);
   }).then(function(){
   // console.log('list credits',listCredits);
    callback(null,listLignesCreditsAchats); 
   });

     //return [];
},


}
module.exports.LignesCreditsAchats = LignesCreditsAchats;