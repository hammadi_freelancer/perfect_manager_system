//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var AjournementsCreditsAchats = {
addAjournementCreditAchats  : function (db,ajournementCreditAchats ,codeUser,callback){
    if(ajournementCreditAchats  != undefined){
        ajournementCreditAchats.code  = utils.isUndefined(ajournementCreditAchats.code)?shortid.generate():ajournementCreditAchats.code;
        ajournementCreditAchats.dateOperationObject = utils.isUndefined(ajournementCreditAchats.dateOperationObject)? 
        new Date():ajournementCreditAchats.dateOperationObject;
        ajournementCreditAchats.userCreatorCode = codeUser;
        ajournementCreditAchats.userLastUpdatorCode = codeUser;
        ajournementCreditAchats.dateCreation = moment(new Date).format('L');
        ajournementCreditAchats.dateLastUpdate = moment(new Date).format('L');
        
        
            db.collection('AjournementCreditAchats').insertOne(
                ajournementCreditAchats,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,ajournementCreditAchats);
                   }
                }
              ) ;
            
}else{
callback(true,null);
}
},
updateAjournementCreditAchats: function (db,ajournementCreditAchats,codeUser, callback){

   
    ajournementCreditAchats.dateOperationObject = utils.isUndefined(ajournementCreditAchats.dateOperationObject)? 
    new Date():ajournementCreditAchats.dateOperationObject;
    ajournementCreditAchats.dateLastUpdate = moment(new Date).format('L');
    
    const criteria = { "code" : ajournementCreditAchats.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
    "codeCreditAchats" : ajournementCreditAchats.codeCreditAchats, 
    "description" : ajournementCreditAchats.description,
     "codeOrganisation" : ajournementCreditAchats.codeOrganisation,
    "dateOperationObject" : ajournementCreditAchats.dateOperationObject ,
    "nouvelleDatePayementObject" : ajournementCreditAchats.nouvelleDatePayementObject ,
    "motif" : ajournementCreditAchats.motif ,
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": ajournementCreditAchats.dateLastUpdate, 
    } ;
        
            db.collection('AjournementCreditAchats').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
     
    
    },
getListAjournementsCreditsAchats : function (db,codeUser,callback)
{
    let listAjournementsCreditsAchats = [];
   
   var cursor =  db.collection('AjournementCreditAchats').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(ajournementCreditAchats,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        ajournementCreditAchats.dateOperation = moment(ajournementCreditAchats.dateOperationObject).format('L');
        ajournementCreditAchats.nouvelleDatePayement = moment(ajournementCreditAchats.nouvelleDatePayementObject).format('L');
        
        listAjournementsCreditsAchats.push(ajournementCreditAchats);
   }).then(function(){
   //  console.log('list payements credits',listPayementsCredits);
    callback(null,listAjournementsCredits); 
   });

},
deleteAjournementCreditAchats: function (db,codeAjournementCreditAchats,codeUser,callback){
    const criteria = { "code" : codeAjournementCreditAchats, "userCreatorCode":codeUser };
      
            db.collection('AjournementCreditAchats').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
    
},

getAjournementCreditAchats: function (db,codeAjournementCreditAchats,codeUser,callback)
{
    const criteria = { "code" : codeAjournementCreditAchats, "userCreatorCode":codeUser };
 
       const ajournmentCreditAchats =  db.collection('AjournementCreditAchats').findOne(
            criteria , function(err,result){
                if(err){
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;               
},


getListAjournementsCreditsAchatsByCodeCredit : function (db,codeUser,codeCreditAchats,callback)
{
    console.log(' the code credit achats is ');
    console.log(codeCreditAchats);
    
    let listAjournementsCreditsAchats = [];
   
   var cursor =  db.collection('AjournementCreditAchats').find( 
       {"userCreatorCode" : codeUser, "codeCreditAchats": codeCreditAchats}
    );
   cursor.forEach(function(ajournementCreditAchats,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        ajournementCreditAchats.dateOperation = moment(ajournementCreditAchats.dateOperationObject).format('L');
        ajournementCreditAchats.nouvelleDatePayement = moment(ajournementCreditAchats.nouvelleDatePayementObject).format('L');
        
        listAjournementsCreditsAchats.push(ajournementCreditAchats);
   }).then(function(){
   // console.log('list credits',listCredits);
    callback(null,listAjournementsCreditsAchats); 
   });
   
}



}
module.exports.AjournementsCreditsAchats = AjournementsCreditsAchats;