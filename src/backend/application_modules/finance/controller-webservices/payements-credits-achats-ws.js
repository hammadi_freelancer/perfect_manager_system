
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var payementsCreditsAchats = require('../model/payements-credits-achats').PayementsCreditsAchats;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addPayementCreditAchats',function(req,res,next){
    
       console.log(" ADD PAYEMENT CREDIT ACHATS IS CALLED") ;
       console.log(" THE PAYEMENT CREDIT ACHATS TO ADDED IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = payementsCreditsAchats.addPayementCreditAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,payementCreditsAchatsAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_PAYEMENT_CREDIT_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(payementCreditsAchatsAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updatePayementCreditAchats',function(req,res,next){
    
       console.log(" UPDATE PAYEMENT CREDIT ACHATS IS CALLED") ;
       console.log(" THE PAYEMENT CREDIT ACHATS TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = payementsCreditsAchats.updatePayementCreditAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,payementCreditAchatsUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_PAYEMENT_CREDIT_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(payementCreditAchatsUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getPayementsCreditsAchats',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    payementsCreditsAchats.getListPayementsCreditsAchats(
        req.app.locals.dataBase,decoded.userData.code, function(err,listPayementsCreditsAchats){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_PAYEMENTS_CREDITS_ACHATS',
                 error_content: err
             });
          }else{
        
         return res.json(listPayementsCreditsAchats);
          }
      });

  });
  router.get('/getPayementCreditAchat/:codePayementCreditAchat',function(req,res,next){
    console.log("GET PAYEMENT CREDIT ACHAT IS CALLED");
    console.log(req.params['codePayementCreditAchat']);
   
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    payementsCreditsAchats.getPayementCreditAchats(
        req.app.locals.dataBase,req.params['codePayementCreditAchat'],decoded.userData.code,
     function(err,payementCreditAchat){
       console.log("GET  PAYEMENT CREDITACHATS : RESULT");
       console.log(payementCreditAchat);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_PAYEMENT_CREDIT_ACHATS',
               error_content: err
           });
        }else{
      
       return res.json(payementCreditAchat);
        }
    });
})

  router.delete('/deletePayementCreditAchats/:codePayementCreditAchats',function(req,res,next){
    
       console.log(" DELETE PAYEMENT CREDIT ACHATS IS CALLED") ;
       console.log(" THE PAYEMENT CREDIT ACHATS TO DELETE IS :",req.params['codePayementCreditAchats']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = payementsCreditsAchats.deletePayementCreditAchats(
        req.app.locals.dataBase,req.params['codePayementCreditAchats'],decoded.userData.code,
        function(err,codePayementCreditAchats){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_PAYEMENT_CREDIT_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codePayementCreditAchats']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

   router.get('/getPayementsCreditsAchatsByCodeCredit/:codeCreditAchats',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    payementsCreditsAchats.getListPayementsCreditsAchatsByCodeCredit(
        req.app.locals.dataBase,decoded.userData.code,req.params['codeCreditAchats'], function(err,listPayementsCreditsAchats){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_PAYEMENTS_CREDITS_ACHATS_BY_CODE_CREDIT',
                 error_content: err
             });
          }else{
        
         return res.json(listPayementsCreditsAchats);
          }
      });

  })
  module.exports.routerPayementsCreditsAchats = router;