
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var payementsDemandesVentes = require('../model/payements-demandes-ventes').PayementsDemandesVentes;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addPayementDemandeVentes',function(req,res,next){
    
       console.log(" ADD PAYEMENT DEMANDE VENTE IS CALLED") ;
       console.log(" THE PAYEMENT DEMANDE VENTE TO ADDED IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = payementsDemandesVentes.addPayementDemandeVentes(
        req.app.locals.dataBase,req.body,decoded.userData.code,
        function(err,payementRelVenteAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_PAYEMENT_REL_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(payementRelVenteAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updatePayementDemandeVentes',function(req,res,next){
    
       console.log(" UPDATE PAYEMENT DEMANDE VENTES  IS CALLED") ;
       console.log("THE PAYEMENT DEMANDE VENTES TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = payementsDemandesVentes.updatePayementDemandeVentes(
        req.app.locals.dataBase,req.body,decoded.userData.code,
         function(err,payementsRelVentesUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_PAYEMENT_DEMANDES_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(payementsRelVentesUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getPayementsDemandesVentes',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
  
    payementsDemandesVentes.getListPayementsDemandesVentes(
        req.app.locals.dataBase,decoded.userData.code, function(err,listPayementsRelVentes){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_PAYEMENTS_DEMANDES_VENTES',
                 error_content: err
             });
          }else{
        
         return res.json(listPayementsRelVentes);
          }
      });

  });
  router.get('/getPayementDemandeVentes/:codePayementDemandeVentes',function(req,res,next){
    console.log("GET PAYEMENT DEMANDE VENTE IS CALLED");
    console.log(req.params['codePayementDemandeVentes']);
   
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    payementsDemandesVentes.getPayementDemandeVentes(
        req.app.locals.dataBase,req.params['codePayementDemandeVentes'],decoded.userData.code,
     function(err,payementRelVentes){
       console.log("GET  PAYEMENT DEMANDE VENTES : RESULT");
       console.log(payementRelVentes);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_PAYEMENT_DEMANDE_VENTES',
               error_content: err
           });
        }else{
      
       return res.json(payementRelVentes);
        }
    });
})

  router.delete('/deletePayementDemandeVentes/:codePayementDemandeVentes',function(req,res,next){
    
       console.log(" DELETE PAYEMENT DEMANDE VENTES IS CALLED") ;
       console.log(" THE PAYEMENT DEMANDE VENTES TO DELETE IS :",req.params['codePayementDemandeVentes']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = payementsDemandesVentes.deletePayementDemandeVentes(
        req.app.locals.dataBase,req.params['codePayementDemandeVentes'],decoded.userData.code,
        function(err,codePayementDemandeVentes){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_PAYEMENT_DEMANDE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codePayementDemandeVentes']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

   router.get('/getPayementsDemandesVentesByCodeDemande/:codeDemandeVentes',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    payementsDemandesVentes.getListPayementsDemandesVentesByCodeDemande(
        req.app.locals.dataBase,req.params['codeDemandeVentes'],decoded.userData.code, function(err,listPayementsRelVen){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_PAYEMENTS_DEMANDE_VENTES_BY_CODE_DEMANDE',
                 error_content: err
             });
          }else{
        
         return res.json(listPayementsRelVen);
          }
      });

  })

  module.exports.routerPayementsDemandesVentes = router;