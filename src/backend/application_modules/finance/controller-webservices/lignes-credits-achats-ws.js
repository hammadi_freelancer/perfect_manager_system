
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var lignesCreditsAchats = require('../model/lignes-credits-achats').LignesCreditsAchats;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addLigneCreditAchats',function(req,res,next){
    
       console.log(" ADD LIGNE CREDIT  ACHATS IS CALLED") ;
       console.log(" THE LIGNE CREDIT ACHATS  TO ADDED IS :",req.body);
      
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = lignesCreditsAchats.addLigneCreditAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,ligneCreditAchatsAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_LIGNE_CREDIT_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(ligneCreditAchatsAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateLigneCreditAchats',function(req,res,next){
    
       console.log(" UPDATE LIGNE CREDIT  ACHATS IS CALLED") ;
       console.log(" THE LIGNE CREDIT ACHATS  TO UPDATE IS :",req.body);
   
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = lignesCreditsAchats.updateLigneCreditAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,ligneCreditAchatsUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_LIGNE_CREDIT_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(ligneCreditAchatsUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getLignesCreditsAchats',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    lignesCreditsAchats.getListLignesCreditsAchats(
        req.app.locals.dataBase,decoded.userData.code, function(err,listLignesCreditsAchats){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_LIGNES_CREDITS_ACHATS',
                 error_content: err
             });
          }else{
        
         return res.json(listLignesCreditsAchats);
          }
      });

  });
  router.get('/getLigneCreditAchats/:codeLigneCreditAchats',function(req,res,next){
    console.log("GET LIGNE CREDIT ACHATS IS CALLED");
   
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    lignesCreditsAchats.getLigneCreditAchats(
        req.app.locals.dataBase,req.params['codeLigneCreditAchats'],decoded.userData.code,
     function(err,ligneCreditAchats){
       console.log("GET  LIGNE CREDIT ACHATS: RESULT");
       console.log(ligneCreditAchats);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIGNE_CREDIT_ACHATS',
               error_content: err
           });
        }else{
      
       return res.json(ligneCreditAchats);
        }
    });
})

  router.delete('/deleteLigneCreditAchats/:codeLigneCreditAchats',function(req,res,next){
    
       console.log(" DELETE LIGNE CREDIT  ACHATS IS CALLED") ;
       console.log(" THE LIGNE CREDIT  ACHATS TO DELETE IS :",req.params['codeLigneCreditAchats']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = lignesCreditsAchats.deleteLigneCreditAchats(
        req.app.locals.dataBase,req.params['codeLigneCreditAchats'],decoded.userData.code,
        function(err,codeLigneCreditAchats){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_LIGNE_CREDIT_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeLigneCreditAchats']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getLignesCreditsAchatsByCodeCredit/:codeCreditAchats',function(req,res,next){
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
    lignesCreditsAchats.getListLignesCreditsAchatsByCodeCredit(
        req.app.locals.dataBase,decoded.userData.code,req.params['codeCreditAchats'],
         function(err,listLignesCreditsAchats){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_LIGNES_CREDITS_ACHATS_BY_CODE_CREDIT',
                 error_content: err
             });
          }else{
        
         return res.json(listLignesCreditsAchats);
          }
      });

  });
  module.exports.routerLignesCreditsAchats = router;