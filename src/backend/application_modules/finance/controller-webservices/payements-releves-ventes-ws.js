
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var payementsRelevesVentes = require('../model/payements-releves-ventes').PayementsRelevesVentes;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addPayementReleveVentes',function(req,res,next){
    
       console.log(" ADD PAYEMENT RELEVE VENTE IS CALLED") ;
       console.log(" THE PAYEMENT RELEVE VENTE TO ADDED IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = payementsRelevesVentes.addPayementReleveVentes(req.app.locals.dataBase,req.body,decoded.userData.code,
        function(err,payementRelVenteAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_PAYEMENT_REL_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(payementRelVenteAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updatePayementReleveVentes',function(req,res,next){
    
       console.log(" UPDATE PAYEMENT RELEVE VENTES  IS CALLED") ;
       console.log("THE PAYEMENT RELEVE VENTES TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = payementsRelevesVentes.updatePayementReleveVentes(
        req.app.locals.dataBase,req.body,decoded.userData.code,
         function(err,payementsRelVentesUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_PAYEMENT_RELEVES_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(payementsRelVentesUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getPayementsRelevesVentes',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
  
    payementsRelevesVentes.getListPayementsRelevesVentes(
        req.app.locals.dataBase,decoded.userData.code, function(err,listPayementsRelVentes){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_PAYEMENTS_RELEVES_VENTES',
                 error_content: err
             });
          }else{
        
         return res.json(listPayementsRelVentes);
          }
      });

  });
  router.get('/getPayementReleveVentes/:codePayementReleveVentes',function(req,res,next){
    console.log("GET PAYEMENT RELEVE VENTE IS CALLED");
    console.log(req.params['codePayementReleveVentes']);
   
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    payementsRelevesVentes.getPayementReleveVentes(
        req.app.locals.dataBase,req.params['codePayementReleveVentes'],decoded.userData.code,
     function(err,payementRelVentes){
       console.log("GET  PAYEMENT RELEVE VENTES : RESULT");
       console.log(payementRelVentes);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_PAYEMENT_RELEVE_VENTES',
               error_content: err
           });
        }else{
      
       return res.json(payementRelVentes);
        }
    });
})

  router.delete('/deletePayementReleveVentes/:codePayementReleveVentes',function(req,res,next){
    
       console.log(" DELETE PAYEMENT RELEVE VENTES IS CALLED") ;
       console.log(" THE PAYEMENT RELEVE VENTES TO DELETE IS :",req.params['codePayementReleveVentes']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = payementsRelevesVentes.deletePayementReleveVentes(
        req.app.locals.dataBase,req.params['codePayementReleveVentes'],decoded.userData.code,
        function(err,codePayementReleveVentes){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_PAYEMENT_RELEVE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codePayementReleveVentes']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

   router.get('/getPayementsReleveVentesByCodeReleve/:codeReleveVentes',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    payementsRelevesVentes.getListPayementsRelevesVentesByCodeReleve(
        req.app.locals.dataBase,req.params['codeReleveVentes'],decoded.userData.code, function(err,listPayementsRelVen){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_PAYEMENTS_RELEVE_VENTES_BY_CODE_RELEVE',
                 error_content: err
             });
          }else{
        
         return res.json(listPayementsRelVen);
          }
      });

  })

  module.exports.routerPayementsRelevesVentes = router;