
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var documentsComptesFournisseurs = require('../model/documents-comptes-fournisseurs').DocumentsComptesFournisseurs;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addDocumentCompteFournisseur',function(req,res,next){
    
       console.log(" ADD DOCUMENT  COMPTE FOURNISSEUR  IS CALLED") ;
       console.log(" THE DOCUMENT COMPTE FOURNISSEUR  TO ADDED IS :",req.body);
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = documentsComptesFournisseurs.addDocumentCompteFournisseur(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,documentCompteFournisseursAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_DOCUMENT_COMPTE_FOURNISSEUR',
                error_content: err
            });
         }else{
       
        return res.json(documentCompteFournisseursAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE FOURNISSEUR IS ",result);
     
   });
router.put('/updateDocumentCompteFournisseur',function(req,res,next){
    
       console.log(" UPDATE DOCUMENT COMPTE FOURNISSEUR  IS CALLED") ;
       // console.log(" THE DOCUMENT COMPTE FOURNISSEUR  TO UPDATE IS :",req.body);
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result =documentsComptesFournisseurs.updateDocumentCompteFournisseur(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,documentCompteFournisseurUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_DOCUMENT_COMPTE_FOURNISSEUR',
                error_content: err
            });
         }else{
       
        return res.json(documentCompteFournisseurUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE FOURNISSEUR IS ",result);
     
   });
router.get('/getDocumentsComptesFournisseurs',function(req,res,next){
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

        documentsComptesFournisseurs.getListDocumentsComptesFournisseurs(
        req.app.locals.dataBase,decoded.userData.code, function(err,listDocumentsComptesFournisseurs){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_COMPTE_FOURNISSEURS',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsComptesFournisseurs);
          }
      });

  });
  router.get('/getDocumentCompteFournisseur/:codeDocumentCompteFournisseur',function(req,res,next){
    console.log("GET DOCUMENT COMPTE FOURNISSEUR  IS CALLED");
    console.log(req.params['codeDocumentCompteFournisseur']);
   
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
        documentsComptesFournisseurs.getDocumentCompteFournisseur(
        req.app.locals.dataBase,req.params['codeDocumentCompteFournisseur'],decoded.userData.code,
     function(err,documentCompteFournisseur){
       console.log("GET  DOCUMENT COMPTE FOURNISSEUR : RESULT");
       console.log(documentCompteFournisseur);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_DOCUMENT_COMPTE_FOURNISSEUR',
               error_content: err
           });
        }else{
      
       return res.json(documentCompteFournisseur);
        }
    });
})

  router.delete('/deleteDocumentCompteFournisseur/:codeDocumentCompteFournisseur',function(req,res,next){
    
       console.log(" DELETE DOCUMENT COMPTE FOURNISSEUR  IS CALLED") ;
       console.log(" THE DOCUMENT COMPTE FOURNISSEUR  TO DELETE IS :",req.params['codeDocumentCompteFournisseur']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = documentsComptesFournisseurs.deleteDocumentCompteFournisseur(
       req.app.locals.dataBase,req.params['codeDocumentCompteFournisseur'],decoded.userData.code,
        function(err,codeDocumentCompteFournisseur){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_DOCUMENT_COMPTE_FOURNISSEUR',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeDocumentCompteFournisseur']);
       }
    }
)
      // console.log("THE RESULT OF ADDING THE FOURNISSEUR IS ",result);
     
   });

 router.get('/getDocumentsComptesFournisseursByCodeCompteFournisseur/:codeCompteFournisseur',function(req,res,next){
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
        documentsComptesFournisseurs.getListDocumentsComptesFournisseursByCodeCompteFournisseur(req.app.locals.dataBase,
        decoded.userData.code,req.params['codeCompteFournisseur'],
         function(err,listDocumentsComptesFournisseurs){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_COMPTE FOURNISSEURS_BY_CODE_COMPTE_FOURNISSEUR',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsComptesFournisseurs);
          }
      });

  });
  module.exports.routerDocumentsComptesFournisseurs = router;