
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var comptesFournisseurs = require('../model/comptes-fournisseurs').ComptesFournisseurs;

// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addCompteFournisseur',function(req,res,next){
    
       console.log(" ADD COMPTE FOURNISSEUR IS CALLED") ;
       console.log(" THE COMPTE FOURNISSEUR  TO ADDED IS :",req.body);
    
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = comptesFournisseurs.addCompteFournisseur(
        req.app.locals.dataBase,req.body,decoded.userData.code,
        function(err,compteFournisseurAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_COMPTE_FOURNISSEUR',
                error_content: err
            });
         }else{
       
        return res.json(compteFournisseurAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateCompteFournisseur',function(req,res,next){
    
       console.log(" UPDATE COMPTE FOURNISSEUR IS CALLED") ;
       console.log(" THE COMPTE FOURNISSEUR  TO UPDATE IS :",req.body);
      
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = comptesFournisseurs.updateCompteFournisseur(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,compteFournisseurUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_COMPTE_FOURNISSEUR',
                error_content: err
            });
         }else{
       
        return res.json(compteFournisseurUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getPageComptesFournisseurs',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    comptesFournisseurs.getPageComptesFournisseurs(req.app.locals.dataBase,
        decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,req.query.filter,
        function(err,listComptesFournisseurs){
         console.log("GET PAGE COMPTES FOURNISSEURS : RESULT");
        // console.log(listCredits);
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_COMPTES_FOURNISSEURS',
                 error_content: err
             });
          }else{
        
         return res.json(listComptesFournisseurs);
          }
      });

  });
  router.get('/getCompteFournisseur/:codeCompteFournisseur',function(req,res,next){
    console.log("GET COMPTE FOURNISSEUR IS CALLED");
    
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    comptesFournisseurs.getCompteFournisseur(
        req.app.locals.dataBase,req.params['codeCompteFournisseur'],
        decoded.userData.code, function(err,compteFournisseur)
        
        {
       console.log("GET  COMPTE FOURNISSEUR  : RESULT");
       console.log(compteFournisseur);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_COMPTE_FOURNISSEUR',
               error_content: err
           });
        }else{
      
       return res.json(compteFournisseur);
        }
    });
})

  router.delete('/deleteCompteFournisseur/:codeCompteFournisseur',function(req,res,next){
    
       console.log(" DELETE COMPTE FOURNISSEUR IS CALLED") ;
       console.log(" THE COMPTE FOURNISSEUR  TO DELETE IS :",req.params['codeCompteFournisseur']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = comptesFournisseurs.deleteCompteFournisseur(
        req.app.locals.dataBase,req.params['codeCompteFournisseur'],decoded.userData.code, function(err,codeCompteFournisseur){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_COMPTE_FOURNISSEUR',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeCompteFournisseur']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });



 router.get('/getTotalComptesFournisseurs',function(req,res,next){
   
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    comptesFournisseurs.getTotalComptesFournisseurs(
        req.app.locals.dataBase,decoded.userData.code,req.query.filter,
        function(err,totalComptesFournisseurs){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_COMPTES_FOURNISSEURS',
                 error_content: err
             });
          }else{
        
         return res.json({'totalComptesFournisseurs':totalComptesFournisseurs});
          }
      });

  });
  

  module.exports.routerComptesFournisseurs = router;