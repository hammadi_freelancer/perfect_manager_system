
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var ajournementsCredits = require('../model/ajournements-credits').AjournementsCredits;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addAjournementCredit',function(req,res,next){
    
       console.log(" ADD AJOURNEMENT CREDIT  IS CALLED") ;
       console.log(" THE AJOURNEMENT CREDIT  TO ADDED IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = ajournementsCredits.addAjournementCredit(req.app.locals.dataBase,
        req.body,decoded.userData.code,function(err,ajournementCreditAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_AJOURNEMENT_CREDIT',
                error_content: err
            });
         }else{
       
        return res.json(ajournementCreditAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateAjournementCredit',function(req,res,next){
    
       console.log(" UPDATE AJOURNEMENT CREDIT  IS CALLED") ;
       console.log(" THE AJOURNEMENT CREDIT  TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = ajournmentsCredits.updateAjournementCredit(req.app.locals.dataBase,
        req.body,decoded.userData.code, function(err,ajournementCreditUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_AJOURNEMENT_CREDIT',
                error_content: err
            });
         }else{
       
        return res.json(ajournementCreditUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getAjournementsCredits',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    ajournementsCredits.getListAjournementsCredits(req.app.locals.dataBase,decoded.userData.code, function(err,listAjournementsCredits){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_AJOURNEMENTS_CREDITS',
                 error_content: err
             });
          }else{
        
         return res.json(listAjournementsCredits);
          }
      });

  });
  router.get('/getAjournementCredit/:codeAjournementCredit',function(req,res,next){
    console.log("GET AJOURNEMENT CREDIT  IS CALLED");
    console.log(req.params['codeAjournementCredit']);
   
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    ajournementsCredits.getAjournementCredit(
        req.app.locals.dataBase,req.params['codeAjournementCredit'],decoded.userData.code,
     function(err,ajournementCredit){
       console.log("GET  AJOURNEMENT CREDIT : RESULT");
       console.log(ajournementCredit);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_AJOURNEMENT_CREDIT',
               error_content: err
           });
        }else{
      
       return res.json(ajournementCredit);
        }
    });
})

  router.delete('/deleteAjournementCredit/:codeAjournementCredit',function(req,res,next){
    
       console.log(" DELETE AJOURNEMENT CREDIT  IS CALLED") ;
       console.log(" THE AJOURNEMENT CREDIT  TO DELETE IS :",req.params['codeAjournementCredit']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = ajournementsCredits.deleteAjournementCredit(
        req.app.locals.dataBase,req.params['codeAjournementCredit'],decoded.userData.code,
        function(err,codeAjournementCredit){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_AJOURNEMENT_CREDIT',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeAjournementCredit']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getAjournementsCreditsByCodeCredit/:codeCredit',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    ajournementsCredits.getListAjournementsCreditsByCodeCredit(
        req.app.locals.dataBase,decoded.userData.code,req.params['codeCredit'],
         function(err,listAjournementsCredits){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_AJOURNEMENTS_CREDITS_BY_CODE_CREDIT',
                 error_content: err
             });
          }else{
        
         return res.json(listAjournementsCredits);
          }
      });

  });
  module.exports.routerAjournementsCredits = router;