
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var comptesClients = require('../model/comptes-clients').ComptesClients;

// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addCompteClient',function(req,res,next){
    
       console.log(" ADD COMPTE CLIENT IS CALLED") ;
       console.log(" THE COMPTE CLIENT  TO ADDED IS :",req.body);
    
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = comptesClients.addCompteClient(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,compteClientAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_COMPTE_CLIENT',
                error_content: err
            });
         }else{
       
        return res.json(compteClientAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateCompteClient',function(req,res,next){
    
       console.log(" UPDATE COMPTE CLIENT IS CALLED") ;
       console.log(" THE COMPTE CLIENT  TO UPDATE IS :",req.body);
       console.log(" ADD COMPTE CLIENT  IS CALLED") ;
       console.log(" THE COMPTE CLIENT  TO ADDED IS :",req.body);
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = comptesClients.updateCompteClient(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,compteClientUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_COMPTE_CLIENT',
                error_content: err
            });
         }else{
       
        return res.json(compteClientUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getPageComptesClients',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    comptesClients.getPageComptesClients(req.app.locals.dataBase,
        decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
        req.query.filter,function(err,listComptesClients){
         console.log("GET PAGE COMPTES CLIENTS : RESULT");
        // console.log(listCredits);
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_COMPTES_CLIENTS',
                 error_content: err
             });
          }else{
        
         return res.json(listComptesClients);
          }
      });

  });
  router.get('/getCompteClient/:codeCompteClient',function(req,res,next){
    console.log("GET COMPTE CLIENT IS CALLED");
    
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    comptesClients.getCompteClient(
        req.app.locals.dataBase,req.params['codeCompteClient'],decoded.userData.code, function(err,compteClient){
       console.log("GET  COMPTE CLIENT  : RESULT");
       console.log(compteClient);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_COMPTE_CLIENT',
               error_content: err
           });
        }else{
      
       return res.json(compteClient);
        }
    });
})

  router.delete('/deleteCompteClient/:codeCompteClient',function(req,res,next){
    
       console.log(" DELETE COMPTE CLIENT IS CALLED") ;
       console.log(" THE COMPTE CLIENT  TO DELETE IS :",req.params['codeCompteClient']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = comptesClients.deleteCompteClient(
        req.app.locals.dataBase,req.params['codeCompteClient'],decoded.userData.code, function(err,codeCompteClient){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_COMPTE_CLIENT',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeCompteClient']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });



 router.get('/getTotalComptesClients',function(req,res,next){
   
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    comptesClients.getTotalComptesClients(
        req.app.locals.dataBase,decoded.userData.code,req.query.filter,
        function(err,totalComptesClients){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_COMPTES_CLIENTS',
                 error_content: err
             });
          }else{
        
         return res.json({'totalComptesClients':totalComptesClients});
          }
      });

  });
  

  module.exports.routerComptesClients = router;