
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var documentsCredits = require('../model/documents-credits').DocumentsCredits;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addDocumentCredit',function(req,res,next){
    
       console.log(" ADD DOCUMENT CREDIT  IS CALLED") ;
       console.log(" THE DOCUMENT CREDIT  TO ADDED IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = documentsCredits.addDocumentCredit(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,documentCreditsAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_DOCUMENT_CREDIT',
                error_content: err
            });
         }else{
       
        return res.json(documentCreditsAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateDocumentCredit',function(req,res,next){
    
       console.log(" UPDATE DOCUMENT CREDIT  IS CALLED") ;
       // console.log(" THE DOCUMENT CREDIT  TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result =documentsCredits.updateDocumentCredit(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,documentCreditUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_DOCUMENT_CREDIT',
                error_content: err
            });
         }else{
       
        return res.json(documentCreditUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getDocumentsCredits',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    documentsCredits.getListDocumentCredits(
        req.app.locals.dataBase,decoded.userData.code, function(err,listDocumentsCredits){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_CREDITS',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsCredits);
          }
      });

  });
  router.get('/getDocumentCredit/:codeDocumentCredit',function(req,res,next){
    console.log("GET DOCUMENT CREDIT  IS CALLED");
    console.log(req.params['codeDocumentCredit']);
   
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    documentsCredits.getDocumentCredit(
        req.app.locals.dataBase,req.params['codeDocumentCredit'],decoded.userData.code,
     function(err,documentCredit){
       console.log("GET  DOCUMENT CREDIT : RESULT");
       console.log(documentCredit);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_DOCUMENT_CREDIT',
               error_content: err
           });
        }else{
      
       return res.json(documentCredit);
        }
    });
})

  router.delete('/deleteDocumentCredit/:codeDocumentCredit',function(req,res,next){
    
       console.log(" DELETE DOCUMENT CREDIT  IS CALLED") ;
       console.log(" THE DOCUMENT CREDIT  TO DELETE IS :",req.params['codeDocumentCredit']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = documentsCredits.deleteDocumentCredit(
       req.app.locals.dataBase,req.params['codeDocumentCredit'],decoded.userData.code,
        function(err,codeDocumentCredit){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_DOCUMENT_CREDIT',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeDocumentCredit']);
       }
    }
)
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getDocumentsCreditsByCodeCredit/:codeCredit',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    documentsCredits.getListDocumentsCreditsByCodeCredit(req.app.locals.dataBase,
        decoded.userData.code,req.params['codeCredit'],
         function(err,listDocumentCredits){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_CREDITS_BY_CODE_CREDIT',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentCredits);
          }
      });

  });
  module.exports.routerDocumentsCredits = router;