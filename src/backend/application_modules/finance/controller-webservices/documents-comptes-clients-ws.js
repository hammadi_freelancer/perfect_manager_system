
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var documentsComptesClients = require('../model/documents-comptes-clients').DocumentsComptesClients;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addDocumentCompteClient',function(req,res,next){
    
       console.log(" ADD DOCUMENT  COMPTE CLIENT  IS CALLED") ;
       console.log(" THE DOCUMENT COMPTE CLIENT  TO ADDED IS :",req.body);
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = documentsComptesClients.addDocumentCompteClient(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,documentCompteClientsAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_DOCUMENT_COMPTE_CLIENT',
                error_content: err
            });
         }else{
       
        return res.json(documentCompteClientsAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateDocumentCompteClient',function(req,res,next){
    
       console.log(" UPDATE DOCUMENT COMPTE CLIENT  IS CALLED") ;
       // console.log(" THE DOCUMENT COMPTE CLIENT  TO UPDATE IS :",req.body);
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result =documentsComptesClients.updateDocumentCompteClient(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,documentCompteClientUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_DOCUMENT_COMPTE_CLIENT',
                error_content: err
            });
         }else{
       
        return res.json(documentCompteClientUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getDocumentsComptesClients',function(req,res,next){
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

        documentsComptesClients.getListDocumentsComptesClients(
        req.app.locals.dataBase,decoded.userData.code, function(err,listDocumentsComptesClients){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_COMPTE_CLIENTS',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsComptesClients);
          }
      });

  });
  router.get('/getDocumentCompteClient/:codeDocumentCompteClient',function(req,res,next){
    console.log("GET DOCUMENT COMPTE CLIENT  IS CALLED");
    console.log(req.params['codeDocumentCompteClient']);
   
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
        documentsComptesClients.getDocumentCompteClient(
        req.app.locals.dataBase,req.params['codeDocumentCompteClient'],decoded.userData.code,
     function(err,documentCompteClient){
       console.log("GET  DOCUMENT COMPTE CLIENT : RESULT");
       console.log(documentCompteClient);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_DOCUMENT_COMPTE_CLIENT',
               error_content: err
           });
        }else{
      
       return res.json(documentCompteClient);
        }
    });
})

  router.delete('/deleteDocumentCompteClient/:codeDocumentCompteClient',function(req,res,next){
    
       console.log(" DELETE DOCUMENT COMPTE CLIENT  IS CALLED") ;
       console.log(" THE DOCUMENT COMPTE CLIENT  TO DELETE IS :",req.params['codeDocumentCompteClient']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = documentsComptesClients.deleteDocumentCompteClient(
       req.app.locals.dataBase,req.params['codeDocumentCompteClient'],decoded.userData.code,
        function(err,codeDocumentCompteClient){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_DOCUMENT_COMPTE_CLIENT',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeDocumentCompteClient']);
       }
    }
)
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getDocumentsComptesClientsByCodeCompteClient/:codeCompteClient',function(req,res,next){
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
        documentsComptesClients.getListDocumentsComptesClientsByCodeCompteClient(req.app.locals.dataBase,
        decoded.userData.code,req.params['codeCompteClient'],
         function(err,listDocumentsComptesClients){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_COMPTE CLIENTS_BY_CODE_COMPTE CLIENT',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsComptesClients);
          }
      });

  });
  module.exports.routerDocumentsComptesClients = router;