
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var transactionsComptesFournisseurs = require('../model/transactions-comptes-fournisseurs').TransactionsComptesFournisseurs;

// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addTransactionCompteFournisseur',function(req,res,next){
    
       console.log(" ADD TRANSACTION COMPTE FOURNISSEUR IS CALLED") ;
       console.log(" THE TRANSACTION COMPTE FOURNISSEUR  TO ADDED IS :",req.body);
    
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = transactionsComptesFournisseurs.addTransactionCompteFournisseur(
        req.app.locals.dataBase,req.body,decoded.userData.code,
        function(err,transCompteFournisseurAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_TRANSACTION_COMPTE_FOURNISSEUR',
                error_content: err
            });
         }else{
       
        return res.json(transCompteFournisseurAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateTransactionCompteFournisseur',function(req,res,next){
    
       console.log(" UPDATE TRANSACTION COMPTE FOURNISSEUR IS CALLED") ;
       console.log(" THE TRANSACTION COMPTE FOURNISSEUR  TO UPDATE IS :",req.body);
      
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = transactionsComptesFournisseurs.updateTransactionCompteFournisseur(
        req.app.locals.dataBase,req.body,decoded.userData.code, 
        function(err,transCompteFournisseurUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_TRANSACTION_COMPTE_FOURNISSEUR',
                error_content: err
            });
         }else{
       
        return res.json(transCompteFournisseurUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getPageTransactionsComptesFournisseurs',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

        transactionsComptesFournisseurs.getPageTransactionsComptesFournisseurs(req.app.locals.dataBase,
        decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
        function(err,listTransactionsComptesFournisseurs){
         console.log("GET PAGE TRANSACTIONS COMPTES FOURNISSEURS : RESULT");
        // console.log(listCredits);
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_TRANSACTIONS_COMPTES_FOURNISSEURS',
                 error_content: err
             });
          }else{
        
         return res.json(listTransactionsComptesFournisseurs);
          }
      });

  });
  router.get('/getTransactionCompteFournisseur/:codeTransactionCompteFournisseur',function(req,res,next){
    console.log("GET TRANSACTION COMPTE FOURNISSEUR IS CALLED");
    
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

        transactionsComptesFournisseurs.getTransactionCompteFournisseur(
        req.app.locals.dataBase,req.params['codeTransactionCompteFournisseur'],
        decoded.userData.code, function(err,transCompteFournisseur)
        
        {
       console.log("GET  TRANSACTION COMPTE FOURNISSEUR  : RESULT");
       console.log(transCompteFournisseur);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_TRANSACTION_COMPTE_FOURNISSEUR',
               error_content: err
           });
        }else{
      
       return res.json(transCompteFournisseur);
        }
    });
})

  router.delete('/deleteTransactionCompteFournisseur/:codeTransactionCompteFournisseur',function(req,res,next){
    
       console.log(" DELETE TRANSACTION COMPTE FOURNISSEUR IS CALLED") ;
       console.log(" THE TRANSACTION COMPTE FOURNISSEUR  TO DELETE IS :",req.params['codeTransactionCompteFournisseur']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = transactionsComptesFournisseurs.deleteTransactionCompteFournisseur(
        req.app.locals.dataBase,req.params['codeTransactionCompteFournisseur'],decoded.userData.code, 
        function(err,codeTrCompteFournisseur){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_TRANSACTION_COMPTE_FOURNISSEUR',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeTransactionCompteFournisseur']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });



 router.get('/getTotalTransactionsComptesFournisseurs',function(req,res,next){
   
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    transactionsComptesFournisseurs.getTotalTransactionsComptesFournisseurs(
        req.app.locals.dataBase,decoded.userData.code,function(err,totalTransComptesFournisseurs){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_TRANSACTIONS_COMPTES_FOURNISSEURS',
                 error_content: err
             });
          }else{
        
         return res.json({'totalTransactionsComptesFournisseurs':totalTransComptesFournisseurs});
          }
      });

  });
  

  module.exports.routerTransactionsComptesFournisseurs = router;