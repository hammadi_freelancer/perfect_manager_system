
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var ajournementsCreditsAchats = require('../model/ajournements-credits-achats').AjournementsCreditsAchats;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addAjournementCreditAchats',function(req,res,next){
    
       console.log(" ADD AJOURNEMENT CREDIT ACHATS IS CALLED") ;
       console.log(" THE AJOURNEMENT CREDIT ACHATS TO ADDED IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = ajournementsCreditsAchats.addAjournementCreditAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,ajournementCreditAchatsAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_AJOURNEMENT_CREDIT_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(ajournementCreditAchatsAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateAjournementCreditAchats',function(req,res,next){
    
       console.log(" UPDATE AJOURNEMENT CREDIT ACHATS IS CALLED") ;
       console.log(" THE AJOURNEMENT CREDIT ACHATS TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = ajournmentsCreditsAchats.updateAjournementCreditAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,ajournementCreditAchatsUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_AJOURNEMENT_CREDIT_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(ajournementCreditAchatsUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getAjournementsCreditsAchats',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    ajournementsCreditsAchats.getListAjournementsCreditsAchats(
        req.app.locals.dataBase,decoded.userData.code, function(err,listAjournementsCreditsAchats){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_AJOURNEMENTS_CREDITS_ACHATS',
                 error_content: err
             });
          }else{
        
         return res.json(listAjournementsCreditsAchats);
          }
      });

  });
  router.get('/getAjournementCreditAchats/:codeAjournementCreditAchats',function(req,res,next){
    console.log("GET AJOURNEMENT CREDIT  ACHATS IS CALLED");
    console.log(req.params['codeAjournementCreditAchats']);
   
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    ajournementsCreditsAchats.getAjournementCreditAchats(
        req.app.locals.dataBase,req.params['codeAjournementCreditAchats'],decoded.userData.code,
     function(err,ajournementCreditAchats){
       console.log("GET  AJOURNEMENT CREDIT ACHATS: RESULT");
       console.log(ajournementCreditAchats);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_AJOURNEMENT_CREDIT_ACHATS',
               error_content: err
           });
        }else{
      
       return res.json(ajournementCreditAchats);
        }
    });
})

  router.delete('/deleteAjournementCreditAchats/:codeAjournementCreditAchats',function(req,res,next){
    
       console.log(" DELETE AJOURNEMENT CREDIT ACHATS IS CALLED") ;
       console.log(" THE AJOURNEMENT CREDIT ACHATS TO DELETE IS :",req.params['codeAjournementCreditAchats']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = ajournementsCreditsAchats.deleteAjournementCreditAchats(
        req.app.locals.dataBase,req.params['codeAjournementCreditAchats'],decoded.userData.code,
        function(err,codeAjournementCreditAchats){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_AJOURNEMENT_CREDIT_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeAjournementCreditAchats']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   
 router.get('/getAjournementsCreditsAchatsByCodeCredit/:codeCreditAchats',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    ajournementsCreditsAchats.getListAjournementsCreditsAchatsByCodeCredit(
        req.app.locals.dataBase,decoded.userData.code,req.params['codeCreditAchats'],
         function(err,listAjournementsCreditsAchats){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_AJOURNEMENTS_CREDITS_ACHATS_BY_CODE_CREDIT',
                 error_content: err
             });
          }else{
        
         return res.json(listAjournementsCreditsAchats);
          }
      });

  });
  module.exports.routerAjournementsCreditsAchats = router;