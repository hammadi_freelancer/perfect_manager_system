
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var payementsFactureAchats = require('../model/payements-factures-achats').PayementsFacturesAchats;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addPayementFactureAchats',function(req,res,next){
    
       console.log(" ADD PAYEMENT FACTURE ACHATS IS CALLED") ;
       console.log(" THE PAYEMENT FACTURE ACHATS TO ADDED IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = payementsFactureAchats.addPayementFactureAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code,
        function(err,payementFactureAchatsAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_PAYEMENT_FACTURE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(payementFactureAchatsAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updatePayementFactureAchats',function(req,res,next){
    
       console.log(" UPDATE PAYEMENT FACTURE ACHATS  IS CALLED") ;
       console.log("THE PAYEMENT FACTURE ACHATS TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = payementsFactureAchats.updatePayementFactureAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code,
         function(err,payementsFactureAchatsUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_PAYEMENT_FACTURE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(payementsFactureAchatsUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getPayementsFacturesAchats',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
  
    payementsFactureAchats.getListPayementsFacturesAchats(
        req.app.locals.dataBase,decoded.userData.code, function(err,listPayementsFacturesAchats){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_PAYEMENTS_FACTURES_ACHATS',
                 error_content: err
             });
          }else{
        
         return res.json(listPayementsFacturesAchats);
          }
      });

  });
  router.get('/getPayementFactureAchats/:codePayementFactureAchats',function(req,res,next){
    console.log("GET PAYEMENT FACTURE ACHATS IS CALLED");
   //  console.log(req.params['codePayementFactureVente']);
   
   //  console.log(req.headers.authorization.slice( 
      //   req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    payementsFactureAchats.getPayementFactureAchats(
        req.app.locals.dataBase,req.params['codePayementFactureAchats'],decoded.userData.code,
     function(err,payementFactureAchats){
       console.log("GET  PAYEMENT FACTURE ACHATS : RESULT");
       console.log(payementFactureAchats);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_PAYEMENT_FACTURE_ACHATS',
               error_content: err
           });
        }else{
      
       return res.json(payementFactureAchats);
        }
    });
})

  router.delete('/deletePayementFactureAchats/:codePayementFactureAchats',function(req,res,next){
    
       console.log(" DELETE PAYEMENT FACTURE ACHATS IS CALLED") ;
       console.log(" THE PAYEMENT FACTURE ACHATS TO DELETE IS :",req.params['codePayementFactureAchats']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = payementsFactureAchats.deletePayementFactureAchats(
        req.app.locals.dataBase,req.params['codePayementFactureAchats'],decoded.userData.code,
        function(err,codePayementFactureAchats){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_PAYEMENT_FACTURE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codePayementFactureAchats']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

   router.get('/getPayementsFacturesAchatsByCodeFactureAchats/:codeFactureAchats',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
    payementsFactureAchats.getListPayementsFactureAchatsByCodeFactureAchats(
        req.app.locals.dataBase,req.params['codeFactureAchats'],decoded.userData.code, function(err,listPayementsFactA){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_PAYEMENTS_FACTURE_ACHATS_BY_CODE_FACTURE',
                 error_content: err
             });
          }else{
        
         return res.json(listPayementsFactA);
          }
      });

  })

  module.exports.routerPayementsFacturesAchats = router;