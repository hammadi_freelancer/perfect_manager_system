
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var transactionsComptesClients = require('../model/transactions-comptes-clients').TransactionsComptesClients;

// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addTransactionCompteClient',function(req,res,next){
    
       console.log(" ADD TRANSACTION COMPTE CLIENT IS CALLED") ;
       console.log(" THE TRANSACTION COMPTE CLIENT  TO ADDED IS :",req.body);
    
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = transactionsComptesClients.addTransactionCompteClient(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,transCompteClientAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_TRANSACTION_COMPTE_CLIENT',
                error_content: err
            });
         }else{
       
        return res.json(transCompteClientAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateTransactionCompteClient',function(req,res,next){
    
       console.log(" UPDATE TRANSACTION COMPTE CLIENT IS CALLED") ;
       console.log(" THE  TRANSACTION COMPTE CLIENT  TO UPDATE IS :",req.body);
     
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = transactionsComptesClients.updateTransactionCompteClient(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,transCompteClientUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_TRANSACTION_COMPTE_CLIENT',
                error_content: err
            });
         }else{
       
        return res.json(transCompteClientUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getPageTransactionsComptesClients',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

        transactionsComptesClients.getPageTransactionsComptesClients(req.app.locals.dataBase,
        decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,function(err,listTransComptesClients){
         console.log("GET PAGE TRANSACTIONS COMPTES CLIENTS : RESULT");
        // console.log(listCredits);
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_TRANSACTIONS_COMPTES_CLIENTS',
                 error_content: err
             });
          }else{
        
         return res.json(listTransComptesClients);
          }
      });

  });
  router.get('/getTransactionCompteClient/:codeTransactionCompteClient',function(req,res,next){
    console.log("GET TRANSACTION COMPTE CLIENT IS CALLED");
    
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
        transactionsComptesClients.getTransactionCompteClient(
        req.app.locals.dataBase,req.params['codeTransactionCompteClient'],decoded.userData.code, function(err,transCompteClient){
       console.log("GET  TRANSACTION COMPTE CLIENT  : RESULT");
       console.log(transCompteClient);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_TRANSACTION_COMPTE_CLIENT',
               error_content: err
           });
        }else{
      
       return res.json(transCompteClient);
        }
    });
})

  router.delete('/deleteTransactionCompteClient/:codeTransactionCompteClient',function(req,res,next){
    
       console.log(" DELETE TRANSACTION COMPTE CLIENT IS CALLED") ;
       console.log(" THE TRANSACTION COMPTE CLIENT  TO DELETE IS :",req.params['codeTransactionCompteClient']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = transactionsComptesClients.deleteTransactionCompteClient(
        req.app.locals.dataBase,req.params['codeTransactionCompteClient'],decoded.userData.code, function(err,codeTransCompteClient){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_TRANSACTION_COMPTE_CLIENT',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeTransactionCompteClient']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });



 router.get('/getTotalTransactionsComptesClients',function(req,res,next){
   
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
        transactionsComptesClients.getTotalTransactionsComptesClients(
        req.app.locals.dataBase,decoded.userData.code,function(err,totalTransComptesClients){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_TRANSACTIONS_COMPTES_CLIENTS',
                 error_content: err
             });
          }else{
        
         return res.json({'totalTransactionsComptesClients':totalTransComptesClients});
          }
      });

  });
  

  module.exports.routerTransactionsComptesClients = router;