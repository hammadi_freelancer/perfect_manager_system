
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var documentsCreditsAchats = require('../model/documents-credits-achats').DocumentsCreditsAchats;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addDocumentCreditAchats',function(req,res,next){
    
       console.log(" ADD DOCUMENT CREDIT ACHATS  IS CALLED") ;
       console.log(" THE DOCUMENT CREDIT ACHATS  TO ADDED IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = documentsCreditsAchats.addDocumentCreditAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,documentCreditsAddedAchats){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_DOCUMENT_CREDIT_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(documentCreditsAddedAchats);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateDocumentCreditAchats',function(req,res,next){
    
       console.log(" UPDATE DOCUMENT CREDIT ACHATS IS CALLED") ;
       console.log(" THE DOCUMENT CREDIT ACHATS  TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result =documentsCreditsAchats.updateDocumentCreditAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,documentCreditAchatsUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_DOCUMENT_CREDIT_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(documentCreditAchatsUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getDocumentsCreditsAchats',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    documentsCreditsAchats.getListDocumentCreditsAchats(
        req.app.locals.dataBase,decoded.userData.code, function(err,listDocumentsCreditsAchats){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_CREDITS_ACHATS',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsCreditsAchats);
          }
      });

  });
  router.get('/getDocumentCreditAchats/:codeDocumentCreditAchats',function(req,res,next){
    console.log("GET DOCUMENT CREDIT ACHATS IS CALLED");
    console.log(req.params['codeDocumentCreditAchats']);
   
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    documentsCreditsAchats.getDocumentCreditAchats(
        req.app.locals.dataBase,req.params['codeDocumentCreditAchats'],decoded.userData.code,
     function(err,documentsCreditsAchats){
       console.log("GET  DOCUMENT CREDIT ACHATS: RESULT");
       // console.log(documentsCreditsAchats);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_DOCUMENT_CREDIT_ACHATS',
               error_content: err
           });
        }else{
      
       return res.json(documentCreditAchats);
        }
    });
})

  router.delete('/deleteDocumentCreditAchats/:codeDocumentCreditAchats',function(req,res,next){
    
       console.log(" DELETE DOCUMENT CREDIT ACHATS IS CALLED") ;
       console.log(" THE DOCUMENT CREDIT  ACHATS TO DELETE IS :",req.params['codeDocumentCreditAchats']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = documentsCreditsAchats.deleteDocumentCreditAchats(
        req.app.locals.dataBase,req.params['codeDocumentCreditAchats'],decoded.userData.code,
        function(err,documentsCreditsAchats){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_DOCUMENT_CREDIT_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeDocumentCreditAchats']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getDocumentsCreditsAchatsByCodeCredit/:codeCreditAchats',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    documentsCreditsAchats.getListDocumentsCreditsAchatsByCodeCredit(
        req.app.locals.dataBase,decoded.userData.code,req.params['codeCreditAchats'],
         function(err,listDocumentCreditsAchats){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_CREDITS_ACHATS_BY_CODE_CREDIT',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentCreditsAchats);
          }
      });

  });
  module.exports.routerDocumentsCreditsAchats = router;