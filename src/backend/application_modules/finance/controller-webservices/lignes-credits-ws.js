
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var lignesCredits = require('../model/lignes-credits').LignesCredits;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addLigneCredit',function(req,res,next){
    
       console.log(" ADD LIGNE CREDIT  IS CALLED") ;
       console.log(" THE LIGNE CREDIT  TO ADDED IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = lignesCredits.addLigneCredit(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,ligneCreditsAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_LIGNE_CREDIT',
                error_content: err
            });
         }else{
       
        return res.json(ligneCreditsAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateLigneCredit',function(req,res,next){
    
       console.log(" UPDATE LIGNE CREDIT  IS CALLED") ;
       console.log(" THE LIGNE CREDIT  TO UPDATE IS :",req.body);
   
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = lignesCredits.updateLigneCredit(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,ligneCreditUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_LIGNE_CREDIT',
                error_content: err
            });
         }else{
       
        return res.json(ligneCreditUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getLignesCredits',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    lignesCredits.getListLignesCredits(
        req.app.locals.dataBase,decoded.userData.code, function(err,listLignesCredits){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_LIGNES_CREDITS',
                 error_content: err
             });
          }else{
        
         return res.json(listLignesCredits);
          }
      });

  });
  router.get('/getLigneCredit/:codeLigneCredit',function(req,res,next){
    console.log("GET LIGNE CREDIT  IS CALLED");
   
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    lignesCredits.getLigneCredit(
        req.app.locals.dataBase,req.params['codeLigneCredit'],decoded.userData.code,
     function(err,ligneCredit){
       console.log("GET  LIGNE CREDIT : RESULT");
       console.log(ligneCredit);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIGNE_CREDIT',
               error_content: err
           });
        }else{
      
       return res.json(ligneCredit);
        }
    });
})

  router.delete('/deleteLigneCredit/:codeLigneCredit',function(req,res,next){
    
       console.log(" DELETE LIGNE CREDIT  IS CALLED") ;
       console.log(" THE LIGNE CREDIT  TO DELETE IS :",req.params['codeLigneCredit']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = lignesCredits.deleteLigneCredit(
        req.app.locals.dataBase,req.params['codeLigneCredit'],decoded.userData.code,
        function(err,codeLigneCredit){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_LIGNE_CREDIT',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeLigneCredit']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getLignesCreditsByCodeCredit/:codeCredit',function(req,res,next){
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
    lignesCredits.getListLignesCreditsByCodeCredit(
        req.app.locals.dataBase,decoded.userData.code,req.params['codeCredit'],
         function(err,listLignesCredits){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_LIGNES_CREDITS_BY_CODE_CREDIT',
                 error_content: err
             });
          }else{
        
         return res.json(listLignesCredits);
          }
      });

  });
  module.exports.routerLignesCredits = router;