
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var tranchesCredits = require('../model/tranches-credits').TranchesCredits;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addTrancheCredit',function(req,res,next){
    
       console.log(" ADD TRANCHE CREDIT  IS CALLED") ;
       console.log(" THE TRANCHE CREDIT  TO ADDED IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = tranchesCredits.addTrancheCredit(req.app.locals.dataBase,req.body,decoded.userData.code,function(err,trancheCreditsAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_TRANCHE_CREDIT',
                error_content: err
            });
         }else{
       
        return res.json(trancheCreditsAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateTrancheCredit',function(req,res,next){
    
       console.log(" UPDATE TRANCHE CREDIT  IS CALLED") ;
       console.log(" THE TRANCHE CREDIT  TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result =tranchesCredits.updateTrancheCredit(req.app.locals.dataBase,req.body,decoded.userData.code, function(err,trancheCreditUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_TRANCHE_CREDIT',
                error_content: err
            });
         }else{
       
        return res.json(trancheCreditUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getTranchesCredits',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    tranchesCredits.getListTranchesCredits(req.app.locals.dataBase,decoded.userData.code, function(err,listTranchesCredits){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_TRANCHES_CREDITS',
                 error_content: err
             });
          }else{
        
         return res.json(listTranchesCredits);
          }
      });

  });
  router.get('/getTrancheCredit/:codeTrancheCredit',function(req,res,next){
    console.log("GET TRANCHE CREDIT  IS CALLED");
    console.log(req.params['codeTrancheCredit']);
   
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    tranchesCredits.getTrancheCredit(req.app.locals.dataBase,req.params['codeTrancheCredit'],decoded.userData.code,
     function(err,trancheCredit){
       console.log("GET  TRANCHE CREDIT : RESULT");
       console.log(trancheCredit);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_TRANCHE_CREDIT',
               error_content: err
           });
        }else{
      
       return res.json(trancheCredit);
        }
    });
})

  router.delete('/deleteTrancheCredit/:codeTrancheCredit',function(req,res,next){
    
       console.log(" DELETE TRANCHE CREDIT  IS CALLED") ;
       console.log(" THE TRANCHE CREDIT  TO DELETE IS :",req.params['codeTrancheCredit']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = tranchesCredits.deleteTrancheCredit(req.app.locals.dataBase,req.params['codeTrancheCredit'],decoded.userData.code,
        function(err,codeTrancheCredit){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_TRANCHE_CREDIT',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeTrancheCredit']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getTranchesCreditsByCodeCredit/:codeCredit',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    tranchesCredits.getListTranchesCreditsByCodeCredit(req.app.locals.dataBase,decoded.userData.code,req.params['codeCredit'],
         function(err,listTranchesCredits){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_TRANCHES_CREDITS_BY_CODE_CREDIT',
                 error_content: err
             });
          }else{
        
         return res.json(listTranchesCredits);
          }
      });

  });
  module.exports.routerTranchesCredits = router;