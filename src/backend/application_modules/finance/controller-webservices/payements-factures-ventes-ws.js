
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var payementsFactureVentes = require('../model/payements-factures-ventes').PayementsFacturesVentes;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addPayementFactureVentes',function(req,res,next){
    
       console.log(" ADD PAYEMENT FACTURE VENTE IS CALLED") ;
       console.log(" THE PAYEMENT FACTURE VENTE TO ADDED IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = payementsFactureVentes.addPayementFactureVente(
        req.app.locals.dataBase,req.body,decoded.userData.code,
        function(err,payementFactureVenteAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_PAYEMENT_FACTURE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(payementFactureVenteAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updatePayementFactureVentes',function(req,res,next){
    
       console.log(" UPDATE PAYEMENT FACTURE VENTES  IS CALLED") ;
       console.log("THE PAYEMENT FACTURE VENTES TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = payementsFactureVentes.updatePayementFactureVentes(
        req.app.locals.dataBase,req.body,decoded.userData.code,
         function(err,payementsFactureVentesUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_PAYEMENT_FACTURE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(payementsFactureVentesUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getPayementsFacturesVentes',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
  
    payementsFactureVentes.getListPayementsFacturesVentes(
        req.app.locals.dataBase,decoded.userData.code, function(err,listPayementsFacturesVentes){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_PAYEMENTS_FACTURES_VENTES',
                 error_content: err
             });
          }else{
        
         return res.json(listPayementsFacturesVentes);
          }
      });

  });
  router.get('/getPayementFactureVentes/:codePayementFactureVentes',function(req,res,next){
    console.log("GET PAYEMENT FACTURE VENTE IS CALLED");
    console.log(req.params['codePayementFactureVente']);
   
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    payementsFactureVentes.getPayementFactureVentes(
        req.app.locals.dataBase,req.params['codePayementFactureVentes'],decoded.userData.code,
     function(err,payementFactureVentes){
       console.log("GET  PAYEMENT FACTURE VENTES : RESULT");
       console.log(payementCreditAchat);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_PAYEMENT_FACTURE_VENTES',
               error_content: err
           });
        }else{
      
       return res.json(payementFactureVentes);
        }
    });
})

  router.delete('/deletePayementFactureVentes/:codePayementFactureVentes',function(req,res,next){
    
       console.log(" DELETE PAYEMENT FACTURE VENTES IS CALLED") ;
       console.log(" THE PAYEMENT FACTURE VENTES TO DELETE IS :",req.params['codePayementFactureVentes']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = payementsFactureVentes.deletePayementFactureVente(
        req.app.locals.dataBase,req.params['codePayementFactureVentes'],decoded.userData.code,
        function(err,codePayementFactureVentes){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_PAYEMENT_FACTURE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codePayementFactureVentes']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

   router.get('/getPayementsFacturesVentesByCodeFactureVentes/:codeFactureVentes',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    payementsFactureVentes.getListPayementsFactureVentesByCodeFactureVente(
        req.app.locals.dataBase,req.params['codeFactureVentes'],decoded.userData.code, function(err,listPayementsFactVen){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_PAYEMENTS_FACTURE_VENTES_BY_CODE_FACTURE',
                 error_content: err
             });
          }else{
        
         return res.json(listPayementsFactVen);
          }
      });

  })

  module.exports.routerPayementsFacturesVentes = router;