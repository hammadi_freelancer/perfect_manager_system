
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var payementsCredits = require('../model/payements-credits').PayementsCredits;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addPayementCredit',function(req,res,next){
    
       console.log(" ADD PAYEMENT CREDIT  IS CALLED") ;
       console.log(" THE PAYEMENT CREDIT  TO ADDED IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = payementsCredits.addPayementCredit(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,payementCreditsAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_PAYEMENT_CREDIT',
                error_content: err
            });
         }else{
       
        return res.json(payementCreditsAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updatePayementCredit',function(req,res,next){
    
       console.log(" UPDATE PAYEMENT CREDIT  IS CALLED") ;
       console.log(" THE PAYEMENT CREDIT  TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = payementsCredits.updatePayementCredit(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,payementCreditUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_PAYEMENT_CREDIT',
                error_content: err
            });
         }else{
       
        return res.json(payementCreditUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getPayementsCredits',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    payementsCredits.getListPayementsCredits(
        req.app.locals.dataBase,decoded.userData.code, function(err,listPayementsCredits){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_PAYEMENTS_CREDITS',
                 error_content: err
             });
          }else{
        
         return res.json(listPayementsCredits);
          }
      });

  });
  router.get('/getPayementCredit/:codePayementCredit',function(req,res,next){
    console.log("GET PAYEMENT CREDIT  IS CALLED");
    console.log(req.params['codePayementCredit']);
   
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    payementsCredits.getPayementCredit(
        req.app.locals.dataBase,req.params['codePayementCredit'],decoded.userData.code,
     function(err,payementCredit){
       console.log("GET  PAYEMENT CREDIT : RESULT");
       console.log(payementCredit);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_PAYEMENT_CREDIT',
               error_content: err
           });
        }else{
      
       return res.json(payementCredit);
        }
    });
})

  router.delete('/deletePayementCredit/:codePayementCredit',function(req,res,next){
    
       console.log(" DELETE PAYEMENT CREDIT  IS CALLED") ;
       console.log(" THE PAYEMENT CREDIT  TO DELETE IS :",req.params['codePayementCredit']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = payementsCredits.deletePayementCredit(
        req.app.locals.dataBase,req.params['codePayementCredit'],decoded.userData.code,
        function(err,codePayementCredit){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_PAYEMENT_CREDIT',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codePayementCredit']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getPayementsCreditsByCodeCredit/:codeCredit',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    payementsCredits.getListPayementsCreditsByCodeCredit(
        req.app.locals.dataBase,decoded.userData.code,req.params['codeCredit'],
         function(err,listPayementsCredits){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_PAYEMENTS_CREDITS_BY_CODE_CREDIT',
                 error_content: err
             });
          }else{
        
         return res.json(listPayementsCredits);
          }
      });

  });
  module.exports.routerPayementsCredits = router;