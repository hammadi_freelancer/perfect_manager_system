
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var credits = require('../model/credits').Credits;
var creditsService = require('../service/credits-ser').CreditsService;

// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addCredit',function(req,res,next){
    
       console.log(" ADD CREDIT IS CALLED") ;
       console.log(" THE CREDIT TO ADDED IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = credits.addCredit(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,creditAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_CREDIT',
                error_content: err
            });
         }else{
       
        return res.json(creditAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateCredit',function(req,res,next){
    
       console.log(" UPDATE CREDIT IS CALLED") ;
       console.log(" THE CREDIT TO UPDATE IS :",req.body);
       console.log(" ADD CREDIT IS CALLED") ;
       console.log(" THE CREDIT TO ADDED IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = credits.updateCredit(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,creditUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_CREDIT',
                error_content: err
            });
         }else{
       
        return res.json(creditUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getCredits',function(req,res,next){
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

      console.log("GET LIST CREDITS IS CALLED");
    credits.getListCredits(req.app.locals.dataBase,
        decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
        req.query.filter,function(err,listCredits){
         console.log("GET LIST CREDITS : RESULT");
         console.log(listCredits);
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_CREDITS',
                 error_content: err
             });
          }else{
        
         return res.json(listCredits);
          }
      });

  });
  router.get('/getCredit/:codeCredit',function(req,res,next){
    console.log("GET CREDIT IS CALLED");
    console.log(req.params['codeCredit']);
   
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    credits.getCredit(
        req.app.locals.dataBase,req.params['codeCredit'],decoded.userData.code, function(err,credit){
       console.log("GET  CREDIT : RESULT");
       console.log(credit);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_CREDIT',
               error_content: err
           });
        }else{
      
       return res.json(credit);
        }
    });
})

  router.delete('/deleteCredit/:codeCredit',function(req,res,next){
    
       console.log(" DELETE CREDIT IS CALLED") ;
       console.log(" THE CREDIT TO DELETE IS :",req.params['codeCredit']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = credits.deleteCredit(
        req.app.locals.dataBase,req.params['codeCredit'],decoded.userData.code, function(err,codeCredit){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_CREDIT',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeCredit']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });



 router.get('/getTotalCredits',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    credits.getTotalCredits(
        req.app.locals.dataBase,decoded.userData.code,req.query.filter,
        function(err,totalCredits){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_CREDITS',
                 error_content: err
             });
          }else{
        
         return res.json({'totalCredits':totalCredits});
          }
      });

  });
  router.get('/getTotalPayementsCreditsByCodeCredit/:codeCredit',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
        creditsService.getTotalPayementsCreditsByCodeCredit(
            req.app.locals.dataBase,req.params['codeCredit'],
        decoded.userData.code,function(err,totalPayCredits){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_PAYEMENTS_CREDITS',
                 error_content: err
             });
          }else{
        
         return res.json({'totalPayementsCredits':totalPayCredits});
          }
      });

  });
  module.exports.routerCredits = router;