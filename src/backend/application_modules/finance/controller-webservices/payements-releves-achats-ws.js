
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var payementsRelevesAchats = require('../model/payements-releves-achats').PayementsRelevesAchats;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addPayementReleveAchats',function(req,res,next){
    
       console.log(" ADD PAYEMENT RELEVE ACHATS IS CALLED") ;
       console.log(" THE PAYEMENT RELEVE ACHATS TO ADDED IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = payementsRelevesAchats.addPayementReleveAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code,
        function(err,payementRelAchatsAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_PAYEMENT_REL_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(payementRelAchatsAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updatePayementReleveAchats',function(req,res,next){
    
       console.log(" UPDATE PAYEMENT RELEVE ACHATS  IS CALLED") ;
       console.log("THE PAYEMENT RELEVE ACHATS TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = payementsRelevesAchats.updatePayementReleveAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code,
         function(err,payementsRelAchatsUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_PAYEMENT_RELEVES_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(payementsRelAchatsUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getPayementsRelevesAchats',function(req,res,next){
 
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
  
    payementsRelevesAchats.getListPayementsRelevesAchats(
        req.app.locals.dataBase,decoded.userData.code, function(err,listPayementsRelAchats){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_PAYEMENTS_RELEVES_ACHATS',
                 error_content: err
             });
          }else{
        
         return res.json(listPayementsRelAchats);
          }
      });

  });
  router.get('/getPayementReleveAchats/:codePayementReleveAchats',function(req,res,next){
    // console.log("GET PAYEMENT RELEVE ACHATS IS CALLED");
    // console.log(req.params['codePayementReleveVentes']);
   
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    payementsRelevesAchats.getPayementReleveAchats(
        req.app.locals.dataBase,req.params['codePayementReleveAchats'],decoded.userData.code,
     function(err,payementRelAchats){
       console.log("GET  PAYEMENT RELEVE ACHATS : RESULT");
       console.log(payementRelAchats);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_PAYEMENT_RELEVE_ACHATS',
               error_content: err
           });
        }else{
      
       return res.json(payementRelAchats);
        }
    });
})

  router.delete('/deletePayementReleveAchats/:codePayementReleveAchats',function(req,res,next){
    
       console.log(" DELETE PAYEMENT RELEVE ACHATS IS CALLED") ;
       console.log(" THE PAYEMENT RELEVE ACHATS TO DELETE IS :",req.params['codePayementReleveAchats']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = payementsRelevesAchats.deletePayementReleveAchats(
        req.app.locals.dataBase,req.params['codePayementReleveAchats'],decoded.userData.code,
        function(err,codePayementReleveAchats){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_PAYEMENT_RELEVE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codePayementReleveAchats']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

   router.get('/getPayementsReleveAchatsByCodeReleve/:codeReleveAchats',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
 // console.log('decoded tocken ');
  // console.log(decoded);
    payementsRelevesAchats.getListPayementsRelevesAchatsByCodeReleve(
        req.app.locals.dataBase,req.params['codeReleveAchats'],decoded.userData.code, function(err,listPayementsRelAchats){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_PAYEMENTS_RELEVES_ACHATS_BY_CODE_RELEVE',
                 error_content: err
             });
          }else{
        
         return res.json(listPayementsRelAchats);
          }
      });

  })

  module.exports.routerPayementsRelevesAchats = router;