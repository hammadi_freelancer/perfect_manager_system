
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var tranchesCreditsAchats = require('../model/tranches-credits-achats').TranchesCreditsAchats;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addTrancheCreditAchats',function(req,res,next){
    
       console.log(" ADD TRANCHE CREDIT ACHATS  IS CALLED") ;
       console.log(" THE TRANCHE CREDIT ACHATS  TO ADDED IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = tranchesCreditsAchats.addTrancheCreditAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,trancheCreditsAchatsAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_TRANCHE_CREDIT_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(trancheCreditsAchatsAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateTrancheCreditAchats',function(req,res,next){
    
       console.log(" UPDATE TRANCHE CREDIT ACHATS IS CALLED") ;
       console.log(" THE TRANCHE CREDIT ACHATS  TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result =tranchesCreditsAchats.updateTrancheCreditAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,trancheCreditAchatsUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_TRANCHE_CREDIT_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(trancheCreditAchatsUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getTranchesCreditsAchats',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    tranchesCreditsAchats.getListTranchesCreditsAchats(
        req.app.locals.dataBase,decoded.userData.code, function(err,listTranchesCreditsAchats){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_TRANCHES_CREDITS_ACHATS',
                 error_content: err
             });
          }else{
        
         return res.json(listTranchesCreditsAchats);
          }
      });

  });
  router.get('/getTrancheCreditAchats/:codeTrancheCreditAchats',function(req,res,next){
    console.log("GET TRANCHE CREDIT ACHATS IS CALLED");
    console.log(req.params['codeTrancheCreditAchats']);
   
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    tranchesCreditsAchats.getTrancheCreditAchats(req.app.locals.dataBase,req.params['codeTrancheCreditAchats'],decoded.userData.code,
     function(err,trancheCreditAchats){
       console.log("GET  TRANCHE CREDIT ACHATS: RESULT");
       console.log(trancheCreditAchats);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_TRANCHE_CREDIT_ACHATS',
               error_content: err
           });
        }else{
      
       return res.json(trancheCreditAchats);
        }
    });
})

  router.delete('/deleteTrancheCreditAchats/:codeTrancheCreditAchats',function(req,res,next){
    
       console.log(" DELETE TRANCHE CREDIT ACHATS  IS CALLED") ;
       console.log(" THE TRANCHE CREDIT  ACHATS TO DELETE IS :",req.params['codeTrancheCreditAchats']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = tranchesCreditsAchats.deleteTrancheCreditAchats(
        req.app.locals.dataBase,req.params['codeTrancheCreditAchats'],decoded.userData.code,
        function(err,codeTrancheCreditAchats){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_TRANCHE_CREDIT_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeTrancheCreditAchats']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getTranchesCreditsAchatsByCodeCredit/:codeCreditAchats',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    tranchesCreditsAchats.getListTranchesCreditsAchatsByCodeCredit(
        req.app.locals.dataBase,decoded.userData.code,req.params['codeCreditAchats'],
         function(err,listTranchesCreditsAchats){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_TRANCHES_CREDITS_ACHATS_BY_CODE_CREDIT',
                 error_content: err
             });
          }else{
        
         return res.json(listTranchesCreditsAchats);
          }
      });

  });
  module.exports.routerTranchesCreditsAchats = router;