//var db = require('../common/MongoDbConnection').getDBConnection;
const mongoClient = require('mongodb').MongoClient;
const url ='mongodb://localhost:27017';
const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var CreditsService = {
getTotalPayementsCreditsByCodeCredit : function (codeCredit,codeUser,callback){
    let listTot = [];
    if(codeCredit != undefined){
      
        
        mongoClient.connect(url,function(err,clientDB){
            if(err)
            {
                callback(err,[]);
            }else{
            const db = clientDB.db(dbName);   
   
           const cursor =  db.collection('PayementCredit').aggregate({ $match: 
            {
                $and: [
                     {"userCreatorCode" : codeUser},
                    { "codeCredit": codeCredit }
                ]
            }
             } , 
             { $group: { _id : null, sum : { $sum: "montantPaye" } } },
            { $project: { _id: 0, sum: 1 } }
         );

         cursor.forEach(function(tot,err){
            if(err)
             {
                 console.log('errors produced :', err);
             }
             
             listTot.push(tot);
        }).then(function(){
       //  console.log('list credits',listCredits);
         callback(null,listTot); 
        });


    }
    });

}else{
callback(true,null);
}
}
}
module.exports.CreditsService = CreditsService;