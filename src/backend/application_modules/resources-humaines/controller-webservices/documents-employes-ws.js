
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var documentsEmployes = require('../model/documents-employes').DocumentsEmployes;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addDocumentEmploye',function(req,res,next){
    
       console.log(" ADD DOCUMENT EMPLOYE  IS CALLED") ;
       console.log(" THE DOCUMENT EMPLOYE  TO ADDED IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = documentsEmployes.addDocumentEmploye(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,documentEmpAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_DOCUMENT_EMPLOYE',
                error_content: err
            });
         }else{
       
        return res.json(documentEmpAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateDocumentEmploye',function(req,res,next){
    
       console.log(" UPDATE DOCUMENT EMPLOYE  IS CALLED") ;
       // console.log(" THE DOCUMENT CREDIT  TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result =documentsEmployes.updateDocumentEmploye(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,documentEmpUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_DOCUMENT_EMPLOYE',
                error_content: err
            });
         }else{
       
        return res.json(documentEmpUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getDocumentsEmployes',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    documentsEmployes.getListDocumentsEmployes(
        req.app.locals.dataBase,decoded.userData.code, function(err,listDocumentsEmps){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_EMPLOYES',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsEmps);
          }
      });

  });
  router.get('/getDocumentEmploye/:codeDocumentEmploye',function(req,res,next){
    console.log("GET DOCUMENT EMPLOYE  IS CALLED");
    console.log(req.params['codeDocumentEmploye']);
   
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    documentsEmployes.getDocumentEmploye(
        req.app.locals.dataBase,req.params['codeDocumentEmploye'],decoded.userData.code,
     function(err,documentEmploye){
     //   console.log("GET  DOCUMENT CREDIT : RESULT");
     //   console.log(documentCredit);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_DOCUMENT_EMPLOYE',
               error_content: err
           });
        }else{
      
       return res.json(documentEmploye);
        }
    });
})

  router.delete('/deleteDocumentEmploye/:codeDocumentEmploye',function(req,res,next){
    
       console.log(" DELETE DOCUMENT EMPLOYE  IS CALLED") ;
       console.log(" THE DOCUMENT EMPLOYE  TO DELETE IS :",req.params['codeDocumentEmploye']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = documentsEmployes.deleteDocumentEmploye(
       req.app.locals.dataBase,req.params['codeDocumentEmploye'],decoded.userData.code,
        function(err,codeDocumentEmploye){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_DOCUMENT_EMPLOYE',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeDocumentEmploye']);
       }
    }
)
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getDocumentsEmployesByCodeEmploye/:codeEmploye',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    documentsEmployes.getListDocumentsEmployesByCodeEmploye(req.app.locals.dataBase,
        decoded.userData.code,req.params['codeEmploye'],
         function(err,listDocumentEmps){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_EMPLOYES_BY_CODE_EMPLOYE',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentEmps);
          }
      });

  });
  module.exports.routerDocumentsEmployes = router;