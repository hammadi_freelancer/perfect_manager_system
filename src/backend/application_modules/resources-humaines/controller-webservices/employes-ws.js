
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var employes = require('../model/employes').Employes;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addEmploye',function(req,res,next){
    
    console.log(" ADD EMPLOYE IS CALLED") ;
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = employes.addEmploye(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,employeAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_EMPLOYE',
                error_content: err
            });
         }else{
       
        return res.json(employeAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateEmploye',function(req,res,next){
    
       console.log(" UPDATE EMPLOYE IS CALLED") ;
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = employes.updateEmploye(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,employeUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_EMPLOYE',
                error_content: err
            });
         }else{
       
        return res.json(employeUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getEmployes',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
      console.log("GET LIST EMPLOYES IS CALLED");
    employes.getListEmployes(
        req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
        req.query.filter,  function(err,listEmployes){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_EMPLOYES',
                 error_content: err
             });
          }else{
        
         return res.json(listEmployes);
          }
      });

  });
  router.get('/getPageEmployes',function(req,res,next){
    
        var decoded = jwt.decode(req.headers.authorization.slice( 
            req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
      
          console.log("GET PAGE EMPLOYES IS CALLED");
        employes.getPageEmployes(
            req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
            req.query.filter, function(err,listEmployes){
              if(err){
                 return  res.json({
                     error_message : 'ERROR_GET_PAGE_EMPLOYES',
                     error_content: err
                 });
              }else{
            
             return res.json(listEmployes);
              }
          });
    
      });
  router.get('/getEmploye/:codeEmploye',function(req,res,next){
    console.log("GET EMPLOYE IS CALLED");
  //  console.log(req.params['codeCredit']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    employes.getEmploye(
        req.app.locals.dataBase,req.params['codeEmploye'],
        decoded.userData.code, function(err,employe){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_EMPLOYE',
               error_content: err
           });
        }else{
      
       return res.json(employe);
        }
    });
})

  router.delete('/deleteEmploye/:codeEmploye',function(req,res,next){
    
       console.log(" DELETE EMPLOYE IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = employes.deleteEmploye(
        req.app.locals.dataBase,req.params['codeEmploye'],decoded.userData.code,
         function(err,codeEmploye){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_EMPLOYE',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeEmploye']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });



 router.get('/getTotalEmployes',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    employes.getTotalEmployes(
        req.app.locals.dataBase,decoded.userData.code,req.query.filter,
        function(err,totalEmployes){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_EMPLOYES',
                 error_content: err
             });
          }else{
        
         return res.json({'totalEmployes':totalEmployes});
          }
      });

  });
  module.exports.routerEmployes = router;