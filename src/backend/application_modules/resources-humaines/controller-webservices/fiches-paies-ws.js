
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var fichesPaies = require('../model/fiches-paies').FichesPaies;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addFichePaie',function(req,res,next){
    
    console.log(" ADD FICHE_PAIE IS CALLED") ;
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = fichesPaies.addFichePaie(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,fichePaieAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_FICHE_PAIE',
                error_content: err
            });
         }else{
       
        return res.json(fichePaieAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateFichePaie',function(req,res,next){
    
       console.log(" UPDATE FICHE_PAIE IS CALLED") ;
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = fichesPaies.updateFichePaie(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,fichePaieUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_FICHE_PAIE',
                error_content: err
            });
         }else{
       
        return res.json(fichePaieUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getFichesPaies',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
      console.log("GET LIST FICHES_PAIES IS CALLED");
    fichesPaies.getListFichesPaies(
        req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
        req.query.filter,  function(err,listFichesPaies){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_FICHES_PAIES',
                 error_content: err
             });
          }else{
        
         return res.json(listFichesPaies);
          }
      });

  });
  router.get('/getPageFichesPaies',function(req,res,next){
    
        var decoded = jwt.decode(req.headers.authorization.slice( 
            req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
      
          console.log("GET PAGE FICHES_PAIES IS CALLED");
        fichesPaies.getPageFichesPaies(
            req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
            req.query.filter, function(err,listFichesPaies){
              if(err){
                 return  res.json({
                     error_message : 'ERROR_GET_PAGE_FICHES_PAIES',
                     error_content: err
                 });
              }else{
            
             return res.json(listFichesPaies);
              }
          });
    
      });
  router.get('/getFichePaie/:codeFichePaie',function(req,res,next){
    console.log("GET FICHE_PAIE IS CALLED");
  //  console.log(req.params['codeCredit']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    fichesPaies.getFichePaie(
        req.app.locals.dataBase,req.params['codeFichePaie'],
        decoded.userData.code, function(err,fichePaie){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_FICHE_PAIE',
               error_content: err
           });
        }else{
      
       return res.json(fichePaie);
        }
    });
})

  router.delete('/deleteFichePaie/:codeFichePaie',function(req,res,next){
    
       console.log(" DELETE FICHE_PAIE IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = fichesPaies.deleteFichePaie(
        req.app.locals.dataBase,req.params['codeFichePaie'],decoded.userData.code,
         function(err,codeFichePaie){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_FICHE_PAIE',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeFichePaie']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });



 router.get('/getTotalFichesPaies',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    fichesPaies.getTotalFichesPaies(
        req.app.locals.dataBase,decoded.userData.code,req.query.filter,
        function(err,totalFichesPaies){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_FICHES_PAIES',
                 error_content: err
             });
          }else{
        
         return res.json({'totalFichesPaies':totalFichesPaies});
          }
      });

  });
  module.exports.routerFichesPaies = router;