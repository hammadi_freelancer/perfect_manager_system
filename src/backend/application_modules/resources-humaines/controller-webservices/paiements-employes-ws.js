
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var paiementsEmployes = require('../model/paiements-employes').PaiementsEmployes;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addPaiementEmploye',function(req,res,next){
    
       console.log(" ADD PAIEMENT EMPLOYE  IS CALLED") ;
       console.log(" THE PAIEMENT EMPLOYE  TO ADDED IS :",req.body);
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = paiementsEmployes.addPaiementEmploye(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,paiementAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_PAIEMENT_EMPLOYE',
                error_content: err
            });
         }else{
       
        return res.json(paiementAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updatePaiementEmploye',function(req,res,next){
    
       console.log(" UPDATE PAIEMENT EMPLOYE  IS CALLED") ;
       console.log(" THE PAIEMENT EMPLOYE TO UPDATE IS :",req.body);
      
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = paiementsEmployes.updatePaiementEmploye(
        req.app.locals.dataBase,req.body,decoded.userData.code,
         function(err,paiementEmployeUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_PAIEMENT_EMPLOYE',
                error_content: err
            });
         }else{
       
        return res.json(paiementEmployeUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getPaiementsEmployes',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    paiementsEmployes.getListPaiementsEmployes(
        req.app.locals.dataBase,decoded.userData.code, function(err,listPaiementsEmployes){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_PAIEMENTS_EMPLOYES',
                 error_content: err
             });
          }else{
        
         return res.json(listPaiementsEmployes);
          }
      });

  });
  router.get('/getPaiementEmploye/:codePaiementEmploye',function(req,res,next){
    console.log("GET PAIEMENT EMPLOYE  IS CALLED");
    console.log(req.params['codePaiementEmploye']);
   
   

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    paiementsEmployes.getPaiementEmploye(
        req.app.locals.dataBase,req.params['codePaiementEmploye'],decoded.userData.code,
     function(err,paiementEmploye){
       console.log("GET  PAIEMENT EMPLOYE : RESULT");
       console.log(paiementEmploye);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_PAIEMENT_EMPLOYE',
               error_content: err
           });
        }else{
      
       return res.json(paiementEmploye);
        }
    });
})

  router.delete('/deletePaiementEmploye/:codePaiementEmploye',function(req,res,next){
    
       console.log(" DELETE PAIEMENT EMPLOYE IS CALLED") ;
       console.log(" THE PAIEMENT EMPLOYE TO DELETE IS :",req.params['codePaiementEmploye']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = paiementsEmployes.deletePaiementEmploye(
        req.app.locals.dataBase,req.params['codePaiementEmploye'],decoded.userData.code,
        function(err,codePaiementEmploye){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_PAIEMENT_EMPLOYE',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codePaiementEmploye']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getPaiementsEmployesByCodeEmploye/:codeEmploye',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    paiementsEmployes.getListPaiementsEmployeByCodeEmploye(
        req.app.locals.dataBase,decoded.userData.code,req.params['codeEmploye'],
         function(err,listPaiementsEmployes){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_PAIEMENTS_EMPLOYES_BY_CODE_EMPLOYE',
                 error_content: err
             });
          }else{
        
         return res.json(listPaiementsEmployes);
          }
      });

  });
  module.exports.routerPaiementsEmployes = router;