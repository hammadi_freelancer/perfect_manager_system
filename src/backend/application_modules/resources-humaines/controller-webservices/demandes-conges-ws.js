
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var demandesConges = require('../model/demandes-conges').DemandesConges;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addDemandeConge',function(req,res,next){
    
    console.log(" ADD DEMANDE_CONGE IS CALLED") ;
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = demandesConges.addDemandeConge(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,demandeAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_DEMANDE_CONGE',
                error_content: err
            });
         }else{
       
        return res.json(demandeAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateDemandeConge',function(req,res,next){
    
       console.log(" UPDATE DEMANDE_CONGE IS CALLED") ;
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = demandesConges.updateDemandeConge(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,demandeUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_DEMANDE_CONGE',
                error_content: err
            });
         }else{
       
        return res.json(demandeUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getDemandesConges',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
      console.log("GET LIST DEMANDE_CONGES IS CALLED");
    demandesConges.getListDemandesConges(
        req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
        req.query.filter,  function(err,listDemandeConges){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DEMANDE_CONGES',
                 error_content: err
             });
          }else{
        
         return res.json(listDemandeConges);
          }
      });

  });
  router.get('/getPageDemandesConges',function(req,res,next){
    
        var decoded = jwt.decode(req.headers.authorization.slice( 
            req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
      
          console.log("GET PAGE DEMANDE_CONGES IS CALLED");
        demandesConges.getPageDemandesConges(
            req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
            req.query.filter, function(err,listDemandeConges){
              if(err){
                 return  res.json({
                     error_message : 'ERROR_GET_PAGE_DEMANDE_CONGES',
                     error_content: err
                 });
              }else{
            
             return res.json(listDemandeConges);
              }
          });
    
      });
  router.get('/getDemandeConge/:codeDemandeConge',function(req,res,next){
    console.log("GET DEMANDE_CONGE IS CALLED");
  //  console.log(req.params['codeCredit']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    demandesConges.getDemandeConge(
        req.app.locals.dataBase,req.params['codeDemandeConge'],
        decoded.userData.code, function(err,demande){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_DEMANDE_CONGE',
               error_content: err
           });
        }else{
      
       return res.json(demande);
        }
    });
})

  router.delete('/deleteDemandeConge/:codeDemandeConge',function(req,res,next){
    
       console.log(" DELETE DEMANDE_CONGE IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = demandesConges.deleteDemandeConge(
        req.app.locals.dataBase,req.params['codeDemandeConge'],decoded.userData.code,
         function(err,codeDemandeConge){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_DEMANDE_CONGE',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeDemandeConge']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });



 router.get('/getTotalDemandesConges',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    demandesConges.getTotalDemandesConges(
        req.app.locals.dataBase,decoded.userData.code,req.query.filter,
        function(err,totalDemandeConges){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_DEMANDE_CONGES',
                 error_content: err
             });
          }else{
        
         return res.json({'totalDemandesConges':totalDemandesConges});
          }
      });

  });
  module.exports.routerDemandesConges = router;