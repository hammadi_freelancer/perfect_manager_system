//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var Employes = {
addEmploye : function (db,employe,codeUser,callback){
    if(employe != undefined){
        employe.code  = utils.isUndefined(employe.code)?shortid.generate():employe.code;
        if(utils.isUndefined(employe.matricule))
            {
               const currentD =  moment(new Date());
              // const genereKey = shortid.generate();
              const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.day().toString()+
              shortid.generate();
              employe.matricule = 'MATR' +generatedCode;
            }
       
            employe.userCreatorCode = codeUser;
            employe.userLastUpdatorCode = codeUser;
            employe.dateCreation = moment(new Date).format('L');
            employe.dateLastUpdate = moment(new Date).format('L');
            
            db.collection('Employe').insertOne(
                employe,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
             // db.close();
             
    

}else{
callback(true,null);
}
},
updateEmploye: function (db,employe,codeUser, callback){

   
    employe.dateLastUpdate = moment(new Date).format('L');
    


    const criteria = { "code" : employe.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
        "matricule" : employe.matricule,  
        "numeroCNSS" : employe.numeroCNSS, 
        "numeroCIN" : employe.numeroCIN ,
         "numeroPassport" : employe.numeroPassport, 
         "numeroCarteSejour":employe.numeroCarteSejour,
         "nom":employe.nom,
         "prenom":employe.nom,
         "nomMere":employe.nom,
         "prenomMere":employe.nom,
         "nomPere":employe.nomPere,
         "dateNaissanceObject":employe.dateNaissanceObject,
         "dateIntegrationObject":employe.dateIntegrationObject,
         "numeroRue":employe.numeroRue,
         "ville":employe.ville,
         "avenue":employe.avenu,
         "pays":employe.avenu,
         "gouvernorat":employe.gouvernorat,
         "email":employe.email,
         "mobile":employe.mobile,
         "tel":employe.tel,
         "nombreEnfants":employe.nombreEnfants,
         "nombreEnfantsHandicapes":employe.nombreEnfantsHandicapes,
         "grade":employe.grade,
         "niveauAcademique":employe.niveauAcademique,
         "postePrincipale":employe.postePrincipale,
         "posteSecondaire":employe.posteSecondaire,
         "competences":employe.competences,
         "nombreAnnesExperiences":employe.nombreAnnesExperiences,
         "typeContrat":employe.typeContrat,
         "etatCivile":employe.etatCivile,
         "description" : employe.description,
         
          "etat": employe.etat,// ouvert/initial/en cours de validation /validé /en cours de validation /cloturé
           "userLastUpdatorCode": codeUser,
           "dateLastUpdate": employe.dateLastUpdate, 
    };
            db.collection('Employe').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },function(employe,err){
                    if(err)
                    callback(false,err);
                    else
                        callback(true,true);
                    
                    
                }
            ) ;
     
    
    },
   
getListEmployes: function (db,codeUser,pageNumber, nbElementsPerPage , filter,callback)
{
    let listEmployes = [];
   
  //console.log('filter recieved is :',filter);
  let filterObject = JSON.parse(filter);
 
   let filterData = {
       "userCreatorCode" : codeUser
   };
   const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
   
   var cursor =  db.collection('Employe').find( 
    conditionBuilt
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );

   cursor.forEach(function(employe,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        employe.dateIntegration= moment(employe.dateIntegrationObject).format('L');
        employe.dateNaissance= moment(employe.dateNaissanceObject).format('L');
       listEmployes.push(employe);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listEmployes); 
   });
     //return [];
},
deleteEmploye: function (db,codeEmploye,codeUser,callback){
    const criteria = { "code" : codeEmploye, "userCreatorCode":codeUser };
    
    db.collection('Employe').deleteOne(
        criteria , function(err,result){
            if(err){
                
                callback(null,null);
            }else{
                callback(false,result);
            }
        }
    ) ;
},

getEmploye: function (db,codeEmploye,codeUser,callback)
{
    const criteria = { "code" : codeEmploye, "userCreatorCode":codeUser };
  
       const employe =  db.collection('Employe').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
        // callback(false,client);
                 
},
getTotalEmployes : function (db,codeUser,filter, callback)
{
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
   var totalEmployes =  db.collection('Employe').find( 
    conditionBuilt
).count(function(err,result){
        callback(null,result); 
    }
);
   


     //return [];
},
getPageEmployes: function (db,codeUser,pageNumber, nbElementsPerPage ,filter, callback)
{
    console.log('filter Employes',filter);
    let listEmployes = [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
      console.log('condition',conditionBuilt);
      
      
   var cursor =  db.collection('Employe').find( conditionBuilt
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(employe,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        employe.dateNaissance= moment(employe.dateNaissanceObject).format('L');
        employe.dateIntegration= moment(employe.dateIntegrationObject).format('L');
        
       // credit.dateDebutPayement = moment(credit.dateDebutPayementObject).format('L');
        
       listEmployes.push(employe);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listEmployes); 
   });
     //return [];
},
buildFilterCondition(filterObject,filterData)
{
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.matricule))
        {
                filterData["matricule"] = filterObject.matricule;
        }
         
        if(!utils.isUndefined(filterObject.etat))
          {
                 
                    filterData["etat"] = filterObject.etat;
                    
         }
         if(!utils.isUndefined(filterObject.dateNaissanceFromObject))
             {
                    
                       filterData["dateNaissanceObject"] = {$gt: filterObject.dateNaissanceFromObject};
                       
            }
            if(!utils.isUndefined(filterObject.dateNaissanceToObject))
             {
                    
                       filterData["dateNaissanceObject"] = {$lt: filterObject.dateNaissanceToObject};
                       
            }
            if(!utils.isUndefined(filterObject.dateIntegrationFromObject))
                {
                       
                          filterData["dateIntegrationObject"] = {$gt: filterObject.dateIntegrationFromObject};
                          
               }
               if(!utils.isUndefined(filterObject.dateIntegrationToObject))
                {
                       
                          filterData["dateIntegrationObject"] = {$lt: filterObject.dateIntegrationToObject};
                          
               }

            if(!utils.isUndefined(filterObject.numeroCIN))
             {
                    
                       filterData["numeroCIN"] = filterObject.numeroCIN ;
                       
            }
            if(!utils.isUndefined(filterObject.numeroCNSS))
             {
                    
                       filterData["numeroCNSS"] = filterObject.numeroCNSS ;
                       
            }
            if(!utils.isUndefined(filterObject.numeroPassport))
             {
                    
                       filterData["numeroPassport"] = filterObject.numeroPassport ;
                       
            }
            if(!utils.isUndefined(filterObject.niveauAcademique))
             {
                    
                       filterData["niveauAcademique"] = filterObject.niveauAcademique ;
                       
            }
            if(!utils.isUndefined(filterObject.nombreEnfants))
             {
                    
                       filterData["nombreEnfants"] = filterObject.nombreEnfants ;
                       
            }
            if(!utils.isUndefined(filterObject.nombreAnnesExperiences))
             {
                    
                       filterData["nombreAnnesExperiences"] = filterObject.nombreAnnesExperiences ;
                       
            }
         
            if(!utils.isUndefined(filterObject.numeroRue))
                {
                       
                          filterData["numeroRue"] = filterObject.numeroRue ;
                          
               }
            
            if(!utils.isUndefined(filterObject.ville))
             {
                    
                       filterData["ville"] = filterObject.ville ;
                       
            }
            if(!utils.isUndefined(filterObject.pays))
                {
                       
                          filterData["pays"] = filterObject.pays ;
                          
               }

            if(!utils.isUndefined(filterObject.nom))
             {
                    
                       filterData["nom"] = filterObject.nom ;
                       
            }
            if(!utils.isUndefined(filterObject.prenom))
             {
                    
                       filterData["prenom"] = filterObject.prenom;
                       
            }
            if(!utils.isUndefined(filterObject.email))
             {
                    
                       filterData["email"] = filterObject.email ;
                       
            }
            if(!utils.isUndefined(filterObject.mobile))
             {
                    
                       filterData["mobile"] = filterObject.mobile ;
                       
            }
            if(!utils.isUndefined(filterObject.tel))
             {
                    
                       filterData["tel"] = filterObject.tel ;
                       
             }
        
     
        }
        return filterData;
},
}
module.exports.Employes = Employes;