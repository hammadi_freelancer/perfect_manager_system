//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var FichesPaies = {
addFichePaie : function (db,fichePaie,codeUser,callback){
    if(fichePaie != undefined){
        fichePaie.code  = utils.isUndefined(fichePaie.code)?shortid.generate():fichePaie.code;
        if(utils.isUndefined(fichePaie.numeroFichePaie))
            {
               const currentD =  moment(new Date());
              // const genereKey = shortid.generate();
              const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.day().toString()+
              shortid.generate();
              fichePaie.numeroFichePaie = 'FI@Paie' +generatedCode;
            }
       
            fichePaie.userCreatorCode = codeUser;
            fichePaie.userLastUpdatorCode = codeUser;
            fichePaie.dateCreation = moment(new Date).format('L');
            fichePaie.dateLastUpdate = moment(new Date).format('L');
            
            db.collection('FichePaie').insertOne(
                fichePaie,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
             // db.close();
             
    

}else{
callback(true,null);
}
},
updateFichePaie: function (db,fichePaie,codeUser, callback){

   
    fichePaie.dateLastUpdate = moment(new Date).format('L');
    


    const criteria = { "code" : fichePaie.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
        "numeroFichePaie" : fichePaie.numeroFichePaie,  
        "employe" : fichePaie.employe, 
        "dateObject" : fichePaie.dateObject ,
         "periodeFromObject" : fichePaie.periodeFromObject, 
         "periodeToObject":fichePaie.periodeToObject,
         "mois":fichePaie.mois,
         "nombreJoursTravailles":fichePaie.nombreJoursTravailles,
         "nombreJoursConges":fichePaie.nombreJoursConges,
         "nombreHeuresConges":fichePaie.nombreHeuresConges,
         "nombreHeuresTravailles":fichePaie.nombreHeuresTravailles,
         "salaireBase":fichePaie.salaireBase,
         "primesRendement":fichePaie.primesRendement,
         "primesTransport":fichePaie.primesTransport,
         "primesPresence":fichePaie.primesPresence,
         "primesObjectifs":fichePaie.primesObjectifs,
         "autresPrimes":fichePaie.autresPrimes,
         "salaireBrut":fichePaie.salaireBrut,
         "valeurImpots":fichePaie.valeurImpots,
         "salaireNet":fichePaie.salaireNet,
         "description" : fichePaie.description,
         
          "etat": fichePaie.etat,// ouvert/initial/en cours de validation /validé /en cours de validation /cloturé
           "userLastUpdatorCode": codeUser,
           "dateLastUpdate": fichePaie.dateLastUpdate, 
    };
            db.collection('FichePaie').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },function(fichePaie,err){
                    if(err)
                    callback(false,err);
                    else
                        callback(true,true);
                    
                    
                }
            ) ;
     
    
    },
   
getListFichesPaies: function (db,codeUser,pageNumber, nbElementsPerPage , filter,callback)
{
    let listFichesPaies = [];
   
  //console.log('filter recieved is :',filter);
  let filterObject = JSON.parse(filter);
 
   let filterData = {
       "userCreatorCode" : codeUser
   };
   const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
   
   var cursor =  db.collection('FichePaie').find( 
    conditionBuilt
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );

   cursor.forEach(function(fichePaie,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        fichePaie.date= moment(fichePaie.dateObject).format('L');
        fichePaie.periodeFrom= moment(fichePaie.periodeFromObject).format('L');
        fichePaie.periodeTo= moment(fichePaie.periodeToObject).format('L');
        
       listFichesPaies.push(fichePaie);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listFichesPaies); 
   });
     //return [];
},
deleteFichePaie: function (db,codeFichePaie,codeUser,callback){
    const criteria = { "code" : codeFichePaie, "userCreatorCode":codeUser };
    
    db.collection('FichePaie').deleteOne(
        criteria , function(err,result){
            if(err){
                
                callback(null,null);
            }else{
                callback(false,result);
            }
        }
    ) ;
},

getFichePaie: function (db,codeFichePaie,codeUser,callback)
{
    const criteria = { "code" : codeFichePaie, "userCreatorCode":codeUser };
  
       const fichePaie =  db.collection('FichePaie').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
        // callback(false,client);
                 
},
getTotalFichesPaies : function (db,codeUser,filter, callback)
{
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
   var totalFichesPaies =  db.collection('FichePaie').find( 
    conditionBuilt
).count(function(err,result){
        callback(null,result); 
    }
);
   


     //return [];
},
getPageFichesPaies: function (db,codeUser,pageNumber, nbElementsPerPage ,filter, callback)
{
    let listFichesPaies = [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
      console.log('condition',conditionBuilt);
      
      
   var cursor =  db.collection('FichePaie').find( conditionBuilt
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(fichePaie,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
      
        
       // credit.dateDebutPayement = moment(credit.dateDebutPayementObject).format('L');
       fichePaie.date= moment(fichePaie.dateObject).format('L');
       fichePaie.periodeFrom= moment(fichePaie.periodeFromObject).format('L');
       fichePaie.periodeTo= moment(fichePaie.periodeToObject).format('L');
       listFichesPaies.push(fichePaie);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listFichesPaies); 
   });
     //return [];
},
buildFilterCondition(filterObject,filterData)
{
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.numeroFichePaie))
        {
                filterData["numeroFichePaie"] = filterObject.numeroFichePaie;
        }
         
        if(!utils.isUndefined(filterObject.etat))
          {
                 
                    filterData["etat"] = filterObject.etat;
                    
         }
         if(!utils.isUndefined(filterObject.dateFromObject))
             {
                    
                       filterData["dateObject"] = {$gt: filterObject.dateFromObject};
                       
            }
            if(!utils.isUndefined(filterObject.dateToObject))
             {
                    
                       filterData["dateObject"] = {$lt: filterObject.dateToObject};
                       
            }
          

            if(!utils.isUndefined(filterObject.cinEmploye))
             {
                    
                       filterData["employe.cin"] = filterObject.cinEmploye ;
                       
            }
            if(!utils.isUndefined(filterObject.nomEmploye))
             {
                    
                       filterData["employe.nom"] = filterObject.nomEmploye ;
                       
            }
            if(!utils.isUndefined(filterObject.prenomEmploye))
             {
                    
                       filterData["employe.prenom"] = filterObject.prenomEmploye ;
                       
            }
     
        }
        return filterData;
},
}
module.exports.FichesPaies = FichesPaies;