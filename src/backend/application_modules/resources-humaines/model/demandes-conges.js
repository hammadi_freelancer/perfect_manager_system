//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var DemandesConges = {
addDemandeConge : function (db,demandeConge,codeUser,callback){
    if(demandeConge != undefined){
        demandeConge.code  = utils.isUndefined(demandeConge.code)?shortid.generate():demandeConge.code;
        if(utils.isUndefined(demandeConge.numeroDemande))
            {
               const currentD =  moment(new Date());
              // const genereKey = shortid.generate();
              const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.day().toString()+
              shortid.generate();
              demandeConge.numeroDemande = 'DEM@CONG' +generatedCode;
            }
       
            demandeConge.userCreatorCode = codeUser;
            demandeConge.userLastUpdatorCode = codeUser;
            demandeConge.dateCreation = moment(new Date).format('L');
            demandeConge.dateLastUpdate = moment(new Date).format('L');
            
            db.collection('DemandeConge').insertOne(
                demandeConge,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
             // db.close();
             
    

}else{
callback(true,null);
}
},
updateDemandeConge: function (db,demandeConge,codeUser, callback){

   
    demandeConge.dateLastUpdate = moment(new Date).format('L');
    


    const criteria = { "code" : demandeConge.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
        "numeroDemande" : demandeConge.numeroDemande,  
        "employe" : demandeConge.employe, 
        "dateObject" : demandeConge.dateObject ,
         "periodeFromObject" : demandeConge.periodeFromObject, 
         "periodeToObject":demandeConge.periodeToObject,
         "dureeJours":demandeConge.dureeJours,
         "dureeHeures":demandeConge.dureeHeures,
         "motif":demandeConge.motif,
         "description" : demandeConge.description,
         
          "etat": demandeConge.etat,// ouvert/initial/en cours de validation /validé /en cours de validation /cloturé
           "userLastUpdatorCode": codeUser,
           "dateLastUpdate": demandeConge.dateLastUpdate, 
    };
            db.collection('DemandeConge').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },function(demandeConge,err){
                    if(err)
                    callback(false,err);
                    else
                        callback(true,true);
                    
                    
                }
            ) ;
     
    
    },
   
getListDemandesConges: function (db,codeUser,pageNumber, nbElementsPerPage , filter,callback)
{
    let listDemandesConges = [];
   
  //console.log('filter recieved is :',filter);
  let filterObject = JSON.parse(filter);
 
   let filterData = {
       "userCreatorCode" : codeUser
   };
   const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
   
   var cursor =  db.collection('DemandeConge').find( 
    conditionBuilt
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );

   cursor.forEach(function(demandeConge,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        demandeConge.date= moment(demandeConge.dateObject).format('L');
        demandeConge.periodeFrom= moment(demandeConge.periodeFromObject).format('L');
        demandeConge.periodeTo= moment(demandeConge.periodeToObject).format('L');
        
        listDemandesConges.push(demandeConge);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listDemandesConges); 
   });
     //return [];
},
deleteDemandeConge: function (db,codeDemandeConge,codeUser,callback){
    const criteria = { "code" : codeDemandeConge, "userCreatorCode":codeUser };
    
    db.collection('DemandeConge').deleteOne(
        criteria , function(err,result){
            if(err){
                
                callback(null,null);
            }else{
                callback(false,result);
            }
        }
    ) ;
},

getDemandeConge: function (db,codeDemandeConge,codeUser,callback)
{
    const criteria = { "code" : codeDemandeConge, "userCreatorCode":codeUser };
  
       const demande =  db.collection('DemandeConge').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
        // callback(false,client);
                 
},
getTotalDemandesConges : function (db,codeUser,filter, callback)
{
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
   var totalDemandes =  db.collection('DemandeConge').find( 
    conditionBuilt
).count(function(err,result){
        callback(null,result); 
    }
);
   


     //return [];
},
getPageDemandesConges: function (db,codeUser,pageNumber, nbElementsPerPage ,filter, callback)
{
    let listDemandesConges = [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
      console.log('condition',conditionBuilt);
      
      
   var cursor =  db.collection('DemandeConge').find( conditionBuilt
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(demandeConge,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        demandeConge.date= moment(demandeConge.dateObject).format('L');
        demandeConge.periodeFrom= moment(demandeConge.periodeFromObject).format('L');
        demandeConge.periodeTo= moment(demandeConge.periodeToObject).format('L');
        
       // credit.dateDebutPayement = moment(credit.dateDebutPayementObject).format('L');
        
       listDemandesConges.push(demandeConge);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listDemandesConges); 
   });
     //return [];
},
buildFilterCondition(filterObject,filterData)
{
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.numeroDemande))
        {
                filterData["numeroDemande"] = filterObject.numeroDemande;
        }
         
        if(!utils.isUndefined(filterObject.nomEmploye))
          {
                 
                    filterData["employe.nom"] = filterObject.nomEmploye;
                    
         }

         if(!utils.isUndefined(filterObject.prenomEmploye))
            {
                   
                      filterData["employe.prenom"] = filterObject.prenomEmploye;
                      
           }

           if(!utils.isUndefined(filterObject.cinEmploye))
            {
                   
                      filterData["employe.numeroCIN"] = filterObject.cinEmploye;
                      
           }

           if(!utils.isUndefined(filterObject.matriculeEmploye))
            {
                   
                      filterData["employe.matricule"] = filterObject.matriculeEmploye;
                      
           }
     
        }
        return filterData;
},
}
module.exports.DemandesConges = DemandesConges;