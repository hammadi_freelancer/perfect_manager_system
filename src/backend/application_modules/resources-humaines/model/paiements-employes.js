//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var PaiementsEmployes = {
addPaiementEmploye : function (db,paiementEmploye,codeUser,callback){
    if(paiementEmploye != undefined){
        paiementEmploye.code  = utils.isUndefined(paiementEmploye.code)?shortid.generate():paiementEmploye.code;
        paiementEmploye.dateOperationObject = utils.isUndefined(paiementEmploye.dateOperationObject)? 
        new Date():paiementEmploye.dateOperationObject;
        paiementEmploye.userCreatorCode = codeUser;
        paiementEmploye.userLastUpdatorCode = codeUser;
        paiementEmploye.dateCreation = moment(new Date).format('L');
        paiementEmploye.dateLastUpdate = moment(new Date).format('L');
        
        if(utils.isUndefined(paiementEmploye.numeroPaiement))
            {
               const currentD =  moment(new Date());
              // const genereKey = shortid.generate();
              const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.day().toString()+
              shortid.generate();
              paiementEmploye.numeroPaiement = 'PAI' +generatedCode;
            }
            db.collection('PaiementEmploye').insertOne(
                paiementEmploye,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,paiementEmploye);
                   }
                }
              ) ;

}else{
callback(true,null);
}
},
updatePaiementEmploye: function (db,paiementEmploye,codeUser, callback){

   
    paiementEmploye.dateOperationObject = utils.isUndefined(paiementEmploye.dateOperationObject)? 
    new Date():paiementEmploye.dateOperationObject;
    paiementEmploye.dateLastUpdate = moment(new Date).format('L');
    
    const criteria = { "code" : paiementEmploye.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
        "numeroPaiement" : paiementEmploye.numeroPaiement,  
        "motif" : paiementEmploye.motif, 
        "datePaimentObject" : paiementEmploye.datePaiementObject ,
         "montant" : paiementEmploye.montant, 
         "description" : paiementEmploye.description,
         "imageFace1" : paiementEmploye.imageFace1,
         "imageFace2" : paiementEmploye.imageFace2,
          "etat": paiementEmploye.etat,// ouvert/initial/en cours de validation /validé /en cours de validation /cloturé
           "userLastUpdatorCode": codeUser,
           "dateLastUpdate": paiementEmploye.dateLastUpdate, 
        } ;

   
            db.collection('PaiementEmploye').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
           
    
    
    },
getListPaiementsEmployes : function (db,codeUser,callback)
{
    let listPaiementsEmployes  = [];
  
   var cursor =  db.collection('PaiementEmploye').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(payEmp,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        payEmp.datePaiement = moment(payEmp.datePaiementObject).format('L');
        
        listPaiementsEmployes.push(payEmp);
   }).then(function(){
  //  console.log('list credits',listPayementsFacturesVentes);
    callback(null,listPaiementsEmployes); 
   });

     //return [];
},
deletePaiementEmploye: function (db,codePaiementEmploye,codeUser,callback){
    const criteria = { "code" : codePaiementEmploye, "userCreatorCode":codeUser };
       
            db.collection('PaiementEmploye').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
      
},

getPaiementEmploye: function (db,codePaiementEmploye,codeUser,callback)
{
    const criteria = { "code" : codePaiementEmploye, "userCreatorCode":codeUser };
  
       const paiementEmp =  db.collection('PaiementEmploye').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                     
},

getListPaiementsEmployeByCodeEmploye: function (db,codeEmploye, codeUser,callback)
{
    let listPaiementsEmployes = [];
    
   var cursor =  db.collection('PaiementEmploye').find( 
       {"userCreatorCode" : codeUser, "codeEmploye": codeEmploye}
    );
   cursor.forEach(function(paiementEmploye,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        paiementEmploye.datePaiement = moment(paiementEmploye.datePaiementObject).format('L');
       //  payementFactureAchats.dateCheque = moment(payementFactureAchats.dateChequeObject).format('L');
        
       listPaiementsEmployes.push(paiementEmploye);
   }).then(function(){
   // console.log('list credits',listCredits);
    callback(null,listPaiementsEmployes); 
   });
   

     //return [];
},
}
module.exports.PaiementsEmployes = PaiementsEmployes;