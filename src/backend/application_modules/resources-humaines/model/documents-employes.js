var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

var DocumentsEmployes = {
addDocumentEmploye : function (db,documentEmploye,codeUser,callback){
    if(documentEmploye != undefined){
        documentEmploye.code  = utils.isUndefined(documentEmploye.code)?'DOC@EMP'+shortid.generate():documentEmploye.code;
        documentEmploye.dateReceptionObject = utils.isUndefined(documentEmploye.dateReceptionObject)? 
        new Date():documentEmploye.dateReceptionObject;
        documentEmploye.userCreatorCode = codeUser;
        documentEmploye.userLastUpdatorCode = codeUser;
        documentEmploye.dateCreation = moment(new Date).format('L');
        documentEmploye.dateLastUpdate = moment(new Date).format('L');
            db.collection('DocumentEmploye').insertOne(
                documentEmploye,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,documentEmploye);
                   }
                }
              ) ;


}else{
callback(true,null);
}
},
updateDocumentEmploye: function (db,documentEmploye,codeUser, callback){

   
    documentEmploye.dateReceptionObject = utils.isUndefined(documentEmploye.dateReceptionObject)? 
    new Date():documentEmploye.dateReceptionObject;
    documentEmploye.dateLastUpdate = moment(new Date).format('L');
    
    const criteria = { "code" : documentEmploye.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
        "dateReceptionObject" : documentEmploye.dateReceptionObject, 
        "codeCompteClient" : documentEmploye.codeCompteClient, 
        "description" : documentEmploye.description,
         "codeOrganisation" : documentEmploye.codeOrganisation,
         "typeDocument" : documentEmploye.typeDocument, // cheque , virement , espece 
         "numeroDocument" : documentEmploye.numeroDocument, 
         "imageFace1":documentEmploye.imageFace1,
         "imageFace2" : documentEmploye.imageFace2 , 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": documentEmploye.dateLastUpdate, 
    } ;
      
            db.collection('DocumentEmploye').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
    
    
    },
getListDocumentsEmployes : function (db,codeUser,callback)
{
    let listDocumentsEmployes = [];
    
   var cursor =  db.collection('DocumentEmploye').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(docEmp,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        docEmp.dateReception = moment(docEmp.dateReceptionObject).format('L');
        listDocumentsEmployes.push(docEmp);
   }).then(function(){
   //  console.log('list payements credits',listPayementsCredits);
    callback(null,listDocumentsEmployes); 
   });
   
},
getPageDocumentsEmployes : function (db,codeUser,pageNumber, nbElementsPerPage , callback)
{
    let listDocEmployes = [];
   
   var cursor =  db.collection('DocumentEmploye').find( 
       {"userCreatorCode" : codeUser}
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(docEmploye,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        docEmploye.dateReception = moment(docEmploye.dateReceptionObject).format('L');
        
        listDocEmployes.push(docEmploye);
   }).then(function(){
    callback(null,listDocEmployes); 
   });
   
},
deleteDocumentEmploye: function (db,codeDocumentEmploye,codeUser,callback){
    const criteria = { "code" : codeDocumentEmploye, "userCreatorCode":codeUser };
       
            db.collection('DocumentEmploye').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
    
},

getDocumentEmploye: function (db,codeDocumentEmploye,codeUser,callback)
{
    const criteria = { "code" : codeDocumentEmploye, "userCreatorCode":codeUser };
 
       const docEmp =  db.collection('DocumentEmploye').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    callback(false,result);
                }
            }
        ) ;
                         
},
getListDocumentsEmployesByCodeEmploye : function (db,codeUser,codeEmploye,callback)
{
    let listDocumentsEmployes = [];
    
   var cursor =  db.collection('DocumentEmploye').find( 
       {"userCreatorCode" : codeUser, "codeEmploye": codeEmploye}
    );
   cursor.forEach(function(documentEmploye,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        documentEmploye.dateReception = moment(documentEmploye.dateReceptionObject).format('L');
        
        listDocumentsEmployes.push(documentEmploye);
   }).then(function(){
  // console.log('list doc credits',listCredits);
    callback(null,listDocumentsEmployes); 
   });
     //return [];
},
getTotalDocumentsEmployes : function (db,codeUser, callback)
{
    let listDocEmployes = [];
   
   var totalDocEmployes =  db.collection('DocumentEmploye').find( 
       {"userCreatorCode" : codeUser}
    ).count(function(err,result){
        callback(null,result); 
    }
);

}
}
module.exports.DocumentsEmployes = DocumentsEmployes;