/**
 * Created by Hammadi on 20/11/2018.
 */
var mongoClient = require('mongodb').MongoClient;
var url ='mongodb://localhost/gestion';
var db = null;

function getDBConnection()
{
  if(db === null){
    mongoClient.connect(url,function(err,db){
        if(err){
        console.log('error in the connection  to the database ',url); 
        return null;  
        }
        console.log('connected to the database ',url);
        this.db = db;
        return db;
    })
  }else{
      return db;
  }
    
}
module.exports.getDBConnection = getDBConnection();