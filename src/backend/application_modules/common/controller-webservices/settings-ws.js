
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var settings = require('../model/settings').Settings;
// var creditsService = require('../service/credits-ser').CreditsService;

// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addSetting',function(req,res,next){
    
       console.log(" ADD SETTING IS CALLED") ;
       console.log(" THE SETTING TO ADDED IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = settings.addSetting(req.body,decoded.userData.code,function(err,settingAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_SETTING',
                error_content: err
            });
         }else{
       
        return res.json(settingAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateSetting',function(req,res,next){
    
       console.log(" UPDATE SETTING IS CALLED") ;
       console.log(" THE SETTING TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = settings.updateSetting(req.body,decoded.userData.code, function(err,settingUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_SETTING',
                error_content: err
            });
         }else{
       
        return res.json(settingUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

   router.get('/getSettingsByEntityName',function(req,res,next){
    
        var decoded = jwt.decode(req.headers.authorization.slice( 
            req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
        settings.getListSettings(decoded.userData.code,req.query.entityName ,function(err,listSettings){
            
              if(err){
                 return  res.json({
                     error_message : 'ERROR_GET_LIST_SETTINGS_BY_ENTITY_NAME',
                     error_content: err
                 });
              }else{
            
             return res.json(listSettings);
              }
          });
    
      })
router.get('/getSettings',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    settings.getListSettings(decoded.userData.code,req.
         req.query.pageNumber, req.query.nbElementsPerPage ,function(err,listSettings){
        
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_SETTINGS',
                 error_content: err
             });
          }else{
        
         return res.json(listSettings);
          }
      });

  });
  router.get('/getSetting/:codeSetting',function(req,res,next){
    console.log("GET SETTING IS CALLED");
    console.log(req.params['codeSetting']);
    console.log(req.headers.authorization.slice(  req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice(  req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    settings.getSetting(req.params['codeSetting'],decoded.userData.code, function(err,setting){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_SETTING',
               error_content: err
           });
        }else{
      
       return res.json(setting);
        }
    });
})

  router.delete('/deleteSetting/:codeSetting',function(req,res,next){
    
       console.log(" DELETE SETTING IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice(   req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = settings.deleteSetting(req.params['codeSetting'],decoded.userData.code, function(err,codeSetting){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_ENTREE',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeSetting']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });



 router.get('/getTotalSettings',function(req,res,next){
 
    var decoded = jwt.decode(req.headers.authorization.slice( req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    settings.getTotalSettings(decoded.userData.code,function(err,totalSettings){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_SETTINGS',
                 error_content: err
             });
          }else{
        
         return res.json({'totalSettings':totalSettings});
          }
      });

  });
 /* router.get('/getTotalPayementsCreditsByCodeCredit/:codeCredit',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
        creditsService.getTotalPayementsCreditsByCodeCredit(req.params['codeCredit'],
        decoded.userData.code,function(err,totalPayCredits){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_PAYEMENTS_CREDITS',
                 error_content: err
             });
          }else{
        
         return res.json({'totalPayementsCredits':totalPayCredits});
          }
      });

  });*/
  module.exports.routerSettings = router;