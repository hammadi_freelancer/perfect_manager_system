

var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var documents = require('../model/documents').Documents;

router.post('/addDocument',function(req,res,next){
    
       console.log(" ADD DOCUMENT  IS CALLED") ;
       console.log(" THE DOCUMENT  TO ADDED IS :",req.body);
       var result = documents.addDocument(req.body,function(err,documentAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_DOCUMENT',
                error_content: err
            });
         }else{
       
        return res.json(documentAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.put('/updateDocument',function(req,res,next){
    
       console.log(" UPDATE DOCUMENT  IS CALLED") ;
       console.log(" THE DOCUMENT  TO UPDATE IS :",req.body);
       var result = documents.updateDocument(req.body,function(err,documentUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_DOCUMENT',
                error_content: err
            });
         }else{
       
        return res.json(documentUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getDocuments',function(req,res,next){
    console.log("GET LIST DOCUMENTS  IS CALLED");
    documents.getListDocuments(function(err,listDocuments){
       console.log("GET LIST DOCUMENTS  : RESULT");
       console.log(listDocuments);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_DOCUMENTS',
               error_content: err
           });
        }else{
      
       return res.json(listDocuments);
        }
    });
})

router.delete('/deleteDocument/:codeDocument',function(req,res,next){
    
       console.log(" DELETE DOCUMENT  IS CALLED") ;
       console.log(" THE DOCUMENT  TO DELETE IS :",req.params['codeDocument']);
       var result = documents.deleteDocument(req.params['codeDocument'],function(err,codeDocument){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_DOCUMENT',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeDocument']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

module.exports.documentsRouter = router;