
var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var taxes = require('../model/taxes').Taxes;


router.get('/getTaxes',function(req,res,next){
    
      console.log("GET LIST TAXES IS CALLED");
      taxes.getListTaxes(function(err,listTaxes)
    {
         console.log("GET LIST TAXES : RESULT");
         console.log(listTaxes);
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_TAXES',
                 error_content: err
             });
          }else{
        
         return res.json(listTaxes);
          }
      });
  });




router.post('/addTaxe',function(req,res,next){
    console.log(" ADD TAXE IS CALLED");
    console.log(" THE TAXE TO ADDED IS :",req.body);
    var result = taxes.addTaxe(req.body);
    console.log("THE RESULT OF ADDING THE TAXE IS ",result);
});

module.exports.routerTaxes = router;