

var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var documentsTypes = require('../model/documents-types').DocumentTypes;

router.post('/addDocumentType',function(req,res,next){
    
       console.log(" ADD DOCUMENT TYPE IS CALLED") ;
       console.log(" THE DOCUMENT TYPE TO ADDED IS :",req.body);
       var result = documentsTypes.addDocumentType(req.body,function(err,documentTypeAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_DOCUMENT_TYPE',
                error_content: err
            });
         }else{
       
        return res.json(documentTypeAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.put('/updateDocumentType',function(req,res,next){
    
       console.log(" UPDATE DOCUMENT TYPE IS CALLED") ;
       console.log(" THE DOCUMENT TYPE TO UPDATE IS :",req.body);
       var result = documentsTypes.updateDocumentType(req.body,function(err,documentTypeUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_DOCUMENT_TYPE',
                error_content: err
            });
         }else{
       
        return res.json(documentTypeUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getDocumentsTypes',function(req,res,next){
    console.log("GET LIST DOCUMENTS TYPES IS CALLED");
    documentTypes.getListDocumentTypes(function(err,listDocumentsTypes){
       console.log("GET LIST DOCUMENTS TYPES : RESULT");
       console.log(listDocumentsTypes);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_DOCUMENTS_TYPES',
               error_content: err
           });
        }else{
      
       return res.json(listDocumentsTypes);
        }
    });
})

router.delete('/deleteDocumentType/:codeDocumentType',function(req,res,next){
    
       console.log(" DELETE DOCUMENT TYPE IS CALLED") ;
       console.log(" THE DOCUMENT TYPE TO DELETE IS :",req.params['codeDocumentType']);
       var result = documentsTypes.deleteDocumentType(req.params['codeDocumentType'],function(err,codeDocumentType){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_DOCUMENT_TYPE',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeDocumentType']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

module.exports.documentsTypesRouter = router;