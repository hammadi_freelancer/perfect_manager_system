//var db = require('../common/MongoDbConnection').getDBConnection;
const mongoClient = require('mongodb').MongoClient;
const url ='mongodb://localhost:27017';
const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var Settings = {
/*initialize : function (codeUser,callback){
    var settings = [
        {entityName : 'EMPLOYE',columnsToDisplay:
    ['matricule','numeroCNSS','numeroCIN','nom','prenom','dateNaissance','dateIntegration',
    'numeroRue','avenue','ville','pays','email','mobile','typeContrat']
},
{entityName : 'CLIENT',columnsToDisplay:
['code','cin','nom','prenom','raisonSociale','matriculeFiscale',
'adresse','mobile','email','mobile']
}
];

    
        mongoClient.connect(url,function(err,clientDB){
            if(err)
             {
                callback(true,"ERROR_DATABASE_CONNECTION");
             }
            console.log('the client connected is ');
            console.log(clientDB)
            const db = clientDB.db(dbName);
            db.collection('Settings').insertMany(
                journal,function(err,result){
                    clientDB.close();
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
             // db.close();
             
        });


}*/
addSetting : function (setting,codeUser,callback){
    if(setting != undefined){
        setting.code  = utils.isUndefined(journal.code)?'SET'+shortid.generate():journal.code;
        setting.userCreatorCode = codeUser;
        setting.userLastUpdatorCode = codeUser;
        setting.dateCreation = moment(new Date).format('L');
        setting.dateLastUpdate = moment(new Date).format('L');
        
        mongoClient.connect(url,function(err,clientDB){
            if(err)
             {
                callback(true,"ERROR_DATABASE_CONNECTION");
             }
            console.log('the client connected is ');
            console.log(clientDB)
            const db = clientDB.db(dbName);
            db.collection('Setting').insertOne(
                setting,function(err,result){
                    clientDB.close();
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
             // db.close();
             
        });

}else{
callback(true,null);
}
}
,
updateSetting: function (setting,codeUser, callback){

   
    setting.dateLastUpdate = moment(new Date).format('L');
    
    const criteria = { "userCreatorCode" : codeUser , "entityName": setting.entityName};
    const dataToUpdate = {
    "columnsToDisplay" : setting.columnsToDisplay,  
     "description" : setting.description,
     "isDefault" : setting.isDefault,
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": setting.dateLastUpdate, 
    } ;
        mongoClient.connect(url,function(err,clientDB){
            if(err)
             {
                callback(true,"ERROR_DATABASE_CONNECTION");
             }
            console.log('the client connected is ');
            console.log(clientDB)
            const db = clientDB.db(dbName);
            db.collection('Setting').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                }
            ) ;
             clientDB.close();
             callback(false,journal);
        });
    
    },
getListSettings : function (codeUser,pageNumber, nbElementsPerPage , callback)
{
    let listSettings = [];
    mongoClient.connect(url,function(err,clientDB){
        if(err)
        {
            callback(err,[]);
        }else{
        const db = clientDB.db(dbName);  
   var cursor =  db.collection('Setting').find( 
       {"userCreatorCode" : codeUser}
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(setting,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        //journal.dateJournal = moment(journal.dateJournalObject).format('L');
       // credit.dateDebutPayement = moment(credit.dateDebutPayementObject).format('L');
        
       listSettings.push(setting);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listSettings); 
   });
   
}
});
     //return [];
},

getListSettingsByEntityName : function (codeUser,entityName , callback)
{
    let listSettings = [];
    mongoClient.connect(url,function(err,clientDB){
        if(err)
        {
            callback(err,[]);
        }else{
        const db = clientDB.db(dbName);  
   var cursor =  db.collection('Setting').find( 
       {"userCreatorCode" : codeUser, "entityName":entityName}
    );
   cursor.forEach(function(setting,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        //journal.dateJournal = moment(journal.dateJournalObject).format('L');
       // credit.dateDebutPayement = moment(credit.dateDebutPayementObject).format('L');
        
       listSettings.push(setting);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listSettings); 
   });
   
}
});
     //return [];
},
deleteSetting: function (codeSetting,codeUser,callback){
    const criteria = { "code" : codeSetting, "userCreatorCode":codeUser };
        mongoClient.connect(url,function(err,clientDB){
            if(err)
             {
                callback(true,"ERROR_DATABASE_CONNECTION");
             }
            console.log('the client connected is ');
            console.log(clientDB)
            const db = clientDB.db(dbName);
            db.collection('Setting').deleteOne(
                criteria 
            ) ;
             clientDB.close();
             callback(false,codeSetting);
        });
},

getSetting: function (codeSetting,codeUser,callback)
{
    const criteria = { "code" : codeSetting, "userCreatorCode":codeUser };
    mongoClient.connect(url,function(err,clientDB){
        if(err)
         {
            callback(true,"ERROR_DATABASE_CONNECTION");
         }
        console.log('the client connected is ');
        console.log(clientDB)
        const db = clientDB.db(dbName);
       const setting =  db.collection('Setting').findOne(
            criteria , function(err,result){
                if(err){
                    clientDB.close();
                    
                    callback(null,null);
                }else{
                    clientDB.close();
                    
                    callback(false,result);
                }
            }
        ) ;
        // callback(false,client);
    });                    
},
getTotalSettings : function (codeUser, callback)
{
    let listSettings = [];
    mongoClient.connect(url,function(err,clientDB){
        if(err)
        {
            callback(err,[]);
        }else{
        const db = clientDB.db(dbName);   
   var totalSettings =  db.collection('Setting').find( 
       {"userCreatorCode" : codeUser, "entityName":entityName}
    ).count(function(err,result){
        callback(null,result); 
    }
);
   
}
});
     //return [];
}
}
module.exports.Settings = Settings;