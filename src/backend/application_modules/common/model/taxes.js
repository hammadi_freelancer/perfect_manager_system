//var db = require('../common/MongoDbConnection').getDBConnection;
const mongoClient = require('mongodb').MongoClient;
const url ='mongodb://localhost:27017';
const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
//we need to have article as a json object : {code :'mmm', design :'ddd'}
var Taxes = {
addTaxe : function (taxe){
    if(taxe != undefined){
        taxe.code  = taxe.code===undefined?shortid.generate():taxe.code;
    mongoClient.connect(url,function(err,clientDB){
        console.log('the client connected is ');
        console.log(clientDB)
        const db = clientDB.db(dbName);
        db.collection('Taxe').insertOne(
            taxe
          ) ;
         // db.close();
         clientDB.close();
    });

       return true;  
}
return false;
},
updateTaxe: function (criteria, dataUpdated){
    db.collection('Taxe').updateOne(
        criteria,{
            $set : dataUpdated
        }
      ) ;
      return true; 
},
getListTaxes : function (callback)
{
    let listTaxes = [];
    mongoClient.connect(url,function(err,clientDB){
        if(err)
        {
            callback(err,[]);
        }else{
        const db = clientDB.db(dbName);   
   var cursor =  db.collection('Taxe').find();
   cursor.forEach(function(taxe,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        listTaxes.push(taxe);
   }).then(function(){
    console.log('list taxes',listTaxes);
    callback(null,listTaxes); 
   });
   
}
});
     //return [];
},
deleteTaxe: function (){

},
getTaxeWithCode: function (code)
{
   return  db.collection('Taxe').findOne(
        
         { 'code' : code});                     
}
}
module.exports.Taxes = Taxes;