//var db = require('../common/MongoDbConnection').getDBConnection;
const mongoClient = require('mongodb').MongoClient;
const url ='mongodb://localhost:27017';
const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
//we need to have article as a json object : {code :'mmm', design :'ddd'}
var Documents = {
addDocument : function (document, callback){
    if(document != undefined){
        document.code  = document.code===undefined?shortid.generate():document.code;
    mongoClient.connect(url,function(err,clientDB){
        if(err)
         {
            callback(true,"ERROR_DATABASE_CONNECTION");
         }
        console.log('the client connected is ');
        console.log(clientDB)
        const db = clientDB.db(dbName);
        db.collection('Document').insertOne(
            document
          ) ;
         // db.close();
         clientDB.close();
         callback(false,document);
    });

       //return true;  
}
//return false;
},
updateDocument: function (document, callback){
    const criteria = { "code" : document.code };
    const dataToUpdate = {"type" : document.type, "description" : document.description,
    "dateDelivraison" : document.dateDelivraison} ;
        mongoClient.connect(url,function(err,clientDB){
            if(err)
             {
                callback(true,"ERROR_DATABASE_CONNECTION");
             }
            console.log('the client connected is ');
            console.log(clientDB)
            const db = clientDB.db(dbName);
            db.collection('Document').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                }
            ) ;
             clientDB.close();
             callback(false,document);
        });
    
    },
getListDocuments : function (callback)
{
    let listDocuments = [];
    mongoClient.connect(url,function(err,clientDB){
        if(err)
        {
            callback("ERROR_DATABASE_CONNECTION",[]);
        }else{
        const db = clientDB.db(dbName);   
   var cursor =  db.collection('Document').find();
   cursor.forEach(function(document,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        listDocuments.push(document);
   }).then(function(){
    console.log('list Documents',listDocuments);
    callback(null,listDocuments); 
   });
   
}
});
     //return [];
},
deleteDocument: function (codeDocument,callback){
    const criteria = { "code" : codeDocument };
        mongoClient.connect(url,function(err,clientDB){
            if(err)
             {
                callback(true,"ERROR_DATABASE_CONNECTION");
             }
            console.log('the client connected is ');
            console.log(clientDB)
            const db = clientDB.db(dbName);
            db.collection('Document').deleteOne(
                criteria 
            ) ;
             clientDB.close();
             callback(false,codeDocument);
        });
},
getDocumentTypesWithCode: function (code)
{
   return  db.collection('Document').findOne(
        
         { 'code' : code});                     
}
}
module.exports.Documents = Documents;