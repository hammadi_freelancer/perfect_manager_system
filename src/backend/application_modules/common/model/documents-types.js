//var db = require('../common/MongoDbConnection').getDBConnection;
const mongoClient = require('mongodb').MongoClient;
const url ='mongodb://localhost:27017';
const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
//we need to have article as a json object : {code :'mmm', design :'ddd'}
var DocumentTypes = {
addDocumentType : function (documentType, callback){
    if(documentType != undefined){
        documentType.code  = documentType.code===undefined?shortid.generate():documentType.code;
    mongoClient.connect(url,function(err,clientDB){
        if(err)
         {
            callback(true,"ERROR_DATABASE_CONNECTION");
         }
        console.log('the client connected is ');
        console.log(clientDB)
        const db = clientDB.db(dbName);
        db.collection('DocumentType').insertOne(
            documentType
          ) ;
         // db.close();
         clientDB.close();
         callback(false,documentType);
    });

       //return true;  
}
//return false;
},
updateDocumentType: function (documentType, callback){
    const criteria = { "code" : documentType.code };
    const dataToUpdate = {"type" : documentType.type, "description" : documentType.description,
    "dateDelivraison" : documentType.dateDelivraison} ;
        mongoClient.connect(url,function(err,clientDB){
            if(err)
             {
                callback(true,"ERROR_DATABASE_CONNECTION");
             }
            console.log('the client connected is ');
            console.log(clientDB)
            const db = clientDB.db(dbName);
            db.collection('DocumentType').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                }
            ) ;
             clientDB.close();
             callback(false,documentType);
        });
    
    },
getListDocumentTypes : function (callback)
{
    let listDocumentTypes = [];
    mongoClient.connect(url,function(err,clientDB){
        if(err)
        {
            callback("ERROR_DATABASE_CONNECTION",[]);
        }else{
        const db = clientDB.db(dbName);   
   var cursor =  db.collection('DocumentType').find();
   cursor.forEach(function(documentType,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        listDocumentTypes.push(documentType);
   }).then(function(){
    console.log('list Documents Types',listDocumentTypes);
    callback(null,listDocumentTypes); 
   });
   
}
});
     //return [];
},
deleteDocumentType: function (codeDocumentType,callback){
    const criteria = { "code" : codeDocumentType };
        mongoClient.connect(url,function(err,clientDB){
            if(err)
             {
                callback(true,"ERROR_DATABASE_CONNECTION");
             }
            console.log('the client connected is ');
            console.log(clientDB)
            const db = clientDB.db(dbName);
            db.collection('DocumentType').deleteOne(
                criteria 
            ) ;
             clientDB.close();
             callback(false,codeDocumentType);
        });
},
getDocumentTypesWithCode: function (code)
{
   return  db.collection('DocumentType').findOne(
        
         { 'code' : code});                     
}
}
module.exports.DocumentTypes = DocumentTypes;