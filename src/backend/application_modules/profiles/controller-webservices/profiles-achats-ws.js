
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var profilesAchats = require('../model/profiles-achats').ProfilesAchats;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addProfileAchats',function(req,res,next){
    
    console.log(" ADD PROFILE ACHATS IS CALLED") ;
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = profilesAchats.addProfileAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,profileAchatsAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_PROFILE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(profileAchatsAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateProfileAchats',function(req,res,next){
    
       console.log(" UPDATE PROFILE ACHATS IS CALLED") ;
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = profilesAchats.updateProfileAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,profileAchatsUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_PROFILE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(profileAchatsUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getProfilesAchats',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
      console.log("GET LIST PROFILES  ACHATS IS CALLED");
    profilesAchats.getListProfilesAchats(
        req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
        req.query.filter,  function(err,listProfiles){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_PROFILES_ACHATS',
                 error_content: err
             });
          }else{
        
         return res.json(listProfiles);
          }
      });

  });
  router.get('/getPageProfilesAchats',function(req,res,next){
    
        var decoded = jwt.decode(req.headers.authorization.slice( 
            req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
      
          console.log("GET PAGE PROFILES ACHATS IS CALLED");
        profilesAchats.getPageProfilesAchats(
            req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
            req.query.filter, function(err,listProfilesAchats){
              if(err){
                 return  res.json({
                     error_message : 'ERROR_GET_PAGE_PROFILES_ACHATS',
                     error_content: err
                 });
              }else{
            
             return res.json(listProfilesAchats);
              }
          });
    
      });
  router.get('/getProfileAchats/:codeProfileAchats',function(req,res,next){
    console.log("GET PROFILE ACHATS IS CALLED");
  //  console.log(req.params['codeCredit']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    profilesAchats.getProfileAchats(
        req.app.locals.dataBase,req.params['codeProfileAchats'],
        decoded.userData.code, function(err,profileAchats){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_PROFILE_ACHATS',
               error_content: err
           });
        }else{
      
       return res.json(profileAchats);
        }
    });
})

  router.delete('/deleteProfileAchats/:codeProfileAchats',function(req,res,next){
    
       console.log(" DELETE PROFILE ACHATS IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = profilesAchats.deleteProfileAchats(
        req.app.locals.dataBase,req.params['codeProfileAchats'],decoded.userData.code, function(err,codeProfileAchats){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_PROFILE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeProfileAchats']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });



 router.get('/getTotalProfilesAchats',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    profilesAchats.getTotalProfilesAchats(
        req.app.locals.dataBase,decoded.userData.code,req.query.filter,
        function(err,totalProfilesAchats){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_PROFILES_ACHATS',
                 error_content: err
             });
          }else{
        
         return res.json({'totalProfilesAchats':totalProfilesAchats});
          }
      });

  });
  module.exports.routerProfilesAchats = router;