
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var profilesVentes = require('../model/profiles-ventes').ProfilesVentes;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addProfileVentes',function(req,res,next){
    
    console.log(" ADD PROFILE VENTES IS CALLED") ;
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = profilesVentes.addProfileVentes(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,profileVentesAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_PROFILE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(profileVentesAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateProfileVentes',function(req,res,next){
    
       console.log(" UPDATE PROFILE VENTES IS CALLED") ;
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = profilesVentes.updateProfileVentes(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,profileVentesUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_PROFILE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(profileVentesUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getProfilesVentes',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
      console.log("GET LIST PROFILES  VENTES IS CALLED");
    profilesVentes.getListProfilesVentes(
        req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
        req.query.filter,  function(err,listProfiles){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_PROFILES_VENTES',
                 error_content: err
             });
          }else{
        
         return res.json(listProfiles);
          }
      });

  });
  router.get('/getPageProfilesVentes',function(req,res,next){
    
        var decoded = jwt.decode(req.headers.authorization.slice( 
            req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
      
          console.log("GET PAGE PROFILES VENTES IS CALLED");
        profilesVentes.getPageProfilesVentes(
            req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
            req.query.filter, function(err,listProfilesVentes){
              if(err){
                 return  res.json({
                     error_message : 'ERROR_GET_PAGE_PROFILES_VENTES',
                     error_content: err
                 });
              }else{
            
             return res.json(listProfilesVentes);
              }
          });
    
      });
  router.get('/getProfileVentes/:codeProfileVentes',function(req,res,next){
    console.log("GET PROFILE VENTES IS CALLED");
  //  console.log(req.params['codeCredit']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    profilesVentes.getProfileVentes(
        req.app.locals.dataBase,req.params['codeProfileVentes'],
        decoded.userData.code, function(err,profileVentes){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_PROFILE_VENTES',
               error_content: err
           });
        }else{
      
       return res.json(profileVentes);
        }
    });
})

  router.delete('/deleteProfileVentes/:codeProfileVentes',function(req,res,next){
    
       console.log(" DELETE PROFILE VENTES IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = profilesVentes.deleteProfileVentes(
        req.app.locals.dataBase,req.params['codeProfileVentes'],decoded.userData.code, function(err,codeProfileVentes){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_PROFILE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeProfileVentes']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });



 router.get('/getTotalProfilesVentes',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    profilesVentes.getTotalProfilesVentes(
        req.app.locals.dataBase,decoded.userData.code,req.query.filter,
        function(err,totalProfilesVentes){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_PROFILES_VENTES',
                 error_content: err
             });
          }else{
        
         return res.json({'totalProfilesVentes':totalProfilesVentes});
          }
      });

  });
  module.exports.routerProfilesVentes = router;