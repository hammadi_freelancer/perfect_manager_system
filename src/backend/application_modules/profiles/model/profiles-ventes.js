//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var ProfilesVentes = {
addProfileVentes : function (db,profileVentes,codeUser,callback){
    if(profileVentes != undefined){
        profileVentes.code  = utils.isUndefined(profileVentes.code)?shortid.generate():profileVentes.code;

        profileVentes.dateProfileVentesObject = utils.isUndefined(profileVentes.dateProfileVentesObject)? 
        new Date():profileVentes.dateProfileVentesObject;
     
        profileVentes.userCreatorCode = codeUser;
        profileVentes.userLastUpdatorCode = codeUser;
        profileVentes.dateCreation = moment(new Date).format('L');
        profileVentes.dateLastUpdate = moment(new Date).format('L');
            db.collection('ProfileVentes').insertOne(
                profileVentes,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
             // db.close();
             
    

}else{
callback(true,null);
}
},
updateProfileVentes: function (db,profileVentes,codeUser, callback){
  
    const criteria = { "code" : profileVentes.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
   "description" :profileVentes.description,
   "valeurTimbreFiscal" :profileVentes.valeurTimbreFiscal,
   "inclureTimbreFiscal" :profileVentes.inclureTimbreFiscal,
   "articleSearchBy" :profileVentes.articleSearchBy,
   "articleSearchBy" :profileVentes.articleSearchBy,
   "unMesureSearchBy" :profileVentes.unMesureSearchBy,
   "configurationSearchBy" :profileVentes.configurationSearchBy,
   "inclureConfigurations" :profileVentes.inclureConfigurations,
   "inclureUnMesure" :profileVentes.inclureUnMesure,
   "magasinSearchBy" :profileVentes.magasinSearchBy,
   "inclureMagasins" :profileVentes.inclureMagasins,
   "inclureRemise" :profileVentes.inclureRemise,
   "defaultRemisePourcentage" :profileVentes.defaultRemisePourcentage,
   "defaultPrixVentePourcentage" :profileVentes.defaultPrixVentePourcentage,
   "inclurePrixVentePublicFacture" :profileVentes.inclurePrixVentePublicFacture,
   "default4rdTaxeLibelle" :profileVentes.default4rdTaxeLibelle,
   "default4rdTaxePourcentage" :profileVentes.default4rdTaxePourcentage,
   "inclure4rdTaxeFacture" :profileVentes.inclure4rdTaxeFacture,

   "default3rdTaxePourcentage" :profileVentes.default3rdTaxePourcentage,
   "inclure3rdTaxeFacture" :profileVentes.inclure3rdTaxeFacture,
   "default3rdTaxeLibelle" :profileVentes.default3rdTaxeLibelle,
   

   "inclureBeneficeFacture" :profileVentes.inclureBeneficeFacture,
   "defaultBeneficePourcentage" :profileVentes.defaultBeneficePourcentage,

   "inclureTVAFacture" :profileVentes.inclureTVAFacture,
   
   "defaultTVAPourcentage" :profileVentes.defaultTVAPourcentage,
   "inclureFodecFacture" :profileVentes.inclureFodecFacture,
   "defaultFodecPourcentage" :profileVentes.defaultFodecPourcentage,
   
      "etat": profileVentes.etat,// ouvert/initial/en cours de validation /validé /en cours de validation /cloturé
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": profileVentes.dateLastUpdate, 
    
       "tvaClaculated" :profileVentes.tvaClaculated,
       "prixClaculated" :profileVentes.prixClaculated,
       "quantiteClaculated" :profileVentes.quantiteClaculated,
       "attenteReceptionCalculated" :profileVentes.attenteReceptionCalculated,
       "attenteReceptionCalculated" :profileVentes.attenteReceptionCalculated,
       "strategieCaclulTVA" :profileVentes.strategieCaclulTVA,
       "strategieCaclulPrix" :profileVentes.strategieCaclulPrix,
       "entityCalculPrix" :profileVentes.entityCalculPrix,
       "entityCalculTVA" :profileVentes.entityCalculTVA,
       "entityCalculQuantite" :profileVentes.entityCalculQuantite,
       "entityCalculAttenteReception" :profileVentes.entityCalculAttenteReception
    } 
            db.collection('ProfileVentes').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },function(profileVentes,err){
                    if(err)
                    callback(false,err);
                    else
                        callback(true,true);
                    
                    
                }
            ) ;
     
    
    },
getListProfilesVentes: function (db,codeUser,pageNumber, nbElementsPerPage ,filter, callback)
{
    
    let listProfilesVentes = [];
    
   //console.log('filter recieved is :',filter);
   let filterObject = JSON.parse(filter);
  
    let filterData = {
        "userCreatorCode" : codeUser
    };
    const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
    
    var cursor =  db.collection('ProfileVentes').find( 
     conditionBuilt
     ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
     .limit(Number(nbElementsPerPage) );
    cursor.forEach(function(profileVentes,err){
        if(err)
         {
             console.log('errors produced :', err);
             callback(null,false); 
             
         }
        // credit.dateDebutPayement = moment(credit.dateDebutPayementObject).format('L');
         
        listProfilesVentes.push(profileVentes);
    }).then(function(){
     // console.log('list credits',listCredits);
     callback(null,listProfilesVentes); 
    });
      //return [];
},

getTotalProfilesVentes : function (db,codeUser,filter, callback)
{
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
   var totalProfileVentes =  db.collection('ProfileVentes').find( 
    conditionBuilt
).count(function(err,result){
        callback(null,result); 
    }
);
   


     //return [];
},
getPageProfilesVentes : function (db,codeUser,pageNumber, nbElementsPerPage ,filter, callback)
{
    let listProfilesVentes= [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
      console.log('condition',conditionBuilt);
      
      
   var cursor =  db.collection('ProfileVentes').find( conditionBuilt
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(profileVentes,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        listProfilesVentes.push(profileVentes);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listProfilesVentes); 
   });
     //return [];
},
buildFilterCondition(filterObject,filterData)
{
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.description))
        {
                filterData["description"] = filterObject.description;
        }
        if(!utils.isUndefined(filterObject.code))
            {
                    filterData["code"] = filterObject.code;
            }
       }
        
     return filterData;
 },
deleteProfileVentes: function (db,codeProfileVentes,codeUser,callback){
    const criteria = { "code" : codeJournal, "userCreatorCode":codeUser };
    
    db.collection('ProfileVentes').deleteOne(
        criteria , function(err,result){
            if(err){
                
                callback(null,null);
            }else{
                callback(false,result);
            }
        }
    ) ;
},

getProfileVentes: function (db,codeProfileVentes,codeUser,callback)
{
    const criteria = { "code" : codeJournal, "userCreatorCode":codeUser };
  
       const profileVentes =  db.collection('ProfileVentes').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
        // callback(false,client);
                 
}
}
module.exports.ProfilesVentes = ProfilesVentes;