//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var ProfilesAchats = {
addProfileAchats : function (db,profileAchats,codeUser,callback){
    if(profileAchats != undefined){
        profileAchats.code  = utils.isUndefined(profileAchats.code)?shortid.generate():profileAchats.code;

        profileAchats.dateProfileAchatsObject = utils.isUndefined(profileAchats.dateProfileAchatsObject)? 
        new Date():profileAchats.dateProfileAchatsObject;
     
        profileAchats.userCreatorCode = codeUser;
        profileAchats.userLastUpdatorCode = codeUser;
        profileAchats.dateCreation = moment(new Date).format('L');
        profileAchats.dateLastUpdate = moment(new Date).format('L');
            db.collection('ProfileAchats').insertOne(
                profileAchats,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
             // db.close();
             
    

}else{
callback(true,null);
}
},
updateProfileAchats: function (db,profileAchats,codeUser, callback){
  
    const criteria = { "code" : profileAchats.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
   "description" :profileAchats.description,
   "valeurTimbreFiscal" :profileAchats.valeurTimbreFiscal,
   "inclureTimbreFiscal" :profileAchats.inclureTimbreFiscal,
   "articleSearchBy" :profileAchats.articleSearchBy,
   "articleSearchBy" :profileAchats.articleSearchBy,
   "unMesureSearchBy" :profileAchats.unMesureSearchBy,
   "configurationSearchBy" :profileAchats.configurationSearchBy,
   "inclureConfigurations" :profileAchats.inclureConfigurations,
   "inclureUnMesure" :profileAchats.inclureUnMesure,
   "magasinSearchBy" :profileAchats.magasinSearchBy,
   "inclureMagasins" :profileAchats.inclureMagasins,
   "inclureRemise" :profileAchats.inclureRemise,
   "defaultRemisePourcentage" :profileAchats.defaultRemisePourcentage,
   "defaultPrixVentePourcentage" :profileAchats.defaultPrixVentePourcentage,
   "inclurePrixVentePublicFacture" :profileAchats.inclurePrixVentePublicFacture,
   "default4rdTaxeLibelle" :profileAchats.default4rdTaxeLibelle,
   "default4rdTaxePourcentage" :profileAchats.default4rdTaxePourcentage,
   "inclure4rdTaxeFacture" :profileAchats.inclure4rdTaxeFacture,

   "default3rdTaxePourcentage" :profileAchats.default3rdTaxePourcentage,
   "inclure3rdTaxeFacture" :profileAchats.inclure3rdTaxeFacture,
   "default3rdTaxeLibelle" :profileAchats.default3rdTaxeLibelle,
   

   "inclureBeneficeFacture" :profileAchats.inclureBeneficeFacture,
   "defaultBeneficePourcentage" :profileAchats.defaultBeneficePourcentage,

   "inclureTVAFacture" :profileAchats.inclureTVAFacture,
   
   "defaultTVAPourcentage" :profileAchats.defaultTVAPourcentage,
   "inclureFodecFacture" :profileAchats.inclureFodecFacture,
   "defaultFodecPourcentage" :profileAchats.defaultFodecPourcentage,
   
      "etat": profileAchats.etat,// ouvert/initial/en cours de validation /validé /en cours de validation /cloturé
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": profileAchats.dateLastUpdate, 
    
       "tvaClaculated" :profileAchats.tvaClaculated,
       "prixClaculated" :profileAchats.prixClaculated,
       "quantiteClaculated" :profileAchats.quantiteClaculated,
       "attenteReceptionCalculated" :profileAchats.attenteReceptionCalculated,
       "attenteReceptionCalculated" :profileAchats.attenteReceptionCalculated,
       "strategieCaclulTVA" :profileAchats.strategieCaclulTVA,
       "strategieCaclulPrix" :profileAchats.strategieCaclulPrix,
       "entityCalculPrix" :profileAchats.entityCalculPrix,
       "entityCalculTVA" :profileAchats.entityCalculTVA,
       "entityCalculQuantite" :profileAchats.entityCalculQuantite,
       "entityCalculAttenteReception" :profileAchats.entityCalculAttenteReception
    } 
            db.collection('ProfileAchats').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },function(profileAchats,err){
                    if(err)
                    callback(false,err);
                    else
                        callback(true,true);
                    
                    
                }
            ) ;
     
    
    },
getListProfilesAchats: function (db,codeUser,pageNumber, nbElementsPerPage ,filter, callback)
{
    
    let listProfilesAchats = [];
    
   //console.log('filter recieved is :',filter);
   let filterObject = JSON.parse(filter);
  
    let filterData = {
        "userCreatorCode" : codeUser
    };
    const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
    
    var cursor =  db.collection('ProfileAchats').find( 
     conditionBuilt
     ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
     .limit(Number(nbElementsPerPage) );
    cursor.forEach(function(profileAchats,err){
        if(err)
         {
             console.log('errors produced :', err);
             callback(null,false); 
             
         }
        // credit.dateDebutPayement = moment(credit.dateDebutPayementObject).format('L');
         
        listProfilesAchats.push(profileAchats);
    }).then(function(){
     // console.log('list credits',listCredits);
     callback(null,listProfilesAchats); 
    });
      //return [];
},

getTotalProfilesAchats : function (db,codeUser,filter, callback)
{
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
   var totalProfileAchats =  db.collection('ProfileAchats').find( 
    conditionBuilt
).count(function(err,result){
        callback(null,result); 
    }
);
   


     //return [];
},
getPageProfilesAchats : function (db,codeUser,pageNumber, nbElementsPerPage ,filter, callback)
{
    let listProfilesAchats= [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
      console.log('condition',conditionBuilt);
      
      
   var cursor =  db.collection('ProfileAchats').find( conditionBuilt
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(profileAchats,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        listProfilesAchats.push(profileAchats);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listProfilesAchats); 
   });
     //return [];
},
buildFilterCondition(filterObject,filterData)
{
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.description))
        {
                filterData["description"] = filterObject.description;
        }
        if(!utils.isUndefined(filterObject.code))
            {
                    filterData["code"] = filterObject.code;
            }
       }
        
     return filterData;
 },
deleteProfileAchats: function (db,codeProfileAchats,codeUser,callback){
    const criteria = { "code" : codeJournal, "userCreatorCode":codeUser };
    
    db.collection('ProfileAchats').deleteOne(
        criteria , function(err,result){
            if(err){
                
                callback(null,null);
            }else{
                callback(false,result);
            }
        }
    ) ;
},

getProfileAchats: function (db,codeProfileAchats,codeUser,callback)
{
    const criteria = { "code" : codeJournal, "userCreatorCode":codeUser };
  
       const profileAchats =  db.collection('ProfileAchats').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
        // callback(false,client);
                 
}
}
module.exports.ProfilesAchats = ProfilesAchats;