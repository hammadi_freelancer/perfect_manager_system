
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var operationsCourantes = require('../model/operations-courantes').OperationsCourantes;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addOperationCourante',function(req,res,next){
    
    console.log(" ADD OPERATION COURANTE IS CALLED") ;
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = operationsCourantes.addOperationCourante(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,operationAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_OPERATION_COURANTE',
                error_content: err
            });
         }else{
       
        return res.json(operationAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateOperationCourante',function(req,res,next){
    
       console.log(" UPDATE OPERATION COURANTE IS CALLED") ;
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = operationsCourantes.updateOperationCourante(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,operationUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_OPERATION',
                error_content: err
            });
         }else{
       
        return res.json(operationUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getOperationsCourantes',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
      console.log("GET LIST OPERATIONS COURANTES IS CALLED");
    operationsCourantes.getListOperationsCourantes(
        req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
        req.query.filter,  function(err,listOperations){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_OPERATIONS_COURANTES',
                 error_content: err
             });
          }else{
        
         return res.json(listOperations);
          }
      });

  });
  router.get('/getPageOperationsCourantes',function(req,res,next){
    
        var decoded = jwt.decode(req.headers.authorization.slice( 
            req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
      
          console.log("GET PAGE OPERATIONS COURANTES IS CALLED");
        operationsCourantes.getPageOperationsCourantes(
            req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
            req.query.filter, function(err,listOperations){
              if(err){
                 return  res.json({
                     error_message : 'ERROR_GET_PAGE_OPERATIONS_COURANTES',
                     error_content: err
                 });
              }else{
            
             return res.json(listOperations);
              }
          });
    
      });
  router.get('/getOperationCourante/:codeOperationCourante',function(req,res,next){
    console.log("GET OPERATION COURANTE IS CALLED");
  //  console.log(req.params['codeCredit']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    operationsCourantes.getOperationCourante(
        req.app.locals.dataBase,req.params['codeOperationCourante'],
        decoded.userData.code, function(err,operationCourante){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_OPERATION_COURANTE',
               error_content: err
           });
        }else{
      
       return res.json(operationCourante);
        }
    });
})

  router.delete('/deleteOperationCourante/:codeOperationCourante',function(req,res,next){
    
       console.log(" DELETE OPERATION COURANTE IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = operationsCourantes.deleteOperationCourante(
        req.app.locals.dataBase,req.params['codeOperationCourante'],decoded.userData.code, function(err,codeOperationCourante){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_OPERATION_COURANTE',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeOperationCourante']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });



 router.get('/getTotalOperationsCourantes',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    operationsCourantes.getTotalOperationsCourantes(
        req.app.locals.dataBase,decoded.userData.code,req.query.filter,
        function(err,totalOperationsCourantes){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_OPERATIONS_COURANTES',
                 error_content: err
             });
          }else{
        
         return res.json({'totalOperationsCourantes':totalOperationsCourantes});
          }
      });

  });
  module.exports.routerOperationsCourantes = router;