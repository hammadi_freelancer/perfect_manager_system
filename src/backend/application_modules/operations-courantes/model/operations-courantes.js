//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var OperationsCourantes = {
addOperationCourante : function (db,operationCourante,codeUser,callback){
    if(operationCourante != undefined){
        operationCourante.code  = utils.isUndefined(operationCourante.code)?shortid.generate():operationCourante.code;

        operationCourante.dateOperationObject = utils.isUndefined(operationCourante.dateOperationObject)? 
        new Date():operationCourante.dateOperationObject;
        if(utils.isUndefined(operationCourante.numeroOperationCourante))
            {
               const currentD =  moment(new Date());
              // const genereKey = shortid.generate();
              const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.day().toString()+
              shortid.generate();
              operationCourante.numeroOperationCourante = 'OPER' +generatedCode;
            }
       
            operationCourante.userCreatorCode = codeUser;
            operationCourante.userLastUpdatorCode = codeUser;
            operationCourante.dateCreation = moment(new Date).format('L');
            operationCourante.dateLastUpdate = moment(new Date).format('L');
            
            db.collection('OperationCourante').insertOne(
                operationCourante,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
             // db.close();
             
    

}else{
callback(true,null);
}
},
updateOperationCourante: function (db,operationCourante,codeUser, callback){

   
    operationCourante.dateLastUpdate = moment(new Date).format('L');
    
    operationCourante.dateOperationObject = utils.isUndefined(operationCourante.dateOperationObject)? 
    new Date():operationCourante.dateOperationObject;

 

    const criteria = { "code" : operationCourante.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
    "dateOperationObject" : operationCourante.dateOperationObject, 
    
    
     "client": operationCourante.client,
     "fournisseur": operationCourante.fournisseur,
     "valeurBenefice": operationCourante.valeurBenefice,
     "valeurFinanciere": operationCourante.valeurFinanciere,
     "payementData": operationCourante.payementData,
     "modePaiement": operationCourante.modePaiement,
     "articlesImpliques": operationCourante.articlesImpliques,
     "etat": operationCourante.etat,
     "type": operationCourante.type,
     "livre": operationCourante.livre,
     "recu": operationCourante.recu,
     "lignesOperationCourante": operationCourante.lignesOperationCourante,
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": operationCourante.dateLastUpdate, 
    } ;
            db.collection('OperationCourante').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },function(operationCourante,err){
                    if(err)
                    callback(false,err);
                    else
                        callback(true,true);
                    
                    
                }
            ) ;
     
    
    },
   
getListOperationsCourantes : function (db,codeUser,pageNumber, nbElementsPerPage , filter,callback)
{
    let listOperationsCourantes = [];
   
  //console.log('filter recieved is :',filter);
  let filterObject = JSON.parse(filter);
 
   let filterData = {
       "userCreatorCode" : codeUser
   };
   const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
   
   var cursor =  db.collection('OperationCourante').find( 
    conditionBuilt
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(operationCourante,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        operationCourante.dateOperation= moment(operationCourante.dateOperationObject).format('L');
       // credit.dateDebutPayement = moment(credit.dateDebutPayementObject).format('L');
        
       listOperationsCourantes.push(operationCourante);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listOperationsCourantes); 
   });
     //return [];
},
deleteOperationCourante: function (db,codeOperationCourante,codeUser,callback){
    const criteria = { "code" : codeOperationCourante, "userCreatorCode":codeUser };
    
    db.collection('OperationCourante').deleteOne(
        criteria , function(err,result){
            if(err){
                
                callback(null,null);
            }else{
                callback(false,result);
            }
        }
    ) ;
},

getOperationCourante: function (db,codeOperationCourante,codeUser,callback)
{
    const criteria = { "code" : codeOperationCourante, "userCreatorCode":codeUser };
  
       const operationCourante =  db.collection('OperationCourante').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
        // callback(false,client);
                 
},
getTotalOperationsCourantes : function (db,codeUser,filter, callback)
{
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
   var totalOperationsCourantes =  db.collection('OperationCourante').find( 
    conditionBuilt
).count(function(err,result){
        callback(null,result); 
    }
);
   


     //return [];
},
getPageOperationsCourantes : function (db,codeUser,pageNumber, nbElementsPerPage ,filter, callback)
{
    console.log('filter OperationCourantes',filter);
    let listOperationsCourantes = [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
      console.log('condition',conditionBuilt);
      
      
   var cursor =  db.collection('OperationCourante').find( conditionBuilt
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(operationCourante,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        operationCourante.dateOperation= moment(operationCourante.dateOperationObject).format('L');
       // credit.dateDebutPayement = moment(credit.dateDebutPayementObject).format('L');
        
       listOperationsCourantes.push(operationCourante);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listOperationsCourantes); 
   });
     //return [];
},
buildFilterCondition(filterObject,filterData)
{
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.numeroOperationCourante))
        {
                filterData["numeroOperationCourante"] = filterObject.numeroOperationCourante;
        }
         
        if(!utils.isUndefined(filterObject.etat))
          {
                 
                    filterData["etat"] = filterObject.etat;
                    
         }
         if(!utils.isUndefined(filterObject.dateOperationFromObject))
             {
                    
                       filterData["dateOperationObject"] = {$gt: filterObject.dateOperationFromObject};
                       
            }
            if(!utils.isUndefined(filterObject.dateOperationToObject))
             {
                    
                       filterData["dateOperationObject"] = {$lt: filterObject.dateOperationToObject};
                       
            }
            if(!utils.isUndefined(filterObject.cinClient))
             {
                    
                       filterData["client.cin"] = filterObject.cinClient ;
                       
            }
            if(!utils.isUndefined(filterObject.nomClient))
             {
                    
                       filterData["client.nom"] = filterObject.nomClient ;
                       
            }
            if(!utils.isUndefined(filterObject.prenomClient))
             {
                    
                       filterData["client.prenom"] = filterObject.prenomClient ;
                       
            }
            if(!utils.isUndefined(filterObject.raisonClient))
             {
                    
                       filterData["client.raisonSociale"] = filterObject.raisonClient ;
                       
            }
            if(!utils.isUndefined(filterObject.matriculeClient))
             {
                    
                       filterData["client.matriculeFiscale"] = filterObject.matriculeClient ;
                       
            }
            if(!utils.isUndefined(filterObject.registreClient))
             {
                    
                       filterData["client.registreCommerce"] = filterObject.registreClient ;
                       
            }
         
     
     
            if(!utils.isUndefined(filterObject.cinFournisseur))
             {
                    
                       filterData["fournisseur.cin"] = filterObject.cinFournisseur ;
                       
            }
            if(!utils.isUndefined(filterObject.nomFournisseur))
             {
                    
                       filterData["fournisseur.nom"] = filterObject.nomFournisseur ;
                       
            }
            if(!utils.isUndefined(filterObject.prenomFournisseur))
             {
                    
                       filterData["fournisseur.prenom"] = filterObject.prenomFournisseur;
                       
            }
            if(!utils.isUndefined(filterObject.raisonFournisseur))
             {
                    
                       filterData["fournisseur.raisonSociale"] = filterObject.raisonFournisseur ;
                       
            }
            if(!utils.isUndefined(filterObject.matriculeFournisseur))
             {
                    
                       filterData["fournisseur.matriculeFiscale"] = filterObject.matriculeFournisseur ;
                       
            }
            if(!utils.isUndefined(filterObject.registreFournisseur))
             {
                    
                       filterData["fournisseur.registreCommerce"] = filterObject.registreFournisseur ;
                       
            }
     
     
            if(!utils.isUndefined(filterObject.valeurBenefice))
             {
                    
                       filterData["valeurBenefice"] = filterObject.valeurBenefice ;
                       
            }
            if(!utils.isUndefined(filterObject.valeurFinanciere))
             {
                    
                       filterData["valeurFinanciere"] = filterObject.valeurFinanciere ;
                       
            }
            if(!utils.isUndefined(filterObject.modePaiement))
             {
                    
                       filterData["modePaiement"] = filterObject.modePaiement ;
                       
            }
            if(!utils.isUndefined(filterObject.type))
             {
                    
                       filterData["type"] = filterObject.type ;
                       
            }
            if(!utils.isUndefined(filterObject.livre))
             {
                    
                       filterData["livre"] = filterObject.livre ;
                       
            }
            if(!utils.isUndefined(filterObject.recu))
             {
                    
                       filterData["recu"] = filterObject.recu ;
                       
            }
        
     
        }
        return filterData;
},
}
module.exports.OperationsCourantes = OperationsCourantes;