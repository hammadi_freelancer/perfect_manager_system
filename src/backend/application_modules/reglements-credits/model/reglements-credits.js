//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var ReglementsCredits = {
addReglementCredit : function (db,reglementCredit,codeUser,callback){
    if(reglementCredit != undefined){
        reglementCredit.code  = utils.isUndefined(reglementCredit.code)?shortid.generate():reglementCredit.code;

        reglementCredit.dateReglementCreditObject = utils.isUndefined(reglementCredit.dateReglementCreditObject)? 
        new Date():reglementCredit.dateReglementCreditObject;
        if(utils.isUndefined(reglementCredit.numeroReglementCredit))
            {
               const currentD =  moment(new Date());
              // const genereKey = shortid.generate();
              const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.day().toString()+
              shortid.generate();
              reglementCredit.numeroReglementCredit = 'REG@CR' +generatedCode;
            }
       
            reglementCredit.userCreatorCode = codeUser;
            reglementCredit.userLastUpdatorCode = codeUser;
            reglementCredit.dateCreation = moment(new Date).format('L');
            reglementCredit.dateLastUpdate = moment(new Date).format('L');
            
            db.collection('ReglementCredit').insertOne(
                reglementCredit,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
             // db.close();
             
    

}else{
callback(true,null);
}
},
updateReglementCredit: function (db,reglementCredit,codeUser, callback){

   
    reglementCredit.dateLastUpdate = moment(new Date).format('L');
    
    reglementCredit.dateReglementCreditObject = utils.isUndefined(reglementCredit.dateReglementCreditObject)? 
    new Date():reglementCredit.dateReglementCreditObject;

 

    const criteria = { "code" : reglementCredit.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
    "dateReglementCreditObject" : reglementCredit.dateReglementCreditObject, 
     "client": reglementCredit.client,
     "valeurRegle": reglementCredit.valeurRegle,
     "periodeFromObject": reglementCredit.periodeFromObject,
     "periodeToObject": reglementCredit.periodeToObject,
     "mois": reglementCredit.mois,
     "modePaiement": reglementCredit.modePaiement,
     "etat": reglementCredit.etat,
     "description": reglementCredit.description,
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": reglementCredit.dateLastUpdate, 
    } ;
            db.collection('ReglementCredit').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },function(reglementCredit,err){
                    if(err)
                    callback(false,err);
                    else
                        callback(true,true);
                    
                    
                }
            ) ;
     
    
    },
   
getListReglementsCredits : function (db,codeUser,pageNumber, nbElementsPerPage , filter,callback)
{
    let listReglementsCredits = [];
   
  //console.log('filter recieved is :',filter);
  let filterObject = JSON.parse(filter);
 
   let filterData = {
       "userCreatorCode" : codeUser
   };
   const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
   
   var cursor =  db.collection('ReglementCredit').find( 
    conditionBuilt
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(reglementCredit,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        reglementCredit.dateReglementCredit= moment(reglementCredit.dateReglementCreditObject).format('L');
        reglementCredit.periodeFrom= moment(reglementCredit.periodeFromObject).format('L');
        reglementCredit.periodeTo= moment(reglementCredit.periodeToObject).format('L');
       listReglementsCredits.push(reglementCredit);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listReglementsCredits); 
   });
     //return [];
},
deleteReglementCredit: function (db,codeReglementCredit,codeUser,callback){
    const criteria = { "code" : codeReglementCredit, "userCreatorCode":codeUser };
    
    db.collection('ReglementCredit').deleteOne(
        criteria , function(err,result){
            if(err){
                
                callback(null,null);
            }else{
                callback(false,result);
            }
        }
    ) ;
},

getReglementCredit: function (db,codeReglementCredit,codeUser,callback)
{
    const criteria = { "code" : codeReglementCredit, "userCreatorCode":codeUser };
  
       const reglementCredit =  db.collection('ReglementCredit').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
        // callback(false,client);
                 
},
getTotalReglementsCredits : function (db,codeUser,filter, callback)
{
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
   var totalReglementsCredits =  db.collection('ReglementCredit').find( 
    conditionBuilt
).count(function(err,result){
        callback(null,result); 
    }
);
   


     //return [];
},
getPageReglementsCredits : function (db,codeUser,pageNumber, nbElementsPerPage ,filter, callback)
{
    console.log('filter ReglementCredits',filter);
    let listReglementsCredits = [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
      console.log('condition',conditionBuilt);
      
      
   var cursor =  db.collection('ReglementCredit').find( conditionBuilt
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(reglementCredit,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        reglementCredit.dateReglementCredit= moment(reglementCredit.dateReglementCreditObject).format('L');
        reglementCredit.periodeFrom= moment(reglementCredit.periodeFromObject).format('L');
        reglementCredit.periodeTo= moment(reglementCredit.periodeToObject).format('L');
        
       listReglementsCredits.push(reglementCredit);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listReglementsCredits); 
   });
     //return [];
},
buildFilterCondition(filterObject,filterData)
{
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.numeroReglementCredit))
        {
                filterData["numeroReglementCredit"] = filterObject.numeroReglementCredit;
        }
         
        if(!utils.isUndefined(filterObject.etat))
          {
                 
                    filterData["etat"] = filterObject.etat;
                    
         }
         if(!utils.isUndefined(filterObject.dateReglementCreditFromObject))
             {
                    
                       filterData["dateReglementCreditObject"] = {$gt: filterObject.dateReglementCreditFromObject};
                       
            }
            if(!utils.isUndefined(filterObject.dateReglementCreditToObject))
             {
                    
                       filterData["dateReglementCreditObject"] = {$lt: filterObject.dateReglementCreditToObject};
                       
            }
            if(!utils.isUndefined(filterObject.cinClient))
             {
                    
                       filterData["client.cin"] = filterObject.cinClient ;
                       
            }
            if(!utils.isUndefined(filterObject.nomClient))
             {
                    
                       filterData["client.nom"] = filterObject.nomClient ;
                       
            }
            if(!utils.isUndefined(filterObject.prenomClient))
             {
                    
                       filterData["client.prenom"] = filterObject.prenomClient ;
                       
            }
            if(!utils.isUndefined(filterObject.raisonClient))
             {
                    
                       filterData["client.raisonSociale"] = filterObject.raisonClient ;
                       
            }
            if(!utils.isUndefined(filterObject.matriculeClient))
             {
                    
                       filterData["client.matriculeFiscale"] = filterObject.matriculeClient ;
                       
            }
            if(!utils.isUndefined(filterObject.registreClient))
             {
                    
                       filterData["client.registreCommerce"] = filterObject.registreClient ;
                       
            }
        
            if(!utils.isUndefined(filterObject.valeurRegle))
             {
                    
                       filterData["valeurRegle"] = filterObject.valeurRegle ;
                       
            }
            if(!utils.isUndefined(filterObject.mois))
             {
                    
                       filterData["mois"] = filterObject.mois ;
                       
            }
            if(!utils.isUndefined(filterObject.modePaiement))
             {
                    
                       filterData["modePaiement"] = filterObject.modePaiement ;
                       
            }
     
        }
        return filterData;
},
}
module.exports.ReglementsCredits = ReglementsCredits;