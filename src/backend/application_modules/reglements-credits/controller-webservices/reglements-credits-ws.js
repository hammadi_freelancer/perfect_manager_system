
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var reglementsCredits = require('../model/reglements-credits').ReglementsCredits;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addReglementCredit',function(req,res,next){
    
    console.log(" ADD REGLEMENT CREDIT IS CALLED") ;
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = reglementsCredits.addReglementCredit(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,regAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_REGLEMENT_CREDIT',
                error_content: err
            });
         }else{
       
        return res.json(regAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateReglementCredit',function(req,res,next){
    
       console.log(" UPDATE REGLEMENT CREDIT IS CALLED") ;
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = reglementsCredit.updateReglementCredit(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,regUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_REGLEMENT_CREDIT',
                error_content: err
            });
         }else{
       
        return res.json(regUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getReglementsCredits',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
      console.log("GET LIST REGLEMENTS C IS CALLED");
    reglementsCredits.getListReglementsCredits(
        req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
        req.query.filter,  function(err,listRegs){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_REGLEMENTS_CREDITS',
                 error_content: err
             });
          }else{
        
         return res.json(listRegs);
          }
      });

  });
  router.get('/getPageReglementsCredits',function(req,res,next){
    
        var decoded = jwt.decode(req.headers.authorization.slice( 
            req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
      
          console.log("GET PAGE REGLEMENTS CREDITS IS CALLED");
        reglementsCredits.getPageReglementsCredits(
            req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
            req.query.filter, function(err,listOperations){
              if(err){
                 return  res.json({
                     error_message : 'ERROR_GET_PAGE_REGLEMENTS_CREDITS',
                     error_content: err
                 });
              }else{
            
             return res.json(listOperations);
              }
          });
    
      });
  router.get('/getReglementCredit/:codeReglementCredit',function(req,res,next){
    console.log("GET REGLEMENT CREDIT IS CALLED");
  //  console.log(req.params['codeCredit']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    reglementsCredits.getReglementCredit(
        req.app.locals.dataBase,req.params['codeReglementCredit'],
        decoded.userData.code, function(err,operationCourante){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_REGLEMENT_CREDIT',
               error_content: err
           });
        }else{
      
       return res.json(operationCourante);
        }
    });
})

  router.delete('/deleteReglementCredit/:codeReglementCredit',function(req,res,next){
    
       console.log(" DELETE REGLEMENT CREDIT IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = reglementsCredits.deleteReglementCredit(
        req.app.locals.dataBase,req.params['codeReglementCredit'],decoded.userData.code, function(err,codeReglementCredit){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_REGLEMENT_CREDIT',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeReglementCredit']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });



 router.get('/getTotalReglementsCredits',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    reglementsCredits.getTotalReglementsCredits(
        req.app.locals.dataBase,decoded.userData.code,req.query.filter,
        function(err,totalReglementsCredits){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_REGLEMENTS_CREDITS',
                 error_content: err
             });
          }else{
        
         return res.json({'totalReglementsCredits':totalReglementsCredits});
          }
      });

  });
  module.exports.routerReglementsCredits = router;