

var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var configurations = require('../model/configurations').Configurations;
var jwt = require('jsonwebtoken');

router.post('/addConfiguration',function(req,res,next){
    
       console.log(" ADD  CONFIGURATION  IS CALLED") ;
       console.log(" THE  CONFIGURATION  TO ADDED IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = configurations.addConfiguration(req.app.locals.dataBase,req.body,decoded.userData.code,function(err,configurationAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_CONFIGURATION',
                error_content: err
            });
         }else{
       
        return res.json(configurationAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.put('/updateConfiguration',function(req,res,next){
    
       console.log(" UPDATE   CONFIGURATION  IS CALLED") ;
       console.log(" THE  CONFIGURATION   TO UPDATE IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = configurations.updateConfiguration(req.app.locals.dataBase,
        req.body,decoded.userData.code,function(err,configurationUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_CONFIGURATION',
                error_content: err
            });
         }else{
       
        return res.json(configurationUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getConfigurations',function(req,res,next){
    console.log("GET LIST  CONFIGURATIONS  IS CALLED");
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    configurations.getListConfigurations(req.app.locals.dataBase,decoded.userData.code,function(err,listConfigurations){
       console.log("GET LIST  CONFIGURATIONS  : RESULT");
       console.log(listConfigurations);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_CONFIGURATIONS',
               error_content: err
           });
        }else{
      
       return res.json(listConfigurations);
        }
    });
})
router.get('/getConfiguration/:codeConfiguration',function(req,res,next){
    console.log("GET  CONFIGURATION  IS CALLED");
    console.log(req.params['codeConfiguration']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    configurations.getConfiguration(req.app.locals.dataBase,req.params['codeConfiguration'],decoded.userData.code,function(err,configuration){
       console.log("GET   CONFIGURATION  : RESULT");
       console.log(configuration);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_CONFIGURATION',
               error_content: err
           });
        }else{
      
       return res.json(configuration);
        }
    });
})

router.get('/getConfigurationsByCodeArticle/:codeArticle',function(req,res,next){
    
        var decoded = jwt.decode(req.headers.authorization.slice( 
            req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    configurations.getListConfigurationsByCodeArticle(
            req.app.locals.dataBase,decoded.userData.code,req.params['codeArticle'],
             function(err,listConfigurations){
           
              if(err){
                 return  res.json({
                     error_message : 'ERROR_GET_CONFIGURATIONS_BY_CODE_ARTICLE',
                     error_content: err
                 });
              }else{
            
             return res.json(listConfigurations);
              }
          });
    
      });
/*router.post('/getFiltredClients',function(req,res,next){
    console.log("GET FILTRED LIST CLIENTS IS CALLED");
    clients.getFiltredListClients(req.body,function(err,listFiltredClients){
       console.log("GET FILTRED LIST CLIENTS : RESULT");
       console.log(listFiltredClients);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_FILTRED_CLIENTS',
               error_content: err
           });
        }else{
      
       return res.json(listFiltredClients);
        }
    });
})*/
router.delete('/deleteConfiguration/:codeConfiguration',function(req,res,next){
    
       console.log(" DELETE  CONFIGURATION  IS CALLED") ;
       console.log(" THE  CONFIGURATION  TO DELETE IS :",req.params['codeConfiguration']);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
     
       var result = configurations.deleteConfiguration(req.app.locals.dataBase,req.params['codeConfiguration'],decoded.userData.code,
       
       function(err,codeConfiguration){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_CONFIGURATION',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeConfiguration']);
    }
       }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

module.exports.ConfigurationsRouter = router;