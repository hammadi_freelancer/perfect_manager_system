

var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var articles = require('../model/articles').Articles;
var jwt = require('jsonwebtoken');
var utils = require('../../utils/utils');

router.post('/addArticle',function(req,res,next){
    

       console.log(" ADD ARTICLE IS CALLED") ;
       console.log(" THE ARTICLE TO ADDED IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = articles.addArticle(req.app.locals.dataBase,req.body,decoded.userData.code,function(err,articleAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_ARTICLE',
                error_content: err
            });
         }else{
       
        return res.json(articleAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.put('/updateArticle',function(req,res,next){
    
       console.log(" UPDATE ARTICLE IS CALLED") ;
       console.log(" THE ARTICLE TO UPDATE IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = articles.updateArticle(req.app.locals.dataBase,req.body,decoded.userData.code,function(err,articleUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_ARTICLE',
                error_content: err
            });
         }else{
       
        return res.json(articleUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getArticles',function(req,res,next){
    console.log("GET LIST ARTICLES IS CALLED");
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    articles.getListArticles(req.app.locals.dataBase,decoded.userData.code,function(err,listArticles){
       console.log("GET LIST ARTICLES : RESULT");
       console.log(listArticles);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_ARTICLES',
               error_content: err
           });
        }else{
      
       return res.json(listArticles);
        }
    });
});
router.get('/getArticle/:codeArticle',function(req,res,next){
    let hasRight = true;
    console.log("GET ARTICLE IS CALLED");
    console.log(req.params['codeArticle']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
        console.log('user data is ');
        console.log(decoded.userData.accessRights );
        //if(utils.isdecoded.userData.accessRights !== undefined)
        if(!utils.isUndefined(decoded.userData.accessRights ))
            {
              let accessRightsArticle =  decoded.userData.accessRights.filter( (accessRight)=>
                (accessRight.entityName ==='Article') );

                if(!utils.isUndefined(accessRightsArticle ))
         {
            let accessRightsGet=  accessRightsArticle.filter(
                 (accessRightArt)=>
        (
            accessRightArt.rights.filter((righ)=>(right==='Modification')).length !== 0) 
        );
        if(accessRightsGet.length === 0){
            hasRight = false;

         }
            }
        }

        if(hasRight){
    articles.getArticle(req.app.locals.dataBase,req.params['codeArticle'],decoded.userData.code,function(err,article){
       console.log("GET  ARTICLE : RESULT");
       console.log(article);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_ARTICLE',
               error_content: err
           });
        }else{
      
       return res.json(article);
        }
    });
}else
{
    return  res.json({
        error_message : 'EDIT_ARTICLE_ACCESS_DENIED',
        error_content: 'ACCESS_DENIED'
    });
}
});
/*router.post('/getFiltredClients',function(req,res,next){
    console.log("GET FILTRED LIST CLIENTS IS CALLED");
    clients.getFiltredListClients(req.body,function(err,listFiltredClients){
       console.log("GET FILTRED LIST CLIENTS : RESULT");
       console.log(listFiltredClients);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_FILTRED_CLIENTS',
               error_content: err
           });
        }else{
      
       return res.json(listFiltredClients);
        }
    });
})*/
router.delete('/deleteArticle/:codeArticle',function(req,res,next){
    
       console.log(" DELETE ARTICLE IS CALLED") ;
       console.log(" THE ARTICLE TO DELETE IS :",req.params['codeArticle']);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = articles.deleteArticle(req.app.locals.dataBase,req.params['codeArticle'],decoded.userData.code,function(err,codeArticle){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_ARTICLE',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeArticle']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.get('/getPageArticles',function(req,res,next){
   //  console.log("GET LIST CLIENTS IS CALLED");
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
        console.log('user data is ', decoded.userData);
    articles.getPageArticles(req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
        req.query.filter,function(err,listArticles){
      //  console.log("GET LIST CLIENTS : RESULT");
      //  console.log(listArticles);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_PAGE_ARTICLES',
               error_content: err
           });
        }else{
      
       return res.json(listArticles);
        }
    });
});
router.get('/getTotalArticles',function(req,res,next){
   
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    articles.getTotalArticles(req.app.locals.dataBase,decoded.userData.code,
        req.query.filter, function(err,totalArticles){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_ARTICLES',
                 error_content: err
             });
          }else{
        
         return res.json({'totalArticles':totalArticles});
          }
      });

  });
module.exports.articlesRouter = router;