

var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var unMesures = require('../model/un-mesures').UnMesures;
var jwt = require('jsonwebtoken');

router.post('/addUnMesure',function(req,res,next){
    
       console.log(" ADD UNMESURE IS CALLED") ;
       console.log(" THE UNMESURE TO ADDED IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = unMesures.addUnMesure(req.app.locals.dataBase,req.body,decoded.userData.code,function(err,unMesureAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_UNMESURE',
                error_content: err
            });
         }else{
       
        return res.json(unMesureAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.put('/updateUnMesure',function(req,res,next){
    
       console.log(" UPDATE UN MESURE IS CALLED") ;
       console.log(" THE UN MESURE  TO UPDATE IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = unMesures.updateUnMesure(req.app.locals.dataBase,req.body,decoded.userData.code,function(err,unMesureUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_UNMESURE',
                error_content: err
            });
         }else{
       
        return res.json(unMesureUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getUnMesures',function(req,res,next){
    console.log("GET LIST UNMESURES IS CALLED");
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    unMesures.getListUnMesures(req.app.locals.dataBase,decoded.userData.code,function(err,listUnMesures){
       console.log("GET LIST UNMESURES : RESULT");
       console.log(listUnMesures);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_UNMESURES',
               error_content: err
           });
        }else{
      
       return res.json(listUnMesures);
        }
    });
})
router.get('/getUnMesure/:codeUnMesure',function(req,res,next){
    console.log("GET UN MESURE IS CALLED");
    console.log(req.params['codeUnMesure']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    unMesures.getUnMesure(req.app.locals.dataBase,req.params['codeUnMesure'],decoded.userData.code,
    function(err,unMesure){
       console.log("GET  UNMESURE : RESULT");
       console.log(unMesure);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_UNMESURE',
               error_content: err
           });
        }else{
      
       return res.json(unMesure);
        }
    });
})
/*router.post('/getFiltredClients',function(req,res,next){
    console.log("GET FILTRED LIST CLIENTS IS CALLED");
    clients.getFiltredListClients(req.body,function(err,listFiltredClients){
       console.log("GET FILTRED LIST CLIENTS : RESULT");
       console.log(listFiltredClients);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_FILTRED_CLIENTS',
               error_content: err
           });
        }else{
      
       return res.json(listFiltredClients);
        }
    });
})*/
router.delete('/deleteUnMesure/:codeUnMesure',function(req,res,next){
    
       console.log(" DELETE UN MESURE IS CALLED") ;
       console.log(" THE UN MESURE TO DELETE IS :",req.params['codeUnMesure']);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = unMesures.deleteUnMesure(req.app.locals.dataBase,req.params['codeUnMesure'],decoded.userData.code,
       function(err,codeUnMesure){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_UNMESURE',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeUnMesure']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

module.exports.unMesuresRouter = router;