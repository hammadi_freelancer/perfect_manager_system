

var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var schemasConfiguration = require('../model/schemas-configuration').SchemasConfiguration;
var jwt = require('jsonwebtoken');

router.post('/addSchemaConfiguration',function(req,res,next){
    
       console.log(" ADD SCHEMA CONFIGURATION  IS CALLED") ;
       console.log(" THE SCHEMA CONFIGURATION  TO ADDED IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = schemasConfiguration.addSchemaConfiguration(req.app.locals.dataBase,req.body,decoded.userData.code,function(err,schemaConfigurationAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_SCHEMACONFIGURATION',
                error_content: err
            });
         }else{
       
        return res.json(schemaConfigurationAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.put('/updateSchemaConfiguration',function(req,res,next){
    
       console.log(" UPDATE  SCHEMA CONFIGURATION  IS CALLED") ;
       console.log(" THE SCHEMA CONFIGURATION   TO UPDATE IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = schemasConfiguration.updateSchemaConfiguration(req.app.locals.dataBase,req.body,decoded.userData.code,function(err,schemaConfigurationUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_SCHEMACONFIGURATION',
                error_content: err
            });
         }else{
       
        return res.json(schemaConfigurationUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getSchemasConfiguration',function(req,res,next){
    console.log("GET LIST SCHEMAS CONFIGURATION  IS CALLED");
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    schemasConfiguration.getListSchemasConfiguration(req.app.locals.dataBase,decoded.userData.code,function(err,listSchemasConfiguration){
       console.log("GET LIST SCHEMAS CONFIGURATION  : RESULT");
       console.log(listSchemasConfiguration);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_SCHEMAS_CONFIGURATION',
               error_content: err
           });
        }else{
      
       return res.json(listSchemasConfiguration);
        }
    });
})
router.get('/getSchemaConfiguration/:codeSchemaConfiguration',function(req,res,next){
    console.log("GET SCHEMA CONFIGURATION  IS CALLED");
    console.log(req.params['codeSchemaConfiguration']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    schemasConfiguration.getSchemaConfiguration(req.app.locals.dataBase,req.params['codeSchemaConfiguration'],decoded.userData.code,function(err,schemaConfiguration){
       console.log("GET  SCHEMA CONFIGURATION  : RESULT");
       console.log(schemaConfiguration);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_SCHEMACONFIGURATION',
               error_content: err
           });
        }else{
      
       return res.json(schemaConfiguration);
        }
    });
})
/*router.post('/getFiltredClients',function(req,res,next){
    console.log("GET FILTRED LIST CLIENTS IS CALLED");
    clients.getFiltredListClients(req.body,function(err,listFiltredClients){
       console.log("GET FILTRED LIST CLIENTS : RESULT");
       console.log(listFiltredClients);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_FILTRED_CLIENTS',
               error_content: err
           });
        }else{
      
       return res.json(listFiltredClients);
        }
    });
})*/
router.delete('/deleteSchemaConfiguration/:codeSchemaConfiguration',function(req,res,next){
    
       console.log(" DELETE SCHEMA CONFIGURATION  IS CALLED") ;
       console.log(" THE SCHEMA CONFIGURATION  TO DELETE IS :",req.params['codeSchemaConfiguration']);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
     
       var result = schemasConfiguration.deleteSchemaConfiguration(req.app.locals.dataBase,req.params['codeSchemaConfiguration'],decoded.userData.code,
       
       function(err,codeSchemaConfiguration){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_SCHEMACONFIGURATION',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeSchemaConfiguration']);
    }
       }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

module.exports.schemasConfigurationRouter = router;