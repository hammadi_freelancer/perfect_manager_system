

var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var magasins = require('../model/magasins').Magasins;
var jwt = require('jsonwebtoken');

router.post('/addMagasin',function(req,res,next){
    
       console.log(" ADD MAGASIN IS CALLED") ;
       console.log(" THE MAGASIN TO ADDED IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = magasins.addMagasin(req.app.locals.dataBase,req.body,decoded.userData.code,function(err,magasinAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_MAGASIN',
                error_content: err
            });
         }else{
       
        return res.json(magasinAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.put('/updateMagasin',function(req,res,next){
    
       console.log(" UPDATE MAGASIN IS CALLED") ;
       console.log(" THE MAGASIN TO UPDATE IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = magasins.updateMagasin(req.app.locals.dataBase,req.body,decoded.userData.code,function(err,magasinUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_MAGASIN',
                error_content: err
            });
         }else{
       
        return res.json(magasinUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getMagasins',function(req,res,next){
    console.log("GET LIST MAGASINS IS CALLED");
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    magasins.getListMagasins(req.app.locals.dataBase,decoded.userData.code,function(err,listMagasins){
       console.log("GET LIST MAGASINS : RESULT");
       console.log(listMagasins);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_MAGASINS',
               error_content: err
           });
        }else{
      
       return res.json(listMagasins);
        }
    });
})
router.get('/getMagasin/:codeMagasin',function(req,res,next){
    console.log("GET MAGASIN IS CALLED");
    console.log(req.params['codeMagasin']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    magasins.getMagasin(req.app.locals.dataBase,req.params['codeMagasin'],decoded.userData.code,function(err,magasin){
       console.log("GET  MAGASIN : RESULT");
       console.log(magasin);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_MAGASIN',
               error_content: err
           });
        }else{
      
       return res.json(magasin);
        }
    });
})
/*router.post('/getFiltredClients',function(req,res,next){
    console.log("GET FILTRED LIST CLIENTS IS CALLED");
    clients.getFiltredListClients(req.body,function(err,listFiltredClients){
       console.log("GET FILTRED LIST CLIENTS : RESULT");
       console.log(listFiltredClients);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_FILTRED_CLIENTS',
               error_content: err
           });
        }else{
      
       return res.json(listFiltredClients);
        }
    });
})*/
router.delete('/deleteMagasin/:codeMagasin',function(req,res,next){
    
       console.log(" DELETE MAGASIN IS CALLED") ;
       console.log(" THE MAGASIN TO DELETE IS :",req.params['codeMagasin']);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = magasins.deleteMagasin(req.app.locals.dataBase,req.params['codeMagasin'],decoded.userData.code,function(err,codeMagasin){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_MAGASIN',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeMagasin']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

module.exports.magasinsRouter = router;