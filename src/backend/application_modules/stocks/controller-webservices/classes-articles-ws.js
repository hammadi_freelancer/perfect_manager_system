

var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var classesArticles = require('../model/classes-articles').ClassesArticles;
var jwt = require('jsonwebtoken');

router.post('/addClasseArticle',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = classesArticles.addClasseArticle(req.app.locals.dataBase,req.body,decoded.userData.code,function(err,classArtAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_CLASSE_ARTICLE',
                error_content: err
            });
         }else{
       
        return res.json(classArtAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.put('/updateClasseArticle',function(req,res,next){
    
       console.log(" UPDATE CLASSE ARTICLE IS CALLED") ;
       console.log(" THE CLASSE ARTICLE TO UPDATE IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = classesArticles.updateClasseArticle(req.app.locals.dataBase,req.body,decoded.userData.code,function(err,classArtUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_CLASSE_ARTICLE',
                error_content: err
            });
         }else{
       
        return res.json(classArtUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getClassesArticles',function(req,res,next){
    console.log("GET LIST CLASSES ARTICLES IS CALLED");
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    classesArticles.getListClassesArticles(req.app.locals.dataBase,
        decoded.userData.code,function(err,listClassesArticles){
       console.log("GET LIST ARTICLES : RESULT");
       console.log(listClassesArticles);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_CLASSES_ARTICLES',
               error_content: err
           });
        }else{
      
       return res.json(listClassesArticles);
        }
    });
})
router.get('/getClasseArticle/:codeClasseArticle',function(req,res,next){
   // console.log("GET ARTICLE IS CALLED");
   // console.log(req.params['codeArticle']);
   var decoded = jwt.decode(req.headers.authorization.slice( 
    req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    classesArticles.getClasseArticle(req.app.locals.dataBase,
        req.params['codeClasseArticle'],decoded.userData.code,function(err,classeArt){
       console.log("GET  CLASSE ARTICLE : RESULT");
       console.log(classeArt);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_CLASSE_ARTICLE',
               error_content: err
           });
        }else{
      
       return res.json(classeArt);
        }
    });
})
/*router.post('/getFiltredClients',function(req,res,next){
    console.log("GET FILTRED LIST CLIENTS IS CALLED");
    clients.getFiltredListClients(req.body,function(err,listFiltredClients){
       console.log("GET FILTRED LIST CLIENTS : RESULT");
       console.log(listFiltredClients);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_FILTRED_CLIENTS',
               error_content: err
           });
        }else{
      
       return res.json(listFiltredClients);
        }
    });
})*/
router.delete('/deleteClasseArticle/:codeClasseArticle',function(req,res,next){
    
       console.log(" DELETE CLASSE ARTICLE IS CALLED") ;
       console.log(" THE CLASSE ARTICLE TO DELETE IS :",req.params['codeClasseArticle']);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = classesArticles.deleteClasseArticle(req.app.locals.dataBase,req.params['codeClasseArticle'],decoded.userData.code,function(err,codeClasseArticle){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_CLASSE_ARTICLE',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeClasseArticle']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

module.exports.classesArticlesRouter = router;