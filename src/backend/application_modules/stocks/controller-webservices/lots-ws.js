

var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var lots = require('../model/lots').Lots;
var jwt = require('jsonwebtoken');

router.post('/addLot',function(req,res,next){
    
       
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = lots.addLot(req.app.locals.dataBase,req.body,decoded.userData.code,function(err,lotAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_LOT',
                error_content: err
            });
         }else{
       
        return res.json(lotAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.put('/updateLot',function(req,res,next){
    
       console.log(" UPDATE LOT IS CALLED") ;
        console.log(" THE LOT TO UPDATE IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = lots.updateLot(req.app.locals.dataBase,req.body,decoded.userData.code,function(err,lotUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_LOT',
                error_content: err
            });
         }else{
       
        return res.json(lotUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getLots',function(req,res,next){
    console.log("GET LIST LOTS IS CALLED");
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    lots.getListLots(req.app.locals.dataBase,decoded.userData.code,function(err,listLots){
       console.log("GET LIST LOTS : RESULT");
       console.log(listLots);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_LOTS',
               error_content: err
           });
        }else{
      
       return res.json(listLots);
        }
    });
})
router.get('/getLot/:codeLot',function(req,res,next){
    console.log("GET LOT IS CALLED");
    console.log(req.params['codeLot']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    lots.getLot(req.app.locals.dataBase,req.params['codeLot'],decoded.userData.code,function(err,lot){
       console.log("GET  LOT : RESULT");
       console.log(lot);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LOT',
               error_content: err
           });
        }else{
      
       return res.json(lot);
        }
    });
})
/*router.post('/getFiltredClients',function(req,res,next){
    console.log("GET FILTRED LIST CLIENTS IS CALLED");
    clients.getFiltredListClients(req.body,function(err,listFiltredClients){
       console.log("GET FILTRED LIST CLIENTS : RESULT");
       console.log(listFiltredClients);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_FILTRED_CLIENTS',
               error_content: err
           });
        }else{
      
       return res.json(listFiltredClients);
        }
    });
})*/
router.delete('/deleteLot/:codeLot',function(req,res,next){
    
       console.log(" DELETE LOT IS CALLED") ;
       console.log(" THE LOT TO DELETE IS :",req.params['codeLot']);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = lots.deleteLot(req.app.locals.dataBase,req.params['codeLot'],
       decoded.userData.code,function(err,codeLot){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_LOT',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeLot']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

module.exports.lotsRouter = router;