//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');
//we need to have article as a json object : {code :'mmm', design :'ddd'}
var  Configurations = {
addConfiguration : function (db,configuration,codeUser, callback){
    if(schemaConfiguration != undefined){

        configuration.code  = utils.isUndefined(configuration.code)?'CFG'+shortid.generate():schemaConfiguration.code;
        configuration.userCreatorCode = codeUser;
        configuration.userLastUpdatorCode = codeUser;
        configuration.dateCreation = moment(new Date).format('L');
        configuration.dateLastUpdate = moment(new Date).format('L');

   
        db.collection('Configuration').insertOne(
            configuration,
            function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }  
          ) ;


       //return true;  
}
//return false;
},
updateConfiguration: function (db,configuration,codeUser, callback){
    const criteria = { "code" : configuration.code, "userCreatorCode":codeUser };
    configuration.userLastUpdatorCode = codeUser;
    configuration.dateLastUpdate = moment(new Date).format('L');
    configuration.schemaConfiguration = schemaConfiguration;
    configuration.imageFace1 = imageFace1;
    configuration.imageFace2 = imageFace2;
    configuration.description = description;
    
  
     
            db.collection('Configuration').updateOne(
                criteria,
                { 
                    $set: configuration
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
         
   
    
    },

    getListConfigurationsByCodeArticle : function (db,codeUser,codeArticle,callback)
    {
        let listConfigurations = [];
       
       var cursor =  db.collection('Configuration').find( 
           {"userCreatorCode" : codeUser, "codeArticle": codeArticle}
        );
       cursor.forEach(function(configuration,err){
           if(err)
            {
                console.log('errors produced :', err);
            }
           // configuration.dateOperation = moment(configuration.dateOperationObject).format('L');
            // configuration.nouvelleDatePayement = moment(configuration.nouvelleDatePayementObject).format('L');
            
            listConfigurations.push(configuration);
       }).then(function(){
       // console.log('list credits',listCredits);
        callback(null,listConfigurations); 
       });
       
  
         //return [];
    },
getListConfigurations : function (db,codeUser,callback)
{
    let listConfigurations = [];
    
  
   var cursor =  db.collection('Configuration').find(
    { "userCreatorCode":codeUser }
   );
   cursor.forEach(function(configuration,err){
       if(err)
        {
            console.log('errors produced :', err);

        }
      
        listConfigurations.push(configuration);
   }).then(function(){
    //console.log('list schemas configuration',listSchemasConfiguration);
    callback(null,listConfigurations); 
   });
},
deleteConfiguration: function (db,codeConfiguration,codeUser,callback){
    const criteria = { "code" : codeConfiguration, "userCreatorCode":codeUser };
    
     
            db.collection('Configuration').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
           
        
},
getConfiguration: function (db,codeConfiguration,codeUser,callback)
{
    const criteria = { "code" : codeConfiguration, "userCreatorCode":codeUser };
    
  
       const configuration =  db.collection('Configuration').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                 
},
};

module.exports.Configurations = Configurations;