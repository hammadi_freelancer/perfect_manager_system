//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');
//we need to have article as a json object : {code :'mmm', design :'ddd'}
var Articles = {
addArticle : function (db,article, codeUser,callback){
    if(article != undefined){

    article.code  = utils.isUndefined(article.code)?shortid.generate():article.code;

    article.userCreatorCode = codeUser;
    article.userLastUpdatorCode = codeUser;
    article.dateCreation = moment(new Date).format('L');
    article.dateLastUpdate = moment(new Date).format('L');

    article.dateExpiration = utils.isUndefined(article.dateExpiration)? 
    article.dateExpiration : moment(article.dateExpiration ).format('L');
 
        db.collection('Article').insertOne(
            article,
            function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            } 
          ) ;
         // db.close();
       //  callback(false,article);
    }

//return false;
},
updateArticle: function (db,article,codeUser, callback){
    
    const criteria = { "code" : article.code,"userCreatorCode" : codeUser };
    
    article.dateExpiration = utils.isUndefined(article.dateExpiration)? 
    article.dateExpiration : moment(article.dateExpiration ).format('L');
    console.log("THE EXPIRATION DATE IS ");
    console.log(article.dateExpiration);
    const dataToUpdate = {
        "libelle" : article.libelle,
        "designation" : article.designation,
        "barCode" : article.barCode,
        "dateExpirationObject" : article.dateExpirationObject,
        "dateFabricationObject" : article.dateFabricationObject,
        "dateAchatsObject" : article.dateAchatsObject,
        "dateAchatsObject" : article.dateAchatsObject,
        "dateVentesObject" : article.dateVentesObject,
        "dateVentesObject" : article.dateVentesObject,
        "dataStock": article.dataStock,
        "dataAchats": article.dataAchats,
        "dataVentes": article.dataVentes,
        "marque" : article.marque,
        "disponibilite" : article.disponibilite,
        "vivialite" : article.vivialite,
        "legetimite" : article.legetimite,
        "conditionsStockage" : article.conditionsStockage,
        "schemaConfiguration" : article.schemaConfiguration,
        "classe" : article.classe,
        "magasins" : article.magasins,
        "sousClasse" : article.sousClasse,
        "lots" : article.lots,
        "unMesures" : article.unMesures,
        "imageFace1" : article.imageFace1,
        "imageFace2" : article.imageFace2,
        "pourcentageTVAVentes" : article.pourcentageTVAVentes,
        "pourcentageTVAAchats" : article.pourcentageTVAAchats,
        "pourcentageBenefice" : article.pourcentageBenefice,
        "pourcentageBeneficeEstime" : article.pourcentageBeneficeEstime,
        "prixVentes" : article.prixVentes
        
} ;
     
            db.collection('Article').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }    
            ) ;
     
    
    },
    getListArticles : function (db,codeUser, callback)
    {
        let listArticles = [];
      
       var cursor =  db.collection('Article').find(
        {"userCreatorCode" : codeUser}
       );
       cursor.forEach(function(article,err){
           if(err)
            {
                console.log('errors produced :', err);
    
            }
            article.dateAchats = moment(article.dateAchatsObject).format('L');
            article.dateVentes = moment(article.dateVentesObject).format('L');
            article.dateExpiration = moment(article.dateExpirationObject).format('L');
            article.dateFabrication = moment(article.dateFabricationObject).format('L');
            if(article.dataStock === undefined)
                {
                    article.dataStock = {};
                }
                if(article.dataAchats === undefined)
                    {
                        article.dataAchats = {};
                    }
                    if(article.dataVentes === undefined)
                        {
                            article.dataVentes = {};
                        }
            listArticles.push(article);
       }).then(function(){
       //  console.log('list clients',listClients);
        callback(null,listArticles); 
       });
       
   
         //return [];
    },

deleteArticle: function (db,codeArticle,codeUser,callback){
    const criteria = { "code" : codeArticle, "userCreatorCode":codeUser };
    
      
            db.collection('Article').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                } 
            ) ;
    
},
getPageArticles : function (db,codeUser,pageNumber,nbElementsPerPage, filter,callback)
{
    let listArticles = [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var cursor =  db.collection('Article').find(
      condition
   ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
   .limit(Number(nbElementsPerPage) );
   
   cursor.forEach(function(article,err){
       if(err)
        {
            console.log('errors produced :', err);

        }
        article.dateAchats = moment(article.dateAchatsObject).format('L');
        article.dateVentes = moment(article.dateVentesObject).format('L');
        article.dateExpiration = moment(article.dateExpirationObject).format('L');
        article.dateFabrication = moment(article.dateFabricationObject).format('L');
        if(article.dataStock === undefined)
            {
                article.dataStock = {};
            }
            if(article.dataAchats === undefined)
                {
                    article.dataAchats = {};
                }
                if(article.dataVentes === undefined)
                    {
                        article.dataVentes = {};
                    }
        listArticles.push(article);
   }).then(function(){
   //  console.log('list clients',listClients);
    callback(null,listArticles); 
   });
   


},
getTotalArticles: function (db,codeUser,filter, callback)
{
   
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var totalArticles=  db.collection('Article').find( 
     condition
    ).count(function(err,result){
        callback(null,result); 
    });

   

},


getArticle: function (db,codeArticle,codeUser,callback)
{
   // const criteria = { "code" : codeArticle };
    const criteria = { "code" : codeArticle, "userCreatorCode":codeUser };
    
 
       const article =  db.collection('Article').findOne(
            criteria , function(err,result){
                if(err){
                    callback(null,null);
                }else{
                    callback(false,result);
                }
            }
        ) ;
                   
},

buildFilterCondition(filterObject,filterData)
{
  
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.code))
        {
                filterData["code"] = filterObject.code;
        }
         
        if(!utils.isUndefined(filterObject.libelle))
          {
                 
                    filterData["libelle"] = filterObject.libelle;
                    
         }
         if(!utils.isUndefined(filterObject.designation))
             {
                    
                       filterData["designation"] =filterObject.designation;
                       
            }
            if(!utils.isUndefined(filterObject.barCode))
             {
                    
                       filterData["barCode"] = filterObject.barCode;
                       
            }
            if(!utils.isUndefined(filterObject.marque))
             {
                    
                       filterData["marque"] = filterObject.marque ;
                       
            }
            if(!utils.isUndefined(filterObject.codeClasse))
             {
                    
                       filterData["classe.code"] = filterObject.codeClasse ;
                       
            }
            if(!utils.isUndefined(filterObject.pourcentageBenefice))
             {
                    
                       filterData["pourcentageBenefice"] = filterObject.pourcentageBenefice ;
                       
            }
       
         
                if(!utils.isUndefined(filterObject.prixVentes))
                    {
                           
                              filterData["prixVentes"] = filterObject.prixVentes ;
                              
                   }

                   if(!utils.isUndefined(filterObject.etat))
                    {
                           
                              filterData["etat"] = filterObject.etat ;
                              
                   }



        }
        return filterData;
},

};

module.exports.Articles = Articles;