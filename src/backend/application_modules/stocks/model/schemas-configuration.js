//var db = require('../common/MongoDbConnection').getDBConnection;
const mongoClient = require('mongodb').MongoClient;
const url ='mongodb://localhost:27017';
const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');
//we need to have article as a json object : {code :'mmm', design :'ddd'}
var  SchemasConfiguration = {
addSchemaConfiguration : function (db,schemaConfiguration,codeUser, callback){
    if(schemaConfiguration != undefined){

        schemaConfiguration.code  = utils.isUndefined(schemaConfiguration.code)?'SCHCFG'+shortid.generate():schemaConfiguration.code;
        schemaConfiguration.userCreatorCode = codeUser;
        schemaConfiguration.userLastUpdatorCode = codeUser;
        schemaConfiguration.dateCreation = moment(new Date).format('L');
        schemaConfiguration.dateLastUpdate = moment(new Date).format('L');

   
        db.collection('SchemaConfiguration').insertOne(
            schemaConfiguration,
            function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }  
          ) ;


       //return true;  
}
//return false;
},
updateSchemaConfiguration: function (db,schemaConfiguration,codeUser, callback){
    const criteria = { "code" : schemaConfiguration.code, "userCreatorCode":codeUser };
    schemaConfiguration.userLastUpdatorCode = codeUser;
    schemaConfiguration.dateLastUpdate = moment(new Date).format('L');
    const dataToUpdate = { 
        "libelle" : schemaConfiguration.libelle, 
        "description" : schemaConfiguration.description, 
        "attribute1Name" : schemaConfiguration.attribute1Name, 
        "attribute2Name" : schemaConfiguration.attribute2Name, 
        "attribute3Name" : schemaConfiguration.attribute3Name, 
        "attribute4Name" : schemaConfiguration.attribute4Name, 
        "attribute1Type" : schemaConfiguration.attribute1Type, 
        "attribute2Type" : schemaConfiguration.attribute2Type, 
        "attribute3Type" : schemaConfiguration.attribute3Type, 
        "attribute4Type" : schemaConfiguration.attribute4Type, 
        
    "configAttributes" : schemaConfiguration.configAttributes,
    "userLastUpdatorCode": codeUser,
    "dateLastUpdate": schemaConfiguration.dateLastUpdate, 

     
} ;
     
            db.collection('SchemaConfiguration').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
         
   
    
    },
getListSchemasConfiguration : function (db,codeUser,callback)
{
    let listSchemasConfiguration = [];
    
  
   var cursor =  db.collection('SchemaConfiguration').find(
    { "userCreatorCode":codeUser }
   );
   cursor.forEach(function(schemaConfiguration,err){
       if(err)
        {
            console.log('errors produced :', err);

        }
      
        listSchemasConfiguration.push(schemaConfiguration);
   }).then(function(){
    console.log('list schemas configuration',listSchemasConfiguration);
    callback(null,listSchemasConfiguration); 
   });
},
deleteSchemaConfiguration: function (db,codeSchemaConfiguration,codeUser,callback){
    const criteria = { "code" : codeSchemaConfiguration, "userCreatorCode":codeUser };
    
     
            db.collection('SchemaConfiguration').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
           
        
},
getSchemaConfiguration: function (db,codeSchemaConfiguration,codeUser,callback)
{
    const criteria = { "code" : codeSchemaConfiguration, "userCreatorCode":codeUser };
    
  
       const schemaConfiguration =  db.collection('SchemaConfiguration').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                 
},
};

module.exports.SchemasConfiguration = SchemasConfiguration;