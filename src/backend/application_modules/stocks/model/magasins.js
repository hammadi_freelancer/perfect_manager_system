//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');
//we need to have article as a json object : {code :'mmm', design :'ddd'}
var Magasins = {
addMagasin : function (db,magasin,codeUser, callback){
    if(magasin != undefined){

        magasin.code  = utils.isUndefined(magasin.code)?'MAG'+shortid.generate():magasin.code;
  
        magasin.userCreatorCode = codeUser;
        magasin.userLastUpdatorCode = codeUser;
        magasin.dateCreation = moment(new Date).format('L');
        magasin.dateLastUpdate = moment(new Date).format('L');
 
        db.collection('Magasin').insertOne(
            magasin,
            function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }  
          ) ;
         // db.close();
        //  clientDB.close();
       //   callback(false,magasin);


       //return true;  
}
//return false;
},
updateMagasin: function (db,magasin,codeUser, callback){
  

    magasin.userLastUpdatorCode = codeUser;
    magasin.dateLastUpdate = moment(new Date).format('L');
    const criteria = { "code" : magasin.code,"userCreatorCode" : codeUser };

    const dataToUpdate = { 
    "libelle" : magasin.libelle, 
    "description" : magasin.description, 
    "adresse" : magasin.adresse, 
    "contact" : magasin.contact,
    "userLastUpdatorCode": codeUser,
    "dateLastUpdate": magasin.dateLastUpdate, 
} ;
        
            db.collection('Magasin').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
          
    
    
    },
getListMagasins : function (db,codeUser,callback)
{
    let listMagasins = [];
 
   var cursor =  db.collection('Magasin').find(
    {"userCreatorCode" : codeUser}
   );
   cursor.forEach(function(magasin,err){
       if(err)
        {
            console.log('errors produced :', err);

        }
      
        listMagasins.push(magasin);
   }).then(function(){
    console.log('list magasins',listMagasins);
    callback(null,listMagasins); 
   });
   
     //return [];
},
deleteMagasin: function (db,codeMagasin,codeUser,callback){
    const criteria = { "code" : codeMagasin, "userCreatorCode":codeUser };
    
      
            db.collection('Magasin').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
          
    
},
getMagasin: function (db,codeMagasin,codeUser,callback)
{
    const criteria = { "code" : codeMagasin, "userCreatorCode":codeUser };
 
       const magasin =  db.collection('Magasin').findOne(
            criteria ,  function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }  
        ) ;
                   
},

};

module.exports.Magasins = Magasins;