//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');
//we need to have article as a json object : {code :'mmm', design :'ddd'}
var Lots = {
addLot: function (db,lot,codeUser, callback){
    if(lot != undefined){

        lot.code  = utils.isUndefined(lot.code)?'LOT'+shortid.generate():lot.code;
  
        lot.userCreatorCode = codeUser;
        lot.userLastUpdatorCode = codeUser;
        lot.dateCreation = moment(new Date).format('L');
        lot.dateLastUpdate = moment(new Date).format('L');
   
        db.collection('Lot').insertOne(
            lot,
            function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }  
          ) ;
       //return true;  
}
//return false;
},
updateLot: function (db,lot,codeUser, callback){
  

    lot.userLastUpdatorCode = codeUser;
    lot.dateLastUpdate = moment(new Date).format('L');
    const criteria = { "code" : lot.code,"userCreatorCode" : codeUser };

    const dataToUpdate = { 
    "libelle" : lot.libelle, 
    "description" : lot.description, 
    "userLastUpdatorCode": codeUser,
    "dateLastUpdate": lot.dateLastUpdate, 
} ;
       
            db.collection('Lot').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
           

    
    },
getListLots : function (db,codeUser,callback)
{
    let listLots = [];
  
   var cursor =  db.collection('Lot').find(
    {"userCreatorCode" : codeUser}
   );
   cursor.forEach(function(lot,err){
       if(err)
        {
            console.log('errors produced :', err);

        }
      
        listLots.push(lot);
   }).then(function(){
  //   console.log('list magasins',listMagasins);
    callback(null,listLots); 
   });
   

     //return [];
},
deleteLot: function (db,codeLot,codeUser,callback){
    const criteria = { "code" : codeLot, "userCreatorCode":codeUser };
    
     
            db.collection('Lot').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
},
getLot: function (db,codeLot,codeUser,callback)
{
    const criteria = { "code" : codeLot, "userCreatorCode":codeUser };
    
  
       const lot =  db.collection('Lot').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                  
},

};

module.exports.Lots = Lots;