//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var moment = require('moment');
var shortid = require('shortid');
var async = require('async');
var util = require('util');
//we need to have article as a json object : {code :'mmm', design :'ddd'}
var ClassesArticles = {
addClasseArticle : function (db,classeArticle,codeUser, callback){
    if(classeArticle != undefined){
        classeArticle.code  = classeArticle.code===undefined?'CLART'+shortid.generate():classeArticle.code;

        classeArticle.userCreatorCode = codeUser;
        classeArticle.userLastUpdatorCode = codeUser;
        classeArticle.dateCreation = moment(new Date).format('L');
        classeArticle.dateLastUpdate = moment(new Date).format('L');

        db.collection('ClasseArticle').insertOne(
            classeArticle,
            function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            } 
          ) ;
   
    }

       //return true;  

},
updateClasseArticle: function (db,classeArticle,codeUser, callback){
    classeArticle.userLastUpdatorCode = codeUser;
    classeArticle.dateLastUpdate = moment(new Date).format('L');
    const criteria = { "code" : classeArticle.code,"userCreatorCode" : codeUser };
    const dataToUpdate = 
    {"libelle" : classeArticle.libelle,
    "description":classeArticle.description, 
    "image": classeArticle.image,
    "userLastUpdatorCode": codeUser,
    "dateLastUpdate": classeArticle.dateLastUpdate, 
} ;
       
            db.collection('ClasseArticle').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                } 
            ) ;

    
    },
getPageClassesArticles : function (db,codeUser,pageNumber,nbElementsPerPage, callback)
    {
        let listClassesArticles = [];
    
       var cursor =  db.collection('ClasseArticle').find(
        {"userCreatorCode" : codeUser}
       ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
       .limit(Number(nbElementsPerPage) );
       cursor.forEach(function(classeArticle,err){
           if(err)
            {
                console.log('errors produced :', err);
    
            }
           
                listClassesArticles.push(classeArticle);
       }).then(function(){
        callback(null,listClassesArticles); 
       });
       
    },

getListClassesArticles : function (db,codeUser, callback)
{
    let listClassesArticles = [];
     
   var cursor =  db.collection('ClasseArticle').find(
    {"userCreatorCode" : codeUser}
   );
   cursor.forEach(function(classeArticle,err){
       if(err)
        {
            console.log('errors produced :', err);

        }
     
        listClassesArticles.push(classeArticle);
   }).then(function(){
   //  console.log('list clients',listClients);
    callback(null,listClassesArticles); 
   });
     //return [];
},
deleteClasseArticle: function (db,codeClasseArticle,codeUser, callback){
    const criteria = { "code" : codeClasseArticle, "userCreatorCode":codeUser };
    
            db.collection('ClasseArticle').deleteOne(
                criteria,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
         
    
},
getClasseArticle: function (db,codeClasseArticle,codeUser, callback)
{
    const criteria = { "code" : codeClasseArticle, "userCreatorCode":codeUser };
    
   
       const classeArt =  db.collection('ClasseArticle').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
        // callback(false,client);
                    
},
getTotalClassesArticles: function (db,codeUser, callback)
{
   
   var totalClassesArticles=  db.collection('ClasseArticle').find( 
       {"userCreatorCode" : codeUser}
    ).count(function(err,result){
        callback(null,result); 
    }
);    //return [];
}

};

module.exports.ClassesArticles = ClassesArticles;