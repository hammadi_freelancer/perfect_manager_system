//var db = require('../common/MongoDbConnection').getDBConnection;
const mongoClient = require('mongodb').MongoClient;
const url ='mongodb://localhost:27017';
const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');
//we need to have article as a json object : {code :'mmm', design :'ddd'}
var  UnMesures = {
addUnMesure : function (db,unMesure,codeUser, callback){
    if(unMesure != undefined){

        unMesure.code  = utils.isUndefined(unMesure.code)?'UNM'+shortid.generate():unMesure.code;
  
        unMesure.userCreatorCode = codeUser;
        unMesure.userLastUpdatorCode = codeUser;
        unMesure.dateCreation = moment(new Date).format('L');
        unMesure.dateLastUpdate = moment(new Date).format('L');

 
        db.collection('UnMesure').insertOne(
            unMesure,
            function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }  
          ) ;
         
       //return true;  
}
//return false;
},
updateUnMesure: function (db,unMesure,codeUser, callback){
    // const criteria = { "code" : unMesure.code };
    const criteria = { "code" : unMesure.code, "userCreatorCode":codeUser };
    unMesure.userLastUpdatorCode = codeUser;
    unMesure.dateLastUpdate = moment(new Date).format('L');

    const dataToUpdate = { 
        "description" : unMesure.description, 
        "libelle" : unMesure.libelle, 
        
    "unitePrincipale" : unMesure.unitePrincipale, 
    "uniteSecondaire" : unMesure.uniteSecondaire,
    "userLastUpdatorCode" : unMesure.userLastUpdatorCode,
    "dateLastUpdate" : unMesure.dateLastUpdate,
     
} ;
            db.collection('UnMesure').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
  
    },
getListUnMesures : function (db,codeUser,callback)
{
    let listUnMesures = [];
  
   var cursor =  db.collection('UnMesure').find(
    { "userCreatorCode":codeUser }
   );
   cursor.forEach(function(unMesure,err){
       if(err)
        {
            console.log('errors produced :', err);

        }
      
        listUnMesures.push(unMesure);
   }).then(function(){
    // console.log('list un mesures',listUnMesures);
    callback(null,listUnMesures); 
   });
   
},
deleteUnMesure: function (db,codeUnMesure,codeUser,callback){
    const criteria = { "code" : codeUnMesure, "userCreatorCode":codeUser };
    
       
            db.collection('UnMesure').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
    
},
getUnMesure: function (db,codeUnMesure,codeUser,callback)
{
    // const criteria = { "code" : codeUnMesure };
    const criteria = { "code" : codeUnMesure,
     "userCreatorCode":codeUser };
    

       const unMesure =  db.collection('UnMesure').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;                   
},
};

module.exports.UnMesures = UnMesures;