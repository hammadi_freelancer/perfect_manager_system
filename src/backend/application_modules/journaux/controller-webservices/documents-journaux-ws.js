
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var documentsJournaux = require('../model/documents-journaux').DocumentsJournaux;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addDocumentJournal',function(req,res,next){
    
       console.log(" ADD DOCUMENT CREDIT  IS CALLED") ;
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = documentsJournaux.addDocumentJournal(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,documentJournalAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_DOCUMENT_JOURNAL',
                error_content: err
            });
         }else{
       
        return res.json(documentJournalAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateDocumentJournal',function(req,res,next){
    
      // console.log(" UPDATE DOCUMENT CREDIT  IS CALLED") ;
       console.log(" THE DOCUMENT JOURNAL  TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result =documentsJournaux.updateDocumentJournal(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,documentJournalUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_DOCUMENT_JOURNAL',
                error_content: err
            });
         }else{
       
        return res.json(documentJournalUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getDocumentsJournaux',function(req,res,next){
 
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    documentsJournaux.getListDocumentsJournaux(
        req.app.locals.dataBase,decoded.userData.code, function(err,listDocumentsJournaux){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_JOURNAUX',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsJournaux);
          }
      });

  });
  router.get('/getDocumentJournal/:codeDocumentJournal',function(req,res,next){
    console.log("GET DOCUMENT JOURNAL  IS CALLED");
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    documentsJournaux.getDocumentJournal(
        req.app.locals.dataBase,req.params['codeDocumentJournal'],decoded.userData.code,
     function(err,documentJournal){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_DOCUMENT_JOURNAL',
               error_content: err
           });
        }else{
      
       return res.json(documentJournal);
        }
    });
})

  router.delete('/deleteDocumentJournal/:codeDocumentJournal',function(req,res,next){
    
       console.log(" DELETE DOCUMENT JOURNAL  IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = documentsJournaux.deleteDocumentJournal(
        req.app.locals.dataBase,req.params['codeDocumentJournal'],decoded.userData.code,
        function(err,codeDocumentJournal){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_DOCUMENT_JOURNAL',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeDocumentJournal']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getDocumentsJournauxByCodeJournal/:codeJournal',function(req,res,next){
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
    documentsJournaux.getListDocumentsJournauxByCodeJournal(
        req.app.locals.dataBase,decoded.userData.code,req.params['codeJournal'],
         function(err,listDocumentJournaux){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_JOURNAUX_BY_CODE_JOURNAL',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentJournaux);
          }
      });

  });
  module.exports.routerDocumentsJournaux = router;