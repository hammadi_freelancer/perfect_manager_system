
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var lignesJournaux = require('../model/lignes-journaux').LignesJournaux;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addLigneJournal',function(req,res,next){
    
       console.log(" ADD LIGNE JOURNAL IS CALLED") ;
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = lignesJournaux.addLigneJournal(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,ligneJournalAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_LIGNE_JOURNAL',
                error_content: err
            });
         }else{
       
        return res.json(ligneJournalAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateLigneJournal',function(req,res,next){
    
      // console.log(" UPDATE DOCUMENT CREDIT  IS CALLED") ;
       console.log(" THE LIGNE JOURNAL  TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result =lignesJournaux.updateLigneJournal(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,ligneJournalUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_LIGNE_JOURNAL',
                error_content: err
            });
         }else{
       
        return res.json(ligneJournalUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getLignesJournaux',function(req,res,next){
 
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
    lignesJournaux.getListLignesJournaux(decoded.userData.code, function(err,listLignesJournaux){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_LIGNES_JOURNAUX',
                 error_content: err
             });
          }else{
        
         return res.json(listLignesJournaux);
          }
      });

  });
  router.get('/getLigneJournal/:codeLigneJournal',function(req,res,next){
    console.log("GET LIGNE JOURNAL  IS CALLED");
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    lignesJournaux.getLigneJournal(req.params['codeLigneJournal'],decoded.userData.code,
     function(err,ligneJournal){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIGNE_JOURNAL',
               error_content: err
           });
        }else{
      
       return res.json(ligneJournal);
        }
    });
})

  router.delete('/deleteLigneJournal/:codeLigneJournal',function(req,res,next){
    
       console.log(" DELETE LIGNE JOURNAL  IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = lignesJournaux.deleteLigneJournal(req.params['codeLigneJournal'],decoded.userData.code,
        function(err,codeLigneJournal){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_LIGNE_JOURNAL',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeLigneJournal']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getLignesJournauxByCodeJournal/:codeJournal',function(req,res,next){
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
    lignesJournaux.getListLignesJournauxByCodeJournal(decoded.userData.code,req.params['codeJournal'],
         function(err,listLignesJournaux){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_LIGNES_JOURNAUX_BY_CODE_JOURNAL',
                 error_content: err
             });
          }else{
        
         return res.json(listLignesJournaux);
          }
      });

  });
  module.exports.routerLignesJournaux = router;