
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var journaux = require('../model/journaux').Journaux;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addJournal',function(req,res,next){
    
    console.log(" ADD JOURNAL IS CALLED") ;
    
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = journaux.addJournal(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,journalAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_JOURNAL',
                error_content: err
            });
         }else{
       
        return res.json(journalAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateJournal',function(req,res,next){
    
       console.log(" UPDATE JOURNAL IS CALLED") ;
     
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = journaux.updateJournal(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,journalUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_JOURNAL',
                error_content: err
            });
         }else{
       
        return res.json(journalUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getJournaux',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
      console.log("GET LIST JOURNAUX IS CALLED");
    journaux.getListJournaux(
        req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
        req.query.filter,function(err,listJournaux){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_JOURNAUX',
                 error_content: err
             });
          }else{
        
         return res.json(listJournaux);
          }
      });

  });
  router.get('/getJournal/:codeJournal',function(req,res,next){
    console.log("GET JOURNAL IS CALLED");
  //  console.log(req.params['codeCredit']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    journaux.getJournal(
        req.app.locals.dataBase,req.params['codeJournal'],decoded.userData.code, function(err,journal){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_JOURNAL',
               error_content: err
           });
        }else{
      
       return res.json(journal);
        }
    });
})

  router.delete('/deleteJournal/:codeJournal',function(req,res,next){
    
       console.log(" DELETE JOURNAL IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = journaux.deleteJournal(
        req.app.locals.dataBase,req.params['codeJournal'],decoded.userData.code, function(err,codeJournal){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_JOURNAL',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeJournal']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });



 router.get('/getTotalJournaux',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    journaux.getTotalJournaux(
        req.app.locals.dataBase,decoded.userData.code,function(err,totalJournaux){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_JOURNAUX',
                 error_content: err
             });
          }else{
        
         return res.json({'totalJournaux':totalJournaux});
          }
      });

  });
  module.exports.routerJournaux = router;