//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var Journaux = {
addJournal : function (db,journal,codeUser,callback){
    if(journal != undefined){
        journal.code  = utils.isUndefined(journal.code)?shortid.generate():journal.code;

        journal.dateJournalObject = utils.isUndefined(journal.dateJournalObject)? 
        new Date():journal.dateJournalObject;
        if(utils.isUndefined(journal.numeroJournal))
            {
               const currentD =  moment(new Date());
              // const genereKey = shortid.generate();
              const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.minutes().toString()+
              currentD.secondes().toString();
              journal.numeroJournal = 'JOURN' +currentD.day().toLocaleString()+ generatedCode;
            }
       
            journal.userCreatorCode = codeUser;
            journal.userLastUpdatorCode = codeUser;
            journal.dateCreation = moment(new Date).format('L');
            journal.dateLastUpdate = moment(new Date).format('L');
        
           
            db.collection('Journal').insertOne(
                journal,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
             // db.close();
             
    

}else{
callback(true,null);
}
},
updateJournal: function (db,journal,codeUser, callback){

   
    journal.dateLastUpdate = moment(new Date).format('L');
    
    journal.dateJournalObject = utils.isUndefined(journal.dateJournalObject)? 
    new Date():journal.dateJournalObject;
    if(utils.isUndefined(journal.numeroJournal))
        {
           const currentD =  moment(new Date());
          // const genereKey = shortid.generate();
           const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.minutes().toString() ;
           journal.numeroJournal = 'JOURN' +currentD.day().toLocaleString()+ generatedCode;
        }
   
 

    const criteria = { "code" : journal.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
    "dateJournalObject" : journal.dateJournalObject,  
    "montantVentes" : journal.montantVentes, 
    "montantAchats" : journal.montantAchats ,
     "montantRH" : journal.montantRH, 
     "montantCharges":journal.montantCharges,
     "description" : journal.description,
     
      "etat": journal.etat,// ouvert/initial/en cours de validation /validé /en cours de validation /cloturé
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": journal.dateLastUpdate, 
    } ;
            db.collection('Journal').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },function(journal,err){
                    if(err)
                    callback(false,err);
                    else
                        callback(true,true);
                    
                    
                }
            ) ;
     
    
    },
getListJournaux : function (db,codeUser,pageNumber, nbElementsPerPage ,filter, callback)
{
    
    //console.log('filter recieved is :',filter);
    let filterObject = JSON.parse(filter);
   // console.log('filter recieved converted to object :',filterObject);
    
    let listJournaux = [];
    let filterData = {
        "userCreatorCode" : codeUser
    };
    if (!utils.isUndefined(filterObject))
    {
    if(!utils.isUndefined(filterObject.numeroJournal))
    {
            // filterData.numeroJournal = filter.numeroJournal;
            filterData = {
                "userCreatorCode" : codeUser,
                "numeroJournal" : filterObject.numeroJournal
            };
    }
     
    if(!utils.isUndefined(filterObject.etat))
        {
                //filterData.etat = filter.etat;
                filterData = {
                    "userCreatorCode" : codeUser,
                    "etat" : filterObject.etat
                };
        }
        if(!utils.isUndefined(filterObject.dateJournalFromObject) && utils.isUndefined(filterObject.dateJournalToObject) )
            {
                    //filterData.etat = filter.etat;
                    filterData = {
                        "userCreatorCode" : codeUser,
                        "dateJournalObject" : {$gt: filterObject.dateJournalFromObject}
                    };
            }
    
            if(utils.isUndefined(filterObject.dateJournalFromObject) && !utils.isUndefined(filterObject.dateJournalToObject) )
                {
                        //filterData.etat = filter.etat;
                        filterData = {
                            "userCreatorCode" : codeUser,
                            "dateJournalObject" : {$lt: filterObject.dateJournalToObject}
                        };
                }
        
                if(!utils.isUndefined(filterObject.dateJournalFromObject) && !utils.isUndefined(filterObject.dateJournalToObject) )
                    {
                            //filterData.etat = filter.etat;
                            filterData = {
                                "userCreatorCode" : codeUser,
                                "dateJournalObject" : {$lt: filterObject.dateJournalToObject ,$gt:filterObject.dateJournalFromObject}
                            };
                    }
         if(!utils.isUndefined(filterObject.dateJournalFromObject) && !utils.isUndefined(filterObject.dateJournalToObject)
        && !utils.isUndefined(filterObject.etat) )
                        {
                                //filterData.etat = filter.etat;
                                filterData = {
                                    "userCreatorCode" : codeUser,
                                    "dateJournalObject" : {$lt: filterObject.dateJournalToObject ,$gt:filterObject.dateJournalFromObject},
                                    "etat": filterObject.etat
                                };
                        }

 if(!utils.isUndefined(filterObject.dateJournalFromObject) && utils.isUndefined(filterObject.dateJournalToObject)
                            && !utils.isUndefined(filterObject.etat) )
                                            {
                                                    //filterData.etat = filter.etat;
                                                    filterData = {
                                                        "userCreatorCode" : codeUser,
                                                        "dateJournalObject" : {$gt:filterObject.dateJournalFromObject},
                                                        "etat": filterObject.etat
                                                    };
                                            }
                    
            if(utils.isUndefined(filterObject.dateJournalFromObject) && !utils.isUndefined(filterObject.dateJournalToObject)
                                 && !utils.isUndefined(filterObject.etat) )
                                                                {
                                                                        //filterData.etat = filter.etat;
                        filterData = {
                                                     "userCreatorCode" : codeUser,
                                          "dateJournalObject" : {$lt:filterObject.dateJournalToObject},
                                                  "etat": filterObject.etat
                                  };
                                          }

        if(!utils.isUndefined(filterObject.etat) && !utils.isUndefined(filterObject.numeroJournal))
            {
                    //filterData.etat = filter.etat;
                    filterData = {
                        "userCreatorCode" : codeUser,
                        "etat" : filterObject.etat,
                        "numeroJournal" : filterObject.numeroJournal
                        
                    };
            }
    

    }
    console.log('filter Data is ',filterData);
   var cursor =  db.collection('Journal').find( 
    filterData
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(journal,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        journal.dateJournal = moment(journal.dateJournalObject).format('L');
       // credit.dateDebutPayement = moment(credit.dateDebutPayementObject).format('L');
        
        listJournaux.push(journal);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listJournaux); 
   });
     //return [];
},
deleteJournal: function (db,codeJournal,codeUser,callback){
    const criteria = { "code" : codeJournal, "userCreatorCode":codeUser };
    
    db.collection('Journal').deleteOne(
        criteria , function(err,result){
            if(err){
                
                callback(null,null);
            }else{
                callback(false,result);
            }
        }
    ) ;
},

getJournal: function (db,codeJournal,codeUser,callback)
{
    const criteria = { "code" : codeJournal, "userCreatorCode":codeUser };
  
       const journal =  db.collection('Journal').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
        // callback(false,client);
                 
},
getTotalJournaux : function (db,codeUser, callback)
{
    let listJournaux = [];
   var totalJournaux =  db.collection('Journal').find( 
       {"userCreatorCode" : codeUser}
    ).count(function(err,result){
        callback(null,result); 
    }
);
   


     //return [];
}
}
module.exports.Journaux = Journaux;