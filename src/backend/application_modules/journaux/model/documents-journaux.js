//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var DocumentsJournaux= {
addDocumentJournal : function (db,documentJournal,codeUser,callback){
    if(documentJournal != undefined){
        documentJournal.code  = utils.isUndefined(documentJournal.code)?shortid.generate():documentJournal.code;
        documentJournal.dateReceptionObject = utils.isUndefined(documentJournal.dateReceptionObject)? 
        new Date():documentJournal.dateReceptionObject;
        if(utils.isUndefined(documentJournal.numeroDocument))
            {
               const currentD =  moment(new Date());
              // const genereKey = shortid.generate();
              const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.minutes().toString()+
              currentD.secondes().toString();
              documentJournal.numeroDocument = 'DOCJR' +currentD.day().toLocaleString()+ generatedCode;
            }
        documentJournal.userCreatorCode = codeUser;
        documentJournal.userLastUpdatorCode = codeUser;
        documentJournal.dateCreation = moment(new Date).format('L');
        documentJournal.dateLastUpdate = moment(new Date).format('L');
        
  
            db.collection('DocumentJournal').insertOne(
                documentJournal,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,documentJournal);
                   }
                }
              ) ;
             // db.close();
             
   

}else{
callback(true,null);
}
},
updateDocumentJournal: function (db,documentJournal,codeUser, callback){

   
    documentJournal.dateReceptionObject = utils.isUndefined(documentJournal.dateReceptionObject)? 
    new Date():documentJournal.dateReceptionObject;

    const criteria = { "code" : documentJournal.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"dateReceptionObject" : documentJournal.dateReceptionObject, 
    "codeJournal" : documentJournal.codeJournal, 
    "description" : documentJournal.description,
     "codeOrganisation" : documentJournal.codeOrganisation,
     "typeDocument" : documentJournal.typeDocument, // cheque, fiche paie, fiche electricié , fiche eau potabe, fiche téléphonique, autres
     "numeroDocument" : documentJournal.numeroDocument, 
     "imageFace1":documentJournal.imageFace1,
     "imageFace2" : documentJournal.imageFace2 , 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": documentJournal.dateLastUpdate, 
    } ;
     
            db.collection('DocumentJournal').updateOne( criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                     callback(true,null);
                    }else{
 
                     callback(false,result);
                    }
                 }
            ) ;
    
    
    },
getListDocumentsJournaux : function (db,codeUser,callback)
{
    let listDocumentsJournaux = [];

   var cursor =  db.collection('DocumentJournal').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(docJournal,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        docJournal.dateReception = moment(docJournal.dateReceptionObject).format('L');
        
        listDocumentsJournaux.push(docJournal);
   }).then(function(){
    // console.log('list documents credits',listDocumentsCredits);
    callback(null,listDocumentsJournaux); 
   });
   

     //return [];
},
deleteDocumentJournal: function (db,codeDocumentJournal,codeUser,callback){
    const criteria = { "code" : codeDocumentJournal, "userCreatorCode":codeUser };
        
            db.collection('DocumentJournal').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                     callback(true,null);
                    }else{
 
                     callback(false,result);
                    }
                 }
            ) ;
    
},

getDocumentJournal: function (db,codeDocumentJournal,codeUser,callback)
{
    const criteria = { "code" : codeDocumentJournal, "userCreatorCode":codeUser };
 
       const journal =  db.collection('DocumentJournal').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            });
                  
},


getListDocumentsJournauxByCodeJournal : function (db,codeUser,codeJournal,callback)
{
    let listDocumentsJournaux = [];

   var cursor =  db.collection('DocumentJournal').find( 
       {"userCreatorCode" : codeUser, "codeJournal": codeJournal},
       
    );
   cursor.forEach(function(documentJournal,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        documentJournal.dateReception = moment(documentJournal.dateReceptionObject).format('L');
        
        listDocumentsJournaux.push(documentJournal);
   }).then(function(){
   // console.log('list credits',listCredits);
    callback(null,listDocumentsJournaux); 
   });
   
}

     //return [];



}
module.exports.DocumentsJournaux = DocumentsJournaux;