//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var LignesJournaux= {
addLigneJournal : function (db,ligneJournal,codeUser,callback){
    if(ligneJournal != undefined){
        ligneJournal.code  = utils.isUndefined(ligneJournal.code)?shortid.generate():ligneJournal.code;
        if(utils.isUndefined(ligneJournal.numeroLigneJournal))
            {
               const currentD =  moment(new Date());
              // const genereKey = shortid.generate();
              const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.minutes().toString()+
              currentD.secondes().toString();
              ligneJournal.numeroLigneJournal = 'LIJOUR' +currentD.day().toLocaleString()+ generatedCode;
            }
            ligneJournal.userCreatorCode = codeUser;
            ligneJournal.userLastUpdatorCode = codeUser;
            ligneJournal.dateCreation = moment(new Date).format('L');
            ligneJournal.dateLastUpdate = moment(new Date).format('L');
            db.collection('LigneJournal').insertOne(
                ligneJournal,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,ligneJournal);
                   }
                }
              ) ;
             // db.close();
             
      

}else{
callback(true,null);
}
},
updateLigneJournal: function (db,ligneJournal,codeUser, callback){
    const criteria = { "code" : ligneJournal.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"heureOperation" : ligneJournal.heureOperation, 
    "codeJournal" : ligneJournal.codeJournal, 
    "description" : ligneJournal.description,
     "codeOrganisation" : ligneJournal.codeOrganisation,
     "montant" : ligneJournal.montant, 
     "modePaiement" : ligneJournal.modePaiement, 
     "typeOperation" : ligneJournal.typeOperation, 
     "etat":ligneJournal.etat,
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": ligneJournal.dateLastUpdate, 
    } ;
       
            db.collection('LigneJournal').updateOne(
                criteria,  { 
                     $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                     callback(true,null);
                    }else{
                     callback(false,ligneJournal);
                    }
                 }

            ) ;
      
    
    },
getListLignesJournaux : function (db,codeUser,callback)
{
    let listLignesJournaux = [];
    
   var cursor =  db.collection('LigneJournal').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(ligneJournal,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        
        listLignesJournaux.push(ligneJournal);
   }).then(function(){
    // console.log('list documents credits',listDocumentsCredits);
    callback(null,listLignesJournaux); 
   });

},
deleteLigneJournal: function (db,codeLigneJournal,codeUser,callback){
    const criteria = { "code" : codeLigneJournal, "userCreatorCode":codeUser };
      
        
            db.collection('LigneJournal').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                     callback(true,null);
                    }else{
 
                        callback(false,codeLigneJournal);                    }
                 }
            ) ;
},

getLigneJournal: function (db,codeLigneJournal,codeUser,callback)
{
    const criteria = { "code" : codeLigneJournal, "userCreatorCode":codeUser };
    
       const lJournal =  db.collection('LigneJournal').findOne(
            criteria , function(err,result){
                if(err){
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
          
},


getListLignesJournauxByCodeJournal : function (db,codeUser,codeJournal,callback)
{
    let listLignesJournaux = [];
     
   var cursor =  db.collection('LigneJournal').find( 
       {"userCreatorCode" : codeUser, "codeJournal": codeJournal}
    );
   cursor.forEach(function(lJournal,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        
        listLignesJournaux.push(lJournal);
   }).then(function(){
   // console.log('list credits',listCredits);
    callback(null,listLignesJournaux); 
   });
   
}


}
module.exports.LignesJournaux = LignesJournaux;