//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var DocumentsRelevesVentes = {
addDocumentReleveVentes : function (db,documentReleveVentes,codeUser,callback){
    if(documentReleveVentes != undefined){
        documentReleveVentes.code  = utils.isUndefined(documentReleveVentes.code)?shortid.generate():documentReleveVentes.code;
        documentReleveVentes.dateReceptionObject = utils.isUndefined(documentReleveVentes.dateReceptionObject)? 
        new Date():documentReleveVentes.dateReceptionObject;
        documentReleveVentes.userCreatorCode = codeUser;
        documentReleveVentes.userLastUpdatorCode = codeUser;
        documentReleveVentes.dateCreation = moment(new Date).format('L');
        documentReleveVentes.dateLastUpdate = moment(new Date).format('L');
    
            db.collection('DocumentReleveVentes').insertOne(
                documentReleveVentes,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,documentReleveVentes);
                   }
                }
              ) ;
   

}else{
callback(true,null);
}
},
updateDocumentReleveVentes: function (db,documentReleveVentes,codeUser, callback){

   
    documentReleveVentes.dateReceptionObject = utils.isUndefined(documentReleveVentes.dateReceptionObject)? 
    new Date():documentReleveVentes.dateReceptionObject;

    const criteria = { "code" : documentReleveVentes.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"dateReceptionObject" : documentReleveVentes.dateReceptionObject, 
    "codeReleveVentes" : documentReleveVentes.codeReleveVentes, 
    "description" : documentReleveVentes.description,
     "codeOrganisation" : documentReleveVentes.codeOrganisation,
     "typeDocument" : documentReleveVentes.typeDocument, // cheque , virement , espece 
     "numeroDocument" : documentReleveVentes.numeroDocument, 
     "imageFace1":documentReleveVentes.imageFace1,
     "imageFace2" : documentReleveVentes.imageFace2 , 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": documentReleveVentes.dateLastUpdate, 
    } ;
       
            db.collection('DocumentReleveVentes').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
    
    },
getListDocumentsRelevesVentes : function (db,codeUser,callback)
{
    let listDocumentsRelevesVentes = [];
   
   var cursor =  db.collection('DocumentReleveVentes').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(docRelVentes,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        docRelVentes.dateReception = moment(docRelVentes.dateReceptionObject).format('L');
        
        listDocumentsRelevesVentes.push(docRelVentes);
   }).then(function(){
    //console.log('list documents credits',listDocumentsCredits);
    callback(null,listDocumentsRelevesVentes); 
   });
   

},
deleteDocumentReleveVentes: function (db,codeDocumentReleveVentes,codeUser,callback){
    const criteria = { "code" : codeDocumentReleveVentes, "userCreatorCode":codeUser };
      
            db.collection('DocumentReleveVentes').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;

},

getDocumentReleveVentes: function (db,codeDocumentReleveVentes,codeUser,callback)
{
    const criteria = { "code" : codeDocumentReleveVentes, "userCreatorCode":codeUser };

       const doc =  db.collection('DocumentReleveVentes').findOne(
            criteria , function(err,result){
                if(err){
                   
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                  
},


getListDocumentsReleveVentesByCodeReleve : function (db,codeUser,codeReleveVentes,callback)
{
    let listDocumentsReleveVentes = [];
  
   var cursor =  db.collection('DocumentReleveVentes').find( 
       {"userCreatorCode" : codeUser, "codeReleveVentes": codeReleveVentes}
    );
   cursor.forEach(function(documentRV,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        documentRV.dateReception = moment(documentRV.dateReceptionObject).format('L');
        
        listDocumentsReleveVentes.push(documentRV);
   }).then(function(){
  // console.log('list doc credits',listCredits);
    callback(null,listDocumentsReleveVentes); 
   });

},


}
module.exports.DocumentsRelevesVentes = DocumentsRelevesVentes;