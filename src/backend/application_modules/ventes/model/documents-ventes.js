//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var DocumentsVentes = {
addDocumentVente : function (db,documentVente,codeUser, callback){
    if(documentVente != undefined){
        documentVente.code  = utils.isUndefined(documentVente.code)?shortid.generate():documentVente.code;
        documentVente.dateVenteObject = utils.isUndefined(documentVente.dateVenteObject)? 
        new Date():documentVente.dateVenteObject;
        if(utils.isUndefined(documentVente.numeroDocumentVente))
            {
               const currentD =  moment(new Date());
              // const genereKey = shortid.generate();
               const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.minutes().toString() ;
               documentVente.numeroDocumentVente = 'DOCVEN' + generatedCode;
            }
        documentVente.userCreatorCode = codeUser;
        documentVente.userLastUpdatorCode = codeUser;
        documentVente.dateCreation = moment(new Date).format('L');
        documentVente.dateLastUpdate = moment(new Date).format('L');
      
            db.collection('DocumentVente').insertOne(
                documentVente,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
 

}else{
callback(true,null);
}
},
updateDocumentVente: function (db,documentVente,codeUser, callback){

    //factureVente.dateFacturation = utils.isUndefined(factureVente.dateFacturation)? 
    //moment(new Date).format('L'): moment(factureVente.dateFacturation).format('L');
   
    documentVente.dateVenteObject = utils.isUndefined(documentVente.dateVenteObject)? 
    new Date():documentVente.dateVenteObject;

    documentVente.userLastUpdatorCode = codeUser;
    documentVente.dateCreation = moment(new Date).format('L');
    documentVente.dateLastUpdate = moment(new Date).format('L');
    const criteria = { "code" : documentVente.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"client" : documentVente.client,
     "dateVenteObject" : documentVente.dateVenteObject ,
     "numeroDocumentVente" : documentVente.numeroDocumentVente,
      "montantAPayer" : documentVente.montantAPayer, 
     "montantTVA": documentVente.montantTVA,
      "montantRemise" : documentVente.montantRemise,
     "montantCout" : documentVente.montantCout ,
      "nbJoursOccupationMainOeuvre" : documentVente.nbJoursOccupationMainOeuvre ,
      "payementData" : documentVente.payementData,
       "etat" : documentVente.etat,
       "nbMagasins" : documentVente.nbMagasins,
       "gestionMarchandises" : documentVente.gestionMarchandises,
       "gestionTaxes" : documentVente.gestionTaxes,
       "gestionLogistique" : documentVente.gestionLogistique,
       "gestionCouts" : documentVente.gestionCouts,
       "gestionMainOeuvres" : documentVente.gestionMainOeuvres,
       "gestionRemises" : documentVente.gestionRemises,
       "credit" : documentVente.credit,
       "listLignesMarchandises": documentVente.listLignesMarchandises,
       "listLignesCouts": documentVente.listLignesCouts,
       "listLignesTaxes": documentVente.listLignesTaxes,
       "listLignesLogistiques": documentVente.listLignesLogistiques,
       "listLignesOccupationMainOeuvres": documentVente.listLignesOccupationMainOeuvres,
       "listLignesRemises": documentVente.listLignesRemises,
       
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": documentVente.dateLastUpdate, 
       

    } ;
      
            db.collection('DocumentVente').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
       
    
    },
getListDocumentsVentes : function (db,codeUser, callback)
{
    let listDocumentsVentes = [];
    
   var cursor =  db.collection('DocumentVente').find(
    {"userCreatorCode" : codeUser}
   );
   cursor.forEach(function(documentVente,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        documentVente.dateVente = 
        moment(documentVente.dateVenteObject).format('L');
        listDocumentsVentes.push(documentVente);
   }).then(function(){
   //  console.log('list factures ventes ',listDocumentsVentes);
    callback(null,listDocumentsVentes); 
   });
   

     //return [];
},
deleteDocumentVente: function (db,codeDocumentVente,codeUser, callback){
    const criteria = { "code" : codeDocumentVente, "userCreatorCode":codeUser };
    
      
            db.collection('DocumentVente').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
      
},

getDocumentVente: function (db,codeDocumentVente,codeUser, callback)
{
    const criteria = { "code" : codeDocumentVente, "userCreatorCode":codeUser };
    
 
       const documentVente =  db.collection('DocumentVente').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                    
},


}
module.exports.DocumentsVentes = DocumentsVentes;