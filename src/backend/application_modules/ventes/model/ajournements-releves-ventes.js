//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var AjournementsRelevesVentes = {
addAjournementReleveVentes : function (db,ajournementReleveVentes,codeUser,callback){
    if(ajournementReleveVentes != undefined){
        ajournementReleveVentes.code  = utils.isUndefined(ajournementReleveVentes.code)?shortid.generate():ajournementReleveVentes.code;
        ajournementReleveVentes.dateReceptionObject = utils.isUndefined(ajournementReleveVentes.dateReceptionObject)? 
        new Date():ajournementReleveVentes.dateReceptionObject;
        ajournementReleveVentes.userCreatorCode = codeUser;
        ajournementReleveVentes.userLastUpdatorCode = codeUser;
        ajournementReleveVentes.dateCreation = moment(new Date).format('L');
        ajournementReleveVentes.dateLastUpdate = moment(new Date).format('L');
        
      
            db.collection('AjournementReleveVentes').insertOne(
                ajournementReleveVentes,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,ajournementReleveVentes);
                   }
                }
              ) ;
             // db.close();
             
    

}else{
callback(true,null);
}
},
updateAjournementReleveVentes: function (db,ajournementReleveVentes,codeUser, callback){
    
       
    ajournementReleveVentes.dateOperationObject = utils.isUndefined(ajournementReleveVentes.dateOperationObject)? 
        new Date():ajournementReleveVentes.dateOperationObject;
        ajournementReleveVentes.dateLastUpdate = moment(new Date).format('L');
        
        const criteria = { "code" : ajournementReleveVentes.code,"userCreatorCode" : codeUser };
        const dataToUpdate = {
        "codeReleveVentes" : ajournementReleveVentes.codeReleveVentes, 
        "description" : ajournementReleveVentes.description,
         "codeOrganisation" : ajournementReleveVentes.codeOrganisation,
        "dateOperationObject" : ajournementReleveVentes.dateOperationObject ,
        "nouvelleDatePayementObject" : ajournementReleveVentes.nouvelleDatePayementObject ,
        "motif" : ajournementReleveVentes.motif ,
           "userLastUpdatorCode": codeUser,
           "dateLastUpdate": ajournementReleveVentes.dateLastUpdate, 
        } ;
          
                db.collection('AjournementReleveVentes').updateOne(
                    criteria,
                    { 
                        $set: dataToUpdate
                    },
                    function(err,result){
                        if(err){
                            
                            callback(null,null);
                        }else{
                            
                            callback(false,result);
                        }
                    }  
                ) ;
                
        
        
        },
    getListAjournementsRelevesVentes : function (db,codeUser,callback)
    {
        let listAjournementsRelevesVentes = [];
         
       var cursor =  db.collection('AjournementReleveVentes').find( 
           {"userCreatorCode" : codeUser}
        );
       cursor.forEach(function(ajournementRelVentes,err){
           if(err)
            {
                console.log('errors produced :', err);
            }
            ajournementRelVentes.dateOperation = moment(ajournementRelVentes.dateOperationObject).format('L');
            ajournementRelVentes.nouvelleDatePayement = moment(ajournementRelVentes.nouvelleDatePayementObject).format('L');
            
            listAjournementsRelevesVentes.push(ajournementRelVentes);
       }).then(function(){
       //  console.log('list payements credits',listPayementsCredits);
        callback(null,listAjournementsRelevesVentes); 
       });
       

         //return [];
    },
    deleteAjournementReleveVentes: function (db,codeAjournementReleveVentes,codeUser,callback){
        const criteria = { "code" : codeAjournementReleveVentes, "userCreatorCode":codeUser };
        
                db.collection('AjournementReleveVentes').deleteOne(
                    criteria ,
                    function(err,result){
                        if(err){
                            
                            callback(null,null);
                        }else{
                            
                            callback(false,result);
                        }
                    }  
                ) ;
         
    },
    
    getAjournementReleveVentes: function (db,codeAjournementReleveVentes,codeUser,callback)
    {
        const criteria = { "code" : codeAjournementReleveVentes, "userCreatorCode":codeUser };
      
           const ajournmentRelVentes =  db.collection('AjournementReleveVentes').findOne(
                criteria , function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }
            ) ;
                        
    },
    
    
    getListAjournementsRelevesVentesByCodeReleveVentes : function (db,codeUser,codeReleveVentes,callback)
    {
        let listAjournementsRelevesVentes = [];
     
       var cursor =  db.collection('AjournementReleveVentes').find( 
           {"userCreatorCode" : codeUser, "codeReleveVentes": codeReleveVentes}
        );
       cursor.forEach(function(ajournementRelVentes,err){
           if(err)
            {
                console.log('errors produced :', err);
            }
            ajournementRelVentes.dateOperation = moment(ajournementRelVentes.dateOperationObject).format('L');
            ajournementRelVentes.nouvelleDatePayement = moment(ajournementRelVentes.nouvelleDatePayementObject).format('L');
            
            listAjournementsRelevesVentes.push(ajournementRelVentes);
       }).then(function(){
       // console.log('list credits',listCredits);
        callback(null,listAjournementsRelevesVentes); 
       });
 
    },
    
    
    }
    module.exports.AjournementsRelevesVentes = AjournementsRelevesVentes;