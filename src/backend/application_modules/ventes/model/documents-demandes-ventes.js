//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var DocumentsDemandesVentes = {
addDocumentDemandeVentes : function (db,documentDemandeVentes,codeUser,callback){
    if(documentDemandeVentes != undefined){
        documentDemandeVentes.code  = utils.isUndefined(documentDemandeVentes.code)?shortid.generate():documentDemandeVentes.code;
        documentDemandeVentes.dateReceptionObject = utils.isUndefined(documentDemandeVentes.dateReceptionObject)? 
        new Date():documentDemandeVentes.dateReceptionObject;
        documentDemandeVentes.userCreatorCode = codeUser;
        documentDemandeVentes.userLastUpdatorCode = codeUser;
        documentDemandeVentes.dateCreation = moment(new Date).format('L');
        documentDemandeVentes.dateLastUpdate = moment(new Date).format('L');
        
      
            db.collection('DocumentDemandeVentes').insertOne(
                documentDemandeVentes,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,documentDemandeVentes);
                   }
                }
              ) ;
    

}else{
callback(true,null);
}
},
updateDocumentDemandeVentes: function (db,documentDemandeVentes,codeUser, callback){

   
    documentDemandeVentes.dateReceptionObject = utils.isUndefined(documentDemandeVentes.dateReceptionObject)? 
    new Date():documentDemandeVentes.dateReceptionObject;

    const criteria = { "code" : documentDemandeVentes.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"dateReceptionObject" : documentDemandeVentes.dateReceptionObject, 
    "codeDemandeVentes" : documentDemandeVentes.codeDemandeVentes, 
    "description" : documentDemandeVentes.description,
     "codeOrganisation" : documentDemandeVentes.codeOrganisation,
     "typeDocument" : documentDemandeVentes.typeDocument, // cheque , virement , espece 
     "numeroDocument" : documentDemandeVentes.numeroDocument, 
     "imageFace1":documentDemandeVentes.imageFace1,
     "imageFace2" : documentDemandeVentes.imageFace2 , 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": documentDemandeVentes.dateLastUpdate, 
    } ;
       
            db.collection('DocumentDemandeVentes').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
     
    
    },
getListDocumentsDemandesVentes : function (db,codeUser,callback)
{
    let listDocumentsDemandesVentes = [];
    
   var cursor =  db.collection('DocumentDemandeVentes').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(docRelVentes,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        docRelVentes.dateReception = moment(docRelVentes.dateReceptionObject).format('L');
        
        listDocumentsDemandesVentes.push(docRelVentes);
   }).then(function(){
    //console.log('list documents credits',listDocumentsCredits);
    callback(null,listDocumentsDemandesVentes); 
   });
   


},
deleteDocumentDemandeVentes: function (db,codeDocumentDemandeVentes,codeUser,callback){
    const criteria = { "code" : codeDocumentDemandeVentes, "userCreatorCode":codeUser };
        
            db.collection('DocumentDemandeVentes').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
      
},

getDocumentDemandeVentes: function (db,codeDocumentDemandeVentes,codeUser,callback)
{
    const criteria = { "code" : codeDocumentDemandeVentes, "userCreatorCode":codeUser };
  
       const doc =  db.collection('DocumentDemandeVentes').findOne(
            criteria , function(err,result){
                if(err){
                  
                    
                    callback(null,null);
                }else{
                
                    
                    callback(false,result);
                }
            }
        ) ;
                   
},


getListDocumentsDemandeVentesByCodeDemande : function (db,codeUser,codeDemandeVentes,callback)
{
    let listDocumentsDemandeVentes = [];
   
   var cursor =  db.collection('DocumentDemandeVentes').find( 
       {"userCreatorCode" : codeUser, "codeDemandeVentes": codeDemandeVentes}
    );
   cursor.forEach(function(documentFV,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        documentRV.dateReception = moment(documentRV.dateReceptionObject).format('L');
        
        listDocumentsDemandeVentes.push(documentRV);
   }).then(function(){
  // console.log('list doc credits',listCredits);
    callback(null,listDocumentsDemandeVentes); 
   });
   

     //return [];
},


}
module.exports.DocumentsDemandesVentes = DocumentsDemandesVentes;