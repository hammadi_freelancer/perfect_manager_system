//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var LignesFactureVentes = {
addLigneFactureVentes : function (db,ligneFactureVentes,codeUser,callback){
    if(ligneFactureVentes != undefined){
        ligneFactureVentes.code  = utils.isUndefined(ligneFactureVentes.code)?shortid.generate():ligneFactureVentes.code;
        if(utils.isUndefined(ligneFactureVentes.numeroLigneFactureVentes))
            {
               const currentD =  moment(new Date());
              // const genereKey = shortid.generate();
               const generatedCode = currentD.year().toString() +'/'+currentD.utc(true).month().toString()+currentD.minutes().toString()+'/'+
               currentD.seconds().toString() ;
               ligneFactureVentes.numeroLigneFactureVentes = 'LIGN' + generatedCode;
            }
        ligneFactureVentes.userCreatorCode = codeUser;
        ligneFactureVentes.userLastUpdatorCode = codeUser;
        ligneFactureVentes.dateCreation = moment(new Date).format('L');
        ligneFactureVentes.dateLastUpdate = moment(new Date).format('L');
        
       
            db.collection('LigneFactureVentes').insertOne(
                ligneFactureVentes,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,ligneFactureVentes);
                   }
                }
              ) ;


}else{
callback(true,null);
}
},
updateLigneFactureVentes: function (db,ligneFactureVentes,codeUser, callback){
    const criteria = { "code" : ligneFactureVentes.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"numeroLigneFactureVentes" : ligneFactureVentes.numeroLigneFactureVentes, 
    "codeFactureVentes" : ligneFactureVentes.codeFactureVentes, 
    "article" : ligneFactureVentes.article, 
    "unMesure" : ligneFactureVentes.unMesure, 
    "magasin" : ligneFactureVentes.magasin, 
    "quantite" : ligneFactureVentes.quantite, 
    "prixUnitaire" : ligneFactureVentes.prixUnitaire, 
    "tva" : ligneFactureVentes.tva, 
    "montantTVA" : ligneFactureVentes.montantTVA, 
    "fodec" : ligneFactureVentes.fodec, 
    "montantFodec" : ligneFactureVentes.montantFodec, 
    "otherTaxe1" : ligneFactureVentes.otherTaxe1, 
    "montantOtherTaxe1" : ligneFactureVentes.montantOtherTaxe1,
    "otherTaxe2" : ligneFactureVentes.otherTaxe2, 
    "montantOtherTaxe2" : ligneFactureVentes.montantOtherTaxe2,  
    "remise" : ligneFactureVentes.remise, 
    "montantRemise" : ligneFactureVentes.montantRemise,
    "prixVente" : ligneFactureVentes.prixVente, 
    "benefice" : ligneFactureVentes.benefice, 
    "montantBenefice" : ligneFactureVentes.montantBenefice, 
    "montantHT" : ligneFactureVentes.montantHT, 
    "montantTTC" : ligneFactureVentes.montantTTC, 
    "montantAPayer" : ligneFactureVentes.montantAPayer, 
    "etat" : ligneFactureVentes.etat, 
    "description" : ligneFactureVentes.description,
     "codeOrganisation" : ligneFactureVentes.codeOrganisation,
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": ligneFactureVentes.dateLastUpdate, 
    } ;
    
            db.collection('LigneFactureVentes').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
           
      
    
    },
getListLignesFactureVentes : function (db,codeUser,callback)
{
    let listLignesFactureVentes = [];
 
   var cursor =  db.collection('LigneFactureVentes').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(ligneFactureVente,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
       // docClient.dateReception = moment(docClient.dateReceptionObject).format('L');
        
       listLignesFactureVentes.push(ligneFactureVente);
   }).then(function(){
    callback(null,listLignesFactureVentes); 
   });

},
deleteLigneFactureVentes: function (db,codeLigneFactureVentes,codeUser,callback){
    const criteria = { "code" : codeLigneFactureVentes, "userCreatorCode":codeUser };
      
            db.collection('LigneFactureVentes').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
      
},

getLigneFactureVentes: function (db,codeLigneFactureVentes,codeUser,callback)
{
    const criteria = { "code" : codeLigneFactureVentes, "userCreatorCode":codeUser };
   
       const ligneFactureVentes=  db.collection('LigneFactureVentes').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                    
},


getListLignesFactureVentesByCodeFactureVentes : function (db,codeUser,codeFactureVentes,callback)
{
    let listLignesFactureVentes = [];
     
   var cursor =  db.collection('LigneFactureVentes').find( 
       {"userCreatorCode" : codeUser, "codeFactureVentes": codeFactureVentes}
    );
   cursor.forEach(function(ligneFactureVentes,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        
        listLignesFactureVentes.push(ligneFactureVentes);
   }).then(function(){
  // console.log('list doc credits',listCredits);
    callback(null,listLignesFactureVentes); 
   });

     //return [];
},


}
module.exports.LignesFactureVentes = LignesFactureVentes;