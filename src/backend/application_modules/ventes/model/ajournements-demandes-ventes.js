//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var AjournementsDemandesVentes = {
addAjournementDemandeVentes : function (db,ajournementDemandeVentes,codeUser,callback){
    if(ajournementDemandeVentes != undefined){
        ajournementDemandeVentes.code  = utils.isUndefined(ajournementDemandeVentes.code)?shortid.generate():ajournementDemandeVentes.code;
        ajournementDemandeVentes.dateReceptionObject = utils.isUndefined(ajournementDemandeVentes.dateReceptionObject)? 
        new Date():ajournementDemandeVentes.dateReceptionObject;
        ajournementDemandeVentes.userCreatorCode = codeUser;
        ajournementDemandeVentes.userLastUpdatorCode = codeUser;
        ajournementDemandeVentes.dateCreation = moment(new Date).format('L');
        ajournementDemandeVentes.dateLastUpdate = moment(new Date).format('L');
        
       
            db.collection('AjournementDemandeVentes').insertOne(
                ajournementDemandeVentes,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,ajournementDemandeVentes);
                   }
                }
              ) ;
     

}else{
callback(true,null);
}
},
updateAjournementDemandeVentes: function (db,ajournementDemandeVentes,codeUser, callback){
    
       
    ajournementDemandeVentes.dateOperationObject = utils.isUndefined(ajournementDemandeVentes.dateOperationObject)? 
        new Date():ajournementDemandeVentes.dateOperationObject;
        ajournementDemandeVentes.dateLastUpdate = moment(new Date).format('L');
        
        const criteria = { "code" : ajournementDemandeVentes.code,"userCreatorCode" : codeUser };
        const dataToUpdate = {
        "codeDemandeVentes" : ajournementDemandeVentes.codeDemandeVentes, 
        "description" : ajournementDemandeVentes.description,
         "codeOrganisation" : ajournementDemandeVentes.codeOrganisation,
        "dateOperationObject" : ajournementDemandeVentes.dateOperationObject ,
        "nouvelleDatePayementObject" : ajournementDemandeVentes.nouvelleDatePayementObject ,
        "motif" : ajournementDemandeVentes.motif ,
           "userLastUpdatorCode": codeUser,
           "dateLastUpdate": ajournementDemandeVentes.dateLastUpdate, 
        } ;
          
                db.collection('AjournementDemandeVentes').updateOne(
                    criteria,
                    { 
                        $set: dataToUpdate
                    },
                    function(err,result){
                        if(err){
                            
                            callback(null,null);
                        }else{
                            
                            callback(false,result);
                        }
                    }  
                ) ;
          
        
        },
    getListAjournementsDemandesVentes : function (db,codeUser,callback)
    {
        let listAjournementsDemandesVentes = [];
         
       var cursor =  db.collection('AjournementDemandeVentes').find( 
           {"userCreatorCode" : codeUser}
        );
       cursor.forEach(function(ajournementRelVentes,err){
           if(err)
            {
                console.log('errors produced :', err);
            }
            ajournementRelVentes.dateOperation = moment(ajournementRelVentes.dateOperationObject).format('L');
            ajournementRelVentes.nouvelleDatePayement = moment(ajournementRelVentes.nouvelleDatePayementObject).format('L');
            
            listAjournementsDemandesVentes.push(ajournementRelVentes);
       }).then(function(){
       //  console.log('list payements credits',listPayementsCredits);
        callback(null,listAjournementsDemandesVentes); 
       });
       
  
         //return [];
    },
    deleteAjournementDemandeVentes: function (db,codeAjournementDemandeVentes,codeUser,callback){
        const criteria = { "code" : codeAjournementDemandeVentes, "userCreatorCode":codeUser };
         
                db.collection('AjournementDemandeVentes').deleteOne(
                    criteria ,
                    function(err,result){
                        if(err){
                            
                            callback(null,null);
                        }else{
                            
                            callback(false,result);
                        }
                    }  
                ) ;
  
    },
    
    getAjournementDemandeVentes: function (db,codeAjournementDemandeVentes,codeUser,callback)
    {
        const criteria = { "code" : codeAjournementDemandeVentes, "userCreatorCode":codeUser };
       
           const ajournmentRelVentes =  db.collection('AjournementDemandeVentes').findOne(
                criteria , function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }
            ) ;
            // callback(false,client);
                         
    },
    
    
    getListAjournementsDemandesVentesByCodeDemandeVentes : function (db,codeUser,codeDemandeVentes,callback)
    {
        let listAjournementsDemandesVentes = [];
      
       var cursor =  db.collection('AjournementDemandeVentes').find( 
           {"userCreatorCode" : codeUser, "codeDemandeVentes": codeDemandeVentes}
        );
       cursor.forEach(function(ajournementRelVentes,err){
           if(err)
            {
                console.log('errors produced :', err);
            }
            ajournementRelVentes.dateOperation = moment(ajournementRelVentes.dateOperationObject).format('L');
            ajournementRelVentes.nouvelleDatePayement = moment(ajournementRelVentes.nouvelleDatePayementObject).format('L');
            
            listAjournementsDemandesVentes.push(ajournementRelVentes);
       }).then(function(){
       // console.log('list credits',listCredits);
        callback(null,listAjournementsDemandesVentes); 
       });
       
  
         //return [];
    },
    
    
    }
    module.exports.AjournementsDemandesVentes = AjournementsDemandesVentes;