//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var RelevesVentes = {
addReleveVentes : function (db,releveVentes,codeUser, callback){


    if(releveVentes != undefined){
        releveVentes.code  = utils.isUndefined(releveVentes.code)?shortid.generate():releveVentes.code;
       if(utils.isUndefined(releveVentes.numeroReleveVente))
        {
           const currentD =  moment(new Date());
          // const genereKey = shortid.generate();
           const generatedCode = currentD.year().toString() +'/'+currentD.utc(true).month().toString()+currentD.minutes().toString() ;
           releveVentes.numeroReleveVente = 'RELV' + generatedCode;

           }
           const listRows =   releveVentes.listLignesRelVentes;
           listRows.forEach((row)=>(row.codeReleveVentes =releveVentes.code, row.numeroReleveVentes=releveVentes.numeroReleveVente ));
           releveVentes.listLignesRelVentes  = listRows;
     
           releveVentes.dateVenteObject = utils.isUndefined(releveVentes.dateVenteObject)? 
        new Date():releveVentes.dateVenteObject;
        releveVentes.userCreatorCode = codeUser;
        releveVentes.userLastUpdatorCode = codeUser;
        releveVentes.dateCreation = moment(new Date).format('L');
        releveVentes.dateLastUpdate = moment(new Date).format('L');
   
            db.collection('ReleveVentes').insertOne(
                releveVentes,function(err,result){
                  //  clientDB.close();
                   // mongoClient.clos
                   if(err){

                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
}else{
callback(true,null);
}
},




updateReleveVentes: function (db,releveVentes,codeUser, callback){

    //factureVente.dateFacturation = utils.isUndefined(factureVente.dateFacturation)? 
    //moment(new Date).format('L'): moment(factureVente.dateFacturation).format('L');
    const listRows =   releveVentes.listLignesRelVentes;
    listRows.forEach((row)=>(row.codeReleveVentes =releveVentes.code,
         row.numeroReleveVente=releveVentes.numeroReleveVente ));
         releveVentes.listLignesRelVentes  = listRows;


         releveVentes.dateVenteObject = utils.isUndefined(releveVentes.dateVenteObject)? 
    new Date():releveVentes.dateVenteObject;

    const dateLastUpdate = moment(new Date).format('L');
    const criteria = { "code" : releveVentes.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"client" : releveVentes.client,
     "dateVenteObject" : releveVentes.dateVenteObject ,
     "numeroReleveVente" : releveVentes.numeroReleveVente, 
     "montantBenefice" : releveVentes.montantBenefice, 
     "montantAPayer": releveVentes.montantAPayer, 
     "regleToutes" : releveVentes.regleToutes,
     "livreToutes" : releveVentes.livreToutes , 
      "payementData" : releveVentes.payementData,
       "etat" : releveVentes.etat,
       "listLignesRelVentes": releveVentes.listLignesRelVentes,
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": dateLastUpdate, 
       

    } ;
      
            db.collection('ReleveVentes').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
     
    
    },
getListRelevesVentes : function (db,codeUser,pageNumber,nbElementsPerPage,filter,callback)
{
    let listRelevesVentes = [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var cursor =  db.collection('ReleveVentes').find(
    condition
   ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
   .limit(Number(nbElementsPerPage) );
   
   cursor.forEach(function(releveVente,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        releveVente.dateVente = moment(releveVente.dateVenteObject).format('L');
 listRelevesVentes.push(releveVente);
   }).then(function(){
    // console.log('list factures ventes ',listFacturesVentes);
    callback(null,listRelevesVentes); 
   });

},
deleteReleveVentes: function (db,codeReleveVentes,codeUser, callback){
    const criteria = { "code" : codeReleveVentes, "userCreatorCode":codeUser };
   
            db.collection('ReleveVentes').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
       
              
        },
getReleveVentes: function (db,codeReleveVentes,codeUser, callback)
{
    const criteria = { "code" : codeReleveVentes, "userCreatorCode":codeUser };
    
  
       const releveVente =  db.collection('ReleveVentes').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                   
},

getTotalRelevesVentes: function (db,codeUser,filter, callback)
{
    
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
    const condition = this.buildFilterCondition(filterObject,filterData);
   var totalRelevesVentes =  db.collection('ReleveVentes').find( 
   condition
    ).count(function(err,result){
        callback(null,result); 
    }
);

},

buildFilterCondition(filterObject,filterData)
{
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.numeroReleveVente))
        {
                filterData["numeroReleveVente"] = filterObject.numeroReleveVente;
        }
         
        if(!utils.isUndefined(filterObject.montantBenefice))
          {
                 
                    filterData["montantBenefice"] = filterObject.montantBenefice;
                    
         }
         if(!utils.isUndefined(filterObject.montantAPayer))
             {
                    
                       filterData["montantAPayer"] =filterObject.montantAPayer;
                       
            }
            if(!utils.isUndefined(filterObject.regleToutes))
             {
                    
                       filterData["regleToutes"] = filterObject.regleToutes;
                       
            }
            if(!utils.isUndefined(filterObject.livreToutes))
             {
                    
                       filterData["livreToutes"] = filterObject.livreToutes ;
                       
            }
            if(!utils.isUndefined(filterObject.etat))
             {
                    
                       filterData["etat"] = filterObject.etat ;
                       
            }
        
            if(!utils.isUndefined(filterObject.dateVenteFromObject))
             {
                    
                       filterData["dateVenteObject"] = {$gt:filterObject.dateVenteFromObject} ;
                       
            }
            if(!utils.isUndefined(filterObject.dateVenteToObject))
                {
                       
                          filterData["dateVenteObject"] = {$lt:filterObject.dateVenteToObject} ;
                          
               }
        }
        return filterData;
},




}
module.exports.RelevesVentes = RelevesVentes;