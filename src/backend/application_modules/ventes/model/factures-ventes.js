//var db = require('../common/MongoDbConnection').getDBConnection;
const mongoClient = require('mongodb').MongoClient;
const url ='mongodb://localhost:27017';
const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var FacturesVentes = {
addFactureVente : function (db,factureVente,codeUser, callback){
    if(factureVente != undefined){
        factureVente.code  = utils.isUndefined(factureVente.code)?shortid.generate():factureVente.code;
       if(utils.isUndefined(factureVente.numeroFactureVente))
        {
           const currentD =  moment(new Date());
          // const genereKey = shortid.generate();
           const generatedCode = currentD.year().toString() +'/'+currentD.utc(true).month().toString()+currentD.minutes().toString() ;
           factureVente.numeroFactureVente = 'FACT' + generatedCode;

           }
           const listRows =   factureVente.listLignesFactures;
           listRows.forEach((row)=>(row.codeFactureVente =factureVente.code, row.numeroFactureVente=factureVente.numeroFactureVente ));
           factureVente.listLignesFactures  = listRows;
       console.log('list rows of facture ventes');
       console.log(listRows);
        factureVente.dateFacturationObject = utils.isUndefined(factureVente.dateFacturationObject)? 
        new Date():factureVente.dateFacturationObject;

        factureVente.dateVenteObject = utils.isUndefined(factureVente.dateVenteObject)? 
        new Date():factureVente.dateVenteObject;

        factureVente.userCreatorCode = codeUser;
        factureVente.userLastUpdatorCode = codeUser;
        factureVente.dateCreation = moment(new Date).format('L');
        factureVente.dateLastUpdate = moment(new Date).format('L');
       
            db.collection('FactureVente').insertOne(
                factureVente,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
   
}else{
callback(true,null);
}
},
updateFactureVente: function (db,factureVente,codeUser, callback){

    //factureVente.dateFacturation = utils.isUndefined(factureVente.dateFacturation)? 
    //moment(new Date).format('L'): moment(factureVente.dateFacturation).format('L');
    const listRows =   factureVente.listLignesFactures;
    listRows.forEach((row)=>(row.codeFactureVente =factureVente.code, row.numeroFactureVente=factureVente.numeroFactureVente ));
    factureVente.listLignesFactures  = listRows;
        console.log('list rows of facture ventes');
        console.log(listRows);
    factureVente.dateFacturationObject = utils.isUndefined(factureVente.dateFacturationObject)? 
    new Date():factureVente.dateFacturationObject;

    factureVente.dateVenteObject = utils.isUndefined(factureVente.dateVenteObject)? 
    new Date():factureVente.dateVenteObject;

    factureVente.userLastUpdatorCode = codeUser;
    factureVente.dateCreation = moment(new Date).format('L');
    factureVente.dateLastUpdate = moment(new Date).format('L');
    const criteria = { "code" : factureVente.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"client" : factureVente.client,
     "dateFacturationObject" : factureVente.dateFacturationObject ,
     "dateVenteObject" : factureVente.dateVenteObject ,
     "numeroFactureVente" : factureVente.numeroFactureVente, "montantAPayer" : factureVente.montantAPayer, 
     "montantTTC": factureVente.montantTTC, "montantHT" : factureVente.montantHT,
     "montantTVA" : factureVente.montantTVA , "montantRemise" : factureVente.montantRemise ,
      "payementData" : factureVente.payementData,
       "etat" : factureVente.etat,"listLignesFactures": factureVente.listLignesFactures,
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": factureVente.dateLastUpdate, 
       

    } ;
      
            db.collection('FactureVente').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
          
      
    
    },
    buildFilterCondition(filterObject,filterData)
    {
        if (!utils.isUndefined(filterObject))
            {
            if(!utils.isUndefined(filterObject.numeroFactureVente))
            {
                    filterData["numeroFactureVente"] = filterObject.numeroFactureVente;
            }
             
            if(!utils.isUndefined(filterObject.nomClient))
              {
                     
                        filterData["client.nom"] = filterObject.nomClient;
                        
             }
             if(!utils.isUndefined(filterObject.prenomClient))
                 {
                        
                           filterData["client.prenom"] =filterObject.prenomClient;
                           
                }
                if(!utils.isUndefined(filterObject.cinClient))
                 {
                        
                           filterData["client.cin"] = filterObject.cinClient;
                           
                }
                if(!utils.isUndefined(filterObject.raisonClient))
                 {
                        
                           filterData["client.raisonSociale"] = filterObject.raisonClient ;
                           
                }
                if(!utils.isUndefined(filterObject.matriculeClient))
                 {
                        
                           filterData["client.matriculeFiscale"] = filterObject.matriculeClient ;
                           
                }
                if(!utils.isUndefined(filterObject.registreClient))
                 {
                        
                           filterData["client.registreCommerce"] = filterObject.registreClient ;
                           
                }
                if(!utils.isUndefined(filterObject.dateFacturationFromObject))
                 {
                        
                           filterData["dateFacturationObject"] = {$gt:filterObject.dateFacturationFromObject} ;
                           
                }
                if(!utils.isUndefined(filterObject.dateFacturationToObject))
                    {
                           
                              filterData["dateFacturationObject"] = {$lt:filterObject.dateFacturationToObject} ;
                              
                   }
                   if(!utils.isUndefined(filterObject.dateVenteFromObject))
                    {
                           
                              filterData["dateVenteObject"] = {$gt:filterObject.dateVenteFromObject} ;
                              
                   }
                   if(!utils.isUndefined(filterObject.dateVenteToObject))
                       {
                              
                                 filterData["dateVenteObject"] = {$lt:filterObject.dateVenteToObject} ;
                                 
                      }

                    if(!utils.isUndefined(filterObject.montantAPayer))
                        {
                               
                                  filterData["montantAPayer"] = filterObject.montantAPayer ;
                                  
                       }

                       if(!utils.isUndefined(filterObject.etat))
                        {
                               
                                  filterData["etat"] = filterObject.etat ;
                                  
                       }



            }
            return filterData;
    },
getListFacturesVentes : function (db,codeUser,pageNumber,nbElementsPerPage,filter, callback)
{
    let listFacturesVentes = [];

    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var cursor =  db.collection('FactureVente').find(
condition   ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
   .limit(Number(nbElementsPerPage) );
   
   cursor.forEach(function(factureVente,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        factureVente.dateFacturation =  moment(factureVente.dateFacturationObject).format('L');
 factureVente.dateVente = moment(factureVente.dateVenteObject).format('L');
        listFacturesVentes.push(factureVente);
   }).then(function(){
    console.log('list factures ventes ',listFacturesVentes);
    callback(null,listFacturesVentes); 
   });
},
deleteFactureVente: function (db,codeFactureVente,codeUser, callback){
    const criteria = { "code" : codeFactureVente, "userCreatorCode":codeUser };
    
  
            db.collection('FactureVente').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
},

getFactureVente: function (db,codeFactureVentes,codeUser, callback)
{
    const criteria = { "code" : codeFactureVentes, "userCreatorCode":codeUser };
    
    console.log(' recuperate facture vente with code ',codeFactureVentes);
 //   console.log(' recuperate facture vente with codeuser  ',userCreatorCode);
    
       const factureVente =  db.collection('FactureVente').findOne(
            criteria , function(err,result){
                if(err){
                    console.log('error gestting facture vente with code ',codeFactureVentes)
                    console.log('error gestting facture vente:',err);
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                   
},

getTotalFacturesVentes: function (db,codeUser, filter,callback)
{

    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var totalFacturesVentes =  db.collection('FactureVente').find( 
condition
).count(function(err,result){
        callback(null,result); 
    }
);
   
}


}
module.exports.FacturesVentes = FacturesVentes;