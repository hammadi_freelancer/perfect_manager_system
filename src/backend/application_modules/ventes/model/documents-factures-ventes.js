//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var DocumentsFacturesVentes = {
addDocumentFactureVentes : function (db,documentFactureVentes,codeUser,callback){
    if(documentFactureVentes != undefined){
        documentFactureVentes.code  = utils.isUndefined(documentFactureVentes.code)?shortid.generate():documentFactureVentes.code;
        documentFactureVentes.dateReceptionObject = utils.isUndefined(documentFactureVentes.dateReceptionObject)? 
        new Date():documentFactureVentes.dateReceptionObject;
        documentFactureVentes.userCreatorCode = codeUser;
        documentFactureVentes.userLastUpdatorCode = codeUser;
        documentFactureVentes.dateCreation = moment(new Date).format('L');
        documentFactureVentes.dateLastUpdate = moment(new Date).format('L');
        
     
            db.collection('DocumentFactureVentes').insertOne(
                documentFactureVentes,function(err,result){
                   // clientDB.close();
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,documentFactureVentes);
                   }
                }
              ) ;


}else{
callback(true,null);
}
},
updateDocumentFactureVentes: function (db,documentFactureVentes,codeUser, callback){

   
    documentFactureVentes.dateReceptionObject = utils.isUndefined(documentFactureVentes.dateReceptionObject)? 
    new Date():documentFactureVentes.dateReceptionObject;

    const criteria = { "code" : documentFactureVentes.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"dateReceptionObject" : documentFactureVentes.dateReceptionObject, 
    "codeFactureVentes" : documentFactureVentes.codeFactureVentes, 
    "description" : documentFactureVentes.description,
     "codeOrganisation" : documentFactureVentes.codeOrganisation,
     "typeDocument" : documentFactureVentes.typeDocument, // cheque , virement , espece 
     "numeroDocument" : documentFactureVentes.numeroDocument, 
     "imageFace1":documentFactureVentes.imageFace1,
     "imageFace2" : documentFactureVentes.imageFace2 , 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": documentFactureVentes.dateLastUpdate, 
    } ;
  
            db.collection('DocumentFactureVentes').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
  
    },
getListDocumentsFacturesVentes : function (db,codeUser,callback)
{
    let listDocumentsFacturesVentes = [];
    
   var cursor =  db.collection('DocumentFactureVentes').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(docFactVentes,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        docFactVentes.dateReception = moment(docFactVentes.dateReceptionObject).format('L');
        
        listDocumentsFacturesVentes.push(docFactVentes);
   }).then(function(){
    //console.log('list documents credits',listDocumentsCredits);
    callback(null,listDocumentsFacturesVentes); 
   });
   

     //return [];
},
deleteDocumentFactureVentes: function (db,codeDocumentFactureVentes,codeUser,callback){
    const criteria = { "code" : codeDocumentFactureVentes, "userCreatorCode":codeUser };
      
            db.collection('DocumentFactureVentes').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
           
       
},

getDocumentFactureVentes: function (db,codeDocumentFactureVentes,codeUser,callback)
{
    const criteria = { "code" : codeDocumentFactureVentes, "userCreatorCode":codeUser };
   
       const credit =  db.collection('DocumentFactureVentes').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                    
},


getListDocumentsFacturesVentesByCodeFactureVentes : function (db,codeUser,codeFactureVentes,callback)
{
    let listDocumentsFacturesVentes = [];
 
   var cursor =  db.collection('DocumentFactureVentes').find( 
       {"userCreatorCode" : codeUser, "codeFactureVentes": codeFactureVentes}
    );
   cursor.forEach(function(documentFV,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        documentFV.dateReception = moment(documentFV.dateReceptionObject).format('L');
        
        listDocumentsFacturesVentes.push(documentFV);
   }).then(function(){
  // console.log('list doc credits',listCredits);
    callback(null,listDocumentsFacturesVentes); 
   });
   

     //return [];
},


}
module.exports.DocumentsFacturesVentes = DocumentsFacturesVentes;