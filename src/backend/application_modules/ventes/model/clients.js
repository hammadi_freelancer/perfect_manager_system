//var db = require('../common/MongoDbConnection').getDBConnection;
const mongoClient = require('mongodb').MongoClient;
const url ='mongodb://localhost:27017';
const dbName = 'gestion';
//console.log('db is ',db);
var moment = require('moment');
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var Clients = {
addClient : function (db,client,codeUser, callback){
    if(client != undefined){
    client.code  = client.code===undefined?shortid.generate():client.code;
    if(utils.isUndefined(client.numeroClient))
        {
           const currentD =  moment(new Date());
          // const genereKey = shortid.generate();
           const generatedCode = currentD.year().toString() +'/'+currentD.utc(true).month().toString()+currentD.minutes().toString()+'/'+
           currentD.seconds().toString() ;
           client.numeroClient = 'CLI' + generatedCode;
        }
        
    client.userCreatorCode = codeUser;
    client.userLastUpdatorCode = codeUser;
    client.dateCreation = moment(new Date).format('L');
    client.dateLastUpdate = moment(new Date).format('L');
  
        db.collection('Client').insertOne(
            client,
            function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }  
          ) ;
        
    

       //return true;  
}
//return false;
},
updateClient: function (db,client,codeUser, callback){
    client.userLastUpdatorCode = codeUser;
    client.dateLastUpdate = moment(new Date).format('L');
    const criteria = { "code" : client.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
        "nom" : client.nom,
        "numeroClient":client.numeroClient,
     "prenom" : client.prenom, 
     "cin" : client.cin, 
     "valeurAchatsRegles" : client.valeurAchatsRegles, 
     "valeurAchats" : client.valeurAchats, 
     "valeurCredits" : client.valeurCredits, 
     "valeurCreditsRegles" : client.valeurCreditsRegles, 
    "adresse" : client.adresse, "ville" : client.ville, 
    "matriculeFiscale" : client.matriculeFiscale, 
    "mobile" : client.mobile,  
    "registreCommerce" : client.registreCommerce,
    "raisonSociale" : client.raisonSociale,
    
    "codePostale" : client.codePostale, "gouvernorat" : client.gouvernorat,
    "image": client.image,
    "userLastUpdatorCode": codeUser,
    "dateLastUpdate": client.dateLastUpdate, 
} ;
       
            db.collection('Client').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
             
      
    
    },
getPageClients : function (db,codeUser,pageNumber,nbElementsPerPage,filter, callback)
    {
        
        
        
        console.log('filter recieved fro mthe client is  :',filter);
        
        let listClients = [];
        let filterObject = JSON.parse(filter);
        
          let filterData = {
              "userCreatorCode" : codeUser
          };
      console.log('filter object  :',filterObject);
      console.log('filter data :',filterData);
      const condition = this.buildFilterCondition(filterObject,filterData);
      console.log('page condition number is :',condition);
      
      
       var cursor =  db.collection('Client').find(
        condition
       ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
       .limit(Number(nbElementsPerPage) );


       cursor.forEach(function(client,err){
           if(err)
            {
                console.log('errors produced :', err);
    
            }
            if(client.responsableCommerciale=== undefined )
                {
                    client.responsableCommerciale = {};
                }
            listClients.push(client);
       }).then(function(){
        console.log('list clients',listClients);
        callback(null,listClients); 
       });

    },
getListClients : function (db,codeUser, callback)
{
    let listClients = [];
     
   var cursor =  db.collection('Client').find(
    {"userCreatorCode" : codeUser}
   );
   cursor.forEach(function(client,err){
       if(err)
        {
            console.log('errors produced :', err);

        }
        if(client.responsableCommerciale=== undefined )
            {
                client.responsableCommerciale = {};
            }
        listClients.push(client);
   }).then(function(){
    console.log('list clients',listClients);
    callback(null,listClients); 
   });
},
deleteClient: function (db,codeClient,codeUser, callback){
    const criteria = { "code" : codeClient, "userCreatorCode":codeUser };
    
            db.collection('Client').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
     
},
getClient: function (db,codeClient,codeUser, callback)
{
    const criteria = { "code" : codeClient, "userCreatorCode":codeUser };
    
  
       const client =  db.collection('Client').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                        
},
// filterClient object of type Client 
buildFilterCondition(filterObject,filterData)
{
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.numeroClient))
        {
                filterData["numeroClient"] = filterObject.numeroClient;
        }
         
        if(!utils.isUndefined(filterObject.nom))
          {
                 
                    filterData["nom"] = filterObject.nom;
                    
         }
         if(!utils.isUndefined(filterObject.prenom))
             {
                    
                       filterData["prenom"] =filterObject.prenom;
                       
            }
            if(!utils.isUndefined(filterObject.cin))
             {
                    
                       filterData["cin"] = filterObject.cin;
                       
            }
            if(!utils.isUndefined(filterObject.raisonSociale))
             {
                    
                       filterData["raisonSociale"] = filterObject.raisonSociale ;
                       
            }
            if(!utils.isUndefined(filterObject.matriculeFiscale))
             {
                    
                       filterData["matriculeFiscale"] = filterObject.matriculeFiscale ;
                       
            }
            if(!utils.isUndefined(filterObject.registreCommerce))
             {
                    
                       filterData["registreCommerce"] = filterObject.registreCommerce ;
                       
            }
            if(!utils.isUndefined(filterObject.mobile))
             {
                    
                       filterData["mobile"] = filterObject.mobile ;
                       
            }
            if(!utils.isUndefined(filterObject.codePostale))
             {
                    
                       filterData["codePostale"] = filterObject.codePostale ;
                       
            }
            if(!utils.isUndefined(filterObject.gouvernorat))
             {
                    
                       filterData["gouvernorat"] = filterObject.gouvernorat ;
                       
            }
            if(!utils.isUndefined(filterObject.ville))
                {
                       
                          filterData["ville"] = filterObject.ville ;
                          
               }
            
     
     
            if(!utils.isUndefined(filterObject.adresse))
             {
                    
                       filterData["adresse"] = filterObject.adresse ;
                       
            }
           
     
        }
        return filterData;
},
getTotalClients: function (db,codeUser,filter, callback)
{
   
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var totalClients=  db.collection('Client').find( 
      condition
    ).count(function(err,result){
        callback(null,result); 
    }
);
}

};

module.exports.Clients = Clients;