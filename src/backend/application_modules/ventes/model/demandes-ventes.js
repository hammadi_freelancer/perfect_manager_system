//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var DemandesVentes = {
addDemandeVentes : function (db,DemandeVentes,codeUser, callback){


    if(DemandeVentes != undefined){
        DemandeVentes.code  = utils.isUndefined(DemandeVentes.code)?shortid.generate():DemandeVentes.code;
       if(utils.isUndefined(DemandeVentes.numeroDemandeVente))
        {
           const currentD =  moment(new Date());
          // const genereKey = shortid.generate();
           const generatedCode = currentD.year().toString() +'/'+currentD.utc(true).month().toString()+currentD.minutes().toString() ;
           DemandeVentes.numeroDemandeVente = 'DEMV' + generatedCode;

           }
           const listRows =   DemandeVentes.listLignesDemandeVentes;
           listRows.forEach((row)=>(row.codeDemandeVentes =DemandeVentes.code, row.numeroDemandeVentes=DemandeVentes.numeroDemandeVente ));
           DemandeVentes.listLignesDemandeVentes  = listRows;
     
           DemandeVentes.dateDemandeVentesObject = utils.isUndefined(DemandeVentes.dateDemandeVentesObject)? 
        new Date():DemandeVentes.dateDemandeVentesObject;
        DemandeVentes.userCreatorCode = codeUser;
        DemandeVentes.userLastUpdatorCode = codeUser;
        DemandeVentes.dateCreation = moment(new Date).format('L');
        DemandeVentes.dateLastUpdate = moment(new Date).format('L');
       
            db.collection('DemandeVentes').insertOne(
                DemandeVentes,function(err,result){
                   
                   // mongoClient.clos
                   if(err){

                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
   

}else{
callback(true,null);
}
},




updateDemandeVentes: function (db,demandeVentes,codeUser, callback){

   
    const listRows =   demandeVentes.listLignesDemandeVentes;
    listRows.forEach((row)=>(row.codeDemandeVentes =demandeVentes.code,
         row.numeroDemandeVente=demandeVentes.numeroDemandeVente ));
         demandeVentes.listLignesDemandeVentes  = listRows;


         demandeVentes.dateDemandeVentesObject = utils.isUndefined(demandeVentes.dateDemandeVentesObject)? 
    new Date():demandeVentes.dateDemandeVentesObject;

    const dateLastUpdate = moment(new Date).format('L');
    const criteria = { "code" : demandeVentes.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"client" : demandeVentes.client,
     "dateDemandeVentesObject" : DemandeVentes.dateDemandeVentesObject ,
     "numeroDemandeVente" : demandeVentes.numeroDemandeVente, 
     "budgetPrepare" : demandeVentes.budgetPrepare, 
     "dateDisponibiliteObject": demandeVentes.dateDisponibiliteObject, 
     "besoinUrgentToutes" : demandeVentes.besoinUrgentToutes,
     "modePayementDesire" : demandeVentes.modePayementDesire , 
      "traiteToutes" : demandeVentes.traiteToutes,
       "etat" : demandeVentes.etat,
       "listLignesDemandeVentes": demandeVentes.listLignesDemandeVentes,
       
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": dateLastUpdate, 

    } ;
       
            db.collection('DemandeVentes').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    console.log('call back is called of update demande vente ')
                    if(err){
                        console.log('call back is called of update demande vente : errer ')
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
           
    
    
    },
getListDemandesVentes : function (db,codeUser, callback)
{
    let listDemandesVentes = [];
    
   var cursor =  db.collection('DemandeVentes').find(
    {"userCreatorCode" : codeUser}
   );
   
   cursor.forEach(function(DemandeVente,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        DemandeVente.dateDemandeVentes = moment(DemandeVente.dateDemandeVentesObject).format('L');
 listDemandesVentes.push(DemandeVente);
   }).then(function(){
    // console.log('list factures ventes ',listFacturesVentes);
    callback(null,listDemandesVentes); 
   });
},
deleteDemandeVentes: function (db,codeDemandeVentes,codeUser, callback){
    const criteria = { "code" : codeDemandeVentes, "userCreatorCode":codeUser };
            db.collection('DemandeVentes').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;   
},

getDemandeVentes: function (db,codeDemandeVentes,codeUser, callback)
{
    const criteria = { "code" : codeDemandeVentes, "userCreatorCode":codeUser };
    
 
       const DemandeVente =  db.collection('DemandeVentes').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                      
},

getTotalDemandesVentes: function (db,codeUser,filter, callback)
{
    let filterObject = JSON.parse(filter);
    let filterData = {
        "userCreatorCode" : codeUser
    };

const condition = this.buildFilterCondition(filterObject,filterData);
  
   var totalDemandesVentes =  db.collection('DemandeVentes').find( 
    condition
    ).count(function(err,result){
        callback(null,result); 
    }
);

},
buildFilterCondition(filterObject,filterData)
{
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.numeroDemandeVentes))
        {
                filterData["numeroDemandeVente"] = filterObject.numeroDemandeVentes;
        }
         
        if(!utils.isUndefined(filterObject.nom))
          {
                 
                    filterData["nom"] = filterObject.nom;
                    
         }
         if(!utils.isUndefined(filterObject.prenom))
             {
                    
                       filterData["prenom"] =filterObject.prenom;
                       
            }
            if(!utils.isUndefined(filterObject.cin))
             {
                    
                       filterData["cin"] = filterObject.cin;
                       
            }
            if(!utils.isUndefined(filterObject.raisonSociale))
             {
                    
                       filterData["raisonSociale"] = filterObject.raisonSociale ;
                       
            }
            if(!utils.isUndefined(filterObject.matriculeFiscale))
             {
                    
                       filterData["matriculeFiscale"] = filterObject.matriculeFiscale ;
                       
            }
            if(!utils.isUndefined(filterObject.registreCommerce))
             {
                    
                       filterData["registreCommerce"] = filterObject.registreCommerce ;
                       
            }
            if(!utils.isUndefined(filterObject.dateDemandeVentesFromObject))
             {
                    
                       filterData["dateDemandeVentesObject"] = {$gt: filterObject.dateDemandeVentesFromObject};
                       
            }
            if(!utils.isUndefined(filterObject.dateDemandeVentesToObject))
             {
                    
                       filterData["dateDemandeVentesToObject"] = {$lt: filterObject.dateDemandeVentesToObject};
                       
            }
            
            if(!utils.isUndefined(filterObject.dateDisponibiliteFromObject))
                {
                       
                          filterData["dateDisponibiliteObject"] = {$gt: filterObject.dateDisponibiliteFromObject};
                          
               }
               if(!utils.isUndefined(filterObject.dateDisponibiliteToObject))
                {
                       
                          filterData["dateDisponibiliteObject"] = {$lt: filterObject.dateDisponibiliteToObject};
                          
               }
            if(!utils.isUndefined(filterObject.budgetPrepare))
             {
                    
                       filterData["budgetPrepare"] = filterObject.budgetPrepare ;
                       
            }
            if(!utils.isUndefined(filterObject.modePayementDesire))
                {
                       
                          filterData["modePayementDesire"] = filterObject.modePayementDesire ;
                          
                }
     
            if(!utils.isUndefined(filterObject.besoinUrgentToutes))
             {
                    
                       filterData["besoinUrgentToutes"] = filterObject.besoinUrgentToutes ;
                       
            }
            if(!utils.isUndefined(filterObject.traiteToutes))
                {
                       
                          filterData["traiteToutes"] = filterObject.traiteToutes ;
                          
               }
               if(!utils.isUndefined(filterObject.etat))
                {
                       
                          filterData["etat"] = filterObject.etat ;
                          
               }
              
     
        }
        return filterData;
},
getPageDemandesVentes : function (db,codeUser,pageNumber,nbElementsPerPage,filter, callback)
{
    let listDemandesVentes = [];
    let filterObject = JSON.parse(filter);
      let filterData = {
          "userCreatorCode" : codeUser
      };

  const condition = this.buildFilterCondition(filterObject,filterData);

  
   var cursor =  db.collection('DemandeVentes').find(
    condition
   ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
   .limit(Number(nbElementsPerPage) );
   
   cursor.forEach(function(DemandeVente,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        DemandeVente.dateDemandeVentes = moment(DemandeVente.dateDemandeVentesObject).format('L');
 listDemandesVentes.push(DemandeVente);
   }).then(function(){
    // console.log('list factures ventes ',listFacturesVentes);
    callback(null,listDemandesVentes); 
   });
},

}
module.exports.DemandesVentes = DemandesVentes;