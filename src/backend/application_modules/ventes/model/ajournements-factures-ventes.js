//var db = require('../common/MongoDbConnection').getDBConnection;
const mongoClient = require('mongodb').MongoClient;
const url ='mongodb://localhost:27017';
const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var AjournementsFacturesVentes = {
addAjournementFactureVentes : function (db,ajournementFactureVentes,codeUser,callback){
    if(ajournementFactureVentes != undefined){
        ajournementFactureVentes.code  = utils.isUndefined(ajournementFactureVentes.code)?shortid.generate():ajournementFactureVentes.code;
        ajournementFactureVentes.dateReceptionObject = utils.isUndefined(ajournementFactureVentes.dateReceptionObject)? 
        new Date():ajournementFactureVentes.dateReceptionObject;
        ajournementFactureVentes.userCreatorCode = codeUser;
        ajournementFactureVentes.userLastUpdatorCode = codeUser;
        ajournementFactureVentes.dateCreation = moment(new Date).format('L');
        ajournementFactureVentes.dateLastUpdate = moment(new Date).format('L');
        
       
            db.collection('AjournementFactureVentes').insertOne(
                ajournementFactureVentes,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,ajournementFactureVentes);
                   }
                }
              ) ;
             // db.close();
             
    

}else{
callback(true,null);
}
},
updateAjournementFactureVentes: function (db,ajournementFactureVentes,codeUser, callback){
    
       
         ajournementFactureVentes.dateOperationObject = utils.isUndefined(ajournementFactureVentes.dateOperationObject)? 
        new Date():ajournementFactureVentes.dateOperationObject;
        ajournementFactureVentes.dateLastUpdate = moment(new Date).format('L');
        
        const criteria = { "code" : ajournementFactureVentes.code,"userCreatorCode" : codeUser };
        const dataToUpdate = {
        "codeFactureVentes" : ajournementFactureVentes.codeFactureVentes, 
        "description" : ajournementFactureVentes.description,
         "codeOrganisation" : ajournementFactureVentes.codeOrganisation,
        "dateOperationObject" : ajournementFactureVentes.dateOperationObject ,
        "nouvelleDatePayementObject" : ajournementFactureVentes.nouvelleDatePayementObject ,
        "motif" : ajournementFactureVentes.motif ,
           "userLastUpdatorCode": codeUser,
           "dateLastUpdate": ajournementFactureVentes.dateLastUpdate, 
        } ;
           
                db.collection('AjournementFactureVentes').updateOne(
                    criteria,
                    { 
                        $set: dataToUpdate
                    },
                    function(err,result){
                        if(err){
                            
                            callback(null,null);
                        }else{
                            
                            callback(false,result);
                        }
                    }  
                ) ;
          
        
        },
    getListAjournementsFacturesVentes : function (db,codeUser,callback)
    {
        let listAjournementsFacturesVentes = [];
       
       var cursor =  db.collection('AjournementFactureVentes').find( 
           {"userCreatorCode" : codeUser}
        );
       cursor.forEach(function(ajournementFactVentes,err){
           if(err)
            {
                console.log('errors produced :', err);
            }
            ajournementFactVentes.dateOperation = moment(ajournementFactVentes.dateOperationObject).format('L');
            ajournementFactVentes.nouvelleDatePayement = moment(ajournementFactVentes.nouvelleDatePayementObject).format('L');
            
            listAjournementsFacturesVentes.push(ajournementFactVentes);
       }).then(function(){
       //  console.log('list payements credits',listPayementsCredits);
        callback(null,listAjournementsFacturesVentes); 
       });
       
   
         //return [];
    },
    deleteAjournementFactureVentes: function (db,codeAjournementFactureVentes,codeUser,callback){
        const criteria = { "code" : codeAjournementFactureVentes, "userCreatorCode":codeUser };
           
                db.collection('AjournementFactureVentes').deleteOne(
                    criteria ,
                    function(err,result){
                        if(err){
                            
                            callback(null,null);
                        }else{
                            
                            callback(false,result);
                        }
                    }  
                ) ;
           
    },
    
    getAjournementFactureVentes: function (db,codeAjournementFactureVentes,codeUser,callback)
    {
        const criteria = { "code" : codeAjournementFactureVentes, "userCreatorCode":codeUser };
       
           const ajournmentFactVentes =  db.collection('AjournementFactureVentes').findOne(
                criteria , function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }
            ) ;
                      
    },
    
    
    getListAjournementsFacturesVentesByCodeFactureVentes : function (db,codeUser,codeFactureVentes,callback)
    {
        let listAjournementsFacturesVentes = [];
       
       var cursor =  db.collection('AjournementFactureVentes').find( 
           {"userCreatorCode" : codeUser, "codeFactureVente": codeFactureVentes}
        );
       cursor.forEach(function(ajournementFactVentes,err){
           if(err)
            {
                console.log('errors produced :', err);
            }
            ajournementFactVentes.dateOperation = moment(ajournementFactVentes.dateOperationObject).format('L');
            ajournementFactVentes.nouvelleDatePayement = moment(ajournementFactVentes.nouvelleDatePayementObject).format('L');
            
            listAjournementsFacturesVentes.push(ajournementFactVentes);
       }).then(function(){
       // console.log('list credits',listCredits);
        callback(null,listAjournementsFacturesVentes); 
       });
       
  
         //return [];
    },
    
    
    }
    module.exports.AjournementsFacturesVentes = AjournementsFacturesVentes;