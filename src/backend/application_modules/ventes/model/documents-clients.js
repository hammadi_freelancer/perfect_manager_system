//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var DocumentsClients = {
addDocumentClient : function (db,documentClient,codeUser,callback){
    if(documentClient != undefined){
        documentClient.code  = utils.isUndefined(documentClient.code)?shortid.generate():documentClient.code;
        documentClient.dateReceptionObject = utils.isUndefined(documentClient.dateReceptionObject)? 
        new Date():documentClient.dateReceptionObject;
        documentClient.userCreatorCode = codeUser;
        documentClient.userLastUpdatorCode = codeUser;
        documentClient.dateCreation = moment(new Date).format('L');
        documentClient.dateLastUpdate = moment(new Date).format('L');
        
       
            db.collection('DocumentClient').insertOne(
                documentClient,function(err,result){
                   // clientDB.close();
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,documentClient);
                   }
                }
              ) ;
   

}else{
callback(true,null);
}
},
updateDocumentClient: function (db,documentClient,codeUser, callback){

   
    documentClient.dateReceptionObject = utils.isUndefined(documentClient.dateReceptionObject)? 
    new Date():documentClient.dateReceptionObject;

    const criteria = { "code" : documentClient.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"dateReceptionObject" : documentClient.dateReceptionObject, 
    "codeClient" : documentClient.codeClient, 
    "description" : documentClient.description,
     "codeOrganisation" : documentClient.codeOrganisation,
     "typeDocument" : documentClient.typeDocument, // cheque , virement , espece 
     "numeroDocument" : documentClient.numeroDocument, 
     "imageFace1":documentClient.imageFace1,
     "imageFace2" : documentClient.imageFace2 , 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": documentClient.dateLastUpdate, 
    } ;
     
            db.collection('DocumentClient').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
             //clientDB.close();
      
    
    },
getListDocumentsClients : function (db,codeUser,callback)
{
    let listDocumentsClients = [];
    
   var cursor =  db.collection('DocumentClient').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(docClient,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        docClient.dateReception = moment(docClient.dateReceptionObject).format('L');
        
        listDocumentsClients.push(docClient);
   }).then(function(){
    callback(null,listDocumentsClients); 
   });
   

     //return [];
},
deleteDocumentClient: function (db,codeDocumentClient,codeUser,callback){
    const criteria = { "code" : codeDocumentClient, "userCreatorCode":codeUser };
       
            db.collection('DocumentClient').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
            
      
},

getDocumentClient: function (db,codeDocumentClient,codeUser,callback)
{
    const criteria = { "code" : codeDocumentClient, "userCreatorCode":codeUser };
   
       const docClient =  db.collection('DocumentClient').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                     
},


getListDocumentsClientsByCodeClient : function (db,codeUser,codeClient,callback)
{
    let listDocumentsClients = [];
    
   var cursor =  db.collection('DocumentClient').find( 
       {"userCreatorCode" : codeUser, "codeClient": codeClient}
    );
   cursor.forEach(function(documentClient,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        documentClient.dateReception = moment(documentClient.dateReceptionObject).format('L');
        
        listDocumentsClients.push(documentClient);
   }).then(function(){
  // console.log('list doc credits',listCredits);
    callback(null,listDocumentsClients); 
   });

},


}
module.exports.DocumentsClients = DocumentsClients;