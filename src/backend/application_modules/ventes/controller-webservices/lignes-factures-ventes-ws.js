
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var lignesFactureVentes = require('../model/lignes-factures-ventes').LignesFactureVentes;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addLigneFactureVentes',function(req,res,next){
    
       console.log(" ADD LIGNE FACTURE VENTES  IS CALLED") ;
       console.log(" THE LIGNE FACTURE VENTES  TO ADDED IS :",req.body);
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = lignesFactureVentes.addLigneFactureVentes(req.app.locals.dataBase,req.body,decoded.userData.code,function(err,ligneFVAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_LIGNE_FACTURE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(ligneFVAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateLigneFactureVentes',function(req,res,next){
    
       console.log(" UPDATE LIGNE FACTURE VENTES  IS CALLED") ;
       // console.log(" THE DOCUMENT CREDIT  TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result =lignesFactureVentes.updateLignesFactureVentes(req.app.locals.dataBase,req.body,decoded.userData.code, function(err,ligneFVUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_LIGNE_FACTURE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(ligneFVUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getLignesFactureVentes',function(req,res,next){
 
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    lignesFactureVentes.getListLignesFactureVentes(req.app.locals.dataBase,decoded.userData.code, function(err,listLignesFVentes){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_LIGNES_FACTURE_VENTES',
                 error_content: err
             });
          }else{
        
         return res.json(listLignesFVentes);
          }
      });

  });
  router.get('/getLigneFactureVentes/:codeLigneFactureVentes',function(req,res,next){
    console.log("GET LIGNE FACT VENTES IS CALLED");
   
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    lignesFactureVentes.getLigneFactureVentes(req.app.locals.dataBase,req.params['codeLigneFactureVentes'],decoded.userData.code,
     function(err,ligneFVentes){
      // console.log("GET  DOCUMENT CREDIT : RESULT");
       //console.log(documentCredit);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIGNE_FACTURE_VENTES',
               error_content: err
           });
        }else{
      
       return res.json(ligneFVentes);
        }
    });
})

  router.delete('/deleteLigneFactureVentes/:codeLigneFactureVentes',function(req,res,next){
    
       console.log(" DELETE LIGNE FACTURE VENTES  IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = lignesFactureVentes.deleteLigneFactureVentes(req.app.locals.dataBase,req.params['codeLigneFactureVentes'],decoded.userData.code,
        function(err,codeLigneFactureVentes){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_LIGNE_FACTURE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeLigneFactureVentes']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getLignesFactureVentesByCodeFactureVentes/:codeFactureVentes',function(req,res,next){
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    lignesFactureVentes.getListLignesFactureVentesByCodeFactureVentes(req.app.locals.dataBase,decoded.userData.code,req.params['codeFactureVentes'],
         function(err,listLignesFVentes){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_LIGNES_FACTURE_VENTES',
                 error_content: err
             });
          }else{
        
         return res.json(listLignesFVentes);
          }
      });

  });
  module.exports.routerLignesFactureVentes = router;