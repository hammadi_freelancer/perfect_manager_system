
var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var demandesVentes = require('../model/demandes-ventes').DemandesVentes;
// var commonManager = require('../application_modules/common/main').CommonManager;
var jwt = require('jsonwebtoken');

router.post('/addDemandeVentes',function(req,res,next){
    
    console.log(" ADD DEMANDE VENTE IS CALLED") ;
       // console.log(" THE FACTURE VENTE TO ADDED IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = demandesVentes.addDemandeVentes(req.app.locals.dataBase,req.body,decoded.userData.code, function(err,relVenteAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_DEMANDE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(relVenteAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateDemandeVentes',function(req,res,next){
    console.log(" UPDATE DEMANDE VENTE IS CALLED") ;
    
      
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = demandesVentes.updateDemandeVentes(req.app.locals.dataBase,req.body,decoded.userData.code, function(err,demandeVenteUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_DEMANDE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(demandeVenteUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getPageDemandesVentes',function(req,res,next){
     //  console.log("GET LIST FACTURES VENTES IS CALLED");
      var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    demandesVentes.getPageDemandeVentes(req.app.locals.dataBase,decoded.userData.code,
        req.query.pageNumber, req.query.nbElementsPerPage ,req.query.filter, function(err,pageDemandesVentes){
         // console.log("GET LIST FACTURES VENTES : RESULT");
        //  console.log(listFacturesVentes);
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_PAGE_DEMANDES_VENTES',
                 error_content: err
             });
          }else{
        
         return res.json(pageDemandesVentes);
          }
      });

  });
  router.get('/getDemandeVentes/:codeDemandeVentes',function(req,res,next){
   
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    demandesVentes.getDemandeVentes(req.app.locals.dataBase,req.params['codeDemandeVentes'],decoded.userData.code, function(err,demandeVentes){
      //  console.log("GET  FACTURE VENTE : RESULT");
     //  console.log(factureVente);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_DEMANDE_VENTES',
               error_content: err
           });
        }else{
      
       return res.json(demandeVentes);
        }
    });
})

  router.delete('/deleteDemandeVentes/:codeDemandeVentes',function(req,res,next){
    
       console.log(" DELETE DEMANDE VENTE IS CALLED") ;
       console.log(" THE DEMANDE VENTE  TO DELETE IS :",req.params['codeDemandeVentes']);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = demandesVentes.deleteDemandeVentes(req.app.locals.dataBase,req.params['codeDemandeVentes'],decoded.userData.code,function(err,codeDemandeVentes){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_DEMANDE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeDemandeVentes']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.get('/getTotalDemandesVentes',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    demandesVentes.getTotalDemandesVentes(req.app.locals.dataBase,decoded.userData.code,req.query.filter,
        function(err,totalDemandesVentes){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_DEMANDES_VENTES',
                 error_content: err
             });
          }else{
        
         return res.json({'totalDemandesVentes':totalDemandesVentes});
          }
      });

  });
  module.exports.routerDemandesVentes = router;