
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var documentsFacturesVentes = require('../model/documents-factures-ventes').DocumentsFacturesVentes;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addDocumentFactureVentes',function(req,res,next){
    
       console.log(" ADD DOCUMENT FACTURE VENTES  IS CALLED") ;
       console.log(" THE DOCUMENT FACTURE VENTES  TO ADDED IS :",req.body);
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = documentsFacturesVentes.addDocumentFactureVentes(req.app.locals.dataBase,req.body,decoded.userData.code,function(err,documentFVAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_DOCUMENT_FACTURE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(documentFVAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateDocumentFactureVentes',function(req,res,next){
    
       console.log(" UPDATE DOCUMENT FACTURE VENTES  IS CALLED") ;
       // console.log(" THE DOCUMENT CREDIT  TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result =documentsFacturesVentes.updateDocumentFactureVentes(req.app.locals.dataBase,req.body,decoded.userData.code, function(err,documentFVUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_DOCUMENT_FACTURE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(documentFVUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getDocumentsFacturesVentes',function(req,res,next){
 
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    documentsFacturesVentes.getListDocumentsFacturesVentes(req.app.locals.dataBase,decoded.userData.code, function(err,listDocumentsFVentes){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_FACTURES_VENTES',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsFVentes);
          }
      });

  });
  router.get('/getDocumentFactureVentes/:codeDocumentFactureVentes',function(req,res,next){
    console.log("GET DOCUMENT FACT VENTES IS CALLED");
   
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    documentsFacturesVentes.getDocumentFactureVentes(req.app.locals.dataBase,req.params['codeDocumentFactureVentes'],decoded.userData.code,
     function(err,documentFVentes){
      // console.log("GET  DOCUMENT CREDIT : RESULT");
       //console.log(documentCredit);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_DOCUMENT_FACTURE_VENTES',
               error_content: err
           });
        }else{
      
       return res.json(documentFVentes);
        }
    });
})

  router.delete('/deleteDocumentFactureVentes/:codeDocumentFactureVentes',function(req,res,next){
    
       console.log(" DELETE DOCUMENT FACTURE VENTES  IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = documentsFacturesVentes.deleteDocumentFactureVentes(req.app.locals.dataBase,req.params['codeDocumentFactureVentes'],decoded.userData.code,
        function(err,codeDocumentFactureVentes){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_DOCUMENT_FACTURE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeDocumentFactureVentes']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getDocumentsFactureVentesByCodeFactureVentes/:codeFactureVentes',function(req,res,next){
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    documentsFacturesVentes.getListDocumentsFacturesVentesByCodeFactureVentes(req.app.locals.dataBase,decoded.userData.code,req.params['codeFactureVentes'],
         function(err,listDocumentFVentes){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_FACTURES_VENTES',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentFVentes);
          }
      });

  });
  module.exports.routerDocumentsFacturesVentes = router;