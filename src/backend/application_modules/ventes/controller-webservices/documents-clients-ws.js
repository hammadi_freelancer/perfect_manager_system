
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var documentsClients = require('../model/documents-clients').DocumentsClients;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addDocumentClient',function(req,res,next){
    
       console.log(" ADD DOCUMENT CLIENT  IS CALLED") ;
       console.log(" THE DOCUMENT CLIENT  TO ADDED IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = documentsClients.addDocumentClient(req.app.locals.dataBase,req.body,decoded.userData.code,function(err,documentClientAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_DOCUMENT_CLIENT',
                error_content: err
            });
         }else{
       
        return res.json(documentClientAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateDocumentClient',function(req,res,next){
    
       console.log(" UPDATE DOCUMENT CLIENT  IS CALLED") ;
       // console.log(" THE DOCUMENT CREDIT  TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result =documentsClients.updateDocumentClient(req.app.locals.dataBase,req.body,decoded.userData.code, function(err,documentClientUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_DOCUMENT_CLIENT',
                error_content: err
            });
         }else{
       
        return res.json(documentClientUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getDocumentsClients',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    documentsClients.getListDocumentsClients(req.app.locals.dataBase,decoded.userData.code, function(err,listDocumentsClients){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_CLIENTS',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsClients);
          }
      });

  });
  router.get('/getDocumentClient/:codeDocumentClient',function(req,res,next){
    console.log("GET DOCUMENT CLIENT  IS CALLED");
    console.log(req.params['codeDocumentClient']);
   
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    documentsClients.getDocumentClient(req.app.locals.dataBase,req.params['codeDocumentClient'],decoded.userData.code,
     function(err,documentClient){
       console.log("GET  DOCUMENT CLIENT: RESULT");
       console.log(documentClient);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_DOCUMENT_CLIENT',
               error_content: err
           });
        }else{
      
       return res.json(documentClient);
        }
    });
})

  router.delete('/deleteDocumentClient/:codeDocumentClient',function(req,res,next){
    
       console.log(" DELETE DOCUMENT CLIENT  IS CALLED") ;
       console.log(" THE DOCUMENT CLIENT  TO DELETE IS :",req.params['codeDocumentClient']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = documentsClients.deleteDocumentClient(req.app.locals.dataBase,req.params['codeDocumentClient'],decoded.userData.code,
        function(err,codeDocumentClient){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_DOCUMENT_CLIENT',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeDocumentClient']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getDocumentsClientsByCodeClient/:codeClient',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    documentsClients.getListDocumentsClientsByCodeClient(req.app.locals.dataBase,decoded.userData.code,req.params['codeClient'],
         function(err,listDocumentClients){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_CLIENTS_BY_CODE_CLIENT',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentClients);
          }
      });

  });
  module.exports.routerDocumentsClients = router;