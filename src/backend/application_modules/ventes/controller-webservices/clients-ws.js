

var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var clients = require('../model/clients').Clients;
var jwt = require('jsonwebtoken');

router.post('/addClient',function(req,res,next){
    
       console.log(" ADD CLIENT IS CALLED") ;
       console.log(" THE CLIENT TO ADDED IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = clients.addClient(req.app.locals.dataBase,req.body,decoded.userData.code,function(err,clientAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_CLIENT',
                error_content: err
            });
         }else{
       
        return res.json(clientAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.put('/updateClient',function(req,res,next){
    
       console.log(" UPDATE CLIENT IS CALLED") ;
       console.log(" THE CLIENT TO UPDATE IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = clients.updateClient(req.app.locals.dataBase,req.body,decoded.userData.code,function(err,clientUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_CLIENT',
                error_content: err
            });
         }else{
       
        return res.json(clientUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getClients',function(req,res,next){
    console.log("GET LIST CLIENTS IS CALLED");
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    clients.getListClients(req.app.locals.dataBase,decoded.userData.code ,
        function(err,listClients){
       console.log("GET LIST CLIENTS : RESULT");
       console.log(listClients);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_CLIENTS',
               error_content: err
           });
        }else{
      
       return res.json(listClients);
        }
    });
});
router.get('/getPageClients',function(req,res,next){
    console.log("GET LIST CLIENTS IS CALLED");
    console.log("PAGE PARAMS",req.query.pageNumber+''+ req.query.nbElementsPerPage);
    
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    clients.getPageClients(req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
      req.query.filter,  function(err,listClients){
       console.log("GET LIST CLIENTS : RESULT");
       console.log(listClients);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_PAGE_CLIENTS',
               error_content: err
           });
        }else{
      
       return res.json(listClients);
        }
    });
});
router.get('/getClient/:codeClient',function(req,res,next){
    console.log("GET CLIENT IS CALLED");
    console.log(req.params['codeClient']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    clients.getClient(req.app.locals.dataBase,req.params['codeClient'],decoded.userData.code, function(err,client){
       console.log("GET  CLIENT : RESULT");
       console.log(client);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_CLIENT',
               error_content: err
           });
        }else{
      
       return res.json(client);
        }
    });
})
router.post('/getFiltredClients',function(req,res,next){
    console.log("GET FILTRED LIST CLIENTS IS CALLED");
    clients.getFiltredListClients(req.app.locals.dataBase,req.body,function(err,listFiltredClients){
       console.log("GET FILTRED LIST CLIENTS : RESULT");
       console.log(listFiltredClients);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_FILTRED_CLIENTS',
               error_content: err
           });
        }else{
      
       return res.json(listFiltredClients);
        }
    });
})
router.delete('/deleteClient/:codeClient',function(req,res,next){
    
       console.log(" DELETE CLIENT IS CALLED") ;
       console.log(" THE CLIENT TO DELETE IS :",req.params['codeClient']);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = clients.deleteClient(req.app.locals.dataBase,req.params['codeClient'],decoded.userData.code, function(err,codeClient){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_CLIENT',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeClient']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.get('/getTotalClients',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    clients.getTotalClients(req.app.locals.dataBase,decoded.userData.code,
        req.query.filter, function(err,totalClients){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_CLIENTS',
                 error_content: err
             });
          }else{
        
         return res.json({'totalClients':totalClients});
          }
      });

  });
module.exports.clientsRouter = router;