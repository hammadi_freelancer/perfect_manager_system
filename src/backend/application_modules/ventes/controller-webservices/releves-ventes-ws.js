
var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var relevesVentes = require('../model/releves-ventes').RelevesVentes;
// var commonManager = require('../application_modules/common/main').CommonManager;
var jwt = require('jsonwebtoken');

router.post('/addReleveVentes',function(req,res,next){
    
       // console.log(" ADD FACTURE VENTE IS CALLED") ;
       // console.log(" THE FACTURE VENTE TO ADDED IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = relevesVentes.addReleveVentes(req.app.locals.dataBase,req.body,decoded.userData.code, function(err,relVenteAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_RELEVE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(relVenteAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateReleveVentes',function(req,res,next){
    
      
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = relevesVentes.updateReleveVentes(req.app.locals.dataBase,req.body,decoded.userData.code, function(err,releveVenteUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_RELEVE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(releveVenteUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getPageRelevesVentes',function(req,res,next){
     //  console.log("GET LIST FACTURES VENTES IS CALLED");
      var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    relevesVentes.getListRelevesVentes(req.app.locals.dataBase,decoded.userData.code,
        req.query.pageNumber, req.query.nbElementsPerPage ,req.query.filter,
         function(err,pageRelevesVentes){
         // console.log("GET LIST FACTURES VENTES : RESULT");
        //  console.log(listFacturesVentes);
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_PAGE_RELEVES_VENTES',
                 error_content: err
             });
          }else{
        
         return res.json(pageRelevesVentes);
          }
      });

  });
  router.get('/getReleveVentes/:codeReleveVentes',function(req,res,next){
   
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    relevesVentes.getReleveVentes(req.app.locals.dataBase,req.params['codeReleveVentes'],decoded.userData.code, function(err,releveVentes){
      //  console.log("GET  FACTURE VENTE : RESULT");
     //  console.log(factureVente);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_RELEVE_VENTES',
               error_content: err
           });
        }else{
      
       return res.json(releveVentes);
        }
    });
})

  router.delete('/deleteReleveVentes/:codeReleveVentes',function(req,res,next){
    
       console.log(" DELETE RELEVE VENTE IS CALLED") ;
       console.log(" THE RELEVE VENTE  TO DELETE IS :",req.params['codeReleveVentes']);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = relevesVentes.deleteReleveVentes(req.app.locals.dataBase,req.params['codeReleveVentes'],decoded.userData.code,function(err,codeReleveVentes){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_RELEVE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeReleveVentes']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.get('/getTotalRelevesVentes',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    relevesVentes.getTotalRelevesVentes(req.app.locals.dataBase,decoded.userData.code,req.query.filter,
        function(err,totalRelevesVentes){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_RELEVES_VENTES',
                 error_content: err
             });
          }else{
        
         return res.json({'totalRelevesVentes':totalRelevesVentes});
          }
      });

  });
  module.exports.routerRelevesVentes = router;