
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var documentsDemandesVentes = require('../model/documents-demandes-ventes').DocumentsDemandesVentes;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addDocumentDemandeVentes',function(req,res,next){
    
       console.log(" ADD DOCUMENT DEMANDE VENTES  IS CALLED") ;
       console.log(" THE DOCUMENT DEMANDE VENTES  TO ADDED IS :",req.body);
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = documentsDemandesVentes.addDocumentDemandeVentes(req.app.locals.dataBase,req.body,decoded.userData.code,function(err,documentRVAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_DOCUMENT_DEMANDE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(documentRVAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateDocumentDemandeVentes',function(req,res,next){
    
       console.log(" UPDATE DOCUMENT DEMANDE VENTES  IS CALLED") ;
       // console.log(" THE DOCUMENT CREDIT  TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result =documentsDemandesVentes.updateDocumentDemandeVentes(req.app.locals.dataBase,req.body,decoded.userData.code, function(err,documentRVUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_DOCUMENT_DEMANDE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(documentRVUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getDocumentsDemandesVentes',function(req,res,next){
 
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    documentsDemandesVentes.getListDocumentsDemandesVentes(req.app.locals.dataBase,decoded.userData.code, function(err,listDocumentsRVentes){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_DEMANDES_VENTES',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsRVentes);
          }
      });

  });
  router.get('/getDocumentDemandeVentes/:codeDocumentDemandeVentes',function(req,res,next){
    console.log("GET DOCUMENT REL VENTES IS CALLED");
   
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    documentsDemandesVentes.getDocumentDemandeVentes(req.app.locals.dataBase,req.params['codeDocumentDemandeVentes'],decoded.userData.code,
     function(err,documentRVentes){
      // console.log("GET  DOCUMENT CREDIT : RESULT");
       //console.log(documentCredit);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_DOCUMENT_DEMANDE_VENTES',
               error_content: err
           });
        }else{
      
       return res.json(documentRVentes);
        }
    });
})

  router.delete('/deleteDocumentDemandeVentes/:codeDocumentDemandeVentes',function(req,res,next){
    
       console.log(" DELETE DOCUMENT DEMANDE VENTES  IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = documentsDemandesVentes.deleteDocumentDemandeVentes(req.app.locals.dataBase,req.params['codeDocumentDemandeVentes'],decoded.userData.code,
        function(err,codeDocumentDemandeVentes){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_DOCUMENT_DEMANDE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeDocumentDemandeVentes']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getDocumentsDemandesVentesByCodeDemande/:codeDemande',function(req,res,next){
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    documentsDemandesVentes.getListDocumentsDemandesVentes(req.app.locals.dataBase,decoded.userData.code,req.params['codeDemande'],
         function(err,listDocumentsRVentes){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_DEMANDES_VENTES',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsRVentes);
          }
      });

  });
  module.exports.routerDocumentsDemandesVentes = router;