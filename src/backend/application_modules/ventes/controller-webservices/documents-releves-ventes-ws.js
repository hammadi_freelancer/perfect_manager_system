
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var documentsRelevesVentes = require('../model/documents-releves-ventes').DocumentsRelevesVentes;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addDocumentReleveVentes',function(req,res,next){
    
       console.log(" ADD DOCUMENT RELEVE VENTES  IS CALLED") ;
       console.log(" THE DOCUMENT RELEVE VENTES  TO ADDED IS :",req.body);
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = documentsRelevesVentes.addDocumentReleveVentes(req.app.locals.dataBase,req.body,decoded.userData.code,function(err,documentRVAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_DOCUMENT_RELEVE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(documentRVAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateDocumentReleveVentes',function(req,res,next){
    
       console.log(" UPDATE DOCUMENT RELEVE VENTES  IS CALLED") ;
       // console.log(" THE DOCUMENT CREDIT  TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result =documentsRelevesVentes.updateDocumentReleveVentes(req.app.locals.dataBase,req.body,decoded.userData.code, function(err,documentRVUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_DOCUMENT_RELEVE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(documentRVUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getDocumentsRelevesVentes',function(req,res,next){
 
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    documentsRelevesVentes.getListDocumentsRelevesVentes(req.app.locals.dataBase,decoded.userData.code, function(err,listDocumentsRVentes){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_RELEVES_VENTES',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsRVentes);
          }
      });

  });
  router.get('/getDocumentReleveVentes/:codeDocumentReleveVentes',function(req,res,next){
    console.log("GET DOCUMENT REL VENTES IS CALLED");
   
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    documentsRelevesVentes.getDocumentReleveVentes(req.app.locals.dataBase,req.params['codeDocumentReleveVentes'],decoded.userData.code,
     function(err,documentRVentes){
      // console.log("GET  DOCUMENT CREDIT : RESULT");
       //console.log(documentCredit);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_DOCUMENT_RELEVE_VENTES',
               error_content: err
           });
        }else{
      
       return res.json(documentRVentes);
        }
    });
})

  router.delete('/deleteDocumentReleveVentes/:codeDocumentReleveVentes',function(req,res,next){
    
       console.log(" DELETE DOCUMENT RELEVE VENTES  IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = documentsRelevesVentes.deleteDocumentReleveVentes(req.app.locals.dataBase,req.params['codeDocumentReleveVentes'],decoded.userData.code,
        function(err,codeDocumentReleveVentes){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_DOCUMENT_RELEVE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeDocumentReleveVentes']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getDocumentsReleveVentesByCodeReleve/:codeReleve',function(req,res,next){
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    documentsRelevesVentes.getListDocumentsReleveVentesByCodeReleve(req.app.locals.dataBase,decoded.userData.code,req.params['codeReleve'],
         function(err,listDocumentsRVentes){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_RELEVES_VENTES',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsRVentes);
          }
      });

  });
  module.exports.routerDocumentsRelevesVentes = router;