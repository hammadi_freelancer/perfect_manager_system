
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var ajournementsRelevesVentes = require('../model/ajournements-releves-ventes').AjournementsRelevesVentes;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addAjournementReleveVentes',function(req,res,next){
    
       console.log(" ADD AJOURNEMENT REL VENTES  IS CALLED") ;
       console.log(" THE AJOURNEMENT REL VENTES  TO ADDED IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = ajournementsRelevesVentes.addAjournementReleveVentes(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,ajournementRelAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_AJOURNEMENT_RELEVES_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(ajournementRelAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateAjournementReleveVentes',function(req,res,next){
    
       console.log(" UPDATE AJOURNEMENT REL VENTES  IS CALLED") ;
       //console.log(" THE AJOURNEMENT FACT VENTES  TO UPDATE IS :",req.body);
      /* console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));*/
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));  
       var result = ajournementsRelevesVentes.updateAjournementReleveVentes(
        req.app.locals.dataBase,req.body,decoded.userData.code,
         function(err,ajournementRelVentes){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_AJOURNEMENT_RELEVE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(ajournementsRelevesVentes);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getAjournementsRelevesVentes',function(req,res,next){
    /*console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));*/
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    ajournementsRelevesVentes.getListAjournementsRelevesVentes(
        req.app.locals.dataBase,decoded.userData.code, function(err,listAjournementsRelVentes){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_AJOURNEMENTS_RELEVES_VENTES',
                 error_content: err
             });
          }else{
        
         return res.json(listAjournementsRelVentes);
          }
      });

  });
  router.get('/getAjournementReleveVentes/:codeAjournementReleveVentes',function(req,res,next){
    console.log("GET AJOURNEMENT RELEVE VENTES  IS CALLED");
    console.log(req.params['codeAjournementReleveVentes']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    ajournementsRelevesVentes.getAjournementReleveVentes(
        req.app.locals.dataBase,req.params['codeAjournementReleveVentes'],decoded.userData.code,
     function(err,ajournementRelVentes){
       //console.log("GET  AJOURNEMENT CREDIT : RESULT");
       // console.log(ajournementCredit);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_AJOURNEMENT_RELEVE_VENTES',
               error_content: err
           });
        }else{
      
       return res.json(ajournementRelVentes);
        }
    });
})

  router.delete('/deleteAjournementReleveVentes/:codeAjournementReleveVentes',function(req,res,next){
    
       console.log(" DELETE AJOURNEMENT RELEVE VENTES  IS CALLED") ;
       console.log(" THE AJOURNEMENT RELEVE VENTES   TO DELETE IS :",req.params['codeAjournementReleveVentes']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = ajournementsRelevesVentes.deleteAjournementReleveVentes(
        req.app.locals.dataBase,req.params['codeAjournementReleveVentes'],decoded.userData.code,
        function(err,codeAjournementReleveVentes){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_AJOURNEMENT_RELEVE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeAjournementReleveVentes']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getAjournementsReleveVentesByCodeReleve/:codeReleveVentes',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    ajournementsRelevesVentes.getListAjournementsRelevesVentesByCodeReleveVentes(
        req.app.locals.dataBase,decoded.userData.code,req.params['codeReleveVentes'],
         function(err,listAjournementsRelVentes){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_AJOURNEMENTS_RELEVES_VENTES_BY_CODE_RELEVE',
                 error_content: err
             });
          }else{
        
         return res.json(listAjournementsRelVentes);
          }
      });

  });
  module.exports.routerAjournementsRelevesVentes= router;