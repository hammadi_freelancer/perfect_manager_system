
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var ajournementsFacturesVentes = require('../model/ajournements-factures-ventes').AjournementsFacturesVentes;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addAjournementFactureVentes',function(req,res,next){
    
       console.log(" ADD AJOURNEMENT FACT VENTES  IS CALLED") ;
       console.log(" THE AJOURNEMENT FACT VENTES  TO ADDED IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = ajournementsFacturesVentes.addAjournementFactureVentes(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,ajournementFactAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_AJOURNEMENT_FACTURE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(ajournementFactAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateAjournementFactureVentes',function(req,res,next){
    
       console.log(" UPDATE AJOURNEMENT FACT VENTES  IS CALLED") ;
       //console.log(" THE AJOURNEMENT FACT VENTES  TO UPDATE IS :",req.body);
      /* console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));*/
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));  
       var result = ajournementsFacturesVentes.updateAjournementFactureVentes(
        req.app.locals.dataBase,req.body,decoded.userData.code,
         function(err,ajournementFactUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_AJOURNEMENT_FACTURE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(ajournementsFacturesVentes);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getAjournementsFacturesVentes',function(req,res,next){
    /*console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));*/
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    ajournementsFacturesVentes.getListAjournementsFacturesVentes(req.app.locals.dataBase,
        decoded.userData.code, function(err,listAjournementsFactVentes){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_AJOURNEMENTS_FACTURES_VENTES',
                 error_content: err
             });
          }else{
        
         return res.json(listAjournementsFactVentes);
          }
      });

  });
  router.get('/getAjournementFactureVentes/:codeAjournementFactureVentes',function(req,res,next){
    console.log("GET AJOURNEMENT FACTURE VENTES  IS CALLED");
    console.log(req.params['codeAjournementFactureVentes']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    ajournementsFacturesVentes.getAjournementFactureVentes(
        req.app.locals.dataBase,req.params['codeAjournementFactureVentes'],decoded.userData.code,
     function(err,ajournementFactVentes){
       //console.log("GET  AJOURNEMENT CREDIT : RESULT");
       // console.log(ajournementCredit);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_AJOURNEMENT_FACTURE_VENTES',
               error_content: err
           });
        }else{
      
       return res.json(ajournementFactVentes);
        }
    });
})

  router.delete('/deleteAjournementFactureVentes/:codeAjournementFactureVentes',function(req,res,next){
    
       console.log(" DELETE AJOURNEMENT FACTURE VENTES  IS CALLED") ;
       console.log(" THE AJOURNEMENT FACTURE VENTES   TO DELETE IS :",req.params['codeAjournementFactureVentes']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = ajournementsFacturesVentes.deleteAjournementFactureVentes(
        req.app.locals.dataBase,req.params['codeAjournementFactureVentes'],decoded.userData.code,
        function(err,codeAjournementFactureVentes){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_AJOURNEMENT_FACTURE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeAjournementFactureVentes']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getAjournementsFacturesVentesByCodeFactureVentes/:codeFactureVentes',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    ajournementsFacturesVentes.getListAjournementsFacturesVentesByCodeFactureVentes(
        req.app.locals.dataBase,decoded.userData.code,req.params['codeFactureVentes'],
         function(err,listAjournementsFactVentes){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_AJOURNEMENTS_FACTURES_VENTES_BY_CODE_FACTURE_VENTES',
                 error_content: err
             });
          }else{
        
         return res.json(listAjournementsFactVentes);
          }
      });

  });
  module.exports.routerAjournementsFacturesVentes= router;