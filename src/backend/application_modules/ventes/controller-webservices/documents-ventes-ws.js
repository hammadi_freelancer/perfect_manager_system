
var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var documentsVentes = require('../model/documents-ventes').DocumentsVentes;
// var commonManager = require('../application_modules/common/main').CommonManager;
var jwt = require('jsonwebtoken');

router.post('/addDocumentVente',function(req,res,next){
    
       console.log(" ADD DOCUMENT VENTE IS CALLED") ;
       console.log(" THE DOCUMENT VENTE TO ADDED IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = documentsVentes.addDocumentVente(req.app.locals.dataBase,req.body,decoded.userData.code, function(err,documentVenteAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_DOCUMENT_VENTE',
                error_content: err
            });
         }else{
       
        return res.json(documentVenteAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateDocumentVente',function(req,res,next){
    
       console.log(" UPDATE DOCUMENT  VENTE IS CALLED") ;
       console.log(" THE DOCUMENT VENTE TO UPDATE IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = documentsVentes.updateDocumentVente(req.app.locals.dataBase,req.body,decoded.userData.code, 
        function(err,documentVenteUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_DOCUMENT_VENTE',
                error_content: err
            });
         }else{
       
        return res.json(documentVenteUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getDocumentsVentes',function(req,res,next){
      console.log("GET LIST DOCUMENTS VENTES IS CALLED");
      var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    documentsVentes.getListDocumentsVentes(req.app.locals.dataBase,decoded.userData.code, function(err,listDocumentsVentes){
         console.log("GET LIST DOCUMENTS VENTES : RESULT");
         console.log(listDocumentsVentes);
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_VENTES',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsVentes);
          }
      });

  });
  router.get('/getDocumentVente/:codeDocumentVente',function(req,res,next){
    console.log("GET DOCUMENT VENTE IS CALLED");
    console.log(req.params['codeDocumentVente']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
        documentsVentes.getDocumentVente(req.app.locals.dataBase,req.params['codeDocumentVente'],decoded.userData.code, function(err,documentVente){
       console.log("GET  DOCUMENT VENTE : RESULT");
       console.log(documentVente);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_DOCUMENT_VENTE',
               error_content: err
           });
        }else{
      
       return res.json(documentVente);
        }
    });
})

  router.delete('/deleteDocumentVente/:codeDocumentVente',function(req,res,next){
    
       console.log(" DELETE DOCUMENT VENTE IS CALLED") ;
       console.log(" THE DOCUMENT VENTE  TO DELETE IS :",req.params['codeDocumentVente']);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = documentsVentes.deleteDocumentVente(req.app.locals.dataBase,req.params['codeDocumentVente'],decoded.userData.code,function(err,codeDocumentVente){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_DOCUMENT_VENTE',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeDocumentVente']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
  module.exports.routerDocumentsVentes = router;