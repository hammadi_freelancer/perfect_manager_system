
var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var parametresVentes = require('../model/parametres-ventes').ParametresVentes;
// var commonManager = require('../application_modules/common/main').CommonManager;
var jwt = require('jsonwebtoken');

router.post('/saveParametresVentes',function(req,res,next){
    
       console.log(" SAVE  PARAMETRES VENTES IS CALLED") ;
       console.log(" THE PARAMETRES VENTES TO ADDED IS :",req.body);

       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = parametresVentes.saveParametresVentes(req.app.locals.dataBase,req.body,decoded.userData.code,
        function(err,parametresVentesAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_SAVE_PARAMETRES_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(parametresVentesAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
  router.get('/getParametresVentes',function(req,res,next){
    console.log("GET PARAMETRES VENTES IS CALLED");
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    parametresVentes.getParametresVentes(req.app.locals.dataBase,decoded.userData.code,function(err,parametresVentesObject){
       console.log("GET  PARAMETRES VENTES : RESULT");
       console.log(parametresVentesObject);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_PARAMETRES_VENTES',
               error_content: err
           });
        }else{
      
       return res.json(parametresVentesObject);
        }
    });
})

 
  module.exports.routerParametresVentes = router;