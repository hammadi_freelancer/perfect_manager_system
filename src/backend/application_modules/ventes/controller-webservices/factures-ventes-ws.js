
var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var facturesVentes = require('../model/factures-ventes').FacturesVentes;
// var commonManager = require('../application_modules/common/main').CommonManager;
var jwt = require('jsonwebtoken');

router.post('/addFactureVente',function(req,res,next){
    
       console.log(" ADD FACTURE VENTE IS CALLED") ;
       console.log(" THE FACTURE VENTE TO ADDED IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = facturesVentes.addFactureVente(req.app.locals.dataBase,req.body,decoded.userData.code, function(err,factureVenteAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_FACTURE_VENTE',
                error_content: err
            });
         }else{
       
        return res.json(factureVenteAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateFactureVente',function(req,res,next){
    
       console.log(" UPDATE FACTURE VENTE IS CALLED") ;
       console.log(" THE FACTURE VENTE TO UPDATE IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = facturesVentes.updateFactureVente(req.app.locals.dataBase,req.body,decoded.userData.code, function(err,factureVenteUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_FACTURE_VENTE',
                error_content: err
            });
         }else{
       
        return res.json(factureVenteUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getFacturesVentes',function(req,res,next){
      console.log("GET LIST FACTURES VENTES IS CALLED");
      var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    facturesVentes.getListFacturesVentes(req.app.locals.dataBase,decoded.userData.code,
        req.query.pageNumber, req.query.nbElementsPerPage ,req.query.filter,
         function(err,listFacturesVentes){
         console.log("GET LIST FACTURES VENTES : RESULT");
         console.log(listFacturesVentes);
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_FACTURES_VENTES',
                 error_content: err
             });
          }else{
        
         return res.json(listFacturesVentes);
          }
      });

  });
  router.get('/getFactureVente/:codeFactureVentes',function(req,res,next){
    console.log("GET FACTURE VENTE IS CALLED");
    console.log(req.params['codeFactureVentes']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    facturesVentes.getFactureVente(req.app.locals.dataBase,req.params['codeFactureVentes'],decoded.userData.code, function(err,factureVente){
       console.log("GET  FACTURE VENTE : RESULT");
       console.log(factureVente);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_FACTURE_VENTE',
               error_content: err
           });
        }else{
      
       return res.json(factureVente);
        }
    });
})

  router.delete('/deleteFactureVente/:codeFactureVentes',function(req,res,next){
    
       console.log(" DELETE FACTURE VENTE IS CALLED") ;
       console.log(" THE FACTURE VENTE  TO DELETE IS :",req.params['codeFactureVente']);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = facturesVentes.deleteFactureVente(req.app.locals.dataBase,req.params['codeFactureVentes'],decoded.userData.code,function(err,codeFactureVente){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_FACTURE_VENTE',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeFactureVentes']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.get('/getTotalFacturesVentes',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    facturesVentes.getTotalFacturesVentes(req.app.locals.dataBase,decoded.userData.code,
        req.query.filter,function(err,totalFacturesVentes){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_FACTURES_VENTES',
                 error_content: err
             });
          }else{
        
         return res.json({'totalFacturesVentes':totalFacturesVentes});
          }
      });

  });
  module.exports.routerFacturesVentes = router;