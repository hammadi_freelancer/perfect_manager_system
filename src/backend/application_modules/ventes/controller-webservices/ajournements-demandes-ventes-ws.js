
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var ajournementsDemandesVentes = require('../model/ajournements-demandes-ventes').AjournementsDemandesVentes;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addAjournementDemandeVentes',function(req,res,next){
    
       console.log(" ADD AJOURNEMENT REL VENTES  IS CALLED") ;
       console.log(" THE AJOURNEMENT REL VENTES  TO ADDED IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = ajournementsDemandesVentes.addAjournementDemandeVentes(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,ajournementRelAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_AJOURNEMENT_DEMANDES_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(ajournementRelAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateAjournementDemandeVentes',function(req,res,next){
    
       console.log(" UPDATE AJOURNEMENT REL VENTES  IS CALLED") ;
       //console.log(" THE AJOURNEMENT FACT VENTES  TO UPDATE IS :",req.body);
      /* console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));*/
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));  
       var result = ajournementsDemandesVentes.updateAjournementDemandeVentes(
        req.app.locals.dataBase,req.body,decoded.userData.code,
         function(err,ajournementRelVentes){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_AJOURNEMENT_DEMANDE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(ajournementsDemandesVentes);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getAjournementsDemandesVentes',function(req,res,next){
    /*console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));*/
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    ajournementsDemandesVentes.getListAjournementsDemandesVentes(
        req.app.locals.dataBase,decoded.userData.code, function(err,listAjournementsRelVentes){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_AJOURNEMENTS_DEMANDES_VENTES',
                 error_content: err
             });
          }else{
        
         return res.json(listAjournementsRelVentes);
          }
      });

  });
  router.get('/getAjournementDemandeVentes/:codeAjournementDemandeVentes',function(req,res,next){
    console.log("GET AJOURNEMENT DEMANDE VENTES  IS CALLED");
    console.log(req.params['codeAjournementDemandeVentes']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    ajournementsDemandesVentes.getAjournementDemandeVentes(
        req.app.locals.dataBase,req.params['codeAjournementDemandeVentes'],decoded.userData.code,
     function(err,ajournementRelVentes){
       //console.log("GET  AJOURNEMENT CREDIT : RESULT");
       // console.log(ajournementCredit);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_AJOURNEMENT_DEMANDE_VENTES',
               error_content: err
           });
        }else{
      
       return res.json(ajournementRelVentes);
        }
    });
})

  router.delete('/deleteAjournementDemandeVentes/:codeAjournementDemandeVentes',function(req,res,next){
    
       console.log(" DELETE AJOURNEMENT DEMANDE VENTES  IS CALLED") ;
       console.log(" THE AJOURNEMENT DEMANDE VENTES   TO DELETE IS :",req.params['codeAjournementDemandeVentes']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = ajournementsDemandesVentes.deleteAjournementDemandeVentes(
        req.app.locals.dataBase,req.params['codeAjournementDemandeVentes'],decoded.userData.code,
        function(err,codeAjournementDemandeVentes){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_AJOURNEMENT_DEMANDE_VENTES',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeAjournementDemandeVentes']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getAjournementsDemandesVentesByCodeDemande/:codeDemandeVentes',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    ajournementsDemandesVentes.getListAjournementsDemandesVentesByCodeDemandeVentes(
        req.app.locals.dataBase,decoded.userData.code,req.params['codeDemandeVentes'],
         function(err,listAjournementsRelVentes){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_AJOURNEMENTS_DEMANDES_VENTES_BY_CODE_DEMANDE',
                 error_content: err
             });
          }else{
        
         return res.json(listAjournementsRelVentes);
          }
      });

  });
  module.exports.routerAjournementsDemandesVentes= router;