
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var reglementsDettes = require('../model/reglements-dettes').ReglementsDettes;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addReglementDette',function(req,res,next){
    
    console.log(" ADD REGLEMENT DETTE IS CALLED") ;
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = reglementsDettes.addReglementDette(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,regAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_REGLEMENT_DETTE',
                error_content: err
            });
         }else{
       
        return res.json(regAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateReglementDette',function(req,res,next){
    
       console.log(" UPDATE REGLEMENT DETTE IS CALLED") ;
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = reglementsDette.updateReglementDette(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,regUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_REGLEMENT_DETTE',
                error_content: err
            });
         }else{
       
        return res.json(regUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getReglementsDettes',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
      console.log("GET LIST REGLEMENTS C IS CALLED");
    reglementsDettes.getListReglementsDettes(
        req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
        req.query.filter,  function(err,listRegs){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_REGLEMENTS_DETTES',
                 error_content: err
             });
          }else{
        
         return res.json(listRegs);
          }
      });

  });
  router.get('/getPageReglementsDettes',function(req,res,next){
    
        var decoded = jwt.decode(req.headers.authorization.slice( 
            req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
      
          console.log("GET PAGE REGLEMENTS DETTES IS CALLED");
        reglementsDettes.getPageReglementsDettes(
            req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
            req.query.filter, function(err,listOperations){
              if(err){
                 return  res.json({
                     error_message : 'ERROR_GET_PAGE_REGLEMENTS_DETTES',
                     error_content: err
                 });
              }else{
            
             return res.json(listOperations);
              }
          });
    
      });
  router.get('/getReglementDette/:codeReglementDette',function(req,res,next){
    console.log("GET REGLEMENT DETTE IS CALLED");
  //  console.log(req.params['codeDette']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    reglementsDettes.getReglementDette(
        req.app.locals.dataBase,req.params['codeReglementDette'],
        decoded.userData.code, function(err,operationCourante){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_REGLEMENT_DETTE',
               error_content: err
           });
        }else{
      
       return res.json(operationCourante);
        }
    });
})

  router.delete('/deleteReglementDette/:codeReglementDette',function(req,res,next){
    
       console.log(" DELETE REGLEMENT DETTE IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = reglementsDettes.deleteReglementDette(
        req.app.locals.dataBase,req.params['codeReglementDette'],decoded.userData.code, function(err,codeReglementDette){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_REGLEMENT_DETTE',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeReglementDette']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });



 router.get('/getTotalReglementsDettes',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    reglementsDettes.getTotalReglementsDettes(
        req.app.locals.dataBase,decoded.userData.code,req.query.filter,
        function(err,totalReglementsDettes){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_REGLEMENTS_DETTES',
                 error_content: err
             });
          }else{
        
         return res.json({'totalReglementsDettes':totalReglementsDettes});
          }
      });

  });
  module.exports.routerReglementsDettes = router;