//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var ReglementsDettes = {
addReglementDette : function (db,reglementDette,codeUser,callback){
    if(reglementDette != undefined){
        reglementDette.code  = utils.isUndefined(reglementDette.code)?shortid.generate():reglementDette.code;

        reglementDette.dateReglementDetteObject = utils.isUndefined(reglementDette.dateReglementDetteObject)? 
        new Date():reglementDette.dateReglementDetteObject;
        if(utils.isUndefined(reglementDette.numeroReglementDette))
            {
               const currentD =  moment(new Date());
              // const genereKey = shortid.generate();
              const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.day().toString()+
              shortid.generate();
              reglementDette.numeroReglementDette = 'REG@DET' +generatedCode;
            }
       
            reglementDette.userCreatorCode = codeUser;
            reglementDette.userLastUpdatorCode = codeUser;
            reglementDette.dateCreation = moment(new Date).format('L');
            reglementDette.dateLastUpdate = moment(new Date).format('L');
            
            db.collection('ReglementDette').insertOne(
                reglementDette,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
             // db.close();
             
    

}else{
callback(true,null);
}
},
updateReglementDette: function (db,reglementDette,codeUser, callback){

   
    reglementDette.dateLastUpdate = moment(new Date).format('L');
    
    reglementDette.dateReglementDetteObject = utils.isUndefined(reglementDette.dateReglementDetteObject)? 
    new Date():reglementDette.dateReglementDetteObject;

 

    const criteria = { "code" : reglementDette.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
    "dateReglementDetteObject" : reglementDette.dateReglementDetteObject, 
     "fournisseur": reglementDette.fournisseur,
     "valeurRegle": reglementDette.valeurRegle,
     "periodeFromObject": reglementDette.periodeFromObject,
     "periodeToObject": reglementDette.periodeToObject,
     "mois": reglementDette.mois,
     "modePaiement": reglementDette.modePaiement,
     "etat": reglementDette.etat,
     "description": reglementDette.description,
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": reglementDette.dateLastUpdate, 
    } ;
            db.collection('ReglementDette').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },function(reglementDette,err){
                    if(err)
                    callback(false,err);
                    else
                        callback(true,true);
                    
                    
                }
            ) ;
     
    
    },
   
getListReglementsDettes : function (db,codeUser,pageNumber, nbElementsPerPage , filter,callback)
{
    let listReglementsDettes = [];
   
  //console.log('filter recieved is :',filter);
  let filterObject = JSON.parse(filter);
 
   let filterData = {
       "userCreatorCode" : codeUser
   };
   const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
   
   var cursor =  db.collection('ReglementDette').find( 
    conditionBuilt
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(reglementDette,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        reglementDette.dateReglementDette= moment(reglementDette.dateReglementDetteObject).format('L');
        reglementDette.periodeFrom= moment(reglementDette.periodeFromObject).format('L');
        reglementDette.periodeTo= moment(reglementDette.periodeToObject).format('L');
       listReglementsDettes.push(reglementDette);
   }).then(function(){
    // console.log('list credits',listDettes);
    callback(null,listReglementsDettes); 
   });
     //return [];
},
deleteReglementDette: function (db,codeReglementDette,codeUser,callback){
    const criteria = { "code" : codeReglementDette, "userCreatorCode":codeUser };
    
    db.collection('ReglementDette').deleteOne(
        criteria , function(err,result){
            if(err){
                
                callback(null,null);
            }else{
                callback(false,result);
            }
        }
    ) ;
},

getReglementDette: function (db,codeReglementDette,codeUser,callback)
{
    const criteria = { "code" : codeReglementDette, "userCreatorCode":codeUser };
  
       const reglementDette =  db.collection('ReglementDette').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
        // callback(false,fournisseur);
                 
},
getTotalReglementsDettes : function (db,codeUser,filter, callback)
{
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
   var totalReglementsDettes =  db.collection('ReglementDette').find( 
    conditionBuilt
).count(function(err,result){
        callback(null,result); 
    }
);
   


     //return [];
},
getPageReglementsDettes : function (db,codeUser,pageNumber, nbElementsPerPage ,filter, callback)
{
    console.log('filter ReglementDettes',filter);
    let listReglementsDettes = [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
      console.log('condition',conditionBuilt);
      
      
   var cursor =  db.collection('ReglementDette').find( conditionBuilt
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(reglementDette,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        reglementDette.dateReglementDette= moment(reglementDette.dateReglementDetteObject).format('L');
        reglementDette.periodeFrom= moment(reglementDette.periodeFromObject).format('L');
        reglementDette.periodeTo= moment(reglementDette.periodeToObject).format('L');
        
       listReglementsDettes.push(reglementDette);
   }).then(function(){
    // console.log('list credits',listDettes);
    callback(null,listReglementsDettes); 
   });
     //return [];
},
buildFilterCondition(filterObject,filterData)
{
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.numeroReglementDette))
        {
                filterData["numeroReglementDette"] = filterObject.numeroReglementDette;
        }
         
        if(!utils.isUndefined(filterObject.etat))
          {
                 
                    filterData["etat"] = filterObject.etat;
                    
         }
         if(!utils.isUndefined(filterObject.dateReglementDetteFromObject))
             {
                    
                       filterData["dateReglementDetteObject"] = {$gt: filterObject.dateReglementDetteFromObject};
                       
            }
            if(!utils.isUndefined(filterObject.dateReglementDetteToObject))
             {
                    
                       filterData["dateReglementDetteObject"] = {$lt: filterObject.dateReglementDetteToObject};
                       
            }
            if(!utils.isUndefined(filterObject.cinClient))
             {
                    
                       filterData["fournisseur.cin"] = filterObject.cinClient ;
                       
            }
            if(!utils.isUndefined(filterObject.nomClient))
             {
                    
                       filterData["fournisseur.nom"] = filterObject.nomClient ;
                       
            }
            if(!utils.isUndefined(filterObject.prenomClient))
             {
                    
                       filterData["fournisseur.prenom"] = filterObject.prenomClient ;
                       
            }
            if(!utils.isUndefined(filterObject.raisonClient))
             {
                    
                       filterData["fournisseur.raisonSociale"] = filterObject.raisonClient ;
                       
            }
            if(!utils.isUndefined(filterObject.matriculeClient))
             {
                    
                       filterData["fournisseur.matriculeFiscale"] = filterObject.matriculeClient ;
                       
            }
            if(!utils.isUndefined(filterObject.registreClient))
             {
                    
                       filterData["fournisseur.registreCommerce"] = filterObject.registreClient ;
                       
            }
        
            if(!utils.isUndefined(filterObject.valeurRegle))
             {
                    
                       filterData["valeurRegle"] = filterObject.valeurRegle ;
                       
            }
            if(!utils.isUndefined(filterObject.mois))
             {
                    
                       filterData["mois"] = filterObject.mois ;
                       
            }
            if(!utils.isUndefined(filterObject.modePaiement))
             {
                    
                       filterData["modePaiement"] = filterObject.modePaiement ;
                       
            }
     
        }
        return filterData;
},
}
module.exports.ReglementsDettes = ReglementsDettes;