
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var historiquesFournisseurs = require('../model/historiques-fournisseurs').HistoriquesFournisseurs;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addHistoriqueFournisseur',function(req,res,next){
    
    console.log(" ADD HISTORIQUE FOURNISSEUR IS CALLED") ;
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = historiquesFournisseurs.addHistoriqueFournisseur(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,regAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_HISTORIQUE_FOURNISSEUR',
                error_content: err
            });
         }else{
       
        return res.json(regAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateHistoriqueFournisseur',function(req,res,next){
    
       console.log(" UPDATE HISTORIQUE FOURNISSEUR IS CALLED") ;
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = reglementsDette.updateHistoriqueFournisseur(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,regUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_HISTORIQUE_FOURNISSEUR',
                error_content: err
            });
         }else{
       
        return res.json(regUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getHistoriquesFournisseurs',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
      console.log("GET LIST HISTORIQUE FOURNISSEURS C IS CALLED");
    historiquesFournisseurs.getListHistoriquesFournisseurs(
        req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
        req.query.filter,  function(err,listRegs){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_HISTORIQUE_FOURNISSEURS',
                 error_content: err
             });
          }else{
        
         return res.json(listRegs);
          }
      });

  });
  router.get('/getPageHistoriquesFournisseurs',function(req,res,next){
    
        var decoded = jwt.decode(req.headers.authorization.slice( 
            req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
      
          console.log("GET PAGE HISTORIQUE FOURNISSEURS IS CALLED");
        historiquesFournisseurs.getPageHistoriquesFournisseurs(
            req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
            req.query.filter, function(err,listOperations){
              if(err){
                 return  res.json({
                     error_message : 'ERROR_GET_PAGE_HISTORIQUE_FOURNISSEURS',
                     error_content: err
                 });
              }else{
            
             return res.json(listOperations);
              }
          });
    
      });
  router.get('/getHistoriqueFournisseur/:codeHistoriqueFournisseur',function(req,res,next){
    console.log("GET HISTORIQUE FOURNISSEUR IS CALLED");
  //  console.log(req.params['codeDette']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    historiquesFournisseurs.getHistoriqueFournisseur(
        req.app.locals.dataBase,req.params['codeHistoriqueFournisseur'],
        decoded.userData.code, function(err,operationCourante){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_HISTORIQUE_FOURNISSEUR',
               error_content: err
           });
        }else{
      
       return res.json(operationCourante);
        }
    });
})

  router.delete('/deleteHistoriqueFournisseur/:codeHistoriqueFournisseur',function(req,res,next){
    
       console.log(" DELETE HISTORIQUE FOURNISSEUR IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = historiquesFournisseurs.deleteHistoriqueFournisseur(
        req.app.locals.dataBase,req.params['codeHistoriqueFournisseur'],decoded.userData.code, function(err,codeHistoriqueFournisseur){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_HISTORIQUE_FOURNISSEUR',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeHistoriqueFournisseur']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });



 router.get('/getTotalHistoriquesFournisseurs',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    historiquesFournisseurs.getTotalHistoriquesFournisseurs(
        req.app.locals.dataBase,decoded.userData.code,req.query.filter,
        function(err,totalHistoriquesFournisseurs){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_HISTORIQUE_FOURNISSEURS',
                 error_content: err
             });
          }else{
        
         return res.json({'totalHistoriquesFournisseurs':totalHistoriquesFournisseurs});
          }
      });

  });
  module.exports.routerHistoriquesFournisseurs = router;