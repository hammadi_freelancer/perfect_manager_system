//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoFournisseur = require('mongodb').MongoFournisseur;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var HistoriquesFournisseurs = {
addHistoriqueFournisseur : function (db,historiqueFournisseur,codeUser,callback){
    if(historiqueFournisseur != undefined){
        historiqueFournisseur.code  = utils.isUndefined(historiqueFournisseur.code)?shortid.generate():historiqueFournisseur.code;

        historiqueFournisseur.dateHistoriqueFournisseurObject = utils.isUndefined(historiqueFournisseur.dateHistoriqueFournisseurObject)? 
        new Date():historiqueFournisseur.dateHistoriqueFournisseurObject;
        if(utils.isUndefined(historiqueFournisseur.numeroHistoriqueFournisseur))
            {
               const currentD =  moment(new Date());
              // const genereKey = shortid.generate();
              const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.day().toString()+
              shortid.generate();
              historiqueFournisseur.numeroHistoriqueFournisseur = 'HIS@FOUR' +generatedCode;
            }
       
            historiqueFournisseur.userCreatorCode = codeUser;
            historiqueFournisseur.userLastUpdatorCode = codeUser;
            historiqueFournisseur.dateCreation = moment(new Date).format('L');
            historiqueFournisseur.dateLastUpdate = moment(new Date).format('L');
            
            db.collection('HistoriqueFournisseur').insertOne(
                historiqueFournisseur,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
             // db.close();
             
    

}else{
callback(true,null);
}
},
updateHistoriqueFournisseur: function (db,historiqueFournisseur,codeUser, callback){

   
    historiqueFournisseur.dateLastUpdate = moment(new Date).format('L');
    
    historiqueFournisseur.dateHistoriqueFournisseurObject = utils.isUndefined(historiqueFournisseur.dateHistoriqueFournisseurObject)? 
    new Date():historiqueFournisseur.dateHistoriqueFournisseurObject;

 

    const criteria = { "code" : historiqueFournisseur.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
    "dateHistoriqueFournisseurObject" : historiqueFournisseur.dateHistoriqueFournisseurObject, 
     "fournisseur": historiqueFournisseur.fournisseur,
     "montantMisEnValeur": historiqueFournisseur.montantMisEnValeur,
     "dateVentesObject": historiqueFournisseur.dateVentesObject,
     "dateReglementObject": historiqueFournisseur.dateReglementObject,
     "additionInfos": historiqueFournisseur.additionInfos,
     "description": historiqueFournisseur.description,
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": historiqueFournisseur.dateLastUpdate, 
    } ;
            db.collection('HistoriqueFournisseur').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },function(historiqueFournisseur,err){
                    if(err)
                    callback(false,err);
                    else
                        callback(true,true);
                    
                    
                }
            ) ;
     
    
    },
   
getListHistoriquesFournisseurs : function (db,codeUser,pageNumber, nbElementsPerPage , filter,callback)
{
    let listHistoriquesFournisseurs = [];
   
  //console.log('filter recieved is :',filter);
  let filterObject = JSON.parse(filter);
 
   let filterData = {
       "userCreatorCode" : codeUser
   };
   const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
   
   var cursor =  db.collection('HistoriqueFournisseur').find( 
    conditionBuilt
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(historiqueFournisseur,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        historiqueFournisseur.dateHistoriqueFournisseur= moment(historiqueFournisseur.dateHistoriqueFournisseurObject).format('L');
        historiqueFournisseur.periodeFrom= moment(historiqueFournisseur.periodeFromObject).format('L');
        historiqueFournisseur.periodeTo= moment(historiqueFournisseur.periodeToObject).format('L');
       listHistoriquesFournisseurs.push(historiqueFournisseur);
   }).then(function(){
    // console.log('list credits',listDettes);
    callback(null,listHistoriquesFournisseurs); 
   });
     //return [];
},
deleteHistoriqueFournisseur: function (db,codeHistoriqueFournisseur,codeUser,callback){
    const criteria = { "code" : codeHistoriqueFournisseur, "userCreatorCode":codeUser };
    
    db.collection('HistoriqueFournisseur').deleteOne(
        criteria , function(err,result){
            if(err){
                
                callback(null,null);
            }else{
                callback(false,result);
            }
        }
    ) ;
},

getHistoriqueFournisseur: function (db,codeHistoriqueFournisseur,codeUser,callback)
{
    const criteria = { "code" : codeHistoriqueFournisseur, "userCreatorCode":codeUser };
  
       const historiqueFournisseur =  db.collection('HistoriqueFournisseur').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
        // callback(false,fournisseur);
                 
},
getTotalHistoriquesFournisseurs : function (db,codeUser,filter, callback)
{
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
   var totalHistoriquesFournisseurs =  db.collection('HistoriqueFournisseur').find( 
    conditionBuilt
).count(function(err,result){
        callback(null,result); 
    }
);
   


     //return [];
},
getPageHistoriquesFournisseurs : function (db,codeUser,pageNumber, nbElementsPerPage ,filter, callback)
{
    console.log('filter HistoriqueFournisseurs',filter);
    let listHistoriquesFournisseurs = [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
      console.log('condition',conditionBuilt);
      
      
   var cursor =  db.collection('HistoriqueFournisseur').find( conditionBuilt
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(historiqueFournisseur,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        historiqueFournisseur.dateHistoriqueFournisseur= moment(historiqueFournisseur.dateHistoriqueFournisseurObject).format('L');
        historiqueFournisseur.periodeFrom= moment(historiqueFournisseur.periodeFromObject).format('L');
        historiqueFournisseur.periodeTo= moment(historiqueFournisseur.periodeToObject).format('L');
        
       listHistoriquesFournisseurs.push(historiqueFournisseur);
   }).then(function(){
    // console.log('list credits',listDettes);
    callback(null,listHistoriquesFournisseurs); 
   });
     //return [];
},
buildFilterCondition(filterObject,filterData)
{
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.numeroHistoriqueFournisseur))
        {
                filterData["numeroHistoriqueFournisseur"] = filterObject.numeroHistoriqueFournisseur;
        }
         
   
         if(!utils.isUndefined(filterObject.dateAchatsFromObject))
             {
                    
                       filterData["dateAchatsObject"] = {$gt: filterObject.dateAchatsFromObject};
                       
            }
            if(!utils.isUndefined(filterObject.dateAchatsToObject))
             {
                    
                       filterData["dateAchatsObject"] = {$lt: filterObject.dateAchatsToObject};
                       
            }
            if(!utils.isUndefined(filterObject.dateReglementFromObject))
                {
                       
                          filterData["dateReglementObject"] = {$gt: filterObject.dateReglementFromObject};
                          
               }
            if(!utils.isUndefined(filterObject.dateReglementToObject))
                {
                       
                          filterData["dateReglementObject"] = {$lt: filterObject.dateReglementToObject};
                          
               }

            if(!utils.isUndefined(filterObject.cinFournisseur))
             {
                    
                       filterData["fournisseur.cin"] = filterObject.cinFournisseur ;
                       
            }
            if(!utils.isUndefined(filterObject.nomFournisseur))
             {
                    
                       filterData["fournisseur.nom"] = filterObject.nomFournisseur ;
                       
            }
            if(!utils.isUndefined(filterObject.prenomFournisseur))
             {
                    
                       filterData["fournisseur.prenom"] = filterObject.prenomFournisseur ;
                       
            }
            if(!utils.isUndefined(filterObject.raisonFournisseur))
             {
                    
                       filterData["fournisseur.raisonSociale"] = filterObject.raisonFournisseur ;
                       
            }
            if(!utils.isUndefined(filterObject.matriculeFournisseur))
             {
                    
                       filterData["fournisseur.matriculeFiscale"] = filterObject.matriculeFournisseur ;
                       
            }
            if(!utils.isUndefined(filterObject.registreFournisseur))
             {
                    
                       filterData["fournisseur.registreCommerce"] = filterObject.registreFournisseur ;
                       
            }
        
            if(!utils.isUndefined(filterObject.montantMisEnValeur))
             {
                    
                       filterData["montantMisEnValeur"] = filterObject.montantMisEnValeur ;
                       
            }
   
     
        }
        return filterData;
},
}
module.exports.HistoriquesFournisseurs = HistoriquesFournisseurs;