
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var documentsLivresOres = require('../model/documents-livres-ores').DocumentsLivresOres;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addDocumentLivreOre',function(req,res,next){
    
       console.log(" ADD DOCUMENT LIVRE ORE   IS CALLED") ;
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = documentsLivresOres.addDocumentLivreOre(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,documentLivreOreAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_DOCUMENT_LIVRE_ORE',
                error_content: err
            });
         }else{
       
        return res.json(documentLivreOreAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateDocumentLivreOre',function(req,res,next){
    
      // console.log(" UPDATE DOCUMENT CREDIT  IS CALLED") ;
       console.log(" THE DOCUMENT LIVRE ORE   TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result =documentsLivresOres.updateDocumentLivreOre(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,documentLivreOreUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_DOCUMENT_LIVRE_ORE',
                error_content: err
            });
         }else{
       
        return res.json(documentLivreOreUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getDocumentsLivresOres',function(req,res,next){
 
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    documentsLivresOres.getListDocumentsLivresOres(
        req.app.locals.dataBase,decoded.userData.code, function(err,listDocumentsLivresOres){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_LIVRES_ORES',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsLivresOres);
          }
      });

  });
  router.get('/getDocumentLivreOre/:codeDocumentLivreOre',function(req,res,next){
    console.log("GET DOCUMENT LIVRE ORE   IS CALLED");
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    documentsLivresOres.getDocumentLivreOre(
        req.app.locals.dataBase,req.params['codeDocumentLivreOre'],decoded.userData.code,
     function(err,documentlo){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_DOCUMENT_LIVRE_ORE',
               error_content: err
           });
        }else{
      
       return res.json(documentlo);
        }
    });
})

  router.delete('/deleteDocumentLivreOre/:codeDocumentLivreOre',function(req,res,next){
    
       console.log(" DELETE DOCUMENT LIVRE ORE   IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = documentsLivresOres.deleteDocumentLivreOre(
        req.app.locals.dataBase,req.params['codeDocumentLivreOre'],decoded.userData.code,
        function(err,codeDocumentLivreOre){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_DOCUMENT_LIVRE_ORE',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeDocumentLivreOre']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getDocumentsLivresOresByCodeLivreOre/:codeLivreOre',function(req,res,next){
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
    documentsLivresOres.getListDocumentsLivresOresByCodeLivreOre(
        req.app.locals.dataBase,decoded.userData.code,req.params['codeLivreOre'],
         function(err,listDocumentsLivresOres){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_LIVRES_ORES',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsLivresOres);
          }
      });

  });
  module.exports.routerDocumentsLivresOres = router;