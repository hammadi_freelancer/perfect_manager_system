
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var lignesLivresOres= require('../model/lignes-livre-ores').LignesLivresOres;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addLigneLivreOre',function(req,res,next){
    
       console.log(" ADD LIGNE LIVRE ORE  IS CALLED") ;
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = lignesLivresOres.addLigneLivreOre(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,ligneLivreOreAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_LIGNE_LIVRE_ORE',
                error_content: err
            });
         }else{
       
        return res.json(ligneLivreOreAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateLigneLivreOre',function(req,res,next){
    
      // console.log(" UPDATE DOCUMENT CREDIT  IS CALLED") ;
       console.log(" THE LIGNE LIVRE ORE   TO UPDATE IS :",req.body);
    
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result =lignesLivresOres.updateLigneLivreOre(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,ligneLivreUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_LIGNE_LIVRE_ORE',
                error_content: err
            });
         }else{
       
        return res.json(ligneLivreUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getLignesLivresOres',function(req,res,next){
 
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
    lignesLivresOres.getListLignesLivresOres(decoded.userData.code, function(err,listLignesLivresOres){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_LIGNES_LIVRES_ORES',
                 error_content: err
             });
          }else{
        
         return res.json(listLignesLivresOres);
          }
      });

  });
  router.get('/getLigneLivreOre/:codeLigneLivreOre',function(req,res,next){
    console.log("GET LIGNE LIVRE ORE   IS CALLED");
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    lignesLivresOres.getLigneLivreOre(req.params['codeLigneLivreOre'],decoded.userData.code,
     function(err,ligneLivreOre){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIGNE_LIVRE_ORE ',
               error_content: err
           });
        }else{
      
       return res.json(ligneLivreOre);
        }
    });
})

  router.delete('/deleteLigneLivreOre/:codeLigneLivreOre',function(req,res,next){
    
       console.log(" DELETE LIGNE LIVRE ORE   IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = lignesLivresOres.deleteLigneLivreOre(req.params['codeLigneLivreOre'],decoded.userData.code,
        function(err,codeLigneLivreOre){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_LIGNE_LIVRE_ORE',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeLigneLivreOre']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getLignesLivresOresByCodeLivreOre/:codeLivreOre',function(req,res,next){
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
    lignesLivresOres.getListLignesLivresOresByCodeLivreOre(decoded.userData.code,req.params['codeLivreOre'],
         function(err,listLignesLivresOres){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_LIGNES_LIVRES_ORES_BY_CODE_LIVRE_ORE',
                 error_content: err
             });
          }else{
        
         return res.json(listLignesLivresOres);
          }
      });

  });
  module.exports.routerLignesLivresOres = router;