
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var livresOres = require('../model/livres-ores').LivresOres;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addLivreOre',function(req,res,next){
    
    console.log(" ADD LIVRE ORE  IS CALLED") ;
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = livresOres.addLivreOre(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,livreOreAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_LIVRE_ORE',
                error_content: err
            });
         }else{
       
        return res.json(livreOreAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateLivreOre',function(req,res,next){
    
       console.log(" UPDATE LIVRE ORE  IS CALLED") ;
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = livresOres.updateLivreOre(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,livreUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_LIVRE_ORE',
                error_content: err
            });
         }else{
       
        return res.json(livreUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getLivresOres',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
      console.log("GET LIST LIVRES ORES  IS CALLED");
    livresOres.getListLivresOres(
        req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
        req.query.filter,function(err,listLivresOres){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_LIVRES_ORES',
                 error_content: err
             });
          }else{
        
         return res.json(listLivresOres);
          }
      });

  });
  router.get('/getLivreOre/:codeLivreOre',function(req,res,next){
    console.log("GET LIVRE ORE  IS CALLED");
  //  console.log(req.params['codeCredit']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    livresOres.getLivreOre(
        req.app.locals.dataBase,req.params['codeLivreOre'],decoded.userData.code, function(err,livreOre){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIVRE_ORE',
               error_content: err
           });
        }else{
      
       return res.json(livreOre);
        }
    });
})

  router.delete('/deleteLivreOre/:codeLivreOre',function(req,res,next){
    
       console.log(" DELETE LIVRE ORE  IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = livresOres.deleteLivreOre(
        req.app.locals.dataBase,req.params['codeLivreOre'],decoded.userData.code, function(err,codeLivreOre){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_LIVRE_ORE',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeLivreOre']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });



 router.get('/getTotalLivresOres',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    livresOres.getTotalLivresOres(
        req.app.locals.dataBase,decoded.userData.code,function(err,totalLivresOres){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_LIVRES_ORES',
                 error_content: err
             });
          }else{
        
         return res.json({'totalLivresOres':totalLivresOres});
          }
      });

  });
  module.exports.routerLivresOres = router;