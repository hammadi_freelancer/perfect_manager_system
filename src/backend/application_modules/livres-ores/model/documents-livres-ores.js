//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var DocumentsLivresOres= {
addDocumentLivreOre : function (db,documentLivreOre,codeUser,callback){
    if(documentLivreOre != undefined){
        documentLivreOre.code  = utils.isUndefined(documentLivreOre.code)?shortid.generate():documentLivreOre.code;
        documentLivreOre.dateReceptionObject = utils.isUndefined(documentLivreOre.dateReceptionObject)? 
        new Date():documentLivreOre.dateReceptionObject;
        if(utils.isUndefined(documentLivreOre.numeroDocument))
            {
               const currentD =  moment(new Date());
              // const genereKey = shortid.generate();
              const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.minutes().toString()+
              currentD.secondes().toString();
              documentLivreOre.numeroDocument = 'DOCLIVOR' +currentD.day().toLocaleString()+ generatedCode;
            }
            documentLivreOre.userCreatorCode = codeUser;
            documentLivreOre.userLastUpdatorCode = codeUser;
            documentLivreOre.dateCreation = moment(new Date).format('L');
            documentLivreOre.dateLastUpdate = moment(new Date).format('L');
            db.collection('DocumentLivreOre').insertOne(
                documentLivreOre,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,documentLivreOre);
                   }
                }
              ) ;
             // db.close();
             
   

}else{
callback(true,null);
}
},
updateDocumentLivreOre: function (db,documentLivreOre,codeUser, callback){

   
    documentLivreOre.dateReceptionObject = utils.isUndefined(documentLivreOre.dateReceptionObject)? 
    new Date():documentLivreOre.dateReceptionObject;

    const criteria = { "code" : documentLivreOre.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"dateReceptionObject" : documentLivreOre.dateReceptionObject, 
    "codeLivreOre" : documentLivreOre.codeLivreOre, 
    "description" : documentLivreOre.description,
     "codeOrganisation" : documentLivreOre.codeOrganisation,
     "typeDocument" : documentLivreOre.typeDocument, // cheque, fiche paie, fiche electricié , fiche eau potabe, fiche téléphonique, autres
     "numeroDocument" : documentLivreOre.numeroDocument, 
     "imageFace1":documentLivreOre.imageFace1,
     "imageFace2" : documentLivreOre.imageFace2 , 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": documentLivreOre.dateLastUpdate, 
    } ;
     
            db.collection('DocumentLivreOre').updateOne( criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                     callback(true,null);
                    }else{
 
                     callback(false,result);
                    }
                 }
            ) ;
    
    
    },
getListDocumentsLivresOres : function (db,codeUser,callback)
{
    let listDocumentsLivresOres = [];

   var cursor =  db.collection('DocumentLivreOre').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(docLivreOre,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        docLivreOre.dateReception = moment(docLivreOre.dateReceptionObject).format('L');
        
        listDocumentsLivresOres.push(docLivreOre);
   }).then(function(){
    // console.log('list documents credits',listDocumentsCredits);
    callback(null,listDocumentsLivresOres); 
   });
   

     //return [];
},
deleteDocumentLivreOre: function (db,codeDocumentLivreOre,codeUser,callback){
    const criteria = { "code" : codeDocumentLivreOre, "userCreatorCode":codeUser };
        
            db.collection('DocumentLivreOre').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                     callback(true,null);
                    }else{
 
                     callback(false,result);
                    }
                 }
            ) ;
    
},

getDocumentLivreOre: function (db,codeDocumentLivreOre,codeUser,callback)
{
    const criteria = { "code" : codeDocumentLivreOre, "userCreatorCode":codeUser };
 
       const docLivOre =  db.collection('DocumentLivreOre').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            });
                  
},


getListDocumentsLivresOresByCodeLivreOre : function (db,codeUser,codeLivreOre,callback)
{
    let listDocumentsLivresOres = [];

   var cursor =  db.collection('DocumentLivreOre').find( 
       {"userCreatorCode" : codeUser, "codeLivreOre": codeLivreOre},
       
    );
   cursor.forEach(function(documentLivreOre,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        documentLivreOre.dateReception = moment(documentLivreOre.dateReceptionObject).format('L');
        
        listDocumentsLivresOres.push(documentLivreOre);
   }).then(function(){
   // console.log('list credits',listCredits);
    callback(null,listDocumentsLivresOres); 
   });
   
}

     //return [];



}
module.exports.DocumentsLivresOres = DocumentsLivresOres;