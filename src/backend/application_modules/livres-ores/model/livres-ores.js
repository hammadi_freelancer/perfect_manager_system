//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var LivresOres = {
addLivreOre : function (db,livreOre,codeUser,callback){
    if(livreOre != undefined){
        livreOre.code  = utils.isUndefined(livreOre.code)?shortid.generate():livreOre.code;

        livreOre.dateLivreOreObject = utils.isUndefined(livreOre.dateLivreOreObject)? 
        new Date():livreOre.dateLivreOreObject;

        if(utils.isUndefined(livreOre.numeroLivreOre))
            {
               const currentD =  moment(new Date());
              // const genereKey = shortid.generate();
              const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.minutes().toString()+
              currentD.secondes().toString();
              livreOre.numeroLivreOre = 'LIVOR' +currentD.day().toLocaleString()+ generatedCode;
            }
       
            livreOre.userCreatorCode = codeUser;
            livreOre.userLastUpdatorCode = codeUser;
            livreOre.dateCreation = moment(new Date).format('L');
            livreOre.dateLastUpdate = moment(new Date).format('L');
            db.collection('LivreOre').insertOne(
                livreOre,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
             // db.close();
             
    

}else{
callback(true,null);
}
},
updateLivreOre: function (db,livreOre,codeUser, callback){

   
    livreOre.dateLastUpdate = moment(new Date).format('L');
    
    livreOre.dateLivreOreObject = utils.isUndefined(livreOre.dateLivreOreObject)? 
    new Date():livreOre.dateLivreOreObject;
    if(utils.isUndefined(livreOre.numeroLivreOre))
        {
           const currentD =  moment(new Date());
          // const genereKey = shortid.generate();
           const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.minutes().toString() ;
           livreOre.numeroLivreOre = 'LIVORE' +currentD.day().toLocaleString()+ generatedCode;
        }
   
 

    const criteria = { "code" : livreOre.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
    "dateLivreOreObject" : livreOre.dateLivreOreObject,  
    "montantVentes" : livreOre.montantVentes, 
    "montantAchats" : livreOre.montantAchats ,
     "montantRH" : livreOre.montantRH, 
     "montantCharges":livreOre.montantCharges,
     "description" : livreOre.description,
      "etat": livreOre.etat,// ouvert/initial/en cours de validation /validé /en cours de validation /cloturé
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": livreOre.dateLastUpdate, 
    } ;
            db.collection('LivreOre').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },function(livreOre,err){
                    if(err)
                    callback(false,err);
                    else
                        callback(true,true);
                    
                    
                }
            ) ;
     
    
    },
    
getListLivresOres : function (db,codeUser,pageNumber, nbElementsPerPage , filter,callback)
{
    let listLivresOres = [];
   
  //console.log('filter recieved is :',filter);
  let filterObject = JSON.parse(filter);
 
   let filterData = {
       "userCreatorCode" : codeUser
   };
   const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
   
   var cursor =  db.collection('LivreOre').find( 
    conditionBuilt
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(livreOre,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        livreOre.dateLivreOre= moment(livreOre.dateLivreOreObject).format('L');
        livreOre.periodeFrom= moment(livreOre.periodeFromObject).format('L');
        livreOre.periodeTo= moment(livreOre.periodeToObject).format('L');
       listLivresOres.push(livreOre);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listLivresOres); 
   });
     //return [];
},
deleteLivreOre: function (db,codeLivreOre,codeUser,callback){
    const criteria = { "code" : codeLivreOre, "userCreatorCode":codeUser };
    db.collection('LivreOre').deleteOne(
        criteria , function(err,result){
            if(err){
                
                callback(null,null);
            }else{
                callback(false,result);
            }
        }
    ) ;
},

getLivreOre: function (db,codeLivreOre,codeUser,callback)
{
    const criteria = { "code" : codeLivreOre, "userCreatorCode":codeUser };
  
       const livreOre=  db.collection('LivreOre').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
        // callback(false,client);
                 
},
getTotalLivresOres : function (db,codeUser,filter, callback)
{
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
   var totalLivresOres =  db.collection('LivreOre').find( 
    conditionBuilt
).count(function(err,result){
        callback(null,result); 
    }
);
   


     //return [];
},
getPageLivresOres : function (db,codeUser,pageNumber, nbElementsPerPage ,filter, callback)
{
   //  console.log('filter OperationCourantes',filter);
    let listLivresOres = [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
      console.log('condition',conditionBuilt);
      
   var cursor =  db.collection('LivreOre').find( conditionBuilt
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(livreOre,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        livreOre.dateLivreOre= moment(livreOre.dateLivreOreObject).format('L');
        livreOre.periodeFrom= moment(livreOre.periodeFromObject).format('L');
        livreOre.periodeTo= moment(livreOre.periodeToObject).format('L');
        
        listLivresOres.push(livreOre);
   }).then(function(){
    // console.log('list credits',listCredits);
    callback(null,listLivresOres); 
   });
     //return [];
},
buildFilterCondition(filterObject,filterData)
{
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.numeroLivreOre))
        {
                filterData["numeroLivreOre"] = filterObject.numeroLivreOre;
        }
         
        if(!utils.isUndefined(filterObject.etat))
          {
                 
                    filterData["etat"] = filterObject.etat;
                    
         }
         if(!utils.isUndefined(filterObject.montantVentes))
             {
                    
                       filterData["montantVentes"] = filterObject.montantVentes ;
                       
            }
            if(!utils.isUndefined(filterObject.montantAchats))
                {
                       
                          filterData["montantAchats"] = filterObject.montantAchats ;
                          
               }
               if(!utils.isUndefined(filterObject.montantCharges))
                {
                       
                          filterData["montantCharges"] = filterObject.montantCharges ;
                          
               }
            if(!utils.isUndefined(filterObject.dateLivreOreToObject))
             {
                    
                       filterData["dateLivreOreObject"] = {$lt: filterObject.dateLivreOreToObject};
                       
            }
   
       
     
        }
        return filterData;
}

}
module.exports.LivresOres = LivresOres;