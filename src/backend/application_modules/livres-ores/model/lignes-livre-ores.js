//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var LignesLivresOres= {
addLigneLivreOre : function (db,ligneLivreOre,codeUser,callback){
    if(ligneLivreOre != undefined){
        ligneLivreOre.code  = utils.isUndefined(ligneLivreOre.code)?shortid.generate():ligneLivreOre.code;
        if(utils.isUndefined(ligneLivreOre.numeroLigneLivreOre))
            {
               const currentD =  moment(new Date());
              // const genereKey = shortid.generate();
              const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.minutes().toString()+
              currentD.secondes().toString();
              ligneLivreOre.numeroLigneLivreOre = 'LILIV' +currentD.day().toLocaleString()+ generatedCode;
            }
            ligneLivreOre.userCreatorCode = codeUser;
            ligneLivreOre.userLastUpdatorCode = codeUser;
            ligneLivreOre.dateCreation = moment(new Date).format('L');
            ligneLivreOre.dateLastUpdate = moment(new Date).format('L');
            db.collection('LigneLivreOre').insertOne(
                ligneLivreOre,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,ligneLivreOre);
                   }
                }
              ) ;
             // db.close();
             
      

}else{
callback(true,null);
}
},
updateLigneLivreOre: function (db,ligneLivreOre,codeUser, callback){
    const criteria = { "code" : ligneLivreOre.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
    "code" : ligneLivreOre.code, 
    "numeroJournal" : ligneLivreOre.numeroJournal, 
    "dateOperationObject" : ligneLivreOre.dateOperationObject, 
    "description" : ligneLivreOre.description,
     "codeOrganisation" : ligneLivreOre.codeOrganisation,
     "montantAchats" : ligneLivreOre.montantAchats, 
     "montantVentes" : ligneLivreOre.montantVentes, 
     "montantRH" : ligneLivreOre.montantRH, 
     "montantCharges" : ligneLivreOre.montantCharges, 
     "etat":ligneLivreOre.etat,
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": ligneLivreOre.dateLastUpdate, 
    } ;
            db.collection('LigneLivreOre').updateOne(
                criteria,  { 
                     $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                     callback(true,null);
                    }else{
                     callback(false,ligneLivreOre);
                    }
                 }

            ) ;
      
    
    },
getListLignesLivresOres: function (db,codeUser,callback)
{
    let listLignesLivresOres = [];
    
   var cursor =  db.collection('LigneLivreOre').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(ligneLivreOre,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        
        listLignesLivresOres.push(ligneLivreOre);
   }).then(function(){
    // console.log('list documents credits',listDocumentsCredits);
    callback(null,listLignesLivresOres); 
   });

},
deleteLigneLivreOre: function (db,codeLigneLivreOre,codeUser,callback){
    const criteria = { "code" : codeLigneLivreOre, "userCreatorCode":codeUser };
      
        
            db.collection('LigneLivreOre').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                     callback(true,null);
                    }else{
 
                        callback(false,codeLigneLivreOre);                    }
                 }
            ) ;
},

getLigneLivreOre: function (db,codeLigneLivreOre,codeUser,callback)
{
    const criteria = { "code" : codeLigneLivreOre, "userCreatorCode":codeUser };
    
       const ligneLivreOre =  db.collection('LigneLivreOre').findOne(
            criteria , function(err,result){
                if(err){
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
          
},


getListLignesLivresOresByCodeLivreOre: function (db,codeUser,codeLivreOre,callback)
{
    let listLignesLivresOres = [];
     
   var cursor =  db.collection('LigneLivreOre').find( 
       {"userCreatorCode" : codeUser, "codeLivreOre": codeLivreOre}
    );
   cursor.forEach(function(ligneLivreOre,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        
        listLignesLivresOres.push(ligneLivreOre);
   }).then(function(){
   // console.log('list credits',listCredits);
    callback(null,listLignesLivresOres); 
   });
   
},
}
module.exports.LignesLivresOres = LignesLivresOres;