
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var historiquesClients = require('../model/historiques-clients').HistoriquesClients;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addHistoriqueClient',function(req,res,next){
    
    console.log(" ADD HISTORIQUE CLIENT IS CALLED") ;
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = historiquesClients.addHistoriqueClient(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,regAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_HISTORIQUE_CLIENT',
                error_content: err
            });
         }else{
       
        return res.json(regAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateHistoriqueClient',function(req,res,next){
    
       console.log(" UPDATE HISTORIQUE CLIENT IS CALLED") ;
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = reglementsDette.updateHistoriqueClient(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,regUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_HISTORIQUE_CLIENT',
                error_content: err
            });
         }else{
       
        return res.json(regUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getHistoriquesClients',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
      console.log("GET LIST HISTORIQUE CLIENTS C IS CALLED");
    historiquesClients.getListHistoriquesClients(
        req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
        req.query.filter,  function(err,listRegs){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_HISTORIQUE_CLIENTS',
                 error_content: err
             });
          }else{
        
         return res.json(listRegs);
          }
      });

  });
  router.get('/getPageHistoriquesClients',function(req,res,next){
    
        var decoded = jwt.decode(req.headers.authorization.slice( 
            req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
      
          console.log("GET PAGE HISTORIQUE CLIENTS IS CALLED");
        historiquesClients.getPageHistoriquesClients(
            req.app.locals.dataBase,decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,
            req.query.filter, function(err,listOperations){
              if(err){
                 return  res.json({
                     error_message : 'ERROR_GET_PAGE_HISTORIQUE_CLIENTS',
                     error_content: err
                 });
              }else{
            
             return res.json(listOperations);
              }
          });
    
      });
  router.get('/getHistoriqueClient/:codeHistoriqueClient',function(req,res,next){
    console.log("GET HISTORIQUE CLIENT IS CALLED");
  //  console.log(req.params['codeDette']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    historiquesClients.getHistoriqueClient(
        req.app.locals.dataBase,req.params['codeHistoriqueClient'],
        decoded.userData.code, function(err,operationCourante){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_HISTORIQUE_CLIENT',
               error_content: err
           });
        }else{
      
       return res.json(operationCourante);
        }
    });
})

  router.delete('/deleteHistoriqueClient/:codeHistoriqueClient',function(req,res,next){
    
       console.log(" DELETE HISTORIQUE CLIENT IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = historiquesClients.deleteHistoriqueClient(
        req.app.locals.dataBase,req.params['codeHistoriqueClient'],decoded.userData.code, function(err,codeHistoriqueClient){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_HISTORIQUE_CLIENT',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeHistoriqueClient']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });



 router.get('/getTotalHistoriquesClients',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    historiquesClients.getTotalHistoriquesClients(
        req.app.locals.dataBase,decoded.userData.code,req.query.filter,
        function(err,totalHistoriquesClients){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_HISTORIQUE_CLIENTS',
                 error_content: err
             });
          }else{
        
         return res.json({'totalHistoriquesClients':totalHistoriquesClients});
          }
      });

  });
  module.exports.routerHistoriquesClients = router;