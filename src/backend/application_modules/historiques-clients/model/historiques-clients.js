//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var HistoriquesClients = {
addHistoriqueClient : function (db,historiqueClient,codeUser,callback){
    if(historiqueClient != undefined){
        historiqueClient.code  = utils.isUndefined(historiqueClient.code)?shortid.generate():historiqueClient.code;

        historiqueClient.dateHistoriqueClientObject = utils.isUndefined(historiqueClient.dateHistoriqueClientObject)? 
        new Date():historiqueClient.dateHistoriqueClientObject;
        if(utils.isUndefined(historiqueClient.numeroHistoriqueClient))
            {
               const currentD =  moment(new Date());
              // const genereKey = shortid.generate();
              const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.day().toString()+
              shortid.generate();
              historiqueClient.numeroHistoriqueClient = 'HIS@CL' +generatedCode;
            }
       
            historiqueClient.userCreatorCode = codeUser;
            historiqueClient.userLastUpdatorCode = codeUser;
            historiqueClient.dateCreation = moment(new Date).format('L');
            historiqueClient.dateLastUpdate = moment(new Date).format('L');
            
            db.collection('HistoriqueClient').insertOne(
                historiqueClient,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
             // db.close();
             
    

}else{
callback(true,null);
}
},
updateHistoriqueClient: function (db,historiqueClient,codeUser, callback){

   
    historiqueClient.dateLastUpdate = moment(new Date).format('L');
    
    historiqueClient.dateHistoriqueClientObject = utils.isUndefined(historiqueClient.dateHistoriqueClientObject)? 
    new Date():historiqueClient.dateHistoriqueClientObject;

 

    const criteria = { "code" : historiqueClient.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {
    "dateHistoriqueClientObject" : historiqueClient.dateHistoriqueClientObject, 
     "client": historiqueClient.client,
     "montantMisEnValeur": historiqueClient.montantMisEnValeur,
     "dateAchatsObject": historiqueClient.dateAchatsObject,
     "dateReglementObject": historiqueClient.dateReglementObject,
     "additionInfos": historiqueClient.additionInfos,
     "description": historiqueClient.description,
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": historiqueClient.dateLastUpdate, 
    } ;
            db.collection('HistoriqueClient').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },function(historiqueClient,err){
                    if(err)
                    callback(false,err);
                    else
                        callback(true,true);
                    
                    
                }
            ) ;
     
    
    },
   
getListHistoriquesClients : function (db,codeUser,pageNumber, nbElementsPerPage , filter,callback)
{
    let listHistoriquesClients = [];
   
  //console.log('filter recieved is :',filter);
  let filterObject = JSON.parse(filter);
 
   let filterData = {
       "userCreatorCode" : codeUser
   };
   const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
   
   var cursor =  db.collection('HistoriqueClient').find( 
    conditionBuilt
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(historiqueClient,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        historiqueClient.dateHistoriqueClient= moment(historiqueClient.dateHistoriqueClientObject).format('L');
        historiqueClient.periodeFrom= moment(historiqueClient.periodeFromObject).format('L');
        historiqueClient.periodeTo= moment(historiqueClient.periodeToObject).format('L');
       listHistoriquesClients.push(historiqueClient);
   }).then(function(){
    // console.log('list credits',listDettes);
    callback(null,listHistoriquesClients); 
   });
     //return [];
},
deleteHistoriqueClient: function (db,codeHistoriqueClient,codeUser,callback){
    const criteria = { "code" : codeHistoriqueClient, "userCreatorCode":codeUser };
    
    db.collection('HistoriqueClient').deleteOne(
        criteria , function(err,result){
            if(err){
                
                callback(null,null);
            }else{
                callback(false,result);
            }
        }
    ) ;
},

getHistoriqueClient: function (db,codeHistoriqueClient,codeUser,callback)
{
    const criteria = { "code" : codeHistoriqueClient, "userCreatorCode":codeUser };
  
       const historiqueClient =  db.collection('HistoriqueClient').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
        // callback(false,fournisseur);
                 
},
getTotalHistoriquesClients : function (db,codeUser,filter, callback)
{
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
   var totalHistoriquesClients =  db.collection('HistoriqueClient').find( 
    conditionBuilt
).count(function(err,result){
        callback(null,result); 
    }
);
   


     //return [];
},
getPageHistoriquesClients : function (db,codeUser,pageNumber, nbElementsPerPage ,filter, callback)
{
    console.log('filter HistoriqueClients',filter);
    let listHistoriquesClients = [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const conditionBuilt =   this.buildFilterCondition(filterObject,filterData);
      console.log('condition',conditionBuilt);
      
      
   var cursor =  db.collection('HistoriqueClient').find( conditionBuilt
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(historiqueClient,err){
       if(err)
        {
            console.log('errors produced :', err);
            callback(null,false); 
            
        }
        historiqueClient.dateHistoriqueClient= moment(historiqueClient.dateHistoriqueClientObject).format('L');
        historiqueClient.periodeFrom= moment(historiqueClient.periodeFromObject).format('L');
        historiqueClient.periodeTo= moment(historiqueClient.periodeToObject).format('L');
        
       listHistoriquesClients.push(historiqueClient);
   }).then(function(){
    // console.log('list credits',listDettes);
    callback(null,listHistoriquesClients); 
   });
     //return [];
},
buildFilterCondition(filterObject,filterData)
{
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.numeroHistoriqueClient))
        {
                filterData["numeroHistoriqueClient"] = filterObject.numeroHistoriqueClient;
        }
         
   
         if(!utils.isUndefined(filterObject.dateAchatsFromObject))
             {
                    
                       filterData["dateAchatsObject"] = {$gt: filterObject.dateAchatsFromObject};
                       
            }
            if(!utils.isUndefined(filterObject.dateAchatsToObject))
             {
                    
                       filterData["dateAchatsObject"] = {$lt: filterObject.dateAchatsToObject};
                       
            }
            if(!utils.isUndefined(filterObject.dateReglementFromObject))
                {
                       
                          filterData["dateReglementObject"] = {$gt: filterObject.dateReglementFromObject};
                          
               }
            if(!utils.isUndefined(filterObject.dateReglementToObject))
                {
                       
                          filterData["dateReglementObject"] = {$lt: filterObject.dateReglementToObject};
                          
               }

            if(!utils.isUndefined(filterObject.cinClient))
             {
                    
                       filterData["client.cin"] = filterObject.cinClient ;
                       
            }
            if(!utils.isUndefined(filterObject.nomClient))
             {
                    
                       filterData["client.nom"] = filterObject.nomClient ;
                       
            }
            if(!utils.isUndefined(filterObject.prenomClient))
             {
                    
                       filterData["client.prenom"] = filterObject.prenomClient ;
                       
            }
            if(!utils.isUndefined(filterObject.raisonClient))
             {
                    
                       filterData["client.raisonSociale"] = filterObject.raisonClient ;
                       
            }
            if(!utils.isUndefined(filterObject.matriculeClient))
             {
                    
                       filterData["client.matriculeFiscale"] = filterObject.matriculeClient ;
                       
            }
            if(!utils.isUndefined(filterObject.registreClient))
             {
                    
                       filterData["client.registreCommerce"] = filterObject.registreClient ;
                       
            }
        
            if(!utils.isUndefined(filterObject.montantMisEnValeur))
             {
                    
                       filterData["montantMisEnValeur"] = filterObject.montantMisEnValeur ;
                       
            }
   
     
        }
        return filterData;
},
}
module.exports.HistoriquesClients = HistoriquesClients;