

var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var organisations = require('../model/organisations').Organisations;
var jwt = require('jsonwebtoken');

router.post('/addOrganisation',function(req,res,next){
    
       console.log(" ADD ORGANISATION IS CALLED") ;
       console.log(" THE ORGANISATION TO ADDED IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = organisations.addOrganisation(req.app.locals.dataBase,req.body,
        decoded.userData.code,function(err,organisationAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_ORGANISATION',
                error_content: err
            });
         }else{
       
        return res.json(organisationAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.put('/updateOrganisation',function(req,res,next){
    
       console.log(" UPDATE ORGANISATION IS CALLED") ;
       console.log(" THE ORGANISATION TO UPDATE IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = organisations.updateOrganisation(req.app.locals.dataBase,req.body,
        decoded.userData.code,function(err,organisationUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_ORGANISATION',
                error_content: err
            });
         }else{
       
        return res.json(organisationUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getOrganisations',function(req,res,next){
    console.log("GET LIST ORGANISATIONS IS CALLED");
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    organisations.getListOrganisations(req.app.locals.dataBase,
        decoded.userData.code,function(err,listOrganisations){
       console.log("GET LIST ORGANISATIONS : RESULT");
       console.log(listOrganisations);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIST_ORGANISATIONS',
               error_content: err
           });
        }else{
      
       return res.json(listOrganisations);
        }
    });
});
router.get('/getPageOrganisations',function(req,res,next){
    //  console.log("GET LIST CLIENTS IS CALLED");
     var decoded = jwt.decode(req.headers.authorization.slice( 
         req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
     organisations.getPageOrganisations(req.app.locals.dataBase,decoded.userData.code, 
        req.query.pageNumber, req.query.nbElementsPerPage ,
         req.query.filter,
         function(err,listOrganisations){
       //  console.log("GET LIST CLIENTS : RESULT");
       //  console.log(listArticles);
         if(err){
            return  res.json({
                error_message : 'ERROR_GET_PAGE_ORGANISATIONS',
                error_content: err
            });
         }else{
       
        return res.json(listOrganisations);
         }
     });
 });
router.get('/getOrganisation/:codeOrganisation',function(req,res,next){
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    organisations.getOrganisation(req.app.locals.dataBase,req.params['codeOrganisation'],
    decoded.userData.code,function(err,organisation){
      //  console.log("GET  GROUPE : RESULT");
      //  console.log(groupe);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_ORGANISATION',
               error_content: err
           });
        }else{
      
       return res.json(organisation);
        }
    });
})

router.delete('/deleteOrganisation/:codeOrganisation',function(req,res,next){
    
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = organisations.deleteOrganisation(req.app.locals.dataBase,req.params['codeOrganisation'],
       decoded.userData.code,function(err,codeOrganisation){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_ORGANISATION',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeOrganisation']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
  router.get('/getTotalOrganisations',function(req,res,next){
    
     var decoded = jwt.decode(req.headers.authorization.slice( 
         req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
         organisations.getTotalOrganisations(req.app.locals.dataBase,decoded.userData.code,
         req.query.filter, function(err,totalOrganisations){
           if(err){
              return  res.json({
                  error_message : 'ERROR_GET_TOTAL_ORGANISATIONS',
                  error_content: err
              });
           }else{
         
          return res.json({'totalOrganisations':totalOrganisations});
           }
       });
    
   });
module.exports.organisationsRouter = router;