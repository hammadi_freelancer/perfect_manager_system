//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');
//we need to have article as a json object : {code :'mmm', design :'ddd'}
var Organisations = {
addOrganisation : function (db,organisation,codeUser, callback){
    if(organisation != undefined){
        organisation.code  = utils.isUndefined(organisation.code)?shortid.generate():organisation.code;
        organisation.userCreatorCode = codeUser;
        organisation.userLastUpdatorCode = codeUser;
        organisation.dateCreation = moment(new Date).format('L');
        organisation.dateLastUpdate = moment(new Date).format('L');
        
        db.collection('Organisation').insertOne(
            organisation,
            function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            } 
          ) ;
     
}
//return false;
},
updateOrganisation: function (db,organisation, codeUser,callback){
    const criteria = { "code" : organisation.code };
    organisation.userCreatorCode = codeUser;
  
   // roles : ADMIN , USER , VISITOR
    const dataToUpdate = {
        "raisonSociale" : organisation.raisonSociale,
        "matriculeFiscale" : organisation.matriculeFiscale,
        "registreCommerce" : organisation.registreCommerce,
        "adresse" : organisation.adresse,
        "telephone" : organisation.adresse,
        "responsable" : organisation.responsable,
   "description" : organisation.description,
    "utilisateurs": organisation.utilisateurs,
    "userLastUpdatorCode":codeUser,
    "dateLastUpdate": moment(new Date).format('L')
} ;
       
            db.collection('Organisation').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                } 
            ) ;
      
    
    },
getListOrganisations : function (db,codeUser,callback)
{
    let listOrganisations = [];
 
   var cursor =  db.collection('Organisation').find();
   cursor.forEach(function(organisation,err){
       if(err)
        {
            console.log('errors produced :', err);

        }
        listOrganisations.push(organisation);
   }).then(function(){
    callback(null,listOrganisations); 
   });
   
},





deleteOrganisation: function (db,codeOrganisation,codeUser,callback){
    const criteria = { "code" : codeOrganisation };
      
            db.collection('Organisation').deleteOne(
                criteria ,
                function(err,result){
                    if(err){                        
                        callback(null,null);
                    }else{
                        callback(false,result);
                    }
                }    
            ) ;
        
},
getOrganisation: function (db,codeOrganisation,codeUser,callback)
{
    const criteria = { "code" : codeOrganisation };

       const organisation =  db.collection('Organisation').findOne(
            criteria , function(err,result){
                if(err){
                    callback(null,null);
                }else{
                    callback(false,result);
                }
            }
        ) ;
                      
},
getTotalOrganisations: function (db,codeUser,filter, callback)
{
       
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
   var totalOrs =  db.collection('Organisation').find( 
    condition
    ).count(function(err,result){
        callback(null,result); 
    });   //return [];
},
buildFilterCondition(filterObject,filterData)
{
  
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.cin))
        {
                filterData["code"] = filterObject.code;
        }
         
        if(!utils.isUndefined(filterObject.nom))
          {
                 
                    filterData["raisonSociale"] = filterObject.raisonSociale;
                    
         }
         if(!utils.isUndefined(filterObject.description))
             {
                    
                       filterData["description"] =filterObject.description;
                       
            }
            if(!utils.isUndefined(filterObject.matriculeFiscale))
                {
                       
                          filterData["matriculeFiscale"] =filterObject.matriculeFiscale;
                          
               }
               if(!utils.isUndefined(filterObject.registreCommerce))
                {
                       
                          filterData["registreCommerce"] =filterObject.registreCommerce;
                          
               }
               if(!utils.isUndefined(filterObject.adresse))
                {
                       
                          filterData["adresse"] =filterObject.adresse;
                          
               }
               if(!utils.isUndefined(filterObject.responsable))
                {
                       
                          filterData["responsable"] =filterObject.responsable;
                          
               }
               if(!utils.isUndefined(filterObject.telephone))
                {
                       
                          filterData["telephone"] =filterObject.telephone;
                          
               }
        }
        return filterData;
},
getPageOrganisations : function (db,codeUser,pageNumber,nbElementsPerPage, filter,callback)
{
    let listOrganisations = [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var cursor =  db.collection('Organisation').find(
      condition
   ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
   .limit(Number(nbElementsPerPage) );
   
   cursor.forEach(function(organisation,err){
       if(err)
        {
            console.log('errors produced :', err);

        }
        // utilisateur.dateNaissance = moment(utilisateur.dateNaissanceObject).format('L');
        // utilisateur.dateInscription = moment(utilisateur.dateInscriptionObject).format('L');
        listOrganisations.push(organisation);
   }).then(function(){
   //  console.log('list clients',listClients);
    callback(null,listOrganisations); 
   });
   


}
}
module.exports.Organisations = Organisations;