//var db = require('../common/MongoDbConnection').getDBConnection;
const mongoClient = require('mongodb').MongoClient;
const url ='mongodb://localhost:27017';
const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var DocumentsFournisseurs = {
addDocumentFournisseur : function (db,documentFournisseur,codeUser,callback){
    if(documentFournisseur != undefined){
        documentFournisseur.code  = utils.isUndefined(documentFournisseur.code)?shortid.generate():documentFournisseur.code;
        documentFournisseur.dateReceptionObject = utils.isUndefined(documentFournisseur.dateReceptionObject)? 
        new Date():documentFournisseur.dateReceptionObject;
        documentFournisseur.userCreatorCode = codeUser;
        documentFournisseur.userLastUpdatorCode = codeUser;
        documentFournisseur.dateCreation = moment(new Date).format('L');
        documentFournisseur.dateLastUpdate = moment(new Date).format('L');
        
    
            db.collection('DocumentFournisseur').insertOne(
                documentFournisseur,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,documentFournisseur);
                   }
                }
              ) ;
      

}else{
callback(true,null);
}
},
updateDocumentFournisseur: function (db,documentFournisseur,codeUser, callback){

   
    documentFournisseur.dateReceptionObject = utils.isUndefined(documentFournisseur.dateReceptionObject)? 
    new Date():documentFournisseur.dateReceptionObject;

    const criteria = { "code" : documentFournisseur.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"dateReceptionObject" : documentFournisseur.dateReceptionObject, 
    "codeFournisseur" : documentFournisseur.codeFournisseur, 
    "description" : documentFournisseur.description,
     "codeOrganisation" : documentFournisseur.codeOrganisation,
     "typeDocument" : documentFournisseur.typeDocument, // cheque , virement , espece 
     "numeroDocument" : documentFournisseur.numeroDocument, 
     "imageFace1":documentFournisseur.imageFace1,
     "imageFace2" : documentFournisseur.imageFace2 , 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": documentFournisseur.dateLastUpdate, 
    } ;
   
            db.collection('DocumentFournisseur').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
      
    
    },
getListDocumentsFournisseurs : function (db,codeUser,callback)
{
    let listDocumentsFournisseurs = [];
    
   var cursor =  db.collection('DocumentFournisseur').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(docFournisseur,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        docFournisseur.dateReception = moment(docFournisseur.dateReceptionObject).format('L');
        
        listDocumentsFournisseurs.push(docFournisseur);
   }).then(function(){
    
    callback(null,listDocumentsFournisseurs); 
   });
   


     //return [];
},
deleteDocumentFournisseur: function (db,codeDocumentFournisseur,codeUser,callback){
    const criteria = { "code" : codeDocumentFournisseur, "userCreatorCode":codeUser };
      
            db.collection('DocumentFournisseur').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
     
},

getDocumentFournisseur: function (db,codeDocumentFournisseur,codeUser,callback)
{
    const criteria = { "code" : codeDocumentFournisseur, "userCreatorCode":codeUser };
 
       const docFour =  db.collection('DocumentFournisseur').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
        // callback(false,client);
                      
},


getListDocumentsFournisseursByCodeFournisseur : function (db,codeUser,codeFournisseur,callback)
{
    let listDocumentsFournisseurs = [];
  
   var cursor =  db.collection('DocumentFournisseur').find( 
       {"userCreatorCode" : codeUser, "codeFournisseur": codeFournisseur}
    );
   cursor.forEach(function(documentFournisseur,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        documentFournisseur.dateReception = moment(documentFournisseur.dateReceptionObject).format('L');
        
        listDocumentsFournisseurs.push(documentFournisseur);
   }).then(function(){
  // console.log('list doc credits',listCredits);
 
    callback(null,listDocumentsFournisseurs); 
   });
   
}
}
module.exports.DocumentsFournisseurs = DocumentsFournisseurs;