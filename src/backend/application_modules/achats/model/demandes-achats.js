//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var DemandesAchats = {
addDemandeAchats : function (db,demandeAchats,codeUser, callback){


    if(demandeAchats != undefined){
        demandeAchats.code  = utils.isUndefined(demandeAchats.code)?shortid.generate():demandeAchats.code;
       if(utils.isUndefined(demandeAchats.numeroDemandeAchats))
        {
           const currentD =  moment(new Date());
          // const genereKey = shortid.generate();
           const generatedCode = currentD.year().toString() +'/'+currentD.utc(true).month().toString()+currentD.minutes().toString() ;
           demandeAchats.numeroDemandeAchats = 'DEMA' + generatedCode;

           }
           const listRows =   demandeAchats.listLignesDemandeAchats;
           listRows.forEach((row)=>(row.codeDemandeAchats =demandeAchats.code, row.numeroDemandeAchats=demandeAchats.numeroDemandeAchats ));
           demandeAchats.listLignesDemandesAchats  = listRows;
     
           demandeAchats.dateDemandeAchatsObject = utils.isUndefined(demandeAchats.dateDemandeAchatsObject)? 
        new Date():demandeAchats.dateDemandeAchatsObject;
        demandeAchats.userCreatorCode = codeUser;
        demandeAchats.userLastUpdatorCode = codeUser;
        demandeAchats.dateCreation = moment(new Date).format('L');
        demandeAchats.dateLastUpdate = moment(new Date).format('L');
      
            db.collection('DemandeAchats').insertOne(
                demandeAchats,function(err,result){
                  //  clientDB.close();
                   // mongoClient.clos
                   if(err){

                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
 
}else{
callback(true,null);
}
},

updateDemandeAchats: function (db,demandeAchats,codeUser, callback){

    //factureVente.dateFacturation = utils.isUndefined(factureVente.dateFacturation)? 
    //moment(new Date).format('L'): moment(factureVente.dateFacturation).format('L');
    const listRows =   demandeAchats.listLignesDemandesAchats;
    listRows.forEach((row)=>(row.codeDemandeAchats =demandeAchats.code,
         row.numeroDemandeAchats=demandeAchats.numeroDemandeAchats ));
         demandeAchats.listLignesDemandesAchats  = listRows;




         demandeAchats.dateDemandeAchatsObject = utils.isUndefined(demandeAchats.dateDemandeAchatsObject)? 
    new Date():demandeAchats.dateDemandeAchatsObject;

    const dateLastUpdate = moment(new Date).format('L');
    const criteria = { "code" : demandeAchats.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"fournisseur" : demandeAchats.fournisseur,
     "dateDemandeAchatsObject" : demandeAchats.dateDemandeAchatsObject ,
     "numeroDemandeAchats" : demandeAchats.numeroDemandeAchats, 
     "budgetPrepare" : demandeAchats.budgetPrepare, 
     "dateDisponibiliteObject": demandeAchats.dateDisponibiliteObject, 
     "besoinUrgentToutes" : demandeAchats.besoinUrgentToutes,
     "modePayementDesire" : demandeAchats.modePayementDesire , 
      "traiteToutes" : demandeAchats.traiteToutes,
       "etat" : demandeAchats.etat,
       "listLignesDemandesAchats": demandeAchats.listLignesDemandesAchats,
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": dateLastUpdate, 
    } ;

            db.collection('DemandeAchats').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
      
    
    },
getListDemandesAchats : function (db,codeUser, callback)
{
    let listDemandesAchats = [];
   
   var cursor =  db.collection('DemandeAchats').find(
    {"userCreatorCode" : codeUser}
   );
   
   cursor.forEach(function(demandeAchats,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        demandeAchats.dateDemandeAchats = moment(demandeAchats.dateDemandeAchatsObject).format('L');
        listDemandesAchats.push(demandeAchats);
   }).then(function(){
    // console.log('list factures ventes ',listFacturesVentes);
    callback(null,listDemandesAchats); 
   });
   

},
getPageDemandesAchats : function (db,codeUser,pageNumber,nbElementsPerPage,filter, callback)
{
    let listDemandesAchats = [];
    let filterObject = JSON.parse(filter);
    let filterData = {
        "userCreatorCode" : codeUser
    };

const condition = this.buildFilterCondition(filterObject,filterData);
   var cursor =  db.collection('DemandeAchats').find(
    condition
   ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
   .limit(Number(nbElementsPerPage) );
   
   cursor.forEach(function(demandeAchats,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        demandeAchats.dateDemandeAchats = moment(demandeAchats.dateDemandeAchatsObject).format('L');
        listDemandesAchats.push(demandeAchats);
   }).then(function(){
    // console.log('list factures ventes ',listFacturesVentes);
    callback(null,listDemandesAchats); 
   });
   

},
deleteDemandeAchats: function (db,codeDemandeAchats,codeUser, callback){
    const criteria = { "code" : codeDemandeAchats, "userCreatorCode":codeUser };
    
    
            db.collection('DemandeAchats').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
      
},

getDemandeAchats: function (db,codeDemandeAchats,codeUser, callback)
{
    const criteria = { "code" : codeDemandeAchats, "userCreatorCode":codeUser };
    
 
       const demandeAchats =  db.collection('DemandeAchats').findOne(
            criteria , function(err,result){
                if(err){
                    //clientDB.close();
                    
                    callback(null,null);
                }else{
                   // clientDB.close();
                    
                    callback(false,result);
                }
            }
        ) ;
                    
},

getTotalDemandesAchats: function (db,codeUser,filter, callback)
{
    let filterObject = JSON.parse(filter);
    let filterData = {
        "userCreatorCode" : codeUser
    };

const condition = this.buildFilterCondition(filterObject,filterData);
   var totalDemandesAchats =  db.collection('DemandeAchats').find( 
    condition
    ).count(function(err,result){
        callback(null,result); 
    }
);
     //return [];
},
buildFilterCondition(filterObject,filterData)
{
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.numeroDemandeVentes))
        {
                filterData["numeroDemandeAchats"] = filterObject.numeroDemandeAchats;
        }
         
        if(!utils.isUndefined(filterObject.nom))
          {
                 
                    filterData["nom"] = filterObject.nom;
                    
         }
         if(!utils.isUndefined(filterObject.prenom))
             {
                    
                       filterData["prenom"] =filterObject.prenom;
                       
            }
            if(!utils.isUndefined(filterObject.cin))
             {
                    
                       filterData["cin"] = filterObject.cin;
                       
            }
            if(!utils.isUndefined(filterObject.raisonSociale))
             {
                    
                       filterData["raisonSociale"] = filterObject.raisonSociale ;
                       
            }
            if(!utils.isUndefined(filterObject.matriculeFiscale))
             {
                    
                       filterData["matriculeFiscale"] = filterObject.matriculeFiscale ;
                       
            }
            if(!utils.isUndefined(filterObject.registreCommerce))
             {
                    
                       filterData["registreCommerce"] = filterObject.registreCommerce ;
                       
            }
            if(!utils.isUndefined(filterObject.dateDemandeAchatsFromObject))
             {
                    
                       filterData["dateDemandeAchatsObject"] = {$gt: filterObject.dateDemandeAchatsFromObject};
                       
            }
            if(!utils.isUndefined(filterObject.dateDemandeAchatsToObject))
             {
                    
                       filterData["dateDemandeAchatsToObject"] = {$lt: filterObject.dateDemandeAchatsToObject};
                       
            }
            
            if(!utils.isUndefined(filterObject.dateDisponibiliteFromObject))
                {
                       
                          filterData["dateDisponibiliteObject"] = {$gt: filterObject.dateDisponibiliteFromObject};
                          
               }
               if(!utils.isUndefined(filterObject.dateDisponibiliteToObject))
                {
                       
                          filterData["dateDisponibiliteObject"] = {$lt: filterObject.dateDisponibiliteToObject};
                          
               }
            if(!utils.isUndefined(filterObject.budgetPrepare))
             {
                    
                       filterData["budgetPrepare"] = filterObject.budgetPrepare ;
                       
            }
            if(!utils.isUndefined(filterObject.modePayementDesire))
                {
                       
                          filterData["modePayementDesire"] = filterObject.modePayementDesire ;
                          
                }
     
            if(!utils.isUndefined(filterObject.besoinUrgentToutes))
             {
                    
                       filterData["besoinUrgentToutes"] = filterObject.besoinUrgentToutes ;
                       
            }
            if(!utils.isUndefined(filterObject.traiteToutes))
                {
                       
                          filterData["traiteToutes"] = filterObject.traiteToutes ;
                          
               }
               if(!utils.isUndefined(filterObject.etat))
                {
                       
                          filterData["etat"] = filterObject.etat ;
                          
               }
              
     
        }
        return filterData;
},

}
module.exports.DemandesAchats = DemandesAchats;