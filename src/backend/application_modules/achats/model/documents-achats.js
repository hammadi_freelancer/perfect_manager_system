//var db = require('../common/MongoDbConnection').getDBConnection;
const mongoClient = require('mongodb').MongoClient;
const url ='mongodb://localhost:27017';
const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var DocumentsAchats = {
addDocumentAchats : function (db,documentAchats,codeUser, callback){
    if(documentAchats != undefined){
        documentAchats.code  = utils.isUndefined(documentAchats.code)?shortid.generate():documentAchats.code;
        documentAchats.dateAchatsObject = utils.isUndefined(documentAchats.dateAchatsObject)? 
        new Date():documentAchats.dateAchatsObject;
        documentAchats.userCreatorCode = codeUser;
        documentAchats.userLastUpdatorCode = codeUser;
        documentAchats.dateCreation = moment(new Date).format('L');
        documentAchats.dateLastUpdate = moment(new Date).format('L');
        
            db.collection('DocumentAchats').insertOne(
                documentAchats,function(err,result){
                   // clientDB.close();
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
 

}else{
callback(true,null);
}
},
updateDocumentAchats: function (db,documentAchats,codeUser, callback){

    //factureVente.dateFacturation = utils.isUndefined(factureVente.dateFacturation)? 
    //moment(new Date).format('L'): moment(factureVente.dateFacturation).format('L');
   
    documentAchats.dateAchatsObject = utils.isUndefined(documentAchats.dateAchatsObject)? 
    new Date():documentAchats.dateAchatsObject;

    documentAchats.userLastUpdatorCode = codeUser;
    documentAchats.dateCreation = moment(new Date).format('L');
    documentAchats.dateLastUpdate = moment(new Date).format('L');
    const criteria = { "code" : documentAchats.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"client" : documentAchats.fournisseur,
     "dateAchatsObject" : documentAchats.dateAchatsObject ,
     "numeroDocumentAchats" : documentAchats.numeroDocumentAchats,
      "montantAPayer" : documentAchats.montantAPayer, 
     "montantTVA": documentAchats.montantTVA,
      "montantRemise" : documentAchats.montantRemise,
     "montantCout" : documentAchats.montantCout ,
      "nbJoursOccupationMainOeuvre" : documentAchats.nbJoursOccupationMainOeuvre ,
      "payementData" : documentAchats.payementData,
       "etat" : documentAchats.etat,
       "nbMagasins" : documentAchats.nbMagasins,
       "gestionMarchandises" : documentAchats.gestionMarchandises,
       "gestionTaxes" : documentAchats.gestionTaxes,
       "gestionLogistique" : documentAchats.gestionLogistique,
       "gestionCouts" : documentAchats.gestionCouts,
       "gestionMainOeuvres" : documentAchats.gestionMainOeuvres,
       "gestionRemises" : documentAchats.gestionRemises,
       "credit" : documentAchats.credit,
       "listLignesMarchandises": documentAchats.listLignesMarchandises,
       "listLignesCouts": documentAchats.listLignesCouts,
       "listLignesTaxes": documentAchats.listLignesTaxes,
       "listLignesLogistiques": documentAchats.listLignesLogistiques,
       "listLignesOccupationMainOeuvres": documentAchats.listLignesOccupationMainOeuvres,
       "listLignesRemises": documentAchats.listLignesRemises,
       
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": documentAchats.dateLastUpdate, 
       

    } ;
     
            db.collection('DocumentAchats').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
     
    
    },
getListDocumentsAchats : function (db,codeUser, callback)
{
    let listDocumentsAchats = [];
   
   var cursor =  db.collection('DocumentAchats').find(
    {"userCreatorCode" : codeUser}
   );
   cursor.forEach(function(documentVente,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        documentAchats.dateAchatsObject = 
        moment(documentAchats.dateAchatsObject).format('L');
        listDocumentsAchats.push(documentAchats);
   }).then(function(){
   //  console.log('list factures ventes ',listDocumentsVentes);
    callback(null,listDocumentsAchats); 
   });
   

     //return [];
},
deleteDocumentAchats: function (db,codeDocumentAchats,codeUser, callback){
    const criteria = { "code" : codeDocumentAchats, "userCreatorCode":codeUser };
    
    
            db.collection('DocumentAchats').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
    
},

getDocumentAchats: function (db,codeDocumentAchats,codeUser, callback)
{
    const criteria = { "code" : codeDocumentAchats, "userCreatorCode":codeUser };
    

       const documentAchats =  db.collection('DocumentAchats').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                    
},


}
module.exports.DocumentsAchats = DocumentsAchats;