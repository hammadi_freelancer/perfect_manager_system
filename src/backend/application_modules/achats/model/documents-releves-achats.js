//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var DocumentsRelevesAchats = {
addDocumentReleveAchats : function (db,documentReleveAchats,codeUser,callback){
    if(documentReleveAchats != undefined){
        documentReleveAchats.code  = utils.isUndefined(documentReleveAchats.code)?shortid.generate():documentReleveAchats.code;
        documentReleveAchats.dateReceptionObject = utils.isUndefined(documentReleveAchats.dateReceptionObject)? 
        new Date():documentReleveAchats.dateReceptionObject;
        documentReleveAchats.userCreatorCode = codeUser;
        documentReleveAchats.userLastUpdatorCode = codeUser;
        documentReleveAchats.dateCreation = moment(new Date).format('L');
        documentReleveAchats.dateLastUpdate = moment(new Date).format('L');
        
   
            db.collection('DocumentReleveAchats').insertOne(
                documentReleveAchats,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,documentReleveAchats);
                   }
                }
              ) ;


}else{
callback(true,null);
}
},
updateDocumentReleveAchats: function (db,documentReleveAchats,codeUser, callback){

   
    documentReleveAchats.dateReceptionObject = utils.isUndefined(documentReleveAchats.dateReceptionObject)? 
    new Date():documentReleveAchats.dateReceptionObject;

    const criteria = { "code" : documentReleveAchats.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"dateReceptionObject" : documentReleveAchats.dateReceptionObject, 
    "codeReleveAchats" : documentReleveAchats.codeReleveAchats, 
    "description" : documentReleveAchats.description,
     "codeOrganisation" : documentReleveAchats.codeOrganisation,
     "typeDocument" : documentReleveAchats.typeDocument, // cheque , virement , espece 
     "numeroDocument" : documentReleveAchats.numeroDocument, 
     "imageFace1":documentReleveAchats.imageFace1,
     "imageFace2" : documentReleveAchats.imageFace2 , 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": documentReleveAchats.dateLastUpdate, 
    } ;
    
            db.collection('DocumentReleveAchats').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
     
    
    },
getListDocumentsRelevesAchats : function (db,codeUser,callback)
{
    let listDocumentsRelevesAchats = [];
    
   var cursor =  db.collection('DocumentReleveAchats').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(docRelAchats,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        docRelAchats.dateReception = moment(docRelAchats.dateReceptionObject).format('L');
        
        listDocumentsRelevesAchats.push(docRelAchats);
   }).then(function(){
    //console.log('list documents credits',listDocumentsCredits);
    callback(null,listDocumentsRelevesAchats); 
   });
   

     //return [];
},
deleteDocumentReleveAchats: function (db,codeDocumentReleveAchats,codeUser,callback){
    const criteria = { "code" : codeDocumentReleveAchats, "userCreatorCode":codeUser };
      
            db.collection('DocumentReleveAchats').deleteOne(
                criteria,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }   
            ) ;
       
},

getDocumentReleveAchats: function (db,codeDocumentReleveAchats,codeUser,callback)
{
    const criteria = { "code" : codeDocumentReleveAchats, "userCreatorCode":codeUser };
 
       const doc =  db.collection('DocumentReleveAchats').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                     
},


getListDocumentsReleveAchatsByCodeReleve : function (db,codeUser,codeReleveAchats,callback)
{
    let listDocumentsReleveAchats = [];
      
   var cursor =  db.collection('DocumentReleveAchats').find( 
       {"userCreatorCode" : codeUser, "codeReleveAchats": codeReleveAchats}
    );
   cursor.forEach(function(documentRA,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        documentRA.dateReception = moment(documentRA.dateReceptionObject).format('L');
        
        listDocumentsReleveAchats.push(documentRA);
   }).then(function(){
  // console.log('list doc credits',listCredits);
    callback(null,listDocumentsReleveAchats); 
   });
},


}
module.exports.DocumentsRelevesAchats = DocumentsRelevesAchats;