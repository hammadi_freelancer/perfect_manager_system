//var db = require('../common/MongoDbConnection').getDBConnection;
const mongoClient = require('mongodb').MongoClient;
const url ='mongodb://localhost:27017';
const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var DocumentsFacturesAchats = {
addDocumentFactureAchats : function (db,documentFactureAchats,codeUser,callback){
    if(documentFactureAchats != undefined){
        documentFactureAchats.code  = utils.isUndefined(documentFactureAchats.code)?shortid.generate():documentFactureAchats.code;
        documentFactureAchats.dateReceptionObject = utils.isUndefined(documentFactureAchats.dateReceptionObject)? 
        new Date():documentFactureAchats.dateReceptionObject;
        documentFactureAchats.userCreatorCode = codeUser;
        documentFactureAchats.userLastUpdatorCode = codeUser;
        documentFactureAchats.dateCreation = moment(new Date).format('L');
        documentFactureAchats.dateLastUpdate = moment(new Date).format('L');
        
    
            db.collection('DocumentFactureAchats').insertOne(
                documentFactureAchats,function(err,result){
                  
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,documentFactureAchats);
                   }
                }
              ) ;
    

}else{
callback(true,null);
}
},
updateDocumentFactureAchats: function (db,documentFactureAchats,codeUser, callback){

   
    documentFactureAchats.dateReceptionObject = utils.isUndefined(documentFactureAchats.dateReceptionObject)? 
    new Date():documentFactureAchats.dateReceptionObject;

    const criteria = { "code" : documentFactureAchats.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"dateReceptionObject" : documentFactureAchats.dateReceptionObject, 
    "codeFactureAchats" : documentFactureAchats.codeFactureAchats, 
    "description" : documentFactureAchats.description,
     "codeOrganisation" : documentFactureAchats.codeOrganisation,
     "typeDocument" : documentFactureAchats.typeDocument, // cheque , virement , espece 
     "numeroDocument" : documentFactureAchats.numeroDocument, 
     "imageFace1":documentFactureAchats.imageFace1,
     "imageFace2" : documentFactureAchats.imageFace2 , 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": documentFactureAchats.dateLastUpdate, 
    } ;
      
            db.collection('DocumentFactureAchats').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
    
    
    },
getListDocumentsFacturesAchats : function (db,codeUser,callback)
{
    let listDocumentsFacturesAchats = [];
  
   var cursor =  db.collection('DocumentFactureAchats').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(docFactAchats,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        docFactAchats.dateReception = moment(docFactAchats.dateReceptionObject).format('L');
        
        listDocumentsFacturesAchats.push(docFactAchats);
   }).then(function(){
    //console.log('list documents credits',listDocumentsCredits);
    callback(null,listDocumentsFacturesAchats); 
   });

},
deleteDocumentFactureAchats: function (db,codeDocumentFactureAchats,codeUser,callback){
    const criteria = { "code" : codeDocumentFactureAchats, "userCreatorCode":codeUser };
     
            db.collection('DocumentFactureAchats').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
     
},

getDocumentFactureAchats: function (db,codeDocumentFactureAchats,codeUser,callback)
{
    const criteria = { "code" : codeDocumentFactureAchats, "userCreatorCode":codeUser };

       const doc =  db.collection('DocumentFactureAchats').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                    
},


getListDocumentsFacturesAchatsByCodeFactureAchats : function (db,codeUser,codeFactureAchats,callback)
{
    let listDocumentsFacturesAchats = [];
  
   var cursor =  db.collection('DocumentFactureAchats').find( 
       {"userCreatorCode" : codeUser, "codeFactureAchats": codeFactureAchats}
    );
   cursor.forEach(function(documentFA,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        documentFA.dateReception = moment(documentFA.dateReceptionObject).format('L');
        
        listDocumentsFacturesAchats.push(documentFA);
   }).then(function(){
  // console.log('list doc credits',listCredits);
    callback(null,listDocumentsFacturesAchats); 
   });
   
}
}
module.exports.DocumentsFacturesAchats = DocumentsFacturesAchats;