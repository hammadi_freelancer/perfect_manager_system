//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var LignesReleveAchats = {
addLigneReleveAchats : function (db,ligneReleveAchats ,codeUser,callback){
    if(ligneReleveAchats  != undefined){
        ligneReleveAchats.code  = utils.isUndefined(ligneReleveAchats.code)?shortid.generate():ligneReleveAchats.code;
        if(utils.isUndefined(ligneReleveAchats.numeroLigneReleveAchats))
            {
               const currentD =  moment(new Date());
              // const genereKey = shortid.generate();
               const generatedCode = currentD.year().toString() +'/'+currentD.utc(true).month().toString()+currentD.minutes().toString()+'/'+
               currentD.seconds().toString() ;
               ligneReleveAchats.numeroLigneReleveAchats = 'LIGN' + generatedCode;
            }
            ligneReleveAchats.userCreatorCode = codeUser;
            ligneReleveAchats.userLastUpdatorCode = codeUser;
            ligneReleveAchats.dateCreation = moment(new Date).format('L');
            ligneReleveAchats.dateLastUpdate = moment(new Date).format('L');
        
      
            db.collection('LigneReleveAchats').insertOne(
                ligneReleveAchats,function(err,result){
                    // clientDB.close();
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,ligneReleveAchats);
                   }
                }
              ) ;
}else{
callback(true,null);
}
},
updateLigneReleveAchats: function (db,ligneReleveAchats,codeUser, callback){
    const criteria = { "code" : ligneReleveAchats.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"numeroLigneReleveAchats" : ligneReleveAchats.numeroLigneReleveAchats, 
    "codeReleveAchats" : ligneReleveAchats.codeReleveAchats, 
    "article" : ligneReleveAchats.article, 
    "unMesure" : ligneReleveAchats.unMesure, 
    "magasin" : ligneReleveAchats.magasin, 
    "quantite" : ligneReleveAchats.quantite, 
    "prixUnitaire" : ligneReleveAchats.prixUnitaire, 
    "tva" : ligneReleveAchats.tva, 
    "montantTVA" : ligneReleveAchats.montantTVA, 
    "fodec" : ligneReleveAchats.fodec, 
    "montantFodec" : ligneReleveAchats.montantFodec, 
    "otherTaxe1" : ligneReleveAchats.otherTaxe1, 
    "montantOtherTaxe1" : ligneReleveAchats.montantOtherTaxe1,
    "otherTaxe2" : ligneReleveAchats.otherTaxe2, 
    "montantOtherTaxe2" : ligneReleveAchats.montantOtherTaxe2,  
    "remise" : ligneReleveAchats.remise, 
    "montantRemise" : ligneReleveAchats.montantRemise,
    "prixVente" : ligneReleveAchats.prixVente, 
    "beneficeEstime" : ligneReleveAchats.beneficeEstime, 
    "montantBeneficeEstime" : ligneReleveAchats.montantBeneficeEstime, 
    "montantHT" : ligneReleveAchats.montantHT, 
    "montantTTC" : ligneReleveAchats.montantTTC, 
    "montantAPayer" : ligneReleveAchats.montantAPayer, 
    "etat" : ligneReleveAchats.etat, 
    "description" : ligneReleveAchats.description,
     "codeOrganisation" : ligneReleveAchats.codeOrganisation,
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": ligneReleveAchats.dateLastUpdate, 
    } ;
    
            db.collection('LigneReleveAchats').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
      
    
    },
getListLignesReleveAchats : function (db,codeUser,callback)
{
    let listLignesReleveAchats = [];
  
   var cursor =  db.collection('LigneReleveAchats').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(ligneReleveAchats,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
       // docClient.dateReception = moment(docClient.dateReceptionObject).format('L');
        
       listLignesReleveAchats.push(ligneReleveAchats);
   }).then(function(){
    callback(null,listLignesReleveAchats); 
   });
   

     //return [];
},
deleteLigneReleveAchats: function (db,codeLigneReleveAchats,codeUser,callback){
    const criteria = { "code" : codeLigneReleveAchats, "userCreatorCode":codeUser };
    
            db.collection('LigneReleveAchats').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
           
      
},

getLigneReleveAchats: function (db,codeLigneReleveAchats,codeUser,callback)
{
    const criteria = { "code" : codeLigneReleveAchats, "userCreatorCode":codeUser };

       const ligneReleveAchats=  db.collection('LigneReleveAchats').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                    
},


getListLignesReleveAchatsByCodeReleveAchats : function (db,codeUser,codeReleveAchats,callback)
{
    let listLignesReleveAchats = [];
   
   var cursor =  db.collection('LigneReleveAchats').find( 
       {"userCreatorCode" : codeUser, "codeReleveAchats": codeReleveAchats}
    );
   cursor.forEach(function(ligneReleveAchats,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        
        listLignesReleveAchats.push(ligneReleveAchats);
   }).then(function(){
  // console.log('list doc credits',listCredits);
    callback(null,listLignesReleveAchats); 
   });
   
}



}
module.exports.LignesReleveAchats = LignesReleveAchats;