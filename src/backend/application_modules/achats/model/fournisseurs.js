//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var Fournisseurs = {
    addFournisseur : function (db,fournisseur,codeUser,  callback){
        if(fournisseur != undefined){
            fournisseur.code  = fournisseur.code===undefined?shortid.generate():fournisseur.code;
            fournisseur.userCreatorCode = codeUser;
            fournisseur.userLastUpdatorCode = codeUser;
            fournisseur.dateCreation = moment(new Date);
            fournisseur.dateLastUpdate = moment(new Date);
     if(utils.isUndefined(fournisseur.numeroFournisseur))
                {
                   const currentD =  moment(new Date());
                  // const genereKey = shortid.generate();
                   const generatedCode = currentD.year().toString() +'/'+currentD.utc(true).month().toString()+currentD.minutes().toString()+'/'+
                   currentD.seconds().toString() ;
                   fournisseur.numeroFournisseur = 'FOUR' + generatedCode;
                }
                

            db.collection('Fournisseur').insertOne(
                fournisseur,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
              ) ;
   
    
           //return true;  
    }
},

updateFournisseur: function (db,fournisseur,codeUser,  callback){

    /*credit.dateOperation = utils.isUndefined(credit.dateOperation)? 
    moment(new Date).format('L'): moment(credit.dateOperation).format('L');

    credit.dateDebutPayement = utils.isUndefined(credit.dateDebutPayement)? 
    moment(new Date).format('L'): moment(credit.dateDebutPayement).format('L');*/
    fournisseur.userLastUpdatorCode = codeUser;
    fournisseur.dateCreation = moment(new Date);
    fournisseur.dateLastUpdate = moment(new Date);
    const criteria = { "code" : fournisseur.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"nom" : fournisseur.nom, "prenom" : fournisseur.prenom, 
    "cin" : fournisseur.cin,
    "valeurVentesRegles" : fournisseur.valeurVentesRegles, 
    "valeurVentes" : fournisseur.valeurVentes, 
    "valeurCredits" : fournisseur.valeurCredits, 
    "valeurCreditsRegles" : fournisseur.valeurCreditsRegles, 
    "adresse" : fournisseur.adresse,
    "matriculeFiscale" : fournisseur.matriculeFiscale,"raisonSociale": fournisseur.raisonSociale,
      "mobile" : fournisseur.mobile,
      "userLastUpdatorCode": codeUser,
      "dateLastUpdate": fournisseur.dateLastUpdate, 
    } ;

            db.collection('Fournisseur').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
   
    
    },
getPageFournisseurs : function (db,codeUser, pageNumber,nbElementsPerPage,filter,callback)
{
    let listFournisseurs = [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition =  this.buildFilterCondition(filterObject,filterData);
   var cursor =  db.collection('Fournisseur').find(
    condition
   
   ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
   .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(fournisseur,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        listFournisseurs.push(fournisseur);
   }).then(function(){
    console.log(' list Fournisseurs',listFournisseurs);
    callback(null,listFournisseurs); 
   });
   

     //return [];
},

getListFournisseurs : function (db,codeUser,callback)
{
    let listFournisseurs = [];
  
   var cursor =  db.collection('Fournisseur').find(
    {"userCreatorCode" : codeUser}
    
   );
   cursor.forEach(function(fournisseur,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        listFournisseurs.push(fournisseur);
   }).then(function(){
    console.log(' list Fournisseurs',listFournisseurs);
    callback(null,listFournisseurs); 
   });
   

},


deleteFournisseur: function (db,codeFournisseur,codeUser, callback){
    const criteria = { "code" : codeFournisseur, "userCreatorCode":codeUser };
    
            db.collection('Fournisseur').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
     
},
getFournisseur: function (db,codeFournisseur,codeUser, callback)
{
    const criteria = { "code" : codeFournisseur, "userCreatorCode":codeUser };
    

       const fournisseur =  db.collection('Fournisseur').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                     
},

getTotalFournisseurs: function (db,codeUser,filter, callback)
{
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var totalFournisseurs=  db.collection('Fournisseur').find( 
        condition    ).count(function(err,result){
        callback(null,result); 
    }
);

},
buildFilterCondition(filterObject,filterData)
{
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.numeroFournisseur))
        {
                filterData["numeroFournisseur"] = filterObject.numeroFournisseur;
        }
         
        if(!utils.isUndefined(filterObject.nom))
          {
                 
                    filterData["nom"] = filterObject.nom;
                    
         }
         if(!utils.isUndefined(filterObject.prenom))
             {
                    
                       filterData["prenom"] =filterObject.prenom;
                       
            }
            if(!utils.isUndefined(filterObject.cin))
             {
                    
                       filterData["cin"] = filterObject.cin;
                       
            }
            if(!utils.isUndefined(filterObject.raisonSociale))
             {
                    
                       filterData["raisonSociale"] = filterObject.raisonSociale ;
                       
            }
            if(!utils.isUndefined(filterObject.matriculeFiscale))
             {
                    
                       filterData["matriculeFiscale"] = filterObject.matriculeFiscale ;
                       
            }
            if(!utils.isUndefined(filterObject.registreCommerce))
             {
                    
                       filterData["registreCommerce"] = filterObject.registreCommerce ;
                       
            }
            if(!utils.isUndefined(filterObject.mobile))
             {
                    
                       filterData["mobile"] = filterObject.mobile ;
                       
            }
            if(!utils.isUndefined(filterObject.codePostale))
             {
                    
                       filterData["codePostale"] = filterObject.codePostale ;
                       
            }
            if(!utils.isUndefined(filterObject.gouvernorat))
             {
                    
                       filterData["gouvernorat"] = filterObject.gouvernorat ;
                       
            }
            if(!utils.isUndefined(filterObject.ville))
                {
                       
                          filterData["ville"] = filterObject.ville ;
                          
               }
            
     
     
            if(!utils.isUndefined(filterObject.adresse))
             {
                    
                       filterData["adresse"] = filterObject.adresse ;
                       
            }
           
     
        }
        return filterData;
},
}
module.exports.Fournisseurs = Fournisseurs;