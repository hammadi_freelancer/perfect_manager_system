//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var RelevesAchats = {
addReleveAchats : function (db,releveAchats,codeUser, callback){


    if(releveAchats != undefined){
        releveAchats.code  = utils.isUndefined(releveAchats.code)?shortid.generate():releveAchats.code;
       if(utils.isUndefined(releveAchats.numeroReleveAchats))
        {
           const currentD =  moment(new Date());
          // const genereKey = shortid.generate();
           const generatedCode = currentD.year().toString() +'/'+currentD.utc(true).month().toString()+currentD.minutes().toString() ;
           releveAchats.numeroReleveAchats = 'RELVA' + generatedCode;

           }
           const listRows =   releveAchats.listLignesRelAchats;
           listRows.forEach((row)=>(row.codeReleveAchats =releveAchats.code, row.numeroReleveAchats=releveAchats.numeroReleveAchats ));
           releveAchats.listLignesRelAchats  = listRows;
     
           releveAchats.dateAchatsObject = utils.isUndefined(releveAchats.dateAchatsObject)? 
        new Date():releveAchats.dateAchatsObject;
        releveAchats.userCreatorCode = codeUser;
        releveAchats.userLastUpdatorCode = codeUser;
        releveAchats.dateCreation = moment(new Date).format('L');
        releveAchats.dateLastUpdate = moment(new Date).format('L');
  
            db.collection('ReleveAchats').insertOne(
                releveAchats,function(err,result){
                   // mongoClient.clos
                   if(err){

                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;


}else{
callback(true,null);
}
},

updateReleveAchats: function (db,releveAchats,codeUser, callback){

    //factureVente.dateFacturation = utils.isUndefined(factureVente.dateFacturation)? 
    //moment(new Date).format('L'): moment(factureVente.dateFacturation).format('L');
    const listRows =   releveAchats.listLignesRelAchats;
    listRows.forEach((row)=>(row.codeReleveAchats =releveAchats.code,
         row.numeroReleveAchats=releveAchats.numeroReleveAchats ));
         releveAchats.listLignesRelAchats  = listRows;


         releveAchats.dateAchatsObject = utils.isUndefined(releveAchats.dateAchatsObject)? 
    new Date():releveAchats.dateAchatsObject;

    const dateLastUpdate = moment(new Date).format('L');
    const criteria = { "code" : releveAchats.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"fournisseur" : releveAchats.fournisseur,
     "dateAchatsObject" : releveAchats.dateAchatsObject ,
     "numeroReleveAchats" : releveAchats.numeroReleveAchats, 
     "montantBeneficeEstime" : releveAchats.montantBeneficeEstime, 
     "montantAPayer": releveAchats.montantAPayer, 
     "regleToutes" : releveAchats.regleToutes,
     "livreToutes" : releveAchats.livreToutes , 
      "payementData" : releveAchats.payementData,
       "etat" : releveAchats.etat,
       "listLignesRelAchats": releveAchats.listLignesRelAchats,
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": dateLastUpdate, 
       

    } ;
    
            db.collection('ReleveAchats').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
     
    
    },
getListRelevesAchats : function (db,codeUser,pageNumber,nbElementsPerPage, filter,callback)
{
    let listRelevesAchats = [];

    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var cursor =  db.collection('ReleveAchats').find(
  condition
   ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
   .limit(Number(nbElementsPerPage) );
   
   cursor.forEach(function(releveAchats,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        releveAchats.dateAchats = moment(releveAchats.dateAchatsObject).format('L');
        listRelevesAchats.push(releveAchats);
   }).then(function(){
    // console.log('list factures ventes ',listFacturesVentes);
    callback(null,listRelevesAchats); 
   });
     //return [];
},
deleteReleveAchats: function (db,codeReleveAchats,codeUser, callback){
    const criteria = { "code" : codeReleveAchats, "userCreatorCode":codeUser };
    
  
            db.collection('ReleveAchats').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
       
},

getReleveAchats: function (db,codeReleveAchats,codeUser, callback)
{
    const criteria = { "code" : codeReleveAchats, "userCreatorCode":codeUser };
    
       const releveAchats =  db.collection('ReleveAchats').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                   
},

getTotalRelevesAchats: function (db,codeUser,filter, callback)
{
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var totalRelevesAchats =  db.collection('ReleveAchats').find( 
condition    ).count(function(err,result){
        callback(null,result); 
    }
);
},
buildFilterCondition(filterObject,filterData)
{
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.numeroReleveAchats))
        {
                filterData["numeroReleveAchats"] = filterObject.numeroReleveAchats;
        }
         
        if(!utils.isUndefined(filterObject.montantBenefice))
          {
                 
                    filterData["montantBenefice"] = filterObject.montantBenefice;
                    
         }
         if(!utils.isUndefined(filterObject.montantAPayer))
             {
                    
                       filterData["montantAPayer"] =filterObject.montantAPayer;
                       
            }
            if(!utils.isUndefined(filterObject.regleToutes))
             {
                    
                       filterData["regleToutes"] = filterObject.regleToutes;
                       
            }
            if(!utils.isUndefined(filterObject.livreToutes))
             {
                    
                       filterData["livreToutes"] = filterObject.livreToutes ;
                       
            }
            if(!utils.isUndefined(filterObject.etat))
             {
                    
                       filterData["etat"] = filterObject.etat ;
                       
            }
        
            if(!utils.isUndefined(filterObject.dateAchatsFromObject))
             {
                    
                       filterData["dateAchatsObject"] = {$gt:filterObject.dateAchatsFromObject} ;
                       
            }
            if(!utils.isUndefined(filterObject.dateAchatsToObject))
                {
                       
                          filterData["dateAchatsObject"] = {$lt:filterObject.dateAchatsToObject} ;
                          
               }
        }
        return filterData;
},
}
module.exports.RelevesAchats = RelevesAchats;