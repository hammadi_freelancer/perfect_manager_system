//var db = require('../common/MongoDbConnection').getDBConnection;
const mongoClient = require('mongodb').MongoClient;
const url ='mongodb://localhost:27017';
const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var AjournementsFacturesAchats = {
addAjournementFactureAchats : function (db,ajournementFactureAchats,codeUser,callback){
    if(ajournementFactureAchats != undefined){
        ajournementFactureAchats.code  = utils.isUndefined(ajournementFactureAchats.code)?shortid.generate():ajournementFactureAchats.code;
        ajournementFactureAchats.dateReceptionObject = utils.isUndefined(ajournementFactureAchats.dateReceptionObject)? 
        new Date():ajournementFactureAchats.dateReceptionObject;
        ajournementFactureAchats.userCreatorCode = codeUser;
        ajournementFactureAchats.userLastUpdatorCode = codeUser;
        ajournementFactureAchats.dateCreation = moment(new Date).format('L');
        ajournementFactureAchats.dateLastUpdate = moment(new Date).format('L');
        
      
            db.collection('AjournementFactureAchats').insertOne(
                ajournementFactureAchats,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,ajournementFactureAchats);
                   }
                }
              ) ;
    

}else{
callback(true,null);
}
},
updateAjournementFactureAchats: function (db,ajournementFactureAchats,codeUser, callback){
    
       
    ajournementFactureAchats.dateOperationObject = utils.isUndefined(ajournementFactureAchats.dateOperationObject)? 
        new Date():ajournementFactureAchats.dateOperationObject;
        ajournementFactureAchats.dateLastUpdate = moment(new Date).format('L');
        
        const criteria = { "code" : ajournementFactureAchats.code,"userCreatorCode" : codeUser };
        const dataToUpdate = {
        "codeFactureAchats" : ajournementFactureAchats.codeFactureAchats, 
        "description" : ajournementFactureAchats.description,
         "codeOrganisation" : ajournementFactureAchats.codeOrganisation,
        "dateOperationObject" : ajournementFactureAchats.dateOperationObject ,
        "nouvelleDatePayementObject" : ajournementFactureAchats.nouvelleDatePayementObject ,
        "motif" : ajournementFactureAchats.motif ,
           "userLastUpdatorCode": codeUser,
           "dateLastUpdate": ajournementFactureAchats.dateLastUpdate, 
        } ;
          
                db.collection('AjournementFactureAchats').updateOne(
                    criteria,
                    { 
                        $set: dataToUpdate
                    },
                    function(err,result){
                        if(err){
                            
                            callback(null,null);
                        }else{
                            
                            callback(false,result);
                        }
                    }  
                ) ;
         
        
        },
    getListAjournementsFacturesAchats : function (db,codeUser,callback)
    {
        let listAjournementsFacturesAchats = [];
        
       var cursor =  db.collection('AjournementFactureAchats').find( 
           {"userCreatorCode" : codeUser}
        );
       cursor.forEach(function(ajournementFactAchats,err){
           if(err)
            {
                console.log('errors produced :', err);
            }
            ajournementFactAchats.dateOperation = moment(ajournementFactAchats.dateOperationObject).format('L');
            ajournementFactAchats.nouvelleDatePayement = moment(ajournementFactAchats.nouvelleDatePayementObject).format('L');
            
            listAjournementsFacturesAchats.push(ajournementFactAchats);
       }).then(function(){
       //  console.log('list payements credits',listPayementsCredits);
        callback(null,listAjournementsFacturesAchats); 
       });
       
  
         //return [];
    },
    deleteAjournementFactureAchats: function (db,codeAjournementFactureAchats,codeUser,callback){
        const criteria = { "code" : codeAjournementFactureAchats, "userCreatorCode":codeUser };
         
                db.collection('AjournementFactureAchats').deleteOne(
                    criteria ,
                    function(err,result){
                        if(err){
                            
                            callback(null,null);
                        }else{
                            
                            callback(false,result);
                        }
                    }  
                ) ;
               
            
    },
    
    getAjournementFactureAchats: function (db,codeAjournementFactureAchats,codeUser,callback)
    {
        const criteria = { "code" : codeAjournementFactureAchats, "userCreatorCode":codeUser };
    
           const ajournmentFactAchats =  db.collection('AjournementFactureAchats').findOne(
                criteria , function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }
            ) ;
                         
    },
    
    
    getListAjournementsFacturesAchatsByCodeFactureAchats: function (db,codeUser,codeFactureAchats,callback)
    {
        let listAjournementsFacturesAchats = [];
      
       var cursor =  db.collection('AjournementFactureAchats').find( 
           {"userCreatorCode" : codeUser, "codeFactureAchats": codeFactureAchats}
        );
       cursor.forEach(function(ajournementFactAchats,err){
           if(err)
            {
                console.log('errors produced :', err);
            }
            ajournementFactAchats.dateOperation = moment(ajournementFactAchats.dateOperationObject).format('L');
            ajournementFactAchats.nouvelleDatePayement = moment(ajournementFactAchats.nouvelleDatePayementObject).format('L');
            
            listAjournementsFacturesAchats.push(ajournementFactAchats);
       }).then(function(){
       // console.log('list credits',listCredits);
        callback(null,listAjournementsFacturesAchats); 
       });
       
    }


    
    
    }
    module.exports.AjournementsFacturesAchats = AjournementsFacturesAchats;