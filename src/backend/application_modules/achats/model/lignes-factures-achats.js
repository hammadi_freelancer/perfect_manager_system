//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var LignesFactureAchats = {
addLigneFactureAchats : function (db,ligneFactureAchats ,codeUser,callback){
    if(ligneFactureAchats  != undefined){
        ligneFactureAchats.code  = utils.isUndefined(ligneFactureAchats.code)?shortid.generate():ligneFactureAchats.code;
        if(utils.isUndefined(ligneFactureAchats.numeroLigneFactureAchats))
            {
               const currentD =  moment(new Date());
              // const genereKey = shortid.generate();
               const generatedCode = currentD.year().toString() +'/'+currentD.utc(true).month().toString()+currentD.minutes().toString()+'/'+
               currentD.seconds().toString() ;
               ligneFactureAchats.numeroLigneFactureAchats = 'LIGN' + generatedCode;
            }
            ligneFactureAchats.userCreatorCode = codeUser;
            ligneFactureAchats.userLastUpdatorCode = codeUser;
            ligneFactureAchats.dateCreation = moment(new Date).format('L');
            ligneFactureAchats.dateLastUpdate = moment(new Date).format('L');
        
     
            db.collection('LigneFactureAchats').insertOne(
                ligneFactureAchats,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,ligneFactureAchats);
                   }
                }
              ) ;
}else{
callback(true,null);
}
},
updateLigneFactureAchats: function (db,ligneFactureAchats,codeUser, callback){
    const criteria = { "code" : ligneFactureAchats.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"numeroLigneFactureAchats" : ligneFactureAchats.numeroLigneFactureAchats, 
    "codeFactureAchats" : ligneFactureAchats.codeFactureAchats, 
    "article" : ligneFactureAchats.article, 
    "unMesure" : ligneFactureAchats.unMesure, 
    "magasin" : ligneFactureAchats.magasin, 
    "quantite" : ligneFactureAchats.quantite, 
    "prixUnitaire" : ligneFactureAchats.prixUnitaire, 
    "tva" : ligneFactureAchats.tva, 
    "montantTVA" : ligneFactureAchats.montantTVA, 
    "fodec" : ligneFactureAchats.fodec, 
    "montantFodec" : ligneFactureAchats.montantFodec, 
    "otherTaxe1" : ligneFactureAchats.otherTaxe1, 
    "montantOtherTaxe1" : ligneFactureAchats.montantOtherTaxe1,
    "otherTaxe2" : ligneFactureAchats.otherTaxe2, 
    "montantOtherTaxe2" : ligneFactureAchats.montantOtherTaxe2,  
    "remise" : ligneFactureAchats.remise, 
    "montantRemise" : ligneFactureAchats.montantRemise,
    "prixVente" : ligneFactureAchats.prixVente, 
    "beneficeEstime" : ligneFactureAchats.beneficeEstime, 
    "montantBeneficeEstime" : ligneFactureAchats.montantBeneficeEstime, 
    "montantHT" : ligneFactureAchats.montantHT, 
    "montantTTC" : ligneFactureAchats.montantTTC, 
    "montantAPayer" : ligneFactureAchats.montantAPayer, 
    "etat" : ligneFactureAchats.etat, 
    "description" : ligneFactureAchats.description,
     "codeOrganisation" : ligneFactureAchats.codeOrganisation,
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": ligneFactureAchats.dateLastUpdate, 
    } ;
      
            db.collection('LigneFactureAchats').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
      ;
    
    },
getListLignesFactureAchats : function (db,codeUser,callback)
{
    let listLignesFactureAchats = [];
  
   var cursor =  db.collection('LigneFactureAchats').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(ligneFactureAchats,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
       // docClient.dateReception = moment(docClient.dateReceptionObject).format('L');
        
       listLignesFactureAchats.push(ligneFactureAchats);
   }).then(function(){
    callback(null,listLignesFactureAchats); 
   });
   

     //return [];
},
deleteLigneFactureAchats: function (db,codeLigneFactureAchats,codeUser,callback){
    const criteria = { "code" : codeLigneFactureAchats, "userCreatorCode":codeUser };
     
            db.collection('LigneFactureAchats').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
       
},

getLigneFactureAchats: function (db,codeLigneFactureAchats,codeUser,callback)
{
    const criteria = { "code" : codeLigneFactureAchats, "userCreatorCode":codeUser };

       const ligneFactureAchats=  db.collection('LigneFactureAchats').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                   
},


getListLignesFactureAchatsByCodeFactureAchats : function (db,codeUser,codeFactureAchats,callback)
{
    let listLignesFactureAchats = [];
     
   var cursor =  db.collection('LigneFactureAchats').find( 
       {"userCreatorCode" : codeUser, "codeFactureAchats": codeFactureAchats}
    );
   cursor.forEach(function(ligneFactureAchats,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        
        listLignesFactureAchats.push(ligneFactureAchats);
   }).then(function(){
  // console.log('list doc credits',listCredits);
    callback(null,listLignesFactureAchats); 
   });
   
}



}
module.exports.LignesFactureAchats = LignesFactureAchats;