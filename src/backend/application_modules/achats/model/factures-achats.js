//var db = require('../common/MongoDbConnection').getDBConnection;
// const mongoClient = require('mongodb').MongoClient;
// const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var FacturesAchats = {
addFactureAchats : function (db,factureAchats,codeUser, callback){
    if(factureAchats != undefined){
        factureAchats.code  = utils.isUndefined(factureAchats.code)?shortid.generate():factureAchats.code;
       if(utils.isUndefined(factureAchats.numeroFactureAchats))
        {
           const currentD =  moment(new Date());
          // const genereKey = shortid.generate();
           const generatedCode = currentD.year().toString() +'/'+currentD.utc(true).month().toString()+currentD.minutes().toString() ;
           factureAchats.numeroFactureAchats = 'FACT' + generatedCode;

           }
           const listRows =   factureAchats.listLignesFacturesAchats;
           listRows.forEach((row)=>(row.codeFactureAchats =factureAchats.code, row.numeroFactureAchats=factureAchats.numeroFactureAchats ));
           factureAchats.listLignesFacturesAchats  = listRows;
       console.log('list rows of facture ventes');
       console.log(listRows);
       factureAchats.dateFacturationObject = utils.isUndefined(factureAchats.dateFacturationObject)? 
        new Date():factureAchats.dateFacturationObject;

        factureAchats.dateAchatsObject = utils.isUndefined(factureAchats.dateAchatsObject)? 
        new Date():factureAchats.dateAchatsObject;

        factureAchats.userCreatorCode = codeUser;
        factureAchats.userLastUpdatorCode = codeUser;
        factureAchats.dateCreation = moment(new Date).format('L');
        factureAchats.dateLastUpdate = moment(new Date).format('L');
    
            db.collection('FactureAchats').insertOne(
                factureAchats,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
   

}else{
callback(true,null);
}
},
updateFactureAchats: function (db,factureAchats,codeUser, callback){

    //factureVente.dateFacturation = utils.isUndefined(factureVente.dateFacturation)? 
    //moment(new Date).format('L'): moment(factureVente.dateFacturation).format('L');
    const listRows =   factureAchats.listLignesFacturesAchats;
    listRows.forEach((row)=>(row.codeFactureAchats =factureAchats.code, row.numeroFactureAchats=factureAchats.numeroFactureAchats ));
    factureAchats.listLignesFacturesAchats  = listRows;
        console.log('list rows of facture achats');
        console.log(listRows);
        factureAchats.dateFacturationObject = utils.isUndefined(factureAchats.dateFacturationObject)? 
    new Date():factureAchats.dateFacturationObject;

    factureAchats.dateAchatsObject = utils.isUndefined(factureAchats.dateAchatsObject)? 
    new Date():factureAchats.dateAchatsObject;

    factureAchats.userLastUpdatorCode = codeUser;
    factureAchats.dateCreation = moment(new Date).format('L');
    factureAchats.dateLastUpdate = moment(new Date).format('L');
    const criteria = { "code" : factureAchats.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"client" : factureAchats.fournisseur,
     "dateFacturationObject" : factureAchats.dateFacturationObject ,
     "dateAchatsObject" : factureAchats.dateAchatsObject ,
     "numeroFactureAchats" : factureAchats.numeroFactureAchats, "montantAPayer" : factureAchats.montantAPayer, 
     "montantTTC": factureAchats.montantTTC, "montantHT" : factureAchats.montantHT,
     "montantTVA" : factureAchats.montantTVA , "montantRemise" : factureAchats.montantRemise ,
      "payementData" : factureAchats.payementData,
       "etat" : factureAchats.etat,"listLignesFacturesAchats": factureAchats.listLignesFacturesAchats,
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": factureAchats.dateLastUpdate, 
       

    } ;
        
            db.collection('FactureAchats').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                } , 
                { 
                    $set: dataToUpdate
                }
            ) ;
      
    
    },
getListFacturesAchats : function (db,codeUser,pageNumber,nbElementsPerPage, filter,callback)
{
    let listFacturesAchats = [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var cursor =  db.collection('FactureAchats').find(
    condition
    
   ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
   .limit(Number(nbElementsPerPage) );
   
   cursor.forEach(function(factureAchats,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        factureAchats.dateFacturation =  moment(factureAchats.dateFacturationObject).format('L');
 factureAchats.dateAchats = moment(factureAchats.dateAchatsObject).format('L');
        listFacturesAchats.push(factureAchats);
   }).then(function(){
    console.log('list factures achats ',listFacturesAchats);
    callback(null,listFacturesAchats); 
   });
   


     //return [];
},
deleteFactureAchats: function (db,codeFactureAchats,codeUser, callback){
    const criteria = { "code" : codeFactureAchats, "userCreatorCode":codeUser };
    
     
            db.collection('FactureAchats').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
   
},

getFactureAchats: function (db,codeFactureAchats,codeUser, callback)
{
    const criteria = { "code" : codeFactureAchats, "userCreatorCode":codeUser };
    
 
       const factureAchats =  db.collection('FactureAchats').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                   
},

getTotalFacturesAchats: function (db,codeUser,filter, callback)
{
   
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var totalFacturesAchats =  db.collection('FactureAchats').find( 
    condition
    ).count(function(err,result){
        callback(null,result); 
    }
);
   
},

buildFilterCondition(filterObject,filterData)
{
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.numeroFactureAchats))
        {
                filterData["numeroFactureAchats"] = filterObject.numeroFactureAchats;
        }
         
        if(!utils.isUndefined(filterObject.nomFournisseur))
          {
                 
                    filterData["fournisseur.nom"] = filterObject.nomFournisseur;
                    
         }
         if(!utils.isUndefined(filterObject.prenomFournisseur))
             {
                    
                       filterData["fournisseur.prenom"] =filterObject.prenomFournisseur;
                       
            }
            if(!utils.isUndefined(filterObject.cinFournisseur))
             {
                    
                       filterData["fournisseur.cin"] = filterObject.cinFournisseur;
                       
            }
            if(!utils.isUndefined(filterObject.raisonFournisseur))
             {
                    
                       filterData["fournisseur.raisonSociale"] = filterObject.raisonFournisseur ;
                       
            }
            if(!utils.isUndefined(filterObject.matriculeFournisseur))
             {
                    
                       filterData["fournisseur.matriculeFiscale"] = filterObject.matriculeFournisseur;
                       
            }
            if(!utils.isUndefined(filterObject.registrFournisseur))
             {
                    
                       filterData["fournisseur.registreCommerce"] = filterObject.registreFournisseur ;
                       
            }
            if(!utils.isUndefined(filterObject.dateFacturationFromObject))
             {
                    
                       filterData["dateFacturationObject"] = {$gt:filterObject.dateFacturationFromObject} ;
                       
            }
            if(!utils.isUndefined(filterObject.dateFacturationToObject))
                {
                       
                          filterData["dateFacturationObject"] = {$lt:filterObject.dateFacturationToObject} ;
                          
               }
               if(!utils.isUndefined(filterObject.dateAchatsFromObject))
                {
                       
                          filterData["dateAchatsObject"] = {$gt:filterObject.dateAchatsFromObject} ;
                          
               }
               if(!utils.isUndefined(filterObject.dateAchatsToObject))
                   {
                          
                             filterData["dateAchatsObject"] = {$lt:filterObject.dateAchatsToObject} ;
                             
                  }

                if(!utils.isUndefined(filterObject.montantAPayer))
                    {
                           
                              filterData["montantAPayer"] = filterObject.montantAPayer ;
                              
                   }

                   if(!utils.isUndefined(filterObject.etat))
                    {
                           
                              filterData["etat"] = filterObject.etat ;
                              
                   }

                }
        return filterData;
},












}
module.exports.FacturesAchats = FacturesAchats;