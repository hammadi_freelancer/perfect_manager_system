//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var ParametresAchats = {
    saveParametresAchats : function (db,parametresAchats, codeUser, callback){
        if(parametresAchats != undefined){
            parametresAchats.userLastUpdatorCode = codeUser;
            parametresAchats.dateCreation = moment(new Date);
            parametresAchats.dateLastUpdate = moment(new Date);
      
            db.collection('ParametresAchats').insertOne(
                parametresAchats,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
              ) ;
             // db.close();
    
    
           //return true;  
    }
    else
        {
            callback(true,"EMPTY_OBJECT_PARAMETRES_ACHATS");
            
        }
},
getParametresAchats: function (db,codeUser,callback)
{
    const criteria = {"userCreatorCode" : codeUser};

       const parametresAchats =  db.collection('ParametresAchats').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                    
},
}
module.exports.ParametresAchats = ParametresAchats;