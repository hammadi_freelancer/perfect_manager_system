//var db = require('../common/MongoDbConnection').getDBConnection;
const mongoClient = require('mongodb').MongoClient;
const url ='mongodb://localhost:27017';
const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var CreditsAchats = {
addCreditAchats : function (db,creditAchats,codeUser,callback){
    if(creditAchats != undefined){
        creditAchats.code  = utils.isUndefined(creditAchats.code)?shortid.generate():creditAchats.code;
        creditAchats.dateOperationObject = utils.isUndefined(creditAchats.dateOperationObject)? 
        new Date():creditAchats.dateOperationObject;
        creditAchats.dateDebutPayementObject = utils.isUndefined(creditAchats.dateDebutPayementObject)? 
        new Date():creditAchats.dateDebutPayementObject;
        if(utils.isUndefined(creditAchats.numeroCreditAchats))
            {
               const currentD =  moment(new Date);
              // const genereKey = shortid.generate();
               const generatedCode = currentD.year().toString() +'/'+currentD.month().toString()+currentD.minutes().toString() ;
               creditAchats.numeroCreditAchats = 'CRDACH' + generatedCode;
            }
        creditAchats.userCreatorCode = codeUser;
        creditAchats.userLastUpdatorCode = codeUser;
        creditAchats.dateCreation = moment(new Date).format('L');
        creditAchats.dateLastUpdate = moment(new Date).format('L');
      
            db.collection('CreditAchats').insertOne(
                creditAchats,function(err,result){
                  //  clientDB.close();
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
  

}else{
callback(true,null);
}
},
updateCreditAchats: function (db,creditAchats,codeUser, callback){

    /*creditAchats.dateOperation = utils.isUndefined(creditAchats.dateOperation)? 
    moment(new Date).format('L'): moment(creditAchats.dateOperation).format('L');
    creditAchats.dateDebutPayement = utils.isUndefined(creditAchats.dateDebutPayement)? 
    moment(new Date).format('L'): moment(creditAchats.dateDebutPayement).format('L');
    */
    creditAchats.dateOperationObject = utils.isUndefined(creditAchats.dateOperationObject)? 
    new Date():creditAchats.dateOperationObject;
    creditAchats.dateDebutPayementObject = utils.isUndefined(creditAchats.dateDebutPayementObject)? 
    new Date():creditAchats.dateDebutPayementObject;

    creditAchats.userLastUpdatorCode = codeUser;
    creditAchats.dateCreation = moment(new Date).format('L');
    creditAchats.dateLastUpdate = moment(new Date).format('L');
    const criteria = { "code" : creditAchats.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"nomClient" : creditAchats.nomClient, "prenomClient" : creditAchats.prenomClient, 
    "cinClient" : creditAchats.cinClient, "descriptionEchange" : creditAchats.descriptionEchange,
    "dateOperationObject" : creditAchats.dateOperationObject ,
     "avance" : creditAchats.avance, "montantReste" : creditAchats.montantReste, 
     "montantAPayer":creditAchats.montantAPayer,
     "numeroCreditAchats":creditAchats.numeroCreditAchats,
     "periodTranche" : creditAchats.periodTranche ,
      "dateDebutPayementObject" : creditAchats.dateDebutPayementObject ,
      "etat": creditAchats.etat,
      "valeurTranche" : creditAchats.valeurTranche,
       "nbreTranches" : creditAchats.nbreTranches, 
       "listTranches": creditAchats.listTranches,
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": creditAchats.dateLastUpdate, 
    } ;
     
            db.collection('CreditAchats').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
       
    
    
    },
getListCreditsAchats : function (db,codeUser,pageNumber, nbElementsPerPage ,filter,callback)
{
    let listCreditsAchats = [];
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var cursor =  db.collection('CreditAchats').find(condition
   ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
   .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(creditAchats,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        creditAchats.dateOperation = moment(creditAchats.dateOperationObject).format('L');
        creditAchats.dateDebutPayement = moment(creditAchats.dateDebutPayementObject).format('L');
        
        listCreditsAchats.push(creditAchats);
   }).then(function(){
    console.log('list credits',listCreditsAchats);
    callback(null,listCreditsAchats); 
   });
   

     //return [];
},
deleteCreditAchats: function (db,codeCreditAchats,codeUser, callback){
    const criteria = { "code" : codeCreditAchats, "userCreatorCode":codeUser };
    
      
            db.collection('CreditAchats').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
    
},

getCreditAchats: function (db,codeCreditAchats,codeUser, callback)
{
    const criteria = { "code" : codeCreditAchats, "userCreatorCode":codeUser };
    

       const creditAchats =  db.collection('CreditAchats').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                       
},
getTotalCreditsAchats : function (db,codeUser,filter, callback)
{
    let filterObject = JSON.parse(filter);
    
      let filterData = {
          "userCreatorCode" : codeUser
      };
      const condition = this.buildFilterCondition(filterObject,filterData);
      
   var totalCreditsAchats =  db.collection('CreditAchats').find( 
    condition
    ).count(function(err,result){
        callback(null,result); 
    }
);

},
buildFilterCondition(filterObject,filterData)
{
    if (!utils.isUndefined(filterObject))
        {
        if(!utils.isUndefined(filterObject.numeroCreditVente))
        {
                filterData["numeroCreditAchats"] = filterObject.numeroCreditAchats;
        }
         
        if(!utils.isUndefined(filterObject.montantAPayer))
          {
                 
                    filterData["montantAPayer"] = filterObject.montantAPayer;
                    
         }
         if(!utils.isUndefined(filterObject.descriptionEchange))
             {
                    
                       filterData["descriptionEchange"] =filterObject.descriptionEchange;
                       
            }
            if(!utils.isUndefined(filterObject.montantReste))
             {
                    
                       filterData["montantReste"] = filterObject.montantReste;
                       
            }
            if(!utils.isUndefined(filterObject.dateDebutPayementFromObject))
                {
                       
                          filterData["dateDebutPayementObject"] = {$gt:filterObject.dateDebutPayementFromObject};
                          
               }
               if(!utils.isUndefined(filterObject.dateDebutPayementToObject))
                {
                       
                          filterData["dateDebutPayementObject"] = {$lt:filterObject.dateDebutPayementToObject};
                          
               }
            if(!utils.isUndefined(filterObject.periodTranche))
             {
                    
                       filterData["periodTranche"] = filterObject.periodTranche ;
                       
            }
            if(!utils.isUndefined(filterObject.nbreTranches))
             {
                    
                       filterData["nbreTranches"] = filterObject.nbreTranches ;
                       
            }
            if(!utils.isUndefined(filterObject.etat))
             {
                    
                       filterData["etat"] = filterObject.etat ;
                       
            }
       
            if(!utils.isUndefined(filterObject.cinFournisseur))
                    {
                           
                              filterData["fournisseur.cin"] = filterObject.cinFournisseur;
                              
                   }
             if(!utils.isUndefined(filterObject.nomFournisseur))
                    {
                           
                              filterData["fournisseur.nom"] = filterObject.nomFournisseur;
                              
                   }

             if(!utils.isUndefined(filterObject.prenomFournisseur))
              {
                           
                              filterData["fournisseur.prenom"] = filterObject.prenomFournisseur ;
                              
              }
              if(!utils.isUndefined(filterObject.raisonFournisseur))
                {
                             
                                filterData["fournisseur.raisonSociale"] = filterObject.raisonFournisseur ;
                                
                }
                if(!utils.isUndefined(filterObject.matriculeFournisseur))
                    {
                                 
                                    filterData["fournisseur.matriculeFiscale"] = filterObject.matriculeFournisseur ;
                                    
                    }
            if(!utils.isUndefined(filterObject.registreFournisseur))
                        {
                                     
                            filterData["fournisseur.registreCommerce"] = filterObject.registreFournisseur ;
                                        
                        }


        }
        return filterData;
},

}
module.exports.CreditsAchats = CreditsAchats;