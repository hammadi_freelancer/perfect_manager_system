//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
// const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var DocumentsDemandesAchats = {
addDocumentDemandeAchats : function (db,documentDemandeAchats,codeUser,callback){
    if(documentDemandeAchats != undefined){
        documentDemandeAchats.code  = utils.isUndefined(documentDemandeAchats.code)?shortid.generate():documentDemandeAchats.code;
        documentDemandeAchats.dateReceptionObject = utils.isUndefined(documentDemandeAchats.dateReceptionObject)? 
        new Date():documentDemandeAchats.dateReceptionObject;
        documentDemandeAchats.userCreatorCode = codeUser;
        documentDemandeAchats.userLastUpdatorCode = codeUser;
        documentDemandeAchats.dateCreation = moment(new Date).format('L');
        documentDemandeAchats.dateLastUpdate = moment(new Date).format('L');
        
       
            db.collection('DocumentDemandeAchats').insertOne(
                documentDemandeAchats,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,documentDemandeAchats);
                   }
                }
              ) ;
    

}else{
callback(true,null);
}
},
updateDocumentDemandeAchats: function (db,documentDemandeAchats,codeUser, callback){

   
    documentDemandeAchats.dateReceptionObject = utils.isUndefined(documentDemandeAchats.dateReceptionObject)? 
    new Date():documentDemandeAchats.dateReceptionObject;

    const criteria = { "code" : documentDemandeAchats.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"dateReceptionObject" : documentDemandeAchats.dateReceptionObject, 
    "codeDemandeAchats" : documentDemandeAchats.codeDemandeAchats, 
    "description" : documentDemandeAchats.description,
     "codeOrganisation" : documentDemandeAchats.codeOrganisation,
     "typeDocument" : documentDemandeAchats.typeDocument, // cheque , virement , espece 
     "numeroDocument" : documentDemandeAchats.numeroDocument, 
     "imageFace1":documentDemandeAchats.imageFace1,
     "imageFace2" : documentDemandeAchats.imageFace2 , 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": documentDemandeAchats.dateLastUpdate, 
    } ;
       
            db.collection('DocumentDemandeAchats').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                },
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
      
    
    },
getListDocumentsDemandesAchats : function (db,codeUser,callback)
{
    let listDocumentsDemandesAchats = [];
  
   var cursor =  db.collection('DocumentDemandeAchats').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(docRelAchats,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        docRelAchats.dateReception = moment(docRelAchats.dateReceptionObject).format('L');
        
        listDocumentsDemandesAchats.push(docRelAchats);
   }).then(function(){
    //console.log('list documents credits',listDocumentsCredits);
    callback(null,listDocumentsDemandesAchats); 
   });

},
deleteDocumentDemandeAchats: function (db,codeDocumentDemandeAchats,codeUser,callback){
    const criteria = { "code" : codeDocumentDemandeAchats, "userCreatorCode":codeUser };
   
            db.collection('DocumentDemandeAchats').deleteOne(
                criteria ,
                function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }  
            ) ;
            
      
},

getDocumentDemandeAchats: function (db,codeDocumentDemandeAchats,codeUser,callback)
{
    const criteria = { "code" : codeDocumentDemandeAchats, "userCreatorCode":codeUser };

       const doc =  db.collection('DocumentDemandeAchats').findOne(
            criteria , function(err,result){
                if(err){
                    
                    callback(null,null);
                }else{
                    
                    callback(false,result);
                }
            }
        ) ;
                    
},


getListDocumentsDemandeAchatsByCodeDemande : function (db,codeUser,codeDemandeAchats,callback)
{
    let listDocumentsDemandeAchats = [];
     
   var cursor =  db.collection('DocumentDemandeAchats').find( 
       {"userCreatorCode" : codeUser, "codeDemandeAchats": codeDemandeAchats}
    );
   cursor.forEach(function(documentRA,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        documentRA.dateReception = moment(documentRA.dateReceptionObject).format('L');
        
        listDocumentsDemandeAchats.push(documentRA);
   }).then(function(){
  // console.log('list doc credits',listCredits);
    callback(null,listDocumentsDemandeAchats); 
   });
   

     //return [];
},


}
module.exports.DocumentsDemandesAchats = DocumentsDemandesAchats;