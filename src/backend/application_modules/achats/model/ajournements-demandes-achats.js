//var db = require('../common/MongoDbConnection').getDBConnection;
//const mongoClient = require('mongodb').MongoClient;
//const url ='mongodb://localhost:27017';
//const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var AjournementsRelevesAchats = {
addAjournementReleveAchats : function (db,ajournementReleveAchats,codeUser,callback){
    if(ajournementReleveAchats != undefined){
        ajournementReleveAchats.code  = utils.isUndefined(ajournementReleveAchats.code)?shortid.generate():ajournementReleveAchats.code;
        ajournementReleveAchats.dateReceptionObject = utils.isUndefined(ajournementReleveAchats.dateReceptionObject)? 
        new Date():ajournementReleveAchats.dateReceptionObject;
        ajournementReleveAchats.userCreatorCode = codeUser;
        ajournementReleveAchats.userLastUpdatorCode = codeUser;
        ajournementReleveAchats.dateCreation = moment(new Date).format('L');
        ajournementReleveAchats.dateLastUpdate = moment(new Date).format('L');
        
     
            db.collection('AjournementReleveAchats').insertOne(
                ajournementReleveAchats,function(err,result){
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,ajournementReleveAchats);
                   }
                }
              ) ;
             // db.close();
             
      

}else{
callback(true,null);
}
},
updateAjournementReleveAchats: function (db,ajournementReleveAchats,codeUser, callback){
    
       
    ajournementReleveAchats.dateOperationObject = utils.isUndefined(ajournementReleveAchats.dateOperationObject)? 
        new Date():ajournementReleveAchats.dateOperationObject;
        ajournementReleveAchats.dateLastUpdate = moment(new Date).format('L');
        
        const criteria = { "code" : ajournementReleveAchats.code,"userCreatorCode" : codeUser };
        const dataToUpdate = {
        "codeReleveAchats" : ajournementReleveAchats.codeReleveAchats, 
        "description" : ajournementReleveAchats.description,
         "codeOrganisation" : ajournementReleveAchats.codeOrganisation,
        "dateOperationObject" : ajournementReleveAchats.dateOperationObject ,
        "nouvelleDatePayementObject" : ajournementReleveAchats.nouvelleDatePayementObject ,
        "motif" : ajournementReleveAchats.motif ,
           "userLastUpdatorCode": codeUser,
           "dateLastUpdate": ajournementReleveAchats.dateLastUpdate, 
        } ;
         
                db.collection('AjournementReleveAchats').updateOne(
                    criteria,
                    { 
                        $set: dataToUpdate
                    },
                    function(err,result){
                        if(err){
                         callback(true,null);
                        }else{
     
                         callback(false,result);
                        }
                     }
                ) ;
            
        
        },
    getListAjournementsRelevesAchats : function (db,codeUser,callback)
    {
        let listAjournementsRelevesAchats = [];
      
       var cursor =  db.collection('AjournementReleveAchats').find( 
           {"userCreatorCode" : codeUser}
        );
       cursor.forEach(function(ajournementRelAchats,err){
           if(err)
            {
                console.log('errors produced :', err);
            }
            ajournementRelAchats.dateOperation = moment(ajournementRelAchats.dateOperationObject).format('L');
            ajournementRelAchats.nouvelleDatePayement = moment(ajournementRelAchats.nouvelleDatePayementObject).format('L');
            
            listAjournementsRelevesAchats.push(ajournementRelAchats);
       }).then(function(){
       //  console.log('list payements credits',listPayementsCredits);
        callback(null,listAjournementsRelevesAchats); 
       });
       

         //return [];
    },
    deleteAjournementReleveAchats: function (db,codeAjournementReleveAchats,codeUser,callback){
        const criteria = { "code" : codeAjournementReleveAchats, "userCreatorCode":codeUser };
          
                db.collection('AjournementReleveAchats').deleteOne(
                    criteria ,
                    function(err,result){
                        if(err){
                            
                            callback(null,null);
                        }else{
                            
                            callback(false,result);
                        }
                    }  
                ) ;
         
    },
    
    getAjournementReleveAchats: function (db,codeAjournementReleveAchats,codeUser,callback)
    {
        const criteria = { "code" : codeAjournementReleveAchats, "userCreatorCode":codeUser };
       
           const ajournmentRelAchats =  db.collection('AjournementReleveAchats').findOne(
                criteria , function(err,result){
                    if(err){
                        
                        callback(null,null);
                    }else{
                        
                        callback(false,result);
                    }
                }
            ) ;
                        
    },
    
    
    getListAjournementsRelevesAchatsByCodeReleveAchats : function (db,codeUser,codeReleveAchats,callback)
    {
        let listAjournementsRelevesAchats = [];
    
       var cursor =  db.collection('AjournementReleveAchats').find( 
           {"userCreatorCode" : codeUser, "codeReleveAchats": codeReleveAchats}
        );
       cursor.forEach(function(ajournementRelAchats,err){
           if(err)
            {
                console.log('errors produced :', err);
            }
            ajournementRelAchats.dateOperation = moment(ajournementRelAchats.dateOperationObject).format('L');
            ajournementRelAchats.nouvelleDatePayement = moment(ajournementRelAchats.nouvelleDatePayementObject).format('L');
            
            listAjournementsRelevesAchats.push(ajournementRelAchats);
       }).then(function(){
       // console.log('list credits',listCredits);
        callback(null,listAjournementsRelevesAchats); 
       });
       
    }


    
    }
    module.exports.AjournementsRelevesAchats = AjournementsRelevesAchats;