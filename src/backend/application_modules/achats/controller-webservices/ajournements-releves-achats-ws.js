
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var ajournementsRelevesAchats = require('../model/ajournements-releves-achats').AjournementsRelevesAchats;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addAjournementReleveAchats',function(req,res,next){
    
       console.log(" ADD AJOURNEMENT REL ACHATS  IS CALLED") ;
       console.log(" THE AJOURNEMENT REL ACHATS  TO ADDED IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = ajournementsRelevesAchats.addAjournementReleveAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,ajournementRelAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_AJOURNEMENT_RELEVE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(ajournementRelAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateAjournementReleveAchats',function(req,res,next){
    
       console.log(" UPDATE AJOURNEMENT REL ACHATS  IS CALLED") ;
       //console.log(" THE AJOURNEMENT FACT VENTES  TO UPDATE IS :",req.body);
      /* console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));*/
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));  
       var result = ajournementsRelevesAchats.updateAjournementReleveAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code,
         function(err,ajournementRelAchats){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_AJOURNEMENT_RELEVE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(ajournementRelAchats);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getAjournementsRelevesAchats',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  
    ajournementsRelevesAchats.getListAjournementsRelevesAchats(
        req.app.locals.dataBase,decoded.userData.code, function(err,listAjournementsRelAchats){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_AJOURNEMENTS_RELEVES_ACHATS',
                 error_content: err
             });
          }else{
        
         return res.json(listAjournementsRelAchats);
          }
      });

  });
  router.get('/getAjournementReleveAchats/:codeAjournementReleveAchats',function(req,res,next){
    // console.log("GET AJOURNEMENT RELEVE ACHATS  IS CALLED");
   // console.log(req.params['codeAjournementReleveAchats']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    ajournementsRelevesAchats.getAjournementReleveAchats(
        req.app.locals.dataBase,req.params['codeAjournementReleveAchats'],decoded.userData.code,
     function(err,ajournementRelAchats){
    
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_AJOURNEMENT_RELEVE_ACHATS',
               error_content: err
           });
        }else{
      
       return res.json(ajournementRelAchats);
        }
    });
})

  router.delete('/deleteAjournementReleveAchats/:codeAjournementReleveAchats',function(req,res,next){
    
       console.log(" DELETE AJOURNEMENT RELEVE ACHATS  IS CALLED") ;
       console.log(" THE AJOURNEMENT RELEVE ACHATS   TO DELETE IS :",req.params['codeAjournementReleveAchats']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = ajournementsRelevesAchats.deleteAjournementReleveAchats(
        req.app.locals.dataBase,req.params['codeAjournementReleveAchats'],decoded.userData.code,
        function(err,codeAjournementReleveAchats){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_AJOURNEMENT_RELEVE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeAjournementReleveAchats']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getAjournementsRelevesAchatsByCodeReleve/:codeReleveAchats',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    ajournementsRelevesAchats.getListAjournementsRelevesAchatsByCodeReleveAchats(
        req.app.locals.dataBase,decoded.userData.code,req.params['codeReleveAchats'],
         function(err,listAjournementsRelAchats){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_AJOURNEMENTS_RELEVES_ACHATS_BY_CODE_RELEVE',
                 error_content: err
             });
          }else{
        
         return res.json(listAjournementsRelAchats);
          }
      });

  });
  module.exports.routerAjournementsRelevesAchats= router;