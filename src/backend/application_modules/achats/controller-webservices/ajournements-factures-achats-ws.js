
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var ajournementsFacturesAchats = require('../model/ajournements-factures-achats').AjournementsFacturesAchats;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addAjournementFactureAchats',function(req,res,next){
    
       console.log(" ADD AJOURNEMENT FACT ACHATS  IS CALLED") ;
       console.log(" THE AJOURNEMENT FACT ACHATS  TO ADDED IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = ajournementsFacturesAchats.addAjournementFactureAchats
       req.app.locals.dataBase,(req.body,decoded.userData.code,function(err,ajournementFactAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_AJOURNEMENT_FACTURE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(ajournementFactAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateAjournementFactureAchats',function(req,res,next){
    
       console.log(" UPDATE AJOURNEMENT FACT ACHATS  IS CALLED") ;
       //console.log(" THE AJOURNEMENT FACT VENTES  TO UPDATE IS :",req.body);
      /* console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));*/
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));  
       var result = ajournementsFacturesAchats.updateAjournementFactureAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code,
         function(err,ajournementFactUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_AJOURNEMENT_FACTURE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(ajournementFactUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getAjournementsFacturesAchats',function(req,res,next){
   
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    ajournementsFacturesAchats.getListAjournementsFacturesAchats(
        req.app.locals.dataBase,decoded.userData.code, function(err,listAjournementsFactAchats){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_AJOURNEMENTS_FACTURES_ACHATS',
                 error_content: err
             });
          }else{
        
         return res.json(listAjournementsFactAchats);
          }
      });

  });
  router.get('/getAjournementFactureAchats/:codeAjournementFactureAchats',function(req,res,next){
    console.log("GET AJOURNEMENT FACTURE ACHATS  IS CALLED");
    console.log(req.params['codeAjournementFactureAchats']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    ajournementsFacturesAchats.getAjournementFactureAchats(
        req.app.locals.dataBase,req.params['codeAjournementFactureAchats'],decoded.userData.code,
     function(err,ajournementFactAchats){
       //console.log("GET  AJOURNEMENT CREDIT : RESULT");
       // console.log(ajournementCredit);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_AJOURNEMENT_FACTURE_ACHATS',
               error_content: err
           });
        }else{
      
       return res.json(ajournementFactAchats);
        }
    });
})

  router.delete('/deleteAjournementFactureAchats/:codeAjournementFactureAchats',function(req,res,next){
    
       console.log(" DELETE AJOURNEMENT FACTURE ACHATS  IS CALLED") ;
       console.log(" THE AJOURNEMENT FACTURE ACHATS   TO DELETE IS :",req.params['codeAjournementFactureAchats']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = ajournementsFacturesAchats.deleteAjournementFactureAchats(
        req.app.locals.dataBase,req.params['codeAjournementFactureAchats'],
       decoded.userData.code,
        function(err,codeAjournementFactureAchats){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_AJOURNEMENT_FACTURE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeAjournementFactureAchats']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getAjournementsFacturesAchatsByCodeFactureAchats/:codeFactureAchats',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    ajournementsFacturesAchats.getListAjournementsFacturesAchatsByCodeFactureAchats(
        req.app.locals.dataBase,decoded.userData.code,req.params['codeFactureAchats'],
         function(err,listAjournementsFactAchats){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_AJOURNEMENTS_FACTURES_ACHATS_BY_CODE_FACTURE_ACHATS',
                 error_content: err
             });
          }else{
        
         return res.json(listAjournementsFactAchats);
          }
      });

  });
  module.exports.routerAjournementsFacturesAchats= router;