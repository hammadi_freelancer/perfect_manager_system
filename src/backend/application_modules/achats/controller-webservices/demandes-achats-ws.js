
var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var demandesAchats = require('../model/demandes-achats').DemandesAchats;
// var commonManager = require('../application_modules/common/main').CommonManager;
var jwt = require('jsonwebtoken');

router.post('/addDemandeAchats',function(req,res,next){
    
    console.log(" ADD DEMANDE ACHATS IS CALLED") ;
       // console.log(" THE FACTURE VENTE TO ADDED IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = demandesAchats.addDemandeAchats(req.app.locals.dataBase,req.body,decoded.userData.code, function(err,demandeAchatsAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_DEMANDE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(demandeAchatsAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateDemandeAchats',function(req,res,next){
    console.log(" UPDATE DEMANDE ACHATS IS CALLED") ;
    
      
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = demandesAchats.updateDemandeAchats(req.app.locals.dataBase,req.body,decoded.userData.code, function(err,demandeAchatsUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_DEMANDE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(demandeAchatsUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getPageDemandesAchats',function(req,res,next){
     //  console.log("GET LIST FACTURES VENTES IS CALLED");
      var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    demandesAchats.getPageDemandesAchats(req.app.locals.dataBase,decoded.userData.code,
        req.query.pageNumber, req.query.nbElementsPerPage ,req.query.filter, function(err,pageDemandesAchats){
         // console.log("GET LIST FACTURES VENTES : RESULT");
        //  console.log(listFacturesVentes);
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_PAGE_DEMANDES_ACHATS',
                 error_content: err
             });
          }else{
        
         return res.json(pageDemandesAchats);
          }
      });

  });
  router.get('/getDemandeAchats/:codeDemandeAchats',function(req,res,next){
   
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    demandesAchats.getDemandeAchats(req.app.locals.dataBase,req.params['codeDemandeAchats'],decoded.userData.code, function(err,demandeAchats){
      //  console.log("GET  FACTURE VENTE : RESULT");
     //  console.log(factureVente);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_DEMANDE_ACHATS',
               error_content: err
           });
        }else{
      
       return res.json(demandeAchats);
        }
    });
})

  router.delete('/deleteDemandeAchats/:codeDemandeAchats',function(req,res,next){
    
       console.log(" DELETE DEMANDE ACHATS IS CALLED") ;
       console.log(" THE DEMANDE ACHATS  TO DELETE IS :",req.params['codeDemandeAchats']);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = demandesAchats.deleteDemandeAchats(req.app.locals.dataBase,req.params['codeDemandeAchats'],decoded.userData.code,function(err,codeDemandeAchats){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_DEMANDE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeDemandeAchats']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.get('/getTotalDemandesAchats',function(req,res,next){
 
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    demandesAchats.getTotalDemandesAchats(req.app.locals.dataBase,decoded.userData.code,req.query.filter,
        function(err,totalDemandesAchats){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_DEMANDES_ACHATS',
                 error_content: err
             });
          }else{
        
         return res.json({'totalDemandesAchats':totalDemandesAchats});
          }
      });

  });
  module.exports.routerDemandesAchats = router;