
var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var parametresAchats = require('../model/parametres-achats').ParametresAchats;
// var commonManager = require('../application_modules/common/main').CommonManager;
var jwt = require('jsonwebtoken');
router.post('/saveParametresAchats',function(req,res,next){
       console.log(" SAVE  PARAMETRES ACHATS IS CALLED") ;
       console.log(" THE PARAMETRES ACHATS TO ADDED IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = parametresAchats.saveParametresAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,parametresAchatsAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_SAVE_PARAMETRES_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(parametresAchatsAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
  router.get('/getParametresAchats',function(req,res,next){
    console.log("GET PARAMETRES ACHATS IS CALLED");
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    parametresAchats.getParametresAchats(
        req.app.locals.dataBase,decoded.userData.code ,function(err,parametresAchatsObject){
       console.log("GET  PARAMETRES ACHATS : RESULT");
       console.log(parametresAchatsObject);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_PARAMETRES_ACHATS',
               error_content: err
           });
        }else{
      
       return res.json(parametresAchatsObject);
        }
    });
})

 
  module.exports.routerParametresAchats = router;