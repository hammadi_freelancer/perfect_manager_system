
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var lignesReleveAchats = require('../model/lignes-releves-achats').LignesReleveAchats;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addLigneReleveAchats',function(req,res,next){
    
       console.log(" ADD LIGNE RELEVE ACHATS  IS CALLED") ;
       console.log(" THE LIGNE RELEVE ACHATS  TO ADDED IS :",req.body);
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = lignesReleveAchats.addLigneReleveAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,ligneFVAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_LIGNE_RELEVE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(ligneFVAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateLigneReleveAchats',function(req,res,next){
    
       console.log(" UPDATE LIGNE RELEVE ACHATS  IS CALLED") ;
       // console.log(" THE DOCUMENT CREDIT  TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result =lignesReleveAchats.updateLignesReleveAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,ligneFVUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_LIGNE_RELEVE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(ligneFVUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getLignesReleveAchats',function(req,res,next){
 
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    lignesReleveAchats.getListLignesReleveAchats(
        req.app.locals.dataBase,decoded.userData.code, function(err,listLignesFAchats){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_LIGNES_RELEVE_ACHATS',
                 error_content: err
             });
          }else{
        
         return res.json(listLignesFAchats);
          }
      });

  });
  router.get('/getLigneReleveAchats/:codeLigneReleveAchats',function(req,res,next){
    console.log("GET LIGNE FACT ACHATS IS CALLED");
   
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    lignesReleveAchats.getLigneReleveAchats(
        req.app.locals.dataBase,req.params['codeLigneReleveAchats'],decoded.userData.code,
     function(err,ligneFAchats){
      // console.log("GET  DOCUMENT CREDIT : RESULT");
       //console.log(documentCredit);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIGNE_RELEVE_ACHATS',
               error_content: err
           });
        }else{
      
       return res.json(ligneFAchats);
        }
    });
})

  router.delete('/deleteLigneReleveAchats/:codeLigneReleveAchats',function(req,res,next){
    
       console.log(" DELETE LIGNE RELEVE ACHATS  IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = lignesReleveAchats.deleteLigneReleveAchats(
        req.app.locals.dataBase,req.params['codeLigneReleveAchats'],decoded.userData.code,
        function(err,codeLigneReleveAchats){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_LIGNE_RELEVE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeLigneReleveAchats']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getLignesReleveAchatsByCodeReleveAchats/:codeReleveAchats',function(req,res,next){
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    lignesReleveAchats.getListLignesReleveAchatsByCodeReleveAchats(
        req.app.locals.dataBase,decoded.userData.code,req.params['codeReleveAchats'],
         function(err,listLignesFAchats){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_LIGNES_RELEVE_ACHATS',
                 error_content: err
             });
          }else{
        
         return res.json(listLignesFAchats);
          }
      });

  });
  module.exports.routesLignesRelevesAchats = router;