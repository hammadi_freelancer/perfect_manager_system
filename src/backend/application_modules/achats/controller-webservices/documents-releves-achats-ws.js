
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var documentsRelevesAchats = require('../model/documents-releves-achats').DocumentsRelevesAchats;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addDocumentReleveAchats',function(req,res,next){
    
       console.log(" ADD DOCUMENT RELEVE ACHATS  IS CALLED") ;
       console.log(" THE DOCUMENT RELEVE ACHATS  TO ADDED IS :",req.body);
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = documentsRelevesAchats.addDocumentReleveAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,documentRAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_DOCUMENT_RELEVE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(documentRAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateDocumentReleveAchats',function(req,res,next){
    
       console.log(" UPDATE DOCUMENT RELEVE ACHATS  IS CALLED") ;
       // console.log(" THE DOCUMENT CREDIT  TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result =documentsRelevesAchats.updateDocumentReleveAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,documentRAUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_DOCUMENT_RELEVE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(documentRAUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getDocumentsRelevesAchats',function(req,res,next){
 
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    documentsRelevesAchats.getListDocumentsRelevesAchats(
        req.app.locals.dataBase,decoded.userData.code, function(err,listDocumentsRelAchats){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_RELEVES_ACHATS',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsRelAchats);
          }
      });

  });
  router.get('/getDocumentReleveAchats/:codeDocumentReleveAchats',function(req,res,next){
    console.log("GET DOCUMENT REL ACHATS IS CALLED");
   
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    documentsRelevesAchats.getDocumentReleveAchats(
        req.app.locals.dataBase,req.params['codeDocumentReleveAchats'],decoded.userData.code,
     function(err,documentRAchats){
      // console.log("GET  DOCUMENT CREDIT : RESULT");
       //console.log(documentCredit);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_DOCUMENT_RELEVE_ACHATS',
               error_content: err
           });
        }else{
      
       return res.json(documentRAchats);
        }
    });
})

  router.delete('/deleteDocumentReleveAchats/:codeDocumentReleveAchats',function(req,res,next){
    
       console.log(" DELETE DOCUMENT RELEVE ACHATS  IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = documentsRelevesAchats.deleteDocumentReleveAchats(req.app.locals.dataBase,
        req.params['codeDocumentReleveAchats'],decoded.userData.code,
        function(err,codeDocumentReleveAchats){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_DOCUMENT_RELEVE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeDocumentReleveAchats']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getDocumentsRelevesAchatsByCodeReleve/:codeReleve',function(req,res,next){
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    documentsRelevesAchats.getListDocumentsReleveAchatsByCodeReleve(req.app.locals.dataBase,
        decoded.userData.code,req.params['codeReleve'],
         function(err,listDocumentsRelAchats){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_RELEVES_ACHATS',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsRelAchats);
          }
      });

  });
  module.exports.routerDocumentsRelevesAchats = router;