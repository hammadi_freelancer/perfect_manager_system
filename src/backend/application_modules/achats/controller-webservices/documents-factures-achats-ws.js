
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var documentsFacturesAchats = require('../model/documents-factures-achats').DocumentsFacturesAchats;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addDocumentFactureAchats',function(req,res,next){
    
       console.log(" ADD DOCUMENT FACTURE ACHATS  IS CALLED") ;
       console.log(" THE DOCUMENT FACTURE ACHATS  TO ADDED IS :",req.body);
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = documentsFacturesAchats.addDocumentFactureAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,documentFAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_DOCUMENT_FACTURE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(documentFAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateDocumentFactureAchats',function(req,res,next){
    
       console.log(" UPDATE DOCUMENT FACTURE ACHATS  IS CALLED") ;
       // console.log(" THE DOCUMENT CREDIT  TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result =documentsFacturesAchats.updateDocumentFactureAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,documentFAUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_DOCUMENT_FACTURE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(documentFAUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getDocumentsFacturesAchats',function(req,res,next){
 
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    documentsFacturesAchats.getListDocumentsFacturesAchats(req.app.locals.dataBase,
        decoded.userData.code, function(err,listDocumentsFAchats){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_FACTURES_ACHATS',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsFAchats);
          }
      });

  });
  router.get('/getDocumentFactureAchats/:codeDocumentFactureAchats',function(req,res,next){
    console.log("GET DOCUMENT FACT ACHATS IS CALLED");
   
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
        documentsFacturesAchats.getDocumentFactureAchats(
            req.app.locals.dataBase,req.params['codeDocumentFactureAchats'],decoded.userData.code,
     function(err,documentFAchats){
      // console.log("GET  DOCUMENT CREDIT : RESULT");
       //console.log(documentCredit);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_DOCUMENT_FACTURE_ACHATS',
               error_content: err
           });
        }else{
      
       return res.json(documentFAchats);
        }
    });
})

  router.delete('/deleteDocumentFactureAchats/:codeDocumentFactureAchats',function(req,res,next){
    
       console.log(" DELETE DOCUMENT FACTURE ACHATS  IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = documentsFacturesAchats.deleteDocumentFactureAchats(
        req.app.locals.dataBase,req.params['codeDocumentFactureAchats'],decoded.userData.code,
        function(err,codeDocumentFactureAchats){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_DOCUMENT_FACTURE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeDocumentFactureAchats']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getDocumentsFacturesAchatsByCodeFactureAchats/:codeFactureAchats',function(req,res,next){
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    documentsFacturesAchats.getListDocumentsFacturesAchatsByCodeFactureAchats(
        req.app.locals.dataBase,decoded.userData.code,req.params['codeFactureAchats'],
         function(err,listDocumentFAchats){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_FACTURES_ACHATS',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentFAchats);
          }
      });

  });
  module.exports.routerDocumentsFacturesAchats = router;