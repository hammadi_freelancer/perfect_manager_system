
var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var documentsAchats = require('../model/documents-achats').DocumentsAchats;
// var commonManager = require('../application_modules/common/main').CommonManager;
var jwt = require('jsonwebtoken');

router.post('/addDocumentAchats',function(req,res,next){
    
       console.log(" ADD DOCUMENT ACHATS IS CALLED") ;
       console.log(" THE DOCUMENT ACHATS TO ADDED IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = documentsAchats.addDocumentAchats(req.body,decoded.userData.code, function(err,documentAchatsAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_DOCUMENT_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(documentAchatsAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateDocumentAchats',function(req,res,next){
    
       console.log(" UPDATE DOCUMENT  ACHATS IS CALLED") ;
       console.log(" THE DOCUMENT ACHATS TO UPDATE IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = documentsAchats.updateDocumentAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,documentsAchatsAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_DOCUMENT_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(documentsAchatsAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getDocumentsAchats',function(req,res,next){
      console.log("GET LIST DOCUMENTS ACHATS IS CALLED");
      var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    documentsAchats.getListDocumentsAchats(req.app.locals.dataBase,decoded.userData.code, function(err,listDocumentsAchats){
         console.log("GET LIST DOCUMENTS ACHATS : RESULT");
         console.log(listDocumentsAchats);
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_ACHATS',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsAchats);
          }
      });

  });
  router.get('/getDocumentAchats/:codeDocumentAchats',function(req,res,next){
    console.log("GET DOCUMENT ACHATS  IS CALLED");
    console.log(req.params['codeDocumentAchats']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
        documentsAchats.getDocumentAchats(req.app.locals.dataBase,req.params['codeDocumentAchats'],decoded.userData.code, function(err,documentAchats){
       console.log("GET  DOCUMENT ACHATS : RESULT");
       console.log(documentVente);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_DOCUMENT_ACHATS',
               error_content: err
           });
        }else{
      
       return res.json(documentAchats);
        }
    });
})

  router.delete('/deleteDocumentAchats/:codeDocumentAchats',function(req,res,next){
    
       console.log(" DELETE DOCUMENT ACHATS  IS CALLED") ;
       console.log(" THE DOCUMENT ACHATS   TO DELETE IS :",req.params['codeDocumentAchats']);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = documentsAchats.deleteDocumentAchats(req.app.locals.dataBase,req.params['codeDocumentAchats'],decoded.userData.code,function(err,codeDocumentAchats){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_DOCUMENT_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeDocumentAchats']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
  module.exports.routerDocumentsAchats = router;