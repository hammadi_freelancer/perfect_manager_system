
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var documentsFournisseurs = require('../model/documents-fournisseurs').DocumentsFournisseurs;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addDocumentFournisseur',function(req,res,next){
    
       console.log(" ADD DOCUMENT FOURNISSEUR  IS CALLED") ;
       console.log(" THE DOCUMENT FOURNISSEUR  TO ADDED IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = documentsFournisseurs.addDocumentFournisseur(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,documentFournisseurAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_DOCUMENT_FOURNISSEUR',
                error_content: err
            });
         }else{
       
        return res.json(documentFournisseurAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE Fournisseur IS ",result);
     
   });
router.put('/updateDocumentFournisseur',function(req,res,next){
    
       console.log(" UPDATE DOCUMENT FOURNISSEUR  IS CALLED") ;
       // console.log(" THE DOCUMENT CREDIT  TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result =documentsFournisseurs.updateDocumentFournisseur(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,documentFournisseurUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_DOCUMENT_FOURNISSEUR',
                error_content: err
            });
         }else{
       
        return res.json(documentFournisseurUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE Fournisseur IS ",result);
     
   });
router.get('/getDocumentsFournisseurs',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    documentsFournisseurs.getListDocumentsFournisseurs(
        req.app.locals.dataBase,decoded.userData.code, function(err,listDocumentsFournisseurs){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_FOURNISSEURS',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsFournisseurs);
          }
      });

  });
  router.get('/getDocumentFournisseur/:codeDocumentFournisseur',function(req,res,next){
    console.log("GET DOCUMENT FOURNISSEUR  IS CALLED");
    console.log(req.params['codeDocumentFournisseur']);
   
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    documentsFournisseurs.getDocumentFournisseur(
        req.app.locals.dataBase,req.params['codeDocumentFournisseur'],decoded.userData.code,
     function(err,documentFournisseur){
       console.log("GET  DOCUMENT Fournisseur: RESULT");
       console.log(documentFournisseur);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_DOCUMENT_FOURNISSEUR',
               error_content: err
           });
        }else{
      
       return res.json(documentFournisseur);
        }
    });
})

  router.delete('/deleteDocumentFournisseur/:codeDocumentFournisseur',function(req,res,next){
    
       console.log(" DELETE DOCUMENT Fournisseur  IS CALLED") ;
       console.log(" THE DOCUMENT Fournisseur  TO DELETE IS :",req.params['codeDocumentFournisseur']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = documentsFournisseurs.deleteDocumentFournisseur(
        req.app.locals.dataBase,req.params['codeDocumentFournisseur'],decoded.userData.code,
        function(err,codeDocumentFournisseur){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_DOCUMENT_FOURNISSEUR',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeDocumentFournisseur']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE Fournisseur IS ",result);
     
   });

 router.get('/getDocumentsFournisseursByCodeFournisseur/:codeFournisseur',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    documentsFournisseurs.getListDocumentsFournisseursByCodeFournisseur(
        req.app.locals.dataBase,decoded.userData.code,req.params['codeFournisseur'],
         function(err,listDocumentFournisseurs){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_Fournisseurs_BY_CODE_FOURNISSEUR',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentFournisseurs);
          }
      });

  });
  module.exports.routerDocumentsFournisseurs = router;