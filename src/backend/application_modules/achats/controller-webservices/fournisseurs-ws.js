
var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var fournisseurs = require('../model/fournisseurs').Fournisseurs;
// var commonManager = require('../application_modules/common/main').CommonManager;
var jwt = require('jsonwebtoken');

router.post('/addFournisseur',function(req,res,next){
    

    console.log(" ADD FOURNISSEUR IS CALLED") ;
    console.log(" THE FOURNISSEUR TO ADDED IS :",req.body);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var result = fournisseurs.addFournisseur(req.app.locals.dataBase,req.body,decoded.userData.code, 
        function(err,fournisseurIsAdded){
     if(err){
         return  res.json({
             error_message : 'ERROR_ADD_FOURNISSEUR',
             error_content: err
         });
      }else{
    
     return res.json(fournisseurIsAdded);
    }
 }
 );
   // c









});
router.put('/updateFournisseur',function(req,res,next){
    
       console.log(" UPDATE FOURNISSEUR IS CALLED") ;
       console.log(" THE FOURNISSEUR TO UPDATE IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = fournisseurs.updateFournisseur(req.app.locals.dataBase,req.body,decoded.userData.code, function(err,fournisseurUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_FOURNISSEUR',
                error_content: err
            });
         }else{
       
        return res.json(fournisseurUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getFournisseurs',function(req,res,next){
    
      console.log("GET LIST FOURNISSEURS IS CALLED");
      var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
      fournisseurs.getListFournisseurs(req.app.locals.dataBase,decoded.userData.code , function(err,listFournisseurs){
         console.log("GET LIST FOURNISSEURS : RESULT");
         console.log(listFournisseurs);
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_FOURNISSEURS',
                 error_content: err
             });
          }else{
        
         return res.json(listFournisseurs);
          }
      });

  });
  router.get('/getPageFournisseurs',function(req,res,next){
    
      console.log("GET LIST FOURNISSEURS IS CALLED");
      var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
      fournisseurs.getPageFournisseurs(req.app.locals.dataBase,decoded.userData.code,
        req.query.pageNumber, req.query.nbElementsPerPage , req.query.filter,function(err,listFournisseurs){
         console.log("GET LIST FOURNISSEURS : RESULT");
         console.log(listFournisseurs);
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_FOURNISSEURS',
                 error_content: err
             });
          }else{
        
         return res.json(listFournisseurs);
          }
      });

  });
  /*router.get('/for',function(req,res,next){
    
      console.log("GET FOURNISSEUR IS CALLED");
  });*/

  router.delete('/deleteFournisseur/:codeFournisseur',function(req,res,next){
    
       console.log(" DELETE FOURNISSEUR IS CALLED") ;
       console.log(" THE FOURNISSEUR TO DELETE IS :",req.params['codeFournisseur']);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = fournisseurs.deleteFournisseur(req.app.locals.dataBase,req.params['codeFournisseur'],decoded.userData.code,
       function(err,codeFournisseur){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_FOURNISSEUR',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeFournisseur']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.get('/getFournisseur/:codeFournisseur',function(req,res,next){
    console.log("GET FOURNISSEUR IS CALLED");
    console.log(req.params['codeFournisseur']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    fournisseurs.getFournisseur(req.app.locals.dataBase,req.params['codeFournisseur'],decoded.userData.code, function(err,fournisseur){
       console.log("GET  FOURNISSEUR : RESULT");
       console.log(fournisseur);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_FOURNISSEUR',
               error_content: err
           });
        }else{
      
       return res.json(fournisseur);
        }
    });
});

router.get('/getTotalFournisseurs',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    fournisseurs.getTotalFournisseurs(req.app.locals.dataBase,decoded.userData.code,req.query.filter,
        function(err,totalFournisseurs){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_FOURNISSEURS',
                 error_content: err
             });
          }else{
        
         return res.json({'totalFournisseurs':totalFournisseurs});
          }
      });

  });
  module.exports.routerFournisseurs = router;