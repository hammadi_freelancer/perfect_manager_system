
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var lignesFactureAchats = require('../model/lignes-factures-achats').LignesFactureAchats;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addLigneFactureAchats',function(req,res,next){
    
       console.log(" ADD LIGNE FACTURE ACHATS  IS CALLED") ;
       console.log(" THE LIGNE FACTURE ACHATS  TO ADDED IS :",req.body);
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = lignesFactureAchats.addLigneFactureAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,ligneFVAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_LIGNE_FACTURE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(ligneFVAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateLigneFactureAchats',function(req,res,next){
    
       console.log(" UPDATE LIGNE FACTURE ACHATS  IS CALLED") ;
       // console.log(" THE DOCUMENT CREDIT  TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result =lignesFactureAchats.updateLignesFactureAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,ligneFVUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_LIGNE_FACTURE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(ligneFVUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getLignesFactureAchats',function(req,res,next){
 
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    lignesFactureAchats.getListLignesFactureAchats(
        req.app.locals.dataBase,decoded.userData.code, function(err,listLignesFAchats){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_LIGNES_FACTURE_ACHATS',
                 error_content: err
             });
          }else{
        
         return res.json(listLignesFAchats);
          }
      });

  });
  router.get('/getLigneFactureAchats/:codeLigneFactureAchats',function(req,res,next){
    console.log("GET LIGNE FACT ACHATS IS CALLED");
   
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    lignesFactureAchats.getLigneFactureAchats(
        req.app.locals.dataBase,req.params['codeLigneFactureAchats'],decoded.userData.code,
     function(err,ligneFAchats){
      // console.log("GET  DOCUMENT CREDIT : RESULT");
       //console.log(documentCredit);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_LIGNE_FACTURE_ACHATS',
               error_content: err
           });
        }else{
      
       return res.json(ligneFAchats);
        }
    });
})

  router.delete('/deleteLigneFactureAchats/:codeLigneFactureAchats',function(req,res,next){
    
       console.log(" DELETE LIGNE FACTURE ACHATS  IS CALLED") ;
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = lignesFactureAchats.deleteLigneFactureAchats(
        req.app.locals.dataBase,req.params['codeLigneFactureAchats'],decoded.userData.code,
        function(err,codeLigneFactureAchats){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_LIGNE_FACTURE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeLigneFactureAchats']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getLignesFactureAchatsByCodeFactureAchats/:codeFactureAchats',function(req,res,next){
  
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    lignesFactureAchats.getListLignesFactureAchatsByCodeFactureAchats(
        req.app.locals.dataBase,decoded.userData.code,req.params['codeFactureAchats'],
         function(err,listLignesFAchats){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_LIGNES_FACTURE_ACHATS',
                 error_content: err
             });
          }else{
        
         return res.json(listLignesFAchats);
          }
      });

  });
  module.exports.routerLignesFactureAchats = router;