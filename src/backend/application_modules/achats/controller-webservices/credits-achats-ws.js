
var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var creditsAchats = require('../model/credits-achats').CreditsAchats;
// var commonManager = require('../application_modules/common/main').CommonManager;
var jwt = require('jsonwebtoken');

router.post('/addCreditAchats',function(req,res,next){
    
       console.log(" ADD CREDIT ACHATS IS CALLED") ;
       console.log(" THE CREDIT ACHATS TO ADDED IS :",req.body);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = creditsAchats.addCreditAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,creditAchatsAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_CREDIT_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(creditAchatsAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateCreditAchats',function(req,res,next){
    
       console.log(" UPDATE CREDIT ACHATS IS CALLED") ;
       console.log(" THE CREDIT ACHATS TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = creditsAchats.updateCreditAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,creditAchatsUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_CREDIT_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(creditAchatsUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getCreditsAchats',function(req,res,next){
    
      console.log("GET LIST CREDITS ACHATS IS CALLED");
     
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

      creditsAchats.getListCreditsAchats(
        req.app.locals.dataBase,decoded.userData.code,req.query.pageNumber, req.query.nbElementsPerPage ,
        req.query.filter,function(err,listCreditsAchats){
         console.log("GET LIST CREDITS ACHATS : RESULT");
         console.log(listCreditsAchats);
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_CREDITS_ACHATS',
                 error_content: err
             });
          }else{
        
         return res.json(listCreditsAchats);
          }
      });

  });
  router.get('/getCreditAchats/:codeCreditAchats',function(req,res,next){
    console.log("GET CREDIT ACHATS IS CALLED");
    console.log(req.params['codeCreditAchats']);
    
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    creditsAchats.getCreditAchats(
        req.app.locals.dataBase,req.params['codeCreditAchats'],decoded.userData.code, function(err,creditAchats){
       console.log("GET  CREDIT ACHATS : RESULT");
       console.log(creditAchats);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_CREDIT_ACHATS',
               error_content: err
           });
        }else{
      
       return res.json(creditAchats);
        }
    });
})

  router.delete('/deleteCreditAchats/:codeCreditAchats',function(req,res,next){
    
       console.log(" DELETE CREDIT ACHATS  IS CALLED") ;
       console.log(" THE CREDIT ACHATS TO DELETE IS :",req.params['codeCreditAchats']);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = creditsAchats.deleteCreditAchats(
        req.app.locals.dataBase,req.params['codeCreditAchats'],decoded.userData.code,
       function(err,codeCredit){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_CREDIT_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeCreditAchats']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

   router.get('/getTotalCreditsAchats',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    creditsAchats.getTotalCreditsAchats(
        req.app.locals.dataBase,decoded.userData.code,req.query.filter,
        function(err,totalCreditsAchats){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_CREDITS_ACHATS',
                 error_content: err
             });
          }else{
        
         return res.json({'totalCreditsAchats':totalCreditsAchats});
          }
      });

  });
  module.exports.routerCreditsAchats = router;