
var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var relevesAchats = require('../model/releves-achats').RelevesAchats;
// var commonManager = require('../application_modules/common/main').CommonManager;
var jwt = require('jsonwebtoken');

router.post('/addReleveAchats',function(req,res,next){
    
       // console.log(" ADD FACTURE VENTE IS CALLED") ;
       // console.log(" THE FACTURE VENTE TO ADDED IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = relevesAchats.addReleveAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,relAchatsAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_RELEVE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(relAchatsAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateReleveAchats',function(req,res,next){
    
      
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = relevesAchats.updateReleveAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code, function(err,releveAchatsUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_RELEVE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(releveAchatsUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getPageRelevesAchats',function(req,res,next){
     //  console.log("GET LIST FACTURES VENTES IS CALLED");
      var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    relevesAchats.getListRelevesAchats(
        req.app.locals.dataBase,decoded.userData.code,
        req.query.pageNumber, req.query.nbElementsPerPage , req.query.filter,
        function(err,pageRelevesAchats){
         // console.log("GET LIST FACTURES VENTES : RESULT");
        //  console.log(listFacturesVentes);
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_PAGE_RELEVES_ACHATS',
                 error_content: err
             });
          }else{
        
         return res.json(pageRelevesAchats);
          }
      });

  });
  router.get('/getReleveAchats/:codeReleveAchats',function(req,res,next){
   
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    relevesAchats.getReleveAchats(
        req.app.locals.dataBase,req.params['codeReleveAchats'],decoded.userData.code, function(err,releveAchats){
      //  console.log("GET  FACTURE VENTE : RESULT");
     //  console.log(factureVente);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_RELEVE_ACHATS',
               error_content: err
           });
        }else{
      
       return res.json(releveAchats);
        }
    });
})

  router.delete('/deleteReleveAchats/:codeReleveAchats',function(req,res,next){
    
       console.log(" DELETE RELEVE ACHATS IS CALLED") ;
       console.log(" THE RELEVE ACHATS  TO DELETE IS :",req.params['codeReleveAchats']);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = relevesAchats.deleteReleveAchats(
        req.app.locals.dataBase,req.params['codeReleveAchats'],decoded.userData.code,function(err,codeReleveAchats){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_RELEVE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeReleveAchats']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   router.get('/getTotalRelevesAchats',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    relevesAchats.getTotalRelevesAchats(req.app.locals.dataBase,
        decoded.userData.code,req.query.filter,
        function(err,totalRelevesAchats){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_RELEVES_ACHATS',
                 error_content: err
             });
          }else{
        
         return res.json({'totalRelevesAchats':totalRelevesAchats});
          }
      });

  });
  module.exports.routerRelevesAchats = router;