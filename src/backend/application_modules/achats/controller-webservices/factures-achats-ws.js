
var express = require('express');
var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var facturesAchats = require('../model/factures-achats').FacturesAchats;
// var commonManager = require('../application_modules/common/main').CommonManager;
var jwt = require('jsonwebtoken');

router.post('/addFactureAchats',function(req,res,next){
    
       console.log(" ADD FACTURE ACHATS IS CALLED") ;
       console.log(" THE FACTURE ACHATS TO ADDED IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = facturesAchats.addFactureAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,factureAchatAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_FACTURE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(factureAchatAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateFactureAchats',function(req,res,next){
    
       console.log(" UPDATE FACTURE ACHATS IS CALLED") ;
       console.log(" THE FACTURE ACHATS TO UPDATE IS :",req.body);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = facturesAchats.updateFactureAchats(
        req.app.locals.dataBase,req.body,decoded.userData.code,function(err,factureAchatUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_FACTURE_ACHAT',
                error_content: err
            });
         }else{
       
        return res.json(factureAchatUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
/*router.get('/getFacturesAchats',function(req,res,next){
    
      console.log("GET LIST FACTURES ACHATS IS CALLED");
    facturesAchats.getListFacturesAchats(req.app.locals.dataBase,function(err,listFacturesAchats){
         console.log("GET LIST FACTURES ACHATS : RESULT");
         console.log(listFacturesAchats);
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_FACTURES_ACHATS',
                 error_content: err
             });
          }else{
        
         return res.json(listFacturesAchats);
          }
      });

  });*/


  router.get('/getPageFacturesAchats',function(req,res,next){
    console.log("GET PAGE FACTURES ACHATS IS CALLED");
    var decoded = jwt.decode(req.headers.authorization.slice( 
      req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
  facturesAchats.getListFacturesAchats(req.app.locals.dataBase,decoded.userData.code,
      req.query.pageNumber, req.query.nbElementsPerPage ,req.query.filter,
       function(err,listFacturesAchats){
       console.log("GET PAGE FACTURES ACHATS : RESULT");
       console.log(listFacturesAchats);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_PAGE_FACTURES_ACHATS',
               error_content: err
           });
        }else{
      
       return res.json(listFacturesAchats);
        }
    });

});
  router.get('/getFactureAchats/:codeFactureAchats',function(req,res,next){
    console.log("GET FACTURE ACHAT IS CALLED");
    console.log(req.params['codeFactureAchat']);
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    facturesAchats.getFactureAchats(
        req.app.locals.dataBase,req.params['codeFactureAchats'],
        decoded.userData.code,function(err,factureAchat){
       console.log("GET  FACTURE ACHAT : RESULT");
       console.log(factureAchat);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_FACTURE_ACHATS',
               error_content: err
           });
        }else{
      
       return res.json(factureAchat);
        }
    });
})

  router.delete('/deleteFactureAchats/:codeFactureAchats',function(req,res,next){
    
       console.log(" DELETE FACTURE ACHATS IS CALLED") ;
       console.log(" THE FACTURE ACHAT  TO DELETE IS :",req.params['codeFactureAchat']);
       var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = facturesAchats.deleteFactureAchats(
        req.app.locals.dataBase,
        req.params['codeFactureAchats'],decoded.userData.code,
        function(err,codeFactureAchats){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_FACTURE_ACHATS',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeFactureAchats']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
   
  router.get('/getTotalFacturesAchats',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    facturesAchats.getTotalFacturesAchats(req.app.locals.dataBase,decoded.userData.code,req.query.filter,
        function(err,totalFacturesAchats){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_FACTURES_ACHATS',
                 error_content: err
             });
          }else{
        
         return res.json({'totalFacturesAchats':totalFacturesAchats});
          }
      });

  });
  module.exports.routerFacturesAchats = router;