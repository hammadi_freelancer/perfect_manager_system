//var db = require('../common/MongoDbConnection').getDBConnection;
const mongoClient = require('mongodb').MongoClient;
const url ='mongodb://localhost:27017';
const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var DocumentsEntrees = {
addDocumentEntree : function (documentEntree,codeUser,callback){
    if(documentEntree != undefined){
        documentEntree.code  = utils.isUndefined(documentEntree.code)?shortid.generate():documentEntree.code;
        documentEntree.dateReceptionObject = utils.isUndefined(documentEntree.dateReceptionObject)? 
        new Date():documentEntree.dateReceptionObject;
        documentEntree.userCreatorCode = codeUser;
        documentEntree.userLastUpdatorCode = codeUser;
        documentEntree.dateCreation = moment(new Date).format('L');
        documentEntree.dateLastUpdate = moment(new Date).format('L');
        
        mongoClient.connect(url,function(err,clientDB){
            if(err)
             {
                callback(true,"ERROR_DATABASE_CONNECTION");
             }
            console.log('the client connected is ');
            console.log(clientDB)
            const db = clientDB.db(dbName);
            db.collection('DocumentEntree').insertOne(
                documentEntree,function(err,result){
                    clientDB.close();
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,documentEntree);
                   }
                }
              ) ;
             // db.close();
             
        });

}else{
callback(true,null);
}
},
updateDocumentEntree: function (documentEntree,codeUser, callback){

   
    documentEntree.dateReceptionObject = utils.isUndefined(documentEntree.dateReceptionObject)? 
    new Date():documentEntree.dateReceptionObject;

    const criteria = { "code" : documentEntree.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"dateReceptionObject" : documentEntree.dateReceptionObject, 
    "codeEntree" : documentEntree.codeEntree, 
    "description" : documentEntree.description,
     "codeOrganisation" : documentEntree.codeOrganisation,
     "typeDocument" : documentEntree.typeDocument, // cheque , virement , espece 
     "numeroDocumentEntree" : documentEntree.numeroDocumentEntree, 
     "imageFace1":documentEntree.imageFace1,
     "imageFace2" : documentEntree.imageFace2 , 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": documentEntree.dateLastUpdate, 
    } ;
        mongoClient.connect(url,function(err,clientDB){
            if(err)
             {
                callback(true,"ERROR_DATABASE_CONNECTION");
             }
            console.log('the client connected is ');
            console.log(clientDB)
            const db = clientDB.db(dbName);
            db.collection('DocumentEntree').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                }
            ) ;
             clientDB.close();
             callback(false,documentEntree);
        });
    
    },
getListDocumentsEntrees : function (codeUser,callback)
{
    let listDocumentsEntrees = [];
    mongoClient.connect(url,function(err,clientDB){
        if(err)
        {
            callback(err,[]);
        }else{
        const db = clientDB.db(dbName);   
   var cursor =  db.collection('DocumentEntree').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(docEntree,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        docEntree.dateReception = moment(docEntree.dateReceptionObject).format('L');
        
        listDocumentsEntrees.push(docEntree);
   }).then(function(){
    console.log('list documents Entrees',listDocumentsEntrees);
    callback(null,listDocumentsEntrees); 
   });
   
}
});
     //return [];
},
deleteDocumentEntree: function (codeDocumentEntree,codeUser,callback){
    const criteria = { "code" : codeDocumentEntree, "userCreatorCode":codeUser };
        mongoClient.connect(url,function(err,clientDB){
            if(err)
             {
                callback(true,"ERROR_DATABASE_CONNECTION");
             }
            console.log('the client connected is ');
            console.log(clientDB)
            const db = clientDB.db(dbName);
            db.collection('DocumentEntree').deleteOne(
                criteria 
            ) ;
             clientDB.close();
             callback(false,codeDocumentEntree);
        });
},

getDocumentEntree: function (codeDocumentEntree,codeUser,callback)
{
    const criteria = { "code" : codeDocumentEntree, "userCreatorCode":codeUser };
    mongoClient.connect(url,function(err,clientDB){
        if(err)
         {
            callback(true,"ERROR_DATABASE_CONNECTION");
         }
        console.log('the client connected is ');
        console.log(clientDB)
        const db = clientDB.db(dbName);
       const Entree =  db.collection('DocumentEntree').findOne(
            criteria , function(err,result){
                if(err){
                    clientDB.close();
                    
                    callback(null,null);
                }else{
                    clientDB.close();
                    
                    callback(false,result);
                }
            }
        ) ;
        // callback(false,client);
    });                    
},


getListDocumentsEntreesByCodeEntree : function (codeUser,codeEntree,callback)
{
    let listDocumentsEntrees = [];
    mongoClient.connect(url,function(err,clientDB){
        if(err)
        {
            callback(err,[]);
        }else{
        const db = clientDB.db(dbName);   
   var cursor =  db.collection('DocumentEntree').find( 
       {"userCreatorCode" : codeUser, "codeEntree": codeEntree}
    );
   cursor.forEach(function(documentEntree,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        documentEntree.dateReception = moment(documentEntree.dateReceptionObject).format('L');
        
        listDocumentsEntrees.push(documentEntree);
   }).then(function(){
  // console.log('list doc Entrees',listEntrees);
    callback(null,listDocumentsEntrees); 
   });
   
}
});
     //return [];
},


}
module.exports.DocumentsEntrees = DocumentsEntrees;