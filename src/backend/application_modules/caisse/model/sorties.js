//var db = require('../common/MongoDbConnection').getDBConnection;
const mongoClient = require('mongodb').MongoClient;
const url ='mongodb://localhost:27017';
const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var Sorties = {
addSortie : function (sortie,codeUser,callback){
    if(sortie != undefined){
        sortie.code  = utils.isUndefined(sortie.code)?shortid.generate():sortie.code;
        sortie.dateOperationObject = utils.isUndefined(sortie.dateOperationObject)? 
        new Date():sortie.dateOperationObject;
        sortie.dateVirementChequeObject = utils.isUndefined(sortie.dateVirementChequeObject)? 
        new Date():sortie.dateVirementChequeObject;
        if(utils.isUndefined(sortie.numeroSortie))
            {
               const currentD =  moment(new Date);
              // const genereKey = shortid.generate();
      const generatedCode = currentD.year().toString() +'/'+
      currentD.month().toString()+currentD.minutes().toString()+currentD.seconds().toString() ;
      sortie.numeroSortie = 'SORT' + generatedCode;
            }
   
            sortie.userCreatorCode = codeUser;
            sortie.userLastUpdatorCode = codeUser;
            sortie.dateCreation = moment(new Date).format('L');
            sortie.dateLastUpdate = moment(new Date).format('L');
        
        mongoClient.connect(url,function(err,clientDB){
            if(err)
             {
                callback(true,"ERROR_DATABASE_CONNECTION");
             }
            console.log('the client connected is ');
            console.log(clientDB)
            const db = clientDB.db(dbName);
            db.collection('Sortie').insertOne(
                sortie,function(err,result){
                    clientDB.close();
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
             // db.close();
             
        });

}else{
callback(true,null);
}
},
updateSortie: function (sortie,codeUser, callback){

    sortie.dateLastUpdate = moment(new Date).format('L');
    
    sortie.dateOperationObject = utils.isUndefined(sortie.dateOperationObject)? 
    new Date():sortie.dateOperationObject;
    sortie.dateVirementChequeObject = utils.isUndefined(sortie.dateVirementChequeObject)? 
    new Date():sortie.dateVirementChequeObject;

    const criteria = { "code" : sortie.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"numeroSortie" : sortie.numeroSortie, 
     "motif" : sortie.motif,
    "dateOperationObject" : sortie.dateOperationObject ,
    "dateVirementChequeObject" : sortie.dateVirementChequeObject ,
    "numeroCheque" : sortie.numeroCheque ,
    "proprietaireCheque" : sortie.proprietaireCheque ,
    "montant":sortie.montant,
    "description":sortie.description,
      "etat": sortie.etat,
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": sortie.dateLastUpdate, 
    } ;
        mongoClient.connect(url,function(err,clientDB){
            if(err)
             {
                callback(true,"ERROR_DATABASE_CONNECTION");
             }
            console.log('the client connected is ');
            console.log(clientDB)
            const db = clientDB.db(dbName);
            db.collection('Sortie').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                }
            ) ;
             clientDB.close();
             callback(false,sortie);
        });
    
    },
getListSorties : function (codeUser,pageNumber, nbElementsPerPage , callback)
{
    let listSorties = [];
    mongoClient.connect(url,function(err,clientDB){
        if(err)
        {
            callback(err,[]);
        }else{
        const db = clientDB.db(dbName);  
   var cursor =  db.collection('Sortie').find( 
       {"userCreatorCode" : codeUser}
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(sortie,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        sortie.dateOperation = moment(sortie.dateOperationObject).format('L');
        sortie.dateVirementCheque = moment(sortie.dateVirementChequeObject).format('L');
        
        listSorties.push(sortie);
   }).then(function(){
    callback(null,listSorties); 
   });
   
}
});
     //return [];
},
deleteSortie: function (codeSortie,codeUser,callback){
    const criteria = { "code" : codeSortie, "userCreatorCode":codeUser };
        mongoClient.connect(url,function(err,clientDB){
            if(err)
             {
                callback(true,"ERROR_DATABASE_CONNECTION");
             }
            console.log('the client connected is ');
            console.log(clientDB)
            const db = clientDB.db(dbName);
            db.collection('Sortie').deleteOne(
                criteria 
            ) ;
             clientDB.close();
             callback(false,codeSortie);
        });
},

getSortie: function (codeSortie,codeUser,callback)
{
    const criteria = { "code" : codeSortie, "userCreatorCode":codeUser };
    mongoClient.connect(url,function(err,clientDB){
        if(err)
         {
            callback(true,"ERROR_DATABASE_CONNECTION");
         }
        console.log('the client connected is ');
        console.log(clientDB)
        const db = clientDB.db(dbName);
       const sortie =  db.collection('Sortie').findOne(
            criteria , function(err,result){
                if(err){
                    clientDB.close();
                    
                    callback(null,null);
                }else{
                    clientDB.close();
                    
                    callback(false,result);
                }
            }
        ) ;
        // callback(false,client);
    });                    
},
getTotalSorties : function (codeUser, callback)
{
    let listSorties = [];
    mongoClient.connect(url,function(err,clientDB){
        if(err)
        {
            callback(err,[]);
        }else{
        const db = clientDB.db(dbName);   
   var totalSorties =  db.collection('Sortie').find( 
       {"userCreatorCode" : codeUser}
    ).count(function(err,result){
        callback(null,result); 
    }
);
   
}
});
     //return [];
}
}
module.exports.Sorties = Sorties;