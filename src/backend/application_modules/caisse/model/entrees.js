//var db = require('../common/MongoDbConnection').getDBConnection;
const mongoClient = require('mongodb').MongoClient;
const url ='mongodb://localhost:27017';
const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var Entrees = {
addEntree : function (entree,codeUser,callback){
    if(entree != undefined){
        entree.code  = utils.isUndefined(entree.code)?shortid.generate():entree.code;
        entree.dateOperationObject = utils.isUndefined(entree.dateOperationObject)? 
        new Date():entree.dateOperationObject;
        entree.dateVirementChequeObject = utils.isUndefined(entree.dateVirementChequeObject)? 
        new Date():entree.dateVirementChequeObject;
        if(utils.isUndefined(entree.numeroEntree))
            {
               const currentD =  moment(new Date);
              // const genereKey = shortid.generate();
      const generatedCode = currentD.year().toString() +'/'+
      currentD.month().toString()+currentD.minutes().toString()+currentD.seconds().toString() ;
               entree.numeroEntree = 'ENT' + generatedCode;
            }
   
            entree.userCreatorCode = codeUser;
            entree.userLastUpdatorCode = codeUser;
            entree.dateCreation = moment(new Date).format('L');
            entree.dateLastUpdate = moment(new Date).format('L');
        
        mongoClient.connect(url,function(err,clientDB){
            if(err)
             {
                callback(true,"ERROR_DATABASE_CONNECTION");
             }
            console.log('the client connected is ');
            console.log(clientDB)
            const db = clientDB.db(dbName);
            db.collection('Entree').insertOne(
                entree,function(err,result){
                    clientDB.close();
                   if(err){
                    callback(true,null);
                   }else{
                    callback(false,result);
                   }
                }
              ) ;
             // db.close();
             
        });

}else{
callback(true,null);
}
},
updateEntree: function (entree,codeUser, callback){

    entree.dateLastUpdate = moment(new Date).format('L');
    
    entree.dateOperationObject = utils.isUndefined(entree.dateOperationObject)? 
    new Date():entree.dateOperationObject;
    entree.dateVirementChequeObject = utils.isUndefined(entree.dateVirementChequeObject)? 
    new Date():entree.dateVirementChequeObject;

    const criteria = { "code" : entree.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"numeroEntree" : entree.numeroEntree, 
     "motif" : entree.motif,
    "dateOperationObject" : entree.dateOperationObject ,
    "dateVirementChequeObject" : entree.dateVirementChequeObject ,
    "numeroCheque" : entree.numeroCheque ,
    "proprietaireCheque" : entree.proprietaireCheque ,
    "montant":entree.montant,
    "description":entree.description,
      "etat": entree.etat,
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": entree.dateLastUpdate, 
    } ;
        mongoClient.connect(url,function(err,clientDB){
            if(err)
             {
                callback(true,"ERROR_DATABASE_CONNECTION");
             }
            console.log('the client connected is ');
            console.log(clientDB)
            const db = clientDB.db(dbName);
            db.collection('Entree').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                }
            ) ;
             clientDB.close();
             callback(false,entree);
        });
    
    },
getListEntrees : function (codeUser,pageNumber, nbElementsPerPage , callback)
{
    let listEntrees = [];
    mongoClient.connect(url,function(err,clientDB){
        if(err)
        {
            callback(err,[]);
        }else{
        const db = clientDB.db(dbName);  
   var cursor =  db.collection('Entree').find( 
       {"userCreatorCode" : codeUser}
    ).skip( pageNumber > 0 ? ( ( pageNumber ) * nbElementsPerPage ) : 0 )
    .limit(Number(nbElementsPerPage) );
   cursor.forEach(function(entree,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        entree.dateOperation = moment(entree.dateOperationObject).format('L');
        entree.dateVirementCheque = moment(entree.dateVirementChequeObject).format('L');
        
        listEntrees.push(entree);
   }).then(function(){
    callback(null,listEntrees); 
   });
   
}
});
     //return [];
},
deleteEntree: function (codeEntree,codeUser,callback){
    const criteria = { "code" : codeEntree, "userCreatorCode":codeUser };
        mongoClient.connect(url,function(err,clientDB){
            if(err)
             {
                callback(true,"ERROR_DATABASE_CONNECTION");
             }
            console.log('the client connected is ');
            console.log(clientDB)
            const db = clientDB.db(dbName);
            db.collection('Entree').deleteOne(
                criteria 
            ) ;
             clientDB.close();
             callback(false,codeEntree);
        });
},

getEntree: function (codeEntree,codeUser,callback)
{
    const criteria = { "code" : codeEntree, "userCreatorCode":codeUser };
    mongoClient.connect(url,function(err,clientDB){
        if(err)
         {
            callback(true,"ERROR_DATABASE_CONNECTION");
         }
        console.log('the client connected is ');
        console.log(clientDB)
        const db = clientDB.db(dbName);
       const entree =  db.collection('Entree').findOne(
            criteria , function(err,result){
                if(err){
                    clientDB.close();
                    
                    callback(null,null);
                }else{
                    clientDB.close();
                    
                    callback(false,result);
                }
            }
        ) ;
        // callback(false,client);
    });                    
},
getTotalEntrees : function (codeUser, callback)
{
    let listEntrees = [];
    mongoClient.connect(url,function(err,clientDB){
        if(err)
        {
            callback(err,[]);
        }else{
        const db = clientDB.db(dbName);   
   var totalEntrees =  db.collection('Entree').find( 
       {"userCreatorCode" : codeUser}
    ).count(function(err,result){
        callback(null,result); 
    }
);
   
}
});
     //return [];
}
}
module.exports.Entrees = Entrees;