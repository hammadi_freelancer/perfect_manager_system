//var db = require('../common/MongoDbConnection').getDBConnection;
const mongoClient = require('mongodb').MongoClient;
const url ='mongodb://localhost:27017';
const dbName = 'gestion';
//console.log('db is ',db);
var shortid = require('shortid');
var async = require('async');
var util = require('util');
var moment = require('moment');
var utils = require('../../utils/utils');

//we need to have article as a json object : {code :'mmm', design :'ddd'}
var DocumentsSorties = {
addDocumentSortie : function (documentSortie,codeUser,callback){
    if(documentSortie != undefined){
        documentSortie.code  = utils.isUndefined(documentSortie.code)?shortid.generate():documentSortie.code;
        documentSortie.dateReceptionObject = utils.isUndefined(documentSortie.dateReceptionObject)? 
        new Date():documentSortie.dateReceptionObject;
        documentSortie.userCreatorCode = codeUser;
        documentSortie.userLastUpdatorCode = codeUser;
        documentSortie.dateCreation = moment(new Date).format('L');
        documentSortie.dateLastUpdate = moment(new Date).format('L');
        
        mongoClient.connect(url,function(err,clientDB){
            if(err)
             {
                callback(true,"ERROR_DATABASE_CONNECTION");
             }
            console.log('the client connected is ');
            console.log(clientDB)
            const db = clientDB.db(dbName);
            db.collection('DocumentSortie').insertOne(
                documentSortie,function(err,result){
                    clientDB.close();
                   if(err){
                    callback(true,null);
                   }else{

                    callback(false,documentSortie);
                   }
                }
              ) ;
             // db.close();
             
        });

}else{
callback(true,null);
}
},
updateDocumentSortie: function (documentSortie,codeUser, callback){

   
    documentSortie.dateReceptionObject = utils.isUndefined(documentSortie.dateReceptionObject)? 
    new Date():documentSortie.dateReceptionObject;

    const criteria = { "code" : documentSortie.code,"userCreatorCode" : codeUser };
    const dataToUpdate = {"dateReceptionObject" : documentSortie.dateReceptionObject, 
    "codeSortie" : documentSortie.codeSortie, 
    "description" : documentSortie.description,
     "codeOrganisation" : documentSortie.codeOrganisation,
     "typeDocument" : documentSortie.typeDocument, // cheque , virement , espece 
     "numeroDocumentSortie" : documentSortie.numeroDocumentSortie, 
     "imageFace1":documentSortie.imageFace1,
     "imageFace2" : documentSortie.imageFace2 , 
       "userLastUpdatorCode": codeUser,
       "dateLastUpdate": documentSortie.dateLastUpdate, 
    } ;
        mongoClient.connect(url,function(err,clientDB){
            if(err)
             {
                callback(true,"ERROR_DATABASE_CONNECTION");
             }
            console.log('the client connected is ');
            console.log(clientDB)
            const db = clientDB.db(dbName);
            db.collection('DocumentSortie').updateOne(
                criteria,
                { 
                    $set: dataToUpdate
                }
            ) ;
             clientDB.close();
             callback(false,documentSortie);
        });
    
    },
getListDocumentsSorties : function (codeUser,callback)
{
    let listDocumentsSorties = [];
    mongoClient.connect(url,function(err,clientDB){
        if(err)
        {
            callback(err,[]);
        }else{
        const db = clientDB.db(dbName);   
   var cursor =  db.collection('DocumentSortie').find( 
       {"userCreatorCode" : codeUser}
    );
   cursor.forEach(function(docSortie,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        docSortie.dateReception = moment(docSortie.dateReceptionObject).format('L');
        
        listDocumentsSorties.push(docSortie);
   }).then(function(){
    console.log('list documents Sorties',listDocumentsSorties);
    callback(null,listDocumentsSorties); 
   });
   
}
});
     //return [];
},
deleteDocumentSortie: function (codeDocumentSortie,codeUser,callback){
    const criteria = { "code" : codeDocumentSortie, "userCreatorCode":codeUser };
        mongoClient.connect(url,function(err,clientDB){
            if(err)
             {
                callback(true,"ERROR_DATABASE_CONNECTION");
             }
            console.log('the client connected is ');
            console.log(clientDB)
            const db = clientDB.db(dbName);
            db.collection('DocumentSortie').deleteOne(
                criteria 
            ) ;
             clientDB.close();
             callback(false,codeDocumentSortie);
        });
},

getDocumentSortie: function (codeDocumentSortie,codeUser,callback)
{
    const criteria = { "code" : codeDocumentSortie, "userCreatorCode":codeUser };
    mongoClient.connect(url,function(err,clientDB){
        if(err)
         {
            callback(true,"ERROR_DATABASE_CONNECTION");
         }
        console.log('the client connected is ');
        console.log(clientDB)
        const db = clientDB.db(dbName);
       const Sortie =  db.collection('DocumentSortie').findOne(
            criteria , function(err,result){
                if(err){
                    clientDB.close();
                    
                    callback(null,null);
                }else{
                    clientDB.close();
                    
                    callback(false,result);
                }
            }
        ) ;
        // callback(false,client);
    });                    
},


getListDocumentsSortiesByCodeSortie : function (codeUser,codeSortie,callback)
{
    let listDocumentsSorties = [];
    mongoClient.connect(url,function(err,clientDB){
        if(err)
        {
            callback(err,[]);
        }else{
        const db = clientDB.db(dbName);   
   var cursor =  db.collection('DocumentSortie').find( 
       {"userCreatorCode" : codeUser, "codeSortie": codeSortie}
    );
   cursor.forEach(function(documentSortie,err){
       if(err)
        {
            console.log('errors produced :', err);
        }
        documentSortie.dateReception = moment(documentSortie.dateReceptionObject).format('L');
        
        listDocumentsSorties.push(documentSortie);
   }).then(function(){
  // console.log('list doc Sorties',listSorties);
    callback(null,listDocumentsSorties); 
   });
   
}
});
     //return [];
},


}
module.exports.DocumentsSorties = DocumentsSorties;