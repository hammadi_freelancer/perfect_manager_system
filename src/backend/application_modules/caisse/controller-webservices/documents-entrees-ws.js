
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var documentsEntrees = require('../model/documents-entrees').DocumentsEntrees;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addDocumentEntree',function(req,res,next){
    
       console.log(" ADD DOCUMENT Entree  IS CALLED") ;
       console.log(" THE DOCUMENT Entree  TO ADDED IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = documentsEntrees.addDocumentEntree(req.body,decoded.userData.code,function(err,documentEntreesAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_DOCUMENT_Entree',
                error_content: err
            });
         }else{
       
        return res.json(documentEntreesAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateDocumentEntree',function(req,res,next){
    
       console.log(" UPDATE DOCUMENT Entree  IS CALLED") ;
       // console.log(" THE DOCUMENT Entree  TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result =documentsEntrees.updateDocumentEntree(req.body,decoded.userData.code, function(err,documentEntreeUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_DOCUMENT_Entree',
                error_content: err
            });
         }else{
       
        return res.json(documentEntreeUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getDocumentsEntrees',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    documentsEntrees.getListDocumentEntrees(decoded.userData.code, function(err,listDocumentsEntrees){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_EntreeS',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsEntrees);
          }
      });

  });
  router.get('/getDocumentEntree/:codeDocumentEntree',function(req,res,next){
    console.log("GET DOCUMENT Entree  IS CALLED");
    console.log(req.params['codeDocumentEntree']);
   
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    documentsEntrees.getDocumentEntree(req.params['codeDocumentEntree'],decoded.userData.code,
     function(err,documentEntree){
       console.log("GET  DOCUMENT Entree : RESULT");
       console.log(documentEntree);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_DOCUMENT_Entree',
               error_content: err
           });
        }else{
      
       return res.json(documentEntree);
        }
    });
})

  router.delete('/deleteDocumentEntree/:codeDocumentEntree',function(req,res,next){
    
       console.log(" DELETE DOCUMENT Entree  IS CALLED") ;
       console.log(" THE DOCUMENT Entree  TO DELETE IS :",req.params['codeDocumentEntree']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = documentsEntrees.deleteDocumentEntree(req.params['codeDocumentEntree'],decoded.userData.code,
        function(err,codeDocumentEntree){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_DOCUMENT_Entree',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeDocumentEntree']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getDocumentsEntreesByCodeEntree/:codeEntree',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    documentsEntrees.getListDocumentsEntreesByCodeEntree(decoded.userData.code,req.params['codeEntree'],
         function(err,listDocumentEntrees){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_EntreeS_BY_CODE_Entree',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentEntrees);
          }
      });

  });
  module.exports.routerDocumentsEntrees = router;