
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var documentsSorties = require('../model/documents-sorties').DocumentsSorties;
// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addDocumentSortie',function(req,res,next){
    
       console.log(" ADD DOCUMENT Sortie  IS CALLED") ;
       console.log(" THE DOCUMENT Sortie  TO ADDED IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = documentsSorties.addDocumentSortie(req.body,decoded.userData.code,function(err,documentSortiesAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_DOCUMENT_Sortie',
                error_content: err
            });
         }else{
       
        return res.json(documentSortiesAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateDocumentSortie',function(req,res,next){
    
       console.log(" UPDATE DOCUMENT Sortie  IS CALLED") ;
       // console.log(" THE DOCUMENT Sortie  TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result =documentsSorties.updateDocumentSortie(req.body,decoded.userData.code, function(err,documentSortieUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_DOCUMENT_Sortie',
                error_content: err
            });
         }else{
       
        return res.json(documentSortieUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getDocumentsSorties',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    documentsSorties.getListDocumentSorties(decoded.userData.code, function(err,listDocumentsSorties){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_SortieS',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentsSorties);
          }
      });

  });
  router.get('/getDocumentSortie/:codeDocumentSortie',function(req,res,next){
    console.log("GET DOCUMENT Sortie  IS CALLED");
    console.log(req.params['codeDocumentSortie']);
   
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    documentsSorties.getDocumentSortie(req.params['codeDocumentSortie'],decoded.userData.code,
     function(err,documentSortie){
       console.log("GET  DOCUMENT Sortie : RESULT");
       console.log(documentSortie);
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_DOCUMENT_Sortie',
               error_content: err
           });
        }else{
      
       return res.json(documentSortie);
        }
    });
})

  router.delete('/deleteDocumentSortie/:codeDocumentSortie',function(req,res,next){
    
       console.log(" DELETE DOCUMENT Sortie  IS CALLED") ;
       console.log(" THE DOCUMENT Sortie  TO DELETE IS :",req.params['codeDocumentSortie']);
       
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var result = documentsSorties.deleteDocumentSortie(req.params['codeDocumentSortie'],decoded.userData.code,
        function(err,codeDocumentSortie){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_DOCUMENT_Sortie',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeDocumentSortie']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });

 router.get('/getDocumentsSortiesByCodeSortie/:codeSortie',function(req,res,next){
    console.log('request  authorization');
    console.log(req.headers.authorization)
    console.log('to decode is ');
    console.log(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    
   // get the decoded payload and header
  // var decoded = jwt.decode(token, {complete: true});
  console.log('decoded tocken ');
   console.log(decoded);
    documentsSorties.getListDocumentsSortiesByCodeSortie(decoded.userData.code,req.params['codeSortie'],
         function(err,listDocumentSorties){
       
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_DOCUMENTS_SortieS_BY_CODE_Sortie',
                 error_content: err
             });
          }else{
        
         return res.json(listDocumentSorties);
          }
      });

  });
  module.exports.routerDocumentsSorties = router;