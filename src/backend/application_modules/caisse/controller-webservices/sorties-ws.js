
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var sorties = require('../model/sorties').Sorties;
// var creditsService = require('../service/credits-ser').CreditsService;

// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addSortie',function(req,res,next){
    
       console.log(" ADD SORTIE IS CALLED") ;
       console.log(" THE SORTIE TO ADDED IS :",req.body);
       console.log(req.headers.authorization.slice(  req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var decoded = jwt.decode(req.headers.authorization.slice(  req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = sorties.addSortie(req.body,decoded.userData.code,function(err,sortieAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_SORTIE',
                error_content: err
            });
         }else{
       
        return res.json(sortieAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateSortie',function(req,res,next){
    
       console.log(" UPDATE SORTIE IS CALLED") ;
       console.log(" THE SORTIE TO UPDATE IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = sorties.updateSortie(req.body,decoded.userData.code, function(err,sortieUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_SORTIE',
                error_content: err
            });
         }else{
       
        return res.json(sortieUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getSorties',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    sorties.getListSorties(decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,function(err,listSorties){
        
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_SORTIES',
                 error_content: err
             });
          }else{
        
         return res.json(listSorties);
          }
      });

  });
  router.get('/getSortie/:codeSortie',function(req,res,next){
    console.log("GET SORTIE IS CALLED");
    console.log(req.params['codeSortie']);
    console.log(req.headers.authorization.slice(  req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice(  req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    sorties.getSortie(req.params['codeSortie'],decoded.userData.code, function(err,sortie){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_SORTIE',
               error_content: err
           });
        }else{
      
       return res.json(sortie);
        }
    });
})

  router.delete('/deleteSortie/:codeSortie',function(req,res,next){
    
       console.log(" DELETE SORTIE IS CALLED") ;
       console.log(" THE SORTIE TO DELETE IS :",req.params['codeSortie']);
       
    var decoded = jwt.decode(req.headers.authorization.slice(   req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = sorties.deleteSortie(req.params['codeSortie'],decoded.userData.code, function(err,codeSortie){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_SORTIE',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeSortie']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });



 router.get('/getTotalSorties',function(req,res,next){
 
    var decoded = jwt.decode(req.headers.authorization.slice( req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    sorties.getTotalSorties(decoded.userData.code,function(err,totalSorties){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_SORTIES',
                 error_content: err
             });
          }else{
        
         return res.json({'totalSorties':totalSorties});
          }
      });

  });
 /* router.get('/getTotalPayementsCreditsByCodeCredit/:codeCredit',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
        creditsService.getTotalPayementsCreditsByCodeCredit(req.params['codeCredit'],
        decoded.userData.code,function(err,totalPayCredits){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_PAYEMENTS_CREDITS',
                 error_content: err
             });
          }else{
        
         return res.json({'totalPayementsCredits':totalPayCredits});
          }
      });

  });*/
  module.exports.routerSorties = router;