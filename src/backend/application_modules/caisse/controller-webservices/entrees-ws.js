
var express = require('express');
var jwt = require('jsonwebtoken');

var router = express.Router();
//var pageManager = require('../persistance_pages/PageManger');
var entrees = require('../model/entrees').Entrees;
// var creditsService = require('../service/credits-ser').CreditsService;

// var commonManager = require('../application_modules/common/main').CommonManager;

router.post('/addEntree',function(req,res,next){
    
       console.log(" ADD ENTREE IS CALLED") ;
       console.log(" THE ENTREE TO ADDED IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = entrees.addEntree(req.body,decoded.userData.code,function(err,entreeAdded){
        if(err){
            return  res.json({
                error_message : 'ERROR_ADD_ENTREE',
                error_content: err
            });
         }else{
       
        return res.json(entreeAdded);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.put('/updateEntree',function(req,res,next){
    
       console.log(" UPDATE ENTREE IS CALLED") ;
       console.log(" THE ENTREE TO UPDATE IS :",req.body);
       console.log(" ADD ENTREE IS CALLED") ;
       console.log(" THE ENTREE TO ADDED IS :",req.body);
       console.log(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var decoded = jwt.decode(req.headers.authorization.slice( 
           req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
           
       var result = entrees.updateEntree(req.body,decoded.userData.code, function(err,entreeUpdated){
        if(err){
            return  res.json({
                error_message : 'ERROR_UPDATE_ENTREE',
                error_content: err
            });
         }else{
       
        return res.json(entreeUpdated);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });
router.get('/getEntrees',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));

    entrees.getListEntrees(decoded.userData.code, req.query.pageNumber, req.query.nbElementsPerPage ,function(err,listEntrees){
        
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_LIST_ENTREES',
                 error_content: err
             });
          }else{
        
         return res.json(listEntrees);
          }
      });

  });
  router.get('/getEntree/:codeEntree',function(req,res,next){
    console.log("GET ENTREE IS CALLED");
    console.log(req.params['codeEntree']);
    console.log(req.headers.authorization.slice(  req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    var decoded = jwt.decode(req.headers.authorization.slice(  req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
    entrees.getEntree(req.params['codeEntree'],decoded.userData.code, function(err,entree){
        if(err){
           return  res.json({
               error_message : 'ERROR_GET_ENTREE',
               error_content: err
           });
        }else{
      
       return res.json(entree);
        }
    });
})

  router.delete('/deleteEntree/:codeEntree',function(req,res,next){
    
       console.log(" DELETE ENTREE IS CALLED") ;
       console.log(" THE ENTREE TO DELETE IS :",req.params['codeEntree']);
       
    var decoded = jwt.decode(req.headers.authorization.slice(   req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
       var result = entrees.deleteEntree(req.params['codeEntree'],decoded.userData.code, function(err,codeEntree){
        if(err){
            return  res.json({
                error_message : 'ERROR_DELETE_ENTREE',
                error_content: err
            });
         }else{
       
        return res.json(req.params['codeEntree']);
       }
    }
    );
      // console.log("THE RESULT OF ADDING THE CLIENT IS ",result);
     
   });



 router.get('/getTotalEntrees',function(req,res,next){
 
    var decoded = jwt.decode(req.headers.authorization.slice( req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
    entrees.getTotalEntrees(decoded.userData.code,function(err,totalEntrees){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_ENTREES',
                 error_content: err
             });
          }else{
        
         return res.json({'totalEntrees':totalEntrees});
          }
      });

  });
 /* router.get('/getTotalPayementsCreditsByCodeCredit/:codeCredit',function(req,res,next){

    var decoded = jwt.decode(req.headers.authorization.slice( 
        req.headers.authorization.indexOf( ' ')+1, req.headers.authorization.length));
 
        creditsService.getTotalPayementsCreditsByCodeCredit(req.params['codeCredit'],
        decoded.userData.code,function(err,totalPayCredits){
          if(err){
             return  res.json({
                 error_message : 'ERROR_GET_TOTAL_PAYEMENTS_CREDITS',
                 error_content: err
             });
          }else{
        
         return res.json({'totalPayementsCredits':totalPayCredits});
          }
      });

  });*/
  module.exports.routerEntrees = router;