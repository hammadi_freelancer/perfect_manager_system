

var server = 'http://node7.im.cx';
//var server = 'http://localhost:3000';
var nbre_sections;
var menu_user = true;
var sections = new Array();
var blocs = new Array();
var blocsNote = new Array();
var socket = io.connect(server);

 $(document).ready(function(){  
     
  $("#curseurHaut").click(function(){
  $("#menu_top").toggle(function(){
    $('html, body').animate({
      scrollTop: '0px'},1500);});});
      
    $("#curseurBas").click(function(){
     $("#menu_bot").toggle(function(){
        $('html, body').animate({
        scrollTop: $(document).height()},1500);});});
        
        
        
        $( ".section:has(table)" ).hover(function() {
  //$( this ).fadeOut( 100 );
  //$( this ).fadeIn( 500 );
  $(this).css('background-color','white');
  $(this).css('cursor','default');
});
      $( ".section:has(textarea)" ).hover(function() {
  //$( this ).fadeOut( 100 );
  //$( this ).fadeIn( 500 );
  $(this).css('background-color','white');
  $(this).css('cursor','default');
  
  
});
           
 });
function addSection()
{
     $(".option").remove();
 var CheminComplet = document.location.href;
 var CheminRepertoire  = CheminComplet.substring( 0 ,CheminComplet.lastIndexOf( "/" ) );
 var id     = CheminComplet.substring(CheminComplet.lastIndexOf( "/" )+1 );
 socket.emit("addNewSection",id);
 //location.reload();
 //socket.on('Sections',function(sections){
   // location.reload();
       
}
     /*nbre_sections = sections.length;
         setContainerHeight(sections.length);
         var html = "";
          for(var i =0 ; i< sections.length; i++)
          {
               var content = sections[i].content;
               if(content ==="default")
               {
                   content = '<img src="images/logo.JPG" style="width :100px; height:40px"/>';
               }
              html += "<section class ='section' options='false' style ='height:40px;text-align:center ;border:1px solid; margin:3px;' onclick='insertOptions(" +i+");'>" +content + "</section>";
              
     }
          $('section').remove();
          $('#container').append(html);
          
        });*/


function addNewPage(current_volume)
{
  var CheminComplet = document.location.href;
  var CheminRepertoire  = CheminComplet.substring( 0 ,CheminComplet.lastIndexOf( "/" ) );
  var id     = CheminComplet.substring(CheminComplet.lastIndexOf( "/" )+1 )
  socket.emit("addNewPage",current_volume,id);
  socket.on("URL_NEW_PAGE",function(url){
            window.location.href= url;
            
         });
 }
  function modifyTitleTop()
  {
       $(".option").remove();
    var val =  $("input[name='titre_haut']").val();
    $("input[name='titre_bas']").val(val);
    var CheminComplet = document.location.href;
    var CheminRepertoire  = CheminComplet.substring( 0 ,CheminComplet.lastIndexOf( "/" ) );
    var id     = CheminComplet.substring(CheminComplet.lastIndexOf( "/" )+1 )
    socket.emit("modifyTitle",val,id);
  }
  function modifyTitleBot()
  { $(".option").remove();
     var val =  $("input[name='titre_bas']").val();
     $("input[name='titre_haut']").val(val);
     var CheminComplet = document.location.href;
     var CheminRepertoire  = CheminComplet.substring( 0 ,CheminComplet.lastIndexOf( "/" ) );
     var id     = CheminComplet.substring(CheminComplet.lastIndexOf( "/" )+1 );
     socket.emit("modifyTitle",val,id);
  }
  function movePreviousPage(current_volume)
  {
     var CheminComplet = document.location.href;
     var CheminRepertoire  = CheminComplet.substring( 0 ,CheminComplet.lastIndexOf( "/" ) );
     var id     = CheminComplet.substring(CheminComplet.lastIndexOf( "/" )+1 );
     socket.emit("getPreviousUrl",current_volume,id);
     socket.on("URL_PREVIOUS_PAGE",function(url){
       window.location.href= url; 
         });
            
   }
   function moveNextPage(current_volume)
   {
     var CheminComplet = document.location.href;
     var CheminRepertoire  = CheminComplet.substring( 0 ,CheminComplet.lastIndexOf( "/" ) );
     var id     = CheminComplet.substring(CheminComplet.lastIndexOf( "/" )+1 );
     socket.emit("getNextUrl",current_volume,id);
     socket.on("URL_NEXT_PAGE",function(url){
            window.location.href= url;
            });
   }
   function addPageWithNewVolume()
   {
     socket.emit("addPageWithNewVolume");
     socket.on("URL_NEW_PAGE",function(url){
            window.location.href= url;  
            });
   }
   function setContainerHeight(nbre)
   {
       
     var h = 650;
     if(nbre>4)
     {
      h=nbre*133;
      h+=40;
      }
      $("#container").height(h);
      //alert(nbre);
      
   }
   
 function insertOptions(enventData)
 {
      //var nb = nbre_sections- $(".option").length;
    //setContainerHeight(nb);
     $(".option").remove();
         
          var html = "";
          //if($( ".section:eq("+enventData.data +")").attr("options")==='false'){
           
           html += "<section class ='option'  style ='height:130px;text-align:center;border:1px solid; margin:3px;'><img src='images/list.png' style='width :130px; height:130px'/></section>";
           html += "<section class='option' style ='height:130px;text-align:center ;border:1px solid; margin:3px;' ><img src='images/note.png' style='width :100px; height:130px'/></section>";

          $( ".section:eq("+enventData.data +")").after(html);
          for(var j= 0; j<2;j++){
              var params = new Array();
                params.push(enventData.data);
                params.push(j);
          $(".option:eq("+j+")").bind("click",params,updateTypeSection);
          
          }
          $( ".section:eq("+enventData.data +")").attr("options","true"); 
          var nb = nbre_sections +3;
          //alert(nbre_sections);
            setContainerHeight(nb);
      //}
    
 }
 function initBlocList(enventData)
 {
      $(".option").remove();
     if($('.section:eq('+enventData.data+')').has('table').length ===0){
     var id_bloc = $('.section:eq('+enventData.data+')').data('bloc');
     console.log("init nbre sec:" + enventData.data);
         console.log("initBlocList:"+id_bloc);

   
    
     var html ="<table data-bloc='"+ id_bloc+"' style='margin-left: auto; margin-right: auto;border:1px solid'><tr><td style='text-align:center;'><input type='text' name='item' size='40' /></td></tr></table>";
      $('.section:eq('+enventData.data+')').html(html);
      //$('.section:eq('+enventData.data+')').css('overflow','auto'); 
      $('.section:eq('+enventData.data+')').css('overflow-y','scroll');
       $("table[data-bloc='"+id_bloc+"'] input:last").bind("change",id_bloc,saveBlocList); 
        $("table[data-bloc='"+id_bloc+"'] input:last").bind("keyup",id_bloc,addNewRow);       
        $("table[data-bloc='"+id_bloc+"'] input:last").bind("focusout",id_bloc,checkRows);
        $( ".section:has(table)" ).hover(function() {
  //$( this ).fadeOut( 100 );
  //$( this ).fadeIn( 500 );
  $(this).css('background-color','white');
  $(this).css('cursor','default');
});
    }
      
 }
 function initBlocNote(event)
 {
      $(".option").remove();
      if($('.section:eq('+event.data+')').has('textarea').length ===0){
     var id_bloc = $('.section:eq('+event.data+')').data('bloc');
     var html ="<textarea data-bloc='"+ id_bloc+"' rows='15' cols='60' style='border:1px solid' name='note' ></textarea>";
      $('.section:eq('+event.data+')').html(html);
            $('.section:eq('+event.data+')').css('overflow','auto');

      //$('.section:eq('+event.data+')').css('overflow','auto');
       //$("table[data-bloc='"+id_bloc+"'] input:last").bind("change",id_bloc,saveBlocList); 
        $("textarea[data-bloc='"+id_bloc+"']").bind("keyup",id_bloc,saveBlocNote);   
           $( ".section:has(textarea)" ).hover(function() {
  //$( this ).fadeOut( 100 );
  //$( this ).fadeIn( 500 );
  $(this).css('background-color','white');
  $(this).css('cursor','default');
});
        
       // $("table[data-bloc='"+id_bloc+"'] input:last").bind("focusout",id_bloc,checkRows);
    }
 }
 function updateTypeSection(event)
 {
     $(".option").remove();
     
     $('.section:eq('+event.data[0]+')').unbind("click",insertOptions);

     var html = "";
    // $( ".section:eq("+event.data[0] +")").css('height','130px');
   if(event.data[1]===0)
   {
    html += "<img src='images/list.png' style='width :130px; height:130px'/>";
      $( ".section:eq("+event.data[0] +")").html(html);
      console.log("updateTypeSection:nbre section:"+ event.data[0]);
     
      $(".section:eq("+event.data[0]+")").bind("click",event.data[0],initBlocList);
      socket.emit("updateTypeSection",sections[event.data[0]],'List');

   }else if(event.data[1]===1){
         html += "<img src='images/note.png' style='width :100px; height:130px'/>";
          $( ".section:eq("+event.data[0] +")").html(html);
      $(".section:eq("+event.data[0]+")").bind("click",event.data[0],initBlocNote);
      socket.emit("updateTypeSection",sections[event.data[0]],'Note')
  
   }
   //var nb = nbre_sections-2;
      setContainerHeight(nbre_sections);
  
 }


function checkRows(enventData)
{
     $(".option").remove();
     var inputs = $("table[data-bloc='"+enventData.data+"'] input");
 for(var i=0; i<inputs.length;i++)
 {
     if(inputs[i].value ==="" && i !==(inputs.length-1))
     {
        
         $("table[data-bloc='"+enventData.data+"'] tr:eq("+i+")").remove();
     }
 }
}
function addNewRow(enventData)
{
    console.log("addNewRow:"+enventData.data);
    $(".option").remove();
    var inputs = $("table[data-bloc='"+enventData.data+"'] input");
    
    if (inputs[inputs.length-1].value!=="" )
 {
     var html="";
          html += "<tr><td  style='background-color: #F5F5F5;'><input type='text' name='item' value='' size='40' style='border:0px' /> </td></tr>";  
        $("table[data-bloc='"+enventData.data+"'] tr:last").after(html);
        $("table[data-bloc='"+enventData.data+"'] input:last").bind("change",enventData.data,saveBlocList); 
        $("table[data-bloc='"+enventData.data+"'] input:last").bind("keyup",enventData.data,addNewRow);       
        $("table[data-bloc='"+enventData.data+"'] input:last").bind("focusout",enventData.data,checkRows);   
        //$(".section :eq(0)").scrollTo("table[data-bloc='"+enventData.data+"'] tr:last");
        
        if(inputs.length >4)
        {
            var nb = inputs.length-5;
            nb = 20*nb;
            $("table[data-bloc='"+enventData.data+"']").parent('.section').animate({
        scrollTop: $('.section:eq(0)').height()+nb},2000);
       }
        

 }
}
function saveBlocNote(eventData)
{
   $(".option").remove();
      var textArea = $("textarea[data-bloc='"+eventData.data+"']");
      
      var cont = textArea[0].value.replace('  ',' ');
     cont =cont.replace(/\n|\r|(\n\r)/g,'  ');
      var blocData = {id:eventData.data,type:'Note',content:cont};
      socket.emit("saveBloc",blocData);

}
 
function saveBlocList(enventData)
{
 console.log("saveBlocList:"+enventData.data);
  $(".option").remove();
 var items ="";
 var inputs = $("table[data-bloc='"+enventData.data+"'] input");
 for(var i=0; i<inputs.length;i++)
 {
     if(inputs[i].value ==="" && i !==(inputs.length-1))
     {
        
         $("table[data-bloc='"+enventData.data+"'] tr:eq("+i+")").remove();
     }
     if(inputs[i].value !=="")
     {
         if(items !== "")
         {
             items+='  ';
         }
         var item = inputs[i].value.replace('  ',' ');
         items+= item;
     }
 }
 if (inputs[inputs.length-1].value!=="" )
 {
     var html="";
          html += "<tr><td  style='background-color: #F5F5F5;'><input type='text' name='item' value='' style='width: 100%;border:0px' /> </td></tr>";  
        $("table[data-bloc='"+enventData.data+"'] tr:last").after(html);
        $("table[data-bloc='"+enventData.data+"'] input:last").bind("change",enventData.data,saveBlocList);
        $("table[data-bloc='"+enventData.data+"'] input:last").bind("keyup",enventData.data,addNewRow); 
        $("table[data-bloc='"+enventData.data+"'] input:last").bind("focusout",enventData.data,checkRows);       


 }
 
   var blocData = {id:enventData.data,type:'List',content:items};
   socket.emit("saveBloc",blocData);
 }
 socket.on('attachBloc',function(id_section,id_bloc){
    for(var i=0;i<sections.length;i++)
    {
        if(sections[i] === id_section)
        {
            $('.section:eq('+i+')').data('bloc',id_bloc);
            break;
        }
    }
     
 });
 socket.on('addNewSection',function(id_section){
     sections.push(id_section);
     var pos = nbre_sections-1;;
      var  html = "<section class ='section' data-type='default' options='false' style ='height:130px;text-align:center;border:1px solid; margin:3px;' ><img src='images/logo.JPG' style='width :100px; height:90px;'/></section>";
       $(".section:eq("+pos+")").after(html);
       pos++;
        $(".section:eq("+pos+")").bind("click",pos,insertOptions);
        nbre_sections++;
       setContainerHeight(nbre_sections+1);
       //$("#container").scrollTo($(".section:eq("+pos+")"));
       if(nbre_sections>4){
       $('html, body').animate({
        scrollTop: $(document).height()},2000);
       }
             
 });
   /* socket.on('DeleteBloc',function(id_bloc,type){
        $("table[data-bloc='"+id_bloc+"']").remove();
        var leng = $(".bloc").length;
        for(var i =0;i<leng;i++)
        {
            //$(".bloc:eq("+i+")").find("td")[1].text(i+1);
            $(".bloc:eq("+i+") td:eq(1)").text(i+1);
        }
    });
    socket.on('AddNewBloc',function(id_bloc,type){
        var nbreBlocs = $(".bloc").length +1;
         if(type ==='List'){
        var html ="<table class='bloc' data-bloc='"+id_bloc+"' style='width: 50%; margin: 10px;border:solid 1px'>";
        html += "<tr><td style='background-color: #FFA07A;width:50%'>Block</td><td style='background-color: #FFE4C4;width:50%'>"+nbreBlocs+"</td></tr>";
          html += "<tr><td colspan='2' style='background-color: #F5F5F5;'><input type='text' name='item' value='' style='width: 100%;border:0px' /> </td></tr></table>";  
        $(".bloc:last").after(html);
         $("table[data-bloc='"+id_bloc+"'] input:last").bind("change",{id_bloc:id_bloc},saveBloc); 
          $("table[data-bloc='"+id_bloc+"'] input:last").bind("keyup",{id_bloc:id_bloc},addNewRow); 
         $("table[data-bloc='"+id_bloc+"'] input:last").bind("focusout",{id_bloc:id_bloc},checkRows); 
     }
     else if(type ==='Note')
     {
         var html ="<table class='bloc' data-bloc='"+id_bloc+"' style='width: 80%; margin: 10px;border:solid 1px'>";
        html += "<tr><td style='background-color: #FFA07A;width:50%'>Block</td><td style='background-color: #FFE4C4;width:50%'>"+nbreBlocs+"</td></tr>";
          html += "<tr><td colspan='2' style='background-color: #F5F5F5;'><textarea  rows='5' name='note' value='' style='width: 100%;border:0px' ></textarea> </td></tr></table>";  
          $(".bloc:last").after(html);
         $("table[data-bloc='"+id_bloc+"'] textarea:last").bind("keyup",{id_bloc:id_bloc},saveBloc); 

        }
    });*/
/*jQuery.fn.synchronizeScroll = function() {
    
             var elements = this;
            if (elements.length <= 1) return;
   
              elements.scroll(
             function() {
                   var left = $(this).scrollLeft();
                   var top = $(this).scrollTop();
                  elements.each(
                 function() {
                      if ($(this).scrollLeft() != left) $(this).scrollLeft(left);
                     if ($(this).scrollTop() != top) $(this).scrollTop(top);
                 }
                 );
             });
          };*/

/*$.fn.scrollTo = function( target, options, callback ){
  if(typeof options == 'function' && arguments.length == 2){ callback = options; options = target; }
  var settings = $.extend({
    scrollTarget  : target,
    offsetTop     : 50,
    duration      : 500,
    easing        : 'swing'
  }, options);
  return this.each(function(){
    var scrollPane = $(this);
    var scrollTarget = (typeof settings.scrollTarget == "number") ? settings.scrollTarget : $(settings.scrollTarget);
    var scrollY = (typeof scrollTarget == "number") ? scrollTarget : scrollTarget.offset().top + scrollPane.scrollTop() - parseInt(settings.offsetTop);
    scrollPane.animate({scrollTop : scrollY }, parseInt(settings.duration), settings.easing, function(){
      if (typeof callback == 'function') { callback.call(this); }
    });
  });
};*/