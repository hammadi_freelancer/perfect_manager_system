var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
// const expressJwt = require('express-jwt');
// const config = require('config.json');
let jwt = require('jsonwebtoken');
let config = require('./middleware/config');
let middleware = require('./middleware/middleware');

const mongoClient = require('mongodb').MongoClient;


// var routes = require('./routes/index');
var utilisateurs = require('./application_modules/utilisateurs/model/utilisateurs').Utilisateurs;

var routesFinanceCredits = require('./application_modules/finance/controller-webservices/credits-ws').routerCredits;
var routesFinancePayementsCredits = require('./application_modules/finance/controller-webservices/payements-credits-ws').routerPayementsCredits;
var routesFinanceAjournementsCredits = require('./application_modules/finance/controller-webservices/ajournements-credits-ws').routerAjournementsCredits;

var routesFinancePayementsCreditsAchats = require('./application_modules/finance/controller-webservices/payements-credits-achats-ws').
routerPayementsCreditsAchats;

var routesFinancePayementsFacturesVentes = require('./application_modules/finance/controller-webservices/payements-factures-ventes-ws').
routerPayementsFacturesVentes;

var routesFinanceAjournementsCreditsAchats = require('./application_modules/finance/controller-webservices/ajournements-credits-achats-ws').routerAjournementsCreditsAchats;

var routesFinanceTranchesCredits = require('./application_modules/finance/controller-webservices/tranches-credits-ws').routerTranchesCredits;
var routesFinanceTranchesCreditsAchats = require('./application_modules/finance/controller-webservices/tranches-credits-achats-ws').routerTranchesCreditsAchats;


var routesFinanceLignesCredits = require('./application_modules/finance/controller-webservices/lignes-credits-ws').routerLignesCredits;
var routesFinanceLignesCreditsAchats = require('./application_modules/finance/controller-webservices/lignes-credits-achats-ws').
routerLignesCreditsAchats;

var routesFinanceDocumentsCredits = require('./application_modules/finance/controller-webservices/documents-credits-ws').routerDocumentsCredits;
var routesFinanceDocumentsCreditsAchats = require('./application_modules/finance/controller-webservices/documents-credits-achats-ws').routerDocumentsCreditsAchats;

var routesVentesClients = require('./application_modules/ventes/controller-webservices/clients-ws').clientsRouter;
var routesCommonTaxes = require('./application_modules/common/controller-webservices/taxes-ws').routerTaxes;
var routesDocumentsTypes = require('./application_modules/common/controller-webservices/documents-types-ws').documentsTypesRouter;
var routesDocuments = require('./application_modules/common/controller-webservices/documents-ws').documentsRouter;
var routesFilesData = require('./application_modules/shared-components/controller-webservices/files-data-ws').filesDataRouter;
var routesAchatsFournisseurs = require('./application_modules/achats/controller-webservices/fournisseurs-ws').routerFournisseurs;
var routesStocksArticles = require('./application_modules/stocks/controller-webservices/articles-ws').articlesRouter;
var routesStocksMagasins = require('./application_modules/stocks/controller-webservices/magasins-ws').magasinsRouter;
var routesStocksUnMesures = require('./application_modules/stocks/controller-webservices/un-mesures-ws').unMesuresRouter;
var routesStocksLots = require('./application_modules/stocks/controller-webservices/lots-ws').lotsRouter;
var routesStocksSchemasConfiguration = require('./application_modules/stocks/controller-webservices/schemas-configuration-ws').schemasConfigurationRouter;



var routesStocksConfigurations = 
require('./application_modules/stocks/controller-webservices/configurations-ws').ConfigurationsRouter;




var routesStocksClassesArticles = require('./application_modules/stocks/controller-webservices/classes-articles-ws').classesArticlesRouter;

var routesAchatsCredits = require('./application_modules/achats/controller-webservices/credits-achats-ws').routerCreditsAchats;

var routesFacturesVentes = require('./application_modules/ventes/controller-webservices/factures-ventes-ws').routerFacturesVentes;


var routesFacturesAchats = require('./application_modules/achats/controller-webservices/factures-achats-ws').routerFacturesAchats;

var routesDocumentsFacturesAchats = require('./application_modules/achats/controller-webservices/documents-factures-achats-ws')
.routerDocumentsFacturesAchats;
var routesAjournementsFacturesAchats = require('./application_modules/achats/controller-webservices/ajournements-factures-achats-ws')
.routerAjournementsFacturesAchats;

var routesPayementsFacturesAchats = require('./application_modules/finance/controller-webservices/payements-factures-achats-ws')
.routerPayementsFacturesAchats;



var routesLignesFacturesAchats = require('./application_modules/achats/controller-webservices/lignes-factures-achats-ws').routerLignesFactureAchats;

var routesRelevesAchats = require('./application_modules/achats/controller-webservices/releves-achats-ws').routerRelevesAchats;

var routesAjournementsRelevesAchats = require('./application_modules/achats/controller-webservices/ajournements-releves-achats-ws')
.routerAjournementsRelevesAchats;
var routesPayementsRelevesAchats = require('./application_modules/finance/controller-webservices/payements-releves-achats-ws')
.routerPayementsRelevesAchats;
var routesDocumentsRelevesAchats = require('./application_modules/achats/controller-webservices/documents-releves-achats-ws')
.routerDocumentsRelevesAchats;
var routesDemandesAchats = require('./application_modules/achats/controller-webservices/demandes-achats-ws')
.routerDemandesAchats;


var routesLignesRelevesAchats = require('./application_modules/achats/controller-webservices/lignes-releves-achats-ws')
.routesLignesRelevesAchats;



var routesDocumentsFacturesVentes = require('./application_modules/ventes/controller-webservices/documents-factures-ventes-ws')
.routerDocumentsFacturesVentes;
var routesLignesFacturesVentes = require('./application_modules/ventes/controller-webservices/lignes-factures-ventes-ws').routerLignesFactureVentes;
var routesRelevesVentes = require('./application_modules/ventes/controller-webservices/releves-ventes-ws').routerRelevesVentes;
var routesAjournementsRelevesVentes = require('./application_modules/ventes/controller-webservices/ajournements-releves-ventes-ws')
.routerAjournementsRelevesVentes;
var routesDocumentsRelevesVentes = require('./application_modules/ventes/controller-webservices/documents-releves-ventes-ws')
.routerDocumentsRelevesVentes;
var routesPayementsRelevesVentes = require('./application_modules/finance/controller-webservices/payements-releves-ventes-ws')
.routerPayementsRelevesVentes;


var routesDemandesVentes = require('./application_modules/ventes/controller-webservices/demandes-ventes-ws').routerDemandesVentes;

var routesAjournementsDemandesVentes = require('./application_modules/ventes/controller-webservices/ajournements-demandes-ventes-ws')
.routerAjournementsDemandesVentes;
var routesDocumentsDemandesVentes = require('./application_modules/ventes/controller-webservices/documents-demandes-ventes-ws')
.routerDocumentsDemandesVentes;
var routesPayementsDemandesVentes = require('./application_modules/finance/controller-webservices/payements-demandes-ventes-ws')
.routerPayementsDemandesVentes;



var routesComptesClients = require('./application_modules/finance/controller-webservices/comptes-clients-ws')
.routerComptesClients;

var routesComptesFournisseurs = require('./application_modules/finance/controller-webservices/comptes-fournisseurs-ws')
.routerComptesFournisseurs;

var routesTransactionsComptesFournisseurs = require('./application_modules/finance/controller-webservices/transactions-comptes-fournisseurs-ws')
.routerTransactionsComptesFournisseurs;

var routesTransactionsComptesClients = require('./application_modules/finance/controller-webservices/transactions-comptes-clients-ws')
.routerTransactionsComptesClients;

var routesDocumentsComptesClients = require('./application_modules/finance/controller-webservices/documents-comptes-clients-ws')
.routerDocumentsComptesClients;

var routesDocumentsComptesFournisseurs= require('./application_modules/finance/controller-webservices/documents-comptes-fournisseurs-ws')
.routerDocumentsComptesFournisseurs;


var routesDocumentsClients = require('./application_modules/ventes/controller-webservices/documents-clients-ws')
.routerDocumentsClients;


var routesDocumentsFournisseurs = require('./application_modules/achats/controller-webservices/documents-fournisseurs-ws')
.routerDocumentsFournisseurs;

var routesAjournementsFacturesVentes = require('./application_modules/ventes/controller-webservices/ajournements-factures-ventes-ws')
.routerAjournementsFacturesVentes;
var routesDocumentsVentes = require('./application_modules/ventes/controller-webservices/documents-ventes-ws').routerDocumentsVentes;
var routesDocumentsAchats = require('./application_modules/achats/controller-webservices/documents-achats-ws').routerDocumentsAchats;

var routesFacturesAchats = require('./application_modules/achats/controller-webservices/factures-achats-ws').routerFacturesAchats;
var routesParametresAchats = require('./application_modules/achats/controller-webservices/parametres-achats-ws').routerParametresAchats;
var routesParametresVentes = require('./application_modules/ventes/controller-webservices/parametres-ventes-ws').routerParametresVentes;

var routesEntrees = require('./application_modules/caisse/controller-webservices/entrees-ws').routerEntrees;
var routesSorties = require('./application_modules/caisse/controller-webservices/sorties-ws').routerSorties;
var routesDocsEntrees = require('./application_modules/caisse/controller-webservices/documents-entrees-ws').routerDocumentsEntrees;
var routesDocsSorties = require('./application_modules/caisse/controller-webservices/documents-sorties-ws').routerDocumentsSorties;

var routesUtilisateurs = require('./application_modules/utilisateurs/controller-webservices/utilisateurs-ws').utilisateursRouter;
var routesAccessRights = require('./application_modules/utilisateurs/controller-webservices/access-rights-ws').accessRightsRouter;
var routesGroupes = require('./application_modules/utilisateurs/controller-webservices/groupes-ws').groupesRouter;

var routesOrganisations = require('./application_modules/organisations/controller-webservices/organisations-ws').organisationsRouter;
var routesJournaux= require('./application_modules/journaux/controller-webservices/journaux-ws').routerJournaux;
var routesLignesJournaux= require('./application_modules/journaux/controller-webservices/lignes-journaux-ws').routerLignesJournaux;
var routesDocumentsJournaux= require('./application_modules/journaux/controller-webservices/documents-journaux-ws').routerDocumentsJournaux;
var routesEmployes= require('./application_modules/resources-humaines/controller-webservices/employes-ws').routerEmployes;
var routesDocsEmployes= require('./application_modules/resources-humaines/controller-webservices/documents-employes-ws').routerDocumentsEmployes;
var routesPaiementsEmployes= require('./application_modules/resources-humaines/controller-webservices/paiements-employes-ws').
routerPaiementsEmployes;
var routesDemandesConges= require('./application_modules/resources-humaines/controller-webservices/demandes-conges-ws').
routerDemandesConges;
var routesFichesPaies= require('./application_modules/resources-humaines/controller-webservices/fiches-paies-ws').
routerFichesPaies;


var routesCommonSettings = require('./application_modules/common/controller-webservices/settings-ws').routerSettings;
var routesJournaux= require('./application_modules/journaux/controller-webservices/journaux-ws').routerJournaux;
var routerOperationsCourantes= require('./application_modules/operations-courantes/controller-webservices/operations-courantes-ws').routerOperationsCourantes;
var routesBonsCommandes= require('./application_modules/bons-commandes/controller-webservices/bons-commandes-ws').routerBonsCommandes;
var routesBonsReceptions= require('./application_modules/bons-receptions/controller-webservices/bons-receptions-ws').routerBonsReceptions;
var routesBonsLivraisons= require('./application_modules/bons-livraisons/controller-webservices/bons-livraisons-ws').routerBonsLivraisons;
var routesLivresOres= require('./application_modules/livres-ores/controller-webservices/livres-ores-ws').routerLivresOres;
var routesLignesLivresOres= require('./application_modules/livres-ores/controller-webservices/lignes-livres-ores-ws').routerLignesLivresOres;
var routesDocumentsLivresOres= require('./application_modules/livres-ores/controller-webservices/documents-livres-ores-ws').routerDocumentsLivresOres;

var routesInventaires= require('./application_modules/inventaires/controller-webservices/inventaires-ws').routerInventaires;
var routesImmobiliersInventaires= require('./application_modules/inventaires/controller-webservices/immobiliers-inventaires-ws').routerImmobiliersInventaires;
var routesMaterielsInventaires= require('./application_modules/inventaires/controller-webservices/materiels-inventaires-ws').routerMaterielsInventaires;
var routesStocksInventaires= require('./application_modules/inventaires/controller-webservices/stocks-inventaires-ws').routerStocksInventaires;
var routesDocumentsInventaires= require('./application_modules/inventaires/controller-webservices/documents-inventaires-ws').routerDocumentsInventaires;

var routesProfilesAchats= require('./application_modules/profiles/controller-webservices/profiles-achats-ws').routerProfilesAchats;
var routesProfilesVentes= require('./application_modules/profiles/controller-webservices/profiles-ventes-ws').routerProfilesVentes;

var routerCompagnesPublicitaires= require('./application_modules/marketing/controller-webservices/compagnes-publicitaires-ws').routerCompagnesPublicitaires;
var routesPromotions= require('./application_modules/marketing/controller-webservices/promotions-ws').routerPromotions;
var routesMannequins= require('./application_modules/marketing/controller-webservices/mannequins-ws').routerMannequins;
var routesDocumentsMannequins= require('./application_modules/marketing/controller-webservices/documents-mannequins-ws').routerDocumentsMannequins;
var routesPaiementsMannequins= require('./application_modules/marketing/controller-webservices/paiements-mannequins-ws').routerPaiementsMannequins;
var routesAlbumsMannequins= require('./application_modules/marketing/controller-webservices/albums-mannequins-ws').routerAlbumsMannequins;


var routesReglementsDettes= require('./application_modules/reglements-dettes/controller-webservices/reglements-dettes-ws').
routerReglementsDettes;

var routesReglementsCredits= require('./application_modules/reglements-credits/controller-webservices/reglements-credits-ws').
routerReglementsCredits;

var routesHistoriquesClients= require('./application_modules/historiques-clients/controller-webservices/historiques-clients-ws').
routerHistoriquesClients;

var routesHistoriquesFournisseurs= require('./application_modules/historiques-fournisseurs/controller-webservices/historiques-fournisseurs-ws').
routerHistoriquesFournisseurs;

var routesActivitesProduction= require('./application_modules/production/controller-webservices/activites-production-ws').activitesProductionRouter;
var routesOrdresProduction= require('./application_modules/production/controller-webservices/ordres-production-ws').ordresProductionRouter;
var routesRelevesProduction= require('./application_modules/production/controller-webservices/materiels-production-ws').materielsProductionRouter;
var routesMaterielsProduction= require('./application_modules/production/controller-webservices/releves-production-ws').relevesProductionRouter;
var routesMaterielsProduction= require('./application_modules/production/controller-webservices/documents-releves-production-ws').routerDocumentsRelevesProduction;
var routesDocumentsOrdresProduction= require('./application_modules/production/controller-webservices/documents-ordres-production-ws').routerDocumentsOrdresProduction;
var routesDocumentsRelevesProduction= require('./application_modules/production/controller-webservices/documents-releves-production-ws').routerDocumentsRelevesProduction;
var routesPaiementsRelevesProduction= require('./application_modules/production/controller-webservices/paiements-releves-production-ws').routerPaiementsRelevesProduction;
var routesPaiementsOrdresProduction= require('./application_modules/production/controller-webservices/paiements-ordres-production-ws').routerPaiementsOrdresProduction;




var app = express();
const mongo_uri ='mongodb://localhost:27017';
const dbName = 'gestion';
mongoClient.connect(mongo_uri, { useNewUrlParser: true })
.then(client => {
  const db = client.db(dbName);
 // const collection = db.collection('my-collection');
  app.listen(8082,() =>
   console.info('REST API running on port 8082')) ;
   app.locals.dataBase = db;
   
}).catch(error => 
  console.error(error));


var cors = require('cors');

app.use(cors());
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
//app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.set('view engine', 'ejs');
//app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'node_modules')));

app.post('/login', login);
//app.use('/', middleware.checkToken );
//app.use('/compopage', express.static('public'));
app.use('/utilisateurs',middleware.checkToken,routesUtilisateurs);
//app.use('/groupes',middleware.checkToken,routesGroupes);
app.use('/groupes',middleware.checkToken,routesGroupes);
app.use('/accessRights',middleware.checkToken,routesAccessRights);
app.use('/organisations',middleware.checkToken,routesOrganisations);
app.use('/finance/credits', middleware.checkToken, routesFinanceCredits);
app.use('/finance/payementsCredits',middleware.checkToken,  routesFinancePayementsCredits);
app.use('/finance/ajournementsCredits',middleware.checkToken,  routesFinanceAjournementsCredits);
app.use('/finance/ajournementsCreditsAchats',middleware.checkToken,  routesFinanceAjournementsCreditsAchats);
app.use('/finance/documentsCredits',middleware.checkToken,  routesFinanceDocumentsCredits);
app.use('/finance/documentsCreditsAchats',middleware.checkToken,  routesFinanceDocumentsCreditsAchats);

app.use('/finance/tranchesCredits',middleware.checkToken,  routesFinanceTranchesCredits);
app.use('/finance/tranchesCreditsAchats',middleware.checkToken,  routesFinanceTranchesCreditsAchats);
app.use('/finance/lignesCredits',middleware.checkToken,  routesFinanceLignesCredits);
app.use('/finance/lignesCreditsAchats',middleware.checkToken,  routesFinanceLignesCreditsAchats);
app.use('/finance/payementsCreditsAchats',middleware.checkToken,  routesFinancePayementsCreditsAchats);
app.use('/finance/payementsFacturesVentes',middleware.checkToken,  routesFinancePayementsFacturesVentes);
app.use('/finance/payementsFacturesAchats',middleware.checkToken,  routesPayementsFacturesAchats);
app.use('/finance/payementsRelevesAchats',middleware.checkToken,  routesPayementsRelevesAchats);
app.use('/finance/payementsRelevesVentes',middleware.checkToken,  routesPayementsRelevesVentes);
app.use('/finance/credits', middleware.checkToken, routesFinanceCredits);
app.use('/finance/comptesClients', middleware.checkToken,routesComptesClients);
app.use('/finance/comptesFournisseurs', middleware.checkToken,routesComptesFournisseurs);
app.use('/finance/transactionsComptesFournisseurs', middleware.checkToken,routesTransactionsComptesFournisseurs);
app.use('/finance/transactionsComptesClients', middleware.checkToken,routesTransactionsComptesClients);
app.use('/finance/documentsComptesClients', middleware.checkToken,routesDocumentsComptesClients);
app.use('/finance/documentsComptesFournisseurs', middleware.checkToken,routesDocumentsComptesFournisseurs);

app.use('/ventes/clients', middleware.checkToken, routesVentesClients);
app.use('/ventes/facturesVentes', middleware.checkToken ,routesFacturesVentes);
app.use('/ventes/relevesVentes', middleware.checkToken ,routesRelevesVentes);
app.use('/ventes/documentsRelevesVentes', middleware.checkToken ,routesDocumentsRelevesVentes);
app.use('/ventes/ajournementsRelevesVentes', middleware.checkToken ,routesAjournementsRelevesVentes);
app.use('/ventes/relevesVentes', middleware.checkToken ,routesRelevesVentes);


app.use('/ventes/demandesVentes', middleware.checkToken ,routesDemandesVentes);
app.use('/ventes/documentsDemandesVentes', middleware.checkToken ,routesDocumentsDemandesVentes);
app.use('/ventes/ajournementsDemandesVentes', middleware.checkToken ,routesAjournementsDemandesVentes);
app.use('/ventes/payementsDemandesVentes', middleware.checkToken ,routesPayementsDemandesVentes);
app.use('/ventes/lignesFacturesVentes', middleware.checkToken ,routesLignesFacturesVentes);
app.use('/ventes/documentsVentes', middleware.checkToken ,routesDocumentsVentes);
app.use('/ventes/documentsFacturesVentes', middleware.checkToken ,routesDocumentsFacturesVentes);
app.use('/ventes/documentsClients', middleware.checkToken ,routesDocumentsClients);
app.use('/ventes/ajournementsFacturesVentes', middleware.checkToken ,routesAjournementsFacturesVentes);
app.use('/achats/documentsAchats', middleware.checkToken ,routesDocumentsAchats);

app.use('/achats/fournisseurs',middleware.checkToken ,routesAchatsFournisseurs);
app.use('/achats/documentsFournisseurs',middleware.checkToken ,routesDocumentsFournisseurs);

app.use('/achats/credits-achats',middleware.checkToken, routesAchatsCredits);

app.use('/achats/facturesAchats',middleware.checkToken ,routesFacturesAchats);
app.use('/achats/documentsFacturesAchats',middleware.checkToken ,routesDocumentsFacturesAchats);
app.use('/achats/ajournementsFacturesAchats',middleware.checkToken ,routesAjournementsFacturesAchats);
app.use('/achats/lignesFacturesAchats',middleware.checkToken ,routesLignesFacturesAchats);

app.use('/achats/relevesAchats',middleware.checkToken ,routesRelevesAchats);
app.use('/achats/demandesAchats',middleware.checkToken ,routesDemandesAchats);

app.use('/achats/documentsRelevesAchats',middleware.checkToken ,routesDocumentsRelevesAchats);
app.use('/achats/ajournementsRelevesAchats',middleware.checkToken ,routesAjournementsRelevesAchats);
app.use('/achats/lignesRelevesAchats',middleware.checkToken ,routesLignesRelevesAchats);

app.use('/achats/parametresAchats',middleware.checkToken ,routesParametresAchats);
app.use('/ventes/parametresVentes',middleware.checkToken ,routesParametresVentes);
app.use('/stocks/articles',middleware.checkToken, routesStocksArticles);
app.use('/stocks/magasins',middleware.checkToken ,routesStocksMagasins);
app.use('/stocks/unMesures', middleware.checkToken , routesStocksUnMesures);
app.use('/stocks/lots', middleware.checkToken , routesStocksLots);
app.use('/stocks/classesArticles', middleware.checkToken , routesStocksClassesArticles);
app.use('/stocks/schemasConfiguration',middleware.checkToken, routesStocksSchemasConfiguration);
app.use('/stocks/configurations',middleware.checkToken, routesStocksConfigurations);

app.use('/journaux/documents',middleware.checkToken,routesDocumentsJournaux);
app.use('/journaux/lignes',middleware.checkToken,routesLignesJournaux);
app.use('/journaux',middleware.checkToken,routesJournaux);

app.use('/profilesAchats', middleware.checkToken , routesProfilesAchats);
app.use('/profilesVentes', middleware.checkToken, routesProfilesVentes);

app.use('/livresOres/documents',middleware.checkToken,routesDocumentsLivresOres);
app.use('/livresOres/lignes',middleware.checkToken,routesLignesLivresOres);
app.use('/livresOres',middleware.checkToken,routesLivresOres);
app.use('/inventaires/documents',middleware.checkToken,routesDocumentsInventaires);
app.use('/inventaires/immobiliersInventaires',middleware.checkToken,routesImmobiliersInventaires);
app.use('/inventaires/materielsInventaires',middleware.checkToken,routesMaterielsInventaires);
app.use('/inventaires/stocksInventaires',middleware.checkToken,routesStocksInventaires);
app.use('/inventaires',middleware.checkToken,routesInventaires);
app.use('/operationsCourantes',middleware.checkToken,routerOperationsCourantes);
app.use('/bonsLivraisons',middleware.checkToken,routesBonsLivraisons);
app.use('/bonsReceptions',middleware.checkToken,routesBonsReceptions);
app.use('/bonsCommandes',middleware.checkToken,routesBonsCommandes);
app.use('/caisse/sorties/documentsSorties',middleware.checkToken ,routesDocsSorties);
app.use('/caisse/entrees/documentsEntrees',middleware.checkToken ,routesDocsEntrees);
app.use('/caisse/sorties',  middleware.checkToken ,routesSorties);
app.use('/caisse/entrees',middleware.checkToken ,routesEntrees);
app.use('/common/taxes',middleware.checkToken ,routesCommonTaxes);
app.use('/common/settings',middleware.checkToken ,routesCommonSettings);
app.use('/documentsTypes',middleware.checkToken ,routesDocumentsTypes);
app.use('/documents',middleware.checkToken ,routesDocuments);
app.use('/shared-components/storage',middleware.checkToken, routesFilesData);
app.use('/rh/employes',middleware.checkToken,routesEmployes);
app.use('/rh/documentsEmployes',middleware.checkToken,routesDocsEmployes);
app.use('/rh/paiementsEmployes',middleware.checkToken,routesPaiementsEmployes);
app.use('/rh/demandesConges',middleware.checkToken,routesDemandesConges);
app.use('/rh/fichesPaies',middleware.checkToken,routesFichesPaies);
app.use('/marketing/promotions',middleware.checkToken,routesPromotions);
// app.use('/marketing/compagnesPublicitaires',middleware.checkToken,routesCompagnesPublicitaires);
app.use('/marketing/mannequins',middleware.checkToken,routesMannequins);
app.use('/marketing/albumsMannequins',middleware.checkToken,routesAlbumsMannequins);
app.use('/marketing/paiementsMannequins',middleware.checkToken,routesPaiementsMannequins);
app.use('/marketing/documentsMannequins',middleware.checkToken,routesDocumentsMannequins);
app.use('/reglementsCredits',middleware.checkToken,routesReglementsCredits);
app.use('/reglementsDettes',middleware.checkToken,routesReglementsDettes);
app.use('/historiquesClients', middleware.checkToken, routesHistoriquesClients);
app.use('/historiquesFournisseurs',middleware.checkToken,routesHistoriquesFournisseurs);

app.use('/ordresProduction',middleware.checkToken,routesOrdresProduction);

app.use('/relevesProduction',middleware.checkToken,routesRelevesProduction);
app.use('/materielsProduction',middleware.checkToken,routesMaterielsProduction);
app.use('/activitesProduction',middleware.checkToken,routesActivitesProduction);
app.use('/documentsOrdresProduction',middleware.checkToken,routesDocumentsOrdresProduction);
app.use('/documentsRelevesProduction',middleware.checkToken,routesDocumentsRelevesProduction);
app.use('/paiementsRelevesProduction',middleware.checkToken,routesPaiementsRelevesProduction);
app.use('/paiementsOrdresProduction',middleware.checkToken,routesPaiementsOrdresProduction);

// app.use('/historiquesFournisseurs',middleware.checkToken,routesDocumentsOrdresProduction);


// app.use('/historiquesFournisseurs',middleware.checkToken,routesActivitesProduction);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  console.log('error .....................');
  //next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json( {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.json( {
    message: err.message,
    error: {}
  });
});


function login (req, res) 
{
  let login = req.body.login;
  let password = req.body.password;
  // For the given username fetch user from DB
  if (login && password) {



       utilisateurs.getUtilisateurWithLoginPassword(req.app.locals.dataBase,login,password,function(err,user){

        if(err)
          {
            console.log('errrrrrrrrrrrrrrrr auth');
            res.send(400).json({
              success: false,
              message: 'Authentication failed! Please check the request'
            });
          }
          else {
            if(user !== undefined && user !== null )
              {
                let token = jwt.sign({userData: user},
                  config.secret,
                  { expiresIn: '24h' // expires in 24 hours
                  }
                );
                res.json({
                  success: true,
                  message: 'Authentication successful!',
                  token: token
                });
              }
              else
                {
                  res.json({
                    success: false,
                    message: 'Incorrect username or password'
                  });
                }
          }
       });
    
      // return the JWT token for the future API calls
      }
    
}



module.exports = app;
